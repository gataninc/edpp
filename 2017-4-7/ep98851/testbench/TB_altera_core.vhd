--- Title: TB_altera_core.vhd
--- Description: Testbench for pixel_combiner.
---
---     o  0
---     | /       Copyright (c) 2011
---    (CL)---o   Critical Link, LLC
---      \
---       O
---
--- Company: Critical Link, LLC.
--- Date: 07/12/2011
--- Version: 1.00
---
--- Revision History
---   1.00 - Initial revision
---
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
library UNISIM;
use UNISIM.vcomponents.all;
use STD.TEXTIO.ALL;

--! @brief
--!
entity TB_altera_core is

end TB_altera_core;

--------------------------------------------------------------------------
-- ARCHITECTURE
--------------------------------------------------------------------------
architecture rtl of TB_altera_core is

	--------------------------------------------------------------------------
	-- Constant Declarations
	--------------------------------------------------------------------------
	constant CLK_PERIOD_100MHZ : time := 10 ns; --! Clock period for s_UUT_clk_100MHz.

	--------------------------------------------------------------------------
	-- Signal Declarations 
	--------------------------------------------------------------------------
	signal s_UUT_stop : std_logic := 'Z'; --! Drive high when TB should stop generating clock and ISIM should exit.

	signal s_UUT_clk_100MHz : std_logic := '0'; --! 100 MHz clock for UUT.

	signal s_UUT_i_ABus : std_logic_vector(4 downto 0);
	signal s_UUT_i_DBus : std_logic_vector(31 downto 0);
	signal s_UUT_o_DBus : std_logic_vector(31 downto 0);
	signal s_UUT_i_wr_en : std_logic;
	signal s_UUT_i_rd_en : std_logic;
	signal s_UUT_i_cs : std_logic;	
	signal s_UUT_i_ilevel : std_logic_vector(1 downto 0) := "01"; -- interrupt level (0=4,1=5,2=6,3=7)
	signal s_UUT_i_ilevel_vld : std_logic := '0'; -- interrupt level valid flag
	signal s_UUT_i_ivector : std_logic_vector(4 downto 0) := "00000"; -- interrupt vector (0 through 31)
	signal s_UUT_i_ivector_vld : std_logic := '0'; -- interrupt vector valid flag

	signal s_UUT_altera_ns : std_logic;
	signal s_UUT_altera_data0 : std_logic;
	signal s_UUT_altera_dclk : std_logic;
	signal s_UUT_altera_asd : std_logic;
	signal s_UUT_altera_ncs : std_logic;
	signal s_UUT_altera_nce : std_logic;
	signal s_UUT_altera_ncon : std_logic;
	signal s_UUT_altera_cdone : std_logic;
	signal s_UUT_flash_enable : std_logic;
	signal s_UUT_i_eth_def : std_logic;

	signal s_UUT_i_altera_data_in : std_logic_Vector( 32 downto 0 );
	signal s_UUT_o_altera_data_out : std_logic_vector( 32 downto 0 );
	signal s_UUT_o_altera_data_oen : std_logic;
	signal s_UUT_i_altera_read_busy : std_logic;
	signal s_UUT_o_altera_write_busy : std_logic;
	signal s_UUT_o_altera_rdfifo_empty : std_logic;
	signal s_UUT_o_altera_rdfifo_full : std_logic;		
	signal s_UUT_i_altera_WFF_FF : std_logic;
	signal s_UUT_i_altera_int : std_logic; --! Active high interrupt from Altera chip.
	signal s_UUT_o_altera_rdfifo_rd_en : std_logic;
	signal s_UUT_o_altera_rdfifo_rd_cnt : std_logic_Vector( 12 downto 0 );
	signal s_UUT_o_altera_int_busy : std_logic; --! When high signals Altera chip that we are still handling interrupt... I think.

	signal s_UUT_o_dma_irq : std_logic;
	signal s_UUT_o_dma_req : std_logic := '0';
	signal s_UUT_o_dma_priority_req : std_logic := '0';
	signal s_UUT_o_dma_rwn : std_logic := '0';
	signal s_UUT_o_dma_addr : std_logic_vector(31 downto 0) := (others=>'0');
	signal s_UUT_o_dma_data : std_logic_vector(31 downto 0) := (others=>'0');
	signal s_UUT_o_dma_count : std_logic_vector(8 downto 0) := (others=>'0');
	signal s_UUT_o_dma_be : std_logic_vector(3 downto 0) := "1111";      
	signal s_UUT_i_dma_rd_stb : std_logic := '0';
	signal s_UUT_i_dma_done : std_logic := '0';

	signal s_UUT_o_user_io : std_logic_vector(31 downto 0);

	------------------------------------------------------------------------------
	-- Components 
	------------------------------------------------------------------------------
	component altera_core is
		Port ( 
			emif_clk : in std_logic;
			altera_clk : in std_logic;
			FPGA_YEAR : in std_logic_vector(4 downto 0);
			FPGA_MONTH : in std_logic_vector(3 downto 0);
			FPGA_DAY : in std_logic_vector(4 downto 0);
			FPGA_VERSION_MAJOR : in std_logic_vector(3 downto 0);
			FPGA_VERSION_MINOR : in std_logic_vector(3 downto 0);
			i_ABus : in std_logic_vector(4 downto 0);
			i_DBus : in std_logic_vector(31 downto 0);
			o_DBus : out std_logic_vector(31 downto 0);
			i_wr_en : in std_logic;
			i_rd_en : in std_logic;
			i_cs : in std_logic;	
			i_ilevel : in std_logic_vector(1 downto 0) := "01"; -- interrupt level (0=4,1=5,2=6,3=7)
			i_ilevel_vld : in std_logic := '0'; -- interrupt level valid flag
			i_ivector : in std_logic_vector(4 downto 0) := "00000"; -- interrupt vector (0 through 31)
			i_ivector_vld : in std_logic := '0'; -- interrupt vector valid flag

			-- FLASH ALTERA EEPROM SIGNALS ----------------------------------------------------------
			altera_ns : in std_logic;
			altera_data0 : in std_logic;
			altera_dclk : out std_logic;
			altera_asd : out std_logic;
			altera_ncs : out std_logic;
			altera_nce : out std_logic;
			altera_ncon : out std_logic;
			altera_cdone : in std_logic;
			flash_enable : buffer std_logic;
			i_eth_def : in std_logic;
			-- FLASH ALTERA EEPROM SIGNALS ----------------------------------------------------------

			-- ALTERA DATA INTERFACE SIGNALS --------------------------------------------------------
			i_altera_data_in : in std_logic_Vector( 32 downto 0 );
			o_altera_data_out : out std_logic_vector( 32 downto 0 );
			o_altera_data_oen : out std_logic;
			i_altera_read_busy : in std_logic;
			o_altera_write_busy : out std_logic;
			o_altera_rdfifo_empty : out std_logic;
			o_altera_rdfifo_full : out std_logic;		
			i_altera_WFF_FF : in std_logic;
			i_altera_int : in std_logic; --! Active high interrupt from Altera chip.
			o_altera_rdfifo_rd_en : out std_logic;
			o_altera_rdfifo_rd_cnt : out std_logic_Vector( 12 downto 0 );
			o_altera_int_busy : out std_logic; --! When high signals Altera chip that we are still handling interrupt... I think.
			-- ALTERA DATA INTERFACE SIGNALS ---------------------------------------------------------

			-- DMA INTERFACE SIGNALS ----------------------------------------------------------------
			o_dma_irq : out std_logic;
			o_dma_req : out std_logic := '0';
			o_dma_priority_req : out std_logic := '0';
			o_dma_rwn : out std_logic := '0';
			o_dma_addr : out std_logic_vector(31 downto 0) := (others=>'0');
			o_dma_data : out std_logic_vector(31 downto 0) := (others=>'0');
			o_dma_count : out std_logic_vector(8 downto 0) := (others=>'0');
			o_dma_be : out std_logic_vector(3 downto 0) := "1111";      
			i_dma_rd_stb : in std_logic := '0';
			i_dma_done : in std_logic := '0';
			-- DMA INTERFACE SIGNALS ----------------------------------------------------------------

			o_user_io : buffer std_logic_vector( 31 downto 0 ) );		
	end component altera_core;

begin

	--! Unit Under Test
	UUT: altera_core 
		port map
		( 
			emif_clk => s_UUT_clk_100MHz,
			altera_clk => s_UUT_clk_100MHz,
			FPGA_YEAR => "00000",
			FPGA_MONTH => "0000",
			FPGA_DAY =>  "00000",
			FPGA_VERSION_MAJOR => "0000",
			FPGA_VERSION_MINOR => "0000",
			i_ABus => s_UUT_i_ABus,
			i_DBus => s_UUT_i_DBus,
			o_DBus => s_UUT_o_DBus,
			i_wr_en => s_UUT_i_wr_en,
			i_rd_en => s_UUT_i_rd_en,
			i_cs => s_UUT_i_cs,
			i_ilevel => s_UUT_i_ilevel,
			i_ilevel_vld => s_UUT_i_ivector_vld,
			i_ivector => s_UUT_i_ivector,
			i_ivector_vld => s_UUT_i_ivector_vld,

			-- FLASH ALTERA EEPROM SIGNALS ----------------------------------------------------------
			altera_ns => s_UUT_altera_ns,
			altera_data0 => s_UUT_altera_data0,
			altera_dclk => s_UUT_altera_dclk,
			altera_asd => s_UUT_altera_asd,
			altera_ncs => s_UUT_altera_ncs,
			altera_nce => s_UUT_altera_nce,
			altera_ncon => s_UUT_altera_ncon,
			altera_cdone => s_UUT_altera_cdone,
			flash_enable => s_UUT_flash_enable,
			i_eth_def => s_UUT_i_eth_def,
			-- FLASH ALTERA EEPROM SIGNALS ----------------------------------------------------------

			-- ALTERA DATA INTERFACE SIGNALS --------------------------------------------------------
			i_altera_data_in => s_UUT_i_altera_data_in,
			o_altera_data_out => s_UUT_o_altera_data_out,
			o_altera_data_oen => s_UUT_o_altera_data_oen,
			i_altera_read_busy => s_UUT_i_altera_read_busy,
			o_altera_write_busy => s_UUT_o_altera_write_busy,
			o_altera_rdfifo_empty => s_UUT_o_altera_rdfifo_empty,
			o_altera_rdfifo_full => s_UUT_o_altera_rdfifo_full,
			i_altera_WFF_FF => s_UUT_i_altera_WFF_FF,
			i_altera_int => s_UUT_i_altera_int,
			o_altera_rdfifo_rd_en => s_UUT_o_altera_rdfifo_rd_en,
			o_altera_rdfifo_rd_cnt => s_UUT_o_altera_rdfifo_rd_cnt,
			o_altera_int_busy => s_UUT_o_altera_int_busy,
			-- ALTERA DATA INTERFACE SIGNALS ---------------------------------------------------------

			-- DMA INTERFACE SIGNALS ----------------------------------------------------------------
			o_dma_irq => s_UUT_o_dma_irq,
			o_dma_req => s_UUT_o_dma_req,
			o_dma_priority_req => s_UUT_o_dma_priority_req,
			o_dma_rwn => s_UUT_o_dma_rwn,
			o_dma_addr => s_UUT_o_dma_addr,
			o_dma_data => s_UUT_o_dma_data,
			o_dma_count => s_UUT_o_dma_count,
			o_dma_be => s_UUT_o_dma_be,
			i_dma_rd_stb => s_UUT_i_dma_rd_stb,
			i_dma_done => s_UUT_i_dma_done,
			-- DMA INTERFACE SIGNALS ----------------------------------------------------------------

			o_user_io => s_UUT_o_user_io
		);

	--! Generate the 100 MHz TB clock. 
	CLK_100MHZ_GEN_PROC : process
	begin
		s_UUT_clk_100MHz <= '0';
		wait for CLK_PERIOD_100MHZ/2;
		s_UUT_clk_100MHz <= '1';
		wait for CLK_PERIOD_100MHZ/2;

		if (s_UUT_stop = '1') then
			wait;
		end if;
	end process CLK_100MHZ_GEN_PROC;

	--! Generate test stimulus.
	TB_PROC : process
	begin
		-- Make sure the system clock(s) are running.
		s_UUT_stop <= 'Z';

		wait until rising_edge(s_UUT_clk_100MHz);

		----------------------------------------------------	
		-- Program EMIF control registers
		----------------------------------------------------

		-- iADR_DMA_IRQ_CLR: Any write to this register clears the pending interrupt
		s_UUT_i_ABus <= STD_LOGIC_VECTOR(TO_UNSIGNED(13, 5));
		s_UUT_i_wr_en <= '1';
		s_UUT_i_cs <= '1';
		wait until rising_edge(s_UUT_clk_100MHz);

		-- iADR_DMA_START_ADDR: Offset into memory space where FPGA DMA space starts
		s_UUT_i_ABus <= STD_LOGIC_VECTOR(TO_UNSIGNED(14, 5));
		s_UUT_i_DBus <= X"80000000"; -- s_dma_base_addr 
		s_UUT_i_wr_en <= '1';
		s_UUT_i_cs <= '1';
		wait until rising_edge(s_UUT_clk_100MHz);

		-- iADR_DMA_SPACE_MASK:
		s_UUT_i_ABus <= STD_LOGIC_VECTOR(TO_UNSIGNED(15, 5));
		s_UUT_i_DBus <= X"0000FFFF"; -- s_dma_space_mask
		s_UUT_i_wr_en <= '1';
		s_UUT_i_cs <= '1';
		wait until rising_edge(s_UUT_clk_100MHz);

		-- iADR_DMA_INT_THRESHOLD:
		s_UUT_i_ABus <= STD_LOGIC_VECTOR(TO_UNSIGNED(17, 5));
		s_UUT_i_DBus <= STD_LOGIC_VECTOR(TO_UNSIGNED(5000, 32)); -- s_dma_timeout_threshold
		s_UUT_i_wr_en <= '1';
		s_UUT_i_cs <= '1';
		wait until rising_edge(s_UUT_clk_100MHz);

		-- iADR_DMA_EN_REG:
		s_UUT_i_ABus <= STD_LOGIC_VECTOR(TO_UNSIGNED(12, 5));
		s_UUT_i_DBus(0) <= '1'; -- s_dma_en
		s_UUT_i_DBus(2) <= '1'; -- s_altera_int_en
		s_UUT_i_wr_en <= '1';
		s_UUT_i_cs <= '1';
		wait until rising_edge(s_UUT_clk_100MHz);

		-- iADR_WR_FLASH_ENABLE: DMA will not run unless flash is disabled
		s_UUT_i_ABus <= STD_LOGIC_VECTOR(TO_UNSIGNED(2, 5));
		s_UUT_i_DBus(0) <= '0'; -- Flash_Enable
		s_UUT_i_wr_en <= '1';
		s_UUT_i_cs <= '1';
		wait until rising_edge(s_UUT_clk_100MHz);

		----------------------------------------------------
		-- DMA triggered by interrupt from Altera FPGA
		----------------------------------------------------

		-- Write some words to the FIFO
		s_UUT_i_altera_data_in(0) <= '0';
		s_UUT_i_altera_data_in(31 downto 1) <= STD_LOGIC_VECTOR(TO_UNSIGNED(1, 31));
		s_UUT_i_altera_data_in(32) <= '1'; -- tied to wr_en for Altera_ReadFifo
		s_UUT_i_altera_read_busy <= '1'; -- tied to wr_en for Altera_ReadFifo
		wait until rising_edge(s_UUT_clk_100MHz);

		s_UUT_i_altera_data_in(0) <= '0';
		s_UUT_i_altera_data_in(31 downto 1) <= STD_LOGIC_VECTOR(TO_UNSIGNED(2, 31));
		s_UUT_i_altera_data_in(32) <= '1'; -- tied to wr_en for Altera_ReadFifo
		s_UUT_i_altera_read_busy <= '1'; -- tied to wr_en for Altera_ReadFifo
		wait until rising_edge(s_UUT_clk_100MHz);

		s_UUT_i_altera_data_in(0) <= '0';
		s_UUT_i_altera_data_in(31 downto 1) <= STD_LOGIC_VECTOR(TO_UNSIGNED(2, 31));
		s_UUT_i_altera_data_in(32) <= '1'; -- tied to wr_en for Altera_ReadFifo
		s_UUT_i_altera_read_busy <= '1'; -- tied to wr_en for Altera_ReadFifo
		wait until rising_edge(s_UUT_clk_100MHz);

		s_UUT_i_altera_data_in(0) <= '0';
		s_UUT_i_altera_data_in(31 downto 1) <= STD_LOGIC_VECTOR(TO_UNSIGNED(3, 31));
		s_UUT_i_altera_data_in(32) <= '1'; -- tied to wr_en for Altera_ReadFifo
		s_UUT_i_altera_read_busy <= '1'; -- tied to wr_en for Altera_ReadFifo
		wait until rising_edge(s_UUT_clk_100MHz);

		s_UUT_i_altera_data_in <= (others => '0'); -- tied to wr_en for Altera_ReadFifo
		s_UUT_i_altera_read_busy <= '0'; -- tied to wr_en for Altera_ReadFifo

		-- Now trigger the interrupt
		s_UUT_i_altera_int <= '1';	
		wait until rising_edge(s_UUT_clk_100MHz);

		s_UUT_i_altera_int <= '0';	
		wait until s_UUT_o_dma_req = '1';
	
		-- Read four samples for DMA	
		s_UUT_i_dma_rd_stb <= '1';
		for ii in 0 to 3 loop
			wait until rising_edge(s_UUT_clk_100MHz);
		end loop;

		s_UUT_i_dma_rd_stb <= '0';
		s_UUT_i_dma_done <= '1';
		wait until rising_edge(s_UUT_clk_100MHz);

		s_UUT_i_dma_done <= '0';
	
		wait until s_UUT_o_dma_irq = '1';
		
		-- iADR_DMA_IRQ_CLR: Any write to this register clears the pending interrupt
		s_UUT_i_ABus <= STD_LOGIC_VECTOR(TO_UNSIGNED(13, 5));
		s_UUT_i_wr_en <= '1';
		s_UUT_i_cs <= '1';
		wait until rising_edge(s_UUT_clk_100MHz);

		-- Loop to wait for the end to settle and provide space between tests
		for ii in 0 to 127 loop
			wait until rising_edge(s_UUT_clk_100MHz);
		end loop;

		----------------------------------------------------
		-- DMA triggered by filling FIFO
		----------------------------------------------------

		-- iADR_DMA_INT_THRESHOLD:
--		s_UUT_i_ABus <= STD_LOGIC_VECTOR(TO_UNSIGNED(17, 5));
--		s_UUT_i_DBus <= STD_LOGIC_VECTOR(TO_UNSIGNED(500000, 32)); -- s_dma_timeout_threshold
--		s_UUT_i_wr_en <= '1';
--		s_UUT_i_cs <= '1';
--		wait until rising_edge(s_UUT_clk_100MHz);

		----------------------------------------------------
		-- DMA triggered by timeout counter reaching threshold	
		----------------------------------------------------

		-- iADR_DMA_INT_THRESHOLD:
--		s_UUT_i_ABus <= STD_LOGIC_VECTOR(TO_UNSIGNED(17, 5));
--		s_UUT_i_DBus <= STD_LOGIC_VECTOR(TO_UNSIGNED(50, 32)); -- s_dma_timeout_threshold
--		s_UUT_i_wr_en <= '1';
--		s_UUT_i_cs <= '1';
--		wait until rising_edge(s_UUT_clk_100MHz);


		----------------------------------------------------
		-- All Done
		----------------------------------------------------
		assert(true);
			report "Simulation Stimuli Complete!"
				severity warning;

		-- We're done, tell the clock to stop.
		s_UUT_stop <= '1';
		wait;
	end process TB_PROC;

end rtl; -- of TB_altera_core

