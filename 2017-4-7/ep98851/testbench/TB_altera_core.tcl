restart

divider add "UUT Ports:"

wave add /tb_altera_core/s_UUT_stop -radix hex

wave add /tb_altera_core/s_UUT_clk_100MHz -radix hex

wave add /tb_altera_core/s_UUT_i_ABus -radix hex
wave add /tb_altera_core/s_UUT_i_DBus -radix hex
wave add /tb_altera_core/s_UUT_o_DBus -radix hex
wave add /tb_altera_core/s_UUT_i_wr_en -radix hex
wave add /tb_altera_core/s_UUT_i_rd_en -radix hex
wave add /tb_altera_core/s_UUT_i_cs -radix hex
# wave add /tb_altera_core/s_UUT_i_ilevel -radix hex
# wave add /tb_altera_core/s_UUT_i_ilevel_vld -radix hex
# wave add /tb_altera_core/s_UUT_i_ivector -radix hex
# wave add /tb_altera_core/s_UUT_i_ivector_vld -radix hex

# wave add /tb_altera_core/s_UUT_altera_ns -radix hex
# wave add /tb_altera_core/s_UUT_altera_data0 -radix hex
# wave add /tb_altera_core/s_UUT_altera_dclk -radix hex
# wave add /tb_altera_core/s_UUT_altera_asd -radix hex
# wave add /tb_altera_core/s_UUT_altera_ncs -radix hex
# wave add /tb_altera_core/s_UUT_altera_nce -radix hex
# wave add /tb_altera_core/s_UUT_altera_ncon -radix hex
# wave add /tb_altera_core/s_UUT_altera_cdone -radix hex
# wave add /tb_altera_core/s_UUT_flash_enable -radix hex
# wave add /tb_altera_core/s_UUT_i_eth_def -radix hex

wave add /tb_altera_core/s_UUT_i_altera_data_in -radix hex
wave add /tb_altera_core/s_UUT_o_altera_data_out -radix hex
wave add /tb_altera_core/s_UUT_o_altera_data_oen -radix hex
wave add /tb_altera_core/s_UUT_i_altera_read_busy -radix hex
wave add /tb_altera_core/s_UUT_o_altera_write_busy -radix hex
wave add /tb_altera_core/s_UUT_o_altera_rdfifo_empty -radix hex
wave add /tb_altera_core/s_UUT_o_altera_rdfifo_full -radix hex
wave add /tb_altera_core/s_UUT_i_altera_WFF_FF -radix hex
wave add /tb_altera_core/s_UUT_i_altera_int -radix hex
wave add /tb_altera_core/s_UUT_o_altera_rdfifo_rd_en -radix hex
wave add /tb_altera_core/s_UUT_o_altera_rdfifo_rd_cnt -radix hex
wave add /tb_altera_core/s_UUT_o_altera_int_busy -radix hex

wave add /tb_altera_core/s_UUT_o_dma_irq -radix hex
wave add /tb_altera_core/s_UUT_o_dma_req -radix hex
wave add /tb_altera_core/s_UUT_o_dma_priority_req -radix hex
wave add /tb_altera_core/s_UUT_o_dma_rwn -radix hex
wave add /tb_altera_core/s_UUT_o_dma_addr -radix hex
wave add /tb_altera_core/s_UUT_o_dma_data -radix hex
wave add /tb_altera_core/s_UUT_o_dma_count -radix hex
wave add /tb_altera_core/s_UUT_o_dma_be -radix hex
wave add /tb_altera_core/s_UUT_i_dma_rd_stb -radix hex
wave add /tb_altera_core/s_UUT_i_dma_done -radix hex

# wave add /tb_altera_core/s_UUT_o_user_io -radix hex

divider add "UUT Internal:"

wave add /tb_altera_core/uut/s_altera_int_en -radix hex

wave add /tb_altera_core/uut/s_i_altera_int_r1 -radix hex
wave add /tb_altera_core/uut/s_i_altera_int_r2 -radix hex

wave add /tb_altera_core/uut/Read_Fifo_Data_in -radix hex
wave add /tb_altera_core/uut/Read_Fifo_Data_Ready -radix hex
wave add /tb_altera_core/uut/s_altera_fifo_rst -radix hex
wave add /tb_altera_core/uut/s_altera_rdfifo_full -radix hex
wave add /tb_altera_core/uut/s_altera_rdfifo_din -radix hex
wave add /tb_altera_core/uut/s_altera_rdfifo_wr_en -radix hex
wave add /tb_altera_core/uut/s_altera_rdfifo_rd_en -radix hex
wave add /tb_altera_core/uut/s_altera_rdfifo_dout -radix hex
wave add /tb_altera_core/uut/s_altera_rdfifo_valid -radix hex
wave add /tb_altera_core/uut/s_altera_rdfifo_empty -radix hex
wave add /tb_altera_core/uut/s_altera_rdfifo_rd_cnt -radix hex

wave add /tb_altera_core/uut/Write_Fifo_Data_in -radix hex
wave add /tb_altera_core/uut/Write_Fifo_data_out -radix hex
wave add /tb_altera_core/uut/Write_Fifo_ef -radix hex
wave add /tb_altera_core/uut/Write_Fifo_ff -radix hex
wave add /tb_altera_core/uut/Write_Fifo_Write_En -radix hex
wave add /tb_altera_core/uut/Write_fifo_wr_Cnt -radix hex

wave add /tb_altera_core/uut/version_reg -radix hex
wave add /tb_altera_core/uut/version_reg1 -radix hex
wave add /tb_altera_core/uut/ver_rd -radix hex

wave add /tb_altera_core/uut/s_altera_data_oen -radix hex
wave add /tb_altera_core/uut/s_altera_write_busy -radix hex
wave add /tb_altera_core/uut/s_altera_int_busy -radix hex

wave add /tb_altera_core/uut/s_err_clr -radix hex

wave add /tb_altera_core/uut/s_err_invalid_rd -radix hex

wave add /tb_altera_core/uut/s_dma_state -radix hex

wave add /tb_altera_core/uut/s_dma_en -radix hex

wave add /tb_altera_core/uut/s_dma_timeout_cntr -radix hex
wave add /tb_altera_core/uut/s_dma_timeout_threshold -radix hex

wave add /tb_altera_core/uut/s_dma_irq -radix hex
wave add /tb_altera_core/uut/s_dma_count -radix hex
wave add /tb_altera_core/uut/s_dma_req -radix hex
wave add /tb_altera_core/uut/s_dma_start_addr -radix hex
wave add /tb_altera_core/uut/s_dma_base_addr -radix hex
wave add /tb_altera_core/uut/s_dma_space_addr -radix hex
wave add /tb_altera_core/uut/s_dma_space_mask -radix hex

run all
