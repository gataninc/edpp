-- Module: core_version.vhd

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity core_version is
   port (
      reset         : in std_logic := '0';                -- reset
      clk           : in std_logic;                       -- system clock
      rd            : in std_logic;                       -- read enable
      ID            : in std_logic_vector(7 downto 0);    -- assigned ID number, 0xFF if unassigned
      version_major : in std_logic_vector(3 downto 0);    -- major version number 1-15
      version_minor : in std_logic_vector(3 downto 0);    -- minor version number 0-15
      year          : in std_logic_vector(4 downto 0);    -- year since 2000
      month         : in std_logic_vector(3 downto 0);    -- month (1-12)
      day           : in std_logic_vector(4 downto 0);    -- day (1-32)
      ilevel        : in std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      ilevel_vld    : in std_logic := '0';                           -- interrupt level valid flag
      ivector       : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      ivector_vld   : in std_logic := '0';                           -- interrupt vector valid flag
      o_data        : out std_logic_vector(31 downto 0)
      );
end core_version;

architecture rtl of core_version is

signal reg_a     : std_logic_vector(31 downto 0);
signal reg_b     : std_logic_vector(31 downto 0);
signal rd_toggle : std_logic := '0';  -- toggle for read enables

begin

reg_a <= "00" & year & month & day & ID & version_major & version_minor;
reg_b <= "0100" & x"0000" & "000" & ivector_vld & ivector & ilevel_vld & ilevel;

-- This process is in place for future additions of "pipelined" or "FIFO'd" version 
-- data that may need to extend past on register.
-- 
read_proc : process (clk)
begin
   if clk'event and clk='1' then
      if rd='1' then
         rd_toggle <= not rd_toggle;
      end if;
      case rd_toggle is
         when '0' =>
            o_data <= reg_a;
         when '1' =>
            o_data <= reg_b;
         when others => null;
      end case;
   end if;
end process read_proc; 

end rtl;  
