--- Title: SRL16xN.vhd
--- Description: Arbitrary width SRL16 component.
---
---     o  0                          
---     | /       Copyright (c) 2008  
---    (CL)---o   Critical Link, LLC  
---      \                            
---       O                           
---
--- Company: Critical Link, LLC
--- Date: 12/19/2008
--- Version: 
---   1.00 - 12/19/2008 - Initial Version
---   

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity SRL16xN is
   generic (
      N    : positive := 1;
      INIT : bit_vector(0 to 15) := x"0000"
   );
   port (
      CLK : in  std_logic;
      A   : in  std_logic_vector(3 downto 0);
      D   : in  std_logic_vector((N-1) downto 0);
      Q   : out std_logic_vector((N-1) downto 0)
   );
end SRL16xN;

architecture structure of SRL16xN is

begin -- architecture: structure

SRL16_gen:
for i in 0 to (N-1) generate
begin
   SRL16_inst : SRL16
      generic map (
         INIT => INIT
      )
      port map (
         Q   => Q(i),
         A0  => A(0),
         A1  => A(1),
         A2  => A(2),
         A3  => A(3),
         CLK => CLK,
         D   => D(i)
      );
end generate;

end structure;
