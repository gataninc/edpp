--- Title: capture_engine.vhd
--- Description: Capture Engine Core (aka: glorified FIFO interface).
---
---     o  0
---     | /       Copyright (c) 2006 - 2007
---    (CL)---o   Critical Link, LLC
---      \
---       O
---
--- Company: Critical Link, LLC.
--- Date: 2/15/2007
--- Version: 1.01
--- Revisions: 1.01 - Added burst-complete toggle input.
---

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity capture_engine is
   generic
   (
      FIFO_DEPTH_TWO_TO_N : integer range 10 to 11 := 10 -- 10 is 1K words
                                                         -- 11 is 2K words
   );
   port
   (
      -- EMIF Interface signals
      clk        : in  std_logic;
      i_ABus     : in  std_logic_vector(4 downto 0);
      i_DBus     : in  std_logic_vector(31 downto 0);
      o_DBus     : out std_logic_vector(31 downto 0);
      i_wr_en    : in  std_logic;
      i_rd_en    : in  std_logic;
      i_cs       : in  std_logic;	
      -- IRQ system signals
      o_irq      : out std_logic;
      i_ilevel        : in std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld    : in std_logic := '0';                           -- interrupt level valid flag
      i_ivector       : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld   : in std_logic := '0';                           -- interrupt vector valid flag
      -- Core/Applicaion signals
      i_fifo_din     : in  std_logic_vector(31 downto 0);
      i_fifo_wr_en   : in  std_logic;
      i_fifo_wr_clk  : in  std_logic;
      i_bc_tgl       : in  std_logic  -- burst-complete toggle input
    );
end capture_engine;


architecture rtl of capture_engine is

-- All Used components should be declared first.
--
component core_version
   port (
      reset         : in std_logic := '0';                -- reset
      clk           : in std_logic;                       -- system clock
      rd            : in std_logic;                       -- read enable
      ID            : in std_logic_vector(7 downto 0);    -- assigned ID number, 0xFF if unassigned
      version_major : in std_logic_vector(3 downto 0);    -- major version number 1-15
      version_minor : in std_logic_vector(3 downto 0);    -- minor version number 0-15
      year          : in std_logic_vector(4 downto 0);    -- year since 2000
      month         : in std_logic_vector(3 downto 0);    -- month (1-12)
      day           : in std_logic_vector(4 downto 0);    -- day (1-32)
      ilevel        : in std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      ilevel_vld    : in std_logic := '0';                           -- interrupt level valid flag
      ivector       : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      ivector_vld   : in std_logic := '0';                           -- interrupt vector valid flag
      o_data        : out std_logic_vector(31 downto 0)
      );
end component;

component capture_afifo1024x32
   port (
   din: IN std_logic_VECTOR(31 downto 0);
   rd_clk: IN std_logic;
   rd_en: IN std_logic;
   rst: IN std_logic;
   wr_clk: IN std_logic;
   wr_en: IN std_logic;
   dout: OUT std_logic_VECTOR(31 downto 0);
   empty: OUT std_logic;
   full: OUT std_logic;
   rd_data_count: OUT std_logic_VECTOR(9 downto 0));
end component;

component capture_afifo2048x32
   port (
   din: IN std_logic_VECTOR(31 downto 0);
   rd_clk: IN std_logic;
   rd_en: IN std_logic;
   rst: IN std_logic;
   wr_clk: IN std_logic;
   wr_en: IN std_logic;
   dout: OUT std_logic_VECTOR(31 downto 0);
   empty: OUT std_logic;
   full: OUT std_logic;
   rd_data_count: OUT std_logic_VECTOR(10 downto 0));
end component;

signal version_reg       : std_logic_vector(31 downto 0);  -- version register
signal ver_rd            : std_logic;
signal reset             : std_logic;

-- FIFO status register fields
signal fsr_lvl           : std_logic_vector(FIFO_DEPTH_TWO_TO_N-1 downto 0);  -- 
signal fsr_f             : std_logic;  -- fifo full
signal fsr_tqf           : std_logic;  -- fifo three quarters full
signal fsr_hf            : std_logic;  -- fifo half full
signal fsr_qf            : std_logic;  -- fifo quarter full
signal fsr_e             : std_logic;  -- fifo empty

-- Burst-complete signals
signal ccsr_bc           : std_logic := '0'; -- burst complete flag
signal bc_tgl_r1         : std_logic := '0'; -- burst complete toggle register 1
signal bc_tgl_r2         : std_logic := '0'; -- burst complete toggle register 2
signal bc_clear          : std_logic := '0'; -- used to clear burst complete flag
signal bc_clear_r1       : std_logic := '0'; -- used to clear burst complete flag

-- interrupt enable register fields
signal ier_bcie          : std_logic;  -- burst complete ie
signal ier_fie           : std_logic;  -- fifo full ie
signal ier_tqfie         : std_logic;  -- three quarters ie
signal ier_hfie          : std_logic;  -- half full ie
signal ier_qfie          : std_logic;  -- quarter full ie

-- FIFO control and data lines
signal fifo_rd_en        : std_logic;
--signal data_empty        : std_logic;
--signal fdata_avail       : std_logic := '0';
signal fifo_dout         : std_logic_vector(31 downto 0);  -- fifo data output

begin -- architecture: rtl of capture_engine

reset_on_cfg: roc port map (O => reset);

version : core_version
   port map(
      reset         => reset,
      clk           => clk,                           -- system clock
      rd            => ver_rd,                        -- read enable
      ID            => CONV_STD_LOGIC_VECTOR( 255, 8), -- assigned ID number, 0xFF if unassigned
      version_major => CONV_STD_LOGIC_VECTOR( 01, 4), -- major version number 1-15
      version_minor => CONV_STD_LOGIC_VECTOR( 01, 4), -- minor version number 0-15
      year          => CONV_STD_LOGIC_VECTOR( 07, 5), -- year since 2000
      month         => CONV_STD_LOGIC_VECTOR( 02, 4), -- month (1-12)
      day           => CONV_STD_LOGIC_VECTOR( 15, 5), -- day (1-31)
      ilevel        => i_ilevel,
      ilevel_vld    => i_ilevel_vld,
      ivector       => i_ivector,
      ivector_vld   => i_ivector_vld,
      o_data        => version_reg
      );

ONE_K_FIFO: if FIFO_DEPTH_TWO_TO_N=10 generate
data_fifo : capture_afifo1024x32
   port map (
   din           => i_fifo_din,
   wr_en         => i_fifo_wr_en,
   wr_clk        => i_fifo_wr_clk,
   rd_en         => fifo_rd_en,
   rd_clk        => clk,
   rst           => reset,
   dout          => fifo_dout,
   full          => fsr_f,
   empty         => fsr_e,
   rd_data_count => fsr_lvl
   );
end generate ONE_K_FIFO;

TWO_K_FIFO: if FIFO_DEPTH_TWO_TO_N=11 generate
data_fifo : capture_afifo2048x32
   port map (
   din           => i_fifo_din,
   wr_en         => i_fifo_wr_en,
   wr_clk        => i_fifo_wr_clk,
   rd_en         => fifo_rd_en,
   rd_clk        => clk,
   rst           => reset,
   dout          => fifo_dout,
   full          => fsr_f,
   empty         => fsr_e,
   rd_data_count => fsr_lvl
   );
end generate TWO_K_FIFO;

o_irq <= (ier_bcie  and ccsr_bc) or -- burst complete
         (ier_fie   and fsr_f)   or -- fifo full
         (ier_tqfie and fsr_tqf) or -- three quarters full
         (ier_hfie  and fsr_hf)  or -- half full
         (ier_qfie  and fsr_qf);    -- quarter full

fsr_tqf <= '1' when fsr_lvl(FIFO_DEPTH_TWO_TO_N-1 downto FIFO_DEPTH_TWO_TO_N-2)="11" else '0';
fsr_hf  <= fsr_lvl(FIFO_DEPTH_TWO_TO_N-1);
fsr_qf  <= fsr_lvl(FIFO_DEPTH_TWO_TO_N-1) or fsr_lvl(FIFO_DEPTH_TWO_TO_N-2);

---- First-word fall-through now taken care of by CoreGen's FIFO Generator
--fifo_rd_en <= NOT fsr_fe and NOT fdata_avail;
--data_empty <= fsr_e and NOT fdata_avail;

--* Handle read requests from the processor
--/
read_regs : process (clk) is
begin
   if clk'event and clk='1' then

---- First-word fall-through now taken care of by CoreGen's FIFO Generator
--      if (fifo_rd_en = '1') then
--         fdata_avail <= '1';
--      elsif (i_ABus="00110" and i_cs='1' and i_rd_en='1') then
--         fdata_avail <= '0';
--      end if;

      if (i_ABus="00110" and i_cs='1' and i_rd_en='1') then
         fifo_rd_en <= '1';
      else
         fifo_rd_en <= '0';
     end if;

      -- toggle version register 
      if i_ABus="00000" and i_cs='1' and i_rd_en='1' then
         ver_rd <= '1';
      else
         ver_rd <= '0';
      end if;

      -- address decoding
      if i_cs = '0' then
         o_DBus <= (others=>'0');
      else
         case i_ABus is
            when "00000" =>
               o_DBus <=  version_reg;
            when "00001" =>
               o_DBus(31 downto 4) <= x"0000000";
               o_DBus(3) <= ccsr_bc;
               o_DBus(2) <='0';
               o_DBus(1) <='0';
               o_DBus(0) <='0';
            when "00100" =>
               o_DBus(31 downto 0) <= CONV_STD_LOGIC_VECTOR(0, 16-FIFO_DEPTH_TWO_TO_N) & fsr_lvl 
                                    & x"00" & "000" & fsr_f & fsr_tqf & fsr_hf & fsr_qf & fsr_e; 
            when "00101" =>
               o_DBus(31 downto 0) <= x"000000" & "000" & ier_bcie & ier_fie & ier_tqfie & ier_hfie & ier_qfie;
            when "00110" =>
               o_DBus(31 downto 0) <= fifo_dout;
            when "00111" =>
               o_DBus(31 downto 0) <= CONV_STD_LOGIC_VECTOR(2**FIFO_DEPTH_TWO_TO_N,32);  -- FIFO DEPTH
            when others =>
               o_DBus <= (others=>'X');
         end case;
      end if;
   end if;
end process read_regs;

--* Decode register write requests.
--/
write_regs : process(clk)
begin
   if clk='1' and clk'event then
      if i_cs = '1' and i_wr_en = '1' then
         case i_ABus is
            when "00001" =>
               if i_DBus(3)='1' then
                  bc_clear <= not bc_clear; -- toggle burst complete clearing register
               end if;
            when "00101" =>
               ier_bcie  <= i_DBus(4);
               ier_fie   <= i_DBus(3);
               ier_tqfie <= i_DBus(2);
               ier_hfie  <= i_DBus(1);
               ier_qfie  <= i_DBus(0);
            when others => NULL;
         end case;
      end if;
   end if;
end process write_regs;

burst_complete : process(clk)
begin
   if clk='1' and clk'event then
      bc_tgl_r1 <= i_bc_tgl;
      bc_tgl_r2 <= bc_tgl_r1;
      bc_clear_r1 <= bc_clear;

      if bc_tgl_r1 /= bc_tgl_r2 then
         ccsr_bc <= '1'; -- set burst complete flag
      elsif bc_clear /= bc_clear_r1 then
         ccsr_bc <= '0'; -- reset burst complete flag
      end if;
   end if;
end process burst_complete;

end rtl;
