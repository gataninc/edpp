--- File: C645x_EMIF.vhd
--- Description: 
---
---     o  0
---     | /       Copyright (c) 2007-2008
---    (CL)---o   Critical Link, LLC
---      \
---       O
---
--- Company: Critical Link, LLC.
--- Date: 2/25/2008
--- Version: 1.01
--- Revisions: 1.00 - Initial version.
---            1.01 - Added capability for accessing CE4 memory space.
---            

library WORK;
library IEEE;
library UNISIM;
use WORK.MITYDSP_PKG.ALL;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use UNISIM.VCOMPONENTS.ALL;

entity C645x_EMIF is
   generic (
      DECODE_BITS : integer range 1 to 15 := 5
   );
   port (
      emif_clk : in std_logic;
      -- EMIF signals
      i_ce2_n : in  std_logic := '1'; -- 0xA0000000 - Asynchronous Memory Space - Mity-1 CE3 @ 0xB0000000
      i_ce4_n : in  std_logic := '1'; -- 0xC0000000 - Synchronous Memory Space
      i_aoe_n : in  std_logic := '1'; -- Async Read-Strobe (Mity-1 ARE_N) / Sync Output-Enable
      i_awe_n : in  std_logic := '1'; -- Async/Sync Write-Enable
      i_rw_n  : in  std_logic := '1'; -- Read-high, Write-low - inverted version of Mity-1 AOE_N
      i_be_n  : in  std_logic_vector(3 downto 0) := (others=>'1');
      i_ea    : in  std_logic_vector(19 downto 0) := (others=>'0');
      i_ed    : in  std_logic_vector(31 downto 0) := (others=>'0');
      o_ed    : out std_logic_vector(31 downto 0);
      t_ed    : out std_logic;
      -- Async Core Interface signals
      be_r   : out std_logic_vector(3 downto 0) := (others=>'1');
      addr_r : out std_logic_vector(4 downto 0) := (others=>'0'); -- Async Address
      cs_r   : out std_logic_vector((2**DECODE_BITS)-1 downto 0) := (others=>'0'); -- Async Core-Select
      rd_lead_r  : out std_logic := '0'; -- old read strobe - leading edge of DSP strobe - risky for post-read ops
      rd_trail_r : out std_logic := '0'; -- new read strobe - trailing edge of DSP strobe - safe for post-read ops
      wr_r   : out std_logic := '0'; -- write strobe - leading edge of DSP strobe
      edi_r  : out std_logic_vector(31 downto 0);
      -- Data-Out-Bus-Mux Inputs
      edo_ce4 : in std_logic_vector(31 downto 0) := (others=>'0');
      edo     : in bus32_vector((2**DECODE_BITS)-1 downto 0)
   );
end C645x_EMIF;

architecture rtl of C645x_EMIF is

signal reset: std_logic;
signal be_n: std_logic_vector(3 downto 0) := (others=>'1'); -- active-low
signal ea: std_logic_vector(19 downto 0) := (others=>'0');
signal ce2: std_logic := '0'; -- active-high
signal aoe: std_logic := '0'; -- active-high
signal aoe_dly: std_logic := '0'; -- active-high
signal awe: std_logic := '0'; -- active-high
signal awe_dly: std_logic := '0'; -- active-high
signal rw: std_logic := '1'; -- read-high, write-low
signal edi: std_logic_vector(31 downto 0);

begin -- architecture: rtl

rst_on_cfg: roc port map (O => reset);  -- Reset On Configuration

-- register input signals
process (emif_clk)
begin
   if rising_edge(emif_clk) then
      -- capture byte-enables and address bus
      be_n <= i_be_n; -- cores expect the active-low signals
      ea <= i_ea;
      -- capture control signals
      ce2 <= not i_ce2_n;
      aoe <= not i_aoe_n;
      awe <= not i_awe_n;
      rw <= i_rw_n;
      -- one-clock delayed registers
      aoe_dly <= aoe;
      awe_dly <= awe;
      -- capture data bus
      edi <= i_ed;
   end if;
end process;

-- register output signals
process (emif_clk, reset)
begin
   if reset='1' then
      be_r <= (others=>'1');
      addr_r <= (others=>'0');
      cs_r <= (others=>'0');
      rd_lead_r <= '0';
      rd_trail_r <= '0';
      wr_r <= '0';
   elsif rising_edge(emif_clk) then
      be_r <= be_n; -- cores expect the active-low signals
      addr_r <= ea(4 downto 0);

      -- address decode
      if ce2='1' then
         for count in 0 to (2**DECODE_BITS)-1 loop
            if ea(DECODE_BITS+4 downto 5) = conv_std_logic_vector(count, DECODE_BITS) then
               cs_r(count) <= '1';
            else
               cs_r(count) <= '0';
            end if;
         end loop;
      else
         cs_r <= (others=>'0');
      end if;

      -- read/write strobes
      rd_lead_r <= aoe and not aoe_dly;
      rd_trail_r <= not aoe and aoe_dly;
      wr_r <= awe and not awe_dly;

      edi_r <= edi;
   end if;
end process;

-- multiplex module data-out busses
process (emif_clk)
   variable ored : std_logic_vector(31 downto 0);
begin
   if rising_edge(emif_clk) then
      ored := (others=>'0');
      
      for count in 0 to (2**DECODE_BITS)-1 loop
         ored := ored or edo(count);
      end loop;
      
      ored := ored or edo_ce4;
      
      o_ed <= ored;
   end if;
end process;

-- drive tri-state enable line
t_ed <= '0' when (i_ce2_n = '0' and i_rw_n = '1') or
                 (i_ce4_n = '0' and i_aoe_n = '0') else '1';

end rtl;
