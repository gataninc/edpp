--- Title: PTPTimeStamp.vhd
--- Description: This module is a helper module and provides ethernet
--- MAC layer filtering/editing specific to the Precision Time
--- Protocol implementation developed for MityDSP applications.
---
--- This module is indended to integrate with the MityDSP clock1305
--- core and the cl_eth_671x MAC layer core.  It essentially looks
--- for PTP multicast addresses, zeros the UDP layer checksum (because
--- it is editing the packet data on the way out the door), and 
--- replaces the timestamp field with a real-time latched version
--- of the timestamp from the clock1305 core.  This operation is 
--- performed for transmit and received packets in order to allow
--- accurate timestamps on send and receive data.
---
---     o  0
---     | /       Copyright (c) 2008
---    (CL)---o   Critical Link, LLC
---      \
---       O
---
--- Company: Critical Link, LLC.
--- Date: 8/20/2008
--- Version: 1.00
---
--- Revision History
---   1.00 - Baseline Code
---
---
library IEEE;
library Unisim;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use Unisim.Vcomponents.all;

entity PTPTimeStamp is
    PORT (
      o_rcv_pkt_data : out std_logic_vector(7 downto 0);
      o_rcv_pkt_we   : out std_logic;
      o_rcv_pkt_addr : out std_logic_vector(10 downto 0);
      i_rcv_pkt_clk  : in  std_logic;
      i_rcv_pkt_data : in  std_logic_vector(7 downto 0);
      i_rcv_pkt_we   : in  std_logic;
      i_rcv_pkt_addr : in  std_logic_vector(10 downto 0);
      
      o_xmit_pkt_data : out std_logic_vector(7 downto 0);
      o_xmit_nib_cnt  : out std_logic_vector(12 downto 0);
      i_xmit_pkt_clk  : in  std_logic;
      i_xmit_pkt_data : in  std_logic_vector(7 downto 0);
      i_xmit_nib_cnt  : in  std_logic_vector(12 downto 0);
        
      i_clock_secs    : in std_logic_vector(31 downto 0);
      i_clock_nsecs   : in std_logic_vector(31 downto 0);
      i_clock_clk     : in std_logic
    );
end entity PTPTimeStamp;
    
architecture RTL of PTPTimeStamp is

signal tx_clock_secs, tx_clock_nsecs : std_logic_vector(31 downto 0) := (others=>'0');
signal rx_clock_secs, rx_clock_nsecs : std_logic_vector(31 downto 0) := (others=>'0');
signal latch_tx_clock, latch_rx_clock : std_logic;
signal latch_tx_clock_meta, latch_rx_clock_meta : std_logic;
signal latch_tx_clock_r1, latch_rx_clock_r1 : std_logic;
signal latch_tx_clock_r2, latch_rx_clock_r2 : std_logic;
signal tx_byte_count : std_logic_vector(10 downto 0);
signal tx_output_byte : std_logic_vector(7 downto 0);
signal rx_output_byte : std_logic_vector(7 downto 0);

-- states used for PtP packet editing...    
type FILTER_STATES is (
      WAIT_MAC,           -- wait for the MAC address to be presented
      QUALILFY_MAC_UDP,   -- check MAC for PTP Multicast address, and UDP protocol type
      ZERO_UDP_CHECKSUM,  -- zero out the UDP checksum (cause we're changing the data)
      QUALIFY_EVENT_TYPE, -- make sure that the event type is correct (SYNC or DELAY_REQ)
      REPLACE_TIMEDATA);  -- replace the data with the latched timestamp info
      
signal txf_sm : FILTER_STATES := WAIT_MAC;
signal rxf_sm : FILTER_STATES := WAIT_MAC;

begin

o_rcv_pkt_data  <= rx_output_byte;
o_xmit_pkt_data <= tx_output_byte;

--@ This process crosses from the ethernet transmit or receive
--  clock domains to the clock1305 domain (typically the EMIF clock)
--  in order to latch the most recent clock value for packet
--  stuffing.  Note:  for place and route, you may need to open 
--  the timing requirements from the rx/tx_clock_*secs variables
--  to the rx/tx_output_byte variables (it should be stable for 
--  several clock cycles in this design).
--/
latch_clocks : process(i_clock_clk)
begin
    if rising_edge(i_clock_clk) then
        latch_tx_clock_meta <= latch_tx_clock;
        latch_tx_clock_r1   <= latch_tx_clock_meta;
        latch_tx_clock_r2   <= latch_tx_clock_r1;
        latch_rx_clock_meta <= latch_rx_clock;
        latch_rx_clock_r1   <= latch_rx_clock_meta;
        latch_rx_clock_r2   <= latch_rx_clock_r1;
        
        if latch_rx_clock_r1 /= latch_rx_clock_r2 then
            rx_clock_secs <= i_clock_secs;
            rx_clock_nsecs <= i_clock_nsecs;
        end if;

        if latch_tx_clock_r1 /= latch_tx_clock_r2 then
            tx_clock_secs <= i_clock_secs;
            tx_clock_nsecs <= i_clock_nsecs;
        end if;

    end if;
end process latch_clocks;

--@ This process filters/edits outbound multicast PtP
--  packets to update the transmit timestamp.
--/
tx_filter : process(i_xmit_pkt_clk)
begin
    if rising_edge(i_xmit_pkt_clk) then
        o_xmit_nib_cnt  <= i_xmit_nib_cnt;
        tx_output_byte  <= i_xmit_pkt_data;
        tx_byte_count <= i_xmit_nib_cnt(11 downto 1);
        -- data is transmitted on every clock cycle (when packet is active)
        case txf_sm is
            when WAIT_MAC =>
                -- tx data counter starts negative to include pre-amble, so wait
                -- until rollover condition is detected...
                if tx_byte_count=CONV_STD_LOGIC_VECTOR(2**11-1,11) then
                    txf_sm <= QUALILFY_MAC_UDP;
                end if;
            when QUALILFY_MAC_UDP =>
                case tx_byte_count(7 downto 0) is
                    when x"00" =>
                        if i_xmit_pkt_data(7 downto 0) /= x"01" then
                            txf_sm <= WAIT_MAC;
                        end if;
                    when x"01" =>
                        if i_xmit_pkt_data(7 downto 0) /= x"00" then
                            txf_sm <= WAIT_MAC;
                        end if;
                    when x"02" =>
                        if i_xmit_pkt_data(7 downto 0) /= x"5E" then
                            txf_sm <= WAIT_MAC;
                        end if;
                    when x"03" =>
                        if i_xmit_pkt_data(7 downto 0) /= x"00" then
                            txf_sm <= WAIT_MAC;
                        end if;
                    when x"04" =>
                        if i_xmit_pkt_data(7 downto 0) /= x"01" then
                            txf_sm <= WAIT_MAC;
                        end if;
                    when x"05" =>
                        if i_xmit_pkt_data(7 downto 0) /= x"81" then
                            txf_sm <= WAIT_MAC;
                        end if;
                    when x"17" =>
                        if i_xmit_pkt_data(7 downto 0) /= x"11" then
                            txf_sm <= WAIT_MAC;
                        else
                            txf_sm <= ZERO_UDP_CHECKSUM;
                        end if;
                    when others => NULL;
                end case;
            when ZERO_UDP_CHECKSUM =>
                if tx_byte_count(7 downto 0) = x"28" or tx_byte_count(7 downto 0) = x"29" then
                    tx_output_byte <= x"00";
                elsif tx_byte_count(7 downto 0) = x"2A" then
                    txf_sm <= QUALIFY_EVENT_TYPE;
                end if;
            when QUALIFY_EVENT_TYPE =>
                case tx_byte_count(7 downto 0) is
                    when x"3E" =>
                        if i_xmit_pkt_data /= x"01" then
                            txf_sm <= WAIT_MAC;
                        end if;
                    when x"4A" =>
                        if i_xmit_pkt_data(7 downto 1) /= "0000000" then
                            txf_sm <= WAIT_MAC;
                        else
                            latch_tx_clock <= not latch_tx_clock;
                            txf_sm <= REPLACE_TIMEDATA;
                        end if;
                    when others => NULL;
                end case;
            when REPLACE_TIMEDATA =>
                case tx_byte_count(7 downto 0) is
                    when x"4E" =>
                       tx_output_byte <= x"01";
                    when x"52" =>
                       tx_output_byte <= tx_clock_secs(31 downto 24);
                    when x"53" =>
                       tx_output_byte <= tx_clock_secs(23 downto 16);
                    when x"54" =>
                       tx_output_byte <= tx_clock_secs(15 downto 8);
                    when x"55" =>
                       tx_output_byte <= tx_clock_secs(7 downto 0);
                    when x"56" =>
                       tx_output_byte <= tx_clock_nsecs(31 downto 24);
                    when x"57" =>
                       tx_output_byte <= tx_clock_nsecs(23 downto 16);
                    when x"58" =>
                       tx_output_byte <= tx_clock_nsecs(15 downto 8);
                    when x"59" =>
                       tx_output_byte <= tx_clock_nsecs(7 downto 0);
                    when x"5A" =>
                       txf_sm <= WAIT_MAC;
                    when others => NULL;
                end case;
            when OTHERS =>
                txf_sm <= WAIT_MAC;
        end case;
    end if;
end process tx_filter;

--@ This process filters/edits inbound multicast PtP
--  packets to update the received timestamp.
--/
rx_filter : process(i_rcv_pkt_clk)
begin
    if rising_edge(i_rcv_pkt_clk) then
        o_rcv_pkt_we    <= i_rcv_pkt_we;
        o_rcv_pkt_addr  <= i_rcv_pkt_addr;
        rx_output_byte  <= i_rcv_pkt_data;
        -- only look at data when it's valid
        if i_rcv_pkt_we = '1' then
            case rxf_sm is
                when WAIT_MAC =>
                    if i_rcv_pkt_addr=CONV_STD_LOGIC_VECTOR(0,11) and
                       i_rcv_pkt_data=x"01" then
                        rxf_sm <= QUALILFY_MAC_UDP;
                    end if;
                when QUALILFY_MAC_UDP =>
                    case i_rcv_pkt_addr(7 downto 0) is
                        when x"01" =>
                            if i_rcv_pkt_data /= x"00" then
                                rxf_sm <= WAIT_MAC;
                            end if;
                        when x"02" =>
                            if i_rcv_pkt_data /= x"5E" then
                                rxf_sm <= WAIT_MAC;
                            end if;
                        when x"03" =>
                            if i_rcv_pkt_data /= x"00" then
                                rxf_sm <= WAIT_MAC;
                            end if;
                        when x"04" =>
                            if i_rcv_pkt_data /= x"01" then
                                rxf_sm <= WAIT_MAC;
                            end if;
                        when x"05" =>
                            if i_rcv_pkt_data /= x"81" then
                                rxf_sm <= WAIT_MAC;
                            end if;
                        when x"17" =>
                            if i_rcv_pkt_data /= x"11" then
                                rxf_sm <= WAIT_MAC;
                            else
                                rxf_sm <= ZERO_UDP_CHECKSUM;
                            end if;
                        when others => NULL;
                    end case;
                when ZERO_UDP_CHECKSUM =>
                    if i_rcv_pkt_addr(7 downto 0) = x"28" or i_rcv_pkt_addr(7 downto 0) = x"29" then
                        rx_output_byte <= x"00";
                    elsif i_rcv_pkt_addr(7 downto 0) = x"2A" then
                        rxf_sm <= QUALIFY_EVENT_TYPE;
                    end if;
                when QUALIFY_EVENT_TYPE =>
                    case i_rcv_pkt_addr(7 downto 0) is
                        when x"3E" =>
                            if i_rcv_pkt_data /= x"01" then
                                rxf_sm <= WAIT_MAC;
                            end if;
                        when x"4A" =>
                            if i_rcv_pkt_data(7 downto 1) /= "0000000" then
                                rxf_sm <= WAIT_MAC;
                            else
                                latch_rx_clock <= not latch_rx_clock;
                                rxf_sm <= REPLACE_TIMEDATA;
                            end if;
                        when others => NULL;
                    end case;
                when REPLACE_TIMEDATA =>
                    case i_rcv_pkt_addr(7 downto 0) is
                        when x"4F" =>
                           rx_output_byte <= x"01";
                        when x"9E" =>
                           rx_output_byte <= rx_clock_secs(31 downto 24);
                        when x"9F" =>
                           rx_output_byte <= rx_clock_secs(23 downto 16);
                        when x"A0" =>
                           rx_output_byte <= rx_clock_secs(15 downto 8);
                        when x"A1" =>
                           rx_output_byte <= rx_clock_secs(7 downto 0);
                        when x"A2" =>
                           rx_output_byte <= rx_clock_nsecs(31 downto 24);
                        when x"A3" =>
                           rx_output_byte <= rx_clock_nsecs(23 downto 16);
                        when x"A4" =>
                           rx_output_byte <= rx_clock_nsecs(15 downto 8);
                        when x"A5" =>
                           rx_output_byte <= rx_clock_nsecs(7 downto 0);
                           rxf_sm <= WAIT_MAC;
                        when others => NULL;
                    end case;
                when OTHERS =>
                    rxf_sm <= WAIT_MAC;
            end case;
        end if;
    end if;
end process rx_filter;
    
    
end RTL;