--- Title: base_module.vhd
--- Description: 
---
---     o  0
---     | /       Copyright (c) 2005-2007
---    (CL)---o   Critical Link, LLC
---      \
---       O
---
--- Company: Critical Link, LLC.
--- Date: 8/23/2007
--- Version: 1.07
--- Revisions: 
---   1.00 - Initial release.
---   1.05 - 09/18/2006 - Added individual IRQ source masking capability.
---                       Also added a version register for the core itself (@ offset 0x18).
---   1.06 - 11/13/2006 - Added scratch pad RAM (16 x 32-bit) @ offsets 0x40 thru 0x7f.
---                       Added a bank-zero clearing input port (for system resets).
---                       Added a watchdog timer.
---                       Changed bank_sel & sba_clk signal to tri-state enables.
---   1.07 - 08/23/2007 - Added support for MityDSP-Pro.
---                       Changed multiple generics to single board-type generic.
---   1.08 - 05/06/2008 - Add support for proper EMIF DCM reset logic.

library WORK;
library IEEE;
library UNISIM;
use WORK.MITYDSP_PKG.ALL;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use UNISIM.VCOMPONENTS.ALL;

entity base_module is
   generic (
      CONFIG : string := "UNKNOWN" -- "MityDSP" | "MityDSP-XM" | "MityDSP-Pro"
      );
   port (
      emif_clk        : in std_logic;
      i_cs            : in std_logic;
      i_ID            : in std_logic_vector(7 downto 0);    -- assigned Application ID number, 0xFF if unassigned
      i_version_major : in std_logic_vector(3 downto 0);    -- major version number 1-15
      i_version_minor : in std_logic_vector(3 downto 0);    -- minor version number 0-15
      i_year          : in std_logic_vector(4 downto 0);    -- year since 2000
      i_month         : in std_logic_vector(3 downto 0);    -- month (1-12)
      i_day           : in std_logic_vector(4 downto 0);    -- day (1-32)
      i_ABus          : in std_logic_vector(4 downto 0);
      i_DBus          : in std_logic_vector(31 downto 0);
      o_DBus          : out std_logic_vector(31 downto 0);
      i_wr_en         : in std_logic;
      i_rd_en         : in std_logic;
      
      i_clk25mhz      : in  std_logic; -- 25 Mhz input clock domain (BUFG desirable, but should also work with local clock)
      o_clken100ms    : out std_logic; -- free-running 100ms period clock-enable in the 25mhz domain
      o_clken4ms      : out std_logic; -- free-running 4 ms period clock-enable in the 25mhz domain
      o_wd_nmi        : out std_logic; -- watchdog timer non-maskable interrupt output (active-high)
      o_wd_rst        : out std_logic; -- watchdog timer dsp reset (active-high)
      
      o_emif_dcm_reset  : out std_logic;                     -- hook to EMIF DCM reset line (not valid if i_clk25mhz is not connected)
      i_emif_dcm_status : in  std_logic_vector(2 downto 0);  -- hook to EMIF DCM status lines (not used if i_clk25mhz is not connected)
      i_emif_dcm_lock   : in  std_logic;                     -- hook to EMIF DCM lock line  (not used if i_clk25mhz is not connected)

      -- these signals are valid only for CONFIG="MityDSP" or CONFIG="MityDSP-XM"
      i_irq_map_4x8   : in  bus8_vector(7 downto 4) := (others=>(others=>'0')); -- ext_irq4,5,6,7
      o_irq_output_4  : out std_logic_vector(7 downto 4);                       -- ext_irq4,5,6,7

      -- these signals are valid only for CONFIG="MityDSP"
      t_bank_sel_n    : out std_logic; -- need inverter / tri-state buffer at top level; 0=HiZ, 1=GND
      o_led_enable    : out std_logic; -- need inverter at top level
      i_bank_zero_clr : in  std_logic := '0'; -- flash bank zero asynchronous clear (active-high)

      -- these signals are valid only for CONFIG="MityDSP-XM"
      o_sba_led       : out std_logic := '1'; -- serial-bank-address data / user LED (FPGA_INIT_B/LED/SBA)
                                              -- direct connection at top level
      t_sba_clk       : out std_logic := '1'; -- serial-bank-address clock
                                              -- need tri-state buffer at top level; 0=GND, 1=HiZ
      
      -- these signals are valid only for CONFIG="MityDSP-Pro"
      i_irq_map_2x16  : in  bus16_vector(5 downto 4) := (others=>(others=>'0'));     -- DSP_INT4/5
      o_irq_output_2  : out std_logic_vector(5 downto 4); -- DSP_INT4/5
      o_sba_data      : out std_logic; -- direct connection at top-level
      o_sba_clk       : out std_logic; -- direct connection at top-level
      o_led_n         : out std_logic_vector(1 downto 0); -- active-low LED outputs - direct connection at top-level
      i_config        : in  std_logic_vector(2 downto 0) := "000"
      );
end base_module;

architecture rtl of base_module is

component core_version
   port (
      clk           : in std_logic;                       -- system clock
      rd            : in std_logic;                       -- read enable
      ID            : in std_logic_vector(7 downto 0);    -- assigned ID number, 0xFF if unassigned
      version_major : in std_logic_vector(3 downto 0);    -- major version number 1-15
      version_minor : in std_logic_vector(3 downto 0);    -- minor version number 0-15
      year          : in std_logic_vector(4 downto 0);    -- year since 2000
      month         : in std_logic_vector(3 downto 0);    -- month (1-12)
      day           : in std_logic_vector(4 downto 0);    -- day (1-32)
      ilevel        : in std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      ilevel_vld    : in std_logic := '0';                           -- interrupt level valid flag
      ivector       : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      ivector_vld   : in std_logic := '0';                           -- interrupt vector valid flag
      o_data        : out std_logic_vector(31 downto 0)
      );
end component;

constant CORE_APPLICATION_ID: std_logic_vector(7 downto 0) := CONV_STD_LOGIC_VECTOR( 0, 8);
constant CORE_VERSION_MAJOR:  std_logic_vector(3 downto 0) := CONV_STD_LOGIC_VECTOR( 1, 4);
constant CORE_VERSION_MINOR:  std_logic_vector(3 downto 0) := CONV_STD_LOGIC_VECTOR( 8, 4);
constant CORE_YEAR:           std_logic_vector(4 downto 0) := CONV_STD_LOGIC_VECTOR( 8, 5);
constant CORE_MONTH:          std_logic_vector(3 downto 0) := CONV_STD_LOGIC_VECTOR(05, 4);
constant CORE_DAY:            std_logic_vector(4 downto 0) := CONV_STD_LOGIC_VECTOR(06, 5);

type key_state_type is (K1, K2, K3);
signal key_state: key_state_type := K1;

constant KEY1 : std_logic_vector(15 downto 0) := x"5A5A";
constant KEY2 : std_logic_vector(15 downto 0) := x"A5A5";
constant KEY3 : std_logic_vector(15 downto 0) := x"5AA5";

constant IRQ_MASK_EN : integer := 31;

constant BANK_BITS_MAX : integer := 8;

signal bank_addr   : std_logic_vector(BANK_BITS_MAX-1 downto 0) := (others=>'0');
signal sba_shift   : std_logic_vector(BANK_BITS_MAX-1 downto 0) := (others=>'0');
signal sba_count   : std_logic_vector(7 downto 0) := (others=>'0');
signal sba_strobe  : std_logic := '0';
signal sba_busy    : std_logic := '0';
signal sba_data    : std_logic;
signal sba_clk     : std_logic;

signal reset       : std_logic;
signal irq_enable  : std_logic_vector(7 downto 4) := (others=>'0');
signal led_enable  : std_logic_vector(1 downto 0) := (others=>'0');

signal fpga_version_reg : std_logic_vector(31 downto 0) := (others=>'0');
signal base_version_reg : std_logic_vector(31 downto 0) := (others=>'0');

signal irq_mask    : std_logic_vector(31 downto 0) := (others=>'0');
signal masked_irqs : std_logic_vector(31 downto 0) := (others=>'0');

signal scratch_ram : bus32_vector(0 to 15) := (others=>(others=>'0'));
attribute ram_style : string;
attribute ram_style of scratch_ram: signal is "distributed";

signal div10_1: std_logic_vector(1 to 10) := "1000000000";
signal div10_2: std_logic_vector(1 to 10) := "1000000000";
signal div10_3: std_logic_vector(1 to 10) := "1000000000";
signal div10_4: std_logic_vector(1 to 10) := "1000000000";
signal div10_5: std_logic_vector(1 to 10) := "1000000000";
signal div5_1:  std_logic_vector(1 to 5)  := "10000";
signal div5_2:  std_logic_vector(1 to 5)  := "10000";
signal div10_1_r: std_logic;
signal div10_2_r: std_logic;
signal div10_3_r: std_logic;
signal div10_4_r: std_logic;
signal div10_5_r: std_logic;
signal div5_1_r:  std_logic;
signal div5_2_r:  std_logic;
signal clken100ms: std_logic;
signal clken4ms: std_logic;

signal wd_kick : std_logic;
signal wd_kick_r1 : std_logic;
signal wd_kick_r2 : std_logic;
signal wd_nmi : std_logic := '0';
signal wd_rst : std_logic := '0';
signal wd_rst_r : std_logic;
signal wd_rst_r1 : std_logic;
signal wd_rst_r2 : std_logic;
signal wd_nmi_en : std_logic := '0';
signal wd_rst_en : std_logic := '0';
signal wd_nmi_clr : std_logic;
signal wd_nmi_clr_r1 : std_logic;
signal wd_nmi_clr_r2 : std_logic;
signal wd_rst_clr : std_logic;
signal wd_rst_clr_r1 : std_logic;
signal wd_rst_clr_r2 : std_logic;
signal wd_timeout : std_logic_vector(15 downto 0);
signal wd_counter : std_logic_vector(15 downto 0);
signal dcm_reset_shift: std_logic_vector(15 downto 0) := "0111111111111111";
signal dcm_lock_r1: std_logic;
signal dcm_lock_r2: std_logic;

begin -- architecture: rtl

assert CONFIG="MityDSP" or CONFIG="MityDSP-XM" or CONFIG="MityDSP-Pro"
report "CONFIG generic must be MityDSP, MityDSP-XM or MityDSP-Pro."
severity FAILURE;

roc_inst: roc port map( o => reset );

fpga_version : core_version
   port map(
      clk           => emif_clk,
      rd            => '0',
      ID            => i_ID,
      version_major => i_version_major,
      version_minor => i_version_minor,
      year          => i_year,
      month         => i_month,
      day           => i_day,
      o_data        => fpga_version_reg
      );

base_version : core_version
   port map(
      clk           => emif_clk,
      rd            => '0',
      ID            => CORE_APPLICATION_ID,
      version_major => CORE_VERSION_MAJOR,
      version_minor => CORE_VERSION_MINOR,
      year          => CORE_YEAR,
      month         => CORE_MONTH,
      day           => CORE_DAY,
      o_data        => base_version_reg
      );

gen_MityDSPandXM_IRQS: if CONFIG="MityDSP" or CONFIG="MityDSP-XM" generate
   masked_irqs( 7 downto  0) <= irq_mask( 7 downto  0) and i_irq_map_4x8(4);
   masked_irqs(15 downto  8) <= irq_mask(15 downto  8) and i_irq_map_4x8(5);
   masked_irqs(23 downto 16) <= irq_mask(23 downto 16) and i_irq_map_4x8(6);
   masked_irqs(31 downto 24) <= irq_mask(31 downto 24) and i_irq_map_4x8(7);
   o_irq_output_4(4) <= '1' when irq_enable(4)='1' and masked_irqs( 7 downto  0) /= x"00" else '0';
   o_irq_output_4(5) <= '1' when irq_enable(5)='1' and masked_irqs(15 downto  8) /= x"00" else '0';
   o_irq_output_4(6) <= '1' when irq_enable(6)='1' and masked_irqs(23 downto 16) /= x"00" else '0';
   o_irq_output_4(7) <= '1' when irq_enable(7)='1' and masked_irqs(31 downto 24) /= x"00" else '0';
   o_irq_output_2 <= "XX";
end generate gen_MityDSPandXM_IRQS;

gen_MityDSPPro_IRQS: if CONFIG="MityDSP-Pro" generate
   masked_irqs(15 downto  0) <= irq_mask(15 downto  0) and i_irq_map_2x16(4);
   masked_irqs(31 downto 16) <= irq_mask(31 downto 16) and i_irq_map_2x16(5);
   o_irq_output_2(4) <= '1' when irq_enable(4)='1' and masked_irqs(15 downto  0) /= x"0000" else '0';
   o_irq_output_2(5) <= '1' when irq_enable(5)='1' and masked_irqs(31 downto 16) /= x"0000" else '0';
   o_irq_output_4 <= "XXXX";
end generate gen_MityDSPPro_IRQS;

reg_read : process (emif_clk)
begin
   if emif_clk'event and emif_clk='1' then
      if i_cs='0' then
         o_DBus <= (others=>'0');
      else
         case i_ABus is
            when "00000" =>
               o_DBus <= fpga_version_reg;
            when "00001" =>
               o_DBus <= sba_busy & "000" & x"000" & CONV_STD_LOGIC_VECTOR(0, (16-BANK_BITS_MAX)) & bank_addr;
            when "00010" =>
               o_DBus <= x"0000000" & irq_enable;
            when "00011" =>
               o_DBus <= x"00000" & '0' & i_config & "000000" & led_enable;
            when "00100" =>
               o_DBus <= masked_irqs;
            when "00101" =>
               o_DBus <= irq_mask;
            when "00110" =>
               o_DBus <= base_version_reg;
            when "00111" =>
               o_DBus <= wd_nmi_en & wd_rst_en & wd_nmi & wd_rst & x"000" & wd_timeout;
            when "1----" =>
               o_DBus <= scratch_ram(CONV_INTEGER(i_ABus(3 downto 0)));
            when others =>
               o_DBus <= (others=>'0');
         end case;
      end if;
   end if;
end process reg_read;

reg_write_common : process (emif_clk)
begin
   if emif_clk'event and emif_clk='1' then
      if i_cs='1' and i_wr_en='1' then
         case i_ABus is
            when "00111" =>
               wd_kick <= not wd_kick;
               wd_nmi_en <= i_DBus(31);
               wd_rst_en <= i_DBus(30);
               if i_DBus(29)='1' and wd_nmi='1' then
                  wd_nmi_clr <= not wd_nmi_clr; -- clock domain cross toggle signal
               end if;
               if i_DBus(28)='1' and wd_rst='1' then
                  wd_rst_clr <= not wd_rst_clr; -- clock domain cross toggle signal
               end if;
               wd_timeout <= i_DBus(15 downto 0);
            when "1----" =>
               scratch_ram(CONV_INTEGER(i_ABus(3 downto 0))) <= i_DBus;
            when others =>
               null;
         end case;
      else -- not writing
         wd_rst_r1 <= wd_rst_r;
         wd_rst_r2 <= wd_rst_r1;
         
         -- detect watchdog reset output flag rising edge
         if wd_rst_r1='1' and wd_rst_r2='0' then
            wd_nmi_en <= '0'; -- disable watchdog NMI
            wd_rst_en <= '0'; -- disable watchdog RST
         end if;
      end if;
   end if;
end process reg_write_common;

gen_MityDSPandXM_reg_write: if CONFIG="MityDSP" or CONFIG="MityDSP-XM" generate
   reg_write_MityDSPandXM : process (emif_clk)
   begin
      if emif_clk'event and emif_clk='1' then
         if i_cs='1' and i_wr_en='1' then
            case i_ABus is
               when "00010" =>
                  irq_enable <= i_DBus(3 downto 0);
                  -- Pre vector enable detection logic
                  if i_DBus(IRQ_MASK_EN) = '0' then
                     irq_mask <= (others=>'1');
                  end if;
               when "00011" =>
                  led_enable(0) <= i_DBus(0);
               when "00101" =>
                  irq_mask <= i_DBus;
               when others =>
                  null;
            end case;
         end if;
      end if;
   end process reg_write_MityDSPandXM;
end generate gen_MityDSPandXM_reg_write;

gen_MityDSPPro_reg_write: if CONFIG="MityDSP-Pro" generate
   reg_write_MityDSPPro : process (emif_clk)
   begin
      if emif_clk'event and emif_clk='1' then
         if i_cs='1' and i_wr_en='1' then
            case i_ABus is
               when "00010" =>
                  irq_enable(5 downto 4) <= i_DBus(1 downto 0);
                  -- Pre vector enable detection logic
                  if i_DBus(IRQ_MASK_EN) = '0' then
                     irq_mask <= (others=>'1');
                  end if;
               when "00011" =>
                  led_enable <= i_DBus(1 downto 0);
               when "00101" =>
                  irq_mask <= i_DBus;
               when others =>
                  null;
            end case;
         end if;
      end if;
   end process reg_write_MityDSPPro;
end generate gen_MityDSPPro_reg_write;

gen_MityDSP_bank_addr: if CONFIG="MityDSP" generate
   constant BANK_BITS: integer range 1 to BANK_BITS_MAX := 1;
begin -- generate
   bank_addr_write_MityDSP: process (emif_clk, reset, i_bank_zero_clr)
   begin
      if reset='1' then
         key_state <= K1; -- need this reset for proper state machine generation
      elsif i_bank_zero_clr='1' then
         bank_addr <= (others=>'0'); -- asynch clear of bank_addr
      elsif emif_clk'event and emif_clk='1' then
         if i_cs='1' and i_wr_en='1' and i_ABus="00001" then
            if i_DBus((BANK_BITS-1) downto 0) = CONV_STD_LOGIC_VECTOR(0, BANK_BITS) then
               case key_state is
                  when K1 =>
                     if i_DBus(31 downto 16) = KEY1 then
                        key_state <= K2;
                     else
                        key_state <= K1;
                     end if;
                  when K2 =>
                     if i_DBus(31 downto 16) = KEY2 then
                        key_state <= K3;
                     else
                        key_state <= K1;
                     end if;
                  when K3 =>
                     if i_DBus(31 downto 16) = KEY3 then
                        key_state <= K1;
                        bank_addr((BANK_BITS-1) downto 0) <= CONV_STD_LOGIC_VECTOR(0, BANK_BITS);
                     else
                        key_state <= K1;
                     end if;
               end case;
            else
               bank_addr((BANK_BITS-1) downto 0) <= i_DBus((BANK_BITS-1) downto 0);
               key_state <= K1;
            end if;
         end if;
      end if;
   end process bank_addr_write_MityDSP;
end generate gen_MityDSP_bank_addr;

gen_MityDSPXM_bank_addr: if CONFIG="MityDSP-XM" generate
   constant BANK_BITS: integer range 1 to BANK_BITS_MAX := 4;
begin -- generate
   bank_addr_write_MityDSPXM: process (emif_clk, reset)
   begin
      if reset='1' then
         key_state <= K1; -- need this reset for proper state machine generation
      elsif emif_clk'event and emif_clk='1' then
         if i_cs='1' and i_wr_en='1' and i_ABus="00001" then
            if i_DBus((BANK_BITS-1) downto 0) = CONV_STD_LOGIC_VECTOR(0, BANK_BITS) then
               case key_state is
                  when K1 =>
                     if i_DBus(31 downto 16) = KEY1 then
                        key_state <= K2;
                     else
                        key_state <= K1;
                     end if;
                  when K2 =>
                     if i_DBus(31 downto 16) = KEY2 then
                        key_state <= K3;
                     else
                        key_state <= K1;
                     end if;
                  when K3 =>
                     if i_DBus(31 downto 16) = KEY3 then
                        key_state <= K1;
                        bank_addr((BANK_BITS-1) downto 0) <= CONV_STD_LOGIC_VECTOR(0, BANK_BITS);
                        sba_strobe <= '1';
                     else
                        key_state <= K1;
                     end if;
               end case;
            else
               bank_addr((BANK_BITS-1) downto 0) <= i_DBus((BANK_BITS-1) downto 0);
               sba_strobe <= '1';
               key_state <= K1;
            end if;
         else
            sba_strobe <= '0';
         end if;
      end if;
   end process bank_addr_write_MityDSPXM;
end generate gen_MityDSPXM_bank_addr;

gen_MityDSPPro_bank_addr: if CONFIG="MityDSP-Pro" generate
   constant BANK_BITS: integer range 1 to BANK_BITS_MAX := 2;
begin -- generate
   bank_addr_write_MityDSPPro: process (emif_clk, reset)
   begin
      if reset='1' then
         key_state <= K1; -- need this reset for proper state machine generation
      elsif emif_clk'event and emif_clk='1' then
         if i_cs='1' and i_wr_en='1' and i_ABus="00001" then
            if i_DBus((BANK_BITS-1) downto 0) = CONV_STD_LOGIC_VECTOR(0, BANK_BITS) then
               case key_state is
                  when K1 =>
                     if i_DBus(31 downto 16) = KEY1 then
                        key_state <= K2;
                     else
                        key_state <= K1;
                     end if;
                  when K2 =>
                     if i_DBus(31 downto 16) = KEY2 then
                        key_state <= K3;
                     else
                        key_state <= K1;
                     end if;
                  when K3 =>
                     if i_DBus(31 downto 16) = KEY3 then
                        key_state <= K1;
                        bank_addr((BANK_BITS-1) downto 0) <= CONV_STD_LOGIC_VECTOR(0, BANK_BITS);
                        sba_strobe <= '1';
                     else
                        key_state <= K1;
                     end if;
               end case;
            else
               bank_addr((BANK_BITS-1) downto 0) <= i_DBus((BANK_BITS-1) downto 0);
               sba_strobe <= '1';
               key_state <= K1;
            end if;
         else
            sba_strobe <= '0';
         end if;
      end if;
   end process bank_addr_write_MityDSPPro;
end generate gen_MityDSPPro_bank_addr;

gen_sba_process: if CONFIG="MityDSP-XM" or CONFIG="MityDSP-Pro" generate
   -- Serial-Bank-Addressing
   sba : process (emif_clk, reset)
   begin
      if reset='1' then
         sba_data <= '1';
         sba_clk <= '1';
      elsif emif_clk'event and emif_clk='1' then
         if sba_strobe='1' and sba_busy='0' then
            sba_busy <= '1';
            sba_shift <= bank_addr;
            sba_count <= CONV_STD_LOGIC_VECTOR((BANK_BITS_MAX*2), 8);
            sba_clk <= '1';
         elsif sba_count = CONV_STD_LOGIC_VECTOR(0, 8) then
            sba_busy <= '0';
            sba_clk <= '1';
         else -- sba_count > 0
            sba_busy <= '1';
            sba_count <= sba_count - '1';
            if sba_count(0) = '0' then
               sba_shift <= sba_shift((BANK_BITS_MAX-2) downto 0) & '0'; -- shift left
               sba_data <= sba_shift(BANK_BITS_MAX-1);
               sba_clk <= '0';
            else -- sba_count(0) = '1'
               sba_clk <= '1';
            end if;
         end if;
      end if;
   end process sba;
end generate gen_sba_process;

gen_MityDSP_banksel_and_led: if CONFIG="MityDSP" generate
   t_bank_sel_n <= bank_addr(0); -- need inverter / tri-state buffer at top-level
   o_led_enable <= led_enable(0); -- need inverter at top-level
   o_sba_led <= 'X';
   t_sba_clk <= 'X';
   o_sba_data <= 'X';
   o_sba_clk <= 'X';
   o_led_n <= "XX";
end generate gen_MityDSP_banksel_and_led;

gen_MityDSPXM_sba_and_led: if CONFIG="MityDSP-XM" generate
   t_bank_sel_n <= 'X';
   o_led_enable <= 'X';
   o_sba_led <= sba_data when sba_busy='1' else not led_enable(0);
   t_sba_clk <= sba_clk;
   o_sba_data <= 'X';
   o_sba_clk <= 'X';
   o_led_n <= "XX";
end generate gen_MityDSPXM_sba_and_led;

gen_MityDSPPro_sba_and_led: if CONFIG="MityDSP-Pro" generate
   t_bank_sel_n <= 'X';
   o_led_enable <= 'X';
   o_sba_led <= 'X';
   t_sba_clk <= 'X';
   o_sba_data <= sba_data;
   o_sba_clk <= sba_clk;
   o_led_n <= not led_enable;
end generate gen_MityDSPPro_sba_and_led;

gen_clken100ms: process (i_clk25mhz)
begin
   if i_clk25mhz'event and i_clk25mhz='1' then
      -- Stage 1: Divide by 10
      div10_1 <= div10_1(10) & div10_1(1 to 9);
      div10_1_r <= div10_1(10);
      
      -- Stage 2: Divide by 10
      if div10_1(10)='1' and div10_1_r='0' then
         div10_2 <= div10_2(10) & div10_2(1 to 9);
      end if;
      div10_2_r <= div10_2(10);
      
      -- Stage 3: Divide by 10
      if div10_2(10)='1' and div10_2_r='0' then
         div10_3 <= div10_3(10) & div10_3(1 to 9);
      end if;
      div10_3_r <= div10_3(10);
      
      -- Stage 4: Divide by 10
      if div10_3(10)='1' and div10_3_r='0' then
         div10_4 <= div10_4(10) & div10_4(1 to 9);
      end if;
      div10_4_r <= div10_4(10);
      
      -- Stage 5: Divide by 10
      if div10_4(10)='1' and div10_4_r='0' then
         div10_5 <= div10_5(10) & div10_5(1 to 9);
      end if;
      div10_5_r <= div10_5(10);
      
      -- Stage 6: Divide by 5
      if div10_5(10)='1' and div10_5_r='0' then
         div5_1 <= div5_1(5) & div5_1(1 to 4);
      end if;
      div5_1_r <= div5_1(5);
      
      -- Stage 7: Divide by 5
      if div5_1(5)='1' and div5_1_r='0' then
         div5_2 <= div5_2(5) & div5_2(1 to 4);
      end if;
      div5_2_r <= div5_2(5);
      
      -- Drive 4 ms output pulse
      if div10_5(10)='1' and div10_5_r='0' then
         clken4ms <= '1';
      else
         clken4ms <= '0';
      end if;

      -- Drive output pulse
      if div5_2(5)='1' and div5_2_r='0' then
         clken100ms <= '1';
      else
         clken100ms <= '0';
      end if;
   end if;
end process gen_clken100ms;

o_clken100ms <= clken100ms;
o_clken4ms   <= clken4ms;

watchdog_timer: process (i_clk25mhz)
begin
   if i_clk25mhz'event and i_clk25mhz='1' then
      wd_kick_r1 <= wd_kick;
      wd_kick_r2 <= wd_kick_r1;

      wd_nmi_clr_r1 <= wd_nmi_clr;
      wd_nmi_clr_r2 <= wd_nmi_clr_r1;

      wd_rst_clr_r1 <= wd_rst_clr;
      wd_rst_clr_r2 <= wd_rst_clr_r1;
      
      if wd_nmi_en='0' and wd_rst_en='0' then -- watchdog disabled
         wd_counter <= wd_timeout; -- reset counter
         wd_nmi <= '0'; -- reset NMI flag
         wd_rst <= '0'; -- reset RST flag
      elsif wd_kick_r1 /= wd_kick_r2 then -- watchdog kicked
         wd_counter <= wd_timeout; -- restart counter
         
         if wd_nmi_clr_r1 /= wd_nmi_clr_r2 then
            wd_nmi <= '0'; -- clear NMI flag
         end if;
         
         if wd_rst_clr_r1 /= wd_rst_clr_r2 then
            wd_rst <= '0'; -- clear RST flag
         end if;
      elsif wd_counter = x"0000" then
         wd_counter <= wd_timeout; -- restart counter

         if wd_nmi_en='1' then
            if wd_nmi='0' then
               wd_nmi <= '1'; -- set NMI flag
            elsif wd_rst_en='1' then -- if NMI hasn't been cleared and RST is enabled...
               wd_rst <= '1'; -- set RST flag
            end if;
         elsif wd_rst_en='1' then
            wd_rst <= '1'; -- set RST flag
         end if;
      else
         if clken100ms='1' then
            wd_counter <= wd_counter - '1'; -- count down in 100ms periods
         end if;
      end if;
      
      if clken100ms='1' then
         wd_rst_r <= wd_rst;
         -- detect setting of RST flag (rising edge)
         if wd_rst='1' and wd_rst_r='0' then
            o_wd_rst <= '1'; -- activate reset for 100ms
         else
            o_wd_rst <= '0';
         end if;
      end if;
   end if;
end process watchdog_timer;

o_wd_nmi <= wd_nmi;

dcm_reset_trigger : process(i_clk25mhz)
begin
   if i_clk25mhz'event and i_clk25mhz='1' then
      dcm_lock_r1 <= i_emif_dcm_lock;
      dcm_lock_r2 <= dcm_lock_r1;
      
      if (clken4ms='1' and dcm_lock_r2='0') or 
         (dcm_lock_r1='0' and dcm_lock_r2='1') or dcm_reset_shift(15)='1' then
         dcm_reset_shift <= dcm_reset_shift(14 downto 0) & dcm_reset_shift(15);
      end if;
   end if;    
end process dcm_reset_trigger;

o_emif_dcm_reset <= i_emif_dcm_status(1) or dcm_reset_shift(15);

end rtl;
