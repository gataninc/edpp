--- Title: MicroSD.vhd
--- Description: 
---
---     o  0
---     | /       Copyright (c) 2005, 2006
---    (CL)---o   Critical Link, LLC
---      \
---       O
---
--- Company: Critical Link, LLC.
--- Date: 11/14/2006
--- Version: 1.00
---
--- Revision History
---   1.0 - Baseline Template
---
library WORK;
library IEEE;
library UNISIM;
use WORK.MITYDSP_PKG.ALL;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use UNISIM.VCOMPONENTS.ALL;

entity MicroSD is
   Port ( 
      clk          : in  std_logic;
      i_ABus       : in  std_logic_vector(4 downto 0);
      i_DBus       : in  std_logic_vector(31 downto 0);
      o_DBus_SPI   : out std_logic_vector(31 downto 0);
      i_cs_SPI     : in  std_logic;	
      o_DBus_GPOUT : out std_logic_vector(31 downto 0);
      i_cs_GPOUT   : in  std_logic;	
      i_wr_en      : in  std_logic;
      i_rd_en      : in  std_logic;
      o_irq        : out std_logic;
      i_ilevel       : in    std_logic_vector(1 downto 0) := "01"; 
      i_ivector      : in    std_logic_vector(4 downto 0) := "00000";
      o_mmc_cs_n     : out   std_logic := '0';
      o_mmc_clk      : out   std_logic := '0';
      o_mmc_di       : out   std_logic := '0';
      i_mmc_do       : in    std_logic := '0'
    );
end MicroSD;

architecture rtl of MicroSD is

component spit
   Port 
   (  
      emif_clk        : in  std_logic;
      i_ABus          : in  std_logic_vector(4 downto 0);
      i_DBus          : in  std_logic_vector(31 downto 0);
      o_DBus          : out std_logic_vector(31 downto 0);
      i_wr_en         : in  std_logic;
      i_rd_en         : in  std_logic;
      i_cs            : in  std_logic;	
      o_irq           : out std_logic;
      i_ilevel        : in std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld    : in std_logic := '1';                           -- interrupt level valid flag
      i_ivector       : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld   : in std_logic := '1';                           -- interrupt vector valid flag
      i_clk           : in  std_logic;
      o_sclk          : out std_logic;                      -- SPI output clock
      o_sync          : out std_logic;                      -- SYNC output 
      o_mosi          : out std_logic;                      -- data output 
      i_miso          : in  std_logic                       -- data input 
    );                          
end component;						 

component gpout is
   generic (
      NUM_IO : integer := 2 
   );
   Port ( 
      clk            : in  std_logic;
      i_ABus         : in  std_logic_vector(4 downto 0);
      i_DBus         : in  std_logic_vector(31 downto 0);
      o_DBus         : out std_logic_vector(31 downto 0);
      i_wr_en        : in  std_logic;
      i_rd_en        : in  std_logic;
      i_cs           : in  std_logic;	
      i_ilevel       : in    std_logic_vector(1 downto 0) := "01";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld   : in    std_logic := '0';                           -- interrupt level valid flag
      i_ivector      : in    std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld  : in    std_logic := '0';                           -- interrupt vector valid flag
      o_port         : out std_logic_vector(NUM_IO-1 downto 0)
    );
end component;						 

signal fast_clk_select : std_logic := '0';
signal clk_raw         : std_logic := '0';
signal clk_400         : std_logic := '0';
signal clk_count       : std_logic_vector(7 downto 0):= (others=>'0');
signal mmc_clk         : std_logic := '0';
signal clk_hs          : std_logic := '0';

begin

-- process to generate either 400 khz clock 
-- or 20 MHz clock 
mmc_clk_gen: process(clk)
begin
    if clk'event and clk='1' then
       if clk_count = x"3E" then
          clk_count <= (others=>'0');
          clk_raw   <= not clk_raw;
       else
          clk_count <= clk_count+'1';
       end if; 
       clk_hs <= not clk_hs;      
    end if;
end process mmc_clk_gen;

-- hmm....
mmc_clk <= clk_hs when fast_clk_select='1' else clk_raw;

--bufg_clk400 : bufg port map (i => clk_raw, o => clk_400);
--bufgmux_spi : bufgmux port map ( O  => mmc_clk,
--                                 I0 => clk_400,
--                                 I1 => clk,
--                                 S  => fast_clk_select);
mmc_spi : spit
   Port map (
      emif_clk        => clk,
      i_ABus          => i_ABus,
      i_DBus          => i_DBus,
      o_DBus          => o_DBus_SPI,
      i_wr_en         => i_wr_en,
      i_rd_en         => i_rd_en,
      i_cs            => i_cs_SPI,
      o_irq           => o_irq,
      i_ilevel        => i_ilevel,
      i_ilevel_vld    => '1',              -- interrupt level valid flag
      i_ivector       => i_ivector,
      i_ivector_vld   => '1',              -- interrupt vector valid flag
      i_clk           => mmc_clk,
      o_sclk          => o_mmc_clk,        -- SPI clock
      o_sync          => open,             -- SYNC output line (chip select??)
      o_mosi          => o_mmc_di,         -- data output line
      i_miso          => i_mmc_do
    );

mmc_gpout: gpout 
   generic map (
      NUM_IO => 2 
   )
   Port Map ( 
      clk            => clk,
      i_ABus         => i_ABus,
      i_DBus         => i_DBus,
      o_DBus         => o_DBus_GPOUT,
      i_wr_en        => i_wr_en,
      i_rd_en        => i_rd_en,
      i_cs           => i_cs_GPOUT,	
      i_ilevel       => "00",
      i_ilevel_vld   => '0',
      i_ivector      => "00000",
      i_ivector_vld  => '0',
      o_port(0)      => o_mmc_cs_n,
      o_port(1)      => fast_clk_select
    );


end rtl;

