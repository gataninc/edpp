--- Title: EMIF_iface.vhd
--- Description: 
---
---     o  0
---     | /       Copyright (c) 2005-2009
---    (CL)---o   Critical Link, LLC
---      \
---       O
---
--- Company: Critical Link, LLC.
--- Date: 02/12/2009
--- Version: 1.02
--- Revisions: 1.00 Baseline
---            1.01 Added ports to support FPGA controlled DMA transactions to SDRAM 
---            1.02 Added support for MityDSP in addition to existing MityDSP-XM support

library WORK;
library IEEE;
library UNISIM;
use WORK.MITYDSP_PKG.ALL;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use UNISIM.VCOMPONENTS.ALL;

entity EMIF_iface is
    generic ( 
        DECODE_BITS       : integer range 1 to 9 := 5;
        INCLUDE_SDRAM_DMA : boolean := FALSE;
        NUM_CTRL_PORTS    : integer range 1 to 4 := 4; -- required if INCLUDE_DMA = true
        CONFIG            : string := "UNKNOWN"        -- "MityDSP" | "MityDSP-XM", required if INCLUDE_DMA = true
    );
    port ( emif_clk  : in std_logic;
           -- EMIF signals
           i_ce2_n   : in std_logic;
           i_ce3_n   : in std_logic;
           i_aoe_n   : in std_logic;
           i_are_n   : in std_logic;
           i_awe_n   : in std_logic;
           i_be_n    : in std_logic_vector(3 downto 0);
           i_ea      : in std_logic_vector(15 downto 2);
           i_ed      : in std_logic_vector(31 downto 0);
           o_ed      : out std_logic_vector(31 downto 0);
           t_ed      : out std_logic;
           o_hold_n  : out std_logic := '1';   
           i_holda_n : in std_logic;   
           i_bus_req : in std_logic;
           
           -- FPGA fabric signals
           be_r      : out std_logic_vector(3 downto 0);
           addr_r    : out std_logic_vector(4 downto 0);
           cs_r      : out std_logic_vector((2**DECODE_BITS)-1 downto 0);
           ce3_r     : out std_logic;
           aoe_r     : out std_logic;
           rd_r      : out std_logic;
           wr_r      : out std_logic;
           edi_r     : out std_logic_vector(31 downto 0);
           edo_ce2   : in std_logic_vector(31 downto 0);
           edo       : in bus32_vector((2**DECODE_BITS)-1 downto 0);

           ----------------------------------------------------------------
           -- The items below this line are only required if INCLUDE_DMA = true
           -- 
           o_ce_n    : out std_logic_vector(3 downto 0) := "0000";
           o_ras_n   : out std_logic := '1';       --AOE/SDRAS
           o_cas_n   : out std_logic := '1';       --ARE/SDCAS
           o_awe_n   : out std_logic := '1';       --AWE/SDWE
           o_be_n    : out std_logic_vector(3 downto 0) := "1111";            --DQM3:0
           o_ea      : out std_logic_vector(15 downto 2) := (others=>'0');    --BA1:0 | A11:0
           t_emif    : out std_logic := '1';         --Drive CEn, AOE, ARE, AWE, BEn, EAn           
            
           -- SDRAM DMA fabric signals (can leave disconnected if INCLUDE_DMA = false)
           -- DMA Request lines (one normal priority, one high priority)
           -- High priority requests mean "take the EMIF bus from the processor" by asserting hold
           -- Low priority requests mean "wait for the EMIF bus to be free" meaning DSP is not using bus
           i_dma_req          : in std_logic_vector(NUM_CTRL_PORTS-1 downto 0) := (others=>'0');
           i_dma_priority_req : in std_logic_vector(NUM_CTRL_PORTS-1 downto 0) := (others=>'0');
           i_dma_rwn          : in std_logic_vector(NUM_CTRL_PORTS-1 downto 0) := (others=>'0');

           -- For the Input Address only bottom SDRAM_BANK_BITS+SDRAM_ROW_BITS+SDRAM_COL_BITS used (in that order)
           -- this is a 32 bit aligned word address (shift by two bits to translate into DSP byte address)
           i_dma_addr        : in  bus32_vector(NUM_CTRL_PORTS-1 downto 0) := (others=>(others=>'0')); 
           i_dma_data        : in  bus32_vector(NUM_CTRL_PORTS-1 downto 0) := (others=>(others=>'0')); 
           -- number of 32 bit words - 1, max 512 at a time (5 uS stall @ 50 Mhz)
           -- Must be a minimum transfer of 4 words (value = 3)
           i_dma_count       : in  bus9_vector(NUM_CTRL_PORTS-1 downto 0)  := (others=>(others=>'0'));  -- number of 32 bit words, max 512 at a time (6 uS stall @ 50 Mhz)
           i_dma_be          : in  bus4_vector(NUM_CTRL_PORTS-1 downto 0)  := (others=>(others=>'1')); 
      
           o_dma_wr_stb      : out std_logic_vector(NUM_CTRL_PORTS-1 downto 0) := (others=>'0');
           o_dma_rd_stb      : out std_logic_vector(NUM_CTRL_PORTS-1 downto 0) := (others=>'0');
           o_dma_data        : out std_logic_vector(31 downto 0) := (others=>'0');
           o_dma_done        : out std_logic_vector(NUM_CTRL_PORTS-1 downto 0)  := (others=>'0')  -- deassert i_dma_*req on next clock after o_dma_done is high
           
    );
end EMIF_iface;

architecture rtl of EMIF_iface is

component EMIF_SDRAM_DMA
   generic (
      CAS_LATENCY       : integer range 2  to 3  := 2;
      T_RAS : std_logic_vector(1 downto 0) := "11";
      T_RC  : std_logic_vector(2 downto 0) := "100";
      T_RCD : std_logic_vector(1 downto 0) := "01";
      T_RP  : std_logic_vector(1 downto 0) := "01";
      T_RRD : std_logic_vector(1 downto 0) := "01";
      T_WR  : std_logic_vector(1 downto 0) := "01";
      SDRAM_COL_BITS    : integer range 7  to 9  := 9;
      SDRAM_ROW_BITS    : integer range 10 to 12 := 12;
      SDRAM_BANK_BITS   : integer range 1 to 3 := 2;
      NUM_CTRL_PORTS    : integer range 1  to 4  := 2;
      SDRAM_CHIP_SELECT : integer range 0 to 3 := 0
   );
   port (
      emif_clk : in std_logic; -- 50 Mhz EMIF clock

      -- EMIF BUS Interface
      o_ce_n         : out std_logic_vector(3 downto 0);
      o_ras_n        : out std_logic;       --AOE/SDRAS
      o_cas_n        : out std_logic;       --ARE/SDCAS
      o_awe_n        : out std_logic;       --AWE/SDWE
      o_be_n         : out std_logic_vector(3 downto 0);     --DQM3:0
      o_ea           : out std_logic_vector(15 downto 2);    --BA1:0 | A11:0
      t_emif         : out std_logic;         --Drive CEn, AOE, ARE, AWE, BEn, EAn

      o_ed           : out std_logic_vector(31 downto 0);
      i_ed           : in  std_logic_vector(31 downto 0);
      t_ed           : out std_logic;         --Drive EDn
      
      o_hold_n       : out std_logic;   --Request EMIF
      i_holda_n      : in std_logic;   --Acknowledge EMIF Request
      i_bus_req      : in std_logic;   --DSP Requests EMIF

      --DMA Data Interface to Other Cores

      -- DMA Request lines (one normal priority, one high priority)
      -- High priority requests mean "take the EMIF bus from the processor" by asserting hold
      -- Low priority requests mean "wait for the EMIF bus to be free" meaning DSP is not using bus
      i_dma_req          : in std_logic_vector(NUM_CTRL_PORTS-1 downto 0);
      i_dma_priority_req : in std_logic_vector(NUM_CTRL_PORTS-1 downto 0);
      i_dma_rwn          : in std_logic_vector(NUM_CTRL_PORTS-1 downto 0);

      -- For the Input Address only bottom SDRAM_BANK_BITS+SDRAM_ROW_BITS+SDRAM_COL_BITS used (in that order)
      -- this is a 32 bit aligned word address (shift by two bits to translate into DSP byte address)
      i_dma_addr        : in  bus32_vector(NUM_CTRL_PORTS-1 downto 0); 
      i_dma_data        : in  bus32_vector(NUM_CTRL_PORTS-1 downto 0);
      -- number of 32 bit words - 1, max 512 at a time (5 uS stall @ 50 Mhz)
      -- Must be a minimum transfer of 4 words (value = 3)
      i_dma_count       : in  bus9_vector(NUM_CTRL_PORTS-1 downto 0); 
      i_dma_be          : in  bus4_vector(NUM_CTRL_PORTS-1 downto 0);
      
      o_dma_wr_stb      : out std_logic_vector(NUM_CTRL_PORTS-1 downto 0);
      o_dma_rd_stb      : out std_logic_vector(NUM_CTRL_PORTS-1 downto 0);
      o_dma_data        : out std_logic_vector(31 downto 0);
      o_dma_done        : out std_logic_vector(NUM_CTRL_PORTS-1 downto 0)  -- deassert i_dma_*req on next clock after o_dma_done is high

   );
end component;

signal reset: std_logic;
signal be: std_logic_vector(3 downto 0) := (others=>'1');
signal ea: std_logic_vector(15 downto 2) := (others=>'0');
signal cs: std_logic_vector((2**DECODE_BITS)-1 downto 0);
signal ce3_n, ce3: std_logic := '1';
signal aoe_n, aoe: std_logic := '1';
signal are_n, are: std_logic := '1';
signal awe_n, awe: std_logic := '1';
signal are_dly, awe_dly: std_logic := '1';
signal rd, wr: std_logic;
signal edi: std_logic_vector(31 downto 0);
signal edo_ce3: std_logic_vector(31 downto 0) := (others=>'0');

-- signals supporting DMA engine
signal ce_n : std_logic_vector(3 downto 0) := "1111";
signal ras_n, cas_n : std_logic := '1';
signal be_n : std_logic_vector(3 downto 0) := "1111";
signal dma_ea : std_logic_vector(15 downto 2) := (others=>'0');
signal emif_t, dma_t_ed : std_logic := '1';
signal dma_ed : std_logic_vector(31 downto 0) := (others=>'0');
signal dma_awe_n : std_logic := '1';
signal ed_t : std_logic := '1';

begin -- architecture: rtl

rst_on_cfg: roc port map (O => reset);  -- Reset On Configuration

-- register input signals
process (emif_clk)
begin
   if emif_clk'event and emif_clk='1' then
      -- capture byte-enables and address bus
      be <= i_be_n;
      ea <= i_ea;
      -- capture control signals
      ce3_n <= i_ce3_n; -- These control signals are NOT inverted here
      aoe_n <= i_aoe_n; -- so that the registers can be packed into IOB's.
      are_n <= i_are_n;
      awe_n <= i_awe_n;
      -- one-clock delayed registers
      are_dly <= are_n;
      awe_dly <= awe_n;
      -- capture data bus
      edi <= i_ed;
   end if;
end process;

-- address decode
process (ce3_n, ea)
begin
   if ce3_n='0' then
      for count in 0 to (2**DECODE_BITS)-1 loop
         if ea(DECODE_BITS+6 downto 7) = conv_std_logic_vector(count, DECODE_BITS) then
            cs(count) <= '1';
         else
            cs(count) <= '0';
         end if;
      end loop;
   else
      cs <= (others=>'0');
   end if;
end process;

-- read/write strobes
rd <= not are_n and are_dly;
wr <= not awe_n and awe_dly;

-- register output signals
process (emif_clk, reset)
begin
   if reset='1' then
      be_r <= (others=>'1');
      addr_r <= (others=>'0');
      cs_r <= (others=>'0');
      ce3_r <= '0';
      aoe_r <= '0';
      rd_r <= '0';
      wr_r <= '0';
   elsif emif_clk'event and emif_clk='1' then
      be_r <= be;
      addr_r <= ea(6 downto 2);
      cs_r <= cs;
      ce3_r <= not ce3_n; -- register and invert to positive logic
      aoe_r <= not aoe_n; -- register and invert to positive logic
      rd_r <= rd;
      wr_r <= wr;
      edi_r <= edi;
   end if;
end process;

-- multiplex module data-out busses
process (emif_clk, edo, edo_ce2)
   variable ored : std_logic_vector(31 downto 0);
begin
   if emif_clk'event and emif_clk='1' then
      ored := edo(0);
      for count in 1 to (2**DECODE_BITS)-1 loop
         ored := ored or edo(count);
      end loop;
--    multiplex ce2 and ce3 busses and SDRAM output bus
      edo_ce3 <= ored or edo_ce2 or dma_ed;
   end if;
end process;

o_ed <= edo_ce3;

-- drive tri-state enable line
t_ed <= dma_t_ed and (i_aoe_n or (i_ce2_n and i_ce3_n));

sdram_dma_std : if INCLUDE_SDRAM_DMA = true and 
               CONFIG = "MityDSP" generate
begin

-- latch output controls to align with data
latch_sigs : process(emif_clk)
begin
    if rising_edge(emif_clk) then
        o_ce_n   <= ce_n;
        o_ras_n  <= ras_n;
        o_cas_n  <= cas_n;
        o_awe_n  <= dma_awe_n;
        o_be_n   <= be_n;
        o_ea     <= dma_ea;
        t_emif   <= emif_t;
        dma_t_ed <= ed_t;
    end if;
end process latch_sigs;

dma_engine : EMIF_SDRAM_DMA
   generic map (
      CAS_LATENCY => 2,
      T_RAS       => "11",
      T_RC        => "100",
      T_RCD       => "01",
      T_RP        => "01",
      T_RRD       => "01",
      T_WR        => "01",
      SDRAM_COL_BITS    =>  8,
      SDRAM_ROW_BITS    =>  11,
      SDRAM_BANK_BITS   =>  2,
      NUM_CTRL_PORTS    =>  NUM_CTRL_PORTS,
      SDRAM_CHIP_SELECT =>  0
   )
   port map (
      emif_clk => emif_clk,

      -- EMIF BUS Interface
      o_ce_n   => ce_n,
      o_ras_n  => ras_n,
      o_cas_n  => cas_n,
      o_awe_n  => dma_awe_n,
      o_be_n   => be_n,
      o_ea     => dma_ea,
      t_emif   => emif_t,

      o_ed     => dma_ed,
      i_ed     => edi,
      t_ed     => ed_t,
      
      o_hold_n   => o_hold_n,
      i_holda_n  => i_holda_n,
      i_bus_req  => i_bus_req,

      --DMA Data Interface to Other Cores

      -- DMA Request lines (one normal priority, one high priority)
      -- High priority requests mean "take the EMIF bus from the processor" by asserting hold
      -- Low priority requests mean "wait for the EMIF bus to be free" meaning DSP is not using bus
      i_dma_req          => i_dma_req,
      i_dma_priority_req => i_dma_priority_req,
      i_dma_rwn          => i_dma_rwn,

      -- For the Input Address only bottom SDRAM_BANK_BITS+SDRAM_ROW_BITS+SDRAM_COL_BITS used (in that order)
      i_dma_addr        => i_dma_addr,
      i_dma_data        => i_dma_data,
      i_dma_count       => i_dma_count,
      i_dma_be          => i_dma_be,
      
      o_dma_wr_stb       => o_dma_wr_stb,
      o_dma_rd_stb      => o_dma_rd_stb,
      o_dma_data        => o_dma_data,
      o_dma_done        => o_dma_done

   );

end generate;

sdram_dma_xm : if INCLUDE_SDRAM_DMA = true and 
               CONFIG = "MityDSP-XM" generate
begin

-- latch output controls to align with data
latch_sigs : process(emif_clk)
begin
    if rising_edge(emif_clk) then
        o_ce_n   <= ce_n;
        o_ras_n  <= ras_n;
        o_cas_n  <= cas_n;
        o_awe_n  <= dma_awe_n;
        o_be_n   <= be_n;
        o_ea     <= dma_ea;
        t_emif   <= emif_t;
        dma_t_ed <= ed_t;
    end if;
end process latch_sigs;

dma_engine : EMIF_SDRAM_DMA
   generic map (
      CAS_LATENCY => 2,
      T_RAS       => "11",
      T_RC        => "100",
      T_RCD       => "01",
      T_RP        => "01",
      T_RRD       => "01",
      T_WR        => "01",
      SDRAM_COL_BITS    =>  9,
      SDRAM_ROW_BITS    =>  12,
      SDRAM_BANK_BITS   =>  2,
      NUM_CTRL_PORTS    =>  NUM_CTRL_PORTS,
      SDRAM_CHIP_SELECT =>  0
   )
   port map (
      emif_clk => emif_clk,

      -- EMIF BUS Interface
      o_ce_n   => ce_n,
      o_ras_n  => ras_n,
      o_cas_n  => cas_n,
      o_awe_n  => dma_awe_n,
      o_be_n   => be_n,
      o_ea     => dma_ea,
      t_emif   => emif_t,

      o_ed     => dma_ed,
      i_ed     => edi,
      t_ed     => ed_t,
      
      o_hold_n   => o_hold_n,
      i_holda_n  => i_holda_n,
      i_bus_req  => i_bus_req,

      --DMA Data Interface to Other Cores

      -- DMA Request lines (one normal priority, one high priority)
      -- High priority requests mean "take the EMIF bus from the processor" by asserting hold
      -- Low priority requests mean "wait for the EMIF bus to be free" meaning DSP is not using bus
      i_dma_req          => i_dma_req,
      i_dma_priority_req => i_dma_priority_req,
      i_dma_rwn          => i_dma_rwn,

      -- For the Input Address only bottom SDRAM_BANK_BITS+SDRAM_ROW_BITS+SDRAM_COL_BITS used (in that order)
      i_dma_addr        => i_dma_addr,
      i_dma_data        => i_dma_data,
      i_dma_count       => i_dma_count,
      i_dma_be          => i_dma_be,
      
      o_dma_wr_stb       => o_dma_wr_stb,
      o_dma_rd_stb      => o_dma_rd_stb,
      o_dma_data        => o_dma_data,
      o_dma_done        => o_dma_done

   );

end generate;

assert (INCLUDE_SDRAM_DMA/=TRUE or CONFIG="MityDSP" or CONFIG="MityDSP-XM")
report "SDRAM DMA only supported with MityDSP or MityDSP-XM CONFIG types."
severity FAILURE;

end rtl;
