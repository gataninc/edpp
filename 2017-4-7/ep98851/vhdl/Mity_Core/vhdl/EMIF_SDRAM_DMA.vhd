--- Title: EMIF_SDRAM_DMA.vhd
--- Description: 
---
--- SDRAM DMA control module (read and write) over shared DSP 6711 32 bit
--- EMIF bus.
---
---     o  0
---     | /       Copyright (c) 2008
---    (CL)---o   Critical Link, LLC
---      \
---       O
---
--- Company: Critical Link, LLC.
--- Date: 07/01/2008
--- Version: 1.00
--- Revisions: 1.00 Baseline

library IEEE;
library WORK;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use WORK.MityDSP_pkg.ALL;

entity EMIF_SDRAM_DMA is
   generic (
      CAS_LATENCY       : integer range 2  to 3  := 2; -- for MityDSP XM
      T_RAS : std_logic_vector(1 downto 0) := "11";    -- not used
      T_RC  : std_logic_vector(2 downto 0) := "100";   -- not used
      T_RCD : std_logic_vector(1 downto 0) := "01";    -- must be a minimum of one clock
      T_RP  : std_logic_vector(1 downto 0) := "01";    -- not used 
      T_RRD : std_logic_vector(1 downto 0) := "01";    -- not used 
      T_WR  : std_logic_vector(1 downto 0) := "01";    -- not used 
      SDRAM_COL_BITS    : integer range 7  to 9  := 9;
      SDRAM_ROW_BITS    : integer range 10 to 12 := 12;
      SDRAM_BANK_BITS   : integer range 1 to 3   := 2;
      NUM_CTRL_PORTS    : integer range 1  to 4  := 2;
      SDRAM_CHIP_SELECT : integer range 0 to 3   := 0
   );
   port (
      emif_clk : in std_logic; -- 50 Mhz EMIF clock

      -- EMIF BUS Interface
      o_ce_n         : out std_logic_vector(3 downto 0);
      o_ras_n        : out std_logic;       --AOE/SDRAS
      o_cas_n        : out std_logic;       --ARE/SDCAS
      o_awe_n        : out std_logic;       --AWE/SDWE
      o_be_n         : out std_logic_vector(3 downto 0);     --DQM3:0
      o_ea           : out std_logic_vector(15 downto 2);    --BA1:0 | A11:0
      t_emif         : out std_logic;         --Drive CEn, AOE, ARE, AWE, BEn, EAn

      o_ed           : out std_logic_vector(31 downto 0);
      i_ed           : in  std_logic_vector(31 downto 0);
      t_ed           : out std_logic;         --Drive EDn
      
      o_hold_n       : out std_logic;   --Request EMIF
      i_holda_n      : in std_logic;   --Acknowledge EMIF Request
      i_bus_req      : in std_logic;   --DSP Requests EMIF

      --DMA Data Interface to Other Cores

      -- DMA Request lines (one normal priority, one high priority)
      -- High priority requests mean "take the EMIF bus from the processor" by asserting hold
      -- Low priority requests mean "wait for the EMIF bus to be free" meaning DSP is not using bus
      i_dma_req          : in std_logic_vector(NUM_CTRL_PORTS-1 downto 0);
      i_dma_priority_req : in std_logic_vector(NUM_CTRL_PORTS-1 downto 0);
      i_dma_rwn         : in std_logic_vector(NUM_CTRL_PORTS-1 downto 0); -- '1' = read, '0'=write

      -- For the Input Address only bottom SDRAM_BANK_BITS+SDRAM_ROW_BITS+SDRAM_COL_BITS used (in that order)
      -- this is a 32 bit aligned word address (shift by two bits to translate into DSP byte address)
      i_dma_addr        : in  bus32_vector(NUM_CTRL_PORTS-1 downto 0); 
      i_dma_data        : in  bus32_vector(NUM_CTRL_PORTS-1 downto 0);
      -- number of 32 bit words - 1, max 512 at a time (5 uS stall @ 50 Mhz)
      -- Must be a minimum transfer of 4 words (value = 3)
      i_dma_count       : in  bus9_vector(NUM_CTRL_PORTS-1 downto 0);
      i_dma_be          : in  bus4_vector(NUM_CTRL_PORTS-1 downto 0);
      
      o_dma_wr_stb      : out std_logic_vector(NUM_CTRL_PORTS-1 downto 0);
      o_dma_rd_stb      : out std_logic_vector(NUM_CTRL_PORTS-1 downto 0);
      o_dma_data        : out std_logic_vector(31 downto 0);
      o_dma_done        : out std_logic_vector(NUM_CTRL_PORTS-1 downto 0) -- deassert i_dma_*req on next clock after o_dma_done is high

   );
end EMIF_SDRAM_DMA;

architecture rtl of EMIF_SDRAM_DMA is

signal col  : std_logic_vector(SDRAM_COL_BITS-1 downto 0);
signal row  : std_logic_vector(SDRAM_ROW_BITS-1 downto 0);
signal bank : std_logic_vector(SDRAM_BANK_BITS-1 downto 0);
signal sdram_addr : std_logic_vector(SDRAM_COL_BITS+SDRAM_ROW_BITS+SDRAM_BANK_BITS-1 downto 0);

signal ce_n : std_logic_vector(3 downto 0) := "1111";
signal ras_n, cas_n, awe_n : std_logic := '1';
signal be_n : std_logic_vector(3 downto 0) := "1111";
signal ea   : std_logic_vector(15 downto 2) := (others=>'0');
signal emif_t, ed_t : std_logic := '1';
signal hold_n : std_logic := '1';

signal tmp_cnt : std_logic_vector(1 downto 0) := "00";
signal dma_rd : std_logic := '0';
signal dma_we : std_logic := '0';
signal dma_done : std_logic := '0';
signal dma_rd_stb : std_logic := '0';
signal dma_rd_stb_r1, dma_rd_stb_r2 : std_logic := '0';
signal dma_din    : std_logic_vector(31 downto 0) := x"00000000";
signal dma_count : std_logic_vector(8 downto 0) := (others=>'0');
signal word_count : std_logic_vector(8 downto 0) := (others=>'0');
signal active_channel : std_logic_vector(NUM_CTRL_PORTS-1 downto 0) := CONV_STD_LOGIC_VECTOR(1,NUM_CTRL_PORTS);
signal chan_mask : std_logic_vector(NUM_CTRL_PORTS-1 downto 0) := CONV_STD_LOGIC_VECTOR(1,NUM_CTRL_PORTS);
signal dma_we_r : std_logic_vector(5 downto 0) := "000000";
signal holda_r : std_logic_vector(3 downto 0) := "0000";
signal holdn_r : std_logic_vector(3 downto 0) := "1111";
    
type ARB_STATES is (
      IDLE,            
      SELECT_CHANNEL,  
      GET_EMIF_BUS,
      PROC_REQ,
      DONE
      );
signal sm_arb : ARB_STATES := IDLE;

type SDRAM_STATES is (
      IDLE,
      ACTIVATE_ROW,
      RANDOM_READ,
      RANDOM_WRITE,
      BURST_TERMINATE,
      PRECHARGE,
      START_NEW_ROW,
      DONE);
      
signal sm_sdram : SDRAM_STATES := IDLE;

begin

-- output assignments
o_ce_n    <= ce_n;
o_ras_n   <= ras_n;
o_cas_n   <= cas_n;
o_awe_n   <= awe_n;
o_be_n    <= be_n;
o_ea      <= ea;
t_emif    <= emif_t;
t_ed      <= ed_t;
o_hold_n  <= '0' when holdn_r /= "1111" else '1';

-- Arbiter state machine
-- This process runs on the 90 degree (data) clock domain, shared between the
-- DMA controller, the MIG (for data), and the requesting units.  It is 
-- responsible for detecting incoming requests, arbitrating the requests,
-- and signaling to the MIG control state machine that we're ready to 
-- initiate a new transfer.
ARB_SM_PROC: process(emif_clk)
begin
    if rising_edge(emif_clk) then
        
        hold_n <= '0'; -- default (normally '1' in else clause in IDLE state)
        holda_r <= holda_r(2 downto 0) & i_holda_n;
        holdn_r <= holdn_r(2 downto 0) & hold_n;
        dma_done <= '0';
                    
        case sm_arb is
            when IDLE =>
                -- if there is a priority request, then figure out which one, get the bus and start
                if i_dma_priority_req /= CONV_STD_LOGIC_VECTOR(0, NUM_CTRL_PORTS) then
                    sm_arb <= SELECT_CHANNEL;
                    active_channel <= CONV_STD_LOGIC_VECTOR(1,NUM_CTRL_PORTS);
                    chan_mask      <= i_dma_priority_req;
                
                -- else if there is a normal request and the bus it free, figure out who, get the bus and start
                elsif i_dma_req /= CONV_STD_LOGIC_VECTOR(0, NUM_CTRL_PORTS) and i_bus_req='0' then
                    sm_arb <= SELECT_CHANNEL;
                    active_channel <= CONV_STD_LOGIC_VECTOR(1,NUM_CTRL_PORTS);
                    chan_mask      <= i_dma_req;
                
                else
                    hold_n <= '1';
                
                end if;
            
            -- arbitration is simple, pick the first one we find (which means lower 
            -- channels always win...).  We may want to make this more robust for 
            -- general purpose designs.
            when SELECT_CHANNEL =>
                if chan_mask(0)='1' then
                    sm_arb <= GET_EMIF_BUS;
                else
                    chan_mask <= '0' & chan_mask(NUM_CTRL_PORTS-1 downto 1);
                    active_channel <= active_channel(NUM_CTRL_PORTS-2 downto 0) & '0';
                end if;
                    
            when GET_EMIF_BUS =>
                if holda_r = "0000" then
                    sm_arb <= PROC_REQ;
                end if;
            
            when PROC_REQ =>
                if sm_sdram = DONE then
                    dma_done <= '1';
                    sm_arb <= DONE;
                end if;
            
            -- extra state to let controlling process release request line
            when DONE =>
                sm_arb <= IDLE;
               
                                    
        end case;
    end if;
end process ARB_SM_PROC;

-- SDRAM controller state machine.  Controls the EMIF bus and handles the 
-- assigned DMA request from the arbiter.
--
-- NOTES: This process is running of the rising edge of the emif_clk.  This
-- assumes a clock to pad delay of at least 2.5 ns is present on the EMIF output
-- interface (both controls and data), as commands and data are latched on the
-- rising edge of the EMIF clock by the SDRAM.  The SDRAM has a very short setup
-- and hold time requirement (typically 1-2 ns for either side).  It "works".
SDRAM_SM_PROC : process (emif_clk)
begin
    if rising_edge(emif_clk) then
        
        dma_rd_stb <= '0';
        dma_we <= '0';
        ea <= (others=>'0');
        ce_n <= "1111";
        be_n <= "1111";
        emif_t <= '0';
        ed_t   <= '1';
            
        case sm_sdram is
            when IDLE =>
                emif_t <= '1';
                col  <= sdram_addr(SDRAM_COL_BITS-1 downto 0);
                row <= sdram_addr(SDRAM_ROW_BITS+SDRAM_COL_BITS-1 downto SDRAM_COL_BITS);
                bank  <= sdram_addr(SDRAM_ROW_BITS+SDRAM_COL_BITS+SDRAM_BANK_BITS-1 downto SDRAM_COL_BITS+SDRAM_ROW_BITS);                    
                word_count <= (others=>'0');
                tmp_cnt <= (others=>'0');
                if sm_arb = PROC_REQ then
                    sm_sdram <= ACTIVATE_ROW;
                end if;
                    
            when ACTIVATE_ROW =>
                ras_n    <= '0';
                cas_n    <= '1';
                awe_n    <= '1';
                ea(SDRAM_ROW_BITS+3 downto SDRAM_ROW_BITS+2) <= bank;
                ea(SDRAM_ROW_BITS+1 downto 2)  <= row;
                if tmp_cnt = "00" then
                    ce_n(SDRAM_CHIP_SELECT)  <= '0';
                else
                    ce_n(SDRAM_CHIP_SELECT)  <= '1';
                end if;
                tmp_cnt <= tmp_cnt+'1';
                if tmp_cnt = T_RCD then
                    if dma_rd='1' then
                        sm_sdram <= RANDOM_READ;
                    else
                        sm_sdram <= RANDOM_WRITE;
                        ed_t <= '0';
                        dma_rd_stb <= '1';
                    end if;
                end if;
                    
            when RANDOM_READ =>
                ras_n    <= '1';
                cas_n    <= '0';
                awe_n    <= '1';
                ce_n(SDRAM_CHIP_SELECT)  <= '0';
                be_n     <= "0000";
                dma_we     <= '1';
                ea(SDRAM_ROW_BITS+3 downto SDRAM_ROW_BITS+2) <= bank;
                ea(SDRAM_COL_BITS+1 downto 2)  <= col;
                -- if we can continue (not done and no need to wrap around)
                if col /= CONV_STD_LOGIC_VECTOR((2**SDRAM_COL_BITS)-1, SDRAM_COL_BITS) and
                   word_count /= dma_count then
                      word_count <= word_count+'1';
                      col <= col+'1';
                else
                    col <= (others=>'0');
                    sm_sdram <= BURST_TERMINATE;
                end if;
            
            when RANDOM_WRITE =>
                ed_t <= '0';
                ras_n    <= '1';
                cas_n    <= '0';
                awe_n    <= '0';
                ce_n(SDRAM_CHIP_SELECT)  <= '0';
                be_n     <= "0000";
                ea(SDRAM_ROW_BITS+3 downto SDRAM_ROW_BITS+2) <= bank;
                ea(SDRAM_COL_BITS+1 downto 2)  <= col;
                if col /= CONV_STD_LOGIC_VECTOR((2**SDRAM_COL_BITS)-1, SDRAM_COL_BITS) and
                    word_count /= dma_count then
                       dma_rd_stb <= '1';
                       word_count <= word_count+'1';
                       col <= col+'1';
                else
                    dma_rd_stb <= '0';
                    col <= (others=>'0');
                    sm_sdram <= BURST_TERMINATE;
                end if;
                
            when BURST_TERMINATE =>
                be_n     <= "0000";
                ras_n    <= '1';
                cas_n    <= '1';
                awe_n    <= '0';
                ce_n(SDRAM_CHIP_SELECT)  <= '0';
                sm_sdram <= PRECHARGE;
                --bug 1692: if word_count /= dma_count and dma_rd='0' then
                if word_count /= dma_count then -- fix 1692
                    word_count <= word_count+'1';
                end if;
                
            when PRECHARGE =>
                be_n     <= "0000";
                ras_n    <= '0';
                cas_n    <= '1';
                awe_n    <= '0';
                ea(12)   <= '1'; -- charge all banks (though it doesn't mattter)
                ea(SDRAM_ROW_BITS+3 downto SDRAM_ROW_BITS+2) <= bank;
                if word_count = dma_count then
                    -- if we were reading, we will wait for the reads to finish before declaring we're really "done"
                    if dma_we_r((CAS_LATENCY+1) downto 0) /= CONV_STD_LOGIC_VECTOR(0,CAS_LATENCY+2) then
                        ce_n(SDRAM_CHIP_SELECT) <= '1'; -- NOOP until the read pipeline empties
                    else
                        ce_n(SDRAM_CHIP_SELECT)  <= '0';
                        sm_sdram <= DONE;
                    end if;
                else
                    ce_n(SDRAM_CHIP_SELECT)  <= '0';
                    if row /= CONV_STD_LOGIC_VECTOR((2**SDRAM_ROW_BITS)-1, SDRAM_ROW_BITS) then
                        row <= row+'1';
                    else
                        row <= (others=>'0');
                        bank <= bank+'1';
                    end if;
                    sm_sdram <= START_NEW_ROW;
                end if;
                    
            when START_NEW_ROW =>
                ce_n(SDRAM_CHIP_SELECT)  <= '1'; -- NOOP
                tmp_cnt <= (others=>'0');
                sm_sdram   <= ACTIVATE_ROW;
                
            when DONE =>
                emif_t <= '1';
                if sm_arb = IDLE then
                    sm_sdram <= IDLE;
                end if;
                    
        end case;
    end if;
end process SDRAM_SM_PROC;

o_ed   <= dma_din when dma_rd_stb_r2='1' or dma_rd_stb_r1='1' else (others=>'0');

-- This process multiplexes the inputs from the various DMA sources and 
-- assigns the DMA outputs accordingly.
DATA_MUX: process(emif_clk)
begin
   if rising_edge(emif_clk) then
      dma_rd_stb_r1 <= dma_rd_stb;
      dma_rd_stb_r2 <= dma_rd_stb_r1;
      dma_we_r <= dma_we_r(4 downto 0) & dma_we;
      for i in 0 to NUM_CTRL_PORTS-1 loop
         if active_channel(i)='1' then
            dma_din           <= i_dma_data(i);
            dma_rd            <= i_dma_rwn(i);
            sdram_addr        <= i_dma_addr(i)(SDRAM_COL_BITS+SDRAM_ROW_BITS+SDRAM_BANK_BITS-1 downto 0);
            dma_count         <= i_dma_count(i);
         end if;
         o_dma_wr_stb(i)     <= dma_we_r(CAS_LATENCY+1) and active_channel(i);
         o_dma_data          <= i_ed;
      end loop;
   end if;    
end process DATA_MUX;

gen_done : for i in 0 to NUM_CTRL_PORTS-1 generate
begin
    o_dma_done(i)   <= dma_done   and active_channel(i);
    o_dma_rd_stb(i) <= dma_rd_stb and active_channel(i);
end generate;

end rtl;
