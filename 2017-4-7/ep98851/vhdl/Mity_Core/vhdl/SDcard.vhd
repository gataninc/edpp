--- Title: SDcard.vhd
--- Description: 
---
---     o  0
---     | /       Copyright (c) 2009
---    (CL)---o   Critical Link, LLC
---      \
---       O
---
--- Company: Critical Link, LLC.
--- Date: 02/19/2009
--- Version: 1.00
---
--- Revision History
---   1.0 - Baseline Template
---
library WORK;
library IEEE;
library UNISIM;
use WORK.MITYDSP_PKG.ALL;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use UNISIM.VCOMPONENTS.ALL;

entity SDcard is
   port ( 
      clk            : in  std_logic;
      i_ABus         : in  std_logic_vector(4 downto 0);
      i_DBus         : in  std_logic_vector(31 downto 0);
      o_DBus_SPI     : out std_logic_vector(31 downto 0);
      o_DBus_GPIO    : out std_logic_vector(31 downto 0);
      i_cs_SPI       : in  std_logic;	
      i_cs_GPIO      : in  std_logic;	
      i_wr_en        : in  std_logic;
      i_rd_en        : in  std_logic;
      o_irq_SPI      : out std_logic;
      o_irq_GPIO     : out std_logic;
      i_ilevel_SPI   : in    std_logic_vector(1 downto 0); 
      i_ilevel_GPIO  : in    std_logic_vector(1 downto 0); 
      i_ivector_SPI  : in    std_logic_vector(4 downto 0);
      i_ivector_GPIO : in    std_logic_vector(4 downto 0);
      o_mmc_cs_n     : out   std_logic := '1'; -- card select (active-low)
      o_mmc_clk      : out   std_logic := '0'; -- card clock
      o_mmc_di       : out   std_logic := '0'; -- card data in
      i_mmc_do       : in    std_logic := '0'; -- card data out
      i_mmc_cd_n     : in    std_logic := '1'  -- card detect (active-low)
    );
end SDcard;

architecture rtl of SDcard is

component spit
   port 
   (  
      emif_clk        : in  std_logic;
      i_ABus          : in  std_logic_vector(4 downto 0);
      i_DBus          : in  std_logic_vector(31 downto 0);
      o_DBus          : out std_logic_vector(31 downto 0);
      i_wr_en         : in  std_logic;
      i_rd_en         : in  std_logic;
      i_cs            : in  std_logic;	
      o_irq           : out std_logic;
      i_ilevel        : in std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld    : in std_logic := '0';                           -- interrupt level valid flag
      i_ivector       : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld   : in std_logic := '0';                           -- interrupt vector valid flag
      i_clk           : in  std_logic;
      o_sclk          : out std_logic;                      -- SPI output clock
      o_sync          : out std_logic;                      -- SYNC output 
      o_mosi          : out std_logic;                      -- data output 
      i_miso          : in  std_logic                       -- data input 
    );                          
end component;						 

component gpio is
   generic (
      NUM_IO : integer
   );
   port ( 
      clk            : in  std_logic;
      i_ABus         : in  std_logic_vector(4 downto 0);
      i_DBus         : in  std_logic_vector(31 downto 0);
      o_DBus         : out std_logic_vector(31 downto 0);
      i_wr_en        : in  std_logic;
      i_rd_en        : in  std_logic;
      i_cs           : in  std_logic;
      o_irq          : out std_logic;
      i_ilevel       : in  std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld   : in  std_logic := '0';                           -- interrupt level valid flag
      i_ivector      : in  std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld  : in  std_logic := '0';                           -- interrupt vector valid flag
      i_port         : in  std_logic_vector(NUM_IO-1 downto 0);
      o_port         : out std_logic_vector(NUM_IO-1 downto 0);
      t_port         : out std_logic_vector(NUM_IO-1 downto 0)
    );
end component;						 

signal fast_clk_select : std_logic := '0';
signal clk_raw         : std_logic := '0';
signal clk_400         : std_logic := '0';
signal clk_count       : std_logic_vector(7 downto 0):= (others=>'0');
signal mmc_clk         : std_logic := '0';
signal clk_hs          : std_logic := '0';

begin

-- process to generate either 400 khz clock 
-- or 20 MHz clock 
mmc_clk_gen: process(clk)
begin
    if clk'event and clk='1' then
       if clk_count = x"3E" then
          clk_count <= (others=>'0');
          clk_raw   <= not clk_raw;
       else
          clk_count <= clk_count+'1';
       end if; 
       clk_hs <= not clk_hs;      
    end if;
end process mmc_clk_gen;

-- hmm....
mmc_clk <= clk_hs when fast_clk_select='1' else clk_raw;

--bufg_clk400 : bufg port map (i => clk_raw, o => clk_400);
--bufgmux_spi : bufgmux port map ( O  => mmc_clk,
--                                 I0 => clk_400,
--                                 I1 => clk,
--                                 S  => fast_clk_select);
mmc_spi : spit
   port map (
      emif_clk        => clk,
      i_ABus          => i_ABus,
      i_DBus          => i_DBus,
      o_DBus          => o_DBus_SPI,
      i_wr_en         => i_wr_en,
      i_rd_en         => i_rd_en,
      i_cs            => i_cs_SPI,
      o_irq           => o_irq_SPI,
      i_ilevel        => i_ilevel_SPI,
      i_ilevel_vld    => '1',              -- interrupt level valid flag
      i_ivector       => i_ivector_SPI,
      i_ivector_vld   => '1',              -- interrupt vector valid flag
      i_clk           => mmc_clk,
      o_sclk          => o_mmc_clk,        -- SPI clock
      o_sync          => open,             -- SYNC output line (chip select??)
      o_mosi          => o_mmc_di,         -- data output line
      i_miso          => i_mmc_do
    );

mmc_gpio: gpio 
   generic map (
      NUM_IO => 3
   )
   port map ( 
      clk            => clk,
      i_ABus         => i_ABus,
      i_DBus         => i_DBus,
      o_DBus         => o_DBus_GPIO,
      i_wr_en        => i_wr_en,
      i_rd_en        => i_rd_en,
      i_cs           => i_cs_GPIO,
      o_irq          => o_irq_GPIO,
      i_ilevel       => i_ilevel_GPIO,
      i_ilevel_vld   => '1',
      i_ivector      => i_ivector_GPIO,
      i_ivector_vld  => '1',
      i_port(0)      => '0',
      i_port(1)      => '0',
      i_port(2)      => i_mmc_cd_n,
      o_port(0)      => o_mmc_cs_n,
      o_port(1)      => fast_clk_select,
      o_port(2)      => open,
      t_port         => open
    );

end rtl;

