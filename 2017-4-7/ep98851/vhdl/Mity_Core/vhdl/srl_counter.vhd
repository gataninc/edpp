--! @file srl_counter.vhd
--!
--! @brief SRL based clock divider
--!

library IEEE;   
--! Uses UNISIM to support simulation in modelsim 
library UNISIM;  
--! Uses WORK library
library WORK;    
--! Uses IEEE library Standard Logic
use IEEE.STD_LOGIC_1164.ALL; 
--! Uses IEEE library Standard Arithmetic
use IEEE.STD_LOGIC_ARITH.ALL;
--! Uses IEEE library Standard Unsigned logic
use IEEE.STD_LOGIC_UNSIGNED.ALL;
--! Uses UNISIM VCOMPONENTS
use UNISIM.VCOMPONENTS.ALL;
--! Requires MityDSP Package (compiled in WORK library)
use WORK.MityDSP_pkg.all;

-------------------------------------------------------------------------------
--! @brief SRL based clock divider
--!
--! Generates a clock enable every N clocks, where N is 
--! 
--!    (D1+1)(D2+1)(D3+1)...(DN+1)
--!
--! Where DX is Division X, and X runs from 1 to NUM_STAGES.
--!
--! The default is 4 stages with 16 delays per stage or (16)^4 clocks (16 K)
--/
entity srl_counter is
   generic (
      --! Number of stages of 16 bit shift registers to synthesize
      NUM_STAGES : integer := 4 
   );
   port (
      clk        :  in  std_logic;    --! input clock
      clk_en     :  out std_logic;    --! output clock enable (1 per cycle)
      DIVISIONS  : bus4_vector(NUM_STAGES-1 downto 0) := (x"F",x"F",x"F",x"F") --! output clock enable (1 per cycle)
      );
end srl_counter;

architecture rtl of srl_counter is

signal div        : std_logic_vector(NUM_STAGES-1 downto 0) := CONV_STD_LOGIC_VECTOR(0, NUM_STAGES); --! divider register
signal div_r1     : std_logic_vector(NUM_STAGES-1 downto 0) := CONV_STD_LOGIC_VECTOR(0, NUM_STAGES);
signal div_clk_en : std_logic_vector(NUM_STAGES downto 0);
signal reset      : std_logic := '0';

begin

reset_config: roc port map (o => reset);

div_process : process(clk, reset)
begin
   if reset='1' then
      for i in 0 to NUM_STAGES-1 loop
         div_r1(i) <= '0';
      end loop;
   elsif clk='1' and clk'event then
      -- latch output of SRL
      for i in 0 to NUM_STAGES-1 loop
         div_r1(i) <= div(i);
      end loop;
      for i in 1 to NUM_STAGES loop
         if div(i-1)='1' and div_r1(i-1)='0' then
            div_clk_en(i) <= '1';
         else
            div_clk_en(i) <= '0';
         end if;
      end loop;
            
   end if;
end process div_process;

clk_en  <= div_clk_en(NUM_STAGES);

SRL16_inst : SRL16
   generic map (
      INIT => X"0001"
   )
   port map (
      Q =>  div(0),              -- SRL data output
      A0 => DIVISIONS(0)(0),     -- Select[0] input
      A1 => DIVISIONS(0)(1),     -- Select[1] input
      A2 => DIVISIONS(0)(2),     -- Select[2] input
      A3 => DIVISIONS(0)(3),     -- Select[3] input
      CLK => clk,                -- Clock input
      D => div(0)                -- SRL data input
  );

gen_block: for i in 1 to NUM_STAGES-1 generate

SRL16_inst : SRL16E
   generic map (
      INIT => X"0001"
   )
   port map (
      Q =>  div(i),              -- SRL data output
      A0 => DIVISIONS(i)(0),     -- Select[0] input
      A1 => DIVISIONS(i)(1),     -- Select[1] input
      A2 => DIVISIONS(i)(2),     -- Select[2] input
      A3 => DIVISIONS(i)(3),     -- Select[3] input
      CE => div_clk_en(i),       -- Clock enable input
      CLK => clk,                -- Clock input
      D => div(i)                -- SRL data input
  );
end generate gen_block;

end rtl;