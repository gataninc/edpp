--- Title: gpio.vhd
--- Description: This module interfaces to the MityDSP for General Purpose I/Os
---
---
---     o  0                          
---     | /       Copyright (c) 2005-2006
---    (CL)---o   Critical Link, LLC  
---      \                            
---       O                           
---
--- Company: Critical Link, LLC.
--- Date: 03/15/2006
--- Version: 
---   1.0 10/13/2005 Initial Version
---   1.1 12/09/2005 Reversed Tristate
---   1.2 03/15/2005 1-based NUM_IO, No Async Reset
---   

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library Unisim;
use Unisim.Vcomponents.all;

entity gpin is
   generic (
      NUM_IO : integer := 32 
   );
   Port ( 
      clk        : in  std_logic;
      i_ABus     : in  std_logic_vector(4 downto 0);
      i_DBus     : in  std_logic_vector(31 downto 0);
      o_DBus     : out std_logic_vector(31 downto 0);
      i_wr_en    : in  std_logic;
      i_rd_en    : in  std_logic;
      i_cs       : in  std_logic;	
      o_irq      : out std_logic;
      i_ilevel       : in    std_logic_vector(1 downto 0) := "01";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld   : in    std_logic := '0';                           -- interrupt level valid flag
      i_ivector      : in    std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld  : in    std_logic := '0';                           -- interrupt vector valid flag
      i_port     : in  std_logic_vector(NUM_IO-1 downto 0)
    );
end gpin;						 

--*
--* @short Register Transfer Logic Implementation of gpio core entity.
--* 
--/
architecture rtl of gpin is

-- All Used components should be declared first.
--
constant CORE_APPLICATION_ID: std_logic_vector(7 downto 0) := CONV_STD_LOGIC_VECTOR( 4, 8);
constant CORE_VERSION_MAJOR:  std_logic_vector(3 downto 0) := CONV_STD_LOGIC_VECTOR( 1, 4);
constant CORE_VERSION_MINOR:  std_logic_vector(3 downto 0) := CONV_STD_LOGIC_VECTOR( 2, 4);
constant CORE_YEAR:           std_logic_vector(4 downto 0) := CONV_STD_LOGIC_VECTOR( 06, 5);
constant CORE_MONTH:          std_logic_vector(3 downto 0) := CONV_STD_LOGIC_VECTOR( 03, 4);
constant CORE_DAY:            std_logic_vector(4 downto 0) := CONV_STD_LOGIC_VECTOR( 15, 5);

component core_version
   port (
      clk           : in std_logic;                       -- system clock
      rd            : in std_logic;                       -- read enable
      ID            : in std_logic_vector(7 downto 0);    -- assigned ID number, 0xFF if unassigned
      version_major : in std_logic_vector(3 downto 0);    -- major version number 1-15
      version_minor : in std_logic_vector(3 downto 0);    -- minor version number 0-15
      year          : in std_logic_vector(4 downto 0);    -- year since 2000
      month         : in std_logic_vector(3 downto 0);    -- month (1-12)
      day           : in std_logic_vector(4 downto 0);    -- day (1-32)
      ilevel        : in std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      ilevel_vld    : in std_logic := '0';                           -- interrupt level valid flag
      ivector       : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      ivector_vld   : in std_logic := '0';                           -- interrupt vector valid flag
      o_data        : out std_logic_vector(31 downto 0)
      );
end component;

signal gpio_read_reg     : std_logic_vector(NUM_IO-1 downto 0) := (others=>'0'); -- read register
signal gpio_ieup_reg     : std_logic_vector(NUM_IO-1 downto 0) := (others=>'0'); -- interrupt enable up register
signal gpio_iedown_reg   : std_logic_vector(NUM_IO-1 downto 0) := (others=>'0'); -- interrupt enable down register
signal i_port_r1         : std_logic_vector(NUM_IO-1 downto 0) := (others=>'0'); -- registered version of input
signal gpio_pend_reg     : std_logic_vector(NUM_IO-1 downto 0) := (others=>'0'); -- interrupt pending from given line
signal version_reg       : std_logic_vector(31 downto 0);
signal ver_rd            : std_logic;
signal r_rd_en           : std_logic;

begin

-- 
-- Version History
-- 1.0  Baseline
-- 1.1  Swapping t_port logic to assert logic 1 for tri-state enable rather than 0
-- 1.2  NUM_IO not 0-based
--
version : core_version
   port map(
      clk           => clk,                  -- system clock
      rd            => ver_rd,               -- read enable
      ID            => CORE_APPLICATION_ID,  -- assigned ID number, 0xFF if unassigned
      version_major => CORE_VERSION_MAJOR,   -- major version number 1-15
      version_minor => CORE_VERSION_MINOR,   -- minor version number 0-15
      year          => CORE_YEAR,            -- year since 2000
      month         => CORE_MONTH,           -- month (1-12)
      day           => CORE_DAY,             -- day (1-31)
      ilevel        => i_ilevel,
      ilevel_vld    => i_ilevel_vld,
      ivector       => i_ivector,
      ivector_vld   => i_ivector_vld,
      o_data        => version_reg
      );

o_irq <= '0' when gpio_pend_reg=CONV_STD_LOGIC_VECTOR(0,NUM_IO-1) else '1';

gpio_block: for i in 0 to NUM_IO-1 generate
   gpio_read_reg(i) <= i_port(i);
   
   -- interrupt logic
   interrupt_block : process(clk)
   begin
      if clk'event and clk='1' then
         i_port_r1(i) <= i_port(i);
         if  gpio_ieup_reg(i)='1' and i_port_r1(i)='0' and i_port(i)='1' then
            gpio_pend_reg(i) <= '1';
         elsif gpio_iedown_reg(i)='1' and i_port_r1(i)='1' and i_port(i)='0' then
            gpio_pend_reg(i) <= '1';
         elsif i_cs = '1' and i_wr_en = '1' and i_ABus = "00101" and
               i_DBus(i)='1' then
            gpio_pend_reg(i) <= '0';
         else
            gpio_pend_reg(i) <= gpio_pend_reg(i);
         end if;
      end if;   
   end process interrupt_block;
end generate gpio_block;

--* Handle read requests from the processor
--/
read_mux_regs : process (clk) is
begin
	if clk'event and clk='1' then
	    r_rd_en <= i_rd_en;
	    
	    -- version stuff
		if i_ABus="00000" and i_cs='1' and i_rd_en='1' then
	       ver_rd <= '1';
	    else
	       ver_rd <= '0';
	    end if;
	    
        if i_cs='0' then
           o_DBus <= (others=>'0');
        else
            case i_ABus is
                when "00000" =>   
                   o_DBus <=  version_reg;
                when "00001" =>
                   o_DBus(NUM_IO-1 downto 0) <= (others=>'0');
                when "00010" =>	
                   o_DBus(NUM_IO-1 downto 0) <= gpio_read_reg;
                when "00011" =>	
                   o_DBus(NUM_IO-1 downto 0) <= gpio_ieup_reg;
                when "00100" =>	
                   o_DBus(NUM_IO-1 downto 0) <= gpio_iedown_reg; 
                when "00101" =>	
                   o_DBus(NUM_IO-1 downto 0) <= gpio_pend_reg;
                when others =>	
                   o_DBus <= (others=>'0');
            end case;
        end if;
    end if;
end process read_mux_regs;

--* Decode register write requests.
--/
wr_ctl_reg : process(clk)
begin
	if clk='1' and clk'event then
		if i_cs = '1' and i_wr_en = '1' then
         -- reset clear register unless explicitly written to.
			case i_ABus is
    			when "00011" =>
    			   gpio_ieup_reg   <= i_DBus(NUM_IO-1 downto 0);	
    			when "00100" =>	
    			   gpio_iedown_reg <= i_DBus(NUM_IO-1 downto 0);
    			when others => NULL;
			end case;
      end if;
	end if;
end process wr_ctl_reg;


end rtl;