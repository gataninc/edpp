--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 


library IEEE;
use IEEE.STD_LOGIC_1164.all;

package MityDSP_pkg is

-- Declare types
  
   type bus32_vector is
      array(natural range <>) of std_logic_vector(31 downto 0);

   type bus16_vector is
      array(natural range <>) of std_logic_vector(15 downto 0);

   type bus9_vector is
      array(natural range <>) of std_logic_vector(8 downto 0);
      
   type bus8_vector is
      array(natural range <>) of std_logic_vector(7 downto 0);

   type bus4_vector is
      array(natural range <>) of std_logic_vector(3 downto 0);

--  type <new_type> is
--    record
--        <type_name>        : std_logic_vector( 7 downto 0);
--        <type_name>        : std_logic;
--    end record;

-- Declare constants

--  constant <constant_name>		: time := <time_unit> ns;
--  constant <constant_name>		: integer := <value>;
 
-- Declare library component entities

component dcm_basic
	port (
		CLKIN_IN        : IN  std_logic;
		RST_IN          : IN  std_logic;          
		CLKIN_IBUFG_OUT : OUT std_logic;
		CLK0_OUT        : OUT std_logic;
		LOCKED_OUT      : OUT std_logic;
		STATUS_OUT      : OUT std_logic_vector(2 downto 0)
	);
end component;

component dcm_basic100
   port ( 
      CLKIN_IN        : in  std_logic; 
      RST_IN          : in  std_logic; 
      CLKIN_IBUFG_OUT : out std_logic; 
      CLK0_OUT        : out std_logic; 
      LOCKED_OUT      : out std_logic; 
      STATUS_OUT      : out std_logic_vector(2 downto 0)
   );
end component;

component EMIF_iface
    generic ( 
        DECODE_BITS       : integer range 1 to 9 := 5;
        INCLUDE_SDRAM_DMA : boolean := FALSE;
        NUM_CTRL_PORTS    : integer range 1 to 4 := 4; -- required if INCLUDE_DMA = true
        CONFIG            : string := "UNKNOWN"        -- "MityDSP" | "MityDSP-XM", required if INCLUDE_DMA = true
    );
    port ( emif_clk  : in std_logic;
           -- EMIF signals
           i_ce2_n   : in std_logic;
           i_ce3_n   : in std_logic;
           i_aoe_n   : in std_logic;
           i_are_n   : in std_logic;
           i_awe_n   : in std_logic;
           i_be_n    : in std_logic_vector(3 downto 0);
           i_ea      : in std_logic_vector(15 downto 2);
           i_ed      : in std_logic_vector(31 downto 0);
           o_ed      : out std_logic_vector(31 downto 0);
           t_ed      : out std_logic;
           o_hold_n  : out std_logic := '1';   
           i_holda_n : in std_logic;   
           i_bus_req : in std_logic;
           
           -- FPGA fabric signals
           be_r      : out std_logic_vector(3 downto 0);
           addr_r    : out std_logic_vector(4 downto 0);
           cs_r      : out std_logic_vector((2**DECODE_BITS)-1 downto 0);
           ce3_r     : out std_logic;
           aoe_r     : out std_logic;
           rd_r      : out std_logic;
           wr_r      : out std_logic;
           edi_r     : out std_logic_vector(31 downto 0);
           edo_ce2   : in std_logic_vector(31 downto 0);
           edo       : in bus32_vector((2**DECODE_BITS)-1 downto 0);

           ----------------------------------------------------------------
           -- The items below this line are only required if INCLUDE_DMA = true
           -- 
           o_ce_n    : out std_logic_vector(3 downto 0) := "0000";
           o_ras_n   : out std_logic := '1';       --AOE/SDRAS
           o_cas_n   : out std_logic := '1';       --ARE/SDCAS
           o_awe_n   : out std_logic := '1';       --AWE/SDWE
           o_be_n    : out std_logic_vector(3 downto 0) := "1111";            --DQM3:0
           o_ea      : out std_logic_vector(15 downto 2) := (others=>'0');    --BA1:0 | A11:0
           t_emif    : out std_logic := '1';         --Drive CEn, AOE, ARE, AWE, BEn, EAn           
            
           -- SDRAM DMA fabric signals (can leave disconnected if INCLUDE_DMA = false)
           -- DMA Request lines (one normal priority, one high priority)
           -- High priority requests mean "take the EMIF bus from the processor" by asserting hold
           -- Low priority requests mean "wait for the EMIF bus to be free" meaning DSP is not using bus
           i_dma_req          : in std_logic_vector(NUM_CTRL_PORTS-1 downto 0) := (others=>'0');
           i_dma_priority_req : in std_logic_vector(NUM_CTRL_PORTS-1 downto 0) := (others=>'0');
           i_dma_rwn          : in std_logic_vector(NUM_CTRL_PORTS-1 downto 0) := (others=>'0');

           -- For the Input Address only bottom SDRAM_BANK_BITS+SDRAM_ROW_BITS+SDRAM_COL_BITS used (in that order)
           -- this is a 32 bit aligned word address (shift by two bits to translate into DSP byte address)
           i_dma_addr        : in  bus32_vector(NUM_CTRL_PORTS-1 downto 0) := (others=>(others=>'0')); 
           i_dma_data        : in  bus32_vector(NUM_CTRL_PORTS-1 downto 0) := (others=>(others=>'0')); 
           -- number of 32 bit words - 1, max 512 at a time (5 uS stall @ 50 Mhz)
           -- Must be a minimum transfer of 4 words (value = 3)
           i_dma_count       : in  bus9_vector(NUM_CTRL_PORTS-1 downto 0)  := (others=>(others=>'0'));  -- number of 32 bit words, max 512 at a time (6 uS stall @ 50 Mhz)
           i_dma_be          : in  bus4_vector(NUM_CTRL_PORTS-1 downto 0)  := (others=>(others=>'1')); 
      
           o_dma_wr_stb      : out std_logic_vector(NUM_CTRL_PORTS-1 downto 0) := (others=>'0');
           o_dma_rd_stb      : out std_logic_vector(NUM_CTRL_PORTS-1 downto 0) := (others=>'0');
           o_dma_data        : out std_logic_vector(31 downto 0) := (others=>'0');
           o_dma_done        : out std_logic_vector(NUM_CTRL_PORTS-1 downto 0)  := (others=>'0')  -- deassert i_dma_*req on next clock after o_dma_done is high
    );
end component;

component C645x_EMIF
   generic (
      DECODE_BITS : integer range 1 to 15 := 5
   );
   port (
      emif_clk : in std_logic;
      -- EMIF signals
      i_ce2_n : in  std_logic := '1'; -- 0xA0000000 - Asynchronous Memory Space - Mity-1 CE3 @ 0xB0000000
      i_ce4_n : in  std_logic := '1'; -- 0xC0000000 - Synchronous Memory Space
      i_aoe_n : in  std_logic := '1'; -- Async Read-Strobe (Mity-1 ARE_N) / Sync Output-Enable
      i_awe_n : in  std_logic := '1'; -- Async/Sync Write-Enable
      i_rw_n  : in  std_logic := '1'; -- Read-high, Write-low - inverted version of Mity-1 AOE_N
      i_be_n  : in  std_logic_vector(3 downto 0) := (others=>'1');
      i_ea    : in  std_logic_vector(19 downto 0) := (others=>'0');
      i_ed    : in  std_logic_vector(31 downto 0) := (others=>'0');
      o_ed    : out std_logic_vector(31 downto 0);
      t_ed    : out std_logic;
      -- Async Core Interface signals
      be_r   : out std_logic_vector(3 downto 0) := (others=>'1');
      addr_r : out std_logic_vector(4 downto 0) := (others=>'0'); -- Async Address
      cs_r   : out std_logic_vector((2**DECODE_BITS)-1 downto 0) := (others=>'0'); -- Async Core-Select
      rd_lead_r  : out std_logic := '0'; -- old read strobe - leading edge of DSP strobe - risky for post-read ops
      rd_trail_r : out std_logic := '0'; -- new read strobe - trailing edge of DSP strobe - safe for post-read ops
      wr_r   : out std_logic := '0'; -- write strobe - leading edge of DSP strobe
      edi_r  : out std_logic_vector(31 downto 0);
      -- Data-Out-Bus-Mux Inputs
      edo_ce4 : in std_logic_vector(31 downto 0) := (others=>'0');
      edo     : in bus32_vector((2**DECODE_BITS)-1 downto 0)
   );
end component;

component base_module
   generic (
      CONFIG : string := "UNKNOWN" -- "MityDSP" | "MityDSP-XM" | "MityDSP-Pro"
      );
   port (
      emif_clk        : in std_logic;
      i_cs            : in std_logic;
      i_ID            : in std_logic_vector(7 downto 0);    -- assigned Application ID number, 0xFF if unassigned
      i_version_major : in std_logic_vector(3 downto 0);    -- major version number 1-15
      i_version_minor : in std_logic_vector(3 downto 0);    -- minor version number 0-15
      i_year          : in std_logic_vector(4 downto 0);    -- year since 2000
      i_month         : in std_logic_vector(3 downto 0);    -- month (1-12)
      i_day           : in std_logic_vector(4 downto 0);    -- day (1-32)
      i_ABus          : in std_logic_vector(4 downto 0);
      i_DBus          : in std_logic_vector(31 downto 0);
      o_DBus          : out std_logic_vector(31 downto 0);
      i_wr_en         : in std_logic;
      i_rd_en         : in std_logic;
      
      i_clk25mhz      : in  std_logic; -- 25 Mhz input clock domain (BUFG desirable, but should also work with local clock)
      o_clken100ms    : out std_logic; -- free-running 100ms period clock-enable in the 25mhz domain
      o_clken4ms      : out std_logic; -- free-running 4 ms period clock-enable in the 25mhz domain
      o_wd_nmi        : out std_logic; -- watchdog timer non-maskable interrupt output (active-high)
      o_wd_rst        : out std_logic; -- watchdog timer dsp reset (active-high)
      
      o_emif_dcm_reset  : out std_logic;                     -- hook to EMIF DCM reset line (not valid if i_clk25mhz is not connected)
      i_emif_dcm_status : in  std_logic_vector(2 downto 0);  -- hook to EMIF DCM status lines (not used if i_clk25mhz is not connected)
      i_emif_dcm_lock   : in  std_logic;                     -- hook to EMIF DCM lock line  (not used if i_clk25mhz is not connected)

      -- these signals are valid only for CONFIG="MityDSP" or CONFIG="MityDSP-XM"
      i_irq_map_4x8   : in  bus8_vector(7 downto 4) := (others=>(others=>'0')); -- ext_irq4,5,6,7
      o_irq_output_4  : out std_logic_vector(7 downto 4);                       -- ext_irq4,5,6,7

      -- these signals are valid only for CONFIG="MityDSP"
      t_bank_sel_n    : out std_logic; -- need inverter / tri-state buffer at top level; 0=HiZ, 1=GND
      o_led_enable    : out std_logic; -- need inverter at top level
      i_bank_zero_clr : in  std_logic := '0'; -- flash bank zero asynchronous clear (active-high)

      -- these signals are valid only for CONFIG="MityDSP-XM"
      o_sba_led       : out std_logic := '1'; -- serial-bank-address data / user LED (FPGA_INIT_B/LED/SBA)
                                              -- direct connection at top level
      t_sba_clk       : out std_logic := '1'; -- serial-bank-address clock
                                              -- need tri-state buffer at top level; 0=GND, 1=HiZ
      
      -- these signals are valid only for CONFIG="MityDSP-Pro"
      i_irq_map_2x16  : in  bus16_vector(5 downto 4) := (others=>(others=>'0'));     -- DSP_INT4/5
      o_irq_output_2  : out std_logic_vector(5 downto 4); -- DSP_INT4/5
      o_sba_data      : out std_logic; -- direct connection at top-level
      o_sba_clk       : out std_logic; -- direct connection at top-level
      o_led_n         : out std_logic_vector(1 downto 0); -- active-low LED outputs - direct connection at top-level
      i_config        : in  std_logic_vector(2 downto 0) := "000"
      );
end component;

component AD7760
   port ( 
      emif_clk      : in  std_logic;
      i_ABus        : in  std_logic_vector(4 downto 0);
      i_DBus        : in  std_logic_vector(31 downto 0);
      o_DBus        : out std_logic_vector(31 downto 0);
      i_wr_en       : in  std_logic;
      i_rd_en       : in  std_logic;
      i_cs          : in  std_logic;	
      o_irq         : out std_logic;
      i_ilevel      : in  std_logic_vector(1 downto 0) := "00";    -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld  : in  std_logic := '1';                        -- interrupt level valid flag
      i_ivector     : in  std_logic_vector(4 downto 0) := "00000"; -- interrupt vector (0 through 31)
      i_ivector_vld : in  std_logic := '1';                        -- interrupt vector valid flag

      i_clk         : in std_logic;
      i_clk_en      : in std_logic;
      i_trigger     : in std_logic;       -- external trigger
      
      -- AD7760 control ports
      i_adc_rst_n   : in  std_logic;
      o_adc_cs_n    : out std_logic;
      o_adc_rdx_wr  : out std_logic;
      i_adc_drdy_n  : in  std_logic;
      i_adc_dout    : in  std_logic_vector(15 downto 0);
      o_adc_din     : out std_logic_vector(15 downto 0);
      o_adc_tri     : out std_logic;
      
      -- Bus Arbiter control ports
      o_bus_req     : out std_logic;
      i_bus_gnt     : in  std_logic
    );
end component;

component ADC9235
   generic (
      PIPE_DELAY : integer := 8
   );
   port ( 
      clk        : in  std_logic;
      i_ABus     : in  std_logic_vector(4 downto 0);
      i_DBus     : in  std_logic_vector(31 downto 0);
      o_DBus     : out std_logic_vector(31 downto 0);
      i_wr_en    : in  std_logic;
      i_rd_en    : in  std_logic;
      i_cs       : in  std_logic;	
      o_irq      : out std_logic;
      i_ilevel        : in std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld    : in std_logic := '1';                           -- interrupt level valid flag
      i_ivector       : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld   : in std_logic := '1';                           -- interrupt vector valid flag
      i_adcdata  : in  std_logic_vector(11 downto 0);  -- ADC9235 data lines
      i_adcotr   : in  std_logic;                      -- ADC9235 out of range lines
      i_udata    : in  std_logic_vector(4 downto 0);   -- User Data lines
      i_aclk     : in  std_logic;                      -- ADC clock
      i_trigger  : in  std_logic;                      -- external trigger
      i_fifo_re  : in  std_logic;                      -- optional FIFO read enable (for CE 2 access)
      o_fifo_data: out std_logic_vector(31 downto 0)   -- optional FIFO output enable (for CE 2 access)
    );
end component;

component ads7843
   port (
      emif_clk       : in std_logic; -- 50 Mhz EMIF clock
      i_ABus         : in  std_logic_vector(4 downto 0);
      i_DBus         : in  std_logic_vector(31 downto 0);
      o_DBus         : out std_logic_vector(31 downto 0);
      i_wr_en        : in  std_logic;
      i_rd_en        : in  std_logic;
      i_cs           : in  std_logic;	
      o_irq          : out std_logic;
      i_ilevel       : in std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld   : in std_logic := '1';                           -- interrupt level valid flag
      i_ivector      : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld  : in std_logic := '1';                           -- interrupt vector valid flag 

      o_ts_cs_n      :out std_logic;
      o_ts_clk       :out std_logic;
      o_ts_din       :out std_logic;
      i_ts_dout      : in std_logic;
      i_ts_busy      : in std_logic;
      i_ts_PenIRQ_n  : in std_logic;
      o_ts_PenIRQ_n  :out std_logic;
      t_ts_PenIRQ_n  :out std_logic
   );
end component;

component ADS8329
   port ( 
      clk        : in  std_logic;
      i_ABus     : in  std_logic_vector(4 downto 0);
      i_be       : in  std_logic_vector(3 downto 0) := "0000";
      i_DBus     : in  std_logic_vector(31 downto 0);
      o_DBus     : out std_logic_vector(31 downto 0);
      i_wr_en    : in  std_logic;
      i_rd_en    : in  std_logic;
      i_cs       : in  std_logic;	
      o_irq      : out std_logic;
      i_ilevel        : in std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld    : in std_logic := '1';                           -- interrupt level valid flag
      i_ivector       : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld   : in std_logic := '1';                           -- interrupt vector valid flag
      i_adc_clk      : in  std_logic;
      i_udata        : in  std_logic_vector(4 downto 0);   -- User Data lines
      i_trigger      : in  std_logic;                      -- external trigger
      o_adc_convst_n : out std_logic;                      -- adc convert start strobe      
      i_adc_eoc      : in  std_logic;                      -- end of convert signal from ADC (not used)
      o_adc_sclk     : out std_logic;                      -- ADC data shift clock (1/2 EMIF clock)
      o_adc_sdi      : out std_logic;                      -- ADC SPI input line
      i_adc_sdo      : in  std_logic;                      -- ADC SPI output line
      o_adc_cs_n     : out std_logic;                      -- ADC frame sync line      
      i_fifo_re      : in  std_logic;                      -- optional FIFO read enable (for CE 2 access)
      o_fifo_data    : out std_logic_vector(31 downto 0)   -- optional FIFO output enable (for CE 2 access)
    );
end component;						 

component ADS8343
   port (  
      emif_clk      : in  std_logic;
      i_ABus        : in  std_logic_vector(4 downto 0);
      i_DBus        : in  std_logic_vector(31 downto 0);
      o_DBus        : out std_logic_vector(31 downto 0);
      i_wr_en       : in  std_logic;
      i_rd_en       : in  std_logic;
      i_cs          : in  std_logic;	
      o_irq         : out std_logic;
      i_ilevel      : in  std_logic_vector(1 downto 0) := "00";    -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld  : in  std_logic := '1';                        -- interrupt level valid flag
      i_ivector     : in  std_logic_vector(4 downto 0) := "00000"; -- interrupt vector (0 through 31)
      i_ivector_vld : in  std_logic := '1';                        -- interrupt vector valid flag
      i_trigger     : in  std_logic; -- external trigger
      i_clk         : in  std_logic;
      i_clk_en      : in  std_logic;
      o_dclk        : out std_logic; -- ADC clock
      o_adc_din     : out std_logic; -- ADS8343 data input line
      i_adc_dout    : in  std_logic; -- ADS8343 data output line
      i_adc_busy    : in  std_logic; -- ADS8343 busy line     
      i_overlap     : in  std_logic; -- when 1: sample rate = 100kHhz
                                     -- when 0: sample rate = 96kHz
      o_cs_n        : out std_logic  -- ADS8343 chip select (if not used, ensure toggles)
    );
end component;						 

component ADS8344
   port (  
      emif_clk      : in  std_logic;
      i_ABus        : in  std_logic_vector(4 downto 0);
      i_DBus        : in  std_logic_vector(31 downto 0);
      o_DBus        : out std_logic_vector(31 downto 0);
      i_wr_en       : in  std_logic;
      i_rd_en       : in  std_logic;
      i_cs          : in  std_logic;	
      o_irq         : out std_logic;
      i_ilevel      : in  std_logic_vector(1 downto 0) := "00";    -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld  : in  std_logic := '1';                        -- interrupt level valid flag
      i_ivector     : in  std_logic_vector(4 downto 0) := "00000"; -- interrupt vector (0 through 31)
      i_ivector_vld : in  std_logic := '1';                        -- interrupt vector valid flag
      i_trigger     : in  std_logic; -- external trigger
      i_clk         : in  std_logic;
      i_clk_en      : in  std_logic;
      o_dclk        : out std_logic; -- ADC clock
      o_adc_din     : out std_logic; -- ADS8344 data input line
      i_adc_dout    : in  std_logic; -- ADS8344 data output line
      i_adc_busy    : in  std_logic; -- ADS8344 busy line     
      i_overlap     : in  std_logic; -- when 1: sample rate = 100kHhz
                                     -- when 0: sample rate = 96kHz
      o_cs_n        : out std_logic  -- ADS8344 chip select (if not used, ensure toggles)
    );
end component;

component ADS8402
   generic (
      FIFO_DEPTH_TWO_TO_N : integer range 10 to 11 := 10 -- 10 is 1 K words
   );
   port ( 
      clk        : in  std_logic;
      i_ABus     : in  std_logic_vector(4 downto 0);
      i_be       : in  std_logic_vector(3 downto 0) := "0000";
      i_DBus     : in  std_logic_vector(31 downto 0);
      o_DBus     : out std_logic_vector(31 downto 0);
      i_wr_en    : in  std_logic;
      i_rd_en    : in  std_logic;
      i_cs       : in  std_logic;	
      o_irq      : out std_logic;
      i_ilevel        : in std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld    : in std_logic := '1';                           -- interrupt level valid flag
      i_ivector       : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld   : in std_logic := '1';                           -- interrupt vector valid flag
      i_adcdata  : in  std_logic_vector(15 downto 8);  -- ADS8402 upper data bus
      i_udata    : in  std_logic_vector(4 downto 0);   -- User Data lines
      i_aclk     : in  std_logic;                      -- ADC clock
      i_trigger  : in  std_logic;                      -- external trigger
      i_busy     : in  std_logic;                      -- ADS8402 busy
      o_byte     : out std_logic;                      -- ADS8402 byte select strobe
      o_convrt_n : out std_logic;                      -- ADS8402 convert start 
      o_cs_n     : out std_logic;                      -- ADS8402 chip select
      o_rd_n     : out std_logic;                      -- ADS8402 read enable
      o_reset_n  : out std_logic;                      -- ADS8402 reset
      i_fifo_re  : in  std_logic;                      -- optional FIFO read enable (for CE 2 access)
      o_fifo_data: out std_logic_vector(31 downto 0)   -- optional FIFO output enable (for CE 2 access)
    );
end component;						 

component awg
   port ( 
      -- EMIF Interface signals
      emif_clk   : in  std_logic;
      i_ABus     : in  std_logic_vector(4 downto 0);
      i_DBus     : in  std_logic_vector(31 downto 0);
      o_DBus     : out std_logic_vector(31 downto 0);
      i_wr_en    : in  std_logic;
      i_rd_en    : in  std_logic;
      i_cs       : in  std_logic;
      -- IRQ system signals
--      o_irq      : out std_logic;
--      i_ilevel       : in    std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
--      i_ilevel_vld   : in    std_logic := '0';                           -- interrupt level valid flag
--      i_ivector      : in    std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
--      i_ivector_vld  : in    std_logic := '0';                           -- interrupt vector valid flag
      -- AWG signals
      i_clk        : in  std_logic;
      i_sample_en  : in  std_logic;
      i_trigger    : in  std_logic;
      o_phase_roll : out std_logic;
      o_phase      : out std_logic_vector(23 downto 0);
      o_data       : out std_logic_vector(15 downto 0)
    );
end component;

component biquad_iir
   port ( 
      -- EMIF Interface signals
      clk        : in  std_logic; -- emif_clk (system clock) from top-level
      i_ABus     : in  std_logic_vector(4 downto 0);
      i_DBus     : in  std_logic_vector(31 downto 0);
      o_DBus     : out std_logic_vector(31 downto 0);
      i_wr_en    : in  std_logic;
      i_rd_en    : in  std_logic;
      i_cs       : in  std_logic;
      -- IRQ system signals
--      o_irq      : out std_logic;
--      i_ilevel       : in    std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
--      i_ilevel_vld   : in    std_logic := '0';                           -- interrupt level valid flag
--      i_ivector      : in    std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
--      i_ivector_vld  : in    std_logic := '0';                           -- interrupt vector valid flag
      
      -- Core/Applicaion signals
      i_np     : in  std_logic_vector(1 downto 0);  -- Number (of data in) Parallel
      i_init   : in  std_logic;                     -- Initialize the filter
      i_d_clk  : in  std_logic;                     -- Data sample clock/strobe
      i_data   : in  std_logic_vector(31 downto 0); -- Input Data
      o_result : out std_logic_vector(31 downto 0); -- Output Result
      o_rdy    : out std_logic                      -- Output Ready
    );
end component;

component camera_link
   generic (
      FIFO_DEPTH_TWO_TO_N : integer range 9 to 15 := 10; -- 2^10 = 1024 32-bit words
      SUPPORT_DMA         : boolean := true
   );
   port (
      -- MityDSP Core interface
      emif_clk   : in  std_logic;
      i_ABus     : in  std_logic_vector(4 downto 0);
      i_DBus     : in  std_logic_vector(31 downto 0);
      o_DBus     : out std_logic_vector(31 downto 0);
      i_wr_en    : in  std_logic;
      i_rd_en    : in  std_logic;
      i_cs       : in  std_logic;   
      o_irq      : out std_logic;
      i_ilevel      : in std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld  : in std_logic := '1';                           -- interrupt level valid flag
      i_ivector     : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld : in std_logic := '1';                           -- interrupt vector valid flag

      -- CameraLink interface
      o_pwr_down_n : out std_logic;
      i_cam_clk    : in  std_logic;
      i_cam_din    : in  std_logic_vector(27 downto 0);
      o_cc0        : out std_logic;
      o_cc1        : out std_logic;
      o_cc2        : out std_logic;
      o_cc3        : out std_logic;
      o_cc_en      : out std_logic;
      -- FIFO write interface
      o_fifo_wr_clk : out std_logic;   
      o_fifo_wr     : out std_logic;
      o_fifo_din    : out std_logic_vector(31 downto 0); -- (15)=frame-sync (14)=line-sync
      -- FIFO read interface
      o_fifo_rst    : out std_logic;
      o_fifo_rd_clk : out std_logic;
      o_fifo_rd     : out std_logic;
      i_fifo_dout   : in  std_logic_vector(31 downto 0);
      i_fifo_full   : in  std_logic;                                        -- fifo's full flag
      i_fifo_empty  : in  std_logic;                                        -- fifo's empty
      i_fifo_rd_cnt : in  std_logic_vector(FIFO_DEPTH_TWO_TO_N-1 downto 0); -- fifo's read count
        
      -- MityDSP EMIF DMA interface (not available for MityDSP-PRO designs)
      o_dma_req          : out std_logic := '0';
      o_dma_priority_req : out std_logic := '0';
      o_dma_rwn          : out std_logic := '0';
      o_dma_addr         : out std_logic_vector(31 downto 0) := (others=>'0');
      o_dma_data         : out std_logic_vector(31 downto 0) := (others=>'0');
      o_dma_count        : out std_logic_vector(8 downto 0) := (others=>'0');
      o_dma_be           : out std_logic_vector(3 downto 0) := "1111";      
      i_dma_wr_stb       : in std_logic := '0';
      i_dma_rd_stb       : in std_logic := '0';
      i_dma_data         : in std_logic_vector(31 downto 0) := (others=>'0');
      i_dma_done         : in std_logic := '0'
   );
end component;

component cl_eth_671x
   generic (
      NUM_MAC_FILTERS      : integer range 1 to 16 := 8;
      USE_INPUT_FILTERING  : boolean               := FALSE;
      USE_OUTPUT_FILTERING : boolean               := FALSE 
   );
   port (
      emif_clk    : in std_logic; -- global buffered EMIF clock
      clk25       : in std_logic; -- global buffered 25MHz clock

      o_ref_clk   :out std_logic; -- 25MHz reference clock for some PHYs
      o_clk2_5    :out std_logic; -- 2.5MHz clock for TX Engine, when using CS8952
      tx_clk      : in std_logic; -- global buffered transmit clock
      rx_clk      : in std_logic; -- global buffered receive clock

      ABus        : in std_logic_vector(4 downto 0);
      iDBus       : in std_logic_vector(31 downto 0);
      oDBus       :out std_logic_vector(31 downto 0);
      wr_en       : in std_logic;
      rd_en       : in std_logic;
      eth_cs      : in std_logic;   --sync cs for eth addr range

      o_mdc       :out std_logic;
      o_mdio      :out std_logic;
      i_mdio      : in std_logic := '0';
      t_mdio      :out std_logic; -- tri-state the MDIO line

      o_tx_spd_100   :out std_logic;
      i_half_duplex  : in std_logic := '0';

      i_col		   : in std_logic := '0';
      i_crs		   : in std_logic := '0';
      o_tx_data	:out std_logic_vector(3 downto 0);
      o_tx_en     :out std_logic;
      o_tx_er     :out std_logic;
      o_rx_en     :out std_logic;
      i_rx_data   : in std_logic_vector(3 downto 0);
      i_rx_dv     : in std_logic;
      i_rx_er     : in std_logic;

      o_eth_rst   :out std_logic;
      int_rxpkt   :out std_logic;

      i_ilevel       : in std_logic_vector(1 downto 0) := "01";      -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld   : in std_logic := '1';                          -- interrupt level valid flag
      i_ivector      : in std_logic_vector(4 downto 0) := "00000";   -- interrupt vector (0 through 31)
      i_ivector_vld  : in std_logic := '1';                          -- interrupt vector valid flag
      
      -- The following ports are only required if USE_INPUT_FILTERING = true
      o_rcv_pkt_data : out std_logic_vector(7 downto 0) := x"00";
      o_rcv_pkt_we   : out std_logic := '0';
      o_rcv_pkt_addr : out std_logic_vector(10 downto 0) := (others=>'0');
      o_rcv_pkt_clk  : out std_logic := '0';
      i_rcv_pkt_data : in  std_logic_vector(7 downto 0) := x"00";
      i_rcv_pkt_we   : in  std_logic := '0';
      i_rcv_pkt_addr : in  std_logic_vector(10 downto 0) := (others=>'0');
      
      -- The following ports are only required if USE_OUTPUT_FILTERING = true
      o_xmit_pkt_data : out std_logic_vector(7 downto 0) := x"00";
      o_xmit_nib_cnt  : out std_logic_vector(12 downto 0) := "0000000000000";
      o_xmit_pkt_clk  : out std_logic := '0';
      i_xmit_pkt_data : in  std_logic_vector(7 downto 0) := x"00";
      i_xmit_nib_cnt  : in  std_logic_vector(12 downto 0) := "0000000000000"
   );
end component;

component cl_eth_671x_min
   port (
      emif_clk    : in std_logic; -- global buffered EMIF clock
      clk25       : in std_logic; -- global buffered 25MHz clock

      o_ref_clk   :out std_logic; -- 25MHz reference clock for some PHYs
      o_clk2_5    :out std_logic; -- 2.5MHz clock for TX Engine, when using CS8952
      tx_clk      : in std_logic; -- global buffered transmit clock
      rx_clk      : in std_logic; -- global buffered receive clock

      ABus        : in std_logic_vector(4 downto 0);
      iDBus       : in std_logic_vector(31 downto 0);
      oDBus       :out std_logic_vector(31 downto 0);
      wr_en       : in std_logic;
      rd_en       : in std_logic;
      eth_cs      : in std_logic;   --sync cs for eth addr range

      o_mdc       :out std_logic;
      o_mdio      :out std_logic;
      i_mdio      : in std_logic := '0';
      t_mdio      :out std_logic; -- tri-state the MDIO line

      o_tx_spd_100   :out std_logic;
      i_half_duplex  : in std_logic := '0';

      i_col		   : in std_logic := '0';
      i_crs		   : in std_logic := '0';
      o_tx_data	:out std_logic_vector(3 downto 0);
      o_tx_en     :out std_logic;
      o_tx_er     :out std_logic;
      o_rx_en     :out std_logic;
      i_rx_data   : in std_logic_vector(3 downto 0);
      i_rx_dv     : in std_logic;
      i_rx_er     : in std_logic;

      o_eth_rst   :out std_logic;
      int_rxpkt   :out std_logic;

      i_ilevel       : in std_logic_vector(1 downto 0) := "01";      -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld   : in std_logic := '1';                          -- interrupt level valid flag
      i_ivector      : in std_logic_vector(4 downto 0) := "00000";   -- interrupt vector (0 through 31)
      i_ivector_vld  : in std_logic := '1'                           -- interrupt vector valid flag
   );
end component;

component cl_eth_671x_wfiltering
   port (
      emif_clk    : in std_logic; -- global buffered EMIF clock
      clk25       : in std_logic; -- global buffered 25MHz clock

      o_ref_clk   :out std_logic; -- 25MHz reference clock for some PHYs
      o_clk2_5    :out std_logic; -- 2.5MHz clock for TX Engine, when using CS8952
      tx_clk      : in std_logic; -- global buffered transmit clock
      rx_clk      : in std_logic; -- global buffered receive clock

      ABus        : in std_logic_vector(4 downto 0);
      iDBus       : in std_logic_vector(31 downto 0);
      oDBus       :out std_logic_vector(31 downto 0);
      wr_en       : in std_logic;
      rd_en       : in std_logic;
      eth_cs      : in std_logic;   --sync cs for eth addr range

      o_mdc       :out std_logic;
      o_mdio      :out std_logic;
      i_mdio      : in std_logic := '0';
      t_mdio      :out std_logic; -- tri-state the MDIO line

      o_tx_spd_100   :out std_logic;
      i_half_duplex  : in std_logic := '0';

      i_col		   : in std_logic := '0';
      i_crs		   : in std_logic := '0';
      o_tx_data	:out std_logic_vector(3 downto 0);
      o_tx_en     :out std_logic;
      o_tx_er     :out std_logic;
      o_rx_en     :out std_logic;
      i_rx_data   : in std_logic_vector(3 downto 0);
      i_rx_dv     : in std_logic;
      i_rx_er     : in std_logic;

      o_eth_rst   :out std_logic;
      int_rxpkt   :out std_logic;

      i_ilevel       : in std_logic_vector(1 downto 0) := "01";      -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld   : in std_logic := '1';                          -- interrupt level valid flag
      i_ivector      : in std_logic_vector(4 downto 0) := "00000";   -- interrupt vector (0 through 31)
      i_ivector_vld  : in std_logic := '1';                          -- interrupt vector valid flag

      o_rcv_pkt_data : out std_logic_vector(7 downto 0);
      o_rcv_pkt_we   : out std_logic;
      o_rcv_pkt_addr : out std_logic_vector(10 downto 0);
      o_rcv_pkt_clk  : out std_logic;
      i_rcv_pkt_data : in  std_logic_vector(7 downto 0);
      i_rcv_pkt_we   : in  std_logic;
      i_rcv_pkt_addr : in  std_logic_vector(10 downto 0);
      
      o_xmit_pkt_data : out std_logic_vector(7 downto 0);
      o_xmit_nib_cnt  : out std_logic_vector(12 downto 0);
      o_xmit_pkt_clk  : out std_logic;
      i_xmit_pkt_data : in  std_logic_vector(7 downto 0);
      i_xmit_nib_cnt  : in  std_logic_vector(12 downto 0)
   );
end component;

component clock1305_mity
   port (
      emif_clk       : in std_logic; -- global buffered EMIF clock

      i_ABus         : in  std_logic_vector(4 downto 0);
      i_DBus         : in  std_logic_vector(31 downto 0);
      o_DBus         : out std_logic_vector(31 downto 0);
      i_wr_en        : in  std_logic;
      i_rd_en        : in  std_logic;
      i_cs           : in  std_logic;
      o_irq          : out std_logic;

      i_ilevel       : in std_logic_vector(1 downto 0) := "01";      -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld   : in std_logic := '1';                          -- interrupt level valid flag
      i_ivector      : in std_logic_vector(4 downto 0) := "00000";   -- interrupt vector (0 through 31)
      i_ivector_vld  : in std_logic := '1';                          -- interrupt vector valid flag
      
      o_clock_secs   : out std_logic_vector(31 downto 0); -- for use in other cores
      o_clock_nsecs  : out std_logic_vector(31 downto 0); -- for use in other cores

      o_clk_1sec     : out std_logic; -- debug/test 
      o_clk_1ms      : out std_logic; -- debug/test
      o_clk_1us      : out std_logic  -- debug/test 
   );
end component;

component clock1305_mitypro
   port (
      emif_clk       : in std_logic; -- global buffered EMIF clock

      i_ABus         : in  std_logic_vector(4 downto 0);
      i_DBus         : in  std_logic_vector(31 downto 0);
      o_DBus         : out std_logic_vector(31 downto 0);
      i_wr_en        : in  std_logic;
      i_rd_en        : in  std_logic;
      i_cs           : in  std_logic;
      o_irq          : out std_logic;

      i_ilevel       : in std_logic_vector(1 downto 0) := "01";      -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld   : in std_logic := '1';                          -- interrupt level valid flag
      i_ivector      : in std_logic_vector(4 downto 0) := "00000";   -- interrupt vector (0 through 31)
      i_ivector_vld  : in std_logic := '1';                          -- interrupt vector valid flag
      
      o_clock_secs   : out std_logic_vector(31 downto 0); -- for use in other cores
      o_clock_nsecs  : out std_logic_vector(31 downto 0); -- for use in other cores

      o_clk_1sec     : out std_logic; -- debug/test 
      o_clk_1ms      : out std_logic; -- debug/test
      o_clk_1us      : out std_logic  -- debug/test 
   );
end component;

component event_counter
   port (  
      emif_clk        : in  std_logic;
      i_ABus          : in  std_logic_vector(4 downto 0);
      i_DBus          : in  std_logic_vector(31 downto 0);
      o_DBus          : out std_logic_vector(31 downto 0);
      i_wr_en         : in  std_logic;
      i_rd_en         : in  std_logic;
      i_cs            : in  std_logic;	
      o_irq           : out std_logic;
      i_ilevel        : in std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld    : in std_logic := '1';                           -- interrupt level valid flag
      i_ivector       : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld   : in std_logic := '1';                           -- interrupt vector valid flag 
      i_trigger       : in std_logic_vector(15 downto 0)               -- trigger input
    );                          
end component;

component ez_usb
   port ( 
      -- EMIF Interface signals
      emif_clk   : in  std_logic;
      i_clk      : in  std_logic;                     -- 2 x IFCLK
      i_ABus     : in  std_logic_vector(4 downto 0);
      i_DBus     : in  std_logic_vector(31 downto 0);
      o_DBus     : out std_logic_vector(31 downto 0);
      i_wr_en    : in  std_logic;
      i_rd_en    : in  std_logic;
      i_cs       : in  std_logic;   
      i_be       : in  std_logic_vector(3 downto 0);       
      -- IRQ system signals
      o_irq         : out std_logic;
      i_ilevel      : in std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld  : in std_logic := '1';                           -- interrupt level valid flag
      i_ivector     : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld : in std_logic := '1';                           -- interrupt vector valid flag      
      -- EZ USB signals
      io_fd      : inout std_logic_vector(7 downto 0);
      o_ifclk    : out std_logic;
      o_slrd     : out std_logic;
      o_slwr     : out std_logic;
      o_sloe     : out std_logic;
      o_slcs_n   : out std_logic;
      i_flaga    : in  std_logic;
      i_flagb    : in  std_logic;
      i_flagc    : in  std_logic;
      o_fifoadr0 : out std_logic;
      o_fifoadr1 : out std_logic;
      o_pktend   : out std_logic
    );
end component;

component gpin
   generic (
      NUM_IO : integer := 32 
   );
   port ( 
      clk        : in  std_logic;
      i_ABus     : in  std_logic_vector(4 downto 0);
      i_DBus     : in  std_logic_vector(31 downto 0);
      o_DBus     : out std_logic_vector(31 downto 0);
      i_wr_en    : in  std_logic;
      i_rd_en    : in  std_logic;
      i_cs       : in  std_logic;	
      o_irq      : out std_logic;
      i_ilevel       : in    std_logic_vector(1 downto 0) := "01";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld   : in    std_logic := '0';                           -- interrupt level valid flag
      i_ivector      : in    std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld  : in    std_logic := '0';                           -- interrupt vector valid flag
      i_port     : in  std_logic_vector(NUM_IO-1 downto 0)
    );
end component;						 

component gpio
   generic (
      NUM_IO : integer := 32;
      GPIO_DIR_OUT   : std_logic_vector(31 downto 0) := (others=>'0')
   );
   port ( 
      clk        : in  std_logic;
      i_ABus     : in  std_logic_vector(4 downto 0);
      i_DBus     : in  std_logic_vector(31 downto 0);
      o_DBus     : out std_logic_vector(31 downto 0);
      i_wr_en    : in  std_logic;
      i_rd_en    : in  std_logic;
      i_cs       : in  std_logic;	
      o_irq      : out std_logic;
      i_ilevel       : in    std_logic_vector(1 downto 0) := "01";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld   : in    std_logic := '0';                           -- interrupt level valid flag
      i_ivector      : in    std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld  : in    std_logic := '0';                           -- interrupt vector valid flag
      i_port     : in  std_logic_vector(NUM_IO-1 downto 0);
      o_port     : out std_logic_vector(NUM_IO-1 downto 0);
      t_port     : out std_logic_vector(NUM_IO-1 downto 0)   -- logic '1' when tristate output should be set
    );
end component;						 

component gpout
   generic (
      NUM_IO : integer := 32 
   );
   port ( 
      clk        : in  std_logic;
      i_ABus     : in  std_logic_vector(4 downto 0);
      i_DBus     : in  std_logic_vector(31 downto 0);
      o_DBus     : out std_logic_vector(31 downto 0);
      i_wr_en    : in  std_logic;
      i_rd_en    : in  std_logic;
      i_cs       : in  std_logic;	
      i_ilevel       : in    std_logic_vector(1 downto 0) := "01";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld   : in    std_logic := '0';                           -- interrupt level valid flag
      i_ivector      : in    std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld  : in    std_logic := '0';                           -- interrupt vector valid flag
      o_port     : out std_logic_vector(NUM_IO-1 downto 0)
    );
end component;						 

component i2c
   generic (
      CLK_DIVIDE : integer := 256;        -- ~200KHz from 50MHz
      BYTES_PER_PAGE : integer := 32;     --Bytes per page write
      WRITE_DLY_CNT_BIT : integer := 10   --Twr Page Write Delay = (2^n)*scl_period
   );
   port ( 
      clk      : in std_logic;
      i_ABus   : in std_logic_vector(4 downto 0);
      i_DBus   : in std_logic_vector(31 downto 0);
      o_DBus   :out std_logic_vector(31 downto 0);
      i_wr_en  : in std_logic;
      i_rd_en  : in std_logic;
      i_cs     : in std_logic;	
      o_irq    :out std_logic;
      i_ilevel       : in std_logic_vector(1 downto 0) := "01";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld   : in std_logic := '0';                           -- interrupt level valid flag
      i_ivector      : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld  : in std_logic := '0';                           -- interrupt vector valid flag
      i_sda    : in std_logic;
      o_sda    :out std_logic;
      o_sdt    :out std_logic;
      o_scl    :out std_logic
    );
end component;

component LVDS_Link1x1
   port (  
      emif_clk      : in  std_logic;
      i_ABus        : in  std_logic_vector(4 downto 0);
      i_DBus        : in  std_logic_vector(31 downto 0);
      o_DBus        : out std_logic_vector(31 downto 0);
      i_wr_en       : in  std_logic;
      i_rd_en       : in  std_logic;
      i_cs          : in  std_logic;	
      o_irq         : out std_logic;
      i_ilevel      : in  std_logic_vector(1 downto 0) := "00";    -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld  : in  std_logic := '1';                        -- interrupt level valid flag
      i_ivector     : in  std_logic_vector(4 downto 0) := "00000"; -- interrupt vector (0 through 31)
      i_ivector_vld : in  std_logic := '1';                        -- interrupt vector valid flag
      i_tx_clk      : in  std_logic;                               -- output clock source
      o_link_tx_clk : out std_logic;                               -- LVDS output link clock (route to LVDS driver pair)
      o_link_data   : out std_logic;                               -- LVDS output data clock (route to LVDS driver pair)
      i_rx_clk      : in  std_logic;                               -- LVDS input  data clock (route to LVDS driver pair)
      i_rx_data     : in  std_logic                                -- LVDS input  data clock (route to LVDS driver pair)
    );                          
end component;

component MicroSD
   port ( 
      clk          : in  std_logic;
      i_ABus       : in  std_logic_vector(4 downto 0);
      i_DBus       : in  std_logic_vector(31 downto 0);
      o_DBus_SPI   : out std_logic_vector(31 downto 0);
      i_cs_SPI     : in  std_logic;	
      o_DBus_GPOUT : out std_logic_vector(31 downto 0);
      i_cs_GPOUT   : in  std_logic;	
      i_wr_en      : in  std_logic;
      i_rd_en      : in  std_logic;
      o_irq        : out std_logic;
      i_ilevel       : in    std_logic_vector(1 downto 0) := "01"; 
      i_ivector      : in    std_logic_vector(4 downto 0) := "00000";
      o_mmc_cs_n     : out   std_logic := '0';
      o_mmc_clk      : out   std_logic := '0';
      o_mmc_di       : out   std_logic := '0';
      i_mmc_do       : in    std_logic := '0'
    );
end component;

component pulseintegrator
   port ( 
      clk        : in  std_logic;
      i_ABus     : in  std_logic_vector(4 downto 0);
      i_be       : in  std_logic_vector(3 downto 0) := "0000";
      i_DBus     : in  std_logic_vector(31 downto 0);
      o_DBus     : out std_logic_vector(31 downto 0);
      i_wr_en    : in  std_logic;
      i_rd_en    : in  std_logic;
      i_cs       : in  std_logic;	
      o_irq      : out std_logic;
      i_ilevel        : in std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld    : in std_logic := '1';                           -- interrupt level valid flag
      i_ivector       : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld   : in std_logic := '1';                           -- interrupt vector valid flag
      o_pulseoutput   : out std_logic;  -- active low output
      i_detect        : in std_logic_vector(3 downto 0);
      i_measenable    : in std_logic_vector(3 downto 0)
    );
end component;

component pwm
   port ( 
      clk        : in  std_logic;                                        -- emif clock
      i_ABus     : in  std_logic_vector(4 downto 0);                     -- emif core address bus
      i_DBus     : in  std_logic_vector(31 downto 0);                    -- emif data bus
      o_DBus     : out std_logic_vector(31 downto 0);                    -- emif output data bus
      i_wr_en    : in  std_logic;                                        -- emif write enable
      i_rd_en    : in  std_logic;                                        -- emif read enable
      i_cs       : in  std_logic;	                                     -- emif core select
      o_irq      : out std_logic;                                        -- IRQ output
      i_ilevel       : in    std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld   : in    std_logic := '0';                           -- interrupt level valid flag
      i_ivector      : in    std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld  : in    std_logic := '0';                           -- interrupt vector valid flag
      o_pulse        : out   std_logic_vector(4 downto 0);               -- pulse output vector
      i_trigger      : in    std_logic;                                  -- pulse trigger
      i_aclk         : in    std_logic;                                  -- reference clock
      i_wd_stop      : in    std_logic := '0'                            -- watch dog stop signal (not latched)
    );
end component;

component spit
   port (  
      emif_clk   : in  std_logic;
      i_ABus     : in  std_logic_vector(4 downto 0);
      i_DBus     : in  std_logic_vector(31 downto 0);
      o_DBus     : out std_logic_vector(31 downto 0);
      i_wr_en    : in  std_logic;
      i_rd_en    : in  std_logic;
      i_cs       : in  std_logic;	
      o_irq      : out std_logic;
      i_ilevel        : in std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld    : in std_logic := '1';                           -- interrupt level valid flag
      i_ivector       : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld   : in std_logic := '1';                           -- interrupt vector valid flag
      i_clk      : in  std_logic;
      o_sclk     : out std_logic;                      -- SPI output clock
      o_sync     : out std_logic;                      -- SYNC output 
      o_mosi     : out std_logic;                      -- data output 
      i_miso     : in  std_logic                       -- data input 
    );                          
end component;

component STEPPER3967
   port ( 
      clk        : in std_logic;
      base_clk   : in std_logic;
      i_ABus     : in std_logic_vector(4 downto 0);
      i_DBus     : in std_logic_vector(31 downto 0);
      o_DBus     :out std_logic_vector(31 downto 0);
      i_wr_en    : in std_logic;
      i_rd_en    : in std_logic;
      i_cs       : in std_logic;
      o_irq      :out std_logic;
      i_ilevel      : in std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld  : in std_logic := '1';                           -- interrupt level valid flag
      i_ivector     : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld : in std_logic := '1';                           -- interrupt vector valid flag

      i_ts          : in std_logic_vector(6 downto 0);
      i_estop       : in std_logic := '0';
      i_ms          : in std_logic_vector(1 downto 0) ;   --Can Hard Code Setting
      o_ms          :out std_logic_vector(1 downto 0);  --For SW Control
      i_ms_vld      : in std_logic := '0';                --MS Bits Are Meaningful

      o_reset_n     :out std_logic;
      o_enable_n    :out std_logic;
      o_sleep_n     :out std_logic;
      o_dir         :out std_logic;
      o_step        :out std_logic;
      i_hi_current  : in std_logic_vector(1 downto 0);     --Can Hard Code the bit[s]
      o_hi_current  :out std_logic_vector(1 downto 0);     --For sW Control
      i_hi_curr_vld : in std_logic   --SW Control Available
    );
end component;

component timing_gen
   Port ( 
      -- EMIF Interface signals
      i_emif_clk : in  std_logic; -- emif_clk (system clock) from top-level
      i_ABus     : in  std_logic_vector(4 downto 0);
      i_DBus     : in  std_logic_vector(31 downto 0);
      o_DBus     : out std_logic_vector(31 downto 0);
      i_wr_en    : in  std_logic;
      i_rd_en    : in  std_logic;
      i_cs       : in  std_logic;
      -- IRQ system signals
      o_irq      : out std_logic;
      i_ilevel       : in    std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld   : in    std_logic := '1';                           -- interrupt level valid flag
      i_ivector      : in    std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld  : in    std_logic := '1';                           -- interrupt vector valid flag
		-- Timing Generator I/O 
      i_clk      : in  std_logic;
      i_clk_en   : in  std_logic;
      o_output   : out std_logic_vector(15 downto 0)
    );
end component;

component uart
   port ( 
      clk 	         : in std_logic;
      i_rcv          : in std_logic;
      o_xmit         : out std_logic;
      o_irq		     : out std_logic;
      i_ilevel       : in std_logic_vector(1 downto 0) := "10";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld   : in std_logic := '1';                           -- interrupt level valid flag
      i_ivector      : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld  : in std_logic := '1';                           -- interrupt vector valid flag
      i_ABus	     : in std_logic_vector(4 downto 0);
      i_be           : in std_logic_vector(3 downto 0);
      i_DBus	     : in std_logic_vector(31 downto 0);
      o_DBus	     : out std_logic_vector(31 downto 0);
      i_wr_en	     : in std_logic;
      i_rd_en	     : in std_logic;
      i_uart_cs	     : in std_logic;	
      i_xmit_cts     : in std_logic := '0';   -- active low
      o_rcv_cts      : out std_logic;         -- active low
      i_dsr          : in std_logic := '0';   -- active low
      o_dtr          : out std_logic;         -- active low
      i_dcd          : in std_logic := '0';   -- active low
      i_ri           : in std_logic := '0';   -- active low
      o_xmit_enb     : out std_logic          -- transmit enable
   );
end component;

-- Declare functions and procedure

--  function <function_name>  (signal <signal_name> : in <type_declaration>) return <type_declaration>;
--  procedure <procedure_name>	(<type_declaration> <constant_name>	: in <type_declaration>);

end MityDSP_pkg;


package body MityDSP_pkg is

-- Example 1
--  function <function_name>  (signal <signal_name> : in <type_declaration>  ) return <type_declaration> is
--    variable <variable_name>     : <type_declaration>;
--  begin
--    <variable_name> := <signal_name> xor <signal_name>);
--    return <variable_name>; 
--  end <function_name>;


-- Example 2
--  function <function_name>  (signal <signal_name> : in <type_declaration>;
--                         signal <signal_name>   : in <type_declaration>  ) return <type_declaration> is
--  begin
--    if (<signal_name> = '1') then
--      return <signal_name>;
--    else
--      return 'Z';
--    end if;
--  end <function_name>;

-- Procedure Example
--  procedure <procedure_name>  (<type_declaration> <constant_name>  : in <type_declaration>) is
--    
--  begin
--    
--  end <procedure_name>;
 
end MityDSP_pkg;
