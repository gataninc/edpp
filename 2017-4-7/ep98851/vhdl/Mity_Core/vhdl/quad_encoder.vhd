--- Title: quad_encoder.vhd
--- Description: Implementation of Quadrature Encoder for NDC 
---
---     o  0
---     | /       Copyright (c) 2006
---    (CL)---o   Critical Link, LLC
---      \
---       O
---
--- Company: Critical Link, LLC.
--- Date: 02/16/2006
--- Version: 1.00
--- Revisions: 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity quad_encoder is
   Port 
   (  
      emif_clk   : in  std_logic; 
      i_phaseA   : in  std_logic;
      i_phaseB   : in  std_logic;
      o_cw       : out std_logic;
      o_ccw      : out std_logic
    );                          
end quad_encoder;						 

architecture rtl of quad_encoder is

signal phaseA_r1, phaseA_r2 : std_logic := '0';
signal phaseB_r1, phaseB_r2 : std_logic := '0';
signal reset : std_logic;
signal init_cnt : std_logic_vector(2 downto 0) := (others => '0');

type QUAD_STATES is (
      INIT,            -- load flip-flops
      ARMED,           -- Waiting for edge on phase A
      WAIT_EDGE        -- waiting for rising edge on phase B
      );
signal sm_state_cw  : QUAD_STATES := ARMED;
signal sm_state_ccw : QUAD_STATES := ARMED;


begin   

reset_on_cfg: roc port map (O => reset);

encode_cw : process(emif_clk, reset)
begin

   if (reset = '1') then
      o_cw  <= '0';
      o_ccw <= '0';
      init_cnt <= (others=>'0');
      sm_state_cw <= INIT;
      sm_state_ccw <= INIT;
            
   elsif (emif_clk'event and emif_clk = '1') then
	  phaseA_r1 <= i_phaseA;  
	  phaseA_r2 <= phaseA_r1;
	  phaseB_r1 <= i_phaseB;  
	  phaseB_r2 <= phaseB_r1;
 
      o_cw  <= '0';
      o_ccw <= '0';
 
      case sm_state_cw is
          when INIT =>
             init_cnt <= init_cnt+'1';
             if init_cnt="111" then
                sm_state_cw <= ARMED;
             end if;
          
          when ARMED =>
             if phaseA_r2 /= phaseA_r1 then
                sm_state_cw <= WAIT_EDGE;
             end if;
             
          -- wait for an edge on B to re-arm for next assessment
          when WAIT_EDGE =>
             if phaseB_r2 /= phaseB_r1 then
                if phaseB_r1 = phaseA_r1 then
                   o_cw <= '1';
                end if;
                sm_state_cw <= ARMED;
             end if;
                      
          when others => NULL;
          
       end case;      

      case sm_state_ccw is
          
          when INIT =>
             if init_cnt="111" then
                sm_state_ccw <= ARMED;
             end if;

          when ARMED =>
             if phaseB_r2 /= phaseB_r1 then
                sm_state_ccw <= WAIT_EDGE;
             end if;
             
          -- wait for an edge on B to re-arm for next assessment
          when WAIT_EDGE =>
             if phaseA_r2 /= phaseA_r1 then
                if phaseB_r1 = phaseA_r1 then
                   o_ccw <= '1';
                end if;
                sm_state_ccw <= ARMED;
             end if;
                      
          when others => NULL;
          
       end case;      

      
   end if;         
          
end process encode_cw;

end rtl;


