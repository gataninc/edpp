--- Title: event_counter.vhd
--- Description: Implementation of Event Counter Core for NDC 
---
---     o  0
---     | /       Copyright (c) 2006
---    (CL)---o   Critical Link, LLC
---      \
---       O
---
--- Company: Critical Link, LLC.
--- Date: 02/16/2006
--- Version: 1.00
--- Revisions: 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity event_counter is
   Port 
   (  
      emif_clk        : in  std_logic;
      i_ABus          : in  std_logic_vector(4 downto 0);
      i_DBus          : in  std_logic_vector(31 downto 0);
      o_DBus          : out std_logic_vector(31 downto 0);
      i_wr_en         : in  std_logic;
      i_rd_en         : in  std_logic;
      i_cs            : in  std_logic;	
      o_irq           : out std_logic;
      i_ilevel        : in std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld    : in std_logic := '1';                           -- interrupt level valid flag
      i_ivector       : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld   : in std_logic := '1';                           -- interrupt vector valid flag 
      i_trigger       : in std_logic_vector(15 downto 0)               -- trigger input
    );                          
end event_counter;						 
                                                                                                                                                        