--- Title: biquad_iir.vhd
--- Description: Biquad Infinite Impulse Response (IIR) Core.  Offers up to four 
---              configurable IIR filters to be used in cascade or to process 
---              up to four seperate data inputs in parallel.  Will utilize 
---              a microcode control signals table to control dataflow.  Each
---              IIR filter has a latency of 10 clock cycles. 
---
---
---     o  0                          
---     | /       Copyright (c) 2006  
---    (CL)---o   Critical Link, LLC  
---      \                            
---       O                           
---
--- Company: Critical Link, LLC
--- Date: 09/18/2006
--- Version: 
---   1.00 - 07/04/2006 - Initial Version
---   1.01 - 09/06/2006 - IIR now normalized to 16384 (0x4000) allowing coefficients to range
---                       from +/- 2.  MAC Overflow Detection also added.  Write a '1' to csr(4)
---                       to clear overflow flag.  Also fixed a few minor bugs.
---   1.02 - 09/09/2006 - Expanded input/output ports to 32-bits.  Expanded coefficients to 18-bits.
---   1.03 - 09/15/2006 - Added initialization input.  Also streamlined the control ROM & logic.
---   1.04 - 09/18/2006 - IIR now initializes with input data instead of forced zeroes.
---   

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library Unisim;
use Unisim.Vcomponents.all;

entity biquad_iir is
   Port ( 
      -- EMIF Interface signals
      clk        : in  std_logic; -- emif_clk (system clock) from top-level
      i_ABus     : in  std_logic_vector(4 downto 0);
      i_DBus     : in  std_logic_vector(31 downto 0);
      o_DBus     : out std_logic_vector(31 downto 0);
      i_wr_en    : in  std_logic;
      i_rd_en    : in  std_logic;
      i_cs       : in  std_logic;
      -- IRQ system signals
--      o_irq      : out std_logic;
--      i_ilevel       : in    std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
--      i_ilevel_vld   : in    std_logic := '0';                           -- interrupt level valid flag
--      i_ivector      : in    std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
--      i_ivector_vld  : in    std_logic := '0';                           -- interrupt vector valid flag
      
      -- Core/Applicaion signals
      i_np     : in  std_logic_vector(1 downto 0);  -- Number (of data in) Parallel
      i_init   : in  std_logic;                     -- Initialize the filter
      i_d_clk  : in  std_logic;                     -- Data sample clock/strobe
      i_data   : in  std_logic_vector(31 downto 0); -- Input Data
      o_result : out std_logic_vector(31 downto 0); -- Output Result
      o_rdy    : out std_logic                      -- Output Ready

    );
end biquad_iir;
