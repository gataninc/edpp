library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity ADC9235 is
   generic
   (
      PIPE_DELAY : integer := 8
   );
   Port 
   ( 
      clk        : in  std_logic;
      i_ABus     : in  std_logic_vector(4 downto 0);
      i_DBus     : in  std_logic_vector(31 downto 0);
      o_DBus     : out std_logic_vector(31 downto 0);
      i_wr_en    : in  std_logic;
      i_rd_en    : in  std_logic;
      i_cs       : in  std_logic;	
      o_irq      : out std_logic;
      i_ilevel        : in std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld    : in std_logic := '1';                           -- interrupt level valid flag
      i_ivector       : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld   : in std_logic := '1';                           -- interrupt vector valid flag
      i_adcdata  : in  std_logic_vector(11 downto 0);  -- ADC9235 data lines
      i_adcotr   : in  std_logic;                      -- ADC9235 out of range lines
      i_udata    : in  std_logic_vector(4 downto 0);   -- User Data lines
      i_aclk     : in  std_logic;                      -- ADC clock
      i_trigger  : in  std_logic;                      -- external trigger
      i_fifo_re  : in  std_logic;                      -- optional FIFO read enable (for CE 2 access)
      o_fifo_data: out std_logic_vector(31 downto 0)   -- optional FIFO output enable (for CE 2 access)
    );
end ADC9235;	