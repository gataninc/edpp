--- Title: ez_usb_ext.vhd
--- Description: EZ-USB interface core module, extensible with external FIFOs.
---
---
---     o  0                          
---     | /       Copyright (c) 2008-2009  
---    (CL)---o   Critical Link, LLC  
---      \                            
---       O                           
---
--- Company: Critical Link, LLC
--- Date: 05/13/2009
--- Revision History: 
---   1.00 - 06/23/2008 - ZNT - Initial Version
---   1.01 -
---   1.02 - 
---   1.03 - 03/09/2009 - MAP - Rewrote main state machine from scratch.
---                             The byte_enable module is no longer needed.
---   1.04 - 03/31/2009 - MAP - Split the io_fd port into i_, o_, and t_ ports.
---   1.05 - 05/07/2009 - MAW - Add DCM reset support for external clock sources.
---   1.06 - 05/13/2009 - MAP - Internal ifclk now generated in sync with external IFCLK.
---                             This requires use of DCM external to this core.
---                             Also, RX-full detection now uses Almost-Full flag.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library Unisim;
use Unisim.Vcomponents.all;

entity ez_usb is
   Port ( 
      i_clk      : in  std_logic; -- 2x IFCLK @ 0   deg. phase via DCM - BUFG routing  - 60MHz
      i_ifclk270 : in  std_logic; -- 1x IFCLK @ 270 deg. phase via DCM - local routing - 30MHz

      -- I/O circuitry required for DCM reset management
      i_clk25mhz : in  std_logic;  -- 25 connected to basemodule 
      i_clken4ms : in  std_logic;  -- from basemodule
      i_dcm_used : in  std_logic := '0'; -- set to '1' if external DCM is used for USB clock
      i_dcm_lock : in  std_logic := '0'; -- pass in dcm lock state
      i_dcm_status : in std_logic_vector(2 downto 0) := "000"; -- pass in bottom 3 bits of dcm status
      o_dcm_reset: out  std_logic := '0'; -- connect to DCM reset line

      -- EMIF Interface signals
      emif_clk   : in  std_logic;
      i_ABus     : in  std_logic_vector(4 downto 0);
      i_DBus     : in  std_logic_vector(31 downto 0);
      o_DBus     : out std_logic_vector(31 downto 0);
      i_wr_en    : in  std_logic;
      i_rd_en    : in  std_logic;
      i_cs       : in  std_logic;   
      i_be       : in  std_logic_vector(3 downto 0);       
      -- IRQ system signals
      o_irq         : out std_logic;
      i_ilevel      : in std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld  : in std_logic := '1';                           -- interrupt level valid flag
      i_ivector     : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld : in std_logic := '1';                           -- interrupt vector valid flag      

      -- EZ USB signals
      i_fd       : in  std_logic_vector(7 downto 0);
      o_fd       : out std_logic_vector(7 downto 0);
      t_fd       : out std_logic; -- add IOBUF's at top level
      o_ifclk    : out std_logic;
      o_slrd     : out std_logic;
      o_slwr     : out std_logic;
      o_sloe     : out std_logic;
      o_slcs_n   : out std_logic;
      i_flaga    : in  std_logic;
      i_flagb    : in  std_logic;
      i_flagc    : in  std_logic;
      o_fifoadr0 : out std_logic;
      o_fifoadr1 : out std_logic;
      o_pktend   : out std_logic
    );
end ez_usb;
