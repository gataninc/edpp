--- Title: camera_link.vhd
--- Description:  This module implements a Camera Link with a register interface
---                for the MityDSP. 
---
---     o  0
---     | /       Copyright (c) 2007-2008
---    (CL)---o   Critical Link, LLC
---      \
---       O
---
--- Company: Critical Link, LLC.
--- Date: 06/28/2008
--- Version: 1.08
--- Revisions: 1.00 Initial version.
---            1.01 Made FIFO interface configurable.
---            1.02 Added FIFO reset capability.
---            1.03 Added DataValid XOR control bit.
---            1.04 Improved framing logic, and renamed XOR bit as INV bit.
---            1.05 Added metastability flip-flop to ccr_en.
---            1.06 Further improvements in framing logic.
---            1.07 Add Frame decimation feature
---            1.08 Add direct DMA writing feature


library WORK;
library IEEE;
library UNISIM;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use UNISIM.VCOMPONENTS.ALL;

entity camera_link is
   generic (
      FIFO_DEPTH_TWO_TO_N : integer range 9 to 15 := 10; -- 2^10 = 1024 32-bit words
      SUPPORT_DMA         : boolean := true
   );
   port (
      -- MityDSP Core interface
      emif_clk   : in  std_logic;
      i_ABus     : in  std_logic_vector(4 downto 0);
      i_DBus     : in  std_logic_vector(31 downto 0);
      o_DBus     : out std_logic_vector(31 downto 0);
      i_wr_en    : in  std_logic;
      i_rd_en    : in  std_logic;
      i_cs       : in  std_logic;   
      o_irq      : out std_logic;
      i_ilevel      : in std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld  : in std_logic := '1';                           -- interrupt level valid flag
      i_ivector     : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld : in std_logic := '1';                           -- interrupt vector valid flag

      -- CameraLink interface
      o_pwr_down_n : out std_logic;
      i_cam_clk    : in  std_logic;
      i_cam_din    : in  std_logic_vector(27 downto 0);
      o_cc0        : out std_logic;
      o_cc1        : out std_logic;
      o_cc2        : out std_logic;
      o_cc3        : out std_logic;
      o_cc_en      : out std_logic;
      -- FIFO write interface
      o_fifo_wr_clk : out std_logic;   
      o_fifo_wr     : out std_logic;
      o_fifo_din    : out std_logic_vector(31 downto 0); -- (15)=frame-sync (14)=line-sync
      -- FIFO read interface
      o_fifo_rst    : out std_logic;
      o_fifo_rd_clk : out std_logic;
      o_fifo_rd     : out std_logic;
      i_fifo_dout   : in  std_logic_vector(31 downto 0);
      i_fifo_full   : in  std_logic;                                        -- fifo's full flag
      i_fifo_empty  : in  std_logic;                                        -- fifo's empty
      i_fifo_rd_cnt : in  std_logic_vector(FIFO_DEPTH_TWO_TO_N-1 downto 0); -- fifo's read count
        
      -- MityDSP EMIF DMA interface (not available for MityDSP-PRO designs)
      o_dma_req          : out std_logic := '0';
      o_dma_priority_req : out std_logic := '0';
      o_dma_rwn          : out std_logic := '0';
      o_dma_addr         : out std_logic_vector(31 downto 0) := (others=>'0');
      o_dma_data         : out std_logic_vector(31 downto 0) := (others=>'0');
      o_dma_count        : out std_logic_vector(8 downto 0) := (others=>'0');
      o_dma_be           : out std_logic_vector(3 downto 0) := "1111";      
      i_dma_wr_stb       : in std_logic := '0';
      i_dma_rd_stb       : in std_logic := '0';
      i_dma_data         : in std_logic_vector(31 downto 0) := (others=>'0');
      i_dma_done         : in std_logic := '0'
      
   );
end camera_link;
