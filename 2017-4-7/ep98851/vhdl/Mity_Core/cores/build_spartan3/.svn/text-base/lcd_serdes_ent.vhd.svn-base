--- Title: lcd_serdes.vhd
--- Description:  Optional Serializer to use less LCD interface pins
---
---     o  0
---     | /       Copyright (c) 2007-2009
---    (CL)---o   Critical Link, LLC
---      \
---       O
---
--- Company: Critical Link, LLC.
--- Date: 03/12/2009
--- Version: 1.00
--- Revisions: 1.00 Initial version.

library IEEE;
library UNISIM;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use UNISIM.VCOMPONENTS.ALL;

entity lcd_serdes is
   port (
      serdes_clk        : in std_logic;
      -- LCD Controller interface
      i_lcd_sdat     : in std_logic;
      i_lcd_hsync    : in std_logic;
      i_lcd_vsync    : in std_logic;
      i_lcd_data_r   : in std_logic_vector(5 downto 0);
      i_lcd_data_g   : in std_logic_vector(5 downto 0);
      i_lcd_data_b   : in std_logic_vector(5 downto 0);
      i_lcd_dotclk   : in std_logic;
      i_lcd_up_dn    : in std_logic;
      i_lcd_r_l      : in std_logic;

      -- LvDS I/O pins for LCD and some control signals
      o_serdes_clk    :out std_logic;
      o_serdes_ctl    :out std_logic;
      o_serdes_b      :out std_logic;
      o_serdes_g      :out std_logic;
      o_serdes_r      :out std_logic;
      
      o_debug         : out std_logic_vector(7 downto 0)
   );
end lcd_serdes;
