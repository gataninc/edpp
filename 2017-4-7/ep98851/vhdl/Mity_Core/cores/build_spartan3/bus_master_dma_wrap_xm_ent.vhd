--- Title: bus_master_dma_wrap_xm.vhd
--- Description: Wrapper for the DMA Core for MityDSP-XM boards
---              DMA core that operates as the EMIF bus master to transfer data
---              between the FPGA and SDRAM using the EMIF bus and arbitrating
---              with the DSP for control of the EMIF bus. 
---
---     o  0
---     | /       Copyright (c) 2006
---    (CL)---o   Critical Link, LLC
---      \
---       O
---
--- Company: Critical Link, LLC.
--- Date: 04/20/2007
--- Version: 1.00
--- Revisions:
--- 1.00 Initial version

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity bus_master_dma_wrap_xm is
   generic (
      BUFF32_ADDR_BITS  : integer range 9  to 11 := 10;
      CAS_LATENCY       : integer range 2  to 3  := 2
   );
   port (
      emif_clk : in std_logic; -- 50 Mhz EMIF clock

      -- DSP BUS Interface
      i_ce0_n  : in std_logic;
      o_ce0_n  :out std_logic;
      i_ce1_n  : in std_logic;
      o_ce1_n  :out std_logic;
      i_ce2_n  : in std_logic;
      i_ce3_n  : in std_logic;
      i_ras_n  : in std_logic;       --AOE/SDRAS
      o_ras_n  :out std_logic;       --AOE/SDRAS
      i_cas_n  : in std_logic;       --ARE/SDCAS
      o_cas_n  :out std_logic;       --ARE/SDCAS
      i_awe_n  : in std_logic;       --AWE/SDWE
      o_awe_n  :out std_logic;       --AWE/SDWE
      i_be_n   : in std_logic_vector(3 downto 0);     --DQM3:0
      o_be_n   :out std_logic_vector(3 downto 0);     --DQM3:0
      i_ea     : in std_logic_vector(15 downto 2);    --BA1:0 | A11:0
      o_ea     :out std_logic_vector(15 downto 2);    --BA1:0 | A11:0
      t_emif   :out std_logic;         --Drive CEn, AOE, ARE, AWE, BEn, EAn

      i_ed     : in std_logic_vector(31 downto 0);
      o_ed     :out std_logic_vector(31 downto 0);
      t_ed     :out std_logic;         --Drive EDn

      --Normal Core Signals...
      i_cs            : in  std_logic;	
      i_ABus          : in  std_logic_vector(4 downto 0);
      i_DBus          : in  std_logic_vector(31 downto 0);
      o_DBus          : out std_logic_vector(31 downto 0);
      i_wr_en         : in  std_logic;
      i_rd_en         : in  std_logic;
      o_irq           : out std_logic;
      i_ilevel        : in std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld    : in std_logic := '1';                           -- interrupt level valid flag
      i_ivector       : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld   : in std_logic := '1';                           -- interrupt vector valid flag 

      --Periodic DMA Trigger
      i_dma_trig0 : in std_logic;
      i_dma_trig1 : in std_logic;
      i_dma_trig2 : in std_logic;
      i_dma_trig3 : in std_logic;
      i_dma_priority_trig0 : in std_logic;
      i_dma_priority_trig1 : in std_logic;
      i_dma_priority_trig2 : in std_logic;
      i_dma_priority_trig3 : in std_logic;
      --DMA Data Interface to Other Core's Buffer
      o_dma_first_word0 :out std_logic;
      o_dma_first_word1 :out std_logic;
      o_dma_first_word2 :out std_logic;
      o_dma_first_word3 :out std_logic;
      o_dma_we          :out std_logic;
      o_dma_data        :out std_logic_vector(31 downto 0);
      o_dma_addr        :out std_logic_vector(BUFF32_ADDR_BITS-1 downto 0);

      --EMIF Master Negotiation
      o_hold_n    :out std_logic;   --Request EMIF
      i_holda_n   : in std_logic;   --Acknowledge EMIF Request
      i_bus_req   : in std_logic    --DSP Requests EMIF
   );
end bus_master_dma_wrap_xm;

