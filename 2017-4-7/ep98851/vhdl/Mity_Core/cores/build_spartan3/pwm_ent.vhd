library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity pwm is
   Port 
   ( 
      clk        : in  std_logic;                                        -- emif clock
      i_ABus     : in  std_logic_vector(4 downto 0);                     -- emif core address bus
      i_DBus     : in  std_logic_vector(31 downto 0);                    -- emif data bus
      o_DBus     : out std_logic_vector(31 downto 0);                    -- emif output data bus
      i_wr_en    : in  std_logic;                                        -- emif write enable
      i_rd_en    : in  std_logic;                                        -- emif read enable
      i_cs       : in  std_logic;	                                     -- emif core select
      o_irq      : out std_logic;                                        -- IRQ output
      i_ilevel       : in    std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld   : in    std_logic := '0';                           -- interrupt level valid flag
      i_ivector      : in    std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld  : in    std_logic := '0';                           -- interrupt vector valid flag
      o_pulse        : out   std_logic_vector(4 downto 0);               -- pulse output vector
      i_trigger      : in    std_logic;                                  -- pulse trigger
      i_aclk         : in    std_logic;                                  -- reference clock
      i_wd_stop      : in    std_logic := '0'                            -- watch dog stop signal (not latched)
    );
end pwm;						 
