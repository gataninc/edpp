--- Title: ADS8343.vhd
--- Description: Wrapper for ADS8343 (4-Ch)
---
---     o  0
---     | /       Copyright (c) 2006-2008
---    (CL)---o   Critical Link, LLC
---      \
---       O
---
--- Company: Critical Link, LLC.
--- Date: 01/15/2008
--- Version: 1.09
--- Revisions: 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity ADS8343 is
   Port 
   (  
      emif_clk      : in  std_logic;
      i_ABus        : in  std_logic_vector(4 downto 0);
      i_DBus        : in  std_logic_vector(31 downto 0);
      o_DBus        : out std_logic_vector(31 downto 0);
      i_wr_en       : in  std_logic;
      i_rd_en       : in  std_logic;
      i_cs          : in  std_logic;	
      o_irq         : out std_logic;
      i_ilevel      : in  std_logic_vector(1 downto 0) := "00";    -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld  : in  std_logic := '1';                        -- interrupt level valid flag
      i_ivector     : in  std_logic_vector(4 downto 0) := "00000"; -- interrupt vector (0 through 31)
      i_ivector_vld : in  std_logic := '1';                        -- interrupt vector valid flag
      i_trigger     : in  std_logic; -- external trigger
      i_clk         : in  std_logic;
      i_clk_en      : in  std_logic;
      o_dclk        : out std_logic; -- ADC clock
      o_adc_din     : out std_logic; -- ADS8343 data input line
      i_adc_dout    : in  std_logic; -- ADS8343 data output line
      i_adc_busy    : in  std_logic; -- ADS8343 busy line     
      i_overlap     : in  std_logic; -- when 1: sample rate = 100kHhz
                                     -- when 0: sample rate = 96kHz
      o_cs_n        : out std_logic  -- ADS8343 chip select (if not used, ensure toggles)
    );
end ADS8343;						 
