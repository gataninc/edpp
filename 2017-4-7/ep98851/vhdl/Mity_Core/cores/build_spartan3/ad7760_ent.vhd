--- Title: AD7760_ent.vhd
--- Description: Entity for Analog Devices AD7760 ADC.
---
---     o  0
---     | /       Copyright (c) 2008
---    (CL)---o   Critical Link, LLC
---      \
---       O
---
--- Company: Critical Link, LLC.
--- Date: 04/11/2008
--- Version: 1.00
--- Revisions: 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity AD7760 is
   port 
   ( 
      emif_clk      : in  std_logic;
      i_ABus        : in  std_logic_vector(4 downto 0);
      i_DBus        : in  std_logic_vector(31 downto 0);
      o_DBus        : out std_logic_vector(31 downto 0);
      i_wr_en       : in  std_logic;
      i_rd_en       : in  std_logic;
      i_cs          : in  std_logic;	
      o_irq         : out std_logic;
      i_ilevel      : in  std_logic_vector(1 downto 0) := "00";    -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld  : in  std_logic := '1';                        -- interrupt level valid flag
      i_ivector     : in  std_logic_vector(4 downto 0) := "00000"; -- interrupt vector (0 through 31)
      i_ivector_vld : in  std_logic := '1';                        -- interrupt vector valid flag

      i_clk         : in std_logic;
      i_clk_en      : in std_logic;
      i_trigger     : in std_logic;       -- external trigger
      
      -- AD7760 control ports
      i_adc_rst_n   : in  std_logic;
      o_adc_cs_n    : out std_logic;
      o_adc_rdx_wr  : out std_logic;
      i_adc_drdy_n  : in  std_logic;
      i_adc_dout    : in  std_logic_vector(15 downto 0);
      o_adc_din     : out std_logic_vector(15 downto 0);
      o_adc_tri     : out std_logic;
      
      -- Bus Arbiter control ports
      o_bus_req     : out std_logic;
      i_bus_gnt     : in  std_logic
    );
end AD7760;