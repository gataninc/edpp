--- Title: distRAM.vhd
--- Description: This core provides an interface to DistRAM in and out config
---   options for a design
---
---
---     o  0                          
---     | /       Copyright (c) 2006  
---    (CL)---o   Critical Link, LLC  
---      \                            
---       O                           
---
--- Company: Critical Link, LLC.
--- Date: 03/09/2006
--- Version: 0.1 03/09/2006 Initial Version

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity distRAM is
   Port ( 
      clk      : in std_logic;
      i_ABus   : in std_logic_vector(4 downto 0);
      i_DBus   : in std_logic_vector(31 downto 0);
      o_DBus   :out std_logic_vector(31 downto 0);
      i_wr_en  : in std_logic;
      i_rd_en  : in std_logic;
      i_cs     : in std_logic;
      i_we     : in std_logic;
      i_ain    : in std_logic_vector(3 downto 0);
      i_data   : in std_logic_vector(31 downto 0); --Data To DSP
      o_din    :out std_logic_vector(31 downto 0); --Can read what is going to DSP
      i_aout   : in std_logic_vector(3 downto 0);
      o_data   :out std_logic_vector(31 downto 0);  --Data From DSP
      i_core_id  : in std_logic_vector(7 downto 0) := CONV_STD_LOGIC_VECTOR( 17, 8) -- override this to FF for custom app
    );
end distRAM;
