-- This wrapper should be used for MityDSP and MityDPS-XM instances of the clock1305 core.

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity clock1305_mity is
   port (
      emif_clk       : in std_logic; -- global buffered EMIF clock

      i_ABus         : in  std_logic_vector(4 downto 0);
      i_DBus         : in  std_logic_vector(31 downto 0);
      o_DBus         : out std_logic_vector(31 downto 0);
      i_wr_en        : in  std_logic;
      i_rd_en        : in  std_logic;
      i_cs           : in  std_logic;
      o_irq          : out std_logic;

      i_ilevel       : in std_logic_vector(1 downto 0) := "01";      -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld   : in std_logic := '1';                          -- interrupt level valid flag
      i_ivector      : in std_logic_vector(4 downto 0) := "00000";   -- interrupt vector (0 through 31)
      i_ivector_vld  : in std_logic := '1';                          -- interrupt vector valid flag
      
      o_clock_secs   : out std_logic_vector(31 downto 0); -- for use in other cores
      o_clock_nsecs  : out std_logic_vector(31 downto 0); -- for use in other cores

      o_clk_1sec     : out std_logic; -- debug/test 
      o_clk_1ms      : out std_logic; -- debug/test
      o_clk_1us      : out std_logic  -- debug/test 
   );
end entity clock1305_mity;