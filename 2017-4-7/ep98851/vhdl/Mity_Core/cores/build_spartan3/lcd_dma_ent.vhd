--- Title: lcd_dma.vhd
--- Description: LCD Display Interface using EMIF_SDRAM_DMA
--- Densitron:84-0023-001T QVGA TFT LCD
--- LGPhillips:LB040Q03 QVGA TFT LCD
---
---     o  0
---     | /       Copyright (c) 2009
---    (CL)---o   Critical Link, LLC
---      \
---       O
---
--- Company: Critical Link, LLC.
--- Date: 03/12/2009
--- Version: 1.00
--- Revisions:
--- 1.00 Initial version
---

library STD;
use STD.TEXTIO.ALL;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_TEXTIO.ALL;

entity lcd_dma is
   generic (
      BUFF32_ADDR_BITS : integer range 9 to 12 := 9;  --1 to 8 BRAMs (x32)
      BUFF32_INIT_FILE : string := "lcd_buff_init.txt"
   );
   port (
      -- MityDSP Core interface
      emif_clk          : in std_logic; -- 50 Mhz EMIF clock
      i_cs              : in std_logic;   --cs for LCD addr range
      i_ABus            : in std_logic_vector(4 downto 0);
      i_DBus            : in std_logic_vector(31 downto 0);
      o_DBus            :out std_logic_vector(31 downto 0);
      i_wr_en           : in std_logic;
      i_rd_en           : in std_logic;
      o_irq             :out std_logic;
      i_ilevel          : in std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld      : in std_logic := '1';                           -- interrupt level valid flag
      i_ivector         : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld     : in std_logic := '1';                           -- interrupt vector valid flag

      -- MityDSP EMIF DMA interface (not available for MityDSP-PRO designs)
      o_dma_req          : out std_logic := '0';
      o_dma_priority_req : out std_logic := '0';
      o_dma_rwn          : out std_logic := '1'; -- only want to read from SDRAM
      o_dma_addr         : out std_logic_vector(31 downto 0) := (others=>'0'); -- SDRAM starting read address
      o_dma_count        : out std_logic_vector(8 downto 0) := (others=>'0'); -- number of 32 bit words minus 1
      i_dma_wr_stb       : in std_logic; -- EMIF DMA writing dma_data from SDRAM to client FIFO
      i_dma_data         : in std_logic_vector(31 downto 0) := (others=>'0');
      i_dma_done         : in std_logic; -- burst is done

      -- LCD interface
      o_lcd_up_dn    :out std_logic; 
      o_lcd_r_l      :out std_logic;
      o_lcd_sdat     :out std_logic;
      o_lcd_hsync    :out std_logic;
      o_lcd_vsync    :out std_logic;
      o_lcd_data_r   :out std_logic_vector(5 downto 0);
      o_lcd_data_g   :out std_logic_vector(5 downto 0);
      o_lcd_data_b   :out std_logic_vector(5 downto 0);
      o_lcd_dotclk   :out std_logic
   );
end lcd_dma;
