library IEEE;
library UNISIM;
library WORK;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use UNISIM.VCOMPONENTS.ALL;
use WORK.mitydsp_pkg.all;

--*
--* Version history
--* 1.3 - added FIFO version register
--* 1.4 - added modem control signals, optimized address space
--*     - removed registered bits that are not used for anything to conserved logic/flip-flops
--*     - removed auto-interrupt enabling logic (this is controlled in SW)
--* 1.5 - Changed interrupt logic to not issue receive interrupt unless:
--*          1. Rx FIFO is at least 1/2 full
--*          2. Rx FIFO is non-empty and at least 15000 emif clocks (300 usec at 50 Mhz) 
--*             has transpired.  (Bugzilla 389)
--* 2.0 - Move RHR to separate 32 bit address space to support MityDSP-Pro
--* 2.1 - Added Xmit-Enable output port (Bugzilla 1658)
--/

entity uart is
   Port ( 
      clk 	         : in std_logic;
      i_rcv          : in std_logic;
      o_xmit         : out std_logic;
      o_irq		     : out std_logic;
      i_ilevel       : in std_logic_vector(1 downto 0) := "10";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld   : in std_logic := '1';                           -- interrupt level valid flag
      i_ivector      : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld  : in std_logic := '1';                           -- interrupt vector valid flag
      i_ABus	     : in std_logic_vector(4 downto 0);
      i_be           : in std_logic_vector(3 downto 0);
      i_DBus	     : in std_logic_vector(31 downto 0);
      o_DBus	     : out std_logic_vector(31 downto 0);
      i_wr_en	     : in std_logic;
      i_rd_en	     : in std_logic;
      i_uart_cs	     : in std_logic;	
      i_xmit_cts     : in std_logic := '0';   -- active low
      o_rcv_cts      : out std_logic;         -- active low
      i_dsr          : in std_logic := '0';   -- active low
      o_dtr          : out std_logic;         -- active low
      i_dcd          : in std_logic := '0';   -- active low
      i_ri           : in std_logic := '0';   -- active low
      o_xmit_enb     : out std_logic          -- transmit enable
   );
end uart;
