--- Title: AWG.vhd
--- Description: Arbitrary Waveform Generator core
---
---
---     o  0                          
---     | /       Copyright (c) 2006  
---    (CL)---o   Critical Link, LLC  
---      \                            
---       O                           
---
--- Company: Critical Link, LLC
--- Date: 09/07/2006
--- Version: 
---   1.00 - 06/05/2006 - Initial Version
---   1.01 - 09/06/2006 - Fixed a few bugs, and added phase roll-over output.
---   1.02 - 09/07/2006 - Added sample enable input for more clocking flexibility.
---   

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library Unisim;
use Unisim.Vcomponents.all;

entity awg is
   Port ( 
      -- EMIF Interface signals
      emif_clk   : in  std_logic;
      i_ABus     : in  std_logic_vector(4 downto 0);
      i_DBus     : in  std_logic_vector(31 downto 0);
      o_DBus     : out std_logic_vector(31 downto 0);
      i_wr_en    : in  std_logic;
      i_rd_en    : in  std_logic;
      i_cs       : in  std_logic;
      -- IRQ system signals
--      o_irq      : out std_logic;
--      i_ilevel       : in    std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
--      i_ilevel_vld   : in    std_logic := '0';                           -- interrupt level valid flag
--      i_ivector      : in    std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
--      i_ivector_vld  : in    std_logic := '0';                           -- interrupt vector valid flag
      -- AWG signals
      i_clk        : in  std_logic;
      i_sample_en  : in  std_logic;
      i_trigger    : in  std_logic;
      o_phase_roll : out std_logic;
      o_phase      : out std_logic_vector(23 downto 0);
      o_data       : out std_logic_vector(15 downto 0)
    );
end awg;
