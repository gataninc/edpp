--- Title: LVDS_Link_ent.vhd
--- Description:
---
---     o  0
---     | /       Copyright (c) 2007
---    (CL)---o   Critical Link, LLC
---      \
---       O
---
--- Company: Critical Link, LLC.
--- Date: 08/08/2007
--- Version: 1.00
--- Revisions: 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity LVDS_Link1x1 is
   Port 
   (  
      emif_clk      : in  std_logic;
      i_ABus        : in  std_logic_vector(4 downto 0);
      i_DBus        : in  std_logic_vector(31 downto 0);
      o_DBus        : out std_logic_vector(31 downto 0);
      i_wr_en       : in  std_logic;
      i_rd_en       : in  std_logic;
      i_cs          : in  std_logic;	
      o_irq         : out std_logic;
      i_ilevel      : in  std_logic_vector(1 downto 0) := "00";    -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld  : in  std_logic := '1';                        -- interrupt level valid flag
      i_ivector     : in  std_logic_vector(4 downto 0) := "00000"; -- interrupt vector (0 through 31)
      i_ivector_vld : in  std_logic := '1';                        -- interrupt vector valid flag
      i_tx_clk      : in  std_logic;                               -- output clock source
      o_link_tx_clk : out std_logic;                               -- LVDS output link clock (route to LVDS driver pair)
      o_link_data   : out std_logic;                               -- LVDS output data clock (route to LVDS driver pair)
      i_rx_clk      : in  std_logic;                               -- LVDS input  data clock (route to LVDS driver pair)
      i_rx_data     : in  std_logic                                -- LVDS input  data clock (route to LVDS driver pair)
    );                          
end LVDS_Link1x1;						 

