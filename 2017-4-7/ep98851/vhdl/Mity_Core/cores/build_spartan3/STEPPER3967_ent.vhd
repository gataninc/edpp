library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity STEPPER3967 is
   Port 
   ( 
      clk        : in std_logic;
      base_clk   : in std_logic;
      i_ABus     : in std_logic_vector(4 downto 0);
      i_DBus     : in std_logic_vector(31 downto 0);
      o_DBus     :out std_logic_vector(31 downto 0);
      i_wr_en    : in std_logic;
      i_rd_en    : in std_logic;
      i_cs       : in std_logic;
      o_irq      :out std_logic;
      i_ilevel      : in std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld  : in std_logic := '1';                           -- interrupt level valid flag
      i_ivector     : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld : in std_logic := '1';                           -- interrupt vector valid flag

      i_ts          : in std_logic_vector(6 downto 0);
      i_estop       : in std_logic := '0';
      i_ms          : in std_logic_vector(1 downto 0) ;   --Can Hard Code Setting
      o_ms          :out std_logic_vector(1 downto 0);  --For SW Control
      i_ms_vld      : in std_logic := '0';                --MS Bits Are Meaningful

      o_reset_n     :out std_logic;
      o_enable_n    :out std_logic;
      o_sleep_n     :out std_logic;
      o_dir         :out std_logic;
      o_step        :out std_logic;
      i_hi_current  : in std_logic_vector(1 downto 0);     --Can Hard Code the bit[s]
      o_hi_current  :out std_logic_vector(1 downto 0);     --For sW Control
      i_hi_curr_vld : in std_logic   --SW Control Available
    );
end STEPPER3967;						