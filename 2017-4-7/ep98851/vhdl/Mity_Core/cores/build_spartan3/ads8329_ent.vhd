--- Title: ADS8329_ent.vhd
--- Description: Entity Declaration Core for ADS8329 ADC.
---
---     o  0
---     | /       Copyright (c) 2007
---    (CL)---o   Critical Link, LLC
---      \
---       O
---
--- Company: Critical Link, LLC.
--- Date: 3/12/2007
--- Version: 1.00
--- Revisions: 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity ADS8329 is
   port 
   ( 
      clk        : in  std_logic;
      i_ABus     : in  std_logic_vector(4 downto 0);
      i_be       : in  std_logic_vector(3 downto 0) := "0000";
      i_DBus     : in  std_logic_vector(31 downto 0);
      o_DBus     : out std_logic_vector(31 downto 0);
      i_wr_en    : in  std_logic;
      i_rd_en    : in  std_logic;
      i_cs       : in  std_logic;	
      o_irq      : out std_logic;
      i_ilevel        : in std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld    : in std_logic := '1';                           -- interrupt level valid flag
      i_ivector       : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld   : in std_logic := '1';                           -- interrupt vector valid flag
      i_adc_clk      : in  std_logic;
      i_udata        : in  std_logic_vector(4 downto 0);   -- User Data lines
      i_trigger      : in  std_logic;                      -- external trigger
      o_adc_convst_n : out std_logic;                      -- adc convert start strobe      
      i_adc_eoc      : in  std_logic;                      -- end of convert signal from ADC (not used)
      o_adc_sclk     : out std_logic;                      -- ADC data shift clock (1/2 EMIF clock)
      o_adc_sdi      : out std_logic;                      -- ADC SPI input line
      i_adc_sdo      : in  std_logic;                      -- ADC SPI output line
      o_adc_cs_n     : out std_logic;                      -- ADC frame sync line      
      i_fifo_re      : in  std_logic;                      -- optional FIFO read enable (for CE 2 access)
      o_fifo_data    : out std_logic_vector(31 downto 0)   -- optional FIFO output enable (for CE 2 access)
    );
end ADS8329;						 
