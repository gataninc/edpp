--- Title: cl_eth_671x_wfiltering.vhd
--- Description: Wrapper for ethernet core with 8 MAC filters and filtering.
---
---
---     o  0                          
---     | /       Copyright (c) 2008  
---    (CL)---o   Critical Link, LLC  
---      \                            
---       O                           
---
--- Company: Critical Link, LLC.

library IEEE;
library Unisim;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use Unisim.Vcomponents.all;

entity cl_eth_671x_wfiltering is
   port (
      emif_clk    : in std_logic; -- global buffered EMIF clock
      clk25       : in std_logic; -- global buffered 25MHz clock

      o_ref_clk   :out std_logic; -- 25MHz reference clock for some PHYs
      o_clk2_5    :out std_logic; -- 2.5MHz clock for TX Engine, when using CS8952
      tx_clk      : in std_logic; -- global buffered transmit clock
      rx_clk      : in std_logic; -- global buffered receive clock

      ABus        : in std_logic_vector(4 downto 0);
      iDBus       : in std_logic_vector(31 downto 0);
      oDBus       :out std_logic_vector(31 downto 0);
      wr_en       : in std_logic;
      rd_en       : in std_logic;
      eth_cs      : in std_logic;   --sync cs for eth addr range

      o_mdc       :out std_logic;
      o_mdio      :out std_logic;
      i_mdio      : in std_logic := '0';
      t_mdio      :out std_logic; -- tri-state the MDIO line

      o_tx_spd_100   :out std_logic;
      i_half_duplex  : in std_logic := '0';

      i_col		   : in std_logic := '0';
      i_crs		   : in std_logic := '0';
      o_tx_data	:out std_logic_vector(3 downto 0);
      o_tx_en     :out std_logic;
      o_tx_er     :out std_logic;
      o_rx_en     :out std_logic;
      i_rx_data   : in std_logic_vector(3 downto 0);
      i_rx_dv     : in std_logic;
      i_rx_er     : in std_logic;

      o_eth_rst   :out std_logic;
      int_rxpkt   :out std_logic;

      i_ilevel       : in std_logic_vector(1 downto 0) := "01";      -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld   : in std_logic := '1';                          -- interrupt level valid flag
      i_ivector      : in std_logic_vector(4 downto 0) := "00000";   -- interrupt vector (0 through 31)
      i_ivector_vld  : in std_logic := '1';                          -- interrupt vector valid flag

      o_rcv_pkt_data : out std_logic_vector(7 downto 0);
      o_rcv_pkt_we   : out std_logic;
      o_rcv_pkt_addr : out std_logic_vector(10 downto 0);
      o_rcv_pkt_clk  : out std_logic;
      i_rcv_pkt_data : in  std_logic_vector(7 downto 0);
      i_rcv_pkt_we   : in  std_logic;
      i_rcv_pkt_addr : in  std_logic_vector(10 downto 0);
      
      o_xmit_pkt_data : out std_logic_vector(7 downto 0);
      o_xmit_nib_cnt  : out std_logic_vector(12 downto 0);
      o_xmit_pkt_clk  : out std_logic;
      i_xmit_pkt_data : in  std_logic_vector(7 downto 0);
      i_xmit_nib_cnt  : in  std_logic_vector(12 downto 0)
      
   );
end entity cl_eth_671x_wfiltering;
