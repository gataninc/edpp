--- Title: i2c.vhd
--- Description: This module interfaces to a serial EEPORM to read/write config
---   options for a design
---
---
---     o  0                          
---     | /       Copyright (c) 2006  
---    (CL)---o   Critical Link, LLC  
---      \                            
---       O                           
---
--- Company: Critical Link, LLC.
--- Date: 01/22/2006
--- Version: 0.1 01/22/2006 Initial Version
---          1.0 03/09/2006 Removed Page Write Delay from FW

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity i2c is
   generic (
      CLK_DIVIDE : integer := 256;        -- ~200KHz from 50MHz
      BYTES_PER_PAGE : integer := 32;     --Bytes per page write
      WRITE_DLY_CNT_BIT : integer := 10   --Twr Page Write Delay = (2^n)*scl_period
   );
   Port ( 
      clk      : in std_logic;
      i_ABus   : in std_logic_vector(4 downto 0);
      i_DBus   : in std_logic_vector(31 downto 0);
      o_DBus   :out std_logic_vector(31 downto 0);
      i_wr_en  : in std_logic;
      i_rd_en  : in std_logic;
      i_cs     : in std_logic;	
      o_irq    :out std_logic;
      i_ilevel       : in std_logic_vector(1 downto 0) := "01";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld   : in std_logic := '0';                           -- interrupt level valid flag
      i_ivector      : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld  : in std_logic := '0';                           -- interrupt vector valid flag
      i_sda    : in std_logic;
      o_sda    :out std_logic;
      o_sdt    :out std_logic;
      o_scl    :out std_logic
    );
end i2c;						 
