--- Title: cl_eth_671x.vhd
--- Description: This module is an Ethernet MAC IP core for connecting a TI 671x
---   DSP to a CS9852 ethernet phy.
---
---
---     o  0                          
---     | /       Copyright (c) 2003-2005  
---    (CL)---o   Critical Link, LLC  
---      \                            
---       O                           
---
--- Company: Critical Link, LLC.
--- Date: 07/29/2008
--- Version: 2.2
---
--------------------------------------------------------------------------------
--- Memory Map:
---
--- Offset Contents
--- ------ ----------------------
--- 0x0000 Core Module Version Register
--- 0x0004 NUM MAC FILTERS (bits 23-20), MAC WRITE OFFSET (bits 19-16) PROM, IRQE, PHY Rst, TX Speed
--- 0x0008 TX MII SHIN/SHOUT
--- 0x000C TX MI CTRL
--- 0x0010 TX Length (in bytes)
--- 0x0014 TX Control byte (xxxxxxPS) P=Program, S=Status; S=1 to initiate a transmit
--- 0x0018 TX Packet FIFO
--- 0x0020 RX Packet FIFO
--- 0x0024 RX Miss Count
--- 0x0028 RX Length (in bytes)
--- 0x002C RX Control byte (xxxxxxxS) S=Status; S=1 for new pkt
--- 0x0040 TX/RX FIFO (duplicate of 0x0020 and 0x0018 functionality)
-------------------------------------------------------------------------------
-- version history
-- 1.01 Baseline Core'd 
-- 1.02 Added core level interrupt enable mask.
-- 1.03 MII MI register alignment & added phy address
-- 1.04 Added Tx/Rx FIFO decoding at 0x0040 to support MityDSP Pro
-- 2.00 Changed Tx/Rx FIFO lengths to add 2 (bogus) bytes to the beginning
--      of the buffers.  This is needed to support the DSP with data alignment
--      and remove extra data copies of packet payloads in memory.  See
--      bug 1096.  This breaks inter-operability with older software, hence
--      the version number bump to 2.0.
-- 2.01 Added Multiple MAC filtering to support IGMP
--      Added Promiscuous Receive bit
-- 2.02 Added Hooks for external packet filtering/editing
-------------------------------------------------------------------------------

library IEEE;
library Unisim;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use Unisim.Vcomponents.all;

entity cl_eth_671x is
   generic (
      NUM_MAC_FILTERS      : integer range 1 to 16 := 8;
      USE_INPUT_FILTERING  : boolean               := FALSE;
      USE_OUTPUT_FILTERING : boolean               := FALSE 
   );
   port (
      emif_clk    : in std_logic; -- global buffered EMIF clock
      clk25       : in std_logic; -- global buffered 25MHz clock

      o_ref_clk   :out std_logic; -- 25MHz reference clock for some PHYs
      o_clk2_5    :out std_logic; -- 2.5MHz clock for TX Engine, when using CS8952
      tx_clk      : in std_logic; -- global buffered transmit clock
      rx_clk      : in std_logic; -- global buffered receive clock

      ABus        : in std_logic_vector(4 downto 0);
      iDBus       : in std_logic_vector(31 downto 0);
      oDBus       :out std_logic_vector(31 downto 0);
      wr_en       : in std_logic;
      rd_en       : in std_logic;
      eth_cs      : in std_logic;   --sync cs for eth addr range

      o_mdc       :out std_logic;
      o_mdio      :out std_logic;
      i_mdio      : in std_logic := '0';
      t_mdio      :out std_logic; -- tri-state the MDIO line

      o_tx_spd_100   :out std_logic;
      i_half_duplex  : in std_logic := '0';

      i_col		   : in std_logic := '0';
      i_crs		   : in std_logic := '0';
      o_tx_data	:out std_logic_vector(3 downto 0);
      o_tx_en     :out std_logic;
      o_tx_er     :out std_logic;
      o_rx_en     :out std_logic;
      i_rx_data   : in std_logic_vector(3 downto 0);
      i_rx_dv     : in std_logic;
      i_rx_er     : in std_logic;

      o_eth_rst   :out std_logic;
      int_rxpkt   :out std_logic;

      i_ilevel       : in std_logic_vector(1 downto 0) := "01";      -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld   : in std_logic := '1';                          -- interrupt level valid flag
      i_ivector      : in std_logic_vector(4 downto 0) := "00000";   -- interrupt vector (0 through 31)
      i_ivector_vld  : in std_logic := '1';                          -- interrupt vector valid flag
      
      -- The following ports are only required if USE_INPUT_FILTERING = true
      o_rcv_pkt_data : out std_logic_vector(7 downto 0) := x"00";
      o_rcv_pkt_we   : out std_logic := '0';
      o_rcv_pkt_addr : out std_logic_vector(10 downto 0) := (others=>'0');
      o_rcv_pkt_clk  : out std_logic := '0';
      i_rcv_pkt_data : in  std_logic_vector(7 downto 0) := x"00";
      i_rcv_pkt_we   : in  std_logic := '0';
      i_rcv_pkt_addr : in  std_logic_vector(10 downto 0) := (others=>'0');
      
      -- The following ports are only required if USE_OUTPUT_FILTERING = true
      o_xmit_pkt_data : out std_logic_vector(7 downto 0) := x"00";
      o_xmit_nib_cnt  : out std_logic_vector(12 downto 0) := "0000000000000";
      o_xmit_pkt_clk  : out std_logic := '0';
      i_xmit_pkt_data : in  std_logic_vector(7 downto 0) := x"00";
      i_xmit_nib_cnt  : in  std_logic_vector(12 downto 0) := "0000000000000"
   );
end entity cl_eth_671x;
