--- Title: ADS8402_wrap.vhd
--- Description: Wrapper Core for ADS8402 ADC.
---
---     o  0
---     | /       Copyright (c) 2006-2008
---    (CL)---o   Critical Link, LLC
---      \
---       O
---
--- Company: Critical Link, LLC.
--- Date: 01/15/2008
--- Version: 1.04
--- Revisions: 

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity ADS8402 is
   generic
   (
      FIFO_DEPTH_TWO_TO_N : integer range 10 to 11 := 10 -- 10 is 1 K words
   );
   port 
   ( 
      clk        : in  std_logic;
      i_ABus     : in  std_logic_vector(4 downto 0);
      i_be       : in  std_logic_vector(3 downto 0) := "0000";
      i_DBus     : in  std_logic_vector(31 downto 0);
      o_DBus     : out std_logic_vector(31 downto 0);
      i_wr_en    : in  std_logic;
      i_rd_en    : in  std_logic;
      i_cs       : in  std_logic;	
      o_irq      : out std_logic;
      i_ilevel        : in std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld    : in std_logic := '1';                           -- interrupt level valid flag
      i_ivector       : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld   : in std_logic := '1';                           -- interrupt vector valid flag
      i_adcdata  : in  std_logic_vector(15 downto 8);  -- ADS8402 upper data bus
      i_udata    : in  std_logic_vector(4 downto 0);   -- User Data lines
      i_aclk     : in  std_logic;                      -- ADC clock
      i_trigger  : in  std_logic;                      -- external trigger
      i_busy     : in  std_logic;                      -- ADS8402 busy
      o_byte     : out std_logic;                      -- ADS8402 byte select strobe
      o_convrt_n : out std_logic;                      -- ADS8402 convert start 
      o_cs_n     : out std_logic;                      -- ADS8402 chip select
      o_rd_n     : out std_logic;                      -- ADS8402 read enable
      o_reset_n  : out std_logic;                      -- ADS8402 reset
      i_fifo_re  : in  std_logic;                      -- optional FIFO read enable (for CE 2 access)
      o_fifo_data: out std_logic_vector(31 downto 0)   -- optional FIFO output enable (for CE 2 access)
    );
end ADS8402;						 
	 