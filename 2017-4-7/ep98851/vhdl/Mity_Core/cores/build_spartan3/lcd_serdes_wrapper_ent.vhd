--- Title: LCD_SerDes_Wrapper.vhd
--- Description: LCD Wrapper to send the signals through SerDes
---
---     o  0
---     | /       Copyright (c) 2007
---    (CL)---o   Critical Link, LLC
---      \
---       O
---
--- Company: Critical Link, LLC.
--- Revisions: 
--- 1.00 2007/03/16 - Get the board up and running...
--- 1.01
--- 1.02

entity lcd_serdes_wrapper is
   generic (
      DECODE_BITS : integer range 1 to 9 := 5;
      BUFF32_ADDR_BITS : integer range 9 to 11 := 9
   );
   port (
      emif_clk	   : in std_logic; -- 50 Mhz EMIF clock
		i_cs        : in std_logic;	--cs for LCD addr range
		i_ABus      : in std_logic_vector(4 downto 0);
		i_DBus      : in std_logic_vector(31 downto 0);
		o_DBus      :out std_logic_vector(31 downto 0);
		i_wr_en     : in std_logic;
		i_rd_en     : in std_logic;
      o_irq           : out std_logic;
      i_ilevel        : in std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
      i_ilevel_vld    : in std_logic := '1';                           -- interrupt level valid flag
      i_ivector       : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
      i_ivector_vld   : in std_logic := '1';                           -- interrupt vector valid flag 

      i_dma_we    : in std_logic;
      i_first_word: in std_logic;
      i_dma_addr  : in std_logic_vector(BUFF32_ADDR_BITS-1 downto 0);
      i_dma_data  : in std_logic_vector(31 downto 0);
      o_dma_trig  :out std_logic;
      o_dma_priority_trig  :out std_logic;

      ts_cs_n     : in std_logic;
      serdes_clk        : in std_logic;
      -- LvDS I/O pins for LCD and some control signals
      o_serdes_clk    :out std_logic;
      o_serdes_ctl    :out std_logic;
      o_serdes_b      :out std_logic;
      o_serdes_g      :out std_logic;
      o_serdes_r      :out std_logic

   );
end lcd_serdes_wrapper;
