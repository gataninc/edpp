---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 23, 2010	
--   Board          : Artemis DPP Board
--   Schematic      : 4035.007.27000 Rev D & E ( Same but different layer order )
--
--   Top Level      : 4035.009.98851, S/W, Artemis, Xilinx
--		110331xx
--			Randomize the data to/from the DSP by XOR bits 31 to 1 by Bit 0, Leave bit 32 intact for data identification purposes
--			This works in both directions and must also be taken into account in the Xilinx FPGA
--- Title: MityDSP_top.vhd
--- Description: Baseline MityDSP or MityDSP-XM MDK-MB FPGA top-level module.
---              Does not include any installed MDK cards.
---
---     o  0
---     | /       Copyright (c) 2005-2009
---    (CL)---o   Critical Link, LLC
---      \
---       O
---
--- Company: Critical Link, LLC.
--- Date: 7/23/2009
--- Version: 1.06
--- MDK Version 2.7.0
---
--- Revision History
---   1.00 - Baseline Template
---   1.01 - Updated to MDK 2.1.0
---   1.02 - Added Dip Switch and Power Switch inputs, additional GPIO
---   1.03 - Updated to MDK 2.4.0
---   1.04 - Fixed GPIN shift ordering (for Rev-B boards)
---   1.05 - Rebuilt for MDK 2.7.0
---   1.06 - Updated to include MityDSP-XM build configuration.
--		1.70 
--		April 5, 2012
--			Module: LTC2207
--				Added input for 1/2 Gain, which now cuts the Dout Gain by a factor of 2 to Discriminators
--		April 4, 2012
--			Changed DSP Code to "Time-Stamp" may steps for specturm clear.
--			No Changed to Altera or Xilinx other than date and version
--		March 27, 2012
--			Added a switch for 1/2 input gain
--				constant CW_HALF_GAIN			: integer := 24;
--			Module: LTC2207
--				Added input for 1/2 Gain, which cuts the DoutM Gain by a factor of 2
--		April 13, 2012
--			Found problem in DSP, with "Spot mapping", the serial port was limiting the speed. Altered DSP code for corrections.
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--- Memory Map Cores
--- 0x000 0 Base Module
--- 0x080 1 UART RS232
--- 0x100 2 UART USB
--- 0x180 3 Ethernet
--- 0x200 4 I2C Real Time Clock
--- 0x280 5 GPOut Chip Selects
--- 0x300 6 GPIn Slow Digital Inputs
--- 0x380 7 GPIn Dip Switch, Power, Display Connector Inputs
--- 0x400 8 Altera Flash Core
---------------------------------------------------------------------------------------------------

library WORK;
	library IEEE;
	library UNISIM;
	
use WORK.MITYDSP_PKG.ALL;
	use IEEE.STD_LOGIC_1164.ALL;
	use IEEE.STD_LOGIC_ARITH.ALL;
	use IEEE.STD_LOGIC_UNSIGNED.ALL;
	use UNISIM.VCOMPONENTS.ALL;

---------------------------------------------------------------------------------------------------
entity ep98851 is
	generic ( 
		DECODE_BITS    		: integer range 1 to 9 := 5; -- (2^DECODE_BITS) cores
		NUM_CTRL_PORTS 		: integer range 1 to 4 := 1; -- Default, but DMA disabled
		MITYCONFIG     		: string := "UNKNOWN" ); -- "UNKNOWN" | "MityDSP" | "MityDSP-XM" set in XCF
	port (
		i_emif_clk	    		: in std_logic; -- 50 MHz EMIF clock
		i_clk25mhz      		: in std_logic; -- 25 MHz clock for watchdog timer logic
--      	i_reset_n         		: in std_logic;  -- active-low reset signal from POR ckt. or manual reset
		io_dsp_rst_n      		: inout std_logic;  -- active-low, open-drain manual reset output/input

		-- DSP BUS Interface
		io_ce_n  				: inout std_logic_vector(3 downto 0);
		o_ce1_en_n 			: out std_logic; 
		io_aoe_n 				: inout std_logic;       --AOE/SDRAS
		io_are_n 				: inout std_logic;       --ARE/SDCAS
		io_awe_n 				: inout std_logic;       --AWE/SDWE
		io_be_n  				: inout std_logic_vector(3 downto 0);
		io_ea    				: inout std_logic_vector(15 downto 2);
		io_ed    				: inout std_logic_vector(31 downto 0);
		o_hold_n       		: out std_logic;
		i_holda_n      		: in std_logic;
		i_bus_req      		: in std_logic;

		-- DSP IRQ lines
		o_dsp_nmi         		: out std_logic; -- DSP's Non-Maskable-Interrupt
		o_dsp_ext_int4    		: out std_logic;
		o_dsp_ext_int5    		: out std_logic;
		o_dsp_ext_int6    		: out std_logic;
		o_dsp_ext_int7    		: out std_logic;

		-- I/O pins for RS 232 interface
		i_rs232_rcv       		: in std_logic;
		o_rs232_xmit      		: out std_logic;

		-- I/O pins for Ethernet Interface
		o_eth_ref_clk     		: out std_logic;
		i_eth_col         		: in std_logic;
		i_eth_crs         		: in std_logic;         
		i_eth_rx_clk	   		: in std_logic;
		i_eth_rx_data     		: in std_logic_vector(3 downto 0);
		i_eth_rx_dv	    		: in std_logic;	
		i_eth_rx_er	    		: in std_logic;			    
		i_eth_tx_clk	    		: in std_logic;	
		o_eth_tx_data     		: out std_logic_vector(3 downto 0);
		o_eth_tx_en	    		: out std_logic;	
		o_eth_rst_n       		: out std_logic;
		o_eth_mdc		    		: out std_logic;
		io_eth_mdio	    		: inout std_logic;

		-- Altera Flash Programming Interface
		i_altera_ns			: in		std_logic;
		i_altera_data0			: in		std_logic;
		i_altera_cdone			: in		std_logic;
		o_altera_dclk			: out	std_logic;
		o_altera_asd			: out	std_logic;
		o_altera_ncs			: out	std_logic;
		o_altera_nce			: out	std_logic;
		o_altera_ncon			: out	std_logic;
		-- Altera Flash Programming Interface
		
		-- Altera Data Bus Interface
		i_altera_int_bar		: in		std_logic;
		i_altera_write_ff_ff	: in		std_logic;
		o_altera_read_ff_ff		: out	std_Logic;
		o_altera_write_busy_bar	: out	std_logic;
		i_altera_read_Busy_bar	: in		std_logic;
		o_altera_Clk			: buffer	std_logic;
		io_altera_dsp			: inout 	std_logic_vector( 32 downto 0 );
--		o_altera_dma_rd		: out	std_logic;
		o_altera_int_busy		: out	std_logic;
		io_altera_spare		: out	std_logic_Vector(  2 downto 0 );
		i_eth_def				: in		std_logic;
		-- Altera Data Bus Interface

		 -- Used only for MITYCONFIG="MityDSP"
		 o_fl_bank_sel_n   		:out std_logic;
		 o_user_led_n      		:out std_logic;
		 -- Used only for MITYCONFIG="MityDSP-XM"
		 o_sba_led         		: out std_logic;
		 o_sba_clk         		: out std_logic;

		o_busy            		:out std_logic );
end ep98851;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
-- ARCHITECTURE
---------------------------------------------------------------------------------------------------
architecture rtl of ep98851 is

	constant FPGA_APPLICATION_ID	: std_logic_vector(7 downto 0) := CONV_STD_LOGIC_VECTOR( 00, 8);

	constant FPGA_YEAR			: std_logic_vector(4 downto 0) := CONV_STD_LOGIC_VECTOR( 13, 5);
	constant FPGA_MONTH			: std_logic_vector(3 downto 0) := CONV_STD_LOGIC_VECTOR(  7, 4);
	constant FPGA_DAY			: std_logic_vector(4 downto 0) := CONV_STD_LOGIC_VECTOR(  1, 5);
	
	constant FPGA_VERSION_MAJOR	: std_logic_vector(3 downto 0) := CONV_STD_LOGIC_VECTOR( 05, 4);		-- 0 to 15
	constant FPGA_VERSION_MINOR	: std_logic_vector(3 downto 0) := CONV_STD_LOGIC_VECTOR( 15, 4);		-- 0 to 15

	constant CORE_BASE_MODULE:     integer := 0;
	
	constant CORE_UART_RS232:      integer := 1;
	constant CORE_UART_RS232_IRQ:  integer := 7;
	constant CORE_UART_RS232_VEC:  integer := 0;
	
	constant CORE_UART_USB:        integer := 2;
	constant CORE_UART_USB_IRQ:    integer := 6;
	constant CORE_UART_USB_VEC:    integer := 0;
	
	constant CORE_ETHERNET:        integer := 3;
	constant CORE_ETHERNET_IRQ:    integer := 5;
	constant CORE_ETHERNET_VEC:    integer := 0;
	
	constant CORE_I2C:             integer := 4;
	constant CORE_I2C_IRQ:         integer := 7;
	constant CORE_I2C_VEC:         integer := 1;
	
	constant CORE_GPOUT:           integer := 5;
	
	constant CORE_GPIN:            integer := 6;
	constant CORE_GPIN_IRQ:        integer := 5;
	constant CORE_GPIN_VEC:        integer := 1;
	
	constant CORE_GPIN_MISC:       integer := 7;
	constant CORE_GPIN_MISC_IRQ:   integer := 5;
	constant CORE_GPIN_MISC_VEC:   integer := 2;
	
	-- Mapping for Altera "GPIO" Core
	constant CORE_ALTERA_BASE:    integer := 8;
	constant CORE_ALTERA_IRQ:   	integer := 5;
	constant CORE_ALTERA_VEC:   	integer := 3;

	----------------------------------------------------------------------------------------------
	-- Component Declarations - most should be in MityDSP_PKG anyway
	----------------------------------------------------------------------------------------------
	component EMIF_iface is
		generic ( 
			DECODE_BITS       			: integer range 1 to 9 := 5;
			INCLUDE_SDRAM_DMA 			: boolean := FALSE;
			NUM_CTRL_PORTS    			: integer range 1 to 4 := 4; -- required if INCLUDE_DMA = true
			CONFIG            			: string := "UNKNOWN"  );       -- "MityDSP" | "MityDSP-XM", required if INCLUDE_DMA = true
		port ( 
			emif_clk  				: in std_logic;
			-- EMIF signals
			i_ce2_n   				: in std_logic;
			i_ce3_n   				: in std_logic;
			i_aoe_n   				: in std_logic;
			i_are_n   				: in std_logic;
			i_awe_n   				: in std_logic;
			i_be_n    				: in std_logic_vector(3 downto 0);
			i_ea     					: in std_logic_vector(15 downto 2);
			i_ed      				: in std_logic_vector(31 downto 0);
			o_ed      				: out std_logic_vector(31 downto 0);
			t_ed      				: out std_logic;
			o_hold_n  				: out std_logic := '1';   
			i_holda_n 				: in std_logic;   
			i_bus_req 				: in std_logic;
			
			-- FPGA fabric signals
			be_r      				: out std_logic_vector(3 downto 0);
			addr_r    				: out std_logic_vector(4 downto 0);
			cs_r      				: out std_logic_vector((2**DECODE_BITS)-1 downto 0);
			ce3_r     				: out std_logic;
			aoe_r     				: out std_logic;
			rd_r      				: out std_logic;
			wr_r      				: out std_logic;
			edi_r     				: out std_logic_vector(31 downto 0);
			edo_ce2   				: in std_logic_vector(31 downto 0);
			edo       				: in bus32_vector((2**DECODE_BITS)-1 downto 0);
			
			----------------------------------------------------------------
			-- The items below this line are only required if INCLUDE_DMA = true
			-- 
			o_ce_n    				: out std_logic_vector(3 downto 0) := "0000";
			o_ras_n   				: out std_logic := '1';       --AOE/SDRAS
			o_cas_n   				: out std_logic := '1';       --ARE/SDCAS
			o_awe_n   				: out std_logic := '1';       --AWE/SDWE
			o_be_n    				: out std_logic_vector(3 downto 0) := "1111";            --DQM3:0
			o_ea      				: out std_logic_vector(15 downto 2) := (others=>'0');    --BA1:0 | A11:0
			t_emif    				: out std_logic := '1';         --Drive CEn, AOE, ARE, AWE, BEn, EAn           
			
			-- SDRAM DMA fabric signals (can leave disconnected if INCLUDE_DMA = false)
			-- DMA Request lines (one normal priority, one high priority)
			-- High priority requests mean "take the EMIF bus from the processor" by asserting hold
			-- Low priority requests mean "wait for the EMIF bus to be free" meaning DSP is not using bus
			i_dma_req          			: in std_logic_vector(NUM_CTRL_PORTS-1 downto 0) := (others=>'0');
			i_dma_priority_req 			: in std_logic_vector(NUM_CTRL_PORTS-1 downto 0) := (others=>'0');
			i_dma_rwn          			: in std_logic_vector(NUM_CTRL_PORTS-1 downto 0) := (others=>'0');
			
			-- For the Input Address only bottom SDRAM_BANK_BITS+SDRAM_ROW_BITS+SDRAM_COL_BITS used (in that order)
			-- this is a 32 bit aligned word address (shift by two bits to translate into DSP byte address)
			i_dma_addr        			: in  bus32_vector(NUM_CTRL_PORTS-1 downto 0) := (others=>(others=>'0')); 
			i_dma_data        			: in  bus32_vector(NUM_CTRL_PORTS-1 downto 0) := (others=>(others=>'0')); 
			-- number of 32 bit words - 1, max 512 at a time (5 uS stall @ 50 Mhz)
			-- Must be a minimum transfer of 4 words (value = 3)
			i_dma_count       			: in  bus9_vector(NUM_CTRL_PORTS-1 downto 0)  := (others=>(others=>'0'));  -- number of 32 bit words, max 512 at a time (6 uS stall @ 50 Mhz)
			i_dma_be          			: in  bus4_vector(NUM_CTRL_PORTS-1 downto 0)  := (others=>(others=>'1')); 
			
			o_dma_wr_stb      			: out std_logic_vector(NUM_CTRL_PORTS-1 downto 0) := (others=>'0');
			o_dma_rd_stb      			: out std_logic_vector(NUM_CTRL_PORTS-1 downto 0) := (others=>'0');
			o_dma_data        			: out std_logic_vector(31 downto 0) := (others=>'0');
			o_dma_done        			: out std_logic_vector(NUM_CTRL_PORTS-1 downto 0)  := (others=>'0'));  -- deassert i_dma_*req on next clock after o_dma_done is high
	end component;
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	component base_module is
		generic (
			CONFIG 					: string := "UNKNOWN" ); -- "MityDSP" | "MityDSP-XM" | "MityDSP-Pro"
		port (
			emif_clk        			: in std_logic;
			i_cs            			: in std_logic;
			i_ID            			: in std_logic_vector(7 downto 0);    -- assigned Application ID number, 0xFF if unassigned
			i_version_major 			: in std_logic_vector(3 downto 0);    -- major version number 1-15
			i_version_minor 			: in std_logic_vector(3 downto 0);    -- minor version number 0-15
			i_year          			: in std_logic_vector(4 downto 0);    -- year since 2000
			i_month         			: in std_logic_vector(3 downto 0);    -- month (1-12)
			i_day           			: in std_logic_vector(4 downto 0);    -- day (1-32)
			i_ABus          			: in std_logic_vector(4 downto 0);
			i_DBus          			: in std_logic_vector(31 downto 0);
			o_DBus          			: out std_logic_vector(31 downto 0);
			i_wr_en         			: in std_logic;
			i_rd_en         			: in std_logic;
			
			i_clk25mhz      			: in  std_logic; -- 25 Mhz input clock domain (BUFG desirable, but should also work with local clock)
			o_clken100ms    			: out std_logic; -- free-running 100ms period clock-enable in the 25mhz domain
			o_clken4ms      			: out std_logic; -- free-running 4 ms period clock-enable in the 25mhz domain
			o_wd_nmi        			: out std_logic; -- watchdog timer non-maskable interrupt output (active-high)
			o_wd_rst        			: out std_logic; -- watchdog timer dsp reset (active-high)
			
			o_emif_dcm_reset  			: out std_logic;                     -- hook to EMIF DCM reset line (not valid if i_clk25mhz is not connected)
			i_emif_dcm_status 			: in  std_logic_vector(2 downto 0);  -- hook to EMIF DCM status lines (not used if i_clk25mhz is not connected)
			i_emif_dcm_lock   			: in  std_logic;                     -- hook to EMIF DCM lock line  (not used if i_clk25mhz is not connected)
			
			-- these signals are valid only for CONFIG="MityDSP" or CONFIG="MityDSP-XM"
			i_irq_map_4x8   			: in  bus8_vector(7 downto 4) := (others=>(others=>'0')); -- ext_irq4,5,6,7
			o_irq_output_4  			: out std_logic_vector(7 downto 4);                       -- ext_irq4,5,6,7
			
			-- these signals are valid only for CONFIG="MityDSP"
			t_bank_sel_n    			: out std_logic; -- need inverter / tri-state buffer at top level; 0=HiZ, 1=GND
			o_led_enable    			: out std_logic; -- need inverter at top level
			i_bank_zero_clr 			: in  std_logic := '0'; -- flash bank zero asynchronous clear (active-high)
			
			-- these signals are valid only for CONFIG="MityDSP-XM"
			o_sba_led       			: out std_logic := '1'; -- serial-bank-address data / user LED (FPGA_INIT_B/LED/SBA)
							 -- direct connection at top level
			t_sba_clk       			: out std_logic := '1'; -- serial-bank-address clock
							 -- need tri-state buffer at top level; 0=GND, 1=HiZ
			
			-- these signals are valid only for CONFIG="MityDSP-Pro"
			i_irq_map_2x16  			: in  bus16_vector(5 downto 4) := (others=>(others=>'0'));     -- DSP_INT4/5
			o_irq_output_2  			: out std_logic_vector(5 downto 4); -- DSP_INT4/5
			o_sba_data      			: out std_logic; -- direct connection at top-level
			o_sba_clk       			: out std_logic; -- direct connection at top-level
			o_led_n         			: out std_logic_vector(1 downto 0); -- active-low LED outputs - direct connection at top-level
			i_config        			: in  std_logic_vector(2 downto 0) := "000" );
		end component;
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	component uart
		Port ( 
			clk 	         				: in std_logic;
			i_rcv          			: in std_logic;
			o_xmit         			: out std_logic;
			o_irq		     		: out std_logic;
			i_ilevel       			: in std_logic_vector(1 downto 0) := "10";       -- interrupt level (0=4,1=5,2=6,3=7)
			i_ilevel_vld   			: in std_logic := '1';                           -- interrupt level valid flag
			i_ivector      			: in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
			i_ivector_vld  			: in std_logic := '1';                           -- interrupt vector valid flag
			i_ABus	     			: in std_logic_vector(4 downto 0);
			i_be           			: in std_logic_vector(3 downto 0);
			i_DBus	     			: in std_logic_vector(31 downto 0);
			o_DBus	     			: out std_logic_vector(31 downto 0);
			i_wr_en	     			: in std_logic;
			i_rd_en	     			: in std_logic;
			i_uart_cs	     			: in std_logic;	
			i_xmit_cts     			: in std_logic := '0';   -- active low
			o_rcv_cts      			: out std_logic;         -- active low
			i_dsr          			: in std_logic := '0';   -- active low
			o_dtr          			: out std_logic;         -- active low
			i_dcd          			: in std_logic := '0';   -- active low
			i_ri           			: in std_logic := '0'    -- active low
			);
	end component;
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	component cl_eth_671x
		port (
			emif_clk    				: in std_logic; -- global buffered EMIF clock
			clk25       				: in std_logic; -- global buffered 25MHz clock
			
			o_ref_clk   				: out std_logic; -- 25MHz reference clock for some PHYs
			o_clk2_5    				: out std_logic; -- 2.5MHz clock for TX Engine, when using CS8952
			tx_clk      				: in std_logic; -- global buffered transmit clock
			rx_clk      				: in std_logic; -- global buffered receive clock
			
			ABus        				: in std_logic_vector(4 downto 0);
			iDBus       				: in std_logic_vector(31 downto 0);
			oDBus       				: out std_logic_vector(31 downto 0);
			wr_en       				: in std_logic;
			rd_en       				: in std_logic;
			eth_cs					: in std_logic;	--sync cs for eth addr range
			
			o_mdc       				: out std_logic;
			o_mdio					: out std_logic;
			i_mdio      				: in std_logic := '0';
			t_mdio      				: out std_logic; -- tri-state the MDIO line
			
			o_tx_spd_100   			: out std_logic;
			i_half_duplex  			: in std_logic := '0';
			
			i_col       				: in std_logic := '0';
			i_crs       				: in std_logic := '0';
			o_tx_data   				: out std_logic_vector(3 downto 0);
			o_tx_en     				: out std_logic;
			o_tx_er     				: out std_logic;
			o_rx_en     				: out std_logic;
			i_rx_data   				: in std_logic_vector(3 downto 0);
			i_rx_dv     				: in std_logic;
			i_rx_er     				: in std_logic;
			
			o_eth_rst					: out std_logic;
			int_rxpkt					: out std_logic;
			
			i_ilevel       			: in    std_logic_vector(1 downto 0) := "01";       -- interrupt level (0=4,1=5,2=6,3=7)
			i_ilevel_vld   			: in    std_logic := '1';                           -- interrupt level valid flag
			i_ivector      			: in    std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
			i_ivector_vld  			: in    std_logic := '1' );              
	end component;
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- EDAX Component Declarations - most should be in MityDSP_PKG anyway
	--------------------------------------------------------------------------		
	component altera_core is
		Port ( 
			emif_clk					: in		std_logic;
			altera_clk				: in		std_logic;
               FPGA_YEAR          			: in		std_logic_vector(4 downto 0);
               FPGA_MONTH         			: in 	std_logic_vector(3 downto 0);
               FPGA_DAY           			: in 	std_logic_vector(4 downto 0);
               FPGA_VERSION_MAJOR			: in 	std_logic_vector(3 downto 0);
               FPGA_VERSION_MINOR			: in 	std_logic_vector(3 downto 0);
               i_ABus     				: in  	std_logic_vector(4 downto 0);
               i_DBus     				: in  	std_logic_vector(31 downto 0);
               o_DBus     				: out 	std_logic_vector(31 downto 0);
               i_wr_en    				: in  	std_logic;
               i_rd_en    				: in	  	std_logic;
               i_cs       				: in  	std_logic;	
               i_ilevel       			: in  	std_logic_vector(1 downto 0) := "01";       -- interrupt level (0=4,1=5,2=6,3=7)
               i_ilevel_vld   			: in  	std_logic := '0';                           -- interrupt level valid flag
               i_ivector      			: in  	std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
               i_ivector_vld  			: in  	std_logic := '0';                           -- interrupt vector valid flag
               -- FLASH ALTERA EEPROM SIGNALS ----------------------------------------------------------
               altera_ns					: in		std_logic;
               altera_data0				: in		std_logic;
               altera_dclk				: out	std_logic;
               altera_asd				: out	std_logic;
               altera_ncs				: out	std_logic;
               altera_nce				: out	std_logic;
               altera_ncon				: out	std_logic;
               altera_cdone				: in	 	std_logic;
               flash_enable				: buffer	std_logic;
			i_eth_def					: in	std_logic;
               -- FLASH ALTERA EEPROM SIGNALS ----------------------------------------------------------
               
               -- ALTERA DATA INTERFACE SIGNALS --------------------------------------------------------
		i_altera_data_in : in std_logic_Vector( 32 downto 0 );
		o_altera_data_out : out std_logic_vector( 32 downto 0 );
		o_altera_data_oen : out std_logic;
		i_altera_read_busy : in std_logic;
		o_altera_write_busy : out std_logic;
		o_altera_rdfifo_empty : out std_logic;
		o_altera_rdfifo_full : out std_logic;		
		i_altera_WFF_FF : in std_logic;
		i_altera_int : in std_logic; --! Active high interrupt from Altera chip.
		o_altera_rdfifo_rd_en : out std_logic;
		o_altera_rdfifo_rd_cnt : out std_logic_Vector( 12 downto 0 );
		o_altera_int_busy : out std_logic; --! When high signals Altera chip that we are still handling interrupt... I think.
               -- ALTERA DATA INTERFACE SIGNALS --------------------------------------------------------
              
               -- DMA INTERFACE SIGNALS ----------------------------------------------------------------
		o_dma_irq : out std_logic;
		o_dma_req : out std_logic := '0';
		o_dma_priority_req : out std_logic := '0';
		o_dma_rwn : out std_logic := '0';
		o_dma_addr : out std_logic_vector(31 downto 0) := (others=>'0');
		o_dma_data : out std_logic_vector(31 downto 0) := (others=>'0');
		o_dma_count : out std_logic_vector(8 downto 0) := (others=>'0');
		o_dma_be : out std_logic_vector(3 downto 0) := "1111";      
		i_dma_rd_stb : in std_logic := '0';
		i_dma_done : in std_logic := '0';
               -- DMA INTERFACE SIGNALS ----------------------------------------------------------------

		o_user_io : buffer std_logic_vector( 31 downto 0 ) );
		end component;						 
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- EDAX Constant/Signal Declarations
	----------------------------------------------------------------------------------------------
	signal altera_ncs					: std_logic;
	signal altera_nce					: std_logic;
	signal altera_ncon					: std_logic;
	signal altera_asd					: std_logic;
	signal altera_dclk					: std_logic;
	signal altera_flash_enable			: std_logic;
			
	signal altera_data_out				: std_logic_Vector( 32 downto 0 );
	signal altera_data_in_mux 			: std_logic_vector( 32 downto 0 );
	
	signal Write_Fifo_ef				: std_logic;
	signal Write_Fifo_ff				: std_logic;
	signal Read_Fifo_ef					: std_logic;
	signal Read_Fifo_ff					: std_logic;					
	signal altera_write_busy				: std_logic;
	signal altera_dsp_oen				: std_logic;
	signal altera_user_io 				: std_logic_vector( 31 downto 0 );		
	signal Read_Fifo_Rd_Stb				: std_logic;
	signal altera_read_ff_ef				: std_logic;
	signal Read_fifo_overflow			: std_logic;
	signal Read_Fifo_Underflow			: std_logic;

	----------------------------------------------------------------------------------------------
	-- MARK_1 Insert Board I/O Component Definitions Here

	-- clock/reset related signals
	signal clk25mhz					: std_logic;
	signal emif_clk					: std_logic;
	signal emif_clk_raw					: std_logic;
	signal emif_clk_dcm					: std_logic;
	signal dcm_lock					: std_logic;
	signal dcm_reset					: std_logic;
	signal dcm_status					: std_logic_vector(7 downto 0);
	signal dcm_reset_shift				: std_logic_vector(15 downto 0) := "0111111111111111";
	signal serdes_clk150_fx				: std_Logic;
	signal serdes_clk150 				: std_logic;
	signal wd_rst						: std_logic;
	signal wd_nmi						: std_logic;
	
	-- EMIF Interface signals
	signal o_ed						: std_logic_vector(31 downto 0);
	signal t_ed						: std_logic;
	signal be_r						: std_logic_vector(3 downto 0);
	signal addr_r						: std_logic_vector(4 downto 0);
	signal cs_r						: std_logic_vector((2**DECODE_BITS)-1 downto 0);
	signal rd_r						: std_logic;
	signal wr_r						: std_logic;
	signal edi_r						: std_logic_vector(31 downto 0);
	signal edo						: bus32_vector((2**DECODE_BITS)-1 downto 0) := (others=>(others=>'X'));
	signal ce_n  						: std_logic_vector(3 downto 0) := "1111";  
	signal aoe_n 						: std_logic := '1';    
	signal are_n 						: std_logic := '1';    
	signal awe_n 						: std_logic := '1';    
	signal be_n  						: std_logic_vector(3 downto 0) := "1111";    
	signal ea    						: std_logic_vector(15 downto 2) := (others=>'0');
	signal t_emif 						: std_logic := '1';     
	signal busy   						: std_logic := '0';
	
	-- DMA signals (not currently used)
	signal dma_req          				: std_logic_vector(NUM_CTRL_PORTS-1 downto 0) := (others=>'0');
	signal dma_priority_req 				: std_logic_vector(NUM_CTRL_PORTS-1 downto 0) := (others=>'0');
	signal dma_rwn          				: std_logic_vector(NUM_CTRL_PORTS-1 downto 0) := (others=>'0');
	signal dma_addr         				: bus32_vector(NUM_CTRL_PORTS-1 downto 0) := (others=>(others=>'0')); 
	signal dma_data         				: bus32_vector(NUM_CTRL_PORTS-1 downto 0) := (others=>(others=>'0')); 
	signal dma_count        				: bus9_vector(NUM_CTRL_PORTS-1 downto 0)  := (others=>(others=>'0'));
	signal dma_be           				: bus4_vector(NUM_CTRL_PORTS-1 downto 0)  := (others=>(others=>'1')); 
	signal dma_wr_stb       				: std_logic_vector(NUM_CTRL_PORTS-1 downto 0) := (others=>'0');
	signal dma_rd_stb       				: std_logic_vector(NUM_CTRL_PORTS-1 downto 0) := (others=>'0');
	signal dma_done         				: std_logic_vector(NUM_CTRL_PORTS-1 downto 0)  := (others=>'0');
		

	-- Base Module signals	
	signal irq_map						: bus8_vector(7 downto 4);
	signal t_fl_bank_sel_n				: std_logic;
	signal led_enable					: std_logic;
	signal sba_led						: std_logic;
	signal sba_clk						: std_logic;
	
	
	-- Ethernet signals
	signal ref_clk    					: std_logic;
	signal rx_clk     					: std_logic;
	signal tx_clk     					: std_logic;
	signal o_eth_mdio 					: std_logic;
	signal t_eth_mdio 					: std_logic;
	signal tx_spd_100 					: std_logic;
	signal phy_rst    					: std_logic;
		

	-- SERDES (GPOutputs) signals
	
	-- Video Signals
	signal dip_sw 						: std_logic_vector(3 downto 0) := "0000";
	signal pow_sw 						: std_logic := '0';

	----------------------------------------------------------------------------------------------
	-- MARK_2 Insert Additional Signal Names Required In The Top Level Here
	----------------------------------------------------------------------------------------------
	
	begin 		-- architecture: rtl
		-----------------------------------------------------------------------------------------
		-- System Clock Buffer Instantiation
		-----------------------------------------------------------------------------------------
		clk25mhz_bufg: bufg 
			port map (
				I 	=> i_clk25mhz, 
				O 	=> clk25mhz);
				
		emif_clk_ibufg: ibufg 
			port map (
				I 	=> i_emif_clk, 
				O 	=> emif_clk_raw);
				
		emif_clk_bufg: bufg 
			port map (
				I 	=> emif_clk_dcm, 
				O 	=> emif_clk);

		altera_clk_buf : bufg
			port map(
				i	=> emif_clk_dcm,
				o 	=> o_altera_clk );
		-- o => o_altera_clk			<= emif_clk;
				
		serdes_clk150_bufg: bufg 
			port map (
				I 	=> serdes_clk150_fx, 
				O 	=> serdes_clk150);
		
		eth_rx_bufg: bufg 
			port map (
				i 	=> i_eth_rx_clk, 
				o 	=> rx_clk);
				
		eth_tx_bufg: bufg 
			port map (
				i 	=> i_eth_tx_clk, 
				o 	=> tx_clk);

		-----------------------------------------------------------------------------------------
		-- System DCM Instantiation
		-----------------------------------------------------------------------------------------
		DCM_emif_serdes : DCM
		   generic map (
			 CLKDV_DIVIDE 			=> 2.0,       	--  Divide by: 1.5,2.0,2.5,3.0,3.5,4.0,4.5,5.0,5.5,6.0,6.5
											--     7.0,7.5,8.0,9.0,10.0,11.0,12.0,13.0,14.0,15.0 or 16.0
			 CLKFX_DIVIDE 			=> 1,         	--  Can be any interger from 1 to 32
			 CLKFX_MULTIPLY 		=> 3,       	--  Can be any integer from 1 to 32
			 CLKIN_DIVIDE_BY_2 		=> FALSE,      --  TRUE/FALSE to enable CLKIN divide by two feature
			 CLKIN_PERIOD 			=> 20.0,       --  Specify period of input clock
			 CLKOUT_PHASE_SHIFT 	=> "NONE",    	--  Specify phase shift of NONE, FIXED or VARIABLE
			 CLK_FEEDBACK 			=> "1X",       --  Specify clock feedback of NONE, 1X or 2X
			 DESKEW_ADJUST 		=> "SYSTEM_SYNCHRONOUS", --  SOURCE_SYNCHRONOUS, SYSTEM_SYNCHRONOUS or
											--     an integer from 0 to 15
			 DFS_FREQUENCY_MODE 	=> "LOW",     	--  HIGH or LOW frequency mode for frequency synthesis
			 DLL_FREQUENCY_MODE 	=> "LOW",     	--  HIGH or LOW frequency mode for DLL
			 DUTY_CYCLE_CORRECTION 	=> TRUE,   	--  Duty cycle correction, TRUE or FALSE
			 FACTORY_JF 			=> X"8080",    --  FACTORY JF Values
			 PHASE_SHIFT 			=> 0,          --  Amount of fixed phase shift from -255 to 255
			 STARTUP_WAIT 			=> FALSE)     	--  Delay configuration DONE until DCM LOCK, TRUE/FALSE
		   port map (
			 CLK0 				=> emif_clk_dcm,    	-- 0 degree DCM CLK ouptput
			 CLK180 				=> open,      			-- 180 degree DCM CLK output
			 CLK270 				=> open,       		-- 270 degree DCM CLK output
			 CLK2X 				=> open,       		-- 2X DCM CLK output
			 CLK2X180 			=> open,          		-- 2X, 180 degree DCM CLK out
			 CLK90 				=> open,             	-- 90 degree DCM CLK output
			 CLKDV 				=> open,             	-- Divided DCM CLK out (CLKDV_DIVIDE)
			 CLKFX 				=> serdes_clk150_fx,	-- DCM CLK synthesis out (M/D)
			 CLKFX180 			=> open,          		-- 180 degree CLK synthesis out
			 LOCKED 				=> dcm_lock,        	-- DCM LOCK status output
			 PSDONE 				=> open,            	-- Dynamic phase adjust done output
			 STATUS 				=> dcm_status,      	-- 8-bit DCM status bits output
			 CLKFB 				=> emif_clk,         	-- DCM clock feedback
			 CLKIN 				=> emif_clk_raw,     	-- Clock input (from IBUFG, BUFG or DCM)
			 PSCLK 				=> '0',              	-- Dynamic phase adjust clock input
			 PSEN 				=> '0',               	-- Dynamic phase adjust enable input
			 PSINCDEC 			=> '0',           		-- Dynamic phase adjust increment/decrement
			 RST 				=> dcm_reset );          -- DCM asynchronous reset input

		-----------------------------------------------------------------------------------------
		-- EMIF Interface Module: CECTL3=0x10410420
		-----------------------------------------------------------------------------------------
		inst_emif_iface: emif_iface
			generic map( 
				DECODE_BITS       	=> DECODE_BITS,
				INCLUDE_SDRAM_DMA 	=> TRUE, -- FALSE,
				NUM_CTRL_PORTS    	=> NUM_CTRL_PORTS,
				CONFIG            	=> MITYCONFIG)
		   port map(
				 emif_clk 		=> emif_clk,
				 i_ce2_n 			=> io_ce_n(2),
				 i_ce3_n 			=> io_ce_n(3),
				 i_aoe_n 			=> io_aoe_n,
				 i_are_n 			=> io_are_n,
				 i_awe_n 			=> io_awe_n,
				 i_be_n 			=> io_be_n,
				 i_ea 			=> io_ea,
				 i_ed 			=> io_ed,
				 o_ed 			=> o_ed,
				 t_ed 			=> t_ed,
				 o_hold_n  		=> o_hold_n,   
				 i_holda_n 		=> i_holda_n,
				 i_bus_req 		=> i_bus_req,
				 be_r 			=> be_r,
				 addr_r 			=> addr_r,
				 cs_r 			=> cs_r,
				 ce3_r 			=> open,
				 aoe_r 			=> open,
				 rd_r 			=> rd_r,
				 wr_r 			=> wr_r,
				 edi_r 			=> edi_r,
				 edo_ce2 			=> x"00000000",
				 edo 			=> edo,
				 o_ce_n    		=> ce_n,
				 o_ras_n   		=> aoe_n,      --AOE/SDRAS
				 o_cas_n   		=> are_n,      --ARE/SDCAS
				 o_awe_n   		=> awe_n,      --AWE/SDWE
				 o_be_n    		=> be_n,       --DQM3:0
				 o_ea     		=> ea,         --BA1:0 | A11:0
				 t_emif    		=> t_emif,     --Drive CEn, AOE, ARE, AWE, BEn, EAn           
				 i_dma_req          => dma_req,
				 i_dma_priority_req => dma_priority_req,
				 i_dma_rwn          => dma_rwn,
				 i_dma_addr         => dma_addr,
				 i_dma_data         => dma_data,
				 i_dma_count        => dma_count,
				 i_dma_be           => dma_be ,      
				 o_dma_wr_stb       => dma_wr_stb,
				 o_dma_rd_stb       => dma_rd_stb,
				 o_dma_data         => open,
				 o_dma_done         => dma_done );
		
		io_ed    	<= o_ed  when( t_ed		= '0' ) else (others=>'Z');
		io_ce_n  	<= ce_n  when( t_emif	= '0' ) else (others=>'Z');
		io_aoe_n 	<= aoe_n when( t_emif	= '0' ) else 'Z';
		io_are_n 	<= are_n when( t_emif	= '0' ) else 'Z';
		io_awe_n 	<= awe_n when( t_emif	= '0' ) else 'Z';
		io_be_n  	<= be_n  when( t_emif	= '0' ) else (others=>'Z');    
		io_ea    	<= ea    when( t_emif	= '0' ) else (others=>'Z');
		o_ce1_en_n <= '0';

--		-----------------------------------------------------------------------------------------
--		-- DMA Writter 
--		-----------------------------------------------------------------------------------------
--		dma_writer_inst : dma_writer
--			generic map (
--				FIFO_DEPTH_TWO_TO_N => 13, 		-- : integer range 9 to 15   := 10; -- 2^10 = 1024 32-bit words
--				DMA_TRANSFER_SIZE  	=> 512 )		-- was 512 : integer range 64 to 512 := 256 ); -- max number of words to transfer at a time
--			port map(
--				emif_clk      		=> emif_clk,
--	 
--				-- FIFO read interface (note FIFO read clock must be attached to EMIF clock!)
--				 o_fifo_rd     	=> altera_read_fifo_rd,
--				 i_fifo_dout   	=> altera_read_fifo_dout,
--				 i_fifo_rd_cnt 	=> altera_read_fifo_rd_cnt,		-- Fifo's read count
--				 o_interrupt_count	=> altera_dma_interrupt_count,
--		   		 i_altera_interrupt => Altera_Irq,
--				 i_altera_dma_req	=> Altera_Dma_Req,
--				 i_Altera_Dma_En 	=> Altera_Dma_En,				
--
--				 -- control interface (from parent core, EMIF clock domain)
--				 i_dma_en         	=> dma_en,
--				 i_dma_start_addr 	=> dma_start_addr,		-- word offsets in SDRAM CE space
--				 i_dma_end_addr   	=> dma_end_addr, 		-- word offsets in SDRAM CE space (last word offset - 1)
--				 i_dma_int_size   	=> dma_int_size, 		-- words per interrupt
--				 i_clear_irq      	=> dma_irq_clear,			-- set to '1' to clear o_dma_irq 
--				 o_dma_irq        	=> irq_map(CORE_ALTERA_IRQ)(CORE_ALTERA_VEC),
--				 o_curr_dma_ptr   	=> curr_dma_ptr,
--			    
--				 -- MityDSP EMIF DMA interface (not available for MityDSP-PRO designs)
--				 o_dma_req          => dma_req(0),			-- Hook to EMIF
--				 o_dma_priority_req => dma_priority_req(0),	-- Hook to EMIF
--				 o_dma_rwn          => dma_rwn(0),			-- Hook to EMIF
--				 o_dma_addr         => dma_addr(0),		-- Hook to EMIF
--				 o_dma_data         => dma_data(0),		-- Hook to EMIF
--				 o_dma_count        => dma_count(0),		-- Hook to EMIF
--				 o_dma_be           => dma_be(0),			-- Hook to EMIF
--				 i_dma_rd_stb       => dma_rd_stb(0),		-- Hook to EMIF
--				 i_dma_done         => dma_done(0) );		-- Hook to EMIF
				 
		-----------------------------------------------------------------------------------------
		-- Base Module
		-----------------------------------------------------------------------------------------
		inst_base_module: base_module
			generic map(
				CONFIG => MITYCONFIG ) -- "MityDSP" | "MityDSP-XM" | "MityDSP-Pro" 
			port map(
				emif_clk 			=> emif_clk,
				i_cs 			=> cs_r(CORE_BASE_MODULE),
				i_ID 			=> FPGA_APPLICATION_ID,
				i_version_major 	=> FPGA_VERSION_MAJOR,
				i_version_minor 	=> FPGA_VERSION_MINOR,
				i_year  			=> FPGA_YEAR,
				i_month 			=> FPGA_MONTH,
				i_day 			=> FPGA_DAY,
				i_ABus 			=> addr_r,
				i_DBus 			=> edi_r,
				o_DBus 			=> edo(CORE_BASE_MODULE),
				i_wr_en 			=> wr_r,
				i_rd_en 			=> rd_r,
				i_clk25mhz 		=> clk25mhz,
				o_clken100ms 		=> open,
				o_wd_nmi 			=> wd_nmi,
				o_wd_rst 			=> wd_rst,			
				o_emif_dcm_reset	=> dcm_reset,
				i_emif_dcm_status 	=> dcm_status(2 downto 0),
				i_emif_dcm_lock   	=> dcm_lock,			
				i_irq_map_4x8     	=> irq_map,
				o_irq_output_4(4) 	=> o_dsp_ext_int4,
				o_irq_output_4(5) 	=> o_dsp_ext_int5,
				o_irq_output_4(6) 	=> o_dsp_ext_int6,
				o_irq_output_4(7) 	=> o_dsp_ext_int7,				 
				t_bank_sel_n     	=> t_fl_bank_sel_n,
				o_led_enable    	=> led_enable,
				i_bank_zero_clr 	=> '0',
                    -- Only Valid for MityDSP
				o_sba_led 		=> sba_led,
				t_sba_clk 		=> sba_clk );

			o_dsp_nmi       <= wd_nmi;
			io_dsp_rst_n    <= '0' when wd_rst='1' else 'Z';

			bank_select_std : if MITYCONFIG="MityDSP" generate
			begin
				o_fl_bank_sel_n <= '0' when t_fl_bank_sel_n='1' else 'Z'; -- use board pull-up to 3.3V
				o_user_led_n    <= not led_enable;
			end generate;

			bank_select_xm : if MITYCONFIG="MityDSP-XM" generate
			begin
				o_sba_led <= sba_led;
				o_sba_clk <= '0' when sba_clk = '0' else 'Z'; -- use board pull-up to 3.3V
			end generate;

			assert (MITYCONFIG="MityDSP" or MITYCONFIG="MityDSP-XM")
				report "MITYCONFIG generic must be set to MityDSP or MityDSP-XM."
				severity FAILURE;

		-----------------------------------------------------------------------------------------
		-- Uart1 : Serial Port
		-----------------------------------------------------------------------------------------
		uart1 : uart
			port map ( 
				clk        		=> emif_clk,
				i_be       		=> be_r,
				i_ABus     		=> addr_r,
				i_DBus     		=> edi_r,
				o_DBus     		=> edo(CORE_UART_RS232),
				i_wr_en    		=> wr_r,
				i_rd_en    		=> rd_r,
				i_uart_cs  		=> cs_r(CORE_UART_RS232),
				i_ilevel      		=> CONV_STD_LOGIC_VECTOR( CORE_UART_RS232_IRQ-4, 2),
				i_ilevel_vld  		=> '1',
				i_ivector     		=> CONV_STD_LOGIC_VECTOR( CORE_UART_RS232_VEC, 5),
				i_ivector_vld 		=> '1',
				o_irq      		=> irq_map(CORE_UART_RS232_IRQ)(CORE_UART_RS232_VEC),		
				i_rcv      		=> i_rs232_rcv,
				o_xmit     		=> o_rs232_xmit,
				i_xmit_cts 		=> '0',
				o_rcv_cts  		=> open,
				i_dsr      		=> '0',
				o_dtr      		=> open,
				i_dcd      		=> '0',
				i_ri       		=> '0' );
		
		-----------------------------------------------------------------------------------------
		-- Ethernet MAC
		-----------------------------------------------------------------------------------------
		emac: cl_eth_671x
			PORT MAP(
				emif_clk       	=> emif_clk,
				ABus           	=> addr_r,
				iDBus          	=> edi_r,
				oDBus          	=> edo(CORE_ETHERNET),
				wr_en          	=> wr_r,
				rd_en          	=> rd_r,
				eth_cs         	=> cs_r(CORE_ETHERNET),
				i_ilevel       	=> CONV_STD_LOGIC_VECTOR( CORE_ETHERNET_IRQ-4, 2),
				i_ilevel_vld   	=> '1',
				i_ivector      	=> CONV_STD_LOGIC_VECTOR( CORE_ETHERNET_VEC, 5),
				i_ivector_vld  	=> '1',
				int_rxpkt      	=> irq_map(CORE_ETHERNET_IRQ)(CORE_ETHERNET_VEC),				
				clk25          	=> clk25mhz,
				o_ref_clk      	=> ref_clk,
				o_clk2_5       	=> open,
				tx_clk         	=> tx_clk,
				rx_clk         	=> rx_clk,
				o_mdc          	=> o_eth_mdc,
				o_mdio         	=> o_eth_mdio,
				i_mdio         	=> io_eth_mdio,
				t_mdio         	=> t_eth_mdio,
				o_tx_spd_100   	=> tx_spd_100,
				i_half_duplex  	=> '0',
				i_col          	=> i_eth_col,
				i_crs          	=> i_eth_crs,
				o_tx_data      	=> o_eth_tx_data,
				o_tx_en        	=> o_eth_tx_en,
				o_tx_er        	=> open,
				o_rx_en        	=> open,
				i_rx_data      	=> i_eth_rx_data,
				i_rx_dv        	=> i_eth_rx_dv,
				i_rx_er        	=> i_eth_rx_er,
				o_eth_rst      	=> phy_rst );
			
		o_eth_ref_clk   <= clk25mhz;
		io_eth_mdio     <= o_eth_mdio when t_eth_mdio='0' else 'Z';
		o_eth_rst_n     <= not phy_rst;
		
		-----------------------------------------------------------------------------------------
		-- EDAX
		-- Altera Flash Interface
		-----------------------------------------------------------------------------------------
		mb_altera_core : altera_core
			Port Map ( 
				emif_clk			=> emif_clk,
				altera_clk		=> o_altera_Clk,
				FPGA_YEAR			=> FPGA_YEAR,
				FPGA_MONTH		=> FPGA_MONTH,
				FPGA_DAY			=> FPGA_DAY,
				FPGA_VERSION_MAJOR	=> FPGA_VERSION_MAJOR,
				FPGA_VERSION_MINOR 	=> FPGA_VERSION_MINOR,
				i_ABus         	=> addr_r,
				i_DBus         	=> edi_r,
				o_DBus         	=> edo(CORE_ALTERA_BASE),
				i_wr_en        	=> wr_r,
				i_rd_en        	=> rd_r,
				i_cs           	=> cs_r(CORE_ALTERA_BASE),	
				i_ilevel       	=> "00",
				i_ilevel_vld   	=> '1',
				i_ivector      	=> "00000",
				i_ivector_vld  	=> '1',
                    -- FLASH ALTERA EEPROM SIGNALS ----------------------------------------------------------
				altera_ns			=> i_altera_ns,
				altera_data0		=> i_altera_data0,
				altera_dclk		=> altera_dclk,
				altera_asd		=> altera_asd,
				altera_ncs		=> altera_ncs,
				altera_nce		=> altera_nce,
				altera_ncon		=> altera_ncon,
				altera_cdone		=> i_altera_cdone,							
				flash_enable		=> altera_flash_enable,
				i_eth_def			=> i_eth_def,
                    -- FLASH ALTERA EEPROM SIGNALS ----------------------------------------------------------
                    
                    -- ALTERA DATA INTERFACE SIGNALS --------------------------------------------------------
		    i_altera_data_in => io_altera_dsp, 
		    o_altera_data_out => altera_data_out,
		    o_altera_data_oen => altera_dsp_oen,
		    i_altera_read_busy => not( i_altera_read_Busy_bar ),
		    o_altera_write_busy => altera_write_busy,
		    o_altera_rdfifo_empty => altera_read_ff_ef,
		    o_altera_rdfifo_full => o_altera_read_ff_ff,
		    i_altera_WFF_FF => i_altera_write_ff_ff,
		    i_altera_int => not( i_altera_int_bar ),
		    o_altera_rdfifo_rd_en => Read_Fifo_Rd_Stb,
		    o_altera_int_busy => o_altera_int_busy,
				
                    -- ALTERA DATA INTERFACE SIGNALS --------------------------------------------------------
   
                    -- DMA INTERFACE SIGNALS ----------------------------------------------------------------
				 o_dma_irq        	=> irq_map(CORE_ALTERA_IRQ)(CORE_ALTERA_VEC),
				 o_dma_req          => dma_req(0),			-- Hook to EMIF
				 o_dma_priority_req => dma_priority_req(0),	-- Hook to EMIF
				 o_dma_rwn          => dma_rwn(0),			-- Hook to EMIF
				 o_dma_addr         => dma_addr(0),		-- Hook to EMIF
				 o_dma_data         => dma_data(0),		-- Hook to EMIF
				 o_dma_count        => dma_count(0),		-- Hook to EMIF
				 o_dma_be           => dma_be(0),			-- Hook to EMIF
				 i_dma_rd_stb       => dma_rd_stb(0),		-- Hook to EMIF
				 i_dma_done         => dma_done(0),		-- Hook to EMIF
                    -- DMA INTERFACE SIGNALS ----------------------------------------------------------------
				o_user_io			=> altera_user_io );
				

			o_Altera_NCS  			<= 'Z' 	when ( altera_flash_enable = '0' ) else altera_ncs;
			o_Altera_NCE			<= 'Z' 	when ( altera_flash_enable = '0' ) else altera_nce;
			o_Altera_NCon			<= 'Z' 	when ( altera_flash_enable = '0' ) else altera_ncon;
			o_Altera_ASD			<= 'Z' 	when ( altera_flash_enable = '0' ) else altera_asd;
			o_Altera_DClk			<= 'Z' 	when ( altera_flash_enable = '0' ) else altera_dclk;

			-- End the Read to Altera so it can maintain bytes/sec and feed it into the stream
               
--			o_altera_dma_rd 	<= dma_rd_stb(0);		-- Reads the ALTERA_CORE_READ_FIFO
			io_altera_spare(2)  <= irq_map(CORE_ALTERA_IRQ)(CORE_ALTERA_VEC);
			io_altera_spare(1) <= '0';
			io_altera_spare(0)	<= dma_req(0);

--			io_altera_spare(2)	<= Read_Fifo_Rd_Stb;
--			io_altera_spare(1)	<= Read_fifo_overflow;
--			io_altera_spare(0)	<= Read_Fifo_Underflow;

			altera_dsp_clock_proc : process( emif_clk )
			begin
				if(( emif_clk'event ) and ( emif_clk = '1' )) then
					o_altera_write_busy_bar	<= not( altera_write_busy );
					if( altera_dsp_oen = '1' )
						then io_altera_dsp	<= altera_data_out;
						else io_altera_dsp	<= (others=>'Z');
					end if;				
				end if;
			end process;
				
			--------------------------------------------------------------------------------
			-- Top Level I/O Assignments
			--------------------------------------------------------------------------------
			busy 		<= not dcm_lock when (io_ce_n(2)='0' or io_ce_n(3)='0') else '0';
			o_busy 	<= busy;
end rtl;		-- ep98850.vhd
---------------------------------------------------------------------------------------------------
