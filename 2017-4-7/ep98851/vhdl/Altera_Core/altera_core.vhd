-------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------
-- Title: ateraflash.vhd
-- Description: This module interfaces to the MityDSP for programming the 
-- Altera FLASH EEPROM
-- Write Data to the Altera FPGA
-- Read Data from the Altera FPGA
-- Process DMA transactions based on the data "Read FIFO";
--
--
--     o  0                          
--     | /       Copyright (c) 2005-2006
--    (CL)---o   Critical Link, LLC  
--      \                            
--       O                           
--
-- Company: Critical Link, LLC.
-- Date: 03/15/2006
-- Version: 
--   1.0 10/13/2005 Initial Version
--   1.1 12/09/2005 Reversed Tristate
--   1.2 03/15/2005 1-based NUM_IO, No Async Reset
---------------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library Unisim;
use Unisim.Vcomponents.all;

---------------------------------------------------------------------------------------------------
entity altera_core is
	Port ( 
		emif_clk : in std_logic;
		altera_clk : in std_logic;
		FPGA_YEAR : in std_logic_vector(4 downto 0);
		FPGA_MONTH : in std_logic_vector(3 downto 0);
		FPGA_DAY : in std_logic_vector(4 downto 0);
		FPGA_VERSION_MAJOR : in std_logic_vector(3 downto 0);
		FPGA_VERSION_MINOR : in std_logic_vector(3 downto 0);
		i_ABus : in std_logic_vector(4 downto 0);
		i_DBus : in std_logic_vector(31 downto 0);
		o_DBus : out std_logic_vector(31 downto 0);
		i_wr_en : in std_logic;
		i_rd_en : in std_logic;
		i_cs : in std_logic;	
		i_ilevel : in std_logic_vector(1 downto 0) := "01"; -- interrupt level (0=4,1=5,2=6,3=7)
		i_ilevel_vld : in std_logic := '0'; -- interrupt level valid flag
		i_ivector : in std_logic_vector(4 downto 0) := "00000"; -- interrupt vector (0 through 31)
		i_ivector_vld : in std_logic := '0'; -- interrupt vector valid flag

		-- FLASH ALTERA EEPROM SIGNALS ----------------------------------------------------------
		altera_ns : in std_logic;
		altera_data0 : in std_logic;
		altera_dclk : out std_logic;
		altera_asd : out std_logic;
		altera_ncs : out std_logic;
		altera_nce : out std_logic;
		altera_ncon : out std_logic;
		altera_cdone : in std_logic;
		flash_enable : buffer std_logic;
		i_eth_def : in std_logic;
		-- FLASH ALTERA EEPROM SIGNALS ----------------------------------------------------------

		-- ALTERA DATA INTERFACE SIGNALS --------------------------------------------------------
		i_altera_data_in : in std_logic_Vector( 32 downto 0 );
		o_altera_data_out : out std_logic_vector( 32 downto 0 );
		o_altera_data_oen : out std_logic;
		i_altera_read_busy : in std_logic;
		o_altera_write_busy : out std_logic;
		o_altera_rdfifo_empty : out std_logic;
		o_altera_rdfifo_full : out std_logic;		
		i_altera_WFF_FF : in std_logic;
		i_altera_int : in std_logic; --! Active high interrupt from Altera chip.
		o_altera_rdfifo_rd_en : out std_logic;
		o_altera_rdfifo_rd_cnt : out std_logic_Vector( 12 downto 0 );
		o_altera_int_busy : out std_logic; --! When high signals Altera chip that we are still handling interrupt... I think.
		-- ALTERA DATA INTERFACE SIGNALS ---------------------------------------------------------

		-- DMA INTERFACE SIGNALS ----------------------------------------------------------------
		o_dma_irq : out std_logic;
		o_dma_req : out std_logic := '0';
		o_dma_priority_req : out std_logic := '0';
		o_dma_rwn : out std_logic := '0';
		o_dma_addr : out std_logic_vector(31 downto 0) := (others=>'0');
		o_dma_data : out std_logic_vector(31 downto 0) := (others=>'0');
		o_dma_count : out std_logic_vector(8 downto 0) := (others=>'0');
		o_dma_be : out std_logic_vector(3 downto 0) := "1111";      
		i_dma_rd_stb : in std_logic := '0';
		i_dma_done : in std_logic := '0';
		-- DMA INTERFACE SIGNALS ----------------------------------------------------------------

		o_user_io : buffer std_logic_vector( 31 downto 0 ) );		
end altera_core;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--*
--* @short Register Transfer Logic Implementation of gpio core entity.
--* 
--/
architecture rtl of altera_core is

	constant CORE_APPLICATION_ID : std_logic_vector(7 downto 0) := CONV_STD_LOGIC_VECTOR( 129, 8);		-- Used by Critical Link and net_drvr_b API
	constant CORE_APP_MAJOR : std_logic_vector(3 downto 0) := CONV_STD_LOGIC_VECTOR( 04, 4);
	constant CORE_APP_MINOR : std_logic_vector(3 downto 0) := CONV_STD_LOGIC_VECTOR( 00, 4);

	-- Gets formatted to:
	-- Minor	3:0
	-- Major	7:4
	-- App ID	15:8
	-- Day		20:16
	-- Month	24:21
	-- Year		30:25

	constant MAX_DMA_TRANSFER_SIZE : integer := 512; --! Max number of words to a DMA transfer at a time. (Limited by bus size). 

	-- Translates constants into a predefined Version number
	component core_version
		port (
			clk           : in std_logic;                       -- system clock
			rd            : in std_logic;                       -- read enable
			ID            : in std_logic_vector(7 downto 0);    -- assigned ID number, 0xFF if unassigned
			version_major : in std_logic_vector(3 downto 0);    -- major version number 1-15
			version_minor : in std_logic_vector(3 downto 0);    -- minor version number 0-15
			year          : in std_logic_vector(4 downto 0);    -- year since 2000
			month         : in std_logic_vector(3 downto 0);    -- month (1-12)
			day           : in std_logic_vector(4 downto 0);    -- day (1-32)
			ilevel        : in std_logic_vector(1 downto 0) := "00";       -- interrupt level (0=4,1=5,2=6,3=7)
			ilevel_vld    : in std_logic := '0';                           -- interrupt level valid flag
			ivector       : in std_logic_vector(4 downto 0) := "00000";    -- interrupt vector (0 through 31)
			ivector_vld   : in std_logic := '0';                           -- interrupt vector valid flag
			o_data        : out std_logic_vector(31 downto 0) );
	end component;
	
	component Altera_WriteFifo
		port (
			din : IN std_logic_VECTOR(32 downto 0);
			rd_clk : IN std_logic;
			rd_en : IN std_logic;
			rst : IN std_logic;
			wr_clk : IN std_logic;
			wr_en : IN std_logic;
			dout : OUT std_logic_VECTOR(32 downto 0);
			wr_data_count : out std_logic_Vector( 9 downto 0 );
			empty : OUT std_logic;
			full : OUT std_logic);
	END component;
	
	component Altera_ReadFifo
		port 
		(
			rst : IN STD_LOGIC;
			wr_clk : IN STD_LOGIC;
			rd_clk : IN STD_LOGIC;
			din : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
			wr_en : IN STD_LOGIC;
			rd_en : IN STD_LOGIC;
			dout : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
			full : OUT STD_LOGIC;
			empty : OUT STD_LOGIC;
			valid : OUT STD_LOGIC;
			rd_data_count : OUT STD_LOGIC_VECTOR(12 DOWNTO 0)
		);
	end component;
	---------------------------------------------------------------------------------------------

	---------------------------------------------------------------------------------------------
	-- Constants for Altera ---------------------------------------------------------------------
	-- Address Decodes for this core ------------------------------------------------------------
	constant iADR_RD_VER : integer := 0;
	constant iADR_RD_VER1 : integer := 1;
	constant iADR_RD_FLASH_STATUS : integer := 2;
	constant iADR_RD_FLASH_DATA : integer := 3;	
	
	constant iADR_WR_FLASH_ENABLE : integer := 2;
	constant iADR_WR_NCS : integer := 3;
	constant iADR_WR_NCE : integer := 4;
	constant iADR_WR_NCON : integer := 5;
	constant iADR_WR_BYTE_MSB : integer := 6;
	constant iADR_WR_BYTE_LSB : integer := 7;
	constant iADR_WR_BYTE_READ : integer := 8;
	constant iADR_WR_FPGA_ADDR : integer := 9;	
	constant iADR_WR_FPGA_DATA : integer := 10;
	constant iADR_DMA_EN_REG : integer := 12;
	constant iADR_DMA_IRQ_CLR : integer := 13;
	constant iADR_DMA_START_ADDR : integer := 14;
	constant iADR_DMA_SPACE_MASK : integer := 15;
	constant iADR_DMA_INT_THRESHOLD : integer := 17;
	constant iADR_DMA_INT_COUNT : integer := 18;
	constant iADR_DMA_XFER_START_ADDR : integer := 21;
	constant iADR_ETH_DEF : integer := 22;
	constant iADR_RD_FPGA_STATUS : integer := 23;	
	constant iADR_RD_FPGA_DATA : integer := 24;	
	constant iADR_USER_IO : integer := 25;
	constant iADR_WR_FIFO_MR : integer := 26;
	-- Address Decodes for this core ------------------------------------------------------------	

	-- Interrupt State Machine ------------------------------------------------------------------
	constant Irq_Idle : integer := 0;
	constant Irq_Dma_Delay : integer := 1;
	constant Irq_Dma_Req : integer := 2;
	constant Irq_Dma_Done : integer := 3;
	constant Irq_Int : integer := 4;
	-- Interrupt State Machine ------------------------------------------------------------------

	-- Flash State machine state Decodes --------------------------------------------------------
	type Flash_States is (
		ST_IDLE,
		ST_WRITE_FLASH_DATA_MSB0,
		ST_WRITE_FLASH_DATA_MSB1,
		ST_WRITE_FLASH_CLK_MSB0,
		ST_WRITE_FLASH_CLK_MSB1,
		ST_READ_FLASH_DATA_MSB0,
		ST_READ_FLASH_DATA_MSB1,
		ST_READ_FLASH_CLK_MSB0,
		ST_READ_FLASH_CLK_MSB1 );
	-- Flash State machine state Decodes --------------------------------------------------------
	
	-- Fifo Data Processing State machine state Decodes -----------------------------------------
	type Altera_States is (
		st_altera_idle,
		st_altera_write_start,
		st_altera_write_data,
		st_altera_write_settle0,
		st_altera_write_settle1 );
	-- Fifo Data Processing State machine state Decodes -----------------------------------------

	-- Constants for Altera ----------------------------------------------------------------------
	
	-- Signals for Altera ------------------------------------------------------------------------
	signal altera_state : Altera_States :=  st_altera_idle;
	
	signal s_dma_irq_clear : std_logic; -- set to '1' to clear s_dma_irq 

	signal Flash_Read_Reg : std_logic_Vector( 7 downto 0 );
	signal Flash_Write_Reg : std_logic_Vector( 7 downto 0 );
	signal Flash_Cnt : integer range 0 to 7;
	signal Flash_State : Flash_States := St_Idle;

	signal s_altera_int_en : std_logic; --! Active high enable for honoring interrupt from Altera chip.

	signal s_i_altera_int_r1 : std_logic := '0'; --! Used to bring Altera interrupt signal into emif clock domain.
	signal s_i_altera_int_r2 : std_logic := '0'; --! Used to bring Altera interrupt signal into emif clock domain.

	signal Read_Fifo_Data_in : std_logic_vector( 31 downto 0 ); --! Register for DSP to read data from Altera chip (via EMIF).
	signal Read_Fifo_Data_Ready : std_logic; --! Register for letting DSP know that Altear FIFO data propagated to EMIF can be read... I think.
	signal s_altera_fifo_rst : std_logic := '0'; --! Asynchronous reset for FIFOs that handle Altera data exchanges.
	signal s_altera_rdfifo_full : std_logic;
	signal s_altera_rdfifo_din : std_logic_vector( 31 downto 0 );
	signal s_altera_rdfifo_wr_en : std_logic;
	signal s_altera_rdfifo_rd_en : std_logic;
	signal s_altera_rdfifo_dout : std_logic_Vector( 31 downto 0 );
	signal s_altera_rdfifo_valid : std_logic := '0'; --! High when first word fall through data for Altera_ReadFifo is valid.
	signal s_altera_rdfifo_empty : std_logic := '0';
	signal s_altera_rdfifo_rd_cnt : std_logic_vector(12 downto 0) := (others => '0');

	signal Write_Fifo_Data_in : std_logic_vector( 32 downto 0 ) := (others=>'0');
	signal Write_Fifo_data_out : std_logic_vector( 32 downto 0 );
	signal Write_Fifo_ef : std_logic;
	signal Write_Fifo_ff : std_logic;
	signal Write_Fifo_Write_En : std_logic;
	signal Write_fifo_wr_Cnt : std_logic_vector( 9 downto 0 );

	signal version_reg : std_logic_vector(31 downto 0);
	signal version_reg1 : std_logic_vector(31 downto 0);
	signal ver_rd : std_logic := '0';

	signal s_altera_data_oen : std_logic := '0';
	signal s_altera_write_busy : std_logic := '0';
	signal s_altera_int_busy : std_logic := '0';

	signal s_err_clr : std_logic := '0'; --! Register used to clear error sticky bits.
	signal s_err_cnt_mismatch : std_logic := '0';
	
	signal s_err_invalid_rd : std_logic := '0'; --! Sticky bit in case a read is attempted from Altera_ReadFifo when there is no valid data available.

	type t_dma_state is 
		(
			DMA_DISABLE_STATE,
			DMA_IDLE_STATE,
			DMA_ACTIVE_STATE,
			DMA_IRQ_STATE
		);
	attribute STATE_ENCODING : string;
	attribute STATE_ENCODING of t_dma_state : type is "0001 0010 0100 1000"; --! One hot encoding.
	signal s_dma_state : t_dma_state := DMA_DISABLE_STATE; --! State machine for DMA transfers (Data is read from Altera_ReadFifo).

	signal s_dma_en : std_logic; --! Active high enable for DMA state machine.

	signal s_dma_timeout_cntr : std_logic_vector(31 downto 0) := (others => '0'); --! Counter for timing out to a Altera_ReadFifo data flush.
	signal s_dma_timeout_threshold : std_logic_vector(31 downto 0) := CONV_STD_LOGIC_VECTOR(5000, 32); --! Register to tell us what the DMA timeout threshold is in clock cycles.

	signal s_dma_num_samples : std_logic_vector(9 downto 0) := (others => '0'); --! Tells DSP number of 32-bit samples transfered in last DMA. Register value for iADR_DMA_INT_COUNT. Effectively s_dma_count + 1.

	signal s_dma_irq : std_logic := '0';
	signal s_dma_count : std_logic_vector(8 downto 0) := (others=>'0'); --! Number of 32-bit words - 1 for requested DMA transfer. NOTE THE MINUS ONE.
	signal s_dma_req : std_logic := '0';
	signal s_dma_start_addr : std_logic_vector(31 downto 0) := (others => '0'); --! Offset into DMA memory space accessed by DSP.
	signal s_dma_base_addr : std_logic_vector(31 downto 0) := (others=>'0'); --! Base address for our DMA transactions.
	signal s_dma_space_addr : std_logic_vector(31 downto 0) := (others=>'0'); --! Internal DMA space address that loops to keep us in the DMA memory we can touch.
	signal s_dma_space_mask : std_logic_vector( 31 downto 0 ); --! BYTE mask to set the size of the DMA memory space and ensure it is a power of 2.

begin

	----------------------------------------------------------------------------------------------
	-- Just maps constants to a core version number
	version : core_version
		port map
		(
			clk           => emif_clk,             -- system clock
			rd            => ver_rd,               -- read enable
			ID            => CORE_APPLICATION_ID,  -- assigned ID number, 0xFF if unassigned
			version_major => FPGA_VERSION_MAJOR,   -- major version number 1-15
			version_minor => FPGA_VERSION_MINOR,   -- minor version number 0-15
			year          => FPGA_YEAR,            -- year since 2000
			month         => FPGA_MONTH,           -- month (1-12)
			day           => FPGA_DAY,             -- day (1-31)
			ilevel        => i_ilevel,
			ilevel_vld    => i_ilevel_vld,
			ivector       => i_ivector,
			ivector_vld   => i_ivector_vld,
			o_data        => version_reg 
		);
			
	-- Version Registter used by EDAX (informative purposes only)
	version_reg1 <= ext(   FPGA_YEAR
			 & FPGA_MONTH
			 & FPGA_DAY
			 & CORE_APP_MAJOR 
			 & CORE_APP_MINOR
			 & FPGA_VERSION_MAJOR
			 & FPGA_VERSION_MINOR, 32 );


	---------------------------------------------------------------------------------------------------
	--***** FLASH PROCESS *****************************************************************************
	flash_proc : process(emif_clk)
	begin
		if( emif_clk='1' and emif_clk'event ) then
			-- Altera Programming - Flash Enable --------------------------------------------------
			if(( i_cs = '1' ) and ( i_wr_en = '1' ) and ( conv_integer( i_ABus ) = iADR_WR_FLASH_ENABLE )) then 
				Flash_Enable <= i_DBus(0);
			end if;
			-- Altera Programming - Flash Enable --------------------------------------------------
				
			-- Altera Programming - NCS  ----------------------------------------------------------
			if( Flash_Enable = '0' ) then 
				Altera_NCS <= '1';
			elsif(( i_cs = '1' ) and ( i_wr_en = '1' ) and ( conv_integer( i_ABus ) = iADR_WR_NCS )) then 
				Altera_NCS <= i_DBus(0);
			end if;
			-- Altera Programming - NCS  ----------------------------------------------------------

			-- Altera Programming - NCE  ----------------------------------------------------------
			if( Flash_Enable = '0' ) then 
				Altera_NCE <= '1';
			elsif(( i_cs = '1' ) and ( i_wr_en = '1' ) and ( conv_integer( i_ABus ) = iADR_WR_NCE )) then 
				Altera_NCE <= i_DBus(0);
			end if;
			-- Altera Programming - NCE  ----------------------------------------------------------

			-- Altera Programming - NCON ----------------------------------------------------------
			if( Flash_Enable = '0' ) then 
				Altera_NCON <= '1';
			elsif(( i_cs = '1' ) and ( i_wr_en = '1' ) and ( conv_integer( i_ABus ) = iADR_WR_NCON )) then 
				Altera_NCON <= i_DBus(0);
			end if;
			-- Altera Programming - NCON ----------------------------------------------------------

			-- Flash Programming State Machine ----------------------------------------------------------
			case Flash_State is
				-- Idle State -------------------------------------------------------------------------
				when ST_IDLE =>
					ALTERA_ASD <= '1';
					ALTERA_DCLK <= '1';
					Flash_Cnt <= 0;

					if(( i_cs = '1' ) and ( i_wr_en = '1' ) and ( flash_enable = '1' )) then
						case conv_integer( i_ABus ) is
							-- Write MSB to LSB
							when iADR_WR_BYTE_MSB => 
								for i in 0 to 7 loop
									Flash_Write_Reg(i) <= i_DBus(i);
								end loop;
								Flash_State <= ST_WRITE_FLASH_DATA_MSB0; -- Write Flash Data Byte MSB to LSB

							-- Write LSB to MSB
							when iADR_WR_BYTE_LSB => 
								-- Reverse Order
								for i in 0 to 7 loop
									Flash_Write_Reg(7-i) <= i_DBus(i);
								end loop;
								Flash_State <= ST_WRITE_FLASH_DATA_MSB0; -- Write Flash Data Long LSB to MSB

							-- Read MSB to LSB
							when iADR_WR_BYTE_READ => 
								Flash_State <= ST_READ_FLASH_DATA_MSB0; -- Start the sequencer to read a byte of data from the flash

							when others => 
								Flash_State <= St_Idle;
						end case;
					end if;
					-- Idle State -------------------------------------------------------------------------		

				-- Write Data MSB to LSB but only 8 bits ----------------------------------------------
				-- Data is clocked on the rising edge of ALTERA_DCLK
				-- Max Clock Rate for Write is 25Mhz
				when ST_WRITE_FLASH_DATA_MSB0 =>
					ALTERA_ASD <= Flash_Write_Reg( 7-Flash_Cnt );
					ALTERA_DCLK <= '1';
					Flash_State <= ST_WRITE_FLASH_CLK_MSB1; -- ST_WRITE_FLASH_DATA_MSB1;

				when ST_WRITE_FLASH_CLK_MSB1 => 
					Flash_Cnt <= Flash_Cnt + 1;
					ALTERA_DCLK <= '0';
					if( Flash_Cnt = 7 ) then 
						Flash_State <= ST_IDLE;
					else 
						Flash_State <= ST_WRITE_FLASH_DATA_MSB0;
					end if;
				-- Write Data MSB to LSB but only 8 bits ----------------------------------------------

				-- Read Data MSB to MSB but only 8 bits ----------------------------------------------
				-- data is clocked out on the falling edge of the clock
				-- but has a max clock freq of 20Mhz, so this must be 2x as slow
				when ST_READ_FLASH_DATA_MSB0 =>
					ALTERA_DCLK <= '0'; -- Set the Clock Low
					Flash_State <= ST_READ_FLASH_DATA_MSB1;

				when ST_READ_FLASH_DATA_MSB1 =>
					ALTERA_DCLK <= '0'; -- Set the Clock Low
					Flash_Read_Reg( 7-Flash_Cnt ) <= altera_data0;
					Flash_State <= ST_READ_FLASH_CLK_MSB0;

				when ST_READ_FLASH_CLK_MSB0 =>
					ALTERA_DCLK <= '1'; -- Set the Clock High
					Flash_State <= ST_READ_FLASH_CLK_MSB1;

				when ST_READ_FLASH_CLK_MSB1 =>
					ALTERA_DCLK <= '1';
					Flash_Cnt <= Flash_Cnt + 1;
					if( Flash_Cnt = 7 ) then 
						Flash_State <= ST_IDLE;
					else 
						Flash_State <= ST_READ_FLASH_DATA_MSB0;
					end if;
				-- Read Data MSB to MSB but only 8 bits ----------------------------------------------

				when others => 
					ALTERA_ASD <= '1';
					ALTERA_DCLK <= '1';
					Flash_State <= ST_IDLE;
			end case;
			-- Flash Programming State Machine ----------------------------------------------------------
		end if; -- if rising edge clock
	end process; -- Flash Proc
	--***** FLASH PROCESS *****************************************************************************
	---------------------------------------------------------------------------------------------------


	---------------------------------------------------------------------------------------------------
	-- FIFO To Write Data to Altera
	Altera_WriteFifo_Inst : Altera_WriteFifo
		port map
		(
			rst => s_altera_fifo_rst,
			wr_clk => emif_clk,
			wr_en => Write_Fifo_Write_En, -- Write Enable into this FIFO
			din => Write_Fifo_Data_in, -- Write Data into this FIFO
			
			rd_clk => altera_clk, 
			rd_en => s_altera_write_busy, -- Read Enable Out of this FIFO
			dout => Write_Fifo_data_out, -- Write Data to Altera
--			wr_data_count => Write_fifo_wr_cnt,
			empty => Write_Fifo_Ef, -- Fifo Empty Flag
			full => Write_Fifo_FF -- FIFO Full Flag
		);
	---------------------------------------------------------------------------------------------------


	---------------------------------------------------------------------------------------------------
	Altera_Write_to_Read_Fifo_Proc : process( altera_clk )
	begin
		if( rising_Edge( altera_clk )) then
			s_altera_rdfifo_din(0) <= i_altera_data_in(0);
			for i in 1 to 31 loop
				s_altera_rdfifo_din(i) <= i_altera_data_in(i) XOR i_altera_data_in(0);
			end loop;

			if(( s_altera_rdfifo_full = '0' ) and ( i_altera_read_busy = '1' ) and ( i_altera_data_in(32) = '1' )) then 
				s_altera_rdfifo_wr_en <= '1';
			else 
				s_altera_rdfifo_wr_en <= '0';
			end if;

			if(( i_altera_read_busy = '1' ) and ( i_altera_data_in(32) = '0' )) then
				Read_Fifo_Data_in(0) <= i_altera_data_in(0);
				for i in 1 to 31 loop
					Read_Fifo_Data_in(i)<= i_altera_data_in(i) XOR i_altera_data_in(0);
				end loop;
			end if;
		end if;
	end process;
	---------------------------------------------------------------------------------------------------

	Altera_ReadFifo_Inst : Altera_ReadFifo
		port map	
		(
			rst => s_altera_fifo_rst, 
			wr_clk => altera_clk,
			rd_clk => emif_clk,
			din => s_altera_rdfifo_din,
			wr_en => s_altera_rdfifo_wr_en,
			rd_en => s_altera_rdfifo_rd_en,
			dout => s_altera_rdfifo_dout,
			full => s_altera_rdfifo_full,
			empty => s_altera_rdfifo_empty,
			valid  => s_altera_rdfifo_valid,
			rd_data_count => s_altera_rdfifo_rd_cnt
		);

	o_altera_rdfifo_rd_cnt <= s_altera_rdfifo_rd_cnt;

	-- DMA cannot be told to wait, so we'll just have to flag an error if we're trying to read an empty FIFO
	s_altera_rdfifo_rd_en <= '1' when i_dma_rd_stb = '1' and i_dma_done = '0' else '0';
	o_altera_rdfifo_rd_en <= s_altera_rdfifo_rd_en;

	o_altera_rdfifo_full <= s_altera_rdfifo_full; 
	o_altera_rdfifo_empty <= s_altera_rdfifo_empty; 
	
	o_dma_data <= s_altera_rdfifo_dout;

	---------------------------------------------------------------------------------------------------
	altera_data_proc : process( altera_clk )
	begin
		if(( altera_clk'event ) and ( altera_clk = '1' )) then

			-- State Machine to Process Xilinx Read/Write FIFO to Altera Write/Read FIFO ----------
			o_altera_data_out(0) <= Write_Fifo_data_out(0);
			o_altera_data_out(32) <= Write_Fifo_data_out(32);
			for i in 1 to 31 loop
				o_altera_data_out(i) <= Write_Fifo_data_out(i) XOR Write_Fifo_data_out(0);
			end loop;

			case altera_state is
				when st_altera_idle =>
					s_altera_write_busy <= '0';
					s_altera_data_oen <= '0';
					if(( i_altera_WFF_FF = '0' ) and ( Write_Fifo_Ef = '0' ) and ( i_altera_read_busy = '0' ))
						then altera_State <= st_altera_write_start;
					end if;

				when st_altera_write_start =>
					s_altera_write_busy <= '0';
					s_altera_data_oen <= '1';
					altera_state <= st_altera_write_data;

				when st_altera_write_data =>
					s_altera_write_busy <= '1';
					s_altera_data_oen <= '1';
					altera_state <= st_altera_write_settle0;

				when st_altera_write_settle0 =>
					s_altera_write_busy <= '0';
					s_altera_data_oen <= '1';
					altera_state <= st_altera_write_settle1;

				when st_altera_write_settle1 =>
					s_altera_write_busy <= '0';
					s_altera_data_oen <= '1';
					altera_state <= st_altera_idle;

				when others =>
					s_altera_write_busy <= '0';
					altera_state <= st_altera_idle;
			end case;

		end if;
	end process;
	---------------------------------------------------------------------------------------------------

	o_altera_data_oen <= s_altera_data_oen;
	o_altera_write_busy <= s_altera_write_busy;

	---------------------------------------------------------------------------------------------------
	-- State machine to write data back and forth between the Xilinx and Altera fifos
	read_data_proc : process( emif_clk )
	begin
		if(( emif_clk'event ) and ( emif_clk = '1' )) then
			if( s_altera_fifo_rst = '1' ) then 
				Write_fifo_wr_cnt <= (others=>'0');
			elsif(( Write_Fifo_Write_En = '1' ) and ( s_altera_write_busy = '0' ) and ( conv_integer( not( Write_Fifo_Wr_Cnt )) /= 0 )) then 
				Write_fifo_Wr_cnt <= Write_Fifo_Wr_Cnt + 1;
			elsif(( Write_Fifo_Write_En = '0' ) and ( s_altera_write_busy = '1' ) and ( conv_integer( Write_Fifo_Wr_Cnt ) /= 0 )) then 
				Write_fifo_wr_Cnt <= Write_fifo_Wr_cnt - 1;
			end if;

			-- Reset FPGA Fifos -------------------------------------------------------------------
			if(( i_cs = '1' ) and ( i_wr_en = '1' ) and ( conv_integer( i_ABus ) = iADR_WR_FIFO_MR )) then 
				s_altera_fifo_rst <= '1';
			else 
				s_altera_fifo_rst <= '0';
			end if;
			-- Reset FPGA Fifos -------------------------------------------------------------------

			-- DMA Enable Register ----------------------------------------------------------------
			if(( i_cs = '1' ) and ( i_wr_en = '1' ) and ( conv_integer( i_ABus ) = iADR_DMA_EN_REG )) then
				s_dma_en <= i_DBus(0);
				s_altera_int_en <= i_DBus(2);
				s_err_clr <= i_DBus(4);
			end if;
			-- DMA Enable Register ----------------------------------------------------------------

			-- DMA IRQ Clear ----------------------------------------------------------------------
			if(( i_cs = '1' ) and ( i_wr_en = '1' ) and ( conv_integer( i_ABus ) = iADR_DMA_IRQ_CLR ) and ( i_DBus(0) = '1' )) then 
				s_dma_irq_clear <= '1';
			else 
				s_dma_irq_clear <= '0';
			end if;
			-- DMA IRQ Clear ( no-data required ) ----------------------------------------------

			-- User I/O ---------------------------------------------------------------------
			if(( i_cs = '1' ) and ( i_wr_en = '1' ) and ( conv_integer( i_ABus ) = iADR_USER_IO )) then 
				o_user_io <= i_DBus;
			end if;
			-- User I/O ---------------------------------------------------------------------

			-- DMA Start Address ---------------------------------------------------------------
			if(( i_cs = '1' ) and ( i_wr_en = '1' ) and ( conv_integer( i_ABus ) = iADR_DMA_START_ADDR )) then 
				s_dma_base_addr <= ext( i_DBus( 27 downto 2 ), 32 ); -- Conv from Bytes to Words
			end if;
			-- DMA Start Address ---------------------------------------------------------------

			-- DMA End Address -----------------------------------------------------------------
			if(( i_cs = '1' ) and ( i_wr_en = '1' ) and ( conv_integer( i_ABus ) = iADR_DMA_SPACE_MASK )) then 
				s_dma_space_mask <= ext( i_DBus( 31 downto 0 ), 32 ); -- Conv from Bytes to Words
			end if;
			-- DMA End Address -----------------------------------------------------------------

			if(( i_cs = '1' ) and ( i_wr_en = '1' ) and ( conv_integer( i_ABus ) = iADR_DMA_INT_THRESHOLD)) then 
				s_dma_timeout_threshold <= ext( i_DBus( 31 downto 0 ), 32 ); 
			end if;
               
			-- version stuff -------------------------------------------------------------------
			if(( i_cs = '1' ) and ( i_rd_en = '1' ) and ( conv_integer( i_ABus ) = iADR_RD_VER )) then 
				ver_rd <= '1';
			else 
				ver_rd <= '0';
			end if;
			-- version stuff -------------------------------------------------------------------

			-- Read Altera Data Ready Flag -------------------------------------
			-- When Altera Writes data - flag gets asserted
			if(( i_altera_read_busy = '1' ) and ( i_altera_data_in(32) = '0' )) then 
				Read_Fifo_Data_Ready <= '1';
			-- flag gets cleared when the data is read
			elsif(( i_cs = '1' ) and ( i_rd_en = '1' ) and ( conv_integer( i_ABus ) = iADR_RD_FPGA_DATA )) then 
				Read_Fifo_Data_Ready <= '0';
			end if;
			-- Read Altera Data Ready Flag -------------------------------------

			-- Read Data Mux ----------------------------------------------------------------------
			if( i_cs = '0' ) then 
				o_DBus <= (others=>'0');
			else
				case conv_integer( i_ABus ) is
					when iADR_RD_VER => 
						o_DBus <= version_reg;

					when iADR_RD_VER1 =>
						o_DBus <= version_reg1;

					when iADR_RD_FLASH_STATUS =>
						if( Flash_State = St_Idle ) then 
							o_DBus(0) <= '0';
						else 
							o_DBus(0) <= '1';
						end if;
						o_DBus(1) <= Altera_Ns;
						o_DBus(2) <= altera_data0;
						o_DBus(3) <= Altera_cdone;
						o_DBus( 31 downto 4 ) <= (others=>'0');

					when iADR_RD_FLASH_DATA		=> 
						o_DBus( 7 downto 0 ) <= Flash_Read_Reg;
						o_DBus( 31 downto 8 ) <= (others=>'0');

					when iADR_RD_FPGA_STATUS => 
						o_DBus( 12 downto  0 ) <= s_altera_rdfifo_rd_cnt;
						o_DBus( 16 ) <= s_altera_rdfifo_empty;
						o_DBus( 17 ) <= s_altera_rdfifo_full;
						o_DBus( 18 ) <= Write_Fifo_Ef;
						o_DBus( 19 ) <= Write_Fifo_FF;
						o_DBus( 28 downto 20 ) <= Write_Fifo_Wr_Cnt( 8 downto 0 );
						o_DBus( 29 ) <= Read_Fifo_Data_Ready; 
						o_DBus( 31 ) <= s_dma_irq;

					-- Read the READ FIFO independent of the DMA Engine
					when iADR_RD_FPGA_DATA =>
						o_DBus <= Read_Fifo_Data_in; 

					when iADR_DMA_EN_REG =>
						o_DBus(0) <= s_dma_en;
						o_DBus(2) <= s_altera_int_en;
						o_DBus(4) <= s_err_clr;
						o_DBus(1) <= s_err_cnt_mismatch;
						o_DBus(30 downto 5) <= (others=>'0');
						o_DBus(31) <= s_err_invalid_rd; -- Read Only

					when iADR_DMA_START_ADDR =>
						o_DBus <= s_dma_base_addr( 29 downto 0 ) & "00";

					when iADR_DMA_SPACE_MASK =>
						o_DBus <= s_dma_space_mask( 31 downto 0 );

					when iADR_DMA_INT_THRESHOLD =>
						o_DBus <= s_dma_timeout_threshold;

					-- used by CaptureThread
					when iADR_DMA_INT_COUNT =>
						o_DBus(11 downto 0) <= s_dma_num_samples & "00"; -- Mult by 4 to conv to bytes

					-- used by CaptureThread
					when iADR_DMA_XFER_START_ADDR =>
						o_DBus <= s_dma_start_addr( 29 downto 0 ) & "00"; -- Mult by 4 to conv to bytes

					when iADR_ETH_DEF =>
						o_DBus( 0 ) <= not( i_eth_def );
						o_DBus( 31 downto 1 ) <= (others=>'0');

					when iADR_USER_IO =>
						o_DBus <= o_User_Io;

					when others => 
						o_DBus <= (others => '0' );
				end case;
			end if;
			-- Read Data Mux ----------------------------------------------------------------------

			-- Write Address to Write FIFO --------------------------------------------------------
			if(( i_cs = '1' ) and ( i_wr_en = '1' )  and ( Write_Fifo_FF = '0' ) and ( conv_integer( i_ABus ) = iADR_WR_FPGA_ADDR )) then
				Write_Fifo_Data_in( 32 ) <= '0';
				Write_Fifo_Data_in( 31 downto 0 ) <= i_DBus;
				Write_Fifo_Write_En <= '1';

			-- Write Data to Write FIFO --------------------------------------------------------
			elsif(( i_cs = '1' ) and ( i_wr_en = '1' )  and ( Write_Fifo_FF = '0' ) and ( conv_integer( i_ABus ) = iADR_WR_FPGA_DATA )) then
				Write_Fifo_Data_in( 32 ) <= '1';
				Write_Fifo_Data_in( 31 downto 0 ) <= i_DBus;
				Write_Fifo_Write_En <= '1';
			else 
				Write_Fifo_Write_En <= '0';
			end if;
			-- Write Data to Write FIFO -------------------------------------------------------
		end if; -- rising edge
	end process;
	---------------------------------------------------------------------------------------------------

	o_dma_rwn <= '0';
	o_dma_priority_req <= '0';  -- Not supported in core I don't think...
	o_dma_be <= "1111";

	proc_dma : process(emif_clk)
		variable v_dma_count : std_logic_vector(9 downto 0) := (others => '0');
	begin
		if(( emif_clk'event ) and ( emif_clk = '1' )) then
			-- Clear error sticky bits
			if (s_err_clr = '1') then
				s_err_invalid_rd <= '0';
			end if;

			-- Check for reads when no valid data available from FIFO
			if (s_altera_rdfifo_rd_en = '1' and s_altera_rdfifo_valid = '0') then
				s_err_invalid_rd <= '1';
			end if;

			--TODO: Additional errors to check for and sticky bit?


			s_dma_req <= '0';
			s_dma_irq <= '0';

			s_err_cnt_mismatch <= '0';

			-- Make sure that altera interrupt is brought into our clock domain
			s_i_altera_int_r1 <= i_altera_int;
			s_i_altera_int_r2 <= s_i_altera_int_r1;

			-- Latch the interrupt from the Altera chip (if interrupts are enabled)
			if (s_altera_int_en = '0') then
				s_altera_int_busy <= '0';
			elsif (s_i_altera_int_r2 = '1') then
				s_altera_int_busy <= '1';
			end if;

			case s_dma_state  is
				when DMA_DISABLE_STATE =>
					-- Clear the DMA count
					s_dma_count <= (others => '0');
					s_dma_num_samples <= (others => '0');

					-- Resert at beginning of DMA address space when disabled
					s_dma_start_addr <= (others => '0');
					s_dma_space_addr <= (others => '0');

					-- Clear the timeout counter	
					s_dma_timeout_cntr <= (others => '0');

					-- Just wait here for the DMA to be enabled
					if (s_dma_en = '1' and flash_enable = '0') then
						s_dma_state <= DMA_IDLE_STATE;
					end if;	

				when DMA_IDLE_STATE =>
					-- Clear the DMA count
					s_dma_count <= (others => '0');
					s_dma_num_samples <= (others => '0');

					-- Increment the timeout counter each cycle
					s_dma_timeout_cntr <= s_dma_timeout_cntr + 1;

					-- If a max sized DMA transfer would cause us to wrap, then start back at the beginning of hte DMA memory space so that
					-- the next DMA is not potentially split over the wrapping boundary
					if ((s_dma_space_addr + CONV_STD_LOGIC_VECTOR(MAX_DMA_TRANSFER_SIZE, 32)) > "00"&s_dma_space_mask(31 downto 2)) then
						s_dma_space_addr <= (others => '0');	
						s_dma_start_addr <= (others => '0');
					end if;

					if (s_dma_en = '0' or Flash_Enable = '1') then 
						-- Transition abck to being disabled if told to
						s_dma_state <= DMA_DISABLE_STATE;
					elsif (CONV_INTEGER(s_altera_rdfifo_rd_cnt) >= MAX_DMA_TRANSFER_SIZE) then
						-- We've crossed the threshold for a full capacity DMA.
						s_dma_state <= DMA_ACTIVE_STATE;
						-- Request a full sized DMA transfer. Keep in mind DMA is looking for number of 32-bit words - 1.
						s_dma_count <= CONV_STD_LOGIC_VECTOR(MAX_DMA_TRANSFER_SIZE - 1, 9);
					elsif (s_altera_int_busy = '1' and s_altera_rdfifo_rd_cnt > 0) then
						-- We've been interrupted by the Altera chip.
						s_dma_state <= DMA_ACTIVE_STATE;
						-- DMA what we have in the fifo. Keep in mind DMA is looking for number of 32-bit words - 1.
						s_dma_count <= s_altera_rdfifo_rd_cnt(8 downto 0) - 1;
					elsif (s_dma_timeout_cntr >= s_dma_timeout_threshold and s_altera_rdfifo_rd_cnt > 0) then
						-- We've timed out and there is data in the FIFO.
						s_dma_state <= DMA_ACTIVE_STATE;
						-- DMA what we have in the fifo. Keep in mind DMA is looking for number of 32-bit words - 1.
						s_dma_count <= s_altera_rdfifo_rd_cnt(8 downto 0) - 1;
					end if;

				when DMA_ACTIVE_STATE =>
					-- Keep the request line pulled high to signal that we are asking for a DMA transfer
					s_dma_req <= '1';

					-- Increment the dma address for each read (wrapping will occur naturally as DMA mem space is power of 2)
					if (s_altera_rdfifo_rd_en = '1') then
						-- Keep in mind mask is a byte mask, but dma_addr is 32-bit word addr
						s_dma_space_addr <= (s_dma_space_addr + 1) and "00"&s_dma_space_mask(31 downto 2);
						s_dma_num_samples <= s_dma_num_samples + 1;
					end if;

					-- Wait for the DMA engine to tell us that it has finished  
					if (i_dma_done = '1') then
						s_dma_state <= DMA_IRQ_STATE;
						-- Deassert s_dma_req next cycle after i_dma_done is high
						s_dma_req <= '0';
					end if;


				when DMA_IRQ_STATE =>	
					-- Trigger an IRQ to the DSP
					s_dma_irq <= '1';

					-- Clear the timeout counter	
					s_dma_timeout_cntr <= (others => '0');

					if (s_dma_irq_clear = '1') then
						-- Return to IDLE once the DSP processes and clears the IRQ
						s_dma_state <= DMA_IDLE_STATE;
						-- Mark that we are no longer busy processing the altera interrupt
						s_altera_int_busy <= '0';
						-- Set the new start address for the next DMA based where we stopped after the last transfer
						s_dma_start_addr <= s_dma_space_addr;
					end if;
			end case;
		end if; 
	end process proc_dma; 

	o_dma_irq <= s_dma_irq; 
	o_dma_count <= s_dma_count;
	o_dma_req <= s_dma_req;
	o_dma_addr <= s_dma_base_addr + s_dma_space_addr;

	o_altera_int_busy <= s_altera_int_busy;

--------------------------------------------------------------------------------------------------------
end rtl; -- altera_core.vhd
--------------------------------------------------------------------------------------------------------
