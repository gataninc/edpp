--- Title: dma_writer.vhd
--- Description: 
--- Utility class for pouring data from a FIFO with an available read count 
--- to SDRAM via the EMIF_Iface DMA engine.  This engine may be used by a 
--- core such as the camera link or an ADC core for utility.
---
---     o  0
---     | /       Copyright (c) 2008
---    (CL)---o   Critical Link, LLC
---      \
---       O
---
--- Company: Critical Link, LLC.
--- Date: 07/01/2008
--- Version: 1.00
--- Revisions: 1.00 Baseline

library WORK;

library IEEE;
	use IEEE.STD_LOGIC_1164.ALL;
	use IEEE.STD_LOGIC_ARITH.ALL;
	use IEEE.STD_LOGIC_UNSIGNED.ALL;
library UNISIM;
	use UNISIM.VCOMPONENTS.ALL;
---------------------------------------------------------------------------------------------------
entity dma_writer is
	generic (
		FIFO_DEPTH_TWO_TO_N : integer range 9 to 15   := 10; -- 2^10 = 1024 32-bit words
		DMA_TRANSFER_SIZE   : integer range 64 to 512 := 256 ); -- max number of words to transfer at a time
	port (
		emif_clk      		: in 	std_logic;
      
		 -- FIFO read interface (note FIFO read clock must be attached to EMIF clock!)
		 o_fifo_rd     	: out 	std_logic;
		 i_fifo_dout   	: in  	std_logic_vector(31 downto 0);
		 i_fifo_rd_cnt 	: in  	std_logic_vector(FIFO_DEPTH_TWO_TO_N-1 downto 0); -- fifo's read count
		 o_interrupt_count	: out	std_logic_vector( 27 downto 0 );
		 i_altera_interrupt	: in		std_logic;
		 i_altera_dma_req	: in		std_logic;
		 i_altera_dma_en	: in		std_logic;
        
		 -- control interface (from parent core, EMIF clock domain)
		 i_dma_en         	: in 	std_logic;
		 i_dma_start_addr 	: in 	std_logic_vector(27 downto 0); -- word offsets in SDRAM CE space
		 i_dma_end_addr   	: in 	std_logic_vector(27 downto 0); -- word offsets in SDRAM CE space (last word offset - 1)
		 i_dma_int_size   	: in 	std_logic_vector(27 downto 0); -- words per interrupt
		   
		 o_dma_irq        	: out	std_logic;
		 i_clear_irq      	: in 	std_logic; -- set to '1' to clear o_dma_irq 
		 o_curr_dma_ptr   	: out 	std_logic_vector(27 downto 0);
	    
		 -- MityDSP EMIF DMA interface (not available for MityDSP-PRO designs)
		 o_dma_req          : out 	std_logic := '0';
		 o_dma_priority_req : out 	std_logic := '0';
		 o_dma_rwn          : out 	std_logic := '0';
		 o_dma_addr         : out 	std_logic_vector(31 downto 0) := (others=>'0');
		 o_dma_data         : out 	std_logic_vector(31 downto 0) := (others=>'0');
		 o_dma_count        : out 	std_logic_vector(8 downto 0) := (others=>'0');
		 o_dma_be           : out  	std_logic_vector(3 downto 0) := "1111";      
		 i_dma_rd_stb       : in 	std_logic := '0';
		 i_dma_done         : in 	std_logic := '0' );
end dma_writer;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture rtl of dma_writer is

	type WRITER_STATES is (
		 ST_IDLE,            
           ST_DMA_REQ,
		 ST_WAIT_DONE
		 );

	-- DMA controls
	signal interrupt_count 		: std_logic_vector(27 downto 0) := (others=>'0');
	signal current_dma_ptr 		: std_logic_vector(27 downto 0) := (others=>'0');
	signal buffer_delta    		: std_logic_vector(27 downto 0) := (others=>'0');
	signal int_strobe      		: std_logic := '0';
	signal dma_irq         		: std_logic := '0';
	signal dma_len         		: std_logic_vector(8 downto 0) := (others=>'0');
	signal dma_req         		: std_logic := '0';
	signal dma_en_r1       		: std_logic := '0';
	signal i_altera_interrupt_del	: std_logic;
	signal wr_sm 				: WRITER_STATES 	:= ST_IDLE;
     signal Pre_Dma_Len        	: std_logic_vector(27 downto 0); -- fifo's read count
    
begin

	o_fifo_rd      	<= i_dma_rd_stb;
	o_dma_rwn      	<= '0';
	o_dma_addr     	<= x"0" & current_dma_ptr;
	o_curr_dma_ptr 	<= current_dma_ptr;
	o_dma_req      	<= dma_req;
	o_dma_priority_req 	<= '0';  -- todo
	o_dma_irq      	<= dma_irq;
	o_dma_count    	<= dma_len;
	o_dma_be       	<= "1111";
	o_dma_data     	<= i_fifo_dout;
	o_interrupt_count	<= interrupt_count;		-- Send this to Altera Core

	buffer_delta 		<= i_dma_end_addr-current_dma_ptr;
     ----------------------------------------------------------------------------------------------
	counters : process(emif_clk)
	begin
		if rising_edge(emif_clk) then
			dma_en_r1 <= i_dma_en;
			i_altera_interrupt_del	<= i_altera_interrupt;
			-- Current DMA Pointer -------------------------------------------------------------
			if( i_dma_en = '0' )
				then current_dma_ptr 	<= i_dma_start_addr;
			elsif( i_dma_rd_stb = '1' ) then
                    if( current_dma_ptr = i_dma_end_addr )
                         then current_dma_ptr     <= i_dma_start_addr;
                         else current_dma_ptr     <= current_dma_ptr + '1';
                    end if;
			end if;
			-- Current DMA Pointer -------------------------------------------------------------
			
			-- Interrupt Count -----------------------------------------------------------------
			if( i_dma_en = '0' )
				then interrupt_count 	<= CONV_STD_LOGIC_VECTOR(1,28);
			elsif( i_dma_rd_stb = '1' ) then
				if( interrupt_count = i_dma_int_size )
					then interrupt_count 	<= CONV_STD_LOGIC_VECTOR(1,28);
				elsif( i_altera_interrupt = '1' )
					then interrupt_count 	<= CONV_STD_LOGIC_VECTOR(1,28);
					else interrupt_count 	<= interrupt_count+'1';
				end if;
			end if;			
			-- Interrupt Count -----------------------------------------------------------------
			
			-- Interrupt Strobe ----------------------------------------------------------------             
               if( i_dma_en = '0' )
				then int_strobe <= '0';
             
               -- i_altera_interrupt will occur when the DMA is done and the FIFO is empty
			elsif(( i_altera_interrupt = '1' ) and ( i_altera_interrupt_del = '0' ))
				then int_strobe <= '1';

			elsif(( i_dma_rd_stb = '1' ) and ( interrupt_count = i_dma_int_size ))
				then int_strobe <= '1';
				else int_strobe <= '0';
			end if;
			-- Interrupt Strobe ----------------------------------------------------------------
		
--			if i_dma_en='1' and dma_en_r1='0' then
--				interrupt_count 	<= CONV_STD_LOGIC_VECTOR(1,28);
--				current_dma_ptr 	<= i_dma_start_addr;
--				int_strobe 		<= '0';
--			elsif i_dma_en='1' then
--				if i_dma_rd_stb='1' then
--					if( current_dma_ptr /= i_dma_end_addr )
--						then current_dma_ptr <= current_dma_ptr+'1';
--						else current_dma_ptr <= i_dma_start_addr;
--					end if;				
--					if( interrupt_count = i_dma_int_size ) then
--						int_strobe 		<= '1';
--						interrupt_count 	<= CONV_STD_LOGIC_VECTOR(1,28);
--					elsif( i_altera_interrupt = '1' ) then
--						int_strobe 		<= '1';
--						interrupt_count 	<= CONV_STD_LOGIC_VECTOR(1,28);
--					else 
--						int_strobe 		<= '0';
--						interrupt_count 	<= interrupt_count+'1';
--					end if;
--				else
--					int_strobe <= '0';
--				end if;

			-- Generate the DMA Interrupt, 
			-- 	if int_stb = 1, assert the interrupt and keep it asserted until the clear
			if( i_dma_en = '0' )
				then dma_irq <= '0';
			elsif(  int_strobe = '1' )
				then dma_irq <= '1';
			elsif( i_clear_irq = '1' )
				then dma_irq <= '0';
			end if;

               -- If an Altera DMA Request occurs
               if( i_altera_dma_en = '1' )
                    then Pre_Dma_Len    <= ext( i_fifo_rd_cnt, 28 );  -- DMA Length is Fifo Read Count
                    else Pre_Dma_len    <= buffer_delta;              -- DMA Length is buffer delta ( EndAddr-CurrPtr)
               end if;

			-- DMA Write State Machine
			case wr_sm is
				when ST_IDLE =>
                         dma_req   <= '0';
                    
					if(( i_dma_en = '1' ) and ( conv_integer( i_fifo_rd_cnt ) > 4 )) then
						if( i_altera_dma_req = '1' ) 
                                   then wr_sm	<= ST_DMA_REQ;						
						elsif( i_fifo_rd_cnt >= CONV_STD_LOGIC_VECTOR(DMA_TRANSFER_SIZE, FIFO_DEPTH_TWO_TO_N)) 
                                   then wr_sm      <= ST_DMA_REQ;						
						end if;
					end if;
                    when ST_DMA_REQ =>
                         dma_req   <= '1';
                        -- If the PreDmaLen > DMA Transfer Size Clamp to DMA Transfer Size
                        if( conv_integer( Pre_Dma_Len ) > DMA_TRANSFER_SIZE-1 )
                              then DMA_Len <= conv_std_logic_Vector( DMA_TRANSFER_SIZE-1, 9 );
                              else DMA_Len <= Pre_Dma_Len( 8 downto 0 );
                         end if;
                         wr_sm <= ST_WAIT_DONE;
    
				when ST_WAIT_DONE =>
                         -- if DMA_DONE
					if( i_dma_done='1' ) then
						dma_req <= '0';               -- Deassert dma_req
                              
                              -- if the fifo is empty 
                              if( conv_integer( i_fifo_rd_cnt ) = 0 )
                                   then wr_sm     <= ST_IDLE;              -- Done go back to idle
                                   else wr_sm     <= ST_DMA_REQ;           -- else more data - do another dma
                              end if;
					end if;
			end case;

		end if;
	end process counters;       
     ----------------------------------------------------------------------------------------------

--     ----------------------------------------------------------------------------------------------
--	interrupts : process(emif_clk)
--	begin
--		if rising_edge(emif_clk) then
--			if( i_dma_en='0' )
--				then dma_irq <= '0';
--			elsif(  int_strobe='1' )
--				then dma_irq <= '1';
--			elsif( i_altera_interrupt = '1' )
--				then dma_irq	<= '1';
--			elsif( i_clear_irq='1' )
--				then dma_irq <= '0';
--			end if;
--		end if;
--	end process interrupts;
--     ----------------------------------------------------------------------------------------------

--
--     ----------------------------------------------------------------------------------------------
--	write_state_machine : process(emif_clk)
--	begin
--		if rising_edge(emif_clk) then
--			case wr_sm is
--				when IDLE =>
--					-- buffer delta is the difference from the current DMA pointer to the end address
--					if( i_altera_dma_en = '1' )
--						then dma_len <= i_fifo_rd_cnt( 8 downto 0 );
--					elsif( buffer_delta > CONV_STD_LOGIC_VECTOR(DMA_TRANSFER_SIZE-1,27))
--						then dma_len <= CONV_STD_LOGIC_VECTOR(DMA_TRANSFER_SIZE-1,9);
--						else dma_len <= buffer_delta(8 downto 0);
--					end if;
--                    
--					if i_dma_en='1' then
--						if( i_fifo_rd_cnt >= CONV_STD_LOGIC_VECTOR(DMA_TRANSFER_SIZE, FIFO_DEPTH_TWO_TO_N)) then
--							dma_req    <= '1';                        
--							wr_sm      <= WAIT_DONE;
--						
--						elsif( i_altera_dma_req = '1' ) then
--							dma_req	<= '1';
--							wr_sm	<= WAIT_DONE;						
--						end if;
--					else
--						dma_req <= '0';
--					end if;
--    
--				when WAIT_DONE =>
--					if i_dma_done='1' then
--						dma_req <= '0';
--						wr_sm   <= IDLE;
--					else
--						dma_req <= '1';
--					end if;
--			end case;
--		end if;
--	end process write_state_machine;
--     ----------------------------------------------------------------------------------------------
       
end rtl;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
