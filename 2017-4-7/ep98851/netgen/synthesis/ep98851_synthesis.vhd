--------------------------------------------------------------------------------
-- Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.28xd
--  \   \         Application: netgen
--  /   /         Filename: ep98851_synthesis.vhd
-- /___/   /\     Timestamp: Mon Oct 15 11:47:35 2012
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -ar Structure -tm ep98851 -w -dir netgen/synthesis -ofmt vhdl -sim ep98851.ngc ep98851_synthesis.vhd 
-- Device	: xc3s400-4-ft256
-- Input file	: ep98851.ngc
-- Output file	: \\nj-mah-fs04\user\jhendrickson\Project_info\Firmware\eDPP\2012_10_15\ep98851\netgen\synthesis\ep98851_synthesis.vhd
-- # of Entities	: 1
-- Design Name	: ep98851
-- Xilinx	: C:\Xilinx\14.2\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------


-- synthesis translate_off
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use UNISIM.VPKG.ALL;

entity ep98851 is
  port (
    io_are_n : inout STD_LOGIC; 
    io_aoe_n : inout STD_LOGIC; 
    io_awe_n : inout STD_LOGIC; 
    io_eth_mdio : inout STD_LOGIC; 
    io_dsp_rst_n : inout STD_LOGIC; 
    i_emif_clk : in STD_LOGIC := 'X'; 
    o_altera_Clk : out STD_LOGIC; 
    o_busy : out STD_LOGIC; 
    o_fl_bank_sel_n : out STD_LOGIC; 
    o_ce1_en_n : out STD_LOGIC; 
    i_eth_rx_clk : in STD_LOGIC := 'X'; 
    i_clk25mhz : in STD_LOGIC := 'X'; 
    o_hold_n : out STD_LOGIC; 
    i_altera_cdone : in STD_LOGIC := 'X'; 
    i_holda_n : in STD_LOGIC := 'X'; 
    o_altera_nce : out STD_LOGIC; 
    o_rs232_xmit : out STD_LOGIC; 
    o_altera_write_busy_bar : out STD_LOGIC; 
    o_altera_ncs : out STD_LOGIC; 
    i_rs232_rcv : in STD_LOGIC := 'X'; 
    i_bus_req : in STD_LOGIC := 'X'; 
    o_altera_dclk : out STD_LOGIC; 
    i_altera_int_bar : in STD_LOGIC := 'X'; 
    i_altera_write_ff_ff : in STD_LOGIC := 'X'; 
    o_eth_mdc : out STD_LOGIC; 
    o_eth_rst_n : out STD_LOGIC; 
    i_eth_def : in STD_LOGIC := 'X'; 
    i_eth_tx_clk : in STD_LOGIC := 'X'; 
    o_dsp_nmi : out STD_LOGIC; 
    i_altera_ns : in STD_LOGIC := 'X'; 
    i_altera_read_Busy_bar : in STD_LOGIC := 'X'; 
    o_sba_led : out STD_LOGIC; 
    o_eth_ref_clk : out STD_LOGIC; 
    o_altera_read_ff_ff : out STD_LOGIC; 
    o_user_led_n : out STD_LOGIC; 
    i_eth_col : in STD_LOGIC := 'X'; 
    o_sba_clk : out STD_LOGIC; 
    i_altera_data0 : in STD_LOGIC := 'X'; 
    i_eth_crs : in STD_LOGIC := 'X'; 
    o_altera_int_busy : out STD_LOGIC; 
    o_dsp_ext_int4 : out STD_LOGIC; 
    o_dsp_ext_int5 : out STD_LOGIC; 
    i_eth_rx_dv : in STD_LOGIC := 'X'; 
    o_dsp_ext_int6 : out STD_LOGIC; 
    i_eth_rx_er : in STD_LOGIC := 'X'; 
    o_dsp_ext_int7 : out STD_LOGIC; 
    o_eth_tx_en : out STD_LOGIC; 
    o_altera_ncon : out STD_LOGIC; 
    o_altera_asd : out STD_LOGIC; 
    io_ce_n : inout STD_LOGIC_VECTOR ( 3 downto 0 ); 
    io_ed : inout STD_LOGIC_VECTOR ( 31 downto 0 ); 
    io_be_n : inout STD_LOGIC_VECTOR ( 3 downto 0 ); 
    io_altera_dsp : inout STD_LOGIC_VECTOR ( 32 downto 0 ); 
    io_ea : inout STD_LOGIC_VECTOR ( 15 downto 2 ); 
    io_altera_spare : out STD_LOGIC_VECTOR ( 2 downto 0 ); 
    o_eth_tx_data : out STD_LOGIC_VECTOR ( 3 downto 0 ); 
    i_eth_rx_data : in STD_LOGIC_VECTOR ( 3 downto 0 ) 
  );
end ep98851;

architecture Structure of ep98851 is
  component uart
    port (
      i_uart_cs : in STD_LOGIC := 'X'; 
      clk : in STD_LOGIC := 'X'; 
      o_xmit : out STD_LOGIC; 
      i_ri : in STD_LOGIC := 'X'; 
      o_rcv_cts : out STD_LOGIC; 
      i_rd_en : in STD_LOGIC := 'X'; 
      i_dsr : in STD_LOGIC := 'X'; 
      o_irq : out STD_LOGIC; 
      i_wr_en : in STD_LOGIC := 'X'; 
      i_xmit_cts : in STD_LOGIC := 'X'; 
      i_rcv : in STD_LOGIC := 'X'; 
      i_ivector_vld : in STD_LOGIC := 'X'; 
      o_dtr : out STD_LOGIC; 
      i_ilevel_vld : in STD_LOGIC := 'X'; 
      i_dcd : in STD_LOGIC := 'X'; 
      o_DBus : out STD_LOGIC_VECTOR ( 31 downto 0 ); 
      i_ilevel : in STD_LOGIC_VECTOR ( 1 downto 0 ); 
      i_ivector : in STD_LOGIC_VECTOR ( 4 downto 0 ); 
      i_be : in STD_LOGIC_VECTOR ( 3 downto 0 ); 
      i_DBus : in STD_LOGIC_VECTOR ( 31 downto 0 ); 
      i_ABus : in STD_LOGIC_VECTOR ( 4 downto 0 ) 
    );
  end component;
  component cl_eth_671x
    port (
      i_col : in STD_LOGIC := 'X'; 
      rd_en : in STD_LOGIC := 'X'; 
      i_crs : in STD_LOGIC := 'X'; 
      int_rxpkt : out STD_LOGIC; 
      i_rx_dv : in STD_LOGIC := 'X'; 
      i_rx_er : in STD_LOGIC := 'X'; 
      rx_clk : in STD_LOGIC := 'X'; 
      wr_en : in STD_LOGIC := 'X'; 
      eth_cs : in STD_LOGIC := 'X'; 
      o_clk2_5 : out STD_LOGIC; 
      o_mdc : out STD_LOGIC; 
      o_tx_en : out STD_LOGIC; 
      o_tx_er : out STD_LOGIC; 
      clk25 : in STD_LOGIC := 'X'; 
      o_tx_spd_100 : out STD_LOGIC; 
      o_ref_clk : out STD_LOGIC; 
      o_eth_rst : out STD_LOGIC; 
      i_mdio : in STD_LOGIC := 'X'; 
      i_half_duplex : in STD_LOGIC := 'X'; 
      tx_clk : in STD_LOGIC := 'X'; 
      t_mdio : out STD_LOGIC; 
      i_ivector_vld : in STD_LOGIC := 'X'; 
      emif_clk : in STD_LOGIC := 'X'; 
      o_mdio : out STD_LOGIC; 
      i_ilevel_vld : in STD_LOGIC := 'X'; 
      o_rx_en : out STD_LOGIC; 
      oDBus : out STD_LOGIC_VECTOR ( 31 downto 0 ); 
      o_tx_data : out STD_LOGIC_VECTOR ( 3 downto 0 ); 
      i_ilevel : in STD_LOGIC_VECTOR ( 1 downto 0 ); 
      ABus : in STD_LOGIC_VECTOR ( 4 downto 0 ); 
      i_ivector : in STD_LOGIC_VECTOR ( 4 downto 0 ); 
      iDBus : in STD_LOGIC_VECTOR ( 31 downto 0 ); 
      i_rx_data : in STD_LOGIC_VECTOR ( 3 downto 0 ) 
    );
  end component;
  component ROC
    port (
      O : out STD_LOGIC 
    );
  end component;
  component Altera_ReadFifo
    port (
      rd_en : in STD_LOGIC := 'X'; 
      rst : in STD_LOGIC := 'X'; 
      empty : out STD_LOGIC; 
      wr_en : in STD_LOGIC := 'X'; 
      rd_clk : in STD_LOGIC := 'X'; 
      valid : out STD_LOGIC; 
      full : out STD_LOGIC; 
      wr_clk : in STD_LOGIC := 'X'; 
      dout : out STD_LOGIC_VECTOR ( 31 downto 0 ); 
      rd_data_count : out STD_LOGIC_VECTOR ( 12 downto 0 ); 
      din : in STD_LOGIC_VECTOR ( 31 downto 0 ) 
    );
  end component;
  component Altera_WriteFifo
    port (
      rd_en : in STD_LOGIC := 'X'; 
      wr_en : in STD_LOGIC := 'X'; 
      full : out STD_LOGIC; 
      empty : out STD_LOGIC; 
      wr_clk : in STD_LOGIC := 'X'; 
      rst : in STD_LOGIC := 'X'; 
      rd_clk : in STD_LOGIC := 'X'; 
      dout : out STD_LOGIC_VECTOR ( 32 downto 0 ); 
      din : in STD_LOGIC_VECTOR ( 32 downto 0 ); 
      wr_data_count : out STD_LOGIC_VECTOR ( 9 downto 0 ) 
    );
  end component;
  signal Mtrien_io_altera_dsp_33 : STD_LOGIC; 
  signal Mtrien_io_altera_dsp_mux0000_norst : STD_LOGIC; 
  signal N11 : STD_LOGIC; 
  signal N114 : STD_LOGIC; 
  signal N13 : STD_LOGIC; 
  signal N15 : STD_LOGIC; 
  signal N17 : STD_LOGIC; 
  signal N19 : STD_LOGIC; 
  signal N197 : STD_LOGIC; 
  signal N199 : STD_LOGIC; 
  signal N201 : STD_LOGIC; 
  signal N203 : STD_LOGIC; 
  signal N205 : STD_LOGIC; 
  signal N207 : STD_LOGIC; 
  signal N21 : STD_LOGIC; 
  signal N213 : STD_LOGIC; 
  signal N215 : STD_LOGIC; 
  signal N217 : STD_LOGIC; 
  signal N219 : STD_LOGIC; 
  signal N221 : STD_LOGIC; 
  signal N223 : STD_LOGIC; 
  signal N226 : STD_LOGIC; 
  signal N229 : STD_LOGIC; 
  signal N23 : STD_LOGIC; 
  signal N231 : STD_LOGIC; 
  signal N234 : STD_LOGIC; 
  signal N237 : STD_LOGIC; 
  signal N241 : STD_LOGIC; 
  signal N248 : STD_LOGIC; 
  signal N25 : STD_LOGIC; 
  signal N250 : STD_LOGIC; 
  signal N252 : STD_LOGIC; 
  signal N254 : STD_LOGIC; 
  signal N256 : STD_LOGIC; 
  signal N260 : STD_LOGIC; 
  signal N269 : STD_LOGIC; 
  signal N27 : STD_LOGIC; 
  signal N271 : STD_LOGIC; 
  signal N273 : STD_LOGIC; 
  signal N275 : STD_LOGIC; 
  signal N286 : STD_LOGIC; 
  signal N29 : STD_LOGIC; 
  signal N290 : STD_LOGIC; 
  signal N299 : STD_LOGIC; 
  signal N301 : STD_LOGIC; 
  signal N302 : STD_LOGIC; 
  signal N304 : STD_LOGIC; 
  signal N306 : STD_LOGIC; 
  signal N308 : STD_LOGIC; 
  signal N31 : STD_LOGIC; 
  signal N310 : STD_LOGIC; 
  signal N312 : STD_LOGIC; 
  signal N314 : STD_LOGIC; 
  signal N316 : STD_LOGIC; 
  signal N318 : STD_LOGIC; 
  signal N320 : STD_LOGIC; 
  signal N322 : STD_LOGIC; 
  signal N324 : STD_LOGIC; 
  signal N326 : STD_LOGIC; 
  signal N328 : STD_LOGIC; 
  signal N33 : STD_LOGIC; 
  signal N330 : STD_LOGIC; 
  signal N332 : STD_LOGIC; 
  signal N334 : STD_LOGIC; 
  signal N336 : STD_LOGIC; 
  signal N338 : STD_LOGIC; 
  signal N340 : STD_LOGIC; 
  signal N342 : STD_LOGIC; 
  signal N344 : STD_LOGIC; 
  signal N346 : STD_LOGIC; 
  signal N348 : STD_LOGIC; 
  signal N35 : STD_LOGIC; 
  signal N350 : STD_LOGIC; 
  signal N352 : STD_LOGIC; 
  signal N354 : STD_LOGIC; 
  signal N356 : STD_LOGIC; 
  signal N358 : STD_LOGIC; 
  signal N360 : STD_LOGIC; 
  signal N362 : STD_LOGIC; 
  signal N364 : STD_LOGIC; 
  signal N365 : STD_LOGIC; 
  signal N366 : STD_LOGIC; 
  signal N367 : STD_LOGIC; 
  signal N368 : STD_LOGIC; 
  signal N369 : STD_LOGIC; 
  signal N37 : STD_LOGIC; 
  signal N370 : STD_LOGIC; 
  signal N371 : STD_LOGIC; 
  signal N372 : STD_LOGIC; 
  signal N373 : STD_LOGIC; 
  signal N374 : STD_LOGIC; 
  signal N375 : STD_LOGIC; 
  signal N376 : STD_LOGIC; 
  signal N377 : STD_LOGIC; 
  signal N378 : STD_LOGIC; 
  signal N379 : STD_LOGIC; 
  signal N380 : STD_LOGIC; 
  signal N381 : STD_LOGIC; 
  signal N382 : STD_LOGIC; 
  signal N383 : STD_LOGIC; 
  signal N384 : STD_LOGIC; 
  signal N385 : STD_LOGIC; 
  signal N386 : STD_LOGIC; 
  signal N387 : STD_LOGIC; 
  signal N388 : STD_LOGIC; 
  signal N389 : STD_LOGIC; 
  signal N39 : STD_LOGIC; 
  signal N390 : STD_LOGIC; 
  signal N391 : STD_LOGIC; 
  signal N392 : STD_LOGIC; 
  signal N393 : STD_LOGIC; 
  signal N394 : STD_LOGIC; 
  signal N395 : STD_LOGIC; 
  signal N396 : STD_LOGIC; 
  signal N397 : STD_LOGIC; 
  signal N398 : STD_LOGIC; 
  signal N399 : STD_LOGIC; 
  signal N400 : STD_LOGIC; 
  signal N401 : STD_LOGIC; 
  signal N402 : STD_LOGIC; 
  signal N403 : STD_LOGIC; 
  signal N404 : STD_LOGIC; 
  signal N405 : STD_LOGIC; 
  signal N406 : STD_LOGIC; 
  signal N407 : STD_LOGIC; 
  signal N408 : STD_LOGIC; 
  signal N409 : STD_LOGIC; 
  signal N41 : STD_LOGIC; 
  signal N410 : STD_LOGIC; 
  signal N411 : STD_LOGIC; 
  signal N412 : STD_LOGIC; 
  signal N413 : STD_LOGIC; 
  signal N414 : STD_LOGIC; 
  signal N415 : STD_LOGIC; 
  signal N416 : STD_LOGIC; 
  signal N417 : STD_LOGIC; 
  signal N418 : STD_LOGIC; 
  signal N419 : STD_LOGIC; 
  signal N420 : STD_LOGIC; 
  signal N421 : STD_LOGIC; 
  signal N422 : STD_LOGIC; 
  signal N423 : STD_LOGIC; 
  signal N424 : STD_LOGIC; 
  signal N425 : STD_LOGIC; 
  signal N426 : STD_LOGIC; 
  signal N427 : STD_LOGIC; 
  signal N428 : STD_LOGIC; 
  signal N429 : STD_LOGIC; 
  signal N43 : STD_LOGIC; 
  signal N430 : STD_LOGIC; 
  signal N431 : STD_LOGIC; 
  signal N432 : STD_LOGIC; 
  signal N433 : STD_LOGIC; 
  signal N434 : STD_LOGIC; 
  signal N435 : STD_LOGIC; 
  signal N436 : STD_LOGIC; 
  signal N437 : STD_LOGIC; 
  signal N438 : STD_LOGIC; 
  signal N439 : STD_LOGIC; 
  signal N440 : STD_LOGIC; 
  signal N441 : STD_LOGIC; 
  signal N442 : STD_LOGIC; 
  signal N443 : STD_LOGIC; 
  signal N444 : STD_LOGIC; 
  signal N445 : STD_LOGIC; 
  signal N446 : STD_LOGIC; 
  signal N447 : STD_LOGIC; 
  signal N448 : STD_LOGIC; 
  signal N45 : STD_LOGIC; 
  signal N47 : STD_LOGIC; 
  signal N49 : STD_LOGIC; 
  signal N51 : STD_LOGIC; 
  signal N53 : STD_LOGIC; 
  signal N541 : STD_LOGIC; 
  signal N543 : STD_LOGIC; 
  signal N545 : STD_LOGIC; 
  signal N549 : STD_LOGIC; 
  signal N55 : STD_LOGIC; 
  signal N556 : STD_LOGIC; 
  signal N558 : STD_LOGIC; 
  signal N560 : STD_LOGIC; 
  signal N562 : STD_LOGIC; 
  signal N564 : STD_LOGIC; 
  signal N566 : STD_LOGIC; 
  signal N57 : STD_LOGIC; 
  signal N574 : STD_LOGIC; 
  signal N578 : STD_LOGIC; 
  signal N580 : STD_LOGIC; 
  signal N588 : STD_LOGIC; 
  signal N59 : STD_LOGIC; 
  signal N590 : STD_LOGIC; 
  signal N592 : STD_LOGIC; 
  signal N594 : STD_LOGIC; 
  signal N596 : STD_LOGIC; 
  signal N598 : STD_LOGIC; 
  signal N600 : STD_LOGIC; 
  signal N602 : STD_LOGIC; 
  signal N604 : STD_LOGIC; 
  signal N608 : STD_LOGIC; 
  signal N61 : STD_LOGIC; 
  signal N610 : STD_LOGIC; 
  signal N612 : STD_LOGIC; 
  signal N614 : STD_LOGIC; 
  signal N616 : STD_LOGIC; 
  signal N618 : STD_LOGIC; 
  signal N620 : STD_LOGIC; 
  signal N622 : STD_LOGIC; 
  signal N624 : STD_LOGIC; 
  signal N626 : STD_LOGIC; 
  signal N628 : STD_LOGIC; 
  signal N63 : STD_LOGIC; 
  signal N630 : STD_LOGIC; 
  signal N632 : STD_LOGIC; 
  signal N634 : STD_LOGIC; 
  signal N636 : STD_LOGIC; 
  signal N638 : STD_LOGIC; 
  signal N640 : STD_LOGIC; 
  signal N65 : STD_LOGIC; 
  signal N67 : STD_LOGIC; 
  signal N678 : STD_LOGIC; 
  signal N680 : STD_LOGIC; 
  signal N688 : STD_LOGIC; 
  signal N69 : STD_LOGIC; 
  signal N692 : STD_LOGIC; 
  signal N694 : STD_LOGIC; 
  signal N7 : STD_LOGIC; 
  signal N700 : STD_LOGIC; 
  signal N702 : STD_LOGIC; 
  signal N704 : STD_LOGIC; 
  signal N706 : STD_LOGIC; 
  signal N708 : STD_LOGIC; 
  signal N71 : STD_LOGIC; 
  signal N710 : STD_LOGIC; 
  signal N714 : STD_LOGIC; 
  signal N715 : STD_LOGIC; 
  signal N716 : STD_LOGIC; 
  signal N717 : STD_LOGIC; 
  signal N718 : STD_LOGIC; 
  signal N719 : STD_LOGIC; 
  signal N720 : STD_LOGIC; 
  signal N721 : STD_LOGIC; 
  signal N722 : STD_LOGIC; 
  signal N723 : STD_LOGIC; 
  signal N724 : STD_LOGIC; 
  signal N725 : STD_LOGIC; 
  signal N726 : STD_LOGIC; 
  signal N727 : STD_LOGIC; 
  signal N728 : STD_LOGIC; 
  signal N729 : STD_LOGIC; 
  signal N73 : STD_LOGIC; 
  signal N730 : STD_LOGIC; 
  signal N731 : STD_LOGIC; 
  signal N732 : STD_LOGIC; 
  signal N733 : STD_LOGIC; 
  signal N734 : STD_LOGIC; 
  signal N735 : STD_LOGIC; 
  signal N736 : STD_LOGIC; 
  signal N737 : STD_LOGIC; 
  signal N738 : STD_LOGIC; 
  signal N739 : STD_LOGIC; 
  signal N740 : STD_LOGIC; 
  signal N741 : STD_LOGIC; 
  signal N742 : STD_LOGIC; 
  signal N743 : STD_LOGIC; 
  signal N744 : STD_LOGIC; 
  signal N745 : STD_LOGIC; 
  signal N746 : STD_LOGIC; 
  signal N747 : STD_LOGIC; 
  signal N748 : STD_LOGIC; 
  signal N749 : STD_LOGIC; 
  signal N750 : STD_LOGIC; 
  signal N751 : STD_LOGIC; 
  signal N752 : STD_LOGIC; 
  signal N753 : STD_LOGIC; 
  signal N754 : STD_LOGIC; 
  signal N755 : STD_LOGIC; 
  signal N756 : STD_LOGIC; 
  signal N757 : STD_LOGIC; 
  signal N758 : STD_LOGIC; 
  signal N759 : STD_LOGIC; 
  signal N760 : STD_LOGIC; 
  signal N761 : STD_LOGIC; 
  signal N762 : STD_LOGIC; 
  signal N763 : STD_LOGIC; 
  signal N764 : STD_LOGIC; 
  signal N765 : STD_LOGIC; 
  signal N766 : STD_LOGIC; 
  signal N767 : STD_LOGIC; 
  signal N768 : STD_LOGIC; 
  signal N769 : STD_LOGIC; 
  signal N770 : STD_LOGIC; 
  signal N771 : STD_LOGIC; 
  signal N772 : STD_LOGIC; 
  signal N773 : STD_LOGIC; 
  signal N774 : STD_LOGIC; 
  signal N775 : STD_LOGIC; 
  signal N776 : STD_LOGIC; 
  signal N777 : STD_LOGIC; 
  signal N778 : STD_LOGIC; 
  signal N779 : STD_LOGIC; 
  signal N78 : STD_LOGIC; 
  signal N780 : STD_LOGIC; 
  signal N781 : STD_LOGIC; 
  signal N782 : STD_LOGIC; 
  signal N783 : STD_LOGIC; 
  signal N784 : STD_LOGIC; 
  signal N785 : STD_LOGIC; 
  signal N786 : STD_LOGIC; 
  signal N787 : STD_LOGIC; 
  signal N788 : STD_LOGIC; 
  signal N789 : STD_LOGIC; 
  signal N790 : STD_LOGIC; 
  signal N791 : STD_LOGIC; 
  signal N792 : STD_LOGIC; 
  signal N793 : STD_LOGIC; 
  signal N794 : STD_LOGIC; 
  signal N795 : STD_LOGIC; 
  signal N796 : STD_LOGIC; 
  signal N797 : STD_LOGIC; 
  signal N798 : STD_LOGIC; 
  signal N799 : STD_LOGIC; 
  signal N800 : STD_LOGIC; 
  signal N801 : STD_LOGIC; 
  signal N802 : STD_LOGIC; 
  signal N86 : STD_LOGIC; 
  signal N87 : STD_LOGIC; 
  signal altera_flash_enable_inv : STD_LOGIC; 
  signal dcm_lock : STD_LOGIC; 
  signal dcm_reset : STD_LOGIC; 
  signal edo_1_0_Q : STD_LOGIC; 
  signal edo_1_10_Q : STD_LOGIC; 
  signal edo_1_11_Q : STD_LOGIC; 
  signal edo_1_12_Q : STD_LOGIC; 
  signal edo_1_13_Q : STD_LOGIC; 
  signal edo_1_14_Q : STD_LOGIC; 
  signal edo_1_15_Q : STD_LOGIC; 
  signal edo_1_16_Q : STD_LOGIC; 
  signal edo_1_17_Q : STD_LOGIC; 
  signal edo_1_18_Q : STD_LOGIC; 
  signal edo_1_19_Q : STD_LOGIC; 
  signal edo_1_1_Q : STD_LOGIC; 
  signal edo_1_20_Q : STD_LOGIC; 
  signal edo_1_21_Q : STD_LOGIC; 
  signal edo_1_22_Q : STD_LOGIC; 
  signal edo_1_23_Q : STD_LOGIC; 
  signal edo_1_24_Q : STD_LOGIC; 
  signal edo_1_25_Q : STD_LOGIC; 
  signal edo_1_26_Q : STD_LOGIC; 
  signal edo_1_27_Q : STD_LOGIC; 
  signal edo_1_28_Q : STD_LOGIC; 
  signal edo_1_29_Q : STD_LOGIC; 
  signal edo_1_2_Q : STD_LOGIC; 
  signal edo_1_30_Q : STD_LOGIC; 
  signal edo_1_31_Q : STD_LOGIC; 
  signal edo_1_3_Q : STD_LOGIC; 
  signal edo_1_4_Q : STD_LOGIC; 
  signal edo_1_5_Q : STD_LOGIC; 
  signal edo_1_6_Q : STD_LOGIC; 
  signal edo_1_7_Q : STD_LOGIC; 
  signal edo_1_8_Q : STD_LOGIC; 
  signal edo_1_9_Q : STD_LOGIC; 
  signal edo_3_0_Q : STD_LOGIC; 
  signal edo_3_10_Q : STD_LOGIC; 
  signal edo_3_11_Q : STD_LOGIC; 
  signal edo_3_12_Q : STD_LOGIC; 
  signal edo_3_13_Q : STD_LOGIC; 
  signal edo_3_14_Q : STD_LOGIC; 
  signal edo_3_15_Q : STD_LOGIC; 
  signal edo_3_16_Q : STD_LOGIC; 
  signal edo_3_17_Q : STD_LOGIC; 
  signal edo_3_18_Q : STD_LOGIC; 
  signal edo_3_19_Q : STD_LOGIC; 
  signal edo_3_1_Q : STD_LOGIC; 
  signal edo_3_20_Q : STD_LOGIC; 
  signal edo_3_21_Q : STD_LOGIC; 
  signal edo_3_22_Q : STD_LOGIC; 
  signal edo_3_23_Q : STD_LOGIC; 
  signal edo_3_24_Q : STD_LOGIC; 
  signal edo_3_25_Q : STD_LOGIC; 
  signal edo_3_26_Q : STD_LOGIC; 
  signal edo_3_27_Q : STD_LOGIC; 
  signal edo_3_28_Q : STD_LOGIC; 
  signal edo_3_29_Q : STD_LOGIC; 
  signal edo_3_2_Q : STD_LOGIC; 
  signal edo_3_30_Q : STD_LOGIC; 
  signal edo_3_31_Q : STD_LOGIC; 
  signal edo_3_3_Q : STD_LOGIC; 
  signal edo_3_4_Q : STD_LOGIC; 
  signal edo_3_5_Q : STD_LOGIC; 
  signal edo_3_6_Q : STD_LOGIC; 
  signal edo_3_7_Q : STD_LOGIC; 
  signal edo_3_8_Q : STD_LOGIC; 
  signal edo_3_9_Q : STD_LOGIC; 
  signal emif_clk : STD_LOGIC; 
  signal emif_clk_dcm : STD_LOGIC; 
  signal emif_clk_raw : STD_LOGIC; 
  signal i_altera_cdone_IBUF_485 : STD_LOGIC; 
  signal i_altera_data0_IBUF_487 : STD_LOGIC; 
  signal i_altera_int_bar_IBUF_489 : STD_LOGIC; 
  signal i_altera_ns_IBUF_491 : STD_LOGIC; 
  signal i_altera_read_Busy_bar_IBUF_493 : STD_LOGIC; 
  signal i_altera_write_ff_ff_IBUF_495 : STD_LOGIC; 
  signal i_bus_req_IBUF_497 : STD_LOGIC; 
  signal i_clk25mhz_IBUFG_499 : STD_LOGIC; 
  signal i_eth_col_IBUF_502 : STD_LOGIC; 
  signal i_eth_crs_IBUF_504 : STD_LOGIC; 
  signal i_eth_def_IBUF_506 : STD_LOGIC; 
  signal i_eth_rx_clk_IBUFG_508 : STD_LOGIC; 
  signal i_eth_rx_data_0_IBUF_513 : STD_LOGIC; 
  signal i_eth_rx_data_1_IBUF_514 : STD_LOGIC; 
  signal i_eth_rx_data_2_IBUF_515 : STD_LOGIC; 
  signal i_eth_rx_data_3_IBUF_516 : STD_LOGIC; 
  signal i_eth_rx_dv_IBUF_518 : STD_LOGIC; 
  signal i_eth_rx_er_IBUF_520 : STD_LOGIC; 
  signal i_eth_tx_clk_IBUFG_522 : STD_LOGIC; 
  signal i_holda_n_IBUF_524 : STD_LOGIC; 
  signal i_rs232_rcv_IBUF_526 : STD_LOGIC; 
  signal inst_base_module_Mshreg_dcm_reset_shift_15_527 : STD_LOGIC; 
  signal inst_base_module_Mshreg_div10_1_10_528 : STD_LOGIC; 
  signal inst_base_module_Mshreg_div10_2_10_529 : STD_LOGIC; 
  signal inst_base_module_Mshreg_div10_3_10_530 : STD_LOGIC; 
  signal inst_base_module_Mshreg_div10_4_10_531 : STD_LOGIC; 
  signal inst_base_module_Mshreg_div10_5_10_532 : STD_LOGIC; 
  signal inst_base_module_Mshreg_div5_1_5_533 : STD_LOGIC; 
  signal inst_base_module_Mshreg_div5_2_5_534 : STD_LOGIC; 
  signal inst_base_module_Msub_wd_counter_addsub0000_cy_0_rt_536 : STD_LOGIC; 
  signal inst_base_module_N0 : STD_LOGIC; 
  signal inst_base_module_N1 : STD_LOGIC; 
  signal inst_base_module_N2 : STD_LOGIC; 
  signal inst_base_module_N3 : STD_LOGIC; 
  signal inst_base_module_N5 : STD_LOGIC; 
  signal inst_base_module_N8 : STD_LOGIC; 
  signal inst_base_module_and0000 : STD_LOGIC; 
  signal inst_base_module_and0000_inv : STD_LOGIC; 
  signal inst_base_module_bank_addr_0_and0000_607 : STD_LOGIC; 
  signal inst_base_module_bank_addr_0_and0000110_608 : STD_LOGIC; 
  signal inst_base_module_bank_addr_0_and0000121_609 : STD_LOGIC; 
  signal inst_base_module_clken100ms_610 : STD_LOGIC; 
  signal inst_base_module_clken100ms_not0001 : STD_LOGIC; 
  signal inst_base_module_clken4ms_612 : STD_LOGIC; 
  signal inst_base_module_clken4ms_not0001 : STD_LOGIC; 
  signal inst_base_module_dcm_lock_r1_614 : STD_LOGIC; 
  signal inst_base_module_dcm_lock_r2_615 : STD_LOGIC; 
  signal inst_base_module_dcm_reset_shift_or0000 : STD_LOGIC; 
  signal inst_base_module_div10_1_1_Q : STD_LOGIC; 
  signal inst_base_module_div10_1_10_Q : STD_LOGIC; 
  signal inst_base_module_div10_2_and0000 : STD_LOGIC; 
  signal inst_base_module_div10_2_r_622 : STD_LOGIC; 
  signal inst_base_module_div10_3_and0000 : STD_LOGIC; 
  signal inst_base_module_div10_3_r_625 : STD_LOGIC; 
  signal inst_base_module_div10_4_and0000 : STD_LOGIC; 
  signal inst_base_module_div10_4_r_628 : STD_LOGIC; 
  signal inst_base_module_div10_5_and0000 : STD_LOGIC; 
  signal inst_base_module_div10_5_r_631 : STD_LOGIC; 
  signal inst_base_module_div5_1_and0000 : STD_LOGIC; 
  signal inst_base_module_div5_1_r_634 : STD_LOGIC; 
  signal inst_base_module_div5_2_and0000 : STD_LOGIC; 
  signal inst_base_module_div5_2_r_637 : STD_LOGIC; 
  signal inst_base_module_i_ABus_4_0 : STD_LOGIC; 
  signal inst_base_module_i_cs_inv : STD_LOGIC; 
  signal inst_base_module_irq_enable_not0001 : STD_LOGIC; 
  signal inst_base_module_irq_mask_and0000 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_0_1_679 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_0_2_680 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_10_1_682 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_10_2_683 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_11_1_685 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_11_2_686 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_12_1_688 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_12_2_689 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_13_1_691 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_13_2_692 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_14_1_694 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_14_2_695 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_15_1_697 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_15_2_698 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_16_1_700 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_16_2_701 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_17_1_703 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_17_2_704 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_18_1_706 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_18_2_707 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_19_1_709 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_19_2_710 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_1_1_712 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_1_2_713 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_20_1_715 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_20_2_716 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_21_1_718 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_21_2_719 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_22_1_721 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_22_2_722 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_23_1_724 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_23_2_725 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_24_1_727 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_24_2_728 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_25_1_730 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_25_2_731 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_26_1_733 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_26_2_734 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_27_1_736 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_27_2_737 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_28_1_739 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_28_2_740 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_29_1_742 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_29_2_743 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_2_1_745 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_2_2_746 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_30_1_748 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_30_2_749 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_31_1_751 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_31_2_752 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_3_1_754 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_3_2_755 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_4_1_757 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_4_2_758 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_5_1_760 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_5_2_761 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_6_1_763 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_6_2_764 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_7_1_766 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_7_2_767 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_8_1_769 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_8_2_770 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_9_1_772 : STD_LOGIC; 
  signal inst_base_module_irq_mask_mux0000_9_2_773 : STD_LOGIC; 
  signal inst_base_module_key_state_and0001 : STD_LOGIC; 
  signal inst_base_module_key_state_cmp_eq0002 : STD_LOGIC; 
  signal inst_base_module_key_state_cmp_eq0003 : STD_LOGIC; 
  signal inst_base_module_key_state_cmp_eq000321_780 : STD_LOGIC; 
  signal inst_base_module_key_state_cmp_eq0004 : STD_LOGIC; 
  signal inst_base_module_key_state_cmp_eq0004110_782 : STD_LOGIC; 
  signal inst_base_module_key_state_cmp_eq00041211_783 : STD_LOGIC; 
  signal inst_base_module_key_state_cmp_eq000421_784 : STD_LOGIC; 
  signal inst_base_module_led_enable_0_not0001 : STD_LOGIC; 
  signal inst_base_module_o_DBus_cmp_eq0000 : STD_LOGIC; 
  signal inst_base_module_o_DBus_cmp_eq0001 : STD_LOGIC; 
  signal inst_base_module_o_DBus_cmp_eq0003 : STD_LOGIC; 
  signal inst_base_module_o_DBus_cmp_eq0004 : STD_LOGIC; 
  signal inst_base_module_o_DBus_cmp_eq0005 : STD_LOGIC; 
  signal inst_base_module_o_DBus_cmp_eq0006 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_0_25_828 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_0_28_829 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_0_38 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_0_7_831 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_10_1_832 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_11_10_833 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_11_4_834 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_12_1_835 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_13_1_836 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_14_1_837 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_15_1_838 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_16_1_839 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_17_1 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_17_11_841 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_17_12_842 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_18_1 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_18_11_844 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_18_12_845 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_19_1_846 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_1_0_847 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_1_16 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_1_7_849 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_20_Q : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_21_1_851 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_22_1_852 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_23_1_853 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_24_1_854 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_25_Q : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_26_Q : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_27_1_857 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_28_18_858 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_28_19_859 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_28_31_860 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_29_1_861 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_2_14 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_2_4_863 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_30_1_864 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_31_1_865 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_3_0_866 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_3_20 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_3_9_868 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_4_1_869 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_5_1_870 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_6_1_871 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_7_1_872 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_8_10_873 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_8_4_874 : STD_LOGIC; 
  signal inst_base_module_o_DBus_mux0002_9_1_875 : STD_LOGIC; 
  signal inst_base_module_o_irq_output_4_5_and0000 : STD_LOGIC; 
  signal inst_base_module_o_irq_output_4_5_and00001_877 : STD_LOGIC; 
  signal inst_base_module_o_wd_rst_878 : STD_LOGIC; 
  signal inst_base_module_o_wd_rst_and0000 : STD_LOGIC; 
  signal inst_base_module_reset : STD_LOGIC; 
  signal inst_base_module_wd_counter_cmp_eq0000 : STD_LOGIC; 
  signal inst_base_module_wd_counter_cmp_eq000012_914 : STD_LOGIC; 
  signal inst_base_module_wd_counter_cmp_eq000025_915 : STD_LOGIC; 
  signal inst_base_module_wd_counter_cmp_eq000049_916 : STD_LOGIC; 
  signal inst_base_module_wd_counter_cmp_eq000062_917 : STD_LOGIC; 
  signal inst_base_module_wd_counter_not0002 : STD_LOGIC; 
  signal inst_base_module_wd_kick_935 : STD_LOGIC; 
  signal inst_base_module_wd_kick_not0001 : STD_LOGIC; 
  signal inst_base_module_wd_kick_r1_937 : STD_LOGIC; 
  signal inst_base_module_wd_kick_r2_938 : STD_LOGIC; 
  signal inst_base_module_wd_nmi_939 : STD_LOGIC; 
  signal inst_base_module_wd_nmi_clr_940 : STD_LOGIC; 
  signal inst_base_module_wd_nmi_clr_not0002 : STD_LOGIC; 
  signal inst_base_module_wd_nmi_clr_not0003 : STD_LOGIC; 
  signal inst_base_module_wd_nmi_clr_r1_943 : STD_LOGIC; 
  signal inst_base_module_wd_nmi_clr_r2_944 : STD_LOGIC; 
  signal inst_base_module_wd_nmi_en_945 : STD_LOGIC; 
  signal inst_base_module_wd_nmi_en_mux0000 : STD_LOGIC; 
  signal inst_base_module_wd_nmi_en_not0001 : STD_LOGIC; 
  signal inst_base_module_wd_nmi_mux0000 : STD_LOGIC; 
  signal inst_base_module_wd_nmi_not0001 : STD_LOGIC; 
  signal inst_base_module_wd_nmi_not000118_950 : STD_LOGIC; 
  signal inst_base_module_wd_nmi_not000156_951 : STD_LOGIC; 
  signal inst_base_module_wd_rst_952 : STD_LOGIC; 
  signal inst_base_module_wd_rst_clr_953 : STD_LOGIC; 
  signal inst_base_module_wd_rst_clr_not0002 : STD_LOGIC; 
  signal inst_base_module_wd_rst_clr_not0003 : STD_LOGIC; 
  signal inst_base_module_wd_rst_clr_r1_956 : STD_LOGIC; 
  signal inst_base_module_wd_rst_clr_r2_957 : STD_LOGIC; 
  signal inst_base_module_wd_rst_en_958 : STD_LOGIC; 
  signal inst_base_module_wd_rst_en_mux0000 : STD_LOGIC; 
  signal inst_base_module_wd_rst_not0002_inv : STD_LOGIC; 
  signal inst_base_module_wd_rst_not0003 : STD_LOGIC; 
  signal inst_base_module_wd_rst_not000320 : STD_LOGIC; 
  signal inst_base_module_wd_rst_not0003201_963 : STD_LOGIC; 
  signal inst_base_module_wd_rst_not0003202_964 : STD_LOGIC; 
  signal inst_base_module_wd_rst_not000360_965 : STD_LOGIC; 
  signal inst_base_module_wd_rst_r_966 : STD_LOGIC; 
  signal inst_base_module_wd_rst_r1_967 : STD_LOGIC; 
  signal inst_base_module_wd_rst_r2_968 : STD_LOGIC; 
  signal inst_base_module_wd_timeout_not0001 : STD_LOGIC; 
  signal inst_emif_iface_Mshreg_o_ce_n_0_986 : STD_LOGIC; 
  signal inst_emif_iface_N12 : STD_LOGIC; 
  signal inst_emif_iface_are_dly_993 : STD_LOGIC; 
  signal inst_emif_iface_are_n_994 : STD_LOGIC; 
  signal inst_emif_iface_awe_dly_995 : STD_LOGIC; 
  signal inst_emif_iface_awe_n_996 : STD_LOGIC; 
  signal inst_emif_iface_ce3_n_1005 : STD_LOGIC; 
  signal inst_emif_iface_cs_0_Q : STD_LOGIC; 
  signal inst_emif_iface_cs_1_Q : STD_LOGIC; 
  signal inst_emif_iface_cs_3_Q : STD_LOGIC; 
  signal inst_emif_iface_cs_8_Q : STD_LOGIC; 
  signal inst_emif_iface_cs_r_0_Q : STD_LOGIC; 
  signal inst_emif_iface_cs_r_1_Q : STD_LOGIC; 
  signal inst_emif_iface_cs_r_3_Q : STD_LOGIC; 
  signal inst_emif_iface_cs_r_8_Q : STD_LOGIC; 
  signal inst_emif_iface_dma_t_ed_1014 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00001_1121 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00011_1122 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00021_1123 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00031_1124 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00041_1125 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00051_1126 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00061_1127 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00071_1128 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00081_1129 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00091_1130 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00101_1131 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00111_1132 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00121_1133 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00131_1134 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00141_1135 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00151_1136 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00161_1137 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00171_1138 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00181_1139 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00191_1140 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00201_1141 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00211_1142 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00221_1143 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00231_1144 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00241_1145 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00251_1146 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00261_1147 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00271_1148 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00281_1149 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00291_1150 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00301_1151 : STD_LOGIC; 
  signal inst_emif_iface_edo_ce3_or00311_1152 : STD_LOGIC; 
  signal inst_emif_iface_o_awe_n_1153 : STD_LOGIC; 
  signal inst_emif_iface_o_cas_n_1155 : STD_LOGIC; 
  signal inst_emif_iface_o_ce_n_0_Q : STD_LOGIC; 
  signal inst_emif_iface_o_ce_n_3_Q : STD_LOGIC; 
  signal inst_emif_iface_o_ce_n_3_1_1158 : STD_LOGIC; 
  signal inst_emif_iface_o_ce_n_3_2_1159 : STD_LOGIC; 
  signal inst_emif_iface_o_ras_n_1173 : STD_LOGIC; 
  signal inst_emif_iface_rd : STD_LOGIC; 
  signal inst_emif_iface_rd_r_1175 : STD_LOGIC; 
  signal inst_emif_iface_reset : STD_LOGIC; 
  signal inst_emif_iface_reset_inv : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_N0 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_N16 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_N17 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_N24 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_N3 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_N30 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_N33 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_N34 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_N35 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_N36 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_N38 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_N41 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_N42 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_N421 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_N43 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_N431 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_N46 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_N47 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_N49 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_N5 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_N51 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_N52 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_N521 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_N54 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_N60 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_N9 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_awe_n_1214 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_awe_n_mux0000 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_19_17_1218 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_19_26_1219 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_19_38 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_20_21_1221 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_20_31_1222 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_20_55 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_cas_n_1225 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_cas_n_mux0000 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_col_and0000 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_0_17_1237 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_0_4_1238 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_1_0_1239 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_1_33 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_2_28_1241 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_2_39_1242 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_2_4_1243 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_0_1244 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_17_1245 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_37 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_53_1247 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_6_1248 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_73 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_4_0_1250 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_4_30 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_5_0_1252 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_5_3_1253 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_5_31 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_6_0_1255 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_6_43 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_7_0_1257 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_7_14_1258 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_7_61 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_dma_done_1301 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_1302 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_mux0002_1303 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_dma_we_1306 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_ea_10_mux0001 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_ea_11_mux0001 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_ea_12_mux0001 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_ea_13_mux0001 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_ea_14_mux0001 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_ea_2_mux0001 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_ea_3_mux0001 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_ea_4_mux0001 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_ea_5_mux0001 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_ea_6_mux0001 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_ea_7_mux0001 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_ea_8_mux0001 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_ea_9_mux0001 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_ed_t_1337 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_ed_t_mux00021 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_emif_t_1339 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_hold_n_1340 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_hold_n_mux000311 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_ras_n_1350 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_ras_n_mux0000 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_10_25 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_10_6_1364 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_11_0_1365 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_11_25_1366 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_11_36_1367 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_11_53 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_12_13_1369 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_12_29_1370 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_12_52 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_12_6_1372 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_13_1 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_13_16_1374 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_13_28 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_13_8_1376 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_14_20_1377 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_14_42 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_14_9 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_14_91_1380 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_15_18_1381 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_15_3_1382 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_15_35 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_15_8_1384 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_16_0_1385 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_16_21_1386 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_16_47 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_17_17_1388 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_17_19_1389 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_17_39_1390 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_17_63_1391 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_18_0_1392 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_18_16_1393 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_18_32 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_9_14_1396 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_9_36 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_sm_arb_FSM_FFd2_1419 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_sm_arb_FSM_FFd3_1420 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_sm_arb_FSM_FFd3_In_1421 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_In_1423 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_In : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_In1_1426 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_In2_1427 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_In21_1429 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_In64 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_In7_1431 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_Out121 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_cmp_eq0004 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt_mux0000_0_1_1437 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt_mux0000_0_2_1438 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_0_12_1449 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_0_20_1450 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_0_50_1451 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_0_51_1452 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_0_58 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_1_3_1454 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_1_44_1455 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_1_47 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_2_SW1 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_6_14 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_6_27 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_6_30_1465 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_6_44_1466 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_7_31_1468 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_8_1_1470 : STD_LOGIC; 
  signal inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_8_2_1471 : STD_LOGIC; 
  signal inst_emif_iface_t_emif_1472 : STD_LOGIC; 
  signal inst_emif_iface_wr : STD_LOGIC; 
  signal inst_emif_iface_wr_r_1474 : STD_LOGIC; 
  signal io_ed_o_ed_not0000_inv : STD_LOGIC; 
  signal io_eth_mdio_o_eth_mdio_not0000_inv : STD_LOGIC; 
  signal irq_map_5_0_Q : STD_LOGIC; 
  signal irq_map_7_0_Q : STD_LOGIC; 
  signal mb_altera_core_Flash_Cnt_mux0000_0_1_1578 : STD_LOGIC; 
  signal mb_altera_core_Flash_Cnt_mux0000_0_2_1579 : STD_LOGIC; 
  signal mb_altera_core_Flash_Cnt_or0000 : STD_LOGIC; 
  signal mb_altera_core_Flash_Read_Reg_0_not0001 : STD_LOGIC; 
  signal mb_altera_core_Flash_Read_Reg_1_not0001 : STD_LOGIC; 
  signal mb_altera_core_Flash_Read_Reg_2_not0001 : STD_LOGIC; 
  signal mb_altera_core_Flash_Read_Reg_3_not0001 : STD_LOGIC; 
  signal mb_altera_core_Flash_Read_Reg_4_not0001 : STD_LOGIC; 
  signal mb_altera_core_Flash_Read_Reg_5_not0001 : STD_LOGIC; 
  signal mb_altera_core_Flash_Read_Reg_6_not0001 : STD_LOGIC; 
  signal mb_altera_core_Flash_Read_Reg_7_not0001 : STD_LOGIC; 
  signal mb_altera_core_Flash_State_FSM_FFd1_1599 : STD_LOGIC; 
  signal mb_altera_core_Flash_State_FSM_FFd2_1600 : STD_LOGIC; 
  signal mb_altera_core_Flash_State_FSM_FFd3_1601 : STD_LOGIC; 
  signal mb_altera_core_Flash_State_FSM_FFd4_1602 : STD_LOGIC; 
  signal mb_altera_core_Flash_State_FSM_FFd5_1603 : STD_LOGIC; 
  signal mb_altera_core_Flash_State_FSM_FFd5_In_1604 : STD_LOGIC; 
  signal mb_altera_core_Flash_State_FSM_FFd6_1605 : STD_LOGIC; 
  signal mb_altera_core_Flash_State_FSM_FFd6_In : STD_LOGIC; 
  signal mb_altera_core_Flash_State_FSM_FFd7_1607 : STD_LOGIC; 
  signal mb_altera_core_Flash_State_FSM_FFd7_In0_1608 : STD_LOGIC; 
  signal mb_altera_core_Flash_State_FSM_FFd7_In20_1609 : STD_LOGIC; 
  signal mb_altera_core_Flash_State_FSM_FFd7_In44 : STD_LOGIC; 
  signal mb_altera_core_Flash_State_FSM_FFd7_In9_1611 : STD_LOGIC; 
  signal mb_altera_core_Flash_State_FSM_N3 : STD_LOGIC; 
  signal mb_altera_core_Flash_State_FSM_Out91 : STD_LOGIC; 
  signal mb_altera_core_Flash_State_cmp_eq0003 : STD_LOGIC; 
  signal mb_altera_core_Flash_Write_Reg_0_and0000 : STD_LOGIC; 
  signal mb_altera_core_Flash_Write_Reg_0_mux0000 : STD_LOGIC; 
  signal mb_altera_core_Flash_Write_Reg_0_mux00001_1618 : STD_LOGIC; 
  signal mb_altera_core_Flash_Write_Reg_1_mux0000 : STD_LOGIC; 
  signal mb_altera_core_Flash_Write_Reg_1_mux00001_1621 : STD_LOGIC; 
  signal mb_altera_core_Flash_Write_Reg_1_mux00002_1622 : STD_LOGIC; 
  signal mb_altera_core_Flash_Write_Reg_2_mux0000 : STD_LOGIC; 
  signal mb_altera_core_Flash_Write_Reg_2_mux00001_1625 : STD_LOGIC; 
  signal mb_altera_core_Flash_Write_Reg_2_mux00002_1626 : STD_LOGIC; 
  signal mb_altera_core_Flash_Write_Reg_3_mux0000 : STD_LOGIC; 
  signal mb_altera_core_Flash_Write_Reg_3_mux00001_1629 : STD_LOGIC; 
  signal mb_altera_core_Flash_Write_Reg_3_mux00002_1630 : STD_LOGIC; 
  signal mb_altera_core_Flash_Write_Reg_4_mux0000 : STD_LOGIC; 
  signal mb_altera_core_Flash_Write_Reg_4_mux00001_1633 : STD_LOGIC; 
  signal mb_altera_core_Flash_Write_Reg_4_mux00002_1634 : STD_LOGIC; 
  signal mb_altera_core_Flash_Write_Reg_5_mux0000 : STD_LOGIC; 
  signal mb_altera_core_Flash_Write_Reg_5_mux00001_1637 : STD_LOGIC; 
  signal mb_altera_core_Flash_Write_Reg_5_mux00002_1638 : STD_LOGIC; 
  signal mb_altera_core_Flash_Write_Reg_6_mux0000 : STD_LOGIC; 
  signal mb_altera_core_Flash_Write_Reg_6_mux00001_1641 : STD_LOGIC; 
  signal mb_altera_core_Flash_Write_Reg_6_mux00002_1642 : STD_LOGIC; 
  signal mb_altera_core_Flash_Write_Reg_7_mux0000 : STD_LOGIC; 
  signal mb_altera_core_Flash_Write_Reg_7_mux00001_1645 : STD_LOGIC; 
  signal mb_altera_core_Flash_Write_Reg_7_mux00002_1646 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_cy_10_rt_1649 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_cy_11_rt_1651 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_cy_12_rt_1653 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_cy_13_rt_1655 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_cy_14_rt_1657 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_cy_15_rt_1659 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_cy_16_rt_1661 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_cy_17_rt_1663 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_cy_18_rt_1665 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_cy_19_rt_1667 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_cy_1_rt_1669 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_cy_20_rt_1671 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_cy_21_rt_1673 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_cy_22_rt_1675 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_cy_23_rt_1677 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_cy_24_rt_1679 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_cy_25_rt_1681 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_cy_26_rt_1683 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_cy_27_rt_1685 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_cy_28_rt_1687 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_cy_2_rt_1689 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_cy_3_rt_1691 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_cy_4_rt_1693 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_cy_5_rt_1695 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_cy_6_rt_1697 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_cy_7_rt_1699 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_cy_8_rt_1701 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_cy_9_rt_1703 : STD_LOGIC; 
  signal mb_altera_core_Madd_add0001_Madd_xor_29_rt_1705 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_num_samples_addsub0000_cy_3_Q : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_num_samples_addsub0000_cy_6_Q : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_start_addr_add0000_cy_10_rt_1750 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_start_addr_add0000_cy_11_rt_1752 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_start_addr_add0000_cy_12_rt_1754 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_start_addr_add0000_cy_13_rt_1756 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_start_addr_add0000_cy_14_rt_1758 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_start_addr_add0000_cy_15_rt_1760 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_start_addr_add0000_cy_16_rt_1762 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_start_addr_add0000_cy_17_rt_1764 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_start_addr_add0000_cy_18_rt_1766 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_start_addr_add0000_cy_19_rt_1768 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_start_addr_add0000_cy_20_rt_1770 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_start_addr_add0000_cy_21_rt_1772 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_start_addr_add0000_cy_22_rt_1774 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_start_addr_add0000_cy_23_rt_1776 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_start_addr_add0000_cy_24_rt_1778 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_start_addr_add0000_cy_25_rt_1780 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_start_addr_add0000_cy_26_rt_1782 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_start_addr_add0000_cy_27_rt_1784 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_start_addr_add0000_cy_28_rt_1786 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_start_addr_add0000_cy_29_rt_1788 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_10_rt_1793 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_11_rt_1795 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_12_rt_1797 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_13_rt_1799 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_14_rt_1801 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_15_rt_1803 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_16_rt_1805 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_17_rt_1807 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_18_rt_1809 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_19_rt_1811 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_1_rt_1813 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_20_rt_1815 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_21_rt_1817 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_22_rt_1819 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_23_rt_1821 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_24_rt_1823 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_25_rt_1825 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_26_rt_1827 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_27_rt_1829 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_28_rt_1831 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_29_rt_1833 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_2_rt_1835 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_30_rt_1837 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_3_rt_1839 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_4_rt_1841 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_5_rt_1843 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_6_rt_1845 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_7_rt_1847 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_8_rt_1849 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_9_rt_1851 : STD_LOGIC; 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_31_rt_1853 : STD_LOGIC; 
  signal mb_altera_core_Msub_s_dma_count_share0000_cy_0_Q : STD_LOGIC; 
  signal mb_altera_core_Msub_s_dma_count_share0000_cy_3_Q : STD_LOGIC; 
  signal mb_altera_core_Msub_s_dma_count_share0000_cy_6_Q : STD_LOGIC; 
  signal mb_altera_core_N0 : STD_LOGIC; 
  signal mb_altera_core_N1 : STD_LOGIC; 
  signal mb_altera_core_N10 : STD_LOGIC; 
  signal mb_altera_core_N12 : STD_LOGIC; 
  signal mb_altera_core_N13 : STD_LOGIC; 
  signal mb_altera_core_N15 : STD_LOGIC; 
  signal mb_altera_core_N17 : STD_LOGIC; 
  signal mb_altera_core_N38 : STD_LOGIC; 
  signal mb_altera_core_N40 : STD_LOGIC; 
  signal mb_altera_core_N41 : STD_LOGIC; 
  signal mb_altera_core_N42 : STD_LOGIC; 
  signal mb_altera_core_N44 : STD_LOGIC; 
  signal mb_altera_core_N7 : STD_LOGIC; 
  signal mb_altera_core_N8 : STD_LOGIC; 
  signal mb_altera_core_N9 : STD_LOGIC; 
  signal mb_altera_core_Read_Fifo_Data_Ready_2024 : STD_LOGIC; 
  signal mb_altera_core_Read_Fifo_Data_Ready_and0000 : STD_LOGIC; 
  signal mb_altera_core_Read_Fifo_Data_in_0_and0000 : STD_LOGIC; 
  signal mb_altera_core_Read_Fifo_Data_in_10_xor0000 : STD_LOGIC; 
  signal mb_altera_core_Read_Fifo_Data_in_11_xor0000 : STD_LOGIC; 
  signal mb_altera_core_Read_Fifo_Data_in_12_xor0000 : STD_LOGIC; 
  signal mb_altera_core_Read_Fifo_Data_in_13_xor0000 : STD_LOGIC; 
  signal mb_altera_core_Read_Fifo_Data_in_14_xor0000 : STD_LOGIC; 
  signal mb_altera_core_Read_Fifo_Data_in_15_xor0000 : STD_LOGIC; 
  signal mb_altera_core_Read_Fifo_Data_in_16_xor0000 : STD_LOGIC; 
  signal mb_altera_core_Read_Fifo_Data_in_17_xor0000 : STD_LOGIC; 
  signal mb_altera_core_Read_Fifo_Data_in_18_xor0000 : STD_LOGIC; 
  signal mb_altera_core_Read_Fifo_Data_in_19_xor0000 : STD_LOGIC; 
  signal mb_altera_core_Read_Fifo_Data_in_20_xor0000 : STD_LOGIC; 
  signal mb_altera_core_Read_Fifo_Data_in_21_xor0000 : STD_LOGIC; 
  signal mb_altera_core_Read_Fifo_Data_in_22_xor0000 : STD_LOGIC; 
  signal mb_altera_core_Read_Fifo_Data_in_23_xor0000 : STD_LOGIC; 
  signal mb_altera_core_Read_Fifo_Data_in_24_xor0000 : STD_LOGIC; 
  signal mb_altera_core_Read_Fifo_Data_in_25_xor0000 : STD_LOGIC; 
  signal mb_altera_core_Read_Fifo_Data_in_26_xor0000 : STD_LOGIC; 
  signal mb_altera_core_Read_Fifo_Data_in_27_xor0000 : STD_LOGIC; 
  signal mb_altera_core_Read_Fifo_Data_in_28_xor0000 : STD_LOGIC; 
  signal mb_altera_core_Read_Fifo_Data_in_29_xor0000 : STD_LOGIC; 
  signal mb_altera_core_Read_Fifo_Data_in_30_xor0000 : STD_LOGIC; 
  signal mb_altera_core_Read_Fifo_Data_in_31_xor0000 : STD_LOGIC; 
  signal mb_altera_core_Write_Fifo_Data_in_10_or0000 : STD_LOGIC; 
  signal mb_altera_core_Write_Fifo_Data_in_10_or000014 : STD_LOGIC; 
  signal mb_altera_core_Write_Fifo_Data_in_10_or0000141_2096 : STD_LOGIC; 
  signal mb_altera_core_Write_Fifo_Data_in_32_mux0000_2121 : STD_LOGIC; 
  signal mb_altera_core_Write_Fifo_Write_En_2128 : STD_LOGIC; 
  signal mb_altera_core_Write_Fifo_Write_En_not0001 : STD_LOGIC; 
  signal mb_altera_core_Write_Fifo_ef : STD_LOGIC; 
  signal mb_altera_core_Write_Fifo_ff : STD_LOGIC; 
  signal mb_altera_core_Write_fifo_wr_Cnt_and0000 : STD_LOGIC; 
  signal mb_altera_core_Write_fifo_wr_Cnt_and000012_2176 : STD_LOGIC; 
  signal mb_altera_core_Write_fifo_wr_Cnt_and000037_2177 : STD_LOGIC; 
  signal mb_altera_core_Write_fifo_wr_Cnt_and0000_inv : STD_LOGIC; 
  signal mb_altera_core_Write_fifo_wr_Cnt_not0001 : STD_LOGIC; 
  signal mb_altera_core_Write_fifo_wr_Cnt_not000110_2180 : STD_LOGIC; 
  signal mb_altera_core_Write_fifo_wr_Cnt_not00014_2181 : STD_LOGIC; 
  signal mb_altera_core_altera_asd_2212 : STD_LOGIC; 
  signal mb_altera_core_altera_asd_mux0000153_2213 : STD_LOGIC; 
  signal mb_altera_core_altera_asd_mux0000192 : STD_LOGIC; 
  signal mb_altera_core_altera_asd_mux000025_2215 : STD_LOGIC; 
  signal mb_altera_core_altera_asd_mux000029_2216 : STD_LOGIC; 
  signal mb_altera_core_altera_asd_mux000039_2217 : STD_LOGIC; 
  signal mb_altera_core_altera_asd_mux00006 : STD_LOGIC; 
  signal mb_altera_core_altera_asd_mux000061_2219 : STD_LOGIC; 
  signal mb_altera_core_altera_asd_mux000096_2220 : STD_LOGIC; 
  signal mb_altera_core_altera_dclk_2221 : STD_LOGIC; 
  signal mb_altera_core_altera_nce_2222 : STD_LOGIC; 
  signal mb_altera_core_altera_nce_and0000 : STD_LOGIC; 
  signal mb_altera_core_altera_ncon_2224 : STD_LOGIC; 
  signal mb_altera_core_altera_ncon_and0000 : STD_LOGIC; 
  signal mb_altera_core_altera_ncs_2226 : STD_LOGIC; 
  signal mb_altera_core_altera_ncs_and0000 : STD_LOGIC; 
  signal mb_altera_core_altera_state_FSM_FFd1_2228 : STD_LOGIC; 
  signal mb_altera_core_altera_state_FSM_FFd2_2229 : STD_LOGIC; 
  signal mb_altera_core_altera_state_FSM_FFd3_2230 : STD_LOGIC; 
  signal mb_altera_core_altera_state_FSM_FFd4_2231 : STD_LOGIC; 
  signal mb_altera_core_altera_state_FSM_FFd4_In1 : STD_LOGIC; 
  signal mb_altera_core_altera_state_FSM_FFd5_2233 : STD_LOGIC; 
  signal mb_altera_core_altera_state_FSM_FFd5_In1_2234 : STD_LOGIC; 
  signal mb_altera_core_flash_enable_2235 : STD_LOGIC; 
  signal mb_altera_core_flash_enable_and0000 : STD_LOGIC; 
  signal mb_altera_core_i_cs_inv : STD_LOGIC; 
  signal mb_altera_core_o_DBus_0_cmp_eq0000 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_0_mux0002104_2240 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_0_mux0002119 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_0_mux000215_2242 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_0_mux00022_2243 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_0_mux000227_2244 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_0_mux000259_2245 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_0_mux000265_2246 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_0_mux000286_2247 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_10_cmp_eq0003_2250 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_10_cmp_eq0004 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_10_cmp_eq0005 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_10_cmp_eq0007 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_10_mux000213_2254 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_10_mux000215_2255 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_10_mux000232_2256 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_10_mux000247_2257 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_10_mux000260_2258 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_11_mux000213_2260 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_11_mux000215_2261 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_11_mux000232_2262 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_11_mux000247_2263 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_11_mux000260_2264 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_12_mux000013_2266 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_12_mux000015_2267 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_12_mux000032_2268 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_12_mux000047_2269 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_12_mux000060_2270 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_13_mux000014_2272 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_13_mux000033_2273 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_13_mux000042_2274 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_13_mux000062 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_13_or0000 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_14_mux000014_2278 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_14_mux000048_2279 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_14_mux000070 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_15_mux000024_2282 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_15_mux000033_2283 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_15_mux00004_2284 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_15_mux000048_2285 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_15_mux000065 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_16_mux000013_2288 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_16_mux000033_2289 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_16_mux000045_2290 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_16_mux000072 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_17_mux000013_2293 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_17_mux000033_2294 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_17_mux000045_2295 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_17_mux000072 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_18_mux000013_2298 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_18_mux000033_2299 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_18_mux000045_2300 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_18_mux000072 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_19_mux000013_2303 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_19_mux000033_2304 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_19_mux000045_2305 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_19_mux000072 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_1_mux000213_2307 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_1_mux000267_2308 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_1_mux000267_SW0 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_1_mux000267_SW01_2310 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_1_mux000292 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_20_mux000013_2314 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_20_mux000015_2315 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_20_mux000032_2316 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_20_mux000047_2317 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_20_mux000060_2318 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_21_mux000013_2320 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_21_mux000015_2321 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_21_mux000032_2322 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_21_mux000047_2323 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_21_mux000060_2324 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_22_mux000013_2326 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_22_mux000033_2327 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_22_mux000045_2328 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_22_mux000072 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_23_mux000013_2331 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_23_mux000015_2332 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_23_mux000032_2333 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_23_mux000047_2334 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_23_mux000060_2335 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_24_mux000013_2337 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_24_mux000033_2338 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_24_mux000045_2339 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_24_mux000072 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_25_mux000013_2342 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_25_mux000015_2343 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_25_mux000032_2344 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_25_mux000047_2345 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_25_mux000060_2346 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_26_mux000013_2348 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_26_mux000015_2349 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_26_mux000032_2350 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_26_mux000047_2351 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_26_mux000060_2352 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_27_mux000013_2354 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_27_mux000033_2355 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_27_mux000045_2356 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_27_mux000072 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_28_mux000021_2359 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_28_mux000032_2360 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_28_mux00004_2361 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_28_mux000052_2362 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_28_mux000063 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_29_mux000013_2365 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_29_mux000015_2366 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_29_mux000034_2367 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_29_mux000040_2368 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_29_mux000074 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_2_mux0002110_2370 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_2_mux0002125 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_2_mux000213_2372 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_2_mux000222_2373 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_2_mux000230_2374 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_2_mux000243_2375 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_2_mux000267_2376 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_2_mux000295_2377 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_30_mux000014_2380 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_30_mux000033_2381 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_30_mux000042_2382 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_30_mux000062 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_30_mux00008_2384 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_31_cmp_eq0000 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_31_mux000013_2387 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_31_mux000015_2388 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_31_mux000032_2389 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_31_mux000047_2390 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_31_mux000060_2391 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_3_mux0000109_2392 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_3_mux0000115_2393 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_3_mux000013_2394 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_3_mux0000150 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_3_mux000016_2396 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_3_mux000069_2397 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_3_mux000080_2398 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_4_mux0002108_2400 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_4_mux0002120 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_4_mux000218 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_4_mux0002181_2403 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_4_mux0002182_2404 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_4_mux000236_2405 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_4_mux000239_2406 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_4_mux000280_2407 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_4_mux00029 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_4_mux000293_2409 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_5_mux000213_2411 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_5_mux000215_2412 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_5_mux000238_2413 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_5_mux000254_2414 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_5_mux000264_2415 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_5_mux000290_2416 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_5_mux000292 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_6_mux000222_2419 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_6_mux000224_2420 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_6_mux00024_2421 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_6_mux000247_2422 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_6_mux000263_2423 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_6_mux000273_2424 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_6_mux000296 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_7_mux000213_2427 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_7_mux000215_2428 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_7_mux000238_2429 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_7_mux000254_2430 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_7_mux000264_2431 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_7_mux000290_2432 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_7_mux000292 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_8_mux0002105 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_8_mux00021051 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_8_mux000210511_2437 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_8_mux000212_2438 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_8_mux000222_2439 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_8_mux000257_2440 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_8_mux000292_2441 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_9_mux000213_2443 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_9_mux000215_2444 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_9_mux000232_2445 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_9_mux000247_2446 : STD_LOGIC; 
  signal mb_altera_core_o_DBus_9_mux000260_2447 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_10_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_11_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_12_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_13_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_14_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_15_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_16_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_17_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_18_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_19_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_1_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_20_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_21_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_22_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_23_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_24_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_25_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_26_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_27_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_28_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_29_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_2_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_30_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_31_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_3_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_4_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_5_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_6_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_7_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_8_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_altera_data_out_9_xor0000 : STD_LOGIC; 
  signal mb_altera_core_o_user_io_and0000 : STD_LOGIC; 
  signal mb_altera_core_s_altera_data_oen_2545 : STD_LOGIC; 
  signal mb_altera_core_s_altera_data_oen_mux00001 : STD_LOGIC; 
  signal mb_altera_core_s_altera_data_oen_mux000011_2547 : STD_LOGIC; 
  signal mb_altera_core_s_altera_data_oen_mux000012_2548 : STD_LOGIC; 
  signal mb_altera_core_s_altera_fifo_rst_2549 : STD_LOGIC; 
  signal mb_altera_core_s_altera_fifo_rst_not0001_2550 : STD_LOGIC; 
  signal mb_altera_core_s_altera_int_busy_2551 : STD_LOGIC; 
  signal mb_altera_core_s_altera_int_busy_mux0000 : STD_LOGIC; 
  signal mb_altera_core_s_altera_int_en_2553 : STD_LOGIC; 
  signal mb_altera_core_s_altera_rdfifo_din_1_xor0000 : STD_LOGIC; 
  signal mb_altera_core_s_altera_rdfifo_din_2_xor0000 : STD_LOGIC; 
  signal mb_altera_core_s_altera_rdfifo_din_3_xor0000 : STD_LOGIC; 
  signal mb_altera_core_s_altera_rdfifo_din_4_xor0000 : STD_LOGIC; 
  signal mb_altera_core_s_altera_rdfifo_din_5_xor0000 : STD_LOGIC; 
  signal mb_altera_core_s_altera_rdfifo_din_6_xor0000 : STD_LOGIC; 
  signal mb_altera_core_s_altera_rdfifo_din_7_xor0000 : STD_LOGIC; 
  signal mb_altera_core_s_altera_rdfifo_din_8_xor0000 : STD_LOGIC; 
  signal mb_altera_core_s_altera_rdfifo_din_9_xor0000 : STD_LOGIC; 
  signal mb_altera_core_s_altera_rdfifo_empty : STD_LOGIC; 
  signal mb_altera_core_s_altera_rdfifo_rd_en : STD_LOGIC; 
  signal mb_altera_core_s_altera_rdfifo_valid : STD_LOGIC; 
  signal mb_altera_core_s_altera_rdfifo_wr_en_2610 : STD_LOGIC; 
  signal mb_altera_core_s_altera_rdfifo_wr_en_not0001 : STD_LOGIC; 
  signal mb_altera_core_s_altera_write_busy_2612 : STD_LOGIC; 
  signal mb_altera_core_s_altera_write_busy_not0001 : STD_LOGIC; 
  signal mb_altera_core_s_dma_base_addr_and0000 : STD_LOGIC; 
  signal mb_altera_core_s_dma_count_mux0003_0_1_2650 : STD_LOGIC; 
  signal mb_altera_core_s_dma_count_mux0003_0_2 : STD_LOGIC; 
  signal mb_altera_core_s_dma_count_mux0003_1_1_2652 : STD_LOGIC; 
  signal mb_altera_core_s_dma_count_mux0003_2_34_2653 : STD_LOGIC; 
  signal mb_altera_core_s_dma_count_mux0003_2_48 : STD_LOGIC; 
  signal mb_altera_core_s_dma_count_mux0003_3_1_2655 : STD_LOGIC; 
  signal mb_altera_core_s_dma_count_mux0003_4_1_2656 : STD_LOGIC; 
  signal mb_altera_core_s_dma_count_mux0003_5_30_2657 : STD_LOGIC; 
  signal mb_altera_core_s_dma_count_mux0003_5_43 : STD_LOGIC; 
  signal mb_altera_core_s_dma_count_mux0003_6_1_2659 : STD_LOGIC; 
  signal mb_altera_core_s_dma_count_mux0003_7_1_2660 : STD_LOGIC; 
  signal mb_altera_core_s_dma_count_mux0003_8_1_2661 : STD_LOGIC; 
  signal mb_altera_core_s_dma_en_2662 : STD_LOGIC; 
  signal mb_altera_core_s_dma_irq_2663 : STD_LOGIC; 
  signal mb_altera_core_s_dma_irq_clear_2664 : STD_LOGIC; 
  signal mb_altera_core_s_dma_irq_clear_not0001 : STD_LOGIC; 
  signal mb_altera_core_s_dma_irq_clear_not000111_2666 : STD_LOGIC; 
  signal mb_altera_core_s_dma_irq_clear_not000123_2667 : STD_LOGIC; 
  signal mb_altera_core_s_dma_num_samples_mux0000_2_1_2681 : STD_LOGIC; 
  signal mb_altera_core_s_dma_num_samples_mux0000_2_2_2682 : STD_LOGIC; 
  signal mb_altera_core_s_dma_num_samples_mux0000_5_1_2686 : STD_LOGIC; 
  signal mb_altera_core_s_dma_num_samples_mux0000_5_2_2687 : STD_LOGIC; 
  signal mb_altera_core_s_dma_num_samples_mux0000_8_1_2691 : STD_LOGIC; 
  signal mb_altera_core_s_dma_num_samples_mux0000_8_2_2692 : STD_LOGIC; 
  signal mb_altera_core_s_dma_req_2694 : STD_LOGIC; 
  signal mb_altera_core_s_dma_req_mux00011 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_10_1_2726 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_10_11_2727 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_11_1_2728 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_12_1_2729 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_13_1_2730 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_14_1_2731 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_15_1_2732 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_16_1_2733 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_17_1_2734 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_18_1_2735 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_19_1_2736 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_20_1_2737 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_21_1_2738 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_22_1_2739 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_23_1_2740 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_24_1_2741 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_25_1_2742 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_26_1_2743 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_27_1_2744 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_28_1_2745 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_29_1_2746 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_2_1_2747 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_30_1_2748 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_31_1_2749 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_3_1_2750 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_4_1_2751 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_5_1_2752 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_6_1_2753 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_7_1_2754 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_8_1_2755 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_addr_mux0000_9_1_2756 : STD_LOGIC; 
  signal mb_altera_core_s_dma_space_mask_and0000 : STD_LOGIC; 
  signal mb_altera_core_s_dma_state_FSM_FFd1_2871 : STD_LOGIC; 
  signal mb_altera_core_s_dma_state_FSM_FFd1_In16_2872 : STD_LOGIC; 
  signal mb_altera_core_s_dma_state_FSM_FFd1_In26 : STD_LOGIC; 
  signal mb_altera_core_s_dma_state_FSM_FFd1_In5_2874 : STD_LOGIC; 
  signal mb_altera_core_s_dma_state_FSM_FFd1_In8_2875 : STD_LOGIC; 
  signal mb_altera_core_s_dma_state_FSM_FFd2_2876 : STD_LOGIC; 
  signal mb_altera_core_s_dma_state_FSM_FFd2_In : STD_LOGIC; 
  signal mb_altera_core_s_dma_state_FSM_FFd2_In1_2878 : STD_LOGIC; 
  signal mb_altera_core_s_dma_state_FSM_FFd2_In2_2879 : STD_LOGIC; 
  signal mb_altera_core_s_dma_state_cmp_eq0003 : STD_LOGIC; 
  signal mb_altera_core_s_dma_state_cmp_ge0000 : STD_LOGIC; 
  signal mb_altera_core_s_dma_state_cmp_ge0001 : STD_LOGIC; 
  signal mb_altera_core_s_dma_timeout_threshold_and0000 : STD_LOGIC; 
  signal mb_altera_core_s_err_clr_3012 : STD_LOGIC; 
  signal mb_altera_core_s_err_clr_and0000 : STD_LOGIC; 
  signal mb_altera_core_s_err_invalid_rd_3014 : STD_LOGIC; 
  signal mb_altera_core_s_err_invalid_rd_and0000 : STD_LOGIC; 
  signal mb_altera_core_s_i_altera_int_r1_3016 : STD_LOGIC; 
  signal mb_altera_core_s_i_altera_int_r2_3017 : STD_LOGIC; 
  signal mb_altera_core_ver_rd_3018 : STD_LOGIC; 
  signal mb_altera_core_ver_rd_not0001 : STD_LOGIC; 
  signal mb_altera_core_version_N01 : STD_LOGIC; 
  signal mb_altera_core_version_o_data_0_Q : STD_LOGIC; 
  signal mb_altera_core_version_o_data_2_Q : STD_LOGIC; 
  signal mb_altera_core_version_rd_toggle_3023 : STD_LOGIC; 
  signal o_altera_Clk_OBUF_3025 : STD_LOGIC; 
  signal o_altera_read_ff_ff_OBUF_3033 : STD_LOGIC; 
  signal o_altera_write_busy_bar_OBUF_3035 : STD_LOGIC; 
  signal o_busy_OBUF_3037 : STD_LOGIC; 
  signal o_dsp_ext_int5_OBUF_3041 : STD_LOGIC; 
  signal o_dsp_ext_int7_OBUF_3044 : STD_LOGIC; 
  signal o_eth_mdc_OBUF_3047 : STD_LOGIC; 
  signal o_eth_mdio : STD_LOGIC; 
  signal o_eth_ref_clk_OBUF_3050 : STD_LOGIC; 
  signal o_eth_rst_n_OBUF_3052 : STD_LOGIC; 
  signal o_eth_tx_data_0_OBUF_3057 : STD_LOGIC; 
  signal o_eth_tx_data_1_OBUF_3058 : STD_LOGIC; 
  signal o_eth_tx_data_2_OBUF_3059 : STD_LOGIC; 
  signal o_eth_tx_data_3_OBUF_3060 : STD_LOGIC; 
  signal o_eth_tx_en_OBUF_3062 : STD_LOGIC; 
  signal o_hold_n_OBUF_3065 : STD_LOGIC; 
  signal o_rs232_xmit_OBUF_3067 : STD_LOGIC; 
  signal o_user_led_n_OBUF_3069 : STD_LOGIC; 
  signal phy_rst : STD_LOGIC; 
  signal rx_clk : STD_LOGIC; 
  signal serdes_clk150_fx : STD_LOGIC; 
  signal t_fl_bank_sel_n_inv : STD_LOGIC; 
  signal tx_clk : STD_LOGIC; 
  signal wd_rst_inv : STD_LOGIC; 
  signal NLW_serdes_clk150_bufg_O_UNCONNECTED : STD_LOGIC; 
  signal NLW_DCM_emif_serdes_DSSEN_UNCONNECTED : STD_LOGIC; 
  signal NLW_DCM_emif_serdes_CLK90_UNCONNECTED : STD_LOGIC; 
  signal NLW_DCM_emif_serdes_CLK180_UNCONNECTED : STD_LOGIC; 
  signal NLW_DCM_emif_serdes_CLK270_UNCONNECTED : STD_LOGIC; 
  signal NLW_DCM_emif_serdes_CLK2X_UNCONNECTED : STD_LOGIC; 
  signal NLW_DCM_emif_serdes_CLK2X180_UNCONNECTED : STD_LOGIC; 
  signal NLW_DCM_emif_serdes_CLKDV_UNCONNECTED : STD_LOGIC; 
  signal NLW_DCM_emif_serdes_CLKFX180_UNCONNECTED : STD_LOGIC; 
  signal NLW_DCM_emif_serdes_PSDONE_UNCONNECTED : STD_LOGIC; 
  signal NLW_DCM_emif_serdes_STATUS_7_UNCONNECTED : STD_LOGIC; 
  signal NLW_DCM_emif_serdes_STATUS_6_UNCONNECTED : STD_LOGIC; 
  signal NLW_DCM_emif_serdes_STATUS_5_UNCONNECTED : STD_LOGIC; 
  signal NLW_DCM_emif_serdes_STATUS_4_UNCONNECTED : STD_LOGIC; 
  signal NLW_DCM_emif_serdes_STATUS_3_UNCONNECTED : STD_LOGIC; 
  signal NLW_DCM_emif_serdes_STATUS_2_UNCONNECTED : STD_LOGIC; 
  signal NLW_DCM_emif_serdes_STATUS_0_UNCONNECTED : STD_LOGIC; 
  signal NLW_uart1_o_rcv_cts_UNCONNECTED : STD_LOGIC; 
  signal NLW_uart1_o_dtr_UNCONNECTED : STD_LOGIC; 
  signal NLW_emac_o_clk2_5_UNCONNECTED : STD_LOGIC; 
  signal NLW_emac_o_tx_er_UNCONNECTED : STD_LOGIC; 
  signal NLW_emac_o_tx_spd_100_UNCONNECTED : STD_LOGIC; 
  signal NLW_emac_o_ref_clk_UNCONNECTED : STD_LOGIC; 
  signal NLW_emac_o_rx_en_UNCONNECTED : STD_LOGIC; 
  signal NLW_mb_altera_core_Altera_WriteFifo_Inst_wr_data_count_9_UNCONNECTED : STD_LOGIC; 
  signal NLW_mb_altera_core_Altera_WriteFifo_Inst_wr_data_count_8_UNCONNECTED : STD_LOGIC; 
  signal NLW_mb_altera_core_Altera_WriteFifo_Inst_wr_data_count_7_UNCONNECTED : STD_LOGIC; 
  signal NLW_mb_altera_core_Altera_WriteFifo_Inst_wr_data_count_6_UNCONNECTED : STD_LOGIC; 
  signal NLW_mb_altera_core_Altera_WriteFifo_Inst_wr_data_count_5_UNCONNECTED : STD_LOGIC; 
  signal NLW_mb_altera_core_Altera_WriteFifo_Inst_wr_data_count_4_UNCONNECTED : STD_LOGIC; 
  signal NLW_mb_altera_core_Altera_WriteFifo_Inst_wr_data_count_3_UNCONNECTED : STD_LOGIC; 
  signal NLW_mb_altera_core_Altera_WriteFifo_Inst_wr_data_count_2_UNCONNECTED : STD_LOGIC; 
  signal NLW_mb_altera_core_Altera_WriteFifo_Inst_wr_data_count_1_UNCONNECTED : STD_LOGIC; 
  signal NLW_mb_altera_core_Altera_WriteFifo_Inst_wr_data_count_0_UNCONNECTED : STD_LOGIC; 
  signal Mtridata_io_altera_dsp : STD_LOGIC_VECTOR ( 32 downto 0 ); 
  signal dcm_status : STD_LOGIC_VECTOR ( 1 downto 1 ); 
  signal dma_addr : STD_LOGIC_VECTOR2 ( 0 downto 0 , 20 downto 0 ); 
  signal dma_data : STD_LOGIC_VECTOR2 ( 0 downto 0 , 31 downto 0 ); 
  signal ea : STD_LOGIC_VECTOR ( 15 downto 15 ); 
  signal inst_base_module_Msub_wd_counter_addsub0000_cy : STD_LOGIC_VECTOR ( 14 downto 0 ); 
  signal inst_base_module_Msub_wd_counter_addsub0000_lut : STD_LOGIC_VECTOR ( 15 downto 1 ); 
  signal inst_base_module_varindex0000 : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal inst_base_module_bank_addr : STD_LOGIC_VECTOR ( 0 downto 0 ); 
  signal inst_base_module_dcm_reset_shift : STD_LOGIC_VECTOR ( 15 downto 15 ); 
  signal inst_base_module_div10_2 : STD_LOGIC_VECTOR ( 10 downto 10 ); 
  signal inst_base_module_div10_3 : STD_LOGIC_VECTOR ( 10 downto 10 ); 
  signal inst_base_module_div10_4 : STD_LOGIC_VECTOR ( 10 downto 10 ); 
  signal inst_base_module_div10_5 : STD_LOGIC_VECTOR ( 10 downto 10 ); 
  signal inst_base_module_div5_1 : STD_LOGIC_VECTOR ( 5 downto 5 ); 
  signal inst_base_module_div5_2 : STD_LOGIC_VECTOR ( 5 downto 5 ); 
  signal inst_base_module_irq_enable : STD_LOGIC_VECTOR ( 7 downto 4 ); 
  signal inst_base_module_irq_mask : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal inst_base_module_irq_mask_mux0000 : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal inst_base_module_key_state : STD_LOGIC_VECTOR ( 2 downto 0 ); 
  signal inst_base_module_key_state_mux0003 : STD_LOGIC_VECTOR ( 2 downto 0 ); 
  signal inst_base_module_led_enable : STD_LOGIC_VECTOR ( 0 downto 0 ); 
  signal inst_base_module_o_DBus : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal inst_base_module_wd_counter : STD_LOGIC_VECTOR ( 15 downto 0 ); 
  signal inst_base_module_wd_counter_addsub0000 : STD_LOGIC_VECTOR ( 15 downto 0 ); 
  signal inst_base_module_wd_counter_mux0000 : STD_LOGIC_VECTOR ( 15 downto 0 ); 
  signal inst_base_module_wd_timeout : STD_LOGIC_VECTOR ( 15 downto 0 ); 
  signal inst_emif_iface_addr_r : STD_LOGIC_VECTOR ( 4 downto 0 ); 
  signal inst_emif_iface_be : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal inst_emif_iface_be_r : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal inst_emif_iface_ea : STD_LOGIC_VECTOR ( 11 downto 2 ); 
  signal inst_emif_iface_edi : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal inst_emif_iface_edi_r : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal inst_emif_iface_edo_ce3 : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal inst_emif_iface_o_be_n : STD_LOGIC_VECTOR ( 3 downto 3 ); 
  signal inst_emif_iface_o_ea : STD_LOGIC_VECTOR ( 14 downto 2 ); 
  signal inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy : STD_LOGIC_VECTOR ( 4 downto 0 ); 
  signal inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_lut : STD_LOGIC_VECTOR ( 4 downto 0 ); 
  signal inst_emif_iface_sdram_dma_std_dma_engine_bank : STD_LOGIC_VECTOR ( 1 downto 0 ); 
  signal inst_emif_iface_sdram_dma_std_dma_engine_be_n : STD_LOGIC_VECTOR ( 0 downto 0 ); 
  signal inst_emif_iface_sdram_dma_std_dma_engine_ce_n_mux0002 : STD_LOGIC_VECTOR ( 3 downto 3 ); 
  signal inst_emif_iface_sdram_dma_std_dma_engine_col : STD_LOGIC_VECTOR ( 7 downto 0 ); 
  signal inst_emif_iface_sdram_dma_std_dma_engine_dma_count : STD_LOGIC_VECTOR ( 8 downto 0 ); 
  signal inst_emif_iface_sdram_dma_std_dma_engine_dma_din : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal inst_emif_iface_sdram_dma_std_dma_engine_dma_we_r : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal inst_emif_iface_sdram_dma_std_dma_engine_ea : STD_LOGIC_VECTOR ( 14 downto 2 ); 
  signal inst_emif_iface_sdram_dma_std_dma_engine_holda_r : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal inst_emif_iface_sdram_dma_std_dma_engine_holdn_r : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row : STD_LOGIC_VECTOR ( 10 downto 0 ); 
  signal inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001 : STD_LOGIC_VECTOR ( 8 downto 8 ); 
  signal inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr : STD_LOGIC_VECTOR ( 20 downto 0 ); 
  signal inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt : STD_LOGIC_VECTOR ( 1 downto 0 ); 
  signal inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt_mux0000 : STD_LOGIC_VECTOR ( 1 downto 0 ); 
  signal inst_emif_iface_sdram_dma_std_dma_engine_word_count : STD_LOGIC_VECTOR ( 8 downto 0 ); 
  signal inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000 : STD_LOGIC_VECTOR ( 8 downto 2 ); 
  signal mb_altera_core_Flash_Cnt : STD_LOGIC_VECTOR ( 2 downto 0 ); 
  signal mb_altera_core_Flash_Cnt_mux0000 : STD_LOGIC_VECTOR ( 2 downto 0 ); 
  signal mb_altera_core_Flash_Read_Reg : STD_LOGIC_VECTOR ( 7 downto 0 ); 
  signal mb_altera_core_Flash_Write_Reg : STD_LOGIC_VECTOR ( 7 downto 0 ); 
  signal mb_altera_core_Madd_add0001_Madd_cy : STD_LOGIC_VECTOR ( 28 downto 0 ); 
  signal mb_altera_core_Madd_add0001_Madd_lut : STD_LOGIC_VECTOR ( 0 downto 0 ); 
  signal mb_altera_core_Madd_o_dma_addr_cy : STD_LOGIC_VECTOR ( 19 downto 0 ); 
  signal mb_altera_core_Madd_o_dma_addr_lut : STD_LOGIC_VECTOR ( 20 downto 0 ); 
  signal mb_altera_core_Madd_s_dma_start_addr_add0000_cy : STD_LOGIC_VECTOR ( 29 downto 9 ); 
  signal mb_altera_core_Madd_s_dma_start_addr_add0000_lut : STD_LOGIC_VECTOR ( 9 downto 9 ); 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy : STD_LOGIC_VECTOR ( 30 downto 0 ); 
  signal mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_lut : STD_LOGIC_VECTOR ( 0 downto 0 ); 
  signal mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy : STD_LOGIC_VECTOR ( 30 downto 0 ); 
  signal mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut : STD_LOGIC_VECTOR ( 30 downto 0 ); 
  signal mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy : STD_LOGIC_VECTOR ( 30 downto 0 ); 
  signal mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal mb_altera_core_Mcompar_s_dma_state_cmp_gt0000_cy : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal mb_altera_core_Mcompar_s_dma_state_cmp_gt0000_lut : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy : STD_LOGIC_VECTOR ( 8 downto 0 ); 
  signal mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut : STD_LOGIC_VECTOR ( 9 downto 0 ); 
  signal mb_altera_core_Read_Fifo_Data_in : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal mb_altera_core_Result : STD_LOGIC_VECTOR ( 9 downto 0 ); 
  signal mb_altera_core_Write_Fifo_Data_in : STD_LOGIC_VECTOR ( 32 downto 0 ); 
  signal mb_altera_core_Write_Fifo_data_out : STD_LOGIC_VECTOR ( 32 downto 0 ); 
  signal mb_altera_core_Write_fifo_wr_Cnt : STD_LOGIC_VECTOR ( 9 downto 0 ); 
  signal mb_altera_core_add0001 : STD_LOGIC_VECTOR ( 29 downto 0 ); 
  signal mb_altera_core_o_DBus : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal mb_altera_core_o_altera_data_out : STD_LOGIC_VECTOR ( 32 downto 0 ); 
  signal mb_altera_core_o_user_io : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal mb_altera_core_s_altera_rdfifo_din : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal mb_altera_core_s_altera_rdfifo_rd_cnt : STD_LOGIC_VECTOR ( 12 downto 1 ); 
  signal mb_altera_core_s_dma_base_addr : STD_LOGIC_VECTOR ( 25 downto 0 ); 
  signal mb_altera_core_s_dma_count : STD_LOGIC_VECTOR ( 8 downto 0 ); 
  signal mb_altera_core_s_dma_num_samples : STD_LOGIC_VECTOR ( 9 downto 0 ); 
  signal mb_altera_core_s_dma_num_samples_mux0000 : STD_LOGIC_VECTOR ( 9 downto 0 ); 
  signal mb_altera_core_s_dma_space_addr : STD_LOGIC_VECTOR ( 29 downto 0 ); 
  signal mb_altera_core_s_dma_space_mask : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal mb_altera_core_s_dma_start_addr : STD_LOGIC_VECTOR ( 29 downto 0 ); 
  signal mb_altera_core_s_dma_start_addr_add0000 : STD_LOGIC_VECTOR ( 29 downto 9 ); 
  signal mb_altera_core_s_dma_start_addr_mux0000 : STD_LOGIC_VECTOR ( 31 downto 2 ); 
  signal mb_altera_core_s_dma_timeout_cntr : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal mb_altera_core_s_dma_timeout_cntr_addsub0000 : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal mb_altera_core_s_dma_timeout_cntr_mux0000 : STD_LOGIC_VECTOR ( 31 downto 0 ); 
  signal mb_altera_core_s_dma_timeout_threshold : STD_LOGIC_VECTOR ( 31 downto 0 ); 
begin
  XST_GND : GND
    port map (
      G => ea(15)
    );
  XST_VCC : VCC
    port map (
      P => Mtrien_io_altera_dsp_mux0000_norst
    );
  Mtridata_io_altera_dsp_0 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(0),
      Q => Mtridata_io_altera_dsp(0)
    );
  Mtridata_io_altera_dsp_1 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(1),
      Q => Mtridata_io_altera_dsp(1)
    );
  Mtridata_io_altera_dsp_2 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(2),
      Q => Mtridata_io_altera_dsp(2)
    );
  Mtridata_io_altera_dsp_3 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(3),
      Q => Mtridata_io_altera_dsp(3)
    );
  Mtridata_io_altera_dsp_4 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(4),
      Q => Mtridata_io_altera_dsp(4)
    );
  Mtridata_io_altera_dsp_5 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(5),
      Q => Mtridata_io_altera_dsp(5)
    );
  Mtridata_io_altera_dsp_6 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(6),
      Q => Mtridata_io_altera_dsp(6)
    );
  Mtridata_io_altera_dsp_7 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(7),
      Q => Mtridata_io_altera_dsp(7)
    );
  Mtridata_io_altera_dsp_8 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(8),
      Q => Mtridata_io_altera_dsp(8)
    );
  Mtridata_io_altera_dsp_9 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(9),
      Q => Mtridata_io_altera_dsp(9)
    );
  Mtridata_io_altera_dsp_10 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(10),
      Q => Mtridata_io_altera_dsp(10)
    );
  Mtridata_io_altera_dsp_11 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(11),
      Q => Mtridata_io_altera_dsp(11)
    );
  Mtridata_io_altera_dsp_12 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(12),
      Q => Mtridata_io_altera_dsp(12)
    );
  Mtridata_io_altera_dsp_13 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(13),
      Q => Mtridata_io_altera_dsp(13)
    );
  Mtridata_io_altera_dsp_14 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(14),
      Q => Mtridata_io_altera_dsp(14)
    );
  Mtridata_io_altera_dsp_15 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(15),
      Q => Mtridata_io_altera_dsp(15)
    );
  Mtridata_io_altera_dsp_16 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(16),
      Q => Mtridata_io_altera_dsp(16)
    );
  Mtridata_io_altera_dsp_17 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(17),
      Q => Mtridata_io_altera_dsp(17)
    );
  Mtridata_io_altera_dsp_18 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(18),
      Q => Mtridata_io_altera_dsp(18)
    );
  Mtridata_io_altera_dsp_19 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(19),
      Q => Mtridata_io_altera_dsp(19)
    );
  Mtridata_io_altera_dsp_20 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(20),
      Q => Mtridata_io_altera_dsp(20)
    );
  Mtridata_io_altera_dsp_21 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(21),
      Q => Mtridata_io_altera_dsp(21)
    );
  Mtridata_io_altera_dsp_22 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(22),
      Q => Mtridata_io_altera_dsp(22)
    );
  Mtridata_io_altera_dsp_23 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(23),
      Q => Mtridata_io_altera_dsp(23)
    );
  Mtridata_io_altera_dsp_24 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(24),
      Q => Mtridata_io_altera_dsp(24)
    );
  Mtridata_io_altera_dsp_25 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(25),
      Q => Mtridata_io_altera_dsp(25)
    );
  Mtridata_io_altera_dsp_26 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(26),
      Q => Mtridata_io_altera_dsp(26)
    );
  Mtridata_io_altera_dsp_27 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(27),
      Q => Mtridata_io_altera_dsp(27)
    );
  Mtridata_io_altera_dsp_28 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(28),
      Q => Mtridata_io_altera_dsp(28)
    );
  Mtridata_io_altera_dsp_29 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(29),
      Q => Mtridata_io_altera_dsp(29)
    );
  Mtridata_io_altera_dsp_30 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(30),
      Q => Mtridata_io_altera_dsp(30)
    );
  Mtridata_io_altera_dsp_31 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(31),
      Q => Mtridata_io_altera_dsp(31)
    );
  Mtridata_io_altera_dsp_32 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_o_altera_data_out(32),
      Q => Mtridata_io_altera_dsp(32)
    );
  o_altera_write_busy_bar_36 : FDR
    port map (
      C => emif_clk,
      D => Mtrien_io_altera_dsp_mux0000_norst,
      R => mb_altera_core_s_altera_write_busy_2612,
      Q => o_altera_write_busy_bar_OBUF_3035
    );
  Mtrien_io_altera_dsp : FDR
    port map (
      C => emif_clk,
      D => Mtrien_io_altera_dsp_mux0000_norst,
      R => mb_altera_core_s_altera_data_oen_2545,
      Q => Mtrien_io_altera_dsp_33
    );
  clk25mhz_bufg : BUFG
    port map (
      I => i_clk25mhz_IBUFG_499,
      O => o_eth_ref_clk_OBUF_3050
    );
  emif_clk_ibufg : IBUFG
    generic map(
      CAPACITANCE => "DONT_CARE",
      IBUF_DELAY_VALUE => "0",
      IBUF_LOW_PWR => TRUE,
      IOSTANDARD => "DEFAULT"
    )
    port map (
      I => i_emif_clk,
      O => emif_clk_raw
    );
  emif_clk_bufg : BUFG
    port map (
      I => emif_clk_dcm,
      O => emif_clk
    );
  altera_clk_buf : BUFG
    port map (
      I => emif_clk_dcm,
      O => o_altera_Clk_OBUF_3025
    );
  serdes_clk150_bufg : BUFG
    port map (
      I => serdes_clk150_fx,
      O => NLW_serdes_clk150_bufg_O_UNCONNECTED
    );
  eth_rx_bufg : BUFG
    port map (
      I => i_eth_rx_clk_IBUFG_508,
      O => rx_clk
    );
  eth_tx_bufg : BUFG
    port map (
      I => i_eth_tx_clk_IBUFG_522,
      O => tx_clk
    );
  DCM_emif_serdes : DCM
    generic map(
      CLKDV_DIVIDE => 2.000000,
      CLKFX_DIVIDE => 1,
      CLKFX_MULTIPLY => 3,
      CLKIN_DIVIDE_BY_2 => FALSE,
      CLKIN_PERIOD => 20.000000,
      CLKOUT_PHASE_SHIFT => "NONE",
      CLK_FEEDBACK => "1X",
      DESKEW_ADJUST => "SYSTEM_SYNCHRONOUS",
      DFS_FREQUENCY_MODE => "LOW",
      DLL_FREQUENCY_MODE => "LOW",
      DSS_MODE => "NONE",
      DUTY_CYCLE_CORRECTION => TRUE,
      PHASE_SHIFT => 0,
      SIM_MODE => "SAFE",
      STARTUP_WAIT => FALSE,
      FACTORY_JF => X"8080"
    )
    port map (
      RST => dcm_reset,
      CLKIN => emif_clk_raw,
      CLKFB => emif_clk,
      PSINCDEC => ea(15),
      PSEN => ea(15),
      PSCLK => ea(15),
      DSSEN => NLW_DCM_emif_serdes_DSSEN_UNCONNECTED,
      CLK0 => emif_clk_dcm,
      CLK90 => NLW_DCM_emif_serdes_CLK90_UNCONNECTED,
      CLK180 => NLW_DCM_emif_serdes_CLK180_UNCONNECTED,
      CLK270 => NLW_DCM_emif_serdes_CLK270_UNCONNECTED,
      CLK2X => NLW_DCM_emif_serdes_CLK2X_UNCONNECTED,
      CLK2X180 => NLW_DCM_emif_serdes_CLK2X180_UNCONNECTED,
      CLKDV => NLW_DCM_emif_serdes_CLKDV_UNCONNECTED,
      CLKFX => serdes_clk150_fx,
      CLKFX180 => NLW_DCM_emif_serdes_CLKFX180_UNCONNECTED,
      LOCKED => dcm_lock,
      PSDONE => NLW_DCM_emif_serdes_PSDONE_UNCONNECTED,
      STATUS(7) => NLW_DCM_emif_serdes_STATUS_7_UNCONNECTED,
      STATUS(6) => NLW_DCM_emif_serdes_STATUS_6_UNCONNECTED,
      STATUS(5) => NLW_DCM_emif_serdes_STATUS_5_UNCONNECTED,
      STATUS(4) => NLW_DCM_emif_serdes_STATUS_4_UNCONNECTED,
      STATUS(3) => NLW_DCM_emif_serdes_STATUS_3_UNCONNECTED,
      STATUS(2) => NLW_DCM_emif_serdes_STATUS_2_UNCONNECTED,
      STATUS(1) => dcm_status(1),
      STATUS(0) => NLW_DCM_emif_serdes_STATUS_0_UNCONNECTED
    );
  uart1 : uart
    port map (
      i_uart_cs => inst_emif_iface_cs_r_1_Q,
      clk => emif_clk,
      o_xmit => o_rs232_xmit_OBUF_3067,
      i_ri => ea(15),
      o_rcv_cts => NLW_uart1_o_rcv_cts_UNCONNECTED,
      i_rd_en => inst_emif_iface_rd_r_1175,
      i_dsr => ea(15),
      o_irq => irq_map_7_0_Q,
      i_wr_en => inst_emif_iface_wr_r_1474,
      i_xmit_cts => ea(15),
      i_rcv => i_rs232_rcv_IBUF_526,
      i_ivector_vld => Mtrien_io_altera_dsp_mux0000_norst,
      o_dtr => NLW_uart1_o_dtr_UNCONNECTED,
      i_ilevel_vld => Mtrien_io_altera_dsp_mux0000_norst,
      i_dcd => ea(15),
      o_DBus(31) => edo_1_31_Q,
      o_DBus(30) => edo_1_30_Q,
      o_DBus(29) => edo_1_29_Q,
      o_DBus(28) => edo_1_28_Q,
      o_DBus(27) => edo_1_27_Q,
      o_DBus(26) => edo_1_26_Q,
      o_DBus(25) => edo_1_25_Q,
      o_DBus(24) => edo_1_24_Q,
      o_DBus(23) => edo_1_23_Q,
      o_DBus(22) => edo_1_22_Q,
      o_DBus(21) => edo_1_21_Q,
      o_DBus(20) => edo_1_20_Q,
      o_DBus(19) => edo_1_19_Q,
      o_DBus(18) => edo_1_18_Q,
      o_DBus(17) => edo_1_17_Q,
      o_DBus(16) => edo_1_16_Q,
      o_DBus(15) => edo_1_15_Q,
      o_DBus(14) => edo_1_14_Q,
      o_DBus(13) => edo_1_13_Q,
      o_DBus(12) => edo_1_12_Q,
      o_DBus(11) => edo_1_11_Q,
      o_DBus(10) => edo_1_10_Q,
      o_DBus(9) => edo_1_9_Q,
      o_DBus(8) => edo_1_8_Q,
      o_DBus(7) => edo_1_7_Q,
      o_DBus(6) => edo_1_6_Q,
      o_DBus(5) => edo_1_5_Q,
      o_DBus(4) => edo_1_4_Q,
      o_DBus(3) => edo_1_3_Q,
      o_DBus(2) => edo_1_2_Q,
      o_DBus(1) => edo_1_1_Q,
      o_DBus(0) => edo_1_0_Q,
      i_ilevel(1) => Mtrien_io_altera_dsp_mux0000_norst,
      i_ilevel(0) => Mtrien_io_altera_dsp_mux0000_norst,
      i_ivector(4) => ea(15),
      i_ivector(3) => ea(15),
      i_ivector(2) => ea(15),
      i_ivector(1) => ea(15),
      i_ivector(0) => ea(15),
      i_be(3) => inst_emif_iface_be_r(3),
      i_be(2) => inst_emif_iface_be_r(2),
      i_be(1) => inst_emif_iface_be_r(1),
      i_be(0) => inst_emif_iface_be_r(0),
      i_DBus(31) => inst_emif_iface_edi_r(31),
      i_DBus(30) => inst_emif_iface_edi_r(30),
      i_DBus(29) => inst_emif_iface_edi_r(29),
      i_DBus(28) => inst_emif_iface_edi_r(28),
      i_DBus(27) => inst_emif_iface_edi_r(27),
      i_DBus(26) => inst_emif_iface_edi_r(26),
      i_DBus(25) => inst_emif_iface_edi_r(25),
      i_DBus(24) => inst_emif_iface_edi_r(24),
      i_DBus(23) => inst_emif_iface_edi_r(23),
      i_DBus(22) => inst_emif_iface_edi_r(22),
      i_DBus(21) => inst_emif_iface_edi_r(21),
      i_DBus(20) => inst_emif_iface_edi_r(20),
      i_DBus(19) => inst_emif_iface_edi_r(19),
      i_DBus(18) => inst_emif_iface_edi_r(18),
      i_DBus(17) => inst_emif_iface_edi_r(17),
      i_DBus(16) => inst_emif_iface_edi_r(16),
      i_DBus(15) => inst_emif_iface_edi_r(15),
      i_DBus(14) => inst_emif_iface_edi_r(14),
      i_DBus(13) => inst_emif_iface_edi_r(13),
      i_DBus(12) => inst_emif_iface_edi_r(12),
      i_DBus(11) => inst_emif_iface_edi_r(11),
      i_DBus(10) => inst_emif_iface_edi_r(10),
      i_DBus(9) => inst_emif_iface_edi_r(9),
      i_DBus(8) => inst_emif_iface_edi_r(8),
      i_DBus(7) => inst_emif_iface_edi_r(7),
      i_DBus(6) => inst_emif_iface_edi_r(6),
      i_DBus(5) => inst_emif_iface_edi_r(5),
      i_DBus(4) => inst_emif_iface_edi_r(4),
      i_DBus(3) => inst_emif_iface_edi_r(3),
      i_DBus(2) => inst_emif_iface_edi_r(2),
      i_DBus(1) => inst_emif_iface_edi_r(1),
      i_DBus(0) => inst_emif_iface_edi_r(0),
      i_ABus(4) => inst_emif_iface_addr_r(4),
      i_ABus(3) => inst_emif_iface_addr_r(3),
      i_ABus(2) => inst_emif_iface_addr_r(2),
      i_ABus(1) => inst_emif_iface_addr_r(1),
      i_ABus(0) => inst_emif_iface_addr_r(0)
    );
  emac_0 : cl_eth_671x
    port map (
      i_col => i_eth_col_IBUF_502,
      rd_en => inst_emif_iface_rd_r_1175,
      i_crs => i_eth_crs_IBUF_504,
      int_rxpkt => irq_map_5_0_Q,
      i_rx_dv => i_eth_rx_dv_IBUF_518,
      i_rx_er => i_eth_rx_er_IBUF_520,
      rx_clk => rx_clk,
      wr_en => inst_emif_iface_wr_r_1474,
      eth_cs => inst_emif_iface_cs_r_3_Q,
      o_clk2_5 => NLW_emac_o_clk2_5_UNCONNECTED,
      o_mdc => o_eth_mdc_OBUF_3047,
      o_tx_en => o_eth_tx_en_OBUF_3062,
      o_tx_er => NLW_emac_o_tx_er_UNCONNECTED,
      clk25 => o_eth_ref_clk_OBUF_3050,
      o_tx_spd_100 => NLW_emac_o_tx_spd_100_UNCONNECTED,
      o_ref_clk => NLW_emac_o_ref_clk_UNCONNECTED,
      o_eth_rst => phy_rst,
      i_mdio => N369,
      i_half_duplex => ea(15),
      tx_clk => tx_clk,
      t_mdio => io_eth_mdio_o_eth_mdio_not0000_inv,
      i_ivector_vld => Mtrien_io_altera_dsp_mux0000_norst,
      emif_clk => emif_clk,
      o_mdio => o_eth_mdio,
      i_ilevel_vld => Mtrien_io_altera_dsp_mux0000_norst,
      o_rx_en => NLW_emac_o_rx_en_UNCONNECTED,
      oDBus(31) => edo_3_31_Q,
      oDBus(30) => edo_3_30_Q,
      oDBus(29) => edo_3_29_Q,
      oDBus(28) => edo_3_28_Q,
      oDBus(27) => edo_3_27_Q,
      oDBus(26) => edo_3_26_Q,
      oDBus(25) => edo_3_25_Q,
      oDBus(24) => edo_3_24_Q,
      oDBus(23) => edo_3_23_Q,
      oDBus(22) => edo_3_22_Q,
      oDBus(21) => edo_3_21_Q,
      oDBus(20) => edo_3_20_Q,
      oDBus(19) => edo_3_19_Q,
      oDBus(18) => edo_3_18_Q,
      oDBus(17) => edo_3_17_Q,
      oDBus(16) => edo_3_16_Q,
      oDBus(15) => edo_3_15_Q,
      oDBus(14) => edo_3_14_Q,
      oDBus(13) => edo_3_13_Q,
      oDBus(12) => edo_3_12_Q,
      oDBus(11) => edo_3_11_Q,
      oDBus(10) => edo_3_10_Q,
      oDBus(9) => edo_3_9_Q,
      oDBus(8) => edo_3_8_Q,
      oDBus(7) => edo_3_7_Q,
      oDBus(6) => edo_3_6_Q,
      oDBus(5) => edo_3_5_Q,
      oDBus(4) => edo_3_4_Q,
      oDBus(3) => edo_3_3_Q,
      oDBus(2) => edo_3_2_Q,
      oDBus(1) => edo_3_1_Q,
      oDBus(0) => edo_3_0_Q,
      o_tx_data(3) => o_eth_tx_data_3_OBUF_3060,
      o_tx_data(2) => o_eth_tx_data_2_OBUF_3059,
      o_tx_data(1) => o_eth_tx_data_1_OBUF_3058,
      o_tx_data(0) => o_eth_tx_data_0_OBUF_3057,
      i_ilevel(1) => ea(15),
      i_ilevel(0) => Mtrien_io_altera_dsp_mux0000_norst,
      ABus(4) => inst_emif_iface_addr_r(4),
      ABus(3) => inst_emif_iface_addr_r(3),
      ABus(2) => inst_emif_iface_addr_r(2),
      ABus(1) => inst_emif_iface_addr_r(1),
      ABus(0) => inst_emif_iface_addr_r(0),
      i_ivector(4) => ea(15),
      i_ivector(3) => ea(15),
      i_ivector(2) => ea(15),
      i_ivector(1) => ea(15),
      i_ivector(0) => ea(15),
      iDBus(31) => inst_emif_iface_edi_r(31),
      iDBus(30) => inst_emif_iface_edi_r(30),
      iDBus(29) => inst_emif_iface_edi_r(29),
      iDBus(28) => inst_emif_iface_edi_r(28),
      iDBus(27) => inst_emif_iface_edi_r(27),
      iDBus(26) => inst_emif_iface_edi_r(26),
      iDBus(25) => inst_emif_iface_edi_r(25),
      iDBus(24) => inst_emif_iface_edi_r(24),
      iDBus(23) => inst_emif_iface_edi_r(23),
      iDBus(22) => inst_emif_iface_edi_r(22),
      iDBus(21) => inst_emif_iface_edi_r(21),
      iDBus(20) => inst_emif_iface_edi_r(20),
      iDBus(19) => inst_emif_iface_edi_r(19),
      iDBus(18) => inst_emif_iface_edi_r(18),
      iDBus(17) => inst_emif_iface_edi_r(17),
      iDBus(16) => inst_emif_iface_edi_r(16),
      iDBus(15) => inst_emif_iface_edi_r(15),
      iDBus(14) => inst_emif_iface_edi_r(14),
      iDBus(13) => inst_emif_iface_edi_r(13),
      iDBus(12) => inst_emif_iface_edi_r(12),
      iDBus(11) => inst_emif_iface_edi_r(11),
      iDBus(10) => inst_emif_iface_edi_r(10),
      iDBus(9) => inst_emif_iface_edi_r(9),
      iDBus(8) => inst_emif_iface_edi_r(8),
      iDBus(7) => inst_emif_iface_edi_r(7),
      iDBus(6) => inst_emif_iface_edi_r(6),
      iDBus(5) => inst_emif_iface_edi_r(5),
      iDBus(4) => inst_emif_iface_edi_r(4),
      iDBus(3) => inst_emif_iface_edi_r(3),
      iDBus(2) => inst_emif_iface_edi_r(2),
      iDBus(1) => inst_emif_iface_edi_r(1),
      iDBus(0) => inst_emif_iface_edi_r(0),
      i_rx_data(3) => i_eth_rx_data_3_IBUF_516,
      i_rx_data(2) => i_eth_rx_data_2_IBUF_515,
      i_rx_data(1) => i_eth_rx_data_1_IBUF_514,
      i_rx_data(0) => i_eth_rx_data_0_IBUF_513
    );
  inst_emif_iface_sdram_dma_std_dma_engine_holda_r_0 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => i_holda_n_IBUF_524,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_holda_r(0)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_holda_r_1 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_holda_r(0),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_holda_r(1)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_holda_r_2 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_holda_r(1),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_holda_r(2)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_holda_r_3 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_holda_r(2),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_holda_r(3)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ea_10 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ea_10_mux0001,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_ea(10)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_0 : FD
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001(8),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_row(0)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ea_2 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ea_2_mux0001,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_ea(2)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ea_11 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ea_11_mux0001,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_ea(11)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ea_12 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ea_12_mux0001,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_ea(12)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_awe_n : FD
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_awe_n_mux0000,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_awe_n_1214
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ea_3 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ea_3_mux0001,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_ea(3)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_cas_n : FD
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_cas_n_mux0000,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_cas_n_1225
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ea_13 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ea_13_mux0001,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_ea(13)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ea_4 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ea_4_mux0001,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_ea(4)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ea_14 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ea_14_mux0001,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_ea(14)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ea_5 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ea_5_mux0001,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_ea(5)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ea_7 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ea_7_mux0001,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_ea(7)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ea_6 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ea_6_mux0001,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_ea(6)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ea_8 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ea_8_mux0001,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_ea(8)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ea_9 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ea_9_mux0001,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_ea(9)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_mux0002_1303,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_1302
    );
  inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt_0 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt_mux0000(1),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt(0)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt_1 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt_mux0000(0),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt(1)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ras_n : FD
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ras_n_mux0000,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_ras_n_1350
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_0 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000(8),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_word_count(0)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_1 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000(7),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_word_count(1)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_2 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000(6),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_word_count(2)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_3 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000(5),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_word_count(3)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_4 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000(4),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_word_count(4)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_5 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000(3),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_word_count(5)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_6 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000(2),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_word_count(6)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_count_0 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => mb_altera_core_s_dma_count(0),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_count(0)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_count_1 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => mb_altera_core_s_dma_count(1),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_count(1)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_count_2 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => mb_altera_core_s_dma_count(2),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_count(2)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_count_3 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => mb_altera_core_s_dma_count(3),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_count(3)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_count_4 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => mb_altera_core_s_dma_count(4),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_count(4)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_count_5 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => mb_altera_core_s_dma_count(5),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_count(5)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_count_6 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => mb_altera_core_s_dma_count(6),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_count(6)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_count_7 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => mb_altera_core_s_dma_count(7),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_count(7)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_count_8 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => mb_altera_core_s_dma_count(8),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_count(8)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr_0 : FDE
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_addr(0, 0),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(0)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr_1 : FDE
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_addr(0, 1),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(1)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr_2 : FDE
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_addr(0, 2),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(2)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr_3 : FDE
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_addr(0, 3),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(3)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr_4 : FDE
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_addr(0, 4),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(4)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr_5 : FDE
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_addr(0, 5),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(5)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr_6 : FDE
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_addr(0, 6),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(6)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr_7 : FDE
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_addr(0, 7),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(7)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr_8 : FDE
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_addr(0, 8),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(8)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr_9 : FDE
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_addr(0, 9),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(9)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr_10 : FDE
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_addr(0, 10),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(10)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr_11 : FDE
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_addr(0, 11),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(11)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr_12 : FDE
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_addr(0, 12),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(12)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr_13 : FDE
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_addr(0, 13),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(13)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr_14 : FDE
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_addr(0, 14),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(14)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr_15 : FDE
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_addr(0, 15),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(15)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr_16 : FDE
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_addr(0, 16),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(16)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr_17 : FDE
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_addr(0, 17),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(17)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr_18 : FDE
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_addr(0, 18),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(18)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr_19 : FDE
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_addr(0, 19),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(19)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr_20 : FDE
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_addr(0, 20),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(20)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_0 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 0),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(0)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_1 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 1),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(1)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_2 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 2),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(2)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_3 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 3),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(3)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_4 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 4),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(4)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_5 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 5),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(5)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_6 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 6),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(6)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_7 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 7),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(7)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_8 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 8),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(8)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_9 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 9),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(9)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_10 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 10),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(10)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_11 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 11),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(11)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_12 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 12),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(12)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_13 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 13),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(13)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_14 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 14),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(14)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_15 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 15),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(15)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_16 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 16),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(16)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_17 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 17),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(17)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_18 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 18),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(18)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_19 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 19),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(19)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_20 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 20),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(20)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_21 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 21),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(21)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_22 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 22),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(22)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_23 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 23),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(23)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_24 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 24),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(24)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_25 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 25),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(25)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_26 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 26),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(26)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_27 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 27),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(27)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_28 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 28),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(28)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_29 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 29),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(29)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_30 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 30),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(30)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_din_31 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => Mtrien_io_altera_dsp_mux0000_norst,
      D => dma_data(0, 31),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(31)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_holdn_r_0 : FD
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_hold_n_1340,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_holdn_r(0)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_holdn_r_1 : FD
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_holdn_r(0),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_holdn_r(1)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_holdn_r_2 : FD
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_holdn_r(1),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_holdn_r(2)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_holdn_r_3 : FD
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_holdn_r(2),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_holdn_r(3)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_1302,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_we : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_cmp_eq0004,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_we_1306
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_we_r_0 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_dma_we_1306,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_we_r(0)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_we_r_1 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_dma_we_r(0),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_we_r(1)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_we_r_2 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_dma_we_r(1),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_we_r(2)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_we_r_3 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_dma_we_r(2),
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_we_r(3)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_lut_0_Q : LUT4
    generic map(
      INIT => X"8421"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_count(1),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_count(0),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(1),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(0),
      O => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_lut(0)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy_0_Q : MUXCY
    port map (
      CI => ea(15),
      DI => Mtrien_io_altera_dsp_mux0000_norst,
      S => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_lut(0),
      O => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(0)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_lut_1_Q : LUT4
    generic map(
      INIT => X"8241"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_count(3),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_count(2),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(2),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(3),
      O => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_lut(1)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy_1_Q : MUXCY
    port map (
      CI => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(0),
      DI => Mtrien_io_altera_dsp_mux0000_norst,
      S => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_lut(1),
      O => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(1)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_lut_2_Q : LUT4
    generic map(
      INIT => X"8421"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_count(5),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_count(4),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(5),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(4),
      O => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_lut(2)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy_2_Q : MUXCY
    port map (
      CI => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(1),
      DI => Mtrien_io_altera_dsp_mux0000_norst,
      S => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_lut(2),
      O => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(2)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_lut_3_Q : LUT4
    generic map(
      INIT => X"8421"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_count(7),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_count(6),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(7),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(6),
      O => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_lut(3)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy_3_Q : MUXCY
    port map (
      CI => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(2),
      DI => Mtrien_io_altera_dsp_mux0000_norst,
      S => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_lut(3),
      O => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(3)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_lut_4_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_count(8),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(8),
      O => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_lut(4)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy_4_Q : MUXCY
    port map (
      CI => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(3),
      DI => Mtrien_io_altera_dsp_mux0000_norst,
      S => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_lut(4),
      O => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(4)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_In_1423,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_In,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sm_arb_FSM_FFd3 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_sm_arb_FSM_FFd3_In_1421,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_sm_arb_FSM_FFd3_1420
    );
  inst_emif_iface_rst_on_cfg : ROC
    port map (
      O => inst_emif_iface_reset
    );
  inst_emif_iface_o_awe_n : FD
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_awe_n_1214,
      Q => inst_emif_iface_o_awe_n_1153
    );
  inst_emif_iface_o_cas_n : FD
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_cas_n_1225,
      Q => inst_emif_iface_o_cas_n_1155
    );
  inst_emif_iface_t_emif : FD
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_emif_t_1339,
      Q => inst_emif_iface_t_emif_1472
    );
  inst_emif_iface_dma_t_ed : FD
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ed_t_1337,
      Q => inst_emif_iface_dma_t_ed_1014
    );
  inst_emif_iface_o_ras_n : FD
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ras_n_1350,
      Q => inst_emif_iface_o_ras_n_1173
    );
  inst_emif_iface_o_be_n_3 : FD
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_be_n(0),
      Q => inst_emif_iface_o_be_n(3)
    );
  inst_emif_iface_o_ea_14 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ea(14),
      Q => inst_emif_iface_o_ea(14)
    );
  inst_emif_iface_o_ea_13 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ea(13),
      Q => inst_emif_iface_o_ea(13)
    );
  inst_emif_iface_o_ea_12 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ea(12),
      Q => inst_emif_iface_o_ea(12)
    );
  inst_emif_iface_o_ea_11 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ea(11),
      Q => inst_emif_iface_o_ea(11)
    );
  inst_emif_iface_o_ea_10 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ea(10),
      Q => inst_emif_iface_o_ea(10)
    );
  inst_emif_iface_o_ea_9 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ea(9),
      Q => inst_emif_iface_o_ea(9)
    );
  inst_emif_iface_o_ea_8 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ea(8),
      Q => inst_emif_iface_o_ea(8)
    );
  inst_emif_iface_o_ea_7 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ea(7),
      Q => inst_emif_iface_o_ea(7)
    );
  inst_emif_iface_o_ea_6 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ea(6),
      Q => inst_emif_iface_o_ea(6)
    );
  inst_emif_iface_o_ea_5 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ea(5),
      Q => inst_emif_iface_o_ea(5)
    );
  inst_emif_iface_o_ea_4 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ea(4),
      Q => inst_emif_iface_o_ea(4)
    );
  inst_emif_iface_o_ea_3 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ea(3),
      Q => inst_emif_iface_o_ea(3)
    );
  inst_emif_iface_o_ea_2 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ea(2),
      Q => inst_emif_iface_o_ea(2)
    );
  inst_emif_iface_o_ce_n_3 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => Mtrien_io_altera_dsp_mux0000_norst,
      Q => inst_emif_iface_o_ce_n_3_Q
    );
  inst_emif_iface_wr_r : FDC
    port map (
      C => emif_clk,
      CLR => inst_emif_iface_reset,
      D => inst_emif_iface_wr,
      Q => inst_emif_iface_wr_r_1474
    );
  inst_emif_iface_rd_r : FDC
    port map (
      C => emif_clk,
      CLR => inst_emif_iface_reset,
      D => inst_emif_iface_rd,
      Q => inst_emif_iface_rd_r_1175
    );
  inst_emif_iface_are_dly : FD
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_are_n_994,
      Q => inst_emif_iface_are_dly_993
    );
  inst_emif_iface_awe_dly : FD
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_awe_n_996,
      Q => inst_emif_iface_awe_dly_995
    );
  inst_emif_iface_be_r_3 : FDP
    port map (
      C => emif_clk,
      D => inst_emif_iface_be(3),
      PRE => inst_emif_iface_reset,
      Q => inst_emif_iface_be_r(3)
    );
  inst_emif_iface_be_r_2 : FDP
    port map (
      C => emif_clk,
      D => inst_emif_iface_be(2),
      PRE => inst_emif_iface_reset,
      Q => inst_emif_iface_be_r(2)
    );
  inst_emif_iface_be_r_1 : FDP
    port map (
      C => emif_clk,
      D => inst_emif_iface_be(1),
      PRE => inst_emif_iface_reset,
      Q => inst_emif_iface_be_r(1)
    );
  inst_emif_iface_be_r_0 : FDP
    port map (
      C => emif_clk,
      D => inst_emif_iface_be(0),
      PRE => inst_emif_iface_reset,
      Q => inst_emif_iface_be_r(0)
    );
  inst_emif_iface_edi_r_31 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(31),
      Q => inst_emif_iface_edi_r(31)
    );
  inst_emif_iface_edi_r_30 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(30),
      Q => inst_emif_iface_edi_r(30)
    );
  inst_emif_iface_edi_r_29 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(29),
      Q => inst_emif_iface_edi_r(29)
    );
  inst_emif_iface_edi_r_28 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(28),
      Q => inst_emif_iface_edi_r(28)
    );
  inst_emif_iface_edi_r_27 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(27),
      Q => inst_emif_iface_edi_r(27)
    );
  inst_emif_iface_edi_r_26 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(26),
      Q => inst_emif_iface_edi_r(26)
    );
  inst_emif_iface_edi_r_25 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(25),
      Q => inst_emif_iface_edi_r(25)
    );
  inst_emif_iface_edi_r_24 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(24),
      Q => inst_emif_iface_edi_r(24)
    );
  inst_emif_iface_edi_r_23 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(23),
      Q => inst_emif_iface_edi_r(23)
    );
  inst_emif_iface_edi_r_22 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(22),
      Q => inst_emif_iface_edi_r(22)
    );
  inst_emif_iface_edi_r_21 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(21),
      Q => inst_emif_iface_edi_r(21)
    );
  inst_emif_iface_edi_r_20 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(20),
      Q => inst_emif_iface_edi_r(20)
    );
  inst_emif_iface_edi_r_19 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(19),
      Q => inst_emif_iface_edi_r(19)
    );
  inst_emif_iface_edi_r_18 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(18),
      Q => inst_emif_iface_edi_r(18)
    );
  inst_emif_iface_edi_r_17 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(17),
      Q => inst_emif_iface_edi_r(17)
    );
  inst_emif_iface_edi_r_16 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(16),
      Q => inst_emif_iface_edi_r(16)
    );
  inst_emif_iface_edi_r_15 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(15),
      Q => inst_emif_iface_edi_r(15)
    );
  inst_emif_iface_edi_r_14 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(14),
      Q => inst_emif_iface_edi_r(14)
    );
  inst_emif_iface_edi_r_13 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(13),
      Q => inst_emif_iface_edi_r(13)
    );
  inst_emif_iface_edi_r_12 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(12),
      Q => inst_emif_iface_edi_r(12)
    );
  inst_emif_iface_edi_r_11 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(11),
      Q => inst_emif_iface_edi_r(11)
    );
  inst_emif_iface_edi_r_10 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(10),
      Q => inst_emif_iface_edi_r(10)
    );
  inst_emif_iface_edi_r_9 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(9),
      Q => inst_emif_iface_edi_r(9)
    );
  inst_emif_iface_edi_r_8 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(8),
      Q => inst_emif_iface_edi_r(8)
    );
  inst_emif_iface_edi_r_7 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(7),
      Q => inst_emif_iface_edi_r(7)
    );
  inst_emif_iface_edi_r_6 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(6),
      Q => inst_emif_iface_edi_r(6)
    );
  inst_emif_iface_edi_r_5 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(5),
      Q => inst_emif_iface_edi_r(5)
    );
  inst_emif_iface_edi_r_4 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(4),
      Q => inst_emif_iface_edi_r(4)
    );
  inst_emif_iface_edi_r_3 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(3),
      Q => inst_emif_iface_edi_r(3)
    );
  inst_emif_iface_edi_r_2 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(2),
      Q => inst_emif_iface_edi_r(2)
    );
  inst_emif_iface_edi_r_1 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(1),
      Q => inst_emif_iface_edi_r(1)
    );
  inst_emif_iface_edi_r_0 : FDE
    port map (
      C => emif_clk,
      CE => inst_emif_iface_reset_inv,
      D => inst_emif_iface_edi(0),
      Q => inst_emif_iface_edi_r(0)
    );
  inst_emif_iface_cs_r_8 : FDC
    port map (
      C => emif_clk,
      CLR => inst_emif_iface_reset,
      D => inst_emif_iface_cs_8_Q,
      Q => inst_emif_iface_cs_r_8_Q
    );
  inst_emif_iface_cs_r_3 : FDC
    port map (
      C => emif_clk,
      CLR => inst_emif_iface_reset,
      D => inst_emif_iface_cs_3_Q,
      Q => inst_emif_iface_cs_r_3_Q
    );
  inst_emif_iface_cs_r_1 : FDC
    port map (
      C => emif_clk,
      CLR => inst_emif_iface_reset,
      D => inst_emif_iface_cs_1_Q,
      Q => inst_emif_iface_cs_r_1_Q
    );
  inst_emif_iface_cs_r_0 : FDC
    port map (
      C => emif_clk,
      CLR => inst_emif_iface_reset,
      D => inst_emif_iface_cs_0_Q,
      Q => inst_emif_iface_cs_r_0_Q
    );
  inst_emif_iface_addr_r_4 : FDC
    port map (
      C => emif_clk,
      CLR => inst_emif_iface_reset,
      D => inst_emif_iface_ea(6),
      Q => inst_emif_iface_addr_r(4)
    );
  inst_emif_iface_addr_r_3 : FDC
    port map (
      C => emif_clk,
      CLR => inst_emif_iface_reset,
      D => inst_emif_iface_ea(5),
      Q => inst_emif_iface_addr_r(3)
    );
  inst_emif_iface_addr_r_2 : FDC
    port map (
      C => emif_clk,
      CLR => inst_emif_iface_reset,
      D => inst_emif_iface_ea(4),
      Q => inst_emif_iface_addr_r(2)
    );
  inst_emif_iface_addr_r_1 : FDC
    port map (
      C => emif_clk,
      CLR => inst_emif_iface_reset,
      D => inst_emif_iface_ea(3),
      Q => inst_emif_iface_addr_r(1)
    );
  inst_emif_iface_addr_r_0 : FDC
    port map (
      C => emif_clk,
      CLR => inst_emif_iface_reset,
      D => inst_emif_iface_ea(2),
      Q => inst_emif_iface_addr_r(0)
    );
  inst_emif_iface_ce3_n : FD
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      D => N366,
      Q => inst_emif_iface_ce3_n_1005
    );
  inst_emif_iface_awe_n : FD
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      D => N368,
      Q => inst_emif_iface_awe_n_996
    );
  inst_emif_iface_edi_31 : FD
    port map (
      C => emif_clk,
      D => N370,
      Q => inst_emif_iface_edi(31)
    );
  inst_emif_iface_edi_30 : FD
    port map (
      C => emif_clk,
      D => N371,
      Q => inst_emif_iface_edi(30)
    );
  inst_emif_iface_edi_29 : FD
    port map (
      C => emif_clk,
      D => N372,
      Q => inst_emif_iface_edi(29)
    );
  inst_emif_iface_edi_28 : FD
    port map (
      C => emif_clk,
      D => N373,
      Q => inst_emif_iface_edi(28)
    );
  inst_emif_iface_edi_27 : FD
    port map (
      C => emif_clk,
      D => N374,
      Q => inst_emif_iface_edi(27)
    );
  inst_emif_iface_edi_26 : FD
    port map (
      C => emif_clk,
      D => N375,
      Q => inst_emif_iface_edi(26)
    );
  inst_emif_iface_edi_25 : FD
    port map (
      C => emif_clk,
      D => N376,
      Q => inst_emif_iface_edi(25)
    );
  inst_emif_iface_edi_24 : FD
    port map (
      C => emif_clk,
      D => N377,
      Q => inst_emif_iface_edi(24)
    );
  inst_emif_iface_edi_23 : FD
    port map (
      C => emif_clk,
      D => N378,
      Q => inst_emif_iface_edi(23)
    );
  inst_emif_iface_edi_22 : FD
    port map (
      C => emif_clk,
      D => N379,
      Q => inst_emif_iface_edi(22)
    );
  inst_emif_iface_edi_21 : FD
    port map (
      C => emif_clk,
      D => N380,
      Q => inst_emif_iface_edi(21)
    );
  inst_emif_iface_edi_20 : FD
    port map (
      C => emif_clk,
      D => N381,
      Q => inst_emif_iface_edi(20)
    );
  inst_emif_iface_edi_19 : FD
    port map (
      C => emif_clk,
      D => N382,
      Q => inst_emif_iface_edi(19)
    );
  inst_emif_iface_edi_18 : FD
    port map (
      C => emif_clk,
      D => N383,
      Q => inst_emif_iface_edi(18)
    );
  inst_emif_iface_edi_17 : FD
    port map (
      C => emif_clk,
      D => N384,
      Q => inst_emif_iface_edi(17)
    );
  inst_emif_iface_edi_16 : FD
    port map (
      C => emif_clk,
      D => N385,
      Q => inst_emif_iface_edi(16)
    );
  inst_emif_iface_edi_15 : FD
    port map (
      C => emif_clk,
      D => N386,
      Q => inst_emif_iface_edi(15)
    );
  inst_emif_iface_edi_14 : FD
    port map (
      C => emif_clk,
      D => N387,
      Q => inst_emif_iface_edi(14)
    );
  inst_emif_iface_edi_13 : FD
    port map (
      C => emif_clk,
      D => N388,
      Q => inst_emif_iface_edi(13)
    );
  inst_emif_iface_edi_12 : FD
    port map (
      C => emif_clk,
      D => N389,
      Q => inst_emif_iface_edi(12)
    );
  inst_emif_iface_edi_11 : FD
    port map (
      C => emif_clk,
      D => N390,
      Q => inst_emif_iface_edi(11)
    );
  inst_emif_iface_edi_10 : FD
    port map (
      C => emif_clk,
      D => N391,
      Q => inst_emif_iface_edi(10)
    );
  inst_emif_iface_edi_9 : FD
    port map (
      C => emif_clk,
      D => N392,
      Q => inst_emif_iface_edi(9)
    );
  inst_emif_iface_edi_8 : FD
    port map (
      C => emif_clk,
      D => N393,
      Q => inst_emif_iface_edi(8)
    );
  inst_emif_iface_edi_7 : FD
    port map (
      C => emif_clk,
      D => N394,
      Q => inst_emif_iface_edi(7)
    );
  inst_emif_iface_edi_6 : FD
    port map (
      C => emif_clk,
      D => N395,
      Q => inst_emif_iface_edi(6)
    );
  inst_emif_iface_edi_5 : FD
    port map (
      C => emif_clk,
      D => N396,
      Q => inst_emif_iface_edi(5)
    );
  inst_emif_iface_edi_4 : FD
    port map (
      C => emif_clk,
      D => N397,
      Q => inst_emif_iface_edi(4)
    );
  inst_emif_iface_edi_3 : FD
    port map (
      C => emif_clk,
      D => N398,
      Q => inst_emif_iface_edi(3)
    );
  inst_emif_iface_edi_2 : FD
    port map (
      C => emif_clk,
      D => N399,
      Q => inst_emif_iface_edi(2)
    );
  inst_emif_iface_edi_1 : FD
    port map (
      C => emif_clk,
      D => N400,
      Q => inst_emif_iface_edi(1)
    );
  inst_emif_iface_edi_0 : FD
    port map (
      C => emif_clk,
      D => N401,
      Q => inst_emif_iface_edi(0)
    );
  inst_emif_iface_are_n : FD
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      D => N365,
      Q => inst_emif_iface_are_n_994
    );
  inst_emif_iface_ea_11 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => N439,
      Q => inst_emif_iface_ea(11)
    );
  inst_emif_iface_ea_10 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => N440,
      Q => inst_emif_iface_ea(10)
    );
  inst_emif_iface_ea_9 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => N441,
      Q => inst_emif_iface_ea(9)
    );
  inst_emif_iface_ea_8 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => N442,
      Q => inst_emif_iface_ea(8)
    );
  inst_emif_iface_ea_7 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => N443,
      Q => inst_emif_iface_ea(7)
    );
  inst_emif_iface_ea_6 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => N444,
      Q => inst_emif_iface_ea(6)
    );
  inst_emif_iface_ea_5 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => N445,
      Q => inst_emif_iface_ea(5)
    );
  inst_emif_iface_ea_4 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => N446,
      Q => inst_emif_iface_ea(4)
    );
  inst_emif_iface_ea_3 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => N447,
      Q => inst_emif_iface_ea(3)
    );
  inst_emif_iface_ea_2 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => N448,
      Q => inst_emif_iface_ea(2)
    );
  inst_emif_iface_be_3 : FD
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      D => N402,
      Q => inst_emif_iface_be(3)
    );
  inst_emif_iface_be_2 : FD
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      D => N403,
      Q => inst_emif_iface_be(2)
    );
  inst_emif_iface_be_1 : FD
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      D => N404,
      Q => inst_emif_iface_be(1)
    );
  inst_emif_iface_be_0 : FD
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      D => N405,
      Q => inst_emif_iface_be(0)
    );
  inst_base_module_roc_inst : ROC
    port map (
      O => inst_base_module_reset
    );
  inst_base_module_Msub_wd_counter_addsub0000_xor_15_Q : XORCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(14),
      LI => inst_base_module_Msub_wd_counter_addsub0000_lut(15),
      O => inst_base_module_wd_counter_addsub0000(15)
    );
  inst_base_module_Msub_wd_counter_addsub0000_xor_14_Q : XORCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(13),
      LI => inst_base_module_Msub_wd_counter_addsub0000_lut(14),
      O => inst_base_module_wd_counter_addsub0000(14)
    );
  inst_base_module_Msub_wd_counter_addsub0000_cy_14_Q : MUXCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(13),
      DI => Mtrien_io_altera_dsp_mux0000_norst,
      S => inst_base_module_Msub_wd_counter_addsub0000_lut(14),
      O => inst_base_module_Msub_wd_counter_addsub0000_cy(14)
    );
  inst_base_module_Msub_wd_counter_addsub0000_xor_13_Q : XORCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(12),
      LI => inst_base_module_Msub_wd_counter_addsub0000_lut(13),
      O => inst_base_module_wd_counter_addsub0000(13)
    );
  inst_base_module_Msub_wd_counter_addsub0000_cy_13_Q : MUXCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(12),
      DI => Mtrien_io_altera_dsp_mux0000_norst,
      S => inst_base_module_Msub_wd_counter_addsub0000_lut(13),
      O => inst_base_module_Msub_wd_counter_addsub0000_cy(13)
    );
  inst_base_module_Msub_wd_counter_addsub0000_xor_12_Q : XORCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(11),
      LI => inst_base_module_Msub_wd_counter_addsub0000_lut(12),
      O => inst_base_module_wd_counter_addsub0000(12)
    );
  inst_base_module_Msub_wd_counter_addsub0000_cy_12_Q : MUXCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(11),
      DI => Mtrien_io_altera_dsp_mux0000_norst,
      S => inst_base_module_Msub_wd_counter_addsub0000_lut(12),
      O => inst_base_module_Msub_wd_counter_addsub0000_cy(12)
    );
  inst_base_module_Msub_wd_counter_addsub0000_xor_11_Q : XORCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(10),
      LI => inst_base_module_Msub_wd_counter_addsub0000_lut(11),
      O => inst_base_module_wd_counter_addsub0000(11)
    );
  inst_base_module_Msub_wd_counter_addsub0000_cy_11_Q : MUXCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(10),
      DI => Mtrien_io_altera_dsp_mux0000_norst,
      S => inst_base_module_Msub_wd_counter_addsub0000_lut(11),
      O => inst_base_module_Msub_wd_counter_addsub0000_cy(11)
    );
  inst_base_module_Msub_wd_counter_addsub0000_xor_10_Q : XORCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(9),
      LI => inst_base_module_Msub_wd_counter_addsub0000_lut(10),
      O => inst_base_module_wd_counter_addsub0000(10)
    );
  inst_base_module_Msub_wd_counter_addsub0000_cy_10_Q : MUXCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(9),
      DI => Mtrien_io_altera_dsp_mux0000_norst,
      S => inst_base_module_Msub_wd_counter_addsub0000_lut(10),
      O => inst_base_module_Msub_wd_counter_addsub0000_cy(10)
    );
  inst_base_module_Msub_wd_counter_addsub0000_xor_9_Q : XORCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(8),
      LI => inst_base_module_Msub_wd_counter_addsub0000_lut(9),
      O => inst_base_module_wd_counter_addsub0000(9)
    );
  inst_base_module_Msub_wd_counter_addsub0000_cy_9_Q : MUXCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(8),
      DI => Mtrien_io_altera_dsp_mux0000_norst,
      S => inst_base_module_Msub_wd_counter_addsub0000_lut(9),
      O => inst_base_module_Msub_wd_counter_addsub0000_cy(9)
    );
  inst_base_module_Msub_wd_counter_addsub0000_xor_8_Q : XORCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(7),
      LI => inst_base_module_Msub_wd_counter_addsub0000_lut(8),
      O => inst_base_module_wd_counter_addsub0000(8)
    );
  inst_base_module_Msub_wd_counter_addsub0000_cy_8_Q : MUXCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(7),
      DI => Mtrien_io_altera_dsp_mux0000_norst,
      S => inst_base_module_Msub_wd_counter_addsub0000_lut(8),
      O => inst_base_module_Msub_wd_counter_addsub0000_cy(8)
    );
  inst_base_module_Msub_wd_counter_addsub0000_xor_7_Q : XORCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(6),
      LI => inst_base_module_Msub_wd_counter_addsub0000_lut(7),
      O => inst_base_module_wd_counter_addsub0000(7)
    );
  inst_base_module_Msub_wd_counter_addsub0000_cy_7_Q : MUXCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(6),
      DI => Mtrien_io_altera_dsp_mux0000_norst,
      S => inst_base_module_Msub_wd_counter_addsub0000_lut(7),
      O => inst_base_module_Msub_wd_counter_addsub0000_cy(7)
    );
  inst_base_module_Msub_wd_counter_addsub0000_xor_6_Q : XORCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(5),
      LI => inst_base_module_Msub_wd_counter_addsub0000_lut(6),
      O => inst_base_module_wd_counter_addsub0000(6)
    );
  inst_base_module_Msub_wd_counter_addsub0000_cy_6_Q : MUXCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(5),
      DI => Mtrien_io_altera_dsp_mux0000_norst,
      S => inst_base_module_Msub_wd_counter_addsub0000_lut(6),
      O => inst_base_module_Msub_wd_counter_addsub0000_cy(6)
    );
  inst_base_module_Msub_wd_counter_addsub0000_xor_5_Q : XORCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(4),
      LI => inst_base_module_Msub_wd_counter_addsub0000_lut(5),
      O => inst_base_module_wd_counter_addsub0000(5)
    );
  inst_base_module_Msub_wd_counter_addsub0000_cy_5_Q : MUXCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(4),
      DI => Mtrien_io_altera_dsp_mux0000_norst,
      S => inst_base_module_Msub_wd_counter_addsub0000_lut(5),
      O => inst_base_module_Msub_wd_counter_addsub0000_cy(5)
    );
  inst_base_module_Msub_wd_counter_addsub0000_xor_4_Q : XORCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(3),
      LI => inst_base_module_Msub_wd_counter_addsub0000_lut(4),
      O => inst_base_module_wd_counter_addsub0000(4)
    );
  inst_base_module_Msub_wd_counter_addsub0000_cy_4_Q : MUXCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(3),
      DI => Mtrien_io_altera_dsp_mux0000_norst,
      S => inst_base_module_Msub_wd_counter_addsub0000_lut(4),
      O => inst_base_module_Msub_wd_counter_addsub0000_cy(4)
    );
  inst_base_module_Msub_wd_counter_addsub0000_xor_3_Q : XORCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(2),
      LI => inst_base_module_Msub_wd_counter_addsub0000_lut(3),
      O => inst_base_module_wd_counter_addsub0000(3)
    );
  inst_base_module_Msub_wd_counter_addsub0000_cy_3_Q : MUXCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(2),
      DI => Mtrien_io_altera_dsp_mux0000_norst,
      S => inst_base_module_Msub_wd_counter_addsub0000_lut(3),
      O => inst_base_module_Msub_wd_counter_addsub0000_cy(3)
    );
  inst_base_module_Msub_wd_counter_addsub0000_xor_2_Q : XORCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(1),
      LI => inst_base_module_Msub_wd_counter_addsub0000_lut(2),
      O => inst_base_module_wd_counter_addsub0000(2)
    );
  inst_base_module_Msub_wd_counter_addsub0000_cy_2_Q : MUXCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(1),
      DI => Mtrien_io_altera_dsp_mux0000_norst,
      S => inst_base_module_Msub_wd_counter_addsub0000_lut(2),
      O => inst_base_module_Msub_wd_counter_addsub0000_cy(2)
    );
  inst_base_module_Msub_wd_counter_addsub0000_xor_1_Q : XORCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(0),
      LI => inst_base_module_Msub_wd_counter_addsub0000_lut(1),
      O => inst_base_module_wd_counter_addsub0000(1)
    );
  inst_base_module_Msub_wd_counter_addsub0000_cy_1_Q : MUXCY
    port map (
      CI => inst_base_module_Msub_wd_counter_addsub0000_cy(0),
      DI => Mtrien_io_altera_dsp_mux0000_norst,
      S => inst_base_module_Msub_wd_counter_addsub0000_lut(1),
      O => inst_base_module_Msub_wd_counter_addsub0000_cy(1)
    );
  inst_base_module_Msub_wd_counter_addsub0000_xor_0_Q : XORCY
    port map (
      CI => Mtrien_io_altera_dsp_mux0000_norst,
      LI => inst_base_module_Msub_wd_counter_addsub0000_cy_0_rt_536,
      O => inst_base_module_wd_counter_addsub0000(0)
    );
  inst_base_module_Msub_wd_counter_addsub0000_cy_0_Q : MUXCY
    port map (
      CI => Mtrien_io_altera_dsp_mux0000_norst,
      DI => ea(15),
      S => inst_base_module_Msub_wd_counter_addsub0000_cy_0_rt_536,
      O => inst_base_module_Msub_wd_counter_addsub0000_cy(0)
    );
  inst_base_module_Mram_scratch_ram32 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(31),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(31)
    );
  inst_base_module_Mram_scratch_ram31 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(30),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(30)
    );
  inst_base_module_Mram_scratch_ram30 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(29),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(29)
    );
  inst_base_module_Mram_scratch_ram29 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(28),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(28)
    );
  inst_base_module_Mram_scratch_ram28 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(27),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(27)
    );
  inst_base_module_Mram_scratch_ram27 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(26),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(26)
    );
  inst_base_module_Mram_scratch_ram26 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(25),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(25)
    );
  inst_base_module_Mram_scratch_ram25 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(24),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(24)
    );
  inst_base_module_Mram_scratch_ram24 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(23),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(23)
    );
  inst_base_module_Mram_scratch_ram23 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(22),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(22)
    );
  inst_base_module_Mram_scratch_ram22 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(21),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(21)
    );
  inst_base_module_Mram_scratch_ram21 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(20),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(20)
    );
  inst_base_module_Mram_scratch_ram20 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(19),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(19)
    );
  inst_base_module_Mram_scratch_ram19 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(18),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(18)
    );
  inst_base_module_Mram_scratch_ram17 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(16),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(16)
    );
  inst_base_module_Mram_scratch_ram16 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(15),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(15)
    );
  inst_base_module_Mram_scratch_ram18 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(17),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(17)
    );
  inst_base_module_Mram_scratch_ram15 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(14),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(14)
    );
  inst_base_module_Mram_scratch_ram14 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(13),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(13)
    );
  inst_base_module_Mram_scratch_ram13 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(12),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(12)
    );
  inst_base_module_Mram_scratch_ram12 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(11),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(11)
    );
  inst_base_module_Mram_scratch_ram11 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(10),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(10)
    );
  inst_base_module_Mram_scratch_ram10 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(9),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(9)
    );
  inst_base_module_Mram_scratch_ram9 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(8),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(8)
    );
  inst_base_module_Mram_scratch_ram8 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(7),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(7)
    );
  inst_base_module_Mram_scratch_ram7 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(6),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(6)
    );
  inst_base_module_Mram_scratch_ram6 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(5),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(5)
    );
  inst_base_module_Mram_scratch_ram5 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(4),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(4)
    );
  inst_base_module_Mram_scratch_ram4 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(3),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(3)
    );
  inst_base_module_Mram_scratch_ram3 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(2),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(2)
    );
  inst_base_module_Mram_scratch_ram2 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(1),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(1)
    );
  inst_base_module_Mram_scratch_ram1 : RAM16X1S
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => inst_emif_iface_addr_r(0),
      A1 => inst_emif_iface_addr_r(1),
      A2 => inst_emif_iface_addr_r(2),
      A3 => inst_emif_iface_addr_r(3),
      D => inst_emif_iface_edi_r(0),
      WCLK => emif_clk,
      WE => inst_base_module_i_ABus_4_0,
      O => inst_base_module_varindex0000(0)
    );
  inst_base_module_key_state_2 : FDCE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_key_state_and0001,
      CLR => inst_base_module_reset,
      D => inst_base_module_key_state_mux0003(0),
      Q => inst_base_module_key_state(2)
    );
  inst_base_module_key_state_0 : FDPE
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_key_state_and0001,
      D => inst_base_module_key_state_mux0003(2),
      PRE => inst_base_module_reset,
      Q => inst_base_module_key_state(0)
    );
  inst_base_module_key_state_1 : FDCE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_key_state_and0001,
      CLR => inst_base_module_reset,
      D => inst_base_module_key_state_mux0003(1),
      Q => inst_base_module_key_state(1)
    );
  inst_base_module_wd_rst_r2 : FDE
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000_inv,
      D => inst_base_module_wd_rst_r1_967,
      Q => inst_base_module_wd_rst_r2_968
    );
  inst_base_module_wd_nmi_clr_r2 : FD
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      D => inst_base_module_wd_nmi_clr_r1_943,
      Q => inst_base_module_wd_nmi_clr_r2_944
    );
  inst_base_module_wd_rst_r1 : FDE
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000_inv,
      D => inst_base_module_wd_rst_r_966,
      Q => inst_base_module_wd_rst_r1_967
    );
  inst_base_module_wd_rst_clr_r2 : FD
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      D => inst_base_module_wd_rst_clr_r1_956,
      Q => inst_base_module_wd_rst_clr_r2_957
    );
  inst_base_module_wd_kick_r2 : FD
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      D => inst_base_module_wd_kick_r1_937,
      Q => inst_base_module_wd_kick_r2_938
    );
  inst_base_module_dcm_lock_r2 : FD
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      D => inst_base_module_dcm_lock_r1_614,
      Q => inst_base_module_dcm_lock_r2_615
    );
  inst_base_module_wd_nmi_clr_r1 : FD
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      D => inst_base_module_wd_nmi_clr_940,
      Q => inst_base_module_wd_nmi_clr_r1_943
    );
  inst_base_module_div10_4_r : FD
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      D => inst_base_module_div10_4(10),
      Q => inst_base_module_div10_4_r_628
    );
  inst_base_module_wd_rst_clr_r1 : FD
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      D => inst_base_module_wd_rst_clr_953,
      Q => inst_base_module_wd_rst_clr_r1_956
    );
  inst_base_module_div10_3_r : FD
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      D => inst_base_module_div10_3(10),
      Q => inst_base_module_div10_3_r_625
    );
  inst_base_module_div10_2_r : FD
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      D => inst_base_module_div10_2(10),
      Q => inst_base_module_div10_2_r_622
    );
  inst_base_module_o_wd_rst : FDE
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      CE => inst_base_module_clken100ms_610,
      D => inst_base_module_o_wd_rst_and0000,
      Q => inst_base_module_o_wd_rst_878
    );
  inst_base_module_div5_2_r : FD
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      D => inst_base_module_div5_2(5),
      Q => inst_base_module_div5_2_r_637
    );
  inst_base_module_wd_kick_r1 : FD
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      D => inst_base_module_wd_kick_935,
      Q => inst_base_module_wd_kick_r1_937
    );
  inst_base_module_wd_rst_r : FDE
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      CE => inst_base_module_clken100ms_610,
      D => inst_base_module_wd_rst_952,
      Q => inst_base_module_wd_rst_r_966
    );
  inst_base_module_div10_5_r : FD
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      D => inst_base_module_div10_5(10),
      Q => inst_base_module_div10_5_r_631
    );
  inst_base_module_div5_1_r : FD
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      D => inst_base_module_div5_1(5),
      Q => inst_base_module_div5_1_r_634
    );
  inst_base_module_clken4ms : FDR
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      D => Mtrien_io_altera_dsp_mux0000_norst,
      R => inst_base_module_clken4ms_not0001,
      Q => inst_base_module_clken4ms_612
    );
  inst_base_module_dcm_lock_r1 : FD
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      D => dcm_lock,
      Q => inst_base_module_dcm_lock_r1_614
    );
  inst_base_module_wd_rst_en : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_wd_nmi_en_not0001,
      D => inst_base_module_wd_rst_en_mux0000,
      Q => inst_base_module_wd_rst_en_958
    );
  inst_base_module_wd_kick : FDE
    port map (
      C => emif_clk,
      CE => inst_base_module_wd_timeout_not0001,
      D => inst_base_module_wd_kick_not0001,
      Q => inst_base_module_wd_kick_935
    );
  inst_base_module_irq_enable_7 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_irq_enable_not0001,
      D => inst_emif_iface_edi_r(3),
      Q => inst_base_module_irq_enable(7)
    );
  inst_base_module_irq_enable_6 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_irq_enable_not0001,
      D => inst_emif_iface_edi_r(2),
      Q => inst_base_module_irq_enable(6)
    );
  inst_base_module_irq_enable_5 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_irq_enable_not0001,
      D => inst_emif_iface_edi_r(1),
      Q => inst_base_module_irq_enable(5)
    );
  inst_base_module_irq_enable_4 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_irq_enable_not0001,
      D => inst_emif_iface_edi_r(0),
      Q => inst_base_module_irq_enable(4)
    );
  inst_base_module_led_enable_0 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_led_enable_0_not0001,
      D => inst_emif_iface_edi_r(0),
      Q => inst_base_module_led_enable(0)
    );
  inst_base_module_bank_addr_0 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_bank_addr_0_and0000_607,
      D => inst_emif_iface_edi_r(0),
      Q => inst_base_module_bank_addr(0)
    );
  inst_base_module_wd_rst_clr : FDE
    port map (
      C => emif_clk,
      CE => inst_base_module_wd_rst_clr_not0002,
      D => inst_base_module_wd_rst_clr_not0003,
      Q => inst_base_module_wd_rst_clr_953
    );
  inst_base_module_wd_timeout_15 : FDE
    port map (
      C => emif_clk,
      CE => inst_base_module_wd_timeout_not0001,
      D => inst_emif_iface_edi_r(15),
      Q => inst_base_module_wd_timeout(15)
    );
  inst_base_module_wd_timeout_14 : FDE
    port map (
      C => emif_clk,
      CE => inst_base_module_wd_timeout_not0001,
      D => inst_emif_iface_edi_r(14),
      Q => inst_base_module_wd_timeout(14)
    );
  inst_base_module_wd_timeout_13 : FDE
    port map (
      C => emif_clk,
      CE => inst_base_module_wd_timeout_not0001,
      D => inst_emif_iface_edi_r(13),
      Q => inst_base_module_wd_timeout(13)
    );
  inst_base_module_wd_timeout_12 : FDE
    port map (
      C => emif_clk,
      CE => inst_base_module_wd_timeout_not0001,
      D => inst_emif_iface_edi_r(12),
      Q => inst_base_module_wd_timeout(12)
    );
  inst_base_module_wd_timeout_11 : FDE
    port map (
      C => emif_clk,
      CE => inst_base_module_wd_timeout_not0001,
      D => inst_emif_iface_edi_r(11),
      Q => inst_base_module_wd_timeout(11)
    );
  inst_base_module_wd_timeout_10 : FDE
    port map (
      C => emif_clk,
      CE => inst_base_module_wd_timeout_not0001,
      D => inst_emif_iface_edi_r(10),
      Q => inst_base_module_wd_timeout(10)
    );
  inst_base_module_wd_timeout_9 : FDE
    port map (
      C => emif_clk,
      CE => inst_base_module_wd_timeout_not0001,
      D => inst_emif_iface_edi_r(9),
      Q => inst_base_module_wd_timeout(9)
    );
  inst_base_module_wd_timeout_8 : FDE
    port map (
      C => emif_clk,
      CE => inst_base_module_wd_timeout_not0001,
      D => inst_emif_iface_edi_r(8),
      Q => inst_base_module_wd_timeout(8)
    );
  inst_base_module_wd_timeout_7 : FDE
    port map (
      C => emif_clk,
      CE => inst_base_module_wd_timeout_not0001,
      D => inst_emif_iface_edi_r(7),
      Q => inst_base_module_wd_timeout(7)
    );
  inst_base_module_wd_timeout_6 : FDE
    port map (
      C => emif_clk,
      CE => inst_base_module_wd_timeout_not0001,
      D => inst_emif_iface_edi_r(6),
      Q => inst_base_module_wd_timeout(6)
    );
  inst_base_module_wd_timeout_5 : FDE
    port map (
      C => emif_clk,
      CE => inst_base_module_wd_timeout_not0001,
      D => inst_emif_iface_edi_r(5),
      Q => inst_base_module_wd_timeout(5)
    );
  inst_base_module_wd_timeout_4 : FDE
    port map (
      C => emif_clk,
      CE => inst_base_module_wd_timeout_not0001,
      D => inst_emif_iface_edi_r(4),
      Q => inst_base_module_wd_timeout(4)
    );
  inst_base_module_wd_timeout_3 : FDE
    port map (
      C => emif_clk,
      CE => inst_base_module_wd_timeout_not0001,
      D => inst_emif_iface_edi_r(3),
      Q => inst_base_module_wd_timeout(3)
    );
  inst_base_module_wd_timeout_2 : FDE
    port map (
      C => emif_clk,
      CE => inst_base_module_wd_timeout_not0001,
      D => inst_emif_iface_edi_r(2),
      Q => inst_base_module_wd_timeout(2)
    );
  inst_base_module_wd_timeout_1 : FDE
    port map (
      C => emif_clk,
      CE => inst_base_module_wd_timeout_not0001,
      D => inst_emif_iface_edi_r(1),
      Q => inst_base_module_wd_timeout(1)
    );
  inst_base_module_wd_timeout_0 : FDE
    port map (
      C => emif_clk,
      CE => inst_base_module_wd_timeout_not0001,
      D => inst_emif_iface_edi_r(0),
      Q => inst_base_module_wd_timeout(0)
    );
  inst_base_module_wd_nmi : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      CE => inst_base_module_wd_nmi_not0001,
      D => inst_base_module_wd_nmi_mux0000,
      R => inst_base_module_wd_rst_not0002_inv,
      Q => inst_base_module_wd_nmi_939
    );
  inst_base_module_div10_1_1 : FD
    generic map(
      INIT => '1'
    )
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      D => inst_base_module_div10_1_10_Q,
      Q => inst_base_module_div10_1_1_Q
    );
  inst_base_module_wd_nmi_en : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_wd_nmi_en_not0001,
      D => inst_base_module_wd_nmi_en_mux0000,
      Q => inst_base_module_wd_nmi_en_945
    );
  inst_base_module_wd_counter_15 : FDE
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      CE => inst_base_module_wd_counter_not0002,
      D => inst_base_module_wd_counter_mux0000(15),
      Q => inst_base_module_wd_counter(15)
    );
  inst_base_module_wd_counter_14 : FDE
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      CE => inst_base_module_wd_counter_not0002,
      D => inst_base_module_wd_counter_mux0000(14),
      Q => inst_base_module_wd_counter(14)
    );
  inst_base_module_wd_counter_13 : FDE
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      CE => inst_base_module_wd_counter_not0002,
      D => inst_base_module_wd_counter_mux0000(13),
      Q => inst_base_module_wd_counter(13)
    );
  inst_base_module_wd_counter_12 : FDE
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      CE => inst_base_module_wd_counter_not0002,
      D => inst_base_module_wd_counter_mux0000(12),
      Q => inst_base_module_wd_counter(12)
    );
  inst_base_module_wd_counter_11 : FDE
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      CE => inst_base_module_wd_counter_not0002,
      D => inst_base_module_wd_counter_mux0000(11),
      Q => inst_base_module_wd_counter(11)
    );
  inst_base_module_wd_counter_10 : FDE
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      CE => inst_base_module_wd_counter_not0002,
      D => inst_base_module_wd_counter_mux0000(10),
      Q => inst_base_module_wd_counter(10)
    );
  inst_base_module_wd_counter_9 : FDE
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      CE => inst_base_module_wd_counter_not0002,
      D => inst_base_module_wd_counter_mux0000(9),
      Q => inst_base_module_wd_counter(9)
    );
  inst_base_module_wd_counter_8 : FDE
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      CE => inst_base_module_wd_counter_not0002,
      D => inst_base_module_wd_counter_mux0000(8),
      Q => inst_base_module_wd_counter(8)
    );
  inst_base_module_wd_counter_7 : FDE
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      CE => inst_base_module_wd_counter_not0002,
      D => inst_base_module_wd_counter_mux0000(7),
      Q => inst_base_module_wd_counter(7)
    );
  inst_base_module_wd_counter_6 : FDE
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      CE => inst_base_module_wd_counter_not0002,
      D => inst_base_module_wd_counter_mux0000(6),
      Q => inst_base_module_wd_counter(6)
    );
  inst_base_module_wd_counter_5 : FDE
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      CE => inst_base_module_wd_counter_not0002,
      D => inst_base_module_wd_counter_mux0000(5),
      Q => inst_base_module_wd_counter(5)
    );
  inst_base_module_wd_counter_4 : FDE
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      CE => inst_base_module_wd_counter_not0002,
      D => inst_base_module_wd_counter_mux0000(4),
      Q => inst_base_module_wd_counter(4)
    );
  inst_base_module_wd_counter_3 : FDE
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      CE => inst_base_module_wd_counter_not0002,
      D => inst_base_module_wd_counter_mux0000(3),
      Q => inst_base_module_wd_counter(3)
    );
  inst_base_module_wd_counter_2 : FDE
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      CE => inst_base_module_wd_counter_not0002,
      D => inst_base_module_wd_counter_mux0000(2),
      Q => inst_base_module_wd_counter(2)
    );
  inst_base_module_wd_counter_1 : FDE
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      CE => inst_base_module_wd_counter_not0002,
      D => inst_base_module_wd_counter_mux0000(1),
      Q => inst_base_module_wd_counter(1)
    );
  inst_base_module_wd_counter_0 : FDE
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      CE => inst_base_module_wd_counter_not0002,
      D => inst_base_module_wd_counter_mux0000(0),
      Q => inst_base_module_wd_counter(0)
    );
  inst_base_module_wd_rst : FDRE
    generic map(
      INIT => '0'
    )
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      CE => inst_base_module_wd_rst_not0003,
      D => inst_base_module_wd_nmi_mux0000,
      R => inst_base_module_wd_rst_not0002_inv,
      Q => inst_base_module_wd_rst_952
    );
  inst_base_module_irq_mask_31 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(0),
      Q => inst_base_module_irq_mask(31)
    );
  inst_base_module_irq_mask_30 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(1),
      Q => inst_base_module_irq_mask(30)
    );
  inst_base_module_irq_mask_29 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(2),
      Q => inst_base_module_irq_mask(29)
    );
  inst_base_module_irq_mask_28 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(3),
      Q => inst_base_module_irq_mask(28)
    );
  inst_base_module_irq_mask_27 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(4),
      Q => inst_base_module_irq_mask(27)
    );
  inst_base_module_irq_mask_26 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(5),
      Q => inst_base_module_irq_mask(26)
    );
  inst_base_module_irq_mask_25 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(6),
      Q => inst_base_module_irq_mask(25)
    );
  inst_base_module_irq_mask_24 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(7),
      Q => inst_base_module_irq_mask(24)
    );
  inst_base_module_irq_mask_23 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(8),
      Q => inst_base_module_irq_mask(23)
    );
  inst_base_module_irq_mask_22 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(9),
      Q => inst_base_module_irq_mask(22)
    );
  inst_base_module_irq_mask_21 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(10),
      Q => inst_base_module_irq_mask(21)
    );
  inst_base_module_irq_mask_20 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(11),
      Q => inst_base_module_irq_mask(20)
    );
  inst_base_module_irq_mask_19 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(12),
      Q => inst_base_module_irq_mask(19)
    );
  inst_base_module_irq_mask_18 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(13),
      Q => inst_base_module_irq_mask(18)
    );
  inst_base_module_irq_mask_17 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(14),
      Q => inst_base_module_irq_mask(17)
    );
  inst_base_module_irq_mask_16 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(15),
      Q => inst_base_module_irq_mask(16)
    );
  inst_base_module_irq_mask_15 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(16),
      Q => inst_base_module_irq_mask(15)
    );
  inst_base_module_irq_mask_14 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(17),
      Q => inst_base_module_irq_mask(14)
    );
  inst_base_module_irq_mask_13 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(18),
      Q => inst_base_module_irq_mask(13)
    );
  inst_base_module_irq_mask_12 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(19),
      Q => inst_base_module_irq_mask(12)
    );
  inst_base_module_irq_mask_11 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(20),
      Q => inst_base_module_irq_mask(11)
    );
  inst_base_module_irq_mask_10 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(21),
      Q => inst_base_module_irq_mask(10)
    );
  inst_base_module_irq_mask_9 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(22),
      Q => inst_base_module_irq_mask(9)
    );
  inst_base_module_irq_mask_8 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(23),
      Q => inst_base_module_irq_mask(8)
    );
  inst_base_module_irq_mask_7 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(24),
      Q => inst_base_module_irq_mask(7)
    );
  inst_base_module_irq_mask_6 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(25),
      Q => inst_base_module_irq_mask(6)
    );
  inst_base_module_irq_mask_5 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(26),
      Q => inst_base_module_irq_mask(5)
    );
  inst_base_module_irq_mask_4 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(27),
      Q => inst_base_module_irq_mask(4)
    );
  inst_base_module_irq_mask_3 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(28),
      Q => inst_base_module_irq_mask(3)
    );
  inst_base_module_irq_mask_2 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(29),
      Q => inst_base_module_irq_mask(2)
    );
  inst_base_module_irq_mask_1 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(30),
      Q => inst_base_module_irq_mask(1)
    );
  inst_base_module_irq_mask_0 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_base_module_and0000,
      D => inst_base_module_irq_mask_mux0000(31),
      Q => inst_base_module_irq_mask(0)
    );
  inst_base_module_clken100ms : FDR
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      D => Mtrien_io_altera_dsp_mux0000_norst,
      R => inst_base_module_clken100ms_not0001,
      Q => inst_base_module_clken100ms_610
    );
  inst_base_module_wd_nmi_clr : FDE
    port map (
      C => emif_clk,
      CE => inst_base_module_wd_nmi_clr_not0002,
      D => inst_base_module_wd_nmi_clr_not0003,
      Q => inst_base_module_wd_nmi_clr_940
    );
  inst_base_module_o_DBus_26 : FDR
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_26_Q,
      R => inst_base_module_i_cs_inv,
      Q => inst_base_module_o_DBus(26)
    );
  inst_base_module_o_DBus_25 : FDR
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_25_Q,
      R => inst_base_module_i_cs_inv,
      Q => inst_base_module_o_DBus(25)
    );
  inst_base_module_o_DBus_20 : FDR
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_20_Q,
      R => inst_base_module_i_cs_inv,
      Q => inst_base_module_o_DBus(20)
    );
  mb_altera_core_altera_state_FSM_FFd1 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_altera_state_FSM_FFd2_2229,
      Q => mb_altera_core_altera_state_FSM_FFd1_2228
    );
  mb_altera_core_altera_state_FSM_FFd2 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_altera_state_FSM_FFd3_2230,
      Q => mb_altera_core_altera_state_FSM_FFd2_2229
    );
  mb_altera_core_altera_state_FSM_FFd3 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_altera_state_FSM_FFd4_2231,
      Q => mb_altera_core_altera_state_FSM_FFd3_2230
    );
  mb_altera_core_s_dma_state_FSM_FFd2 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_state_FSM_FFd2_In,
      Q => mb_altera_core_s_dma_state_FSM_FFd2_2876
    );
  mb_altera_core_Flash_State_FSM_FFd1 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_Flash_State_FSM_FFd2_1600,
      Q => mb_altera_core_Flash_State_FSM_FFd1_1599
    );
  mb_altera_core_Flash_State_FSM_FFd2 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_Flash_State_FSM_FFd3_1601,
      Q => mb_altera_core_Flash_State_FSM_FFd2_1600
    );
  mb_altera_core_Flash_State_FSM_FFd4 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_Flash_State_FSM_FFd6_1605,
      Q => mb_altera_core_Flash_State_FSM_FFd4_1602
    );
  mb_altera_core_Flash_State_FSM_FFd3 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_Flash_State_FSM_FFd5_1603,
      Q => mb_altera_core_Flash_State_FSM_FFd3_1601
    );
  mb_altera_core_Flash_State_FSM_FFd6 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_Flash_State_FSM_FFd6_In,
      Q => mb_altera_core_Flash_State_FSM_FFd6_1605
    );
  mb_altera_core_Flash_State_FSM_FFd5 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_Flash_State_FSM_FFd5_In_1604,
      Q => mb_altera_core_Flash_State_FSM_FFd5_1603
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_30_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(29),
      DI => ea(15),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(30),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(30)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_29_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(28),
      DI => mb_altera_core_s_dma_space_mask(31),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(29),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(29)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_29_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(31),
      I1 => mb_altera_core_s_dma_start_addr_add0000(29),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(29)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_28_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(27),
      DI => mb_altera_core_s_dma_space_mask(30),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(28),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(28)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_28_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(30),
      I1 => mb_altera_core_s_dma_start_addr_add0000(28),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(28)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_27_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(26),
      DI => mb_altera_core_s_dma_space_mask(29),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(27),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(27)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_27_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(29),
      I1 => mb_altera_core_s_dma_start_addr_add0000(27),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(27)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_26_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(25),
      DI => mb_altera_core_s_dma_space_mask(28),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(26),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(26)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_26_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(28),
      I1 => mb_altera_core_s_dma_start_addr_add0000(26),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(26)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_25_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(24),
      DI => mb_altera_core_s_dma_space_mask(27),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(25),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(25)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_25_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(27),
      I1 => mb_altera_core_s_dma_start_addr_add0000(25),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(25)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_24_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(23),
      DI => mb_altera_core_s_dma_space_mask(26),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(24),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(24)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_24_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(26),
      I1 => mb_altera_core_s_dma_start_addr_add0000(24),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(24)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_23_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(22),
      DI => mb_altera_core_s_dma_space_mask(25),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(23),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(23)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_23_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(25),
      I1 => mb_altera_core_s_dma_start_addr_add0000(23),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(23)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_22_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(21),
      DI => mb_altera_core_s_dma_space_mask(24),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(22),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(22)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_22_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(24),
      I1 => mb_altera_core_s_dma_start_addr_add0000(22),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(22)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_21_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(20),
      DI => mb_altera_core_s_dma_space_mask(23),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(21),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(21)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_21_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(23),
      I1 => mb_altera_core_s_dma_start_addr_add0000(21),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(21)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_20_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(19),
      DI => mb_altera_core_s_dma_space_mask(22),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(20),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(20)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_20_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(22),
      I1 => mb_altera_core_s_dma_start_addr_add0000(20),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(20)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_19_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(18),
      DI => mb_altera_core_s_dma_space_mask(21),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(19),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(19)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_19_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(21),
      I1 => mb_altera_core_s_dma_start_addr_add0000(19),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(19)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_18_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(17),
      DI => mb_altera_core_s_dma_space_mask(20),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(18),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(18)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_18_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(20),
      I1 => mb_altera_core_s_dma_start_addr_add0000(18),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(18)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_17_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(16),
      DI => mb_altera_core_s_dma_space_mask(19),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(17),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(17)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_17_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(19),
      I1 => mb_altera_core_s_dma_start_addr_add0000(17),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(17)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_16_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(15),
      DI => mb_altera_core_s_dma_space_mask(18),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(16),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(16)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_16_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(18),
      I1 => mb_altera_core_s_dma_start_addr_add0000(16),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(16)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_15_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(14),
      DI => mb_altera_core_s_dma_space_mask(17),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(15),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(15)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_15_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(17),
      I1 => mb_altera_core_s_dma_start_addr_add0000(15),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(15)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_14_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(13),
      DI => mb_altera_core_s_dma_space_mask(16),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(14),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(14)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_14_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(16),
      I1 => mb_altera_core_s_dma_start_addr_add0000(14),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(14)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_13_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(12),
      DI => mb_altera_core_s_dma_space_mask(15),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(13),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(13)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_13_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(15),
      I1 => mb_altera_core_s_dma_start_addr_add0000(13),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(13)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_12_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(11),
      DI => mb_altera_core_s_dma_space_mask(14),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(12),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(12)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_12_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(14),
      I1 => mb_altera_core_s_dma_start_addr_add0000(12),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(12)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_11_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(10),
      DI => mb_altera_core_s_dma_space_mask(13),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(11),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(11)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_11_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(13),
      I1 => mb_altera_core_s_dma_start_addr_add0000(11),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(11)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_10_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(9),
      DI => mb_altera_core_s_dma_space_mask(12),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(10),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(10)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_10_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(12),
      I1 => mb_altera_core_s_dma_start_addr_add0000(10),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(10)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_9_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(8),
      DI => mb_altera_core_s_dma_space_mask(11),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(9),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(9)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_9_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(11),
      I1 => mb_altera_core_s_dma_start_addr_add0000(9),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(9)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_8_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(7),
      DI => mb_altera_core_s_dma_space_mask(10),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(8),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(8)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_8_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(10),
      I1 => mb_altera_core_s_dma_space_addr(8),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(8)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_7_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(6),
      DI => mb_altera_core_s_dma_space_mask(9),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(7),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(7)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_7_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(9),
      I1 => mb_altera_core_s_dma_space_addr(7),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(7)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_6_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(5),
      DI => mb_altera_core_s_dma_space_mask(8),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(6),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(6)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_6_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(8),
      I1 => mb_altera_core_s_dma_space_addr(6),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(6)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_5_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(4),
      DI => mb_altera_core_s_dma_space_mask(7),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(5),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(5)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_5_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(7),
      I1 => mb_altera_core_s_dma_space_addr(5),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(5)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_4_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(3),
      DI => mb_altera_core_s_dma_space_mask(6),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(4),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(4)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_4_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(6),
      I1 => mb_altera_core_s_dma_space_addr(4),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(4)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_3_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(2),
      DI => mb_altera_core_s_dma_space_mask(5),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(3),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(3)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_3_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(5),
      I1 => mb_altera_core_s_dma_space_addr(3),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(3)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_2_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(1),
      DI => mb_altera_core_s_dma_space_mask(4),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(2),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(2)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_2_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(4),
      I1 => mb_altera_core_s_dma_space_addr(2),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(2)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_1_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(0),
      DI => mb_altera_core_s_dma_space_mask(3),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(1),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(1)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_1_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(3),
      I1 => mb_altera_core_s_dma_space_addr(1),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(1)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy_0_Q : MUXCY
    port map (
      CI => Mtrien_io_altera_dsp_mux0000_norst,
      DI => mb_altera_core_s_dma_space_mask(2),
      S => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(0),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(0)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_0_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(2),
      I1 => mb_altera_core_s_dma_space_addr(0),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(0)
    );
  mb_altera_core_Madd_add0001_Madd_xor_29_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(28),
      LI => mb_altera_core_Madd_add0001_Madd_xor_29_rt_1705,
      O => mb_altera_core_add0001(29)
    );
  mb_altera_core_Madd_add0001_Madd_xor_28_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(27),
      LI => mb_altera_core_Madd_add0001_Madd_cy_28_rt_1687,
      O => mb_altera_core_add0001(28)
    );
  mb_altera_core_Madd_add0001_Madd_cy_28_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(27),
      DI => ea(15),
      S => mb_altera_core_Madd_add0001_Madd_cy_28_rt_1687,
      O => mb_altera_core_Madd_add0001_Madd_cy(28)
    );
  mb_altera_core_Madd_add0001_Madd_xor_27_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(26),
      LI => mb_altera_core_Madd_add0001_Madd_cy_27_rt_1685,
      O => mb_altera_core_add0001(27)
    );
  mb_altera_core_Madd_add0001_Madd_cy_27_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(26),
      DI => ea(15),
      S => mb_altera_core_Madd_add0001_Madd_cy_27_rt_1685,
      O => mb_altera_core_Madd_add0001_Madd_cy(27)
    );
  mb_altera_core_Madd_add0001_Madd_xor_26_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(25),
      LI => mb_altera_core_Madd_add0001_Madd_cy_26_rt_1683,
      O => mb_altera_core_add0001(26)
    );
  mb_altera_core_Madd_add0001_Madd_cy_26_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(25),
      DI => ea(15),
      S => mb_altera_core_Madd_add0001_Madd_cy_26_rt_1683,
      O => mb_altera_core_Madd_add0001_Madd_cy(26)
    );
  mb_altera_core_Madd_add0001_Madd_xor_25_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(24),
      LI => mb_altera_core_Madd_add0001_Madd_cy_25_rt_1681,
      O => mb_altera_core_add0001(25)
    );
  mb_altera_core_Madd_add0001_Madd_cy_25_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(24),
      DI => ea(15),
      S => mb_altera_core_Madd_add0001_Madd_cy_25_rt_1681,
      O => mb_altera_core_Madd_add0001_Madd_cy(25)
    );
  mb_altera_core_Madd_add0001_Madd_xor_24_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(23),
      LI => mb_altera_core_Madd_add0001_Madd_cy_24_rt_1679,
      O => mb_altera_core_add0001(24)
    );
  mb_altera_core_Madd_add0001_Madd_cy_24_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(23),
      DI => ea(15),
      S => mb_altera_core_Madd_add0001_Madd_cy_24_rt_1679,
      O => mb_altera_core_Madd_add0001_Madd_cy(24)
    );
  mb_altera_core_Madd_add0001_Madd_xor_23_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(22),
      LI => mb_altera_core_Madd_add0001_Madd_cy_23_rt_1677,
      O => mb_altera_core_add0001(23)
    );
  mb_altera_core_Madd_add0001_Madd_cy_23_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(22),
      DI => ea(15),
      S => mb_altera_core_Madd_add0001_Madd_cy_23_rt_1677,
      O => mb_altera_core_Madd_add0001_Madd_cy(23)
    );
  mb_altera_core_Madd_add0001_Madd_xor_22_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(21),
      LI => mb_altera_core_Madd_add0001_Madd_cy_22_rt_1675,
      O => mb_altera_core_add0001(22)
    );
  mb_altera_core_Madd_add0001_Madd_cy_22_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(21),
      DI => ea(15),
      S => mb_altera_core_Madd_add0001_Madd_cy_22_rt_1675,
      O => mb_altera_core_Madd_add0001_Madd_cy(22)
    );
  mb_altera_core_Madd_add0001_Madd_xor_21_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(20),
      LI => mb_altera_core_Madd_add0001_Madd_cy_21_rt_1673,
      O => mb_altera_core_add0001(21)
    );
  mb_altera_core_Madd_add0001_Madd_cy_21_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(20),
      DI => ea(15),
      S => mb_altera_core_Madd_add0001_Madd_cy_21_rt_1673,
      O => mb_altera_core_Madd_add0001_Madd_cy(21)
    );
  mb_altera_core_Madd_add0001_Madd_xor_20_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(19),
      LI => mb_altera_core_Madd_add0001_Madd_cy_20_rt_1671,
      O => mb_altera_core_add0001(20)
    );
  mb_altera_core_Madd_add0001_Madd_cy_20_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(19),
      DI => ea(15),
      S => mb_altera_core_Madd_add0001_Madd_cy_20_rt_1671,
      O => mb_altera_core_Madd_add0001_Madd_cy(20)
    );
  mb_altera_core_Madd_add0001_Madd_xor_19_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(18),
      LI => mb_altera_core_Madd_add0001_Madd_cy_19_rt_1667,
      O => mb_altera_core_add0001(19)
    );
  mb_altera_core_Madd_add0001_Madd_cy_19_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(18),
      DI => ea(15),
      S => mb_altera_core_Madd_add0001_Madd_cy_19_rt_1667,
      O => mb_altera_core_Madd_add0001_Madd_cy(19)
    );
  mb_altera_core_Madd_add0001_Madd_xor_18_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(17),
      LI => mb_altera_core_Madd_add0001_Madd_cy_18_rt_1665,
      O => mb_altera_core_add0001(18)
    );
  mb_altera_core_Madd_add0001_Madd_cy_18_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(17),
      DI => ea(15),
      S => mb_altera_core_Madd_add0001_Madd_cy_18_rt_1665,
      O => mb_altera_core_Madd_add0001_Madd_cy(18)
    );
  mb_altera_core_Madd_add0001_Madd_xor_17_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(16),
      LI => mb_altera_core_Madd_add0001_Madd_cy_17_rt_1663,
      O => mb_altera_core_add0001(17)
    );
  mb_altera_core_Madd_add0001_Madd_cy_17_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(16),
      DI => ea(15),
      S => mb_altera_core_Madd_add0001_Madd_cy_17_rt_1663,
      O => mb_altera_core_Madd_add0001_Madd_cy(17)
    );
  mb_altera_core_Madd_add0001_Madd_xor_16_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(15),
      LI => mb_altera_core_Madd_add0001_Madd_cy_16_rt_1661,
      O => mb_altera_core_add0001(16)
    );
  mb_altera_core_Madd_add0001_Madd_cy_16_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(15),
      DI => ea(15),
      S => mb_altera_core_Madd_add0001_Madd_cy_16_rt_1661,
      O => mb_altera_core_Madd_add0001_Madd_cy(16)
    );
  mb_altera_core_Madd_add0001_Madd_xor_15_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(14),
      LI => mb_altera_core_Madd_add0001_Madd_cy_15_rt_1659,
      O => mb_altera_core_add0001(15)
    );
  mb_altera_core_Madd_add0001_Madd_cy_15_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(14),
      DI => ea(15),
      S => mb_altera_core_Madd_add0001_Madd_cy_15_rt_1659,
      O => mb_altera_core_Madd_add0001_Madd_cy(15)
    );
  mb_altera_core_Madd_add0001_Madd_xor_14_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(13),
      LI => mb_altera_core_Madd_add0001_Madd_cy_14_rt_1657,
      O => mb_altera_core_add0001(14)
    );
  mb_altera_core_Madd_add0001_Madd_cy_14_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(13),
      DI => ea(15),
      S => mb_altera_core_Madd_add0001_Madd_cy_14_rt_1657,
      O => mb_altera_core_Madd_add0001_Madd_cy(14)
    );
  mb_altera_core_Madd_add0001_Madd_xor_13_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(12),
      LI => mb_altera_core_Madd_add0001_Madd_cy_13_rt_1655,
      O => mb_altera_core_add0001(13)
    );
  mb_altera_core_Madd_add0001_Madd_cy_13_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(12),
      DI => ea(15),
      S => mb_altera_core_Madd_add0001_Madd_cy_13_rt_1655,
      O => mb_altera_core_Madd_add0001_Madd_cy(13)
    );
  mb_altera_core_Madd_add0001_Madd_xor_12_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(11),
      LI => mb_altera_core_Madd_add0001_Madd_cy_12_rt_1653,
      O => mb_altera_core_add0001(12)
    );
  mb_altera_core_Madd_add0001_Madd_cy_12_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(11),
      DI => ea(15),
      S => mb_altera_core_Madd_add0001_Madd_cy_12_rt_1653,
      O => mb_altera_core_Madd_add0001_Madd_cy(12)
    );
  mb_altera_core_Madd_add0001_Madd_xor_11_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(10),
      LI => mb_altera_core_Madd_add0001_Madd_cy_11_rt_1651,
      O => mb_altera_core_add0001(11)
    );
  mb_altera_core_Madd_add0001_Madd_cy_11_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(10),
      DI => ea(15),
      S => mb_altera_core_Madd_add0001_Madd_cy_11_rt_1651,
      O => mb_altera_core_Madd_add0001_Madd_cy(11)
    );
  mb_altera_core_Madd_add0001_Madd_xor_10_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(9),
      LI => mb_altera_core_Madd_add0001_Madd_cy_10_rt_1649,
      O => mb_altera_core_add0001(10)
    );
  mb_altera_core_Madd_add0001_Madd_cy_10_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(9),
      DI => ea(15),
      S => mb_altera_core_Madd_add0001_Madd_cy_10_rt_1649,
      O => mb_altera_core_Madd_add0001_Madd_cy(10)
    );
  mb_altera_core_Madd_add0001_Madd_xor_9_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(8),
      LI => mb_altera_core_Madd_add0001_Madd_cy_9_rt_1703,
      O => mb_altera_core_add0001(9)
    );
  mb_altera_core_Madd_add0001_Madd_cy_9_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(8),
      DI => ea(15),
      S => mb_altera_core_Madd_add0001_Madd_cy_9_rt_1703,
      O => mb_altera_core_Madd_add0001_Madd_cy(9)
    );
  mb_altera_core_Madd_add0001_Madd_xor_8_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(7),
      LI => mb_altera_core_Madd_add0001_Madd_cy_8_rt_1701,
      O => mb_altera_core_add0001(8)
    );
  mb_altera_core_Madd_add0001_Madd_cy_8_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(7),
      DI => ea(15),
      S => mb_altera_core_Madd_add0001_Madd_cy_8_rt_1701,
      O => mb_altera_core_Madd_add0001_Madd_cy(8)
    );
  mb_altera_core_Madd_add0001_Madd_xor_7_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(6),
      LI => mb_altera_core_Madd_add0001_Madd_cy_7_rt_1699,
      O => mb_altera_core_add0001(7)
    );
  mb_altera_core_Madd_add0001_Madd_cy_7_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(6),
      DI => ea(15),
      S => mb_altera_core_Madd_add0001_Madd_cy_7_rt_1699,
      O => mb_altera_core_Madd_add0001_Madd_cy(7)
    );
  mb_altera_core_Madd_add0001_Madd_xor_6_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(5),
      LI => mb_altera_core_Madd_add0001_Madd_cy_6_rt_1697,
      O => mb_altera_core_add0001(6)
    );
  mb_altera_core_Madd_add0001_Madd_cy_6_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(5),
      DI => ea(15),
      S => mb_altera_core_Madd_add0001_Madd_cy_6_rt_1697,
      O => mb_altera_core_Madd_add0001_Madd_cy(6)
    );
  mb_altera_core_Madd_add0001_Madd_xor_5_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(4),
      LI => mb_altera_core_Madd_add0001_Madd_cy_5_rt_1695,
      O => mb_altera_core_add0001(5)
    );
  mb_altera_core_Madd_add0001_Madd_cy_5_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(4),
      DI => ea(15),
      S => mb_altera_core_Madd_add0001_Madd_cy_5_rt_1695,
      O => mb_altera_core_Madd_add0001_Madd_cy(5)
    );
  mb_altera_core_Madd_add0001_Madd_xor_4_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(3),
      LI => mb_altera_core_Madd_add0001_Madd_cy_4_rt_1693,
      O => mb_altera_core_add0001(4)
    );
  mb_altera_core_Madd_add0001_Madd_cy_4_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(3),
      DI => ea(15),
      S => mb_altera_core_Madd_add0001_Madd_cy_4_rt_1693,
      O => mb_altera_core_Madd_add0001_Madd_cy(4)
    );
  mb_altera_core_Madd_add0001_Madd_xor_3_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(2),
      LI => mb_altera_core_Madd_add0001_Madd_cy_3_rt_1691,
      O => mb_altera_core_add0001(3)
    );
  mb_altera_core_Madd_add0001_Madd_cy_3_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(2),
      DI => ea(15),
      S => mb_altera_core_Madd_add0001_Madd_cy_3_rt_1691,
      O => mb_altera_core_Madd_add0001_Madd_cy(3)
    );
  mb_altera_core_Madd_add0001_Madd_xor_2_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(1),
      LI => mb_altera_core_Madd_add0001_Madd_cy_2_rt_1689,
      O => mb_altera_core_add0001(2)
    );
  mb_altera_core_Madd_add0001_Madd_cy_2_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(1),
      DI => ea(15),
      S => mb_altera_core_Madd_add0001_Madd_cy_2_rt_1689,
      O => mb_altera_core_Madd_add0001_Madd_cy(2)
    );
  mb_altera_core_Madd_add0001_Madd_xor_1_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(0),
      LI => mb_altera_core_Madd_add0001_Madd_cy_1_rt_1669,
      O => mb_altera_core_add0001(1)
    );
  mb_altera_core_Madd_add0001_Madd_cy_1_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_add0001_Madd_cy(0),
      DI => ea(15),
      S => mb_altera_core_Madd_add0001_Madd_cy_1_rt_1669,
      O => mb_altera_core_Madd_add0001_Madd_cy(1)
    );
  mb_altera_core_Madd_add0001_Madd_xor_0_Q : XORCY
    port map (
      CI => ea(15),
      LI => mb_altera_core_Madd_add0001_Madd_lut(0),
      O => mb_altera_core_add0001(0)
    );
  mb_altera_core_Madd_add0001_Madd_cy_0_Q : MUXCY
    port map (
      CI => ea(15),
      DI => Mtrien_io_altera_dsp_mux0000_norst,
      S => mb_altera_core_Madd_add0001_Madd_lut(0),
      O => mb_altera_core_Madd_add0001_Madd_cy(0)
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_xor_9_Q : XORCY
    port map (
      CI => mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy(8),
      LI => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(9),
      O => mb_altera_core_Result(9)
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_xor_8_Q : XORCY
    port map (
      CI => mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy(7),
      LI => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(8),
      O => mb_altera_core_Result(8)
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy_8_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy(7),
      DI => mb_altera_core_Write_fifo_wr_Cnt(8),
      S => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(8),
      O => mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy(8)
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_xor_7_Q : XORCY
    port map (
      CI => mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy(6),
      LI => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(7),
      O => mb_altera_core_Result(7)
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy_7_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy(6),
      DI => mb_altera_core_Write_fifo_wr_Cnt(7),
      S => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(7),
      O => mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy(7)
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_xor_6_Q : XORCY
    port map (
      CI => mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy(5),
      LI => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(6),
      O => mb_altera_core_Result(6)
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy_6_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy(5),
      DI => mb_altera_core_Write_fifo_wr_Cnt(6),
      S => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(6),
      O => mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy(6)
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_xor_5_Q : XORCY
    port map (
      CI => mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy(4),
      LI => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(5),
      O => mb_altera_core_Result(5)
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy_5_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy(4),
      DI => mb_altera_core_Write_fifo_wr_Cnt(5),
      S => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(5),
      O => mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy(5)
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_xor_4_Q : XORCY
    port map (
      CI => mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy(3),
      LI => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(4),
      O => mb_altera_core_Result(4)
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy_4_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy(3),
      DI => mb_altera_core_Write_fifo_wr_Cnt(4),
      S => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(4),
      O => mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy(4)
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_xor_3_Q : XORCY
    port map (
      CI => mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy(2),
      LI => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(3),
      O => mb_altera_core_Result(3)
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy_3_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy(2),
      DI => mb_altera_core_Write_fifo_wr_Cnt(3),
      S => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(3),
      O => mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy(3)
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_xor_2_Q : XORCY
    port map (
      CI => mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy(1),
      LI => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(2),
      O => mb_altera_core_Result(2)
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy_2_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy(1),
      DI => mb_altera_core_Write_fifo_wr_Cnt(2),
      S => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(2),
      O => mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy(2)
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_xor_1_Q : XORCY
    port map (
      CI => mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy(0),
      LI => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(1),
      O => mb_altera_core_Result(1)
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy_1_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy(0),
      DI => mb_altera_core_Write_fifo_wr_Cnt(1),
      S => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(1),
      O => mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy(1)
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_xor_0_Q : XORCY
    port map (
      CI => mb_altera_core_Write_fifo_wr_Cnt_and0000_inv,
      LI => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(0),
      O => mb_altera_core_Result(0)
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy_0_Q : MUXCY
    port map (
      CI => mb_altera_core_Write_fifo_wr_Cnt_and0000_inv,
      DI => mb_altera_core_Write_fifo_wr_Cnt(0),
      S => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(0),
      O => mb_altera_core_Mcount_Write_fifo_wr_Cnt_cy(0)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_gt0000_cy_3_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_gt0000_cy(2),
      DI => ea(15),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_gt0000_lut(3),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_gt0000_cy(3)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_gt0000_cy_2_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_gt0000_cy(1),
      DI => ea(15),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_gt0000_lut(2),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_gt0000_cy(2)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_gt0000_lut_2_Q : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => mb_altera_core_s_altera_rdfifo_rd_cnt(8),
      I1 => mb_altera_core_s_altera_rdfifo_rd_cnt(9),
      I2 => mb_altera_core_s_altera_rdfifo_rd_cnt(10),
      I3 => mb_altera_core_s_altera_rdfifo_rd_cnt(11),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_gt0000_lut(2)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_gt0000_cy_1_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_gt0000_cy(0),
      DI => ea(15),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_gt0000_lut(1),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_gt0000_cy(1)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_gt0000_lut_1_Q : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => mb_altera_core_s_altera_rdfifo_rd_cnt(4),
      I1 => mb_altera_core_s_altera_rdfifo_rd_cnt(5),
      I2 => mb_altera_core_s_altera_rdfifo_rd_cnt(6),
      I3 => mb_altera_core_s_altera_rdfifo_rd_cnt(7),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_gt0000_lut(1)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_gt0000_cy_0_Q : MUXCY
    port map (
      CI => Mtrien_io_altera_dsp_mux0000_norst,
      DI => ea(15),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_gt0000_lut(0),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_gt0000_cy(0)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_gt0000_lut_0_Q : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => mb_altera_core_Msub_s_dma_count_share0000_cy_0_Q,
      I1 => mb_altera_core_s_altera_rdfifo_rd_cnt(1),
      I2 => mb_altera_core_s_altera_rdfifo_rd_cnt(2),
      I3 => mb_altera_core_s_altera_rdfifo_rd_cnt(3),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_gt0000_lut(0)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_31_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(30),
      DI => mb_altera_core_s_dma_timeout_cntr(31),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(31),
      O => mb_altera_core_s_dma_state_cmp_ge0001
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_31_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(31),
      I1 => mb_altera_core_s_dma_timeout_threshold(31),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(31)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_30_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(29),
      DI => mb_altera_core_s_dma_timeout_cntr(30),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(30),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(30)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_30_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(30),
      I1 => mb_altera_core_s_dma_timeout_threshold(30),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(30)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_29_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(28),
      DI => mb_altera_core_s_dma_timeout_cntr(29),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(29),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(29)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_29_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(29),
      I1 => mb_altera_core_s_dma_timeout_threshold(29),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(29)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_28_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(27),
      DI => mb_altera_core_s_dma_timeout_cntr(28),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(28),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(28)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_28_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(28),
      I1 => mb_altera_core_s_dma_timeout_threshold(28),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(28)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_27_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(26),
      DI => mb_altera_core_s_dma_timeout_cntr(27),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(27),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(27)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_27_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(27),
      I1 => mb_altera_core_s_dma_timeout_threshold(27),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(27)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_26_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(25),
      DI => mb_altera_core_s_dma_timeout_cntr(26),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(26),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(26)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_26_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(26),
      I1 => mb_altera_core_s_dma_timeout_threshold(26),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(26)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_25_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(24),
      DI => mb_altera_core_s_dma_timeout_cntr(25),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(25),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(25)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_25_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(25),
      I1 => mb_altera_core_s_dma_timeout_threshold(25),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(25)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_24_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(23),
      DI => mb_altera_core_s_dma_timeout_cntr(24),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(24),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(24)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_24_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(24),
      I1 => mb_altera_core_s_dma_timeout_threshold(24),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(24)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_23_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(22),
      DI => mb_altera_core_s_dma_timeout_cntr(23),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(23),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(23)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_23_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(23),
      I1 => mb_altera_core_s_dma_timeout_threshold(23),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(23)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_22_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(21),
      DI => mb_altera_core_s_dma_timeout_cntr(22),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(22),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(22)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_22_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(22),
      I1 => mb_altera_core_s_dma_timeout_threshold(22),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(22)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_21_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(20),
      DI => mb_altera_core_s_dma_timeout_cntr(21),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(21),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(21)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_21_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(21),
      I1 => mb_altera_core_s_dma_timeout_threshold(21),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(21)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_20_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(19),
      DI => mb_altera_core_s_dma_timeout_cntr(20),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(20),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(20)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_20_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(20),
      I1 => mb_altera_core_s_dma_timeout_threshold(20),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(20)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_19_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(18),
      DI => mb_altera_core_s_dma_timeout_cntr(19),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(19),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(19)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_19_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(19),
      I1 => mb_altera_core_s_dma_timeout_threshold(19),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(19)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_18_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(17),
      DI => mb_altera_core_s_dma_timeout_cntr(18),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(18),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(18)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_18_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(18),
      I1 => mb_altera_core_s_dma_timeout_threshold(18),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(18)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_17_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(16),
      DI => mb_altera_core_s_dma_timeout_cntr(17),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(17),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(17)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_17_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(17),
      I1 => mb_altera_core_s_dma_timeout_threshold(17),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(17)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_16_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(15),
      DI => mb_altera_core_s_dma_timeout_cntr(16),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(16),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(16)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_16_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(16),
      I1 => mb_altera_core_s_dma_timeout_threshold(16),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(16)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_15_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(14),
      DI => mb_altera_core_s_dma_timeout_cntr(15),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(15),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(15)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_15_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(15),
      I1 => mb_altera_core_s_dma_timeout_threshold(15),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(15)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_14_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(13),
      DI => mb_altera_core_s_dma_timeout_cntr(14),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(14),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(14)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_14_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(14),
      I1 => mb_altera_core_s_dma_timeout_threshold(14),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(14)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_13_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(12),
      DI => mb_altera_core_s_dma_timeout_cntr(13),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(13),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(13)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_13_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(13),
      I1 => mb_altera_core_s_dma_timeout_threshold(13),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(13)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_12_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(11),
      DI => mb_altera_core_s_dma_timeout_cntr(12),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(12),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(12)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_12_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(12),
      I1 => mb_altera_core_s_dma_timeout_threshold(12),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(12)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_11_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(10),
      DI => mb_altera_core_s_dma_timeout_cntr(11),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(11),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(11)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_11_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(11),
      I1 => mb_altera_core_s_dma_timeout_threshold(11),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(11)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_10_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(9),
      DI => mb_altera_core_s_dma_timeout_cntr(10),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(10),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(10)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_10_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(10),
      I1 => mb_altera_core_s_dma_timeout_threshold(10),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(10)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_9_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(8),
      DI => mb_altera_core_s_dma_timeout_cntr(9),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(9),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(9)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_9_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(9),
      I1 => mb_altera_core_s_dma_timeout_threshold(9),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(9)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_8_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(7),
      DI => mb_altera_core_s_dma_timeout_cntr(8),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(8),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(8)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_8_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(8),
      I1 => mb_altera_core_s_dma_timeout_threshold(8),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(8)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_7_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(6),
      DI => mb_altera_core_s_dma_timeout_cntr(7),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(7),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(7)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_7_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(7),
      I1 => mb_altera_core_s_dma_timeout_threshold(7),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(7)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_6_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(5),
      DI => mb_altera_core_s_dma_timeout_cntr(6),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(6),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(6)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_6_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(6),
      I1 => mb_altera_core_s_dma_timeout_threshold(6),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(6)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_5_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(4),
      DI => mb_altera_core_s_dma_timeout_cntr(5),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(5),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(5)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_5_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(5),
      I1 => mb_altera_core_s_dma_timeout_threshold(5),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(5)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_4_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(3),
      DI => mb_altera_core_s_dma_timeout_cntr(4),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(4),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(4)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_4_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(4),
      I1 => mb_altera_core_s_dma_timeout_threshold(4),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(4)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_3_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(2),
      DI => mb_altera_core_s_dma_timeout_cntr(3),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(3),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(3)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_3_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(3),
      I1 => mb_altera_core_s_dma_timeout_threshold(3),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(3)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_2_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(1),
      DI => mb_altera_core_s_dma_timeout_cntr(2),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(2),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(2)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_2_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(2),
      I1 => mb_altera_core_s_dma_timeout_threshold(2),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(2)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_1_Q : MUXCY
    port map (
      CI => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(0),
      DI => mb_altera_core_s_dma_timeout_cntr(1),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(1),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(1)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_1_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(1),
      I1 => mb_altera_core_s_dma_timeout_threshold(1),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(1)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy_0_Q : MUXCY
    port map (
      CI => Mtrien_io_altera_dsp_mux0000_norst,
      DI => mb_altera_core_s_dma_timeout_cntr(0),
      S => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(0),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_cy(0)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut_0_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(0),
      I1 => mb_altera_core_s_dma_timeout_threshold(0),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_ge0001_lut(0)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_31_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(30),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_31_rt_1853,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(31)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_30_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(29),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_30_rt_1837,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(30)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_30_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(29),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_30_rt_1837,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(30)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_29_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(28),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_29_rt_1833,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(29)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_29_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(28),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_29_rt_1833,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(29)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_28_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(27),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_28_rt_1831,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(28)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_28_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(27),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_28_rt_1831,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(28)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_27_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(26),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_27_rt_1829,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(27)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_27_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(26),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_27_rt_1829,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(27)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_26_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(25),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_26_rt_1827,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(26)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_26_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(25),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_26_rt_1827,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(26)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_25_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(24),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_25_rt_1825,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(25)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_25_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(24),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_25_rt_1825,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(25)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_24_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(23),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_24_rt_1823,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(24)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_24_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(23),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_24_rt_1823,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(24)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_23_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(22),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_23_rt_1821,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(23)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_23_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(22),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_23_rt_1821,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(23)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_22_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(21),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_22_rt_1819,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(22)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_22_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(21),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_22_rt_1819,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(22)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_21_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(20),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_21_rt_1817,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(21)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_21_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(20),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_21_rt_1817,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(21)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_20_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(19),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_20_rt_1815,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(20)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_20_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(19),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_20_rt_1815,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(20)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_19_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(18),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_19_rt_1811,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(19)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_19_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(18),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_19_rt_1811,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(19)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_18_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(17),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_18_rt_1809,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(18)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_18_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(17),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_18_rt_1809,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(18)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_17_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(16),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_17_rt_1807,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(17)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_17_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(16),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_17_rt_1807,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(17)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_16_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(15),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_16_rt_1805,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(16)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_16_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(15),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_16_rt_1805,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(16)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_15_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(14),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_15_rt_1803,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(15)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_15_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(14),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_15_rt_1803,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(15)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_14_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(13),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_14_rt_1801,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(14)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_14_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(13),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_14_rt_1801,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(14)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_13_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(12),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_13_rt_1799,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(13)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_13_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(12),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_13_rt_1799,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(13)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_12_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(11),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_12_rt_1797,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(12)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_12_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(11),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_12_rt_1797,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(12)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_11_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(10),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_11_rt_1795,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(11)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_11_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(10),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_11_rt_1795,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(11)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_10_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(9),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_10_rt_1793,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(10)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_10_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(9),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_10_rt_1793,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(10)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_9_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(8),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_9_rt_1851,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(9)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_9_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(8),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_9_rt_1851,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(9)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_8_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(7),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_8_rt_1849,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(8)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_8_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(7),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_8_rt_1849,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(8)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_7_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(6),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_7_rt_1847,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(7)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_7_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(6),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_7_rt_1847,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(7)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_6_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(5),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_6_rt_1845,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(6)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_6_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(5),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_6_rt_1845,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(6)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_5_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(4),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_5_rt_1843,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(5)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_5_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(4),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_5_rt_1843,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(5)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_4_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(3),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_4_rt_1841,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(4)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_4_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(3),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_4_rt_1841,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(4)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_3_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(2),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_3_rt_1839,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(3)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_3_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(2),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_3_rt_1839,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(3)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_2_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(1),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_2_rt_1835,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(2)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_2_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(1),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_2_rt_1835,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(2)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_1_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(0),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_1_rt_1813,
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(1)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_1_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(0),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_1_rt_1813,
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(1)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_0_Q : XORCY
    port map (
      CI => ea(15),
      LI => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_lut(0),
      O => mb_altera_core_s_dma_timeout_cntr_addsub0000(0)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_0_Q : MUXCY
    port map (
      CI => ea(15),
      DI => Mtrien_io_altera_dsp_mux0000_norst,
      S => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_lut(0),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy(0)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_xor_29_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(28),
      LI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_29_rt_1788,
      O => mb_altera_core_s_dma_start_addr_add0000(29)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_29_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(28),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_29_rt_1788,
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(29)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_xor_28_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(27),
      LI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_28_rt_1786,
      O => mb_altera_core_s_dma_start_addr_add0000(28)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_28_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(27),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_28_rt_1786,
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(28)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_xor_27_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(26),
      LI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_27_rt_1784,
      O => mb_altera_core_s_dma_start_addr_add0000(27)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_27_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(26),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_27_rt_1784,
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(27)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_xor_26_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(25),
      LI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_26_rt_1782,
      O => mb_altera_core_s_dma_start_addr_add0000(26)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_26_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(25),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_26_rt_1782,
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(26)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_xor_25_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(24),
      LI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_25_rt_1780,
      O => mb_altera_core_s_dma_start_addr_add0000(25)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_25_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(24),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_25_rt_1780,
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(25)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_xor_24_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(23),
      LI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_24_rt_1778,
      O => mb_altera_core_s_dma_start_addr_add0000(24)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_24_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(23),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_24_rt_1778,
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(24)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_xor_23_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(22),
      LI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_23_rt_1776,
      O => mb_altera_core_s_dma_start_addr_add0000(23)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_23_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(22),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_23_rt_1776,
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(23)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_xor_22_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(21),
      LI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_22_rt_1774,
      O => mb_altera_core_s_dma_start_addr_add0000(22)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_22_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(21),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_22_rt_1774,
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(22)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_xor_21_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(20),
      LI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_21_rt_1772,
      O => mb_altera_core_s_dma_start_addr_add0000(21)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_21_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(20),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_21_rt_1772,
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(21)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_xor_20_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(19),
      LI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_20_rt_1770,
      O => mb_altera_core_s_dma_start_addr_add0000(20)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_20_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(19),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_20_rt_1770,
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(20)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_xor_19_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(18),
      LI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_19_rt_1768,
      O => mb_altera_core_s_dma_start_addr_add0000(19)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_19_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(18),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_19_rt_1768,
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(19)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_xor_18_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(17),
      LI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_18_rt_1766,
      O => mb_altera_core_s_dma_start_addr_add0000(18)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_18_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(17),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_18_rt_1766,
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(18)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_xor_17_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(16),
      LI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_17_rt_1764,
      O => mb_altera_core_s_dma_start_addr_add0000(17)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_17_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(16),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_17_rt_1764,
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(17)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_xor_16_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(15),
      LI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_16_rt_1762,
      O => mb_altera_core_s_dma_start_addr_add0000(16)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_16_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(15),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_16_rt_1762,
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(16)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_xor_15_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(14),
      LI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_15_rt_1760,
      O => mb_altera_core_s_dma_start_addr_add0000(15)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_15_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(14),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_15_rt_1760,
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(15)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_xor_14_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(13),
      LI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_14_rt_1758,
      O => mb_altera_core_s_dma_start_addr_add0000(14)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_14_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(13),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_14_rt_1758,
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(14)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_xor_13_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(12),
      LI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_13_rt_1756,
      O => mb_altera_core_s_dma_start_addr_add0000(13)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_13_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(12),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_13_rt_1756,
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(13)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_xor_12_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(11),
      LI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_12_rt_1754,
      O => mb_altera_core_s_dma_start_addr_add0000(12)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_12_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(11),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_12_rt_1754,
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(12)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_xor_11_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(10),
      LI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_11_rt_1752,
      O => mb_altera_core_s_dma_start_addr_add0000(11)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_11_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(10),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_11_rt_1752,
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(11)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_xor_10_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(9),
      LI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_10_rt_1750,
      O => mb_altera_core_s_dma_start_addr_add0000(10)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_10_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(9),
      DI => ea(15),
      S => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_10_rt_1750,
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(10)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_xor_9_Q : XORCY
    port map (
      CI => ea(15),
      LI => mb_altera_core_Madd_s_dma_start_addr_add0000_lut(9),
      O => mb_altera_core_s_dma_start_addr_add0000(9)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_9_Q : MUXCY
    port map (
      CI => ea(15),
      DI => Mtrien_io_altera_dsp_mux0000_norst,
      S => mb_altera_core_Madd_s_dma_start_addr_add0000_lut(9),
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(9)
    );
  mb_altera_core_Madd_o_dma_addr_xor_20_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(19),
      LI => mb_altera_core_Madd_o_dma_addr_lut(20),
      O => dma_addr(0, 20)
    );
  mb_altera_core_Madd_o_dma_addr_lut_20_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_s_dma_base_addr(20),
      I1 => mb_altera_core_s_dma_space_addr(20),
      O => mb_altera_core_Madd_o_dma_addr_lut(20)
    );
  mb_altera_core_Madd_o_dma_addr_xor_19_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(18),
      LI => mb_altera_core_Madd_o_dma_addr_lut(19),
      O => dma_addr(0, 19)
    );
  mb_altera_core_Madd_o_dma_addr_cy_19_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(18),
      DI => mb_altera_core_s_dma_base_addr(19),
      S => mb_altera_core_Madd_o_dma_addr_lut(19),
      O => mb_altera_core_Madd_o_dma_addr_cy(19)
    );
  mb_altera_core_Madd_o_dma_addr_lut_19_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_s_dma_base_addr(19),
      I1 => mb_altera_core_s_dma_space_addr(19),
      O => mb_altera_core_Madd_o_dma_addr_lut(19)
    );
  mb_altera_core_Madd_o_dma_addr_xor_18_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(17),
      LI => mb_altera_core_Madd_o_dma_addr_lut(18),
      O => dma_addr(0, 18)
    );
  mb_altera_core_Madd_o_dma_addr_cy_18_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(17),
      DI => mb_altera_core_s_dma_base_addr(18),
      S => mb_altera_core_Madd_o_dma_addr_lut(18),
      O => mb_altera_core_Madd_o_dma_addr_cy(18)
    );
  mb_altera_core_Madd_o_dma_addr_lut_18_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_s_dma_base_addr(18),
      I1 => mb_altera_core_s_dma_space_addr(18),
      O => mb_altera_core_Madd_o_dma_addr_lut(18)
    );
  mb_altera_core_Madd_o_dma_addr_xor_17_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(16),
      LI => mb_altera_core_Madd_o_dma_addr_lut(17),
      O => dma_addr(0, 17)
    );
  mb_altera_core_Madd_o_dma_addr_cy_17_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(16),
      DI => mb_altera_core_s_dma_base_addr(17),
      S => mb_altera_core_Madd_o_dma_addr_lut(17),
      O => mb_altera_core_Madd_o_dma_addr_cy(17)
    );
  mb_altera_core_Madd_o_dma_addr_lut_17_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_s_dma_base_addr(17),
      I1 => mb_altera_core_s_dma_space_addr(17),
      O => mb_altera_core_Madd_o_dma_addr_lut(17)
    );
  mb_altera_core_Madd_o_dma_addr_xor_16_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(15),
      LI => mb_altera_core_Madd_o_dma_addr_lut(16),
      O => dma_addr(0, 16)
    );
  mb_altera_core_Madd_o_dma_addr_cy_16_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(15),
      DI => mb_altera_core_s_dma_base_addr(16),
      S => mb_altera_core_Madd_o_dma_addr_lut(16),
      O => mb_altera_core_Madd_o_dma_addr_cy(16)
    );
  mb_altera_core_Madd_o_dma_addr_lut_16_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_s_dma_base_addr(16),
      I1 => mb_altera_core_s_dma_space_addr(16),
      O => mb_altera_core_Madd_o_dma_addr_lut(16)
    );
  mb_altera_core_Madd_o_dma_addr_xor_15_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(14),
      LI => mb_altera_core_Madd_o_dma_addr_lut(15),
      O => dma_addr(0, 15)
    );
  mb_altera_core_Madd_o_dma_addr_cy_15_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(14),
      DI => mb_altera_core_s_dma_base_addr(15),
      S => mb_altera_core_Madd_o_dma_addr_lut(15),
      O => mb_altera_core_Madd_o_dma_addr_cy(15)
    );
  mb_altera_core_Madd_o_dma_addr_lut_15_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_s_dma_base_addr(15),
      I1 => mb_altera_core_s_dma_space_addr(15),
      O => mb_altera_core_Madd_o_dma_addr_lut(15)
    );
  mb_altera_core_Madd_o_dma_addr_xor_14_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(13),
      LI => mb_altera_core_Madd_o_dma_addr_lut(14),
      O => dma_addr(0, 14)
    );
  mb_altera_core_Madd_o_dma_addr_cy_14_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(13),
      DI => mb_altera_core_s_dma_base_addr(14),
      S => mb_altera_core_Madd_o_dma_addr_lut(14),
      O => mb_altera_core_Madd_o_dma_addr_cy(14)
    );
  mb_altera_core_Madd_o_dma_addr_lut_14_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_s_dma_base_addr(14),
      I1 => mb_altera_core_s_dma_space_addr(14),
      O => mb_altera_core_Madd_o_dma_addr_lut(14)
    );
  mb_altera_core_Madd_o_dma_addr_xor_13_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(12),
      LI => mb_altera_core_Madd_o_dma_addr_lut(13),
      O => dma_addr(0, 13)
    );
  mb_altera_core_Madd_o_dma_addr_cy_13_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(12),
      DI => mb_altera_core_s_dma_base_addr(13),
      S => mb_altera_core_Madd_o_dma_addr_lut(13),
      O => mb_altera_core_Madd_o_dma_addr_cy(13)
    );
  mb_altera_core_Madd_o_dma_addr_lut_13_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_s_dma_base_addr(13),
      I1 => mb_altera_core_s_dma_space_addr(13),
      O => mb_altera_core_Madd_o_dma_addr_lut(13)
    );
  mb_altera_core_Madd_o_dma_addr_xor_12_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(11),
      LI => mb_altera_core_Madd_o_dma_addr_lut(12),
      O => dma_addr(0, 12)
    );
  mb_altera_core_Madd_o_dma_addr_cy_12_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(11),
      DI => mb_altera_core_s_dma_base_addr(12),
      S => mb_altera_core_Madd_o_dma_addr_lut(12),
      O => mb_altera_core_Madd_o_dma_addr_cy(12)
    );
  mb_altera_core_Madd_o_dma_addr_lut_12_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_s_dma_base_addr(12),
      I1 => mb_altera_core_s_dma_space_addr(12),
      O => mb_altera_core_Madd_o_dma_addr_lut(12)
    );
  mb_altera_core_Madd_o_dma_addr_xor_11_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(10),
      LI => mb_altera_core_Madd_o_dma_addr_lut(11),
      O => dma_addr(0, 11)
    );
  mb_altera_core_Madd_o_dma_addr_cy_11_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(10),
      DI => mb_altera_core_s_dma_base_addr(11),
      S => mb_altera_core_Madd_o_dma_addr_lut(11),
      O => mb_altera_core_Madd_o_dma_addr_cy(11)
    );
  mb_altera_core_Madd_o_dma_addr_lut_11_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_s_dma_base_addr(11),
      I1 => mb_altera_core_s_dma_space_addr(11),
      O => mb_altera_core_Madd_o_dma_addr_lut(11)
    );
  mb_altera_core_Madd_o_dma_addr_xor_10_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(9),
      LI => mb_altera_core_Madd_o_dma_addr_lut(10),
      O => dma_addr(0, 10)
    );
  mb_altera_core_Madd_o_dma_addr_cy_10_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(9),
      DI => mb_altera_core_s_dma_base_addr(10),
      S => mb_altera_core_Madd_o_dma_addr_lut(10),
      O => mb_altera_core_Madd_o_dma_addr_cy(10)
    );
  mb_altera_core_Madd_o_dma_addr_lut_10_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_s_dma_base_addr(10),
      I1 => mb_altera_core_s_dma_space_addr(10),
      O => mb_altera_core_Madd_o_dma_addr_lut(10)
    );
  mb_altera_core_Madd_o_dma_addr_xor_9_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(8),
      LI => mb_altera_core_Madd_o_dma_addr_lut(9),
      O => dma_addr(0, 9)
    );
  mb_altera_core_Madd_o_dma_addr_cy_9_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(8),
      DI => mb_altera_core_s_dma_base_addr(9),
      S => mb_altera_core_Madd_o_dma_addr_lut(9),
      O => mb_altera_core_Madd_o_dma_addr_cy(9)
    );
  mb_altera_core_Madd_o_dma_addr_lut_9_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_s_dma_base_addr(9),
      I1 => mb_altera_core_s_dma_space_addr(9),
      O => mb_altera_core_Madd_o_dma_addr_lut(9)
    );
  mb_altera_core_Madd_o_dma_addr_xor_8_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(7),
      LI => mb_altera_core_Madd_o_dma_addr_lut(8),
      O => dma_addr(0, 8)
    );
  mb_altera_core_Madd_o_dma_addr_cy_8_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(7),
      DI => mb_altera_core_s_dma_base_addr(8),
      S => mb_altera_core_Madd_o_dma_addr_lut(8),
      O => mb_altera_core_Madd_o_dma_addr_cy(8)
    );
  mb_altera_core_Madd_o_dma_addr_lut_8_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_s_dma_base_addr(8),
      I1 => mb_altera_core_s_dma_space_addr(8),
      O => mb_altera_core_Madd_o_dma_addr_lut(8)
    );
  mb_altera_core_Madd_o_dma_addr_xor_7_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(6),
      LI => mb_altera_core_Madd_o_dma_addr_lut(7),
      O => dma_addr(0, 7)
    );
  mb_altera_core_Madd_o_dma_addr_cy_7_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(6),
      DI => mb_altera_core_s_dma_base_addr(7),
      S => mb_altera_core_Madd_o_dma_addr_lut(7),
      O => mb_altera_core_Madd_o_dma_addr_cy(7)
    );
  mb_altera_core_Madd_o_dma_addr_lut_7_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_s_dma_base_addr(7),
      I1 => mb_altera_core_s_dma_space_addr(7),
      O => mb_altera_core_Madd_o_dma_addr_lut(7)
    );
  mb_altera_core_Madd_o_dma_addr_xor_6_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(5),
      LI => mb_altera_core_Madd_o_dma_addr_lut(6),
      O => dma_addr(0, 6)
    );
  mb_altera_core_Madd_o_dma_addr_cy_6_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(5),
      DI => mb_altera_core_s_dma_base_addr(6),
      S => mb_altera_core_Madd_o_dma_addr_lut(6),
      O => mb_altera_core_Madd_o_dma_addr_cy(6)
    );
  mb_altera_core_Madd_o_dma_addr_lut_6_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_s_dma_base_addr(6),
      I1 => mb_altera_core_s_dma_space_addr(6),
      O => mb_altera_core_Madd_o_dma_addr_lut(6)
    );
  mb_altera_core_Madd_o_dma_addr_xor_5_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(4),
      LI => mb_altera_core_Madd_o_dma_addr_lut(5),
      O => dma_addr(0, 5)
    );
  mb_altera_core_Madd_o_dma_addr_cy_5_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(4),
      DI => mb_altera_core_s_dma_base_addr(5),
      S => mb_altera_core_Madd_o_dma_addr_lut(5),
      O => mb_altera_core_Madd_o_dma_addr_cy(5)
    );
  mb_altera_core_Madd_o_dma_addr_lut_5_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_s_dma_base_addr(5),
      I1 => mb_altera_core_s_dma_space_addr(5),
      O => mb_altera_core_Madd_o_dma_addr_lut(5)
    );
  mb_altera_core_Madd_o_dma_addr_xor_4_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(3),
      LI => mb_altera_core_Madd_o_dma_addr_lut(4),
      O => dma_addr(0, 4)
    );
  mb_altera_core_Madd_o_dma_addr_cy_4_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(3),
      DI => mb_altera_core_s_dma_base_addr(4),
      S => mb_altera_core_Madd_o_dma_addr_lut(4),
      O => mb_altera_core_Madd_o_dma_addr_cy(4)
    );
  mb_altera_core_Madd_o_dma_addr_lut_4_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_s_dma_base_addr(4),
      I1 => mb_altera_core_s_dma_space_addr(4),
      O => mb_altera_core_Madd_o_dma_addr_lut(4)
    );
  mb_altera_core_Madd_o_dma_addr_xor_3_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(2),
      LI => mb_altera_core_Madd_o_dma_addr_lut(3),
      O => dma_addr(0, 3)
    );
  mb_altera_core_Madd_o_dma_addr_cy_3_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(2),
      DI => mb_altera_core_s_dma_base_addr(3),
      S => mb_altera_core_Madd_o_dma_addr_lut(3),
      O => mb_altera_core_Madd_o_dma_addr_cy(3)
    );
  mb_altera_core_Madd_o_dma_addr_lut_3_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_s_dma_base_addr(3),
      I1 => mb_altera_core_s_dma_space_addr(3),
      O => mb_altera_core_Madd_o_dma_addr_lut(3)
    );
  mb_altera_core_Madd_o_dma_addr_xor_2_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(1),
      LI => mb_altera_core_Madd_o_dma_addr_lut(2),
      O => dma_addr(0, 2)
    );
  mb_altera_core_Madd_o_dma_addr_cy_2_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(1),
      DI => mb_altera_core_s_dma_base_addr(2),
      S => mb_altera_core_Madd_o_dma_addr_lut(2),
      O => mb_altera_core_Madd_o_dma_addr_cy(2)
    );
  mb_altera_core_Madd_o_dma_addr_lut_2_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_s_dma_base_addr(2),
      I1 => mb_altera_core_s_dma_space_addr(2),
      O => mb_altera_core_Madd_o_dma_addr_lut(2)
    );
  mb_altera_core_Madd_o_dma_addr_xor_1_Q : XORCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(0),
      LI => mb_altera_core_Madd_o_dma_addr_lut(1),
      O => dma_addr(0, 1)
    );
  mb_altera_core_Madd_o_dma_addr_cy_1_Q : MUXCY
    port map (
      CI => mb_altera_core_Madd_o_dma_addr_cy(0),
      DI => mb_altera_core_s_dma_base_addr(1),
      S => mb_altera_core_Madd_o_dma_addr_lut(1),
      O => mb_altera_core_Madd_o_dma_addr_cy(1)
    );
  mb_altera_core_Madd_o_dma_addr_lut_1_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_s_dma_base_addr(1),
      I1 => mb_altera_core_s_dma_space_addr(1),
      O => mb_altera_core_Madd_o_dma_addr_lut(1)
    );
  mb_altera_core_Madd_o_dma_addr_xor_0_Q : XORCY
    port map (
      CI => ea(15),
      LI => mb_altera_core_Madd_o_dma_addr_lut(0),
      O => dma_addr(0, 0)
    );
  mb_altera_core_Madd_o_dma_addr_cy_0_Q : MUXCY
    port map (
      CI => ea(15),
      DI => mb_altera_core_s_dma_base_addr(0),
      S => mb_altera_core_Madd_o_dma_addr_lut(0),
      O => mb_altera_core_Madd_o_dma_addr_cy(0)
    );
  mb_altera_core_Madd_o_dma_addr_lut_0_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_s_dma_base_addr(0),
      I1 => mb_altera_core_s_dma_space_addr(0),
      O => mb_altera_core_Madd_o_dma_addr_lut(0)
    );
  mb_altera_core_Write_fifo_wr_Cnt_9 : FDRE
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_fifo_wr_Cnt_not0001,
      D => mb_altera_core_Result(9),
      R => mb_altera_core_s_altera_fifo_rst_2549,
      Q => mb_altera_core_Write_fifo_wr_Cnt(9)
    );
  mb_altera_core_Write_fifo_wr_Cnt_8 : FDRE
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_fifo_wr_Cnt_not0001,
      D => mb_altera_core_Result(8),
      R => mb_altera_core_s_altera_fifo_rst_2549,
      Q => mb_altera_core_Write_fifo_wr_Cnt(8)
    );
  mb_altera_core_Write_fifo_wr_Cnt_7 : FDRE
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_fifo_wr_Cnt_not0001,
      D => mb_altera_core_Result(7),
      R => mb_altera_core_s_altera_fifo_rst_2549,
      Q => mb_altera_core_Write_fifo_wr_Cnt(7)
    );
  mb_altera_core_Write_fifo_wr_Cnt_6 : FDRE
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_fifo_wr_Cnt_not0001,
      D => mb_altera_core_Result(6),
      R => mb_altera_core_s_altera_fifo_rst_2549,
      Q => mb_altera_core_Write_fifo_wr_Cnt(6)
    );
  mb_altera_core_Write_fifo_wr_Cnt_5 : FDRE
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_fifo_wr_Cnt_not0001,
      D => mb_altera_core_Result(5),
      R => mb_altera_core_s_altera_fifo_rst_2549,
      Q => mb_altera_core_Write_fifo_wr_Cnt(5)
    );
  mb_altera_core_Write_fifo_wr_Cnt_4 : FDRE
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_fifo_wr_Cnt_not0001,
      D => mb_altera_core_Result(4),
      R => mb_altera_core_s_altera_fifo_rst_2549,
      Q => mb_altera_core_Write_fifo_wr_Cnt(4)
    );
  mb_altera_core_Write_fifo_wr_Cnt_3 : FDRE
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_fifo_wr_Cnt_not0001,
      D => mb_altera_core_Result(3),
      R => mb_altera_core_s_altera_fifo_rst_2549,
      Q => mb_altera_core_Write_fifo_wr_Cnt(3)
    );
  mb_altera_core_Write_fifo_wr_Cnt_2 : FDRE
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_fifo_wr_Cnt_not0001,
      D => mb_altera_core_Result(2),
      R => mb_altera_core_s_altera_fifo_rst_2549,
      Q => mb_altera_core_Write_fifo_wr_Cnt(2)
    );
  mb_altera_core_Write_fifo_wr_Cnt_1 : FDRE
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_fifo_wr_Cnt_not0001,
      D => mb_altera_core_Result(1),
      R => mb_altera_core_s_altera_fifo_rst_2549,
      Q => mb_altera_core_Write_fifo_wr_Cnt(1)
    );
  mb_altera_core_Write_fifo_wr_Cnt_0 : FDRE
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_fifo_wr_Cnt_not0001,
      D => mb_altera_core_Result(0),
      R => mb_altera_core_s_altera_fifo_rst_2549,
      Q => mb_altera_core_Write_fifo_wr_Cnt(0)
    );
  mb_altera_core_version_rd_toggle : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_ver_rd_3018,
      D => mb_altera_core_version_N01,
      Q => mb_altera_core_version_rd_toggle_3023
    );
  mb_altera_core_version_o_data_0 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_version_N01,
      Q => mb_altera_core_version_o_data_0_Q
    );
  mb_altera_core_version_o_data_2 : FD
    port map (
      C => emif_clk,
      D => mb_altera_core_version_rd_toggle_3023,
      Q => mb_altera_core_version_o_data_2_Q
    );
  mb_altera_core_s_dma_irq : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_state_cmp_eq0003,
      Q => mb_altera_core_s_dma_irq_2663
    );
  mb_altera_core_s_i_altera_int_r2 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_i_altera_int_r1_3016,
      Q => mb_altera_core_s_i_altera_int_r2_3017
    );
  mb_altera_core_s_altera_rdfifo_din_29 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_Read_Fifo_Data_in_29_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(29)
    );
  mb_altera_core_s_altera_rdfifo_din_28 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_Read_Fifo_Data_in_28_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(28)
    );
  mb_altera_core_s_altera_rdfifo_din_27 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_Read_Fifo_Data_in_27_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(27)
    );
  mb_altera_core_s_altera_rdfifo_din_26 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_Read_Fifo_Data_in_26_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(26)
    );
  mb_altera_core_s_altera_rdfifo_din_31 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_Read_Fifo_Data_in_31_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(31)
    );
  mb_altera_core_s_altera_rdfifo_din_25 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_Read_Fifo_Data_in_25_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(25)
    );
  mb_altera_core_s_altera_rdfifo_din_30 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_Read_Fifo_Data_in_30_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(30)
    );
  mb_altera_core_s_altera_rdfifo_din_19 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_Read_Fifo_Data_in_19_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(19)
    );
  mb_altera_core_s_altera_rdfifo_din_24 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_Read_Fifo_Data_in_24_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(24)
    );
  mb_altera_core_s_altera_rdfifo_din_18 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_Read_Fifo_Data_in_18_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(18)
    );
  mb_altera_core_s_altera_rdfifo_din_23 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_Read_Fifo_Data_in_23_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(23)
    );
  mb_altera_core_s_altera_rdfifo_din_17 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_Read_Fifo_Data_in_17_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(17)
    );
  mb_altera_core_s_altera_rdfifo_din_22 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_Read_Fifo_Data_in_22_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(22)
    );
  mb_altera_core_s_altera_rdfifo_din_16 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_Read_Fifo_Data_in_16_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(16)
    );
  mb_altera_core_s_altera_rdfifo_din_21 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_Read_Fifo_Data_in_21_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(21)
    );
  mb_altera_core_s_altera_rdfifo_din_15 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_Read_Fifo_Data_in_15_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(15)
    );
  mb_altera_core_altera_ncs : FDSE
    port map (
      C => emif_clk,
      CE => mb_altera_core_altera_ncs_and0000,
      D => inst_emif_iface_edi_r(0),
      S => altera_flash_enable_inv,
      Q => mb_altera_core_altera_ncs_2226
    );
  mb_altera_core_s_altera_rdfifo_din_20 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_Read_Fifo_Data_in_20_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(20)
    );
  mb_altera_core_s_altera_rdfifo_din_14 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_Read_Fifo_Data_in_14_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(14)
    );
  mb_altera_core_o_altera_data_out_9 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_9_xor0000,
      Q => mb_altera_core_o_altera_data_out(9)
    );
  mb_altera_core_s_altera_rdfifo_din_13 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_Read_Fifo_Data_in_13_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(13)
    );
  mb_altera_core_o_altera_data_out_8 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_8_xor0000,
      Q => mb_altera_core_o_altera_data_out(8)
    );
  mb_altera_core_s_altera_rdfifo_din_12 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_Read_Fifo_Data_in_12_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(12)
    );
  mb_altera_core_altera_nce : FDSE
    port map (
      C => emif_clk,
      CE => mb_altera_core_altera_nce_and0000,
      D => inst_emif_iface_edi_r(0),
      S => altera_flash_enable_inv,
      Q => mb_altera_core_altera_nce_2222
    );
  mb_altera_core_s_altera_rdfifo_din_11 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_Read_Fifo_Data_in_11_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(11)
    );
  mb_altera_core_o_altera_data_out_6 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_6_xor0000,
      Q => mb_altera_core_o_altera_data_out(6)
    );
  mb_altera_core_o_altera_data_out_7 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_7_xor0000,
      Q => mb_altera_core_o_altera_data_out(7)
    );
  mb_altera_core_s_altera_rdfifo_din_10 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_Read_Fifo_Data_in_10_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(10)
    );
  mb_altera_core_o_altera_data_out_5 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_5_xor0000,
      Q => mb_altera_core_o_altera_data_out(5)
    );
  mb_altera_core_o_altera_data_out_4 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_4_xor0000,
      Q => mb_altera_core_o_altera_data_out(4)
    );
  mb_altera_core_o_altera_data_out_3 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_3_xor0000,
      Q => mb_altera_core_o_altera_data_out(3)
    );
  mb_altera_core_o_altera_data_out_2 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_2_xor0000,
      Q => mb_altera_core_o_altera_data_out(2)
    );
  mb_altera_core_o_altera_data_out_1 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_1_xor0000,
      Q => mb_altera_core_o_altera_data_out(1)
    );
  mb_altera_core_Read_Fifo_Data_in_29 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_Read_Fifo_Data_in_29_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(29)
    );
  mb_altera_core_Read_Fifo_Data_in_28 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_Read_Fifo_Data_in_28_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(28)
    );
  mb_altera_core_Read_Fifo_Data_in_27 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_Read_Fifo_Data_in_27_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(27)
    );
  mb_altera_core_Read_Fifo_Data_in_31 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_Read_Fifo_Data_in_31_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(31)
    );
  mb_altera_core_Read_Fifo_Data_in_26 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_Read_Fifo_Data_in_26_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(26)
    );
  mb_altera_core_Read_Fifo_Data_in_25 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_Read_Fifo_Data_in_25_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(25)
    );
  mb_altera_core_Read_Fifo_Data_in_30 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_Read_Fifo_Data_in_30_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(30)
    );
  mb_altera_core_Read_Fifo_Data_in_19 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_Read_Fifo_Data_in_19_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(19)
    );
  mb_altera_core_Read_Fifo_Data_in_24 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_Read_Fifo_Data_in_24_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(24)
    );
  mb_altera_core_Read_Fifo_Data_in_18 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_Read_Fifo_Data_in_18_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(18)
    );
  mb_altera_core_Read_Fifo_Data_in_23 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_Read_Fifo_Data_in_23_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(23)
    );
  mb_altera_core_Read_Fifo_Data_in_22 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_Read_Fifo_Data_in_22_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(22)
    );
  mb_altera_core_Read_Fifo_Data_in_16 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_Read_Fifo_Data_in_16_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(16)
    );
  mb_altera_core_Read_Fifo_Data_in_17 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_Read_Fifo_Data_in_17_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(17)
    );
  mb_altera_core_Read_Fifo_Data_in_21 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_Read_Fifo_Data_in_21_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(21)
    );
  mb_altera_core_Read_Fifo_Data_in_15 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_Read_Fifo_Data_in_15_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(15)
    );
  mb_altera_core_Read_Fifo_Data_in_20 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_Read_Fifo_Data_in_20_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(20)
    );
  mb_altera_core_Read_Fifo_Data_in_14 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_Read_Fifo_Data_in_14_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(14)
    );
  mb_altera_core_Read_Fifo_Data_in_13 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_Read_Fifo_Data_in_13_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(13)
    );
  mb_altera_core_s_err_invalid_rd : FDSE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_err_clr_3012,
      D => ea(15),
      S => mb_altera_core_s_err_invalid_rd_and0000,
      Q => mb_altera_core_s_err_invalid_rd_3014
    );
  mb_altera_core_o_altera_data_out_29 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_29_xor0000,
      Q => mb_altera_core_o_altera_data_out(29)
    );
  mb_altera_core_o_altera_data_out_28 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_28_xor0000,
      Q => mb_altera_core_o_altera_data_out(28)
    );
  mb_altera_core_Read_Fifo_Data_in_12 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_Read_Fifo_Data_in_12_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(12)
    );
  mb_altera_core_o_altera_data_out_27 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_27_xor0000,
      Q => mb_altera_core_o_altera_data_out(27)
    );
  mb_altera_core_Read_Fifo_Data_in_11 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_Read_Fifo_Data_in_11_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(11)
    );
  mb_altera_core_o_altera_data_out_26 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_26_xor0000,
      Q => mb_altera_core_o_altera_data_out(26)
    );
  mb_altera_core_Read_Fifo_Data_in_10 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_Read_Fifo_Data_in_10_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(10)
    );
  mb_altera_core_o_altera_data_out_25 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_25_xor0000,
      Q => mb_altera_core_o_altera_data_out(25)
    );
  mb_altera_core_o_altera_data_out_31 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_31_xor0000,
      Q => mb_altera_core_o_altera_data_out(31)
    );
  mb_altera_core_o_altera_data_out_30 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_30_xor0000,
      Q => mb_altera_core_o_altera_data_out(30)
    );
  mb_altera_core_o_altera_data_out_19 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_19_xor0000,
      Q => mb_altera_core_o_altera_data_out(19)
    );
  mb_altera_core_o_altera_data_out_24 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_24_xor0000,
      Q => mb_altera_core_o_altera_data_out(24)
    );
  mb_altera_core_Read_Fifo_Data_in_9 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_s_altera_rdfifo_din_9_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(9)
    );
  mb_altera_core_o_altera_data_out_18 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_18_xor0000,
      Q => mb_altera_core_o_altera_data_out(18)
    );
  mb_altera_core_o_altera_data_out_23 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_23_xor0000,
      Q => mb_altera_core_o_altera_data_out(23)
    );
  mb_altera_core_Read_Fifo_Data_in_8 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_s_altera_rdfifo_din_8_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(8)
    );
  mb_altera_core_o_altera_data_out_17 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_17_xor0000,
      Q => mb_altera_core_o_altera_data_out(17)
    );
  mb_altera_core_o_altera_data_out_22 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_22_xor0000,
      Q => mb_altera_core_o_altera_data_out(22)
    );
  mb_altera_core_Read_Fifo_Data_in_7 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_s_altera_rdfifo_din_7_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(7)
    );
  mb_altera_core_o_altera_data_out_16 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_16_xor0000,
      Q => mb_altera_core_o_altera_data_out(16)
    );
  mb_altera_core_o_altera_data_out_21 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_21_xor0000,
      Q => mb_altera_core_o_altera_data_out(21)
    );
  mb_altera_core_o_altera_data_out_15 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_15_xor0000,
      Q => mb_altera_core_o_altera_data_out(15)
    );
  mb_altera_core_o_altera_data_out_20 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_20_xor0000,
      Q => mb_altera_core_o_altera_data_out(20)
    );
  mb_altera_core_Read_Fifo_Data_in_6 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_s_altera_rdfifo_din_6_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(6)
    );
  mb_altera_core_Read_Fifo_Data_in_5 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_s_altera_rdfifo_din_5_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(5)
    );
  mb_altera_core_o_altera_data_out_14 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_14_xor0000,
      Q => mb_altera_core_o_altera_data_out(14)
    );
  mb_altera_core_Read_Fifo_Data_in_4 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_s_altera_rdfifo_din_4_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(4)
    );
  mb_altera_core_o_altera_data_out_13 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_13_xor0000,
      Q => mb_altera_core_o_altera_data_out(13)
    );
  mb_altera_core_Read_Fifo_Data_in_3 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_s_altera_rdfifo_din_3_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(3)
    );
  mb_altera_core_o_altera_data_out_12 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_12_xor0000,
      Q => mb_altera_core_o_altera_data_out(12)
    );
  mb_altera_core_Read_Fifo_Data_in_2 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_s_altera_rdfifo_din_2_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(2)
    );
  mb_altera_core_o_altera_data_out_11 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_11_xor0000,
      Q => mb_altera_core_o_altera_data_out(11)
    );
  mb_altera_core_Read_Fifo_Data_in_1 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => mb_altera_core_s_altera_rdfifo_din_1_xor0000,
      Q => mb_altera_core_Read_Fifo_Data_in(1)
    );
  mb_altera_core_o_altera_data_out_10 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_o_altera_data_out_10_xor0000,
      Q => mb_altera_core_o_altera_data_out(10)
    );
  mb_altera_core_s_altera_rdfifo_din_9 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_s_altera_rdfifo_din_9_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(9)
    );
  mb_altera_core_s_altera_rdfifo_din_8 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_s_altera_rdfifo_din_8_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(8)
    );
  mb_altera_core_s_altera_rdfifo_din_7 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_s_altera_rdfifo_din_7_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(7)
    );
  mb_altera_core_altera_ncon : FDSE
    port map (
      C => emif_clk,
      CE => mb_altera_core_altera_ncon_and0000,
      D => inst_emif_iface_edi_r(0),
      S => altera_flash_enable_inv,
      Q => mb_altera_core_altera_ncon_2224
    );
  mb_altera_core_s_altera_rdfifo_din_6 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_s_altera_rdfifo_din_6_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(6)
    );
  mb_altera_core_s_altera_rdfifo_din_5 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_s_altera_rdfifo_din_5_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(5)
    );
  mb_altera_core_s_altera_rdfifo_din_4 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_s_altera_rdfifo_din_4_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(4)
    );
  mb_altera_core_s_altera_rdfifo_din_3 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_s_altera_rdfifo_din_3_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(3)
    );
  mb_altera_core_s_altera_rdfifo_din_2 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_s_altera_rdfifo_din_2_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(2)
    );
  mb_altera_core_s_altera_rdfifo_din_1 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_s_altera_rdfifo_din_1_xor0000,
      Q => mb_altera_core_s_altera_rdfifo_din(1)
    );
  mb_altera_core_Write_Fifo_Data_in_9 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(9),
      Q => mb_altera_core_Write_Fifo_Data_in(9)
    );
  mb_altera_core_Write_Fifo_Data_in_7 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(7),
      Q => mb_altera_core_Write_Fifo_Data_in(7)
    );
  mb_altera_core_Write_Fifo_Data_in_8 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(8),
      Q => mb_altera_core_Write_Fifo_Data_in(8)
    );
  mb_altera_core_Write_Fifo_Data_in_6 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(6),
      Q => mb_altera_core_Write_Fifo_Data_in(6)
    );
  mb_altera_core_Write_Fifo_Data_in_5 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(5),
      Q => mb_altera_core_Write_Fifo_Data_in(5)
    );
  mb_altera_core_flash_enable : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_flash_enable_and0000,
      D => inst_emif_iface_edi_r(0),
      Q => mb_altera_core_flash_enable_2235
    );
  mb_altera_core_Write_Fifo_Data_in_4 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(4),
      Q => mb_altera_core_Write_Fifo_Data_in(4)
    );
  mb_altera_core_Write_Fifo_Data_in_3 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(3),
      Q => mb_altera_core_Write_Fifo_Data_in(3)
    );
  mb_altera_core_Write_Fifo_Data_in_2 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(2),
      Q => mb_altera_core_Write_Fifo_Data_in(2)
    );
  mb_altera_core_s_altera_int_en : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_err_clr_and0000,
      D => inst_emif_iface_edi_r(2),
      Q => mb_altera_core_s_altera_int_en_2553
    );
  mb_altera_core_Write_Fifo_Data_in_1 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(1),
      Q => mb_altera_core_Write_Fifo_Data_in(1)
    );
  mb_altera_core_Write_Fifo_Data_in_0 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(0),
      Q => mb_altera_core_Write_Fifo_Data_in(0)
    );
  mb_altera_core_s_dma_space_mask_31 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(31),
      Q => mb_altera_core_s_dma_space_mask(31)
    );
  mb_altera_core_s_dma_space_mask_30 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(30),
      Q => mb_altera_core_s_dma_space_mask(30)
    );
  mb_altera_core_s_dma_space_mask_29 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(29),
      Q => mb_altera_core_s_dma_space_mask(29)
    );
  mb_altera_core_s_dma_space_mask_28 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(28),
      Q => mb_altera_core_s_dma_space_mask(28)
    );
  mb_altera_core_s_dma_space_mask_27 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(27),
      Q => mb_altera_core_s_dma_space_mask(27)
    );
  mb_altera_core_s_dma_space_mask_26 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(26),
      Q => mb_altera_core_s_dma_space_mask(26)
    );
  mb_altera_core_s_dma_space_mask_25 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(25),
      Q => mb_altera_core_s_dma_space_mask(25)
    );
  mb_altera_core_s_dma_space_mask_24 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(24),
      Q => mb_altera_core_s_dma_space_mask(24)
    );
  mb_altera_core_s_dma_space_mask_23 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(23),
      Q => mb_altera_core_s_dma_space_mask(23)
    );
  mb_altera_core_s_dma_space_mask_22 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(22),
      Q => mb_altera_core_s_dma_space_mask(22)
    );
  mb_altera_core_s_dma_space_mask_21 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(21),
      Q => mb_altera_core_s_dma_space_mask(21)
    );
  mb_altera_core_s_dma_space_mask_20 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(20),
      Q => mb_altera_core_s_dma_space_mask(20)
    );
  mb_altera_core_s_dma_space_mask_19 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(19),
      Q => mb_altera_core_s_dma_space_mask(19)
    );
  mb_altera_core_s_dma_space_mask_18 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(18),
      Q => mb_altera_core_s_dma_space_mask(18)
    );
  mb_altera_core_s_dma_space_mask_17 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(17),
      Q => mb_altera_core_s_dma_space_mask(17)
    );
  mb_altera_core_s_dma_space_mask_16 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(16),
      Q => mb_altera_core_s_dma_space_mask(16)
    );
  mb_altera_core_s_dma_space_mask_15 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(15),
      Q => mb_altera_core_s_dma_space_mask(15)
    );
  mb_altera_core_s_dma_space_mask_14 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(14),
      Q => mb_altera_core_s_dma_space_mask(14)
    );
  mb_altera_core_s_dma_space_mask_13 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(13),
      Q => mb_altera_core_s_dma_space_mask(13)
    );
  mb_altera_core_s_dma_space_mask_12 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(12),
      Q => mb_altera_core_s_dma_space_mask(12)
    );
  mb_altera_core_s_dma_space_mask_11 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(11),
      Q => mb_altera_core_s_dma_space_mask(11)
    );
  mb_altera_core_s_dma_space_mask_10 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(10),
      Q => mb_altera_core_s_dma_space_mask(10)
    );
  mb_altera_core_s_dma_space_mask_9 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(9),
      Q => mb_altera_core_s_dma_space_mask(9)
    );
  mb_altera_core_s_dma_space_mask_8 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(8),
      Q => mb_altera_core_s_dma_space_mask(8)
    );
  mb_altera_core_s_dma_space_mask_7 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(7),
      Q => mb_altera_core_s_dma_space_mask(7)
    );
  mb_altera_core_s_dma_space_mask_6 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(6),
      Q => mb_altera_core_s_dma_space_mask(6)
    );
  mb_altera_core_s_dma_space_mask_5 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(5),
      Q => mb_altera_core_s_dma_space_mask(5)
    );
  mb_altera_core_s_dma_space_mask_4 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(4),
      Q => mb_altera_core_s_dma_space_mask(4)
    );
  mb_altera_core_s_dma_space_mask_3 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(3),
      Q => mb_altera_core_s_dma_space_mask(3)
    );
  mb_altera_core_s_dma_space_mask_2 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(2),
      Q => mb_altera_core_s_dma_space_mask(2)
    );
  mb_altera_core_s_dma_space_mask_1 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(1),
      Q => mb_altera_core_s_dma_space_mask(1)
    );
  mb_altera_core_s_dma_space_mask_0 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_space_mask_and0000,
      D => inst_emif_iface_edi_r(0),
      Q => mb_altera_core_s_dma_space_mask(0)
    );
  mb_altera_core_s_dma_timeout_threshold_31 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(31),
      Q => mb_altera_core_s_dma_timeout_threshold(31)
    );
  mb_altera_core_s_dma_timeout_threshold_30 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(30),
      Q => mb_altera_core_s_dma_timeout_threshold(30)
    );
  mb_altera_core_s_dma_timeout_threshold_29 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(29),
      Q => mb_altera_core_s_dma_timeout_threshold(29)
    );
  mb_altera_core_s_dma_timeout_threshold_28 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(28),
      Q => mb_altera_core_s_dma_timeout_threshold(28)
    );
  mb_altera_core_s_dma_timeout_threshold_27 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(27),
      Q => mb_altera_core_s_dma_timeout_threshold(27)
    );
  mb_altera_core_s_dma_timeout_threshold_26 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(26),
      Q => mb_altera_core_s_dma_timeout_threshold(26)
    );
  mb_altera_core_s_dma_timeout_threshold_25 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(25),
      Q => mb_altera_core_s_dma_timeout_threshold(25)
    );
  mb_altera_core_s_dma_timeout_threshold_24 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(24),
      Q => mb_altera_core_s_dma_timeout_threshold(24)
    );
  mb_altera_core_s_dma_timeout_threshold_23 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(23),
      Q => mb_altera_core_s_dma_timeout_threshold(23)
    );
  mb_altera_core_s_dma_timeout_threshold_22 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(22),
      Q => mb_altera_core_s_dma_timeout_threshold(22)
    );
  mb_altera_core_s_dma_timeout_threshold_21 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(21),
      Q => mb_altera_core_s_dma_timeout_threshold(21)
    );
  mb_altera_core_s_dma_timeout_threshold_20 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(20),
      Q => mb_altera_core_s_dma_timeout_threshold(20)
    );
  mb_altera_core_s_dma_timeout_threshold_19 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(19),
      Q => mb_altera_core_s_dma_timeout_threshold(19)
    );
  mb_altera_core_s_dma_timeout_threshold_18 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(18),
      Q => mb_altera_core_s_dma_timeout_threshold(18)
    );
  mb_altera_core_s_dma_timeout_threshold_17 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(17),
      Q => mb_altera_core_s_dma_timeout_threshold(17)
    );
  mb_altera_core_s_dma_timeout_threshold_16 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(16),
      Q => mb_altera_core_s_dma_timeout_threshold(16)
    );
  mb_altera_core_s_dma_timeout_threshold_15 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(15),
      Q => mb_altera_core_s_dma_timeout_threshold(15)
    );
  mb_altera_core_s_dma_timeout_threshold_14 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(14),
      Q => mb_altera_core_s_dma_timeout_threshold(14)
    );
  mb_altera_core_s_dma_timeout_threshold_13 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(13),
      Q => mb_altera_core_s_dma_timeout_threshold(13)
    );
  mb_altera_core_s_dma_timeout_threshold_12 : FDE
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(12),
      Q => mb_altera_core_s_dma_timeout_threshold(12)
    );
  mb_altera_core_s_dma_timeout_threshold_11 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(11),
      Q => mb_altera_core_s_dma_timeout_threshold(11)
    );
  mb_altera_core_s_dma_timeout_threshold_10 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(10),
      Q => mb_altera_core_s_dma_timeout_threshold(10)
    );
  mb_altera_core_s_dma_timeout_threshold_9 : FDE
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(9),
      Q => mb_altera_core_s_dma_timeout_threshold(9)
    );
  mb_altera_core_s_dma_timeout_threshold_8 : FDE
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(8),
      Q => mb_altera_core_s_dma_timeout_threshold(8)
    );
  mb_altera_core_s_dma_timeout_threshold_7 : FDE
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(7),
      Q => mb_altera_core_s_dma_timeout_threshold(7)
    );
  mb_altera_core_s_dma_timeout_threshold_6 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(6),
      Q => mb_altera_core_s_dma_timeout_threshold(6)
    );
  mb_altera_core_s_dma_timeout_threshold_5 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(5),
      Q => mb_altera_core_s_dma_timeout_threshold(5)
    );
  mb_altera_core_s_dma_timeout_threshold_4 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(4),
      Q => mb_altera_core_s_dma_timeout_threshold(4)
    );
  mb_altera_core_s_dma_timeout_threshold_3 : FDE
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(3),
      Q => mb_altera_core_s_dma_timeout_threshold(3)
    );
  mb_altera_core_s_dma_timeout_threshold_2 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(2),
      Q => mb_altera_core_s_dma_timeout_threshold(2)
    );
  mb_altera_core_s_dma_timeout_threshold_1 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(1),
      Q => mb_altera_core_s_dma_timeout_threshold(1)
    );
  mb_altera_core_s_dma_timeout_threshold_0 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_timeout_threshold_and0000,
      D => inst_emif_iface_edi_r(0),
      Q => mb_altera_core_s_dma_timeout_threshold(0)
    );
  mb_altera_core_Flash_Write_Reg_7 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_Flash_State_FSM_N3,
      D => mb_altera_core_Flash_Write_Reg_7_mux0000,
      Q => mb_altera_core_Flash_Write_Reg(7)
    );
  mb_altera_core_Flash_Write_Reg_6 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_Flash_State_FSM_N3,
      D => mb_altera_core_Flash_Write_Reg_6_mux0000,
      Q => mb_altera_core_Flash_Write_Reg(6)
    );
  mb_altera_core_ver_rd : FDR
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => Mtrien_io_altera_dsp_mux0000_norst,
      R => mb_altera_core_ver_rd_not0001,
      Q => mb_altera_core_ver_rd_3018
    );
  mb_altera_core_o_altera_data_out_0 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_Write_Fifo_data_out(0),
      Q => mb_altera_core_o_altera_data_out(0)
    );
  mb_altera_core_Flash_Write_Reg_5 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_Flash_State_FSM_N3,
      D => mb_altera_core_Flash_Write_Reg_5_mux0000,
      Q => mb_altera_core_Flash_Write_Reg(5)
    );
  mb_altera_core_Flash_Write_Reg_4 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_Flash_State_FSM_N3,
      D => mb_altera_core_Flash_Write_Reg_4_mux0000,
      Q => mb_altera_core_Flash_Write_Reg(4)
    );
  mb_altera_core_Flash_Write_Reg_3 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_Flash_State_FSM_N3,
      D => mb_altera_core_Flash_Write_Reg_3_mux0000,
      Q => mb_altera_core_Flash_Write_Reg(3)
    );
  mb_altera_core_Flash_Write_Reg_2 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_Flash_State_FSM_N3,
      D => mb_altera_core_Flash_Write_Reg_2_mux0000,
      Q => mb_altera_core_Flash_Write_Reg(2)
    );
  mb_altera_core_Read_Fifo_Data_Ready : FDSE
    port map (
      C => emif_clk,
      CE => mb_altera_core_Read_Fifo_Data_Ready_and0000,
      D => ea(15),
      S => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      Q => mb_altera_core_Read_Fifo_Data_Ready_2024
    );
  mb_altera_core_Flash_Write_Reg_1 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_Flash_State_FSM_N3,
      D => mb_altera_core_Flash_Write_Reg_1_mux0000,
      Q => mb_altera_core_Flash_Write_Reg(1)
    );
  mb_altera_core_Flash_Write_Reg_0 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_Flash_State_FSM_N3,
      D => mb_altera_core_Flash_Write_Reg_0_mux0000,
      Q => mb_altera_core_Flash_Write_Reg(0)
    );
  mb_altera_core_s_altera_int_busy : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_altera_int_busy_mux0000,
      Q => mb_altera_core_s_altera_int_busy_2551
    );
  mb_altera_core_s_dma_start_addr_29 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(2),
      Q => mb_altera_core_s_dma_start_addr(29)
    );
  mb_altera_core_s_dma_start_addr_28 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(3),
      Q => mb_altera_core_s_dma_start_addr(28)
    );
  mb_altera_core_s_dma_start_addr_27 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(4),
      Q => mb_altera_core_s_dma_start_addr(27)
    );
  mb_altera_core_s_dma_start_addr_26 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(5),
      Q => mb_altera_core_s_dma_start_addr(26)
    );
  mb_altera_core_s_dma_start_addr_25 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(6),
      Q => mb_altera_core_s_dma_start_addr(25)
    );
  mb_altera_core_s_dma_start_addr_24 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(7),
      Q => mb_altera_core_s_dma_start_addr(24)
    );
  mb_altera_core_s_dma_start_addr_23 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(8),
      Q => mb_altera_core_s_dma_start_addr(23)
    );
  mb_altera_core_s_dma_start_addr_22 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(9),
      Q => mb_altera_core_s_dma_start_addr(22)
    );
  mb_altera_core_s_dma_start_addr_21 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(10),
      Q => mb_altera_core_s_dma_start_addr(21)
    );
  mb_altera_core_s_dma_start_addr_20 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(11),
      Q => mb_altera_core_s_dma_start_addr(20)
    );
  mb_altera_core_s_dma_start_addr_19 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(12),
      Q => mb_altera_core_s_dma_start_addr(19)
    );
  mb_altera_core_s_dma_start_addr_18 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(13),
      Q => mb_altera_core_s_dma_start_addr(18)
    );
  mb_altera_core_s_dma_start_addr_17 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(14),
      Q => mb_altera_core_s_dma_start_addr(17)
    );
  mb_altera_core_s_dma_start_addr_16 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(15),
      Q => mb_altera_core_s_dma_start_addr(16)
    );
  mb_altera_core_s_dma_start_addr_15 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(16),
      Q => mb_altera_core_s_dma_start_addr(15)
    );
  mb_altera_core_s_dma_start_addr_14 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(17),
      Q => mb_altera_core_s_dma_start_addr(14)
    );
  mb_altera_core_s_dma_start_addr_13 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(18),
      Q => mb_altera_core_s_dma_start_addr(13)
    );
  mb_altera_core_s_dma_start_addr_12 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(19),
      Q => mb_altera_core_s_dma_start_addr(12)
    );
  mb_altera_core_s_dma_start_addr_11 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(20),
      Q => mb_altera_core_s_dma_start_addr(11)
    );
  mb_altera_core_s_dma_start_addr_10 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(21),
      Q => mb_altera_core_s_dma_start_addr(10)
    );
  mb_altera_core_s_dma_start_addr_9 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(22),
      Q => mb_altera_core_s_dma_start_addr(9)
    );
  mb_altera_core_s_dma_start_addr_8 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(23),
      Q => mb_altera_core_s_dma_start_addr(8)
    );
  mb_altera_core_s_dma_start_addr_7 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(24),
      Q => mb_altera_core_s_dma_start_addr(7)
    );
  mb_altera_core_s_dma_start_addr_6 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(25),
      Q => mb_altera_core_s_dma_start_addr(6)
    );
  mb_altera_core_s_dma_start_addr_5 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(26),
      Q => mb_altera_core_s_dma_start_addr(5)
    );
  mb_altera_core_s_dma_start_addr_4 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(27),
      Q => mb_altera_core_s_dma_start_addr(4)
    );
  mb_altera_core_s_dma_start_addr_3 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(28),
      Q => mb_altera_core_s_dma_start_addr(3)
    );
  mb_altera_core_s_dma_start_addr_2 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(29),
      Q => mb_altera_core_s_dma_start_addr(2)
    );
  mb_altera_core_s_dma_start_addr_1 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(30),
      Q => mb_altera_core_s_dma_start_addr(1)
    );
  mb_altera_core_s_dma_start_addr_0 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_start_addr_mux0000(31),
      Q => mb_altera_core_s_dma_start_addr(0)
    );
  mb_altera_core_s_altera_rdfifo_wr_en : FDR
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => Mtrien_io_altera_dsp_mux0000_norst,
      R => mb_altera_core_s_altera_rdfifo_wr_en_not0001,
      Q => mb_altera_core_s_altera_rdfifo_wr_en_2610
    );
  mb_altera_core_s_dma_num_samples_9 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_num_samples_mux0000(0),
      Q => mb_altera_core_s_dma_num_samples(9)
    );
  mb_altera_core_s_dma_num_samples_8 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_num_samples_mux0000(1),
      Q => mb_altera_core_s_dma_num_samples(8)
    );
  mb_altera_core_s_dma_num_samples_7 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_num_samples_mux0000(2),
      Q => mb_altera_core_s_dma_num_samples(7)
    );
  mb_altera_core_s_dma_num_samples_6 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_num_samples_mux0000(3),
      Q => mb_altera_core_s_dma_num_samples(6)
    );
  mb_altera_core_s_dma_num_samples_5 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_num_samples_mux0000(4),
      Q => mb_altera_core_s_dma_num_samples(5)
    );
  mb_altera_core_s_dma_num_samples_4 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_num_samples_mux0000(5),
      Q => mb_altera_core_s_dma_num_samples(4)
    );
  mb_altera_core_s_dma_num_samples_3 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_num_samples_mux0000(6),
      Q => mb_altera_core_s_dma_num_samples(3)
    );
  mb_altera_core_s_dma_num_samples_2 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_num_samples_mux0000(7),
      Q => mb_altera_core_s_dma_num_samples(2)
    );
  mb_altera_core_s_dma_num_samples_1 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_num_samples_mux0000(8),
      Q => mb_altera_core_s_dma_num_samples(1)
    );
  mb_altera_core_s_dma_num_samples_0 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_num_samples_mux0000(9),
      Q => mb_altera_core_s_dma_num_samples(0)
    );
  mb_altera_core_s_dma_base_addr_25 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_base_addr_and0000,
      D => inst_emif_iface_edi_r(27),
      Q => mb_altera_core_s_dma_base_addr(25)
    );
  mb_altera_core_s_dma_base_addr_24 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_base_addr_and0000,
      D => inst_emif_iface_edi_r(26),
      Q => mb_altera_core_s_dma_base_addr(24)
    );
  mb_altera_core_s_dma_base_addr_23 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_base_addr_and0000,
      D => inst_emif_iface_edi_r(25),
      Q => mb_altera_core_s_dma_base_addr(23)
    );
  mb_altera_core_s_dma_base_addr_22 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_base_addr_and0000,
      D => inst_emif_iface_edi_r(24),
      Q => mb_altera_core_s_dma_base_addr(22)
    );
  mb_altera_core_s_dma_base_addr_21 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_base_addr_and0000,
      D => inst_emif_iface_edi_r(23),
      Q => mb_altera_core_s_dma_base_addr(21)
    );
  mb_altera_core_s_dma_base_addr_20 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_base_addr_and0000,
      D => inst_emif_iface_edi_r(22),
      Q => mb_altera_core_s_dma_base_addr(20)
    );
  mb_altera_core_s_dma_base_addr_19 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_base_addr_and0000,
      D => inst_emif_iface_edi_r(21),
      Q => mb_altera_core_s_dma_base_addr(19)
    );
  mb_altera_core_s_dma_base_addr_18 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_base_addr_and0000,
      D => inst_emif_iface_edi_r(20),
      Q => mb_altera_core_s_dma_base_addr(18)
    );
  mb_altera_core_s_dma_base_addr_17 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_base_addr_and0000,
      D => inst_emif_iface_edi_r(19),
      Q => mb_altera_core_s_dma_base_addr(17)
    );
  mb_altera_core_s_dma_base_addr_16 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_base_addr_and0000,
      D => inst_emif_iface_edi_r(18),
      Q => mb_altera_core_s_dma_base_addr(16)
    );
  mb_altera_core_s_dma_base_addr_15 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_base_addr_and0000,
      D => inst_emif_iface_edi_r(17),
      Q => mb_altera_core_s_dma_base_addr(15)
    );
  mb_altera_core_s_dma_base_addr_14 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_base_addr_and0000,
      D => inst_emif_iface_edi_r(16),
      Q => mb_altera_core_s_dma_base_addr(14)
    );
  mb_altera_core_s_dma_base_addr_13 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_base_addr_and0000,
      D => inst_emif_iface_edi_r(15),
      Q => mb_altera_core_s_dma_base_addr(13)
    );
  mb_altera_core_s_dma_base_addr_12 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_base_addr_and0000,
      D => inst_emif_iface_edi_r(14),
      Q => mb_altera_core_s_dma_base_addr(12)
    );
  mb_altera_core_s_dma_base_addr_11 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_base_addr_and0000,
      D => inst_emif_iface_edi_r(13),
      Q => mb_altera_core_s_dma_base_addr(11)
    );
  mb_altera_core_s_dma_base_addr_10 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_base_addr_and0000,
      D => inst_emif_iface_edi_r(12),
      Q => mb_altera_core_s_dma_base_addr(10)
    );
  mb_altera_core_s_dma_base_addr_9 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_base_addr_and0000,
      D => inst_emif_iface_edi_r(11),
      Q => mb_altera_core_s_dma_base_addr(9)
    );
  mb_altera_core_s_dma_base_addr_8 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_base_addr_and0000,
      D => inst_emif_iface_edi_r(10),
      Q => mb_altera_core_s_dma_base_addr(8)
    );
  mb_altera_core_s_dma_base_addr_7 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_base_addr_and0000,
      D => inst_emif_iface_edi_r(9),
      Q => mb_altera_core_s_dma_base_addr(7)
    );
  mb_altera_core_s_dma_base_addr_6 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_base_addr_and0000,
      D => inst_emif_iface_edi_r(8),
      Q => mb_altera_core_s_dma_base_addr(6)
    );
  mb_altera_core_s_dma_base_addr_5 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_base_addr_and0000,
      D => inst_emif_iface_edi_r(7),
      Q => mb_altera_core_s_dma_base_addr(5)
    );
  mb_altera_core_s_dma_base_addr_4 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_base_addr_and0000,
      D => inst_emif_iface_edi_r(6),
      Q => mb_altera_core_s_dma_base_addr(4)
    );
  mb_altera_core_s_dma_base_addr_3 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_base_addr_and0000,
      D => inst_emif_iface_edi_r(5),
      Q => mb_altera_core_s_dma_base_addr(3)
    );
  mb_altera_core_s_dma_base_addr_2 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_base_addr_and0000,
      D => inst_emif_iface_edi_r(4),
      Q => mb_altera_core_s_dma_base_addr(2)
    );
  mb_altera_core_s_dma_base_addr_1 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_base_addr_and0000,
      D => inst_emif_iface_edi_r(3),
      Q => mb_altera_core_s_dma_base_addr(1)
    );
  mb_altera_core_s_dma_base_addr_0 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_dma_base_addr_and0000,
      D => inst_emif_iface_edi_r(2),
      Q => mb_altera_core_s_dma_base_addr(0)
    );
  mb_altera_core_o_altera_data_out_32 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_Write_Fifo_data_out(32),
      Q => mb_altera_core_o_altera_data_out(32)
    );
  mb_altera_core_s_dma_irq_clear : FDR
    port map (
      C => emif_clk,
      D => Mtrien_io_altera_dsp_mux0000_norst,
      R => mb_altera_core_s_dma_irq_clear_not0001,
      Q => mb_altera_core_s_dma_irq_clear_2664
    );
  mb_altera_core_Flash_Cnt_2 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_Flash_Cnt_mux0000(0),
      Q => mb_altera_core_Flash_Cnt(2)
    );
  mb_altera_core_Flash_Cnt_1 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_Flash_Cnt_mux0000(1),
      Q => mb_altera_core_Flash_Cnt(1)
    );
  mb_altera_core_Flash_Cnt_0 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_Flash_Cnt_mux0000(2),
      Q => mb_altera_core_Flash_Cnt(0)
    );
  mb_altera_core_s_altera_write_busy : FDR
    generic map(
      INIT => '0'
    )
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => Mtrien_io_altera_dsp_mux0000_norst,
      R => mb_altera_core_s_altera_write_busy_not0001,
      Q => mb_altera_core_s_altera_write_busy_2612
    );
  mb_altera_core_s_altera_fifo_rst : FDR
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => Mtrien_io_altera_dsp_mux0000_norst,
      R => mb_altera_core_s_altera_fifo_rst_not0001_2550,
      Q => mb_altera_core_s_altera_fifo_rst_2549
    );
  mb_altera_core_Read_Fifo_Data_in_0 : FDE
    port map (
      C => o_altera_Clk_OBUF_3025,
      CE => mb_altera_core_Read_Fifo_Data_in_0_and0000,
      D => N438,
      Q => mb_altera_core_Read_Fifo_Data_in(0)
    );
  mb_altera_core_Write_Fifo_Data_in_29 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(29),
      Q => mb_altera_core_Write_Fifo_Data_in(29)
    );
  mb_altera_core_s_dma_en : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_err_clr_and0000,
      D => inst_emif_iface_edi_r(0),
      Q => mb_altera_core_s_dma_en_2662
    );
  mb_altera_core_Write_Fifo_Data_in_28 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(28),
      Q => mb_altera_core_Write_Fifo_Data_in(28)
    );
  mb_altera_core_s_dma_timeout_cntr_31 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(0),
      Q => mb_altera_core_s_dma_timeout_cntr(31)
    );
  mb_altera_core_s_dma_timeout_cntr_30 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(1),
      Q => mb_altera_core_s_dma_timeout_cntr(30)
    );
  mb_altera_core_s_dma_timeout_cntr_29 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(2),
      Q => mb_altera_core_s_dma_timeout_cntr(29)
    );
  mb_altera_core_s_dma_timeout_cntr_28 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(3),
      Q => mb_altera_core_s_dma_timeout_cntr(28)
    );
  mb_altera_core_s_dma_timeout_cntr_27 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(4),
      Q => mb_altera_core_s_dma_timeout_cntr(27)
    );
  mb_altera_core_s_dma_timeout_cntr_26 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(5),
      Q => mb_altera_core_s_dma_timeout_cntr(26)
    );
  mb_altera_core_s_dma_timeout_cntr_25 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(6),
      Q => mb_altera_core_s_dma_timeout_cntr(25)
    );
  mb_altera_core_s_dma_timeout_cntr_24 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(7),
      Q => mb_altera_core_s_dma_timeout_cntr(24)
    );
  mb_altera_core_s_dma_timeout_cntr_23 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(8),
      Q => mb_altera_core_s_dma_timeout_cntr(23)
    );
  mb_altera_core_s_dma_timeout_cntr_22 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(9),
      Q => mb_altera_core_s_dma_timeout_cntr(22)
    );
  mb_altera_core_s_dma_timeout_cntr_21 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(10),
      Q => mb_altera_core_s_dma_timeout_cntr(21)
    );
  mb_altera_core_s_dma_timeout_cntr_20 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(11),
      Q => mb_altera_core_s_dma_timeout_cntr(20)
    );
  mb_altera_core_s_dma_timeout_cntr_19 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(12),
      Q => mb_altera_core_s_dma_timeout_cntr(19)
    );
  mb_altera_core_s_dma_timeout_cntr_18 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(13),
      Q => mb_altera_core_s_dma_timeout_cntr(18)
    );
  mb_altera_core_s_dma_timeout_cntr_17 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(14),
      Q => mb_altera_core_s_dma_timeout_cntr(17)
    );
  mb_altera_core_s_dma_timeout_cntr_16 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(15),
      Q => mb_altera_core_s_dma_timeout_cntr(16)
    );
  mb_altera_core_s_dma_timeout_cntr_15 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(16),
      Q => mb_altera_core_s_dma_timeout_cntr(15)
    );
  mb_altera_core_s_dma_timeout_cntr_14 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(17),
      Q => mb_altera_core_s_dma_timeout_cntr(14)
    );
  mb_altera_core_s_dma_timeout_cntr_13 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(18),
      Q => mb_altera_core_s_dma_timeout_cntr(13)
    );
  mb_altera_core_s_dma_timeout_cntr_12 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(19),
      Q => mb_altera_core_s_dma_timeout_cntr(12)
    );
  mb_altera_core_s_dma_timeout_cntr_11 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(20),
      Q => mb_altera_core_s_dma_timeout_cntr(11)
    );
  mb_altera_core_s_dma_timeout_cntr_10 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(21),
      Q => mb_altera_core_s_dma_timeout_cntr(10)
    );
  mb_altera_core_s_dma_timeout_cntr_9 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(22),
      Q => mb_altera_core_s_dma_timeout_cntr(9)
    );
  mb_altera_core_s_dma_timeout_cntr_8 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(23),
      Q => mb_altera_core_s_dma_timeout_cntr(8)
    );
  mb_altera_core_s_dma_timeout_cntr_7 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(24),
      Q => mb_altera_core_s_dma_timeout_cntr(7)
    );
  mb_altera_core_s_dma_timeout_cntr_6 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(25),
      Q => mb_altera_core_s_dma_timeout_cntr(6)
    );
  mb_altera_core_s_dma_timeout_cntr_5 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(26),
      Q => mb_altera_core_s_dma_timeout_cntr(5)
    );
  mb_altera_core_s_dma_timeout_cntr_4 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(27),
      Q => mb_altera_core_s_dma_timeout_cntr(4)
    );
  mb_altera_core_s_dma_timeout_cntr_3 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(28),
      Q => mb_altera_core_s_dma_timeout_cntr(3)
    );
  mb_altera_core_s_dma_timeout_cntr_2 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(29),
      Q => mb_altera_core_s_dma_timeout_cntr(2)
    );
  mb_altera_core_s_dma_timeout_cntr_1 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(30),
      Q => mb_altera_core_s_dma_timeout_cntr(1)
    );
  mb_altera_core_s_dma_timeout_cntr_0 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_timeout_cntr_mux0000(31),
      Q => mb_altera_core_s_dma_timeout_cntr(0)
    );
  mb_altera_core_Write_Fifo_Data_in_32 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_Write_Fifo_Data_in_32_mux0000_2121,
      Q => mb_altera_core_Write_Fifo_Data_in(32)
    );
  mb_altera_core_Write_Fifo_Data_in_27 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(27),
      Q => mb_altera_core_Write_Fifo_Data_in(27)
    );
  mb_altera_core_Write_Fifo_Data_in_31 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(31),
      Q => mb_altera_core_Write_Fifo_Data_in(31)
    );
  mb_altera_core_Write_Fifo_Data_in_26 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(26),
      Q => mb_altera_core_Write_Fifo_Data_in(26)
    );
  mb_altera_core_Write_Fifo_Data_in_30 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(30),
      Q => mb_altera_core_Write_Fifo_Data_in(30)
    );
  mb_altera_core_Write_Fifo_Data_in_25 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(25),
      Q => mb_altera_core_Write_Fifo_Data_in(25)
    );
  mb_altera_core_Write_Fifo_Data_in_24 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(24),
      Q => mb_altera_core_Write_Fifo_Data_in(24)
    );
  mb_altera_core_Write_Fifo_Data_in_19 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(19),
      Q => mb_altera_core_Write_Fifo_Data_in(19)
    );
  mb_altera_core_Write_Fifo_Write_En : FDR
    port map (
      C => emif_clk,
      D => Mtrien_io_altera_dsp_mux0000_norst,
      R => mb_altera_core_Write_Fifo_Write_En_not0001,
      Q => mb_altera_core_Write_Fifo_Write_En_2128
    );
  mb_altera_core_Write_Fifo_Data_in_23 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(23),
      Q => mb_altera_core_Write_Fifo_Data_in(23)
    );
  mb_altera_core_Write_Fifo_Data_in_18 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(18),
      Q => mb_altera_core_Write_Fifo_Data_in(18)
    );
  mb_altera_core_Write_Fifo_Data_in_22 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(22),
      Q => mb_altera_core_Write_Fifo_Data_in(22)
    );
  mb_altera_core_Write_Fifo_Data_in_17 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(17),
      Q => mb_altera_core_Write_Fifo_Data_in(17)
    );
  mb_altera_core_Write_Fifo_Data_in_16 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(16),
      Q => mb_altera_core_Write_Fifo_Data_in(16)
    );
  mb_altera_core_Write_Fifo_Data_in_21 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(21),
      Q => mb_altera_core_Write_Fifo_Data_in(21)
    );
  mb_altera_core_s_err_clr : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_s_err_clr_and0000,
      D => inst_emif_iface_edi_r(4),
      Q => mb_altera_core_s_err_clr_3012
    );
  mb_altera_core_Write_Fifo_Data_in_15 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(15),
      Q => mb_altera_core_Write_Fifo_Data_in(15)
    );
  mb_altera_core_Write_Fifo_Data_in_20 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(20),
      Q => mb_altera_core_Write_Fifo_Data_in(20)
    );
  mb_altera_core_Write_Fifo_Data_in_14 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(14),
      Q => mb_altera_core_Write_Fifo_Data_in(14)
    );
  mb_altera_core_Write_Fifo_Data_in_13 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(13),
      Q => mb_altera_core_Write_Fifo_Data_in(13)
    );
  mb_altera_core_Flash_Read_Reg_7 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_Flash_Read_Reg_7_not0001,
      D => i_altera_data0_IBUF_487,
      Q => mb_altera_core_Flash_Read_Reg(7)
    );
  mb_altera_core_s_altera_rdfifo_din_0 : FD
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => N438,
      Q => mb_altera_core_s_altera_rdfifo_din(0)
    );
  mb_altera_core_Write_Fifo_Data_in_12 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(12),
      Q => mb_altera_core_Write_Fifo_Data_in(12)
    );
  mb_altera_core_Flash_Read_Reg_6 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_Flash_Read_Reg_6_not0001,
      D => i_altera_data0_IBUF_487,
      Q => mb_altera_core_Flash_Read_Reg(6)
    );
  mb_altera_core_Write_Fifo_Data_in_11 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(11),
      Q => mb_altera_core_Write_Fifo_Data_in(11)
    );
  mb_altera_core_Flash_Read_Reg_5 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_Flash_Read_Reg_5_not0001,
      D => i_altera_data0_IBUF_487,
      Q => mb_altera_core_Flash_Read_Reg(5)
    );
  mb_altera_core_Write_Fifo_Data_in_10 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      D => inst_emif_iface_edi_r(10),
      Q => mb_altera_core_Write_Fifo_Data_in(10)
    );
  mb_altera_core_Flash_Read_Reg_4 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_Flash_Read_Reg_4_not0001,
      D => i_altera_data0_IBUF_487,
      Q => mb_altera_core_Flash_Read_Reg(4)
    );
  mb_altera_core_o_user_io_31 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(31),
      Q => mb_altera_core_o_user_io(31)
    );
  mb_altera_core_o_user_io_30 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(30),
      Q => mb_altera_core_o_user_io(30)
    );
  mb_altera_core_o_user_io_29 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(29),
      Q => mb_altera_core_o_user_io(29)
    );
  mb_altera_core_o_user_io_28 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(28),
      Q => mb_altera_core_o_user_io(28)
    );
  mb_altera_core_o_user_io_27 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(27),
      Q => mb_altera_core_o_user_io(27)
    );
  mb_altera_core_o_user_io_26 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(26),
      Q => mb_altera_core_o_user_io(26)
    );
  mb_altera_core_o_user_io_25 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(25),
      Q => mb_altera_core_o_user_io(25)
    );
  mb_altera_core_o_user_io_24 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(24),
      Q => mb_altera_core_o_user_io(24)
    );
  mb_altera_core_o_user_io_23 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(23),
      Q => mb_altera_core_o_user_io(23)
    );
  mb_altera_core_o_user_io_22 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(22),
      Q => mb_altera_core_o_user_io(22)
    );
  mb_altera_core_o_user_io_21 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(21),
      Q => mb_altera_core_o_user_io(21)
    );
  mb_altera_core_o_user_io_20 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(20),
      Q => mb_altera_core_o_user_io(20)
    );
  mb_altera_core_o_user_io_19 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(19),
      Q => mb_altera_core_o_user_io(19)
    );
  mb_altera_core_o_user_io_18 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(18),
      Q => mb_altera_core_o_user_io(18)
    );
  mb_altera_core_o_user_io_17 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(17),
      Q => mb_altera_core_o_user_io(17)
    );
  mb_altera_core_o_user_io_16 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(16),
      Q => mb_altera_core_o_user_io(16)
    );
  mb_altera_core_o_user_io_15 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(15),
      Q => mb_altera_core_o_user_io(15)
    );
  mb_altera_core_o_user_io_14 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(14),
      Q => mb_altera_core_o_user_io(14)
    );
  mb_altera_core_o_user_io_13 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(13),
      Q => mb_altera_core_o_user_io(13)
    );
  mb_altera_core_o_user_io_12 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(12),
      Q => mb_altera_core_o_user_io(12)
    );
  mb_altera_core_o_user_io_11 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(11),
      Q => mb_altera_core_o_user_io(11)
    );
  mb_altera_core_o_user_io_10 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(10),
      Q => mb_altera_core_o_user_io(10)
    );
  mb_altera_core_o_user_io_9 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(9),
      Q => mb_altera_core_o_user_io(9)
    );
  mb_altera_core_o_user_io_8 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(8),
      Q => mb_altera_core_o_user_io(8)
    );
  mb_altera_core_o_user_io_7 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(7),
      Q => mb_altera_core_o_user_io(7)
    );
  mb_altera_core_o_user_io_6 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(6),
      Q => mb_altera_core_o_user_io(6)
    );
  mb_altera_core_o_user_io_5 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(5),
      Q => mb_altera_core_o_user_io(5)
    );
  mb_altera_core_o_user_io_4 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(4),
      Q => mb_altera_core_o_user_io(4)
    );
  mb_altera_core_o_user_io_3 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(3),
      Q => mb_altera_core_o_user_io(3)
    );
  mb_altera_core_o_user_io_2 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(2),
      Q => mb_altera_core_o_user_io(2)
    );
  mb_altera_core_o_user_io_1 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(1),
      Q => mb_altera_core_o_user_io(1)
    );
  mb_altera_core_o_user_io_0 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_o_user_io_and0000,
      D => inst_emif_iface_edi_r(0),
      Q => mb_altera_core_o_user_io(0)
    );
  mb_altera_core_Flash_Read_Reg_3 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_Flash_Read_Reg_3_not0001,
      D => i_altera_data0_IBUF_487,
      Q => mb_altera_core_Flash_Read_Reg(3)
    );
  mb_altera_core_Flash_Read_Reg_1 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_Flash_Read_Reg_1_not0001,
      D => i_altera_data0_IBUF_487,
      Q => mb_altera_core_Flash_Read_Reg(1)
    );
  mb_altera_core_Flash_Read_Reg_2 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_Flash_Read_Reg_2_not0001,
      D => i_altera_data0_IBUF_487,
      Q => mb_altera_core_Flash_Read_Reg(2)
    );
  mb_altera_core_Flash_Read_Reg_0 : FDE
    port map (
      C => emif_clk,
      CE => mb_altera_core_Flash_Read_Reg_0_not0001,
      D => i_altera_data0_IBUF_487,
      Q => mb_altera_core_Flash_Read_Reg(0)
    );
  inst_base_module_wd_rst_and00001 : LUT2
    generic map(
      INIT => X"1"
    )
    port map (
      I0 => inst_base_module_wd_nmi_en_945,
      I1 => inst_base_module_wd_rst_en_958,
      O => inst_base_module_wd_rst_not0002_inv
    );
  inst_base_module_o_emif_dcm_reset1 : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => dcm_status(1),
      I1 => inst_base_module_dcm_reset_shift(15),
      O => dcm_reset
    );
  inst_base_module_o_irq_output_4_7_and00001 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => irq_map_7_0_Q,
      I1 => inst_base_module_irq_mask(24),
      I2 => inst_base_module_irq_enable(7),
      O => o_dsp_ext_int7_OBUF_3044
    );
  busy1 : LUT3
    generic map(
      INIT => X"15"
    )
    port map (
      I0 => dcm_lock,
      I1 => N364,
      I2 => N366,
      O => o_busy_OBUF_3037
    );
  inst_emif_iface_sdram_dma_std_dma_engine_o_hold_n_and00001 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_holdn_r(1),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_holdn_r(0),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_holdn_r(3),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_holdn_r(2),
      O => o_hold_n_OBUF_3065
    );
  inst_emif_iface_wr1 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => inst_emif_iface_awe_n_996,
      I1 => inst_emif_iface_awe_dly_995,
      O => inst_emif_iface_wr
    );
  inst_emif_iface_rd1 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => inst_emif_iface_are_n_994,
      I1 => inst_emif_iface_are_dly_993,
      O => inst_emif_iface_rd
    );
  inst_base_module_o_wd_rst_and00001 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => inst_base_module_wd_rst_r_966,
      I1 => inst_base_module_wd_rst_952,
      O => inst_base_module_o_wd_rst_and0000
    );
  mb_altera_core_Mxor_s_altera_rdfifo_din_9_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N429,
      O => mb_altera_core_s_altera_rdfifo_din_9_xor0000
    );
  mb_altera_core_Mxor_s_altera_rdfifo_din_8_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N430,
      O => mb_altera_core_s_altera_rdfifo_din_8_xor0000
    );
  mb_altera_core_Mxor_s_altera_rdfifo_din_7_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N431,
      O => mb_altera_core_s_altera_rdfifo_din_7_xor0000
    );
  mb_altera_core_Mxor_s_altera_rdfifo_din_6_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N432,
      O => mb_altera_core_s_altera_rdfifo_din_6_xor0000
    );
  mb_altera_core_Mxor_s_altera_rdfifo_din_5_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N433,
      O => mb_altera_core_s_altera_rdfifo_din_5_xor0000
    );
  mb_altera_core_Mxor_s_altera_rdfifo_din_4_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N434,
      O => mb_altera_core_s_altera_rdfifo_din_4_xor0000
    );
  mb_altera_core_Mxor_s_altera_rdfifo_din_3_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N435,
      O => mb_altera_core_s_altera_rdfifo_din_3_xor0000
    );
  mb_altera_core_Mxor_s_altera_rdfifo_din_2_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N436,
      O => mb_altera_core_s_altera_rdfifo_din_2_xor0000
    );
  mb_altera_core_Mxor_s_altera_rdfifo_din_1_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N437,
      O => mb_altera_core_s_altera_rdfifo_din_1_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_9_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(9),
      O => mb_altera_core_o_altera_data_out_9_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_8_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(8),
      O => mb_altera_core_o_altera_data_out_8_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_7_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(7),
      O => mb_altera_core_o_altera_data_out_7_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_6_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(6),
      O => mb_altera_core_o_altera_data_out_6_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_5_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(5),
      O => mb_altera_core_o_altera_data_out_5_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_4_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(4),
      O => mb_altera_core_o_altera_data_out_4_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_3_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(3),
      O => mb_altera_core_o_altera_data_out_3_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_31_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(31),
      O => mb_altera_core_o_altera_data_out_31_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_30_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(30),
      O => mb_altera_core_o_altera_data_out_30_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_2_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(2),
      O => mb_altera_core_o_altera_data_out_2_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_29_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(29),
      O => mb_altera_core_o_altera_data_out_29_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_28_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(28),
      O => mb_altera_core_o_altera_data_out_28_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_27_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(27),
      O => mb_altera_core_o_altera_data_out_27_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_26_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(26),
      O => mb_altera_core_o_altera_data_out_26_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_25_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(25),
      O => mb_altera_core_o_altera_data_out_25_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_24_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(24),
      O => mb_altera_core_o_altera_data_out_24_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_23_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(23),
      O => mb_altera_core_o_altera_data_out_23_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_22_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(22),
      O => mb_altera_core_o_altera_data_out_22_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_21_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(21),
      O => mb_altera_core_o_altera_data_out_21_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_20_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(20),
      O => mb_altera_core_o_altera_data_out_20_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_1_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(1),
      O => mb_altera_core_o_altera_data_out_1_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_19_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(19),
      O => mb_altera_core_o_altera_data_out_19_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_18_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(18),
      O => mb_altera_core_o_altera_data_out_18_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_17_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(17),
      O => mb_altera_core_o_altera_data_out_17_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_16_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(16),
      O => mb_altera_core_o_altera_data_out_16_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_15_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(15),
      O => mb_altera_core_o_altera_data_out_15_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_14_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(14),
      O => mb_altera_core_o_altera_data_out_14_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_13_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(13),
      O => mb_altera_core_o_altera_data_out_13_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_12_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(12),
      O => mb_altera_core_o_altera_data_out_12_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_11_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(11),
      O => mb_altera_core_o_altera_data_out_11_xor0000
    );
  mb_altera_core_Mxor_o_altera_data_out_10_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_data_out(0),
      I1 => mb_altera_core_Write_Fifo_data_out(10),
      O => mb_altera_core_o_altera_data_out_10_xor0000
    );
  mb_altera_core_Mxor_Read_Fifo_Data_in_31_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N407,
      O => mb_altera_core_Read_Fifo_Data_in_31_xor0000
    );
  mb_altera_core_Mxor_Read_Fifo_Data_in_30_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N408,
      O => mb_altera_core_Read_Fifo_Data_in_30_xor0000
    );
  mb_altera_core_Mxor_Read_Fifo_Data_in_29_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N409,
      O => mb_altera_core_Read_Fifo_Data_in_29_xor0000
    );
  mb_altera_core_Mxor_Read_Fifo_Data_in_28_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N410,
      O => mb_altera_core_Read_Fifo_Data_in_28_xor0000
    );
  mb_altera_core_Mxor_Read_Fifo_Data_in_27_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N411,
      O => mb_altera_core_Read_Fifo_Data_in_27_xor0000
    );
  mb_altera_core_Mxor_Read_Fifo_Data_in_26_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N412,
      O => mb_altera_core_Read_Fifo_Data_in_26_xor0000
    );
  mb_altera_core_Mxor_Read_Fifo_Data_in_25_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N413,
      O => mb_altera_core_Read_Fifo_Data_in_25_xor0000
    );
  mb_altera_core_Mxor_Read_Fifo_Data_in_24_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N414,
      O => mb_altera_core_Read_Fifo_Data_in_24_xor0000
    );
  mb_altera_core_Mxor_Read_Fifo_Data_in_23_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N415,
      O => mb_altera_core_Read_Fifo_Data_in_23_xor0000
    );
  mb_altera_core_Mxor_Read_Fifo_Data_in_22_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N416,
      O => mb_altera_core_Read_Fifo_Data_in_22_xor0000
    );
  mb_altera_core_Mxor_Read_Fifo_Data_in_21_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N417,
      O => mb_altera_core_Read_Fifo_Data_in_21_xor0000
    );
  mb_altera_core_Mxor_Read_Fifo_Data_in_20_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N418,
      O => mb_altera_core_Read_Fifo_Data_in_20_xor0000
    );
  mb_altera_core_Mxor_Read_Fifo_Data_in_19_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N419,
      O => mb_altera_core_Read_Fifo_Data_in_19_xor0000
    );
  mb_altera_core_Mxor_Read_Fifo_Data_in_18_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N420,
      O => mb_altera_core_Read_Fifo_Data_in_18_xor0000
    );
  mb_altera_core_Mxor_Read_Fifo_Data_in_17_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N421,
      O => mb_altera_core_Read_Fifo_Data_in_17_xor0000
    );
  mb_altera_core_Mxor_Read_Fifo_Data_in_16_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N422,
      O => mb_altera_core_Read_Fifo_Data_in_16_xor0000
    );
  mb_altera_core_Mxor_Read_Fifo_Data_in_15_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N423,
      O => mb_altera_core_Read_Fifo_Data_in_15_xor0000
    );
  mb_altera_core_Mxor_Read_Fifo_Data_in_14_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N424,
      O => mb_altera_core_Read_Fifo_Data_in_14_xor0000
    );
  mb_altera_core_Mxor_Read_Fifo_Data_in_13_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N425,
      O => mb_altera_core_Read_Fifo_Data_in_13_xor0000
    );
  mb_altera_core_Mxor_Read_Fifo_Data_in_12_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N426,
      O => mb_altera_core_Read_Fifo_Data_in_12_xor0000
    );
  mb_altera_core_Mxor_Read_Fifo_Data_in_11_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N427,
      O => mb_altera_core_Read_Fifo_Data_in_11_xor0000
    );
  mb_altera_core_Mxor_Read_Fifo_Data_in_10_xor0000_Result1 : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => N438,
      I1 => N428,
      O => mb_altera_core_Read_Fifo_Data_in_10_xor0000
    );
  inst_base_module_wd_nmi_mux00001 : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => inst_base_module_wd_kick_r2_938,
      I1 => inst_base_module_wd_kick_r1_937,
      O => inst_base_module_wd_nmi_mux0000
    );
  inst_emif_iface_t_ed1 : LUT4
    generic map(
      INIT => X"AA80"
    )
    port map (
      I0 => inst_emif_iface_dma_t_ed_1014,
      I1 => N364,
      I2 => N366,
      I3 => N367,
      O => io_ed_o_ed_not0000_inv
    );
  inst_emif_iface_sdram_dma_std_dma_engine_awe_n_mux00001 : LUT4
    generic map(
      INIT => X"2F22"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_awe_n_1214,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      O => inst_emif_iface_sdram_dma_std_dma_engine_awe_n_mux0000
    );
  inst_emif_iface_sdram_dma_std_dma_engine_cas_n_mux00001 : LUT4
    generic map(
      INIT => X"E5E4"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_cas_n_1225,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      O => inst_emif_iface_sdram_dma_std_dma_engine_cas_n_mux0000
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ras_n_mux00001 : LUT4
    generic map(
      INIT => X"64FC"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_ras_n_1350,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      O => inst_emif_iface_sdram_dma_std_dma_engine_ras_n_mux0000
    );
  inst_base_module_div5_2_and00001 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => inst_base_module_div5_1_r_634,
      I1 => inst_base_module_div5_1(5),
      O => inst_base_module_div5_2_and0000
    );
  inst_base_module_div10_5_and00001 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => inst_base_module_div10_4_r_628,
      I1 => inst_base_module_div10_4(10),
      O => inst_base_module_div10_5_and0000
    );
  inst_base_module_div10_4_and00001 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => inst_base_module_div10_3_r_625,
      I1 => inst_base_module_div10_3(10),
      O => inst_base_module_div10_4_and0000
    );
  inst_base_module_div10_3_and00001 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => inst_base_module_div10_2_r_622,
      I1 => inst_base_module_div10_2(10),
      O => inst_base_module_div10_3_and0000
    );
  inst_base_module_div10_2_and00001 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => inst_base_module_div10_1_1_Q,
      I1 => inst_base_module_div10_1_10_Q,
      O => inst_base_module_div10_2_and0000
    );
  mb_altera_core_Flash_Read_Reg_7_not00011 : LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => mb_altera_core_Flash_Cnt(0),
      I1 => mb_altera_core_Flash_Cnt(2),
      I2 => mb_altera_core_Flash_State_FSM_FFd3_1601,
      I3 => mb_altera_core_Flash_Cnt(1),
      O => mb_altera_core_Flash_Read_Reg_7_not0001
    );
  mb_altera_core_Flash_Read_Reg_6_not00011 : LUT4
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => mb_altera_core_Flash_State_FSM_FFd3_1601,
      I1 => mb_altera_core_Flash_Cnt(0),
      I2 => mb_altera_core_Flash_Cnt(2),
      I3 => mb_altera_core_Flash_Cnt(1),
      O => mb_altera_core_Flash_Read_Reg_6_not0001
    );
  mb_altera_core_Flash_Read_Reg_5_not00011 : LUT4
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => mb_altera_core_Flash_State_FSM_FFd3_1601,
      I1 => mb_altera_core_Flash_Cnt(1),
      I2 => mb_altera_core_Flash_Cnt(0),
      I3 => mb_altera_core_Flash_Cnt(2),
      O => mb_altera_core_Flash_Read_Reg_5_not0001
    );
  mb_altera_core_Flash_Read_Reg_4_not00011 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => mb_altera_core_Flash_Cnt(0),
      I1 => mb_altera_core_Flash_State_FSM_FFd3_1601,
      I2 => mb_altera_core_Flash_Cnt(1),
      I3 => mb_altera_core_Flash_Cnt(2),
      O => mb_altera_core_Flash_Read_Reg_4_not0001
    );
  mb_altera_core_Flash_Read_Reg_3_not00011 : LUT4
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => mb_altera_core_Flash_Cnt(2),
      I1 => mb_altera_core_Flash_State_FSM_FFd3_1601,
      I2 => mb_altera_core_Flash_Cnt(0),
      I3 => mb_altera_core_Flash_Cnt(1),
      O => mb_altera_core_Flash_Read_Reg_3_not0001
    );
  mb_altera_core_Flash_Read_Reg_2_not00011 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => mb_altera_core_Flash_Cnt(0),
      I1 => mb_altera_core_Flash_State_FSM_FFd3_1601,
      I2 => mb_altera_core_Flash_Cnt(2),
      I3 => mb_altera_core_Flash_Cnt(1),
      O => mb_altera_core_Flash_Read_Reg_2_not0001
    );
  mb_altera_core_Flash_Read_Reg_1_not00011 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => mb_altera_core_Flash_Cnt(1),
      I1 => mb_altera_core_Flash_State_FSM_FFd3_1601,
      I2 => mb_altera_core_Flash_Cnt(2),
      I3 => mb_altera_core_Flash_Cnt(0),
      O => mb_altera_core_Flash_Read_Reg_1_not0001
    );
  inst_base_module_dcm_reset_shift_or00001 : LUT4
    generic map(
      INIT => X"BFAE"
    )
    port map (
      I0 => inst_base_module_dcm_reset_shift(15),
      I1 => inst_base_module_dcm_lock_r2_615,
      I2 => inst_base_module_dcm_lock_r1_614,
      I3 => inst_base_module_clken4ms_612,
      O => inst_base_module_dcm_reset_shift_or0000
    );
  inst_emif_iface_cs_8_mux0001_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_emif_iface_ce3_n_1005,
      I1 => inst_emif_iface_ea(9),
      I2 => inst_emif_iface_ea(8),
      O => N7
    );
  inst_emif_iface_cs_8_mux0001 : LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => inst_emif_iface_ea(7),
      I1 => inst_emif_iface_ea(11),
      I2 => inst_emif_iface_ea(10),
      I3 => N7,
      O => inst_emif_iface_cs_8_Q
    );
  inst_emif_iface_cs_0_mux0001 : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => inst_emif_iface_ea(7),
      I1 => inst_emif_iface_ea(11),
      I2 => inst_emif_iface_ea(10),
      I3 => N7,
      O => inst_emif_iface_cs_0_Q
    );
  mb_altera_core_Read_Fifo_Data_in_0_and00001 : LUT2
    generic map(
      INIT => X"1"
    )
    port map (
      I0 => i_altera_read_Busy_bar_IBUF_493,
      I1 => N406,
      O => mb_altera_core_Read_Fifo_Data_in_0_and0000
    );
  inst_base_module_clken100ms_not00011 : LUT2
    generic map(
      INIT => X"D"
    )
    port map (
      I0 => inst_base_module_div5_2(5),
      I1 => inst_base_module_div5_2_r_637,
      O => inst_base_module_clken100ms_not0001
    );
  mb_altera_core_s_altera_rdfifo_wr_en_not00011 : LUT3
    generic map(
      INIT => X"EF"
    )
    port map (
      I0 => i_altera_read_Busy_bar_IBUF_493,
      I1 => o_altera_read_ff_ff_OBUF_3033,
      I2 => N406,
      O => mb_altera_core_s_altera_rdfifo_wr_en_not0001
    );
  mb_altera_core_s_dma_state_FSM_FFd2_In11 : LUT3
    generic map(
      INIT => X"DF"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I1 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I2 => mb_altera_core_s_dma_irq_clear_2664,
      O => mb_altera_core_N9
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_In21 : LUT3
    generic map(
      INIT => X"04"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      O => inst_emif_iface_sdram_dma_std_dma_engine_N34
    );
  inst_emif_iface_cs_3_mux00011 : LUT3
    generic map(
      INIT => X"08"
    )
    port map (
      I0 => inst_emif_iface_N12,
      I1 => inst_emif_iface_ea(8),
      I2 => inst_emif_iface_ea(9),
      O => inst_emif_iface_cs_3_Q
    );
  inst_emif_iface_cs_1_mux00012 : LUT3
    generic map(
      INIT => X"04"
    )
    port map (
      I0 => inst_emif_iface_ea(9),
      I1 => inst_emif_iface_N12,
      I2 => inst_emif_iface_ea(8),
      O => inst_emif_iface_cs_1_Q
    );
  mb_altera_core_s_altera_int_busy_mux00001 : LUT4
    generic map(
      INIT => X"8880"
    )
    port map (
      I0 => mb_altera_core_s_altera_int_en_2553,
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_altera_int_busy_2551,
      I3 => mb_altera_core_s_i_altera_int_r2_3017,
      O => mb_altera_core_s_altera_int_busy_mux0000
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ea_9_mux00011 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(7),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N34,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_col(7),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N41,
      O => inst_emif_iface_sdram_dma_std_dma_engine_ea_9_mux0001
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ea_8_mux00011 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(6),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N34,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_col(6),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N41,
      O => inst_emif_iface_sdram_dma_std_dma_engine_ea_8_mux0001
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ea_7_mux00011 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(5),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N34,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_col(5),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N41,
      O => inst_emif_iface_sdram_dma_std_dma_engine_ea_7_mux0001
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ea_6_mux00011 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(4),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N34,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_col(4),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N41,
      O => inst_emif_iface_sdram_dma_std_dma_engine_ea_6_mux0001
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ea_5_mux00011 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(3),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N34,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_col(3),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N41,
      O => inst_emif_iface_sdram_dma_std_dma_engine_ea_5_mux0001
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ea_4_mux00011 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(2),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N34,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_col(2),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N41,
      O => inst_emif_iface_sdram_dma_std_dma_engine_ea_4_mux0001
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ea_3_mux00011 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(1),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N34,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_col(1),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N41,
      O => inst_emif_iface_sdram_dma_std_dma_engine_ea_3_mux0001
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ea_2_mux00011 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(0),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N34,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_col(0),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N41,
      O => inst_emif_iface_sdram_dma_std_dma_engine_ea_2_mux0001
    );
  inst_emif_iface_cs_1_mux000111 : LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => inst_emif_iface_ce3_n_1005,
      I1 => inst_emif_iface_ea(10),
      I2 => inst_emif_iface_ea(7),
      I3 => inst_emif_iface_ea(11),
      O => inst_emif_iface_N12
    );
  mb_altera_core_Flash_Cnt_mux0000_1_1 : LUT4
    generic map(
      INIT => X"286C"
    )
    port map (
      I0 => mb_altera_core_Flash_Cnt_or0000,
      I1 => mb_altera_core_Flash_Cnt(1),
      I2 => mb_altera_core_Flash_Cnt(0),
      I3 => mb_altera_core_Flash_State_FSM_FFd7_1607,
      O => mb_altera_core_Flash_Cnt_mux0000(1)
    );
  inst_emif_iface_edo_ce3_or0031_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(0),
      I1 => edo_3_0_Q,
      I2 => edo_1_0_Q,
      O => N11
    );
  inst_emif_iface_edo_ce3_or0030_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(1),
      I1 => edo_3_1_Q,
      I2 => edo_1_1_Q,
      O => N13
    );
  inst_emif_iface_edo_ce3_or0029_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(2),
      I1 => edo_3_2_Q,
      I2 => edo_1_2_Q,
      O => N15
    );
  inst_emif_iface_edo_ce3_or0028_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(3),
      I1 => edo_3_3_Q,
      I2 => edo_1_3_Q,
      O => N17
    );
  inst_emif_iface_edo_ce3_or0027_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(4),
      I1 => edo_3_4_Q,
      I2 => edo_1_4_Q,
      O => N19
    );
  inst_emif_iface_edo_ce3_or0026_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(5),
      I1 => edo_3_5_Q,
      I2 => edo_1_5_Q,
      O => N21
    );
  inst_emif_iface_edo_ce3_or0025_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(6),
      I1 => edo_3_6_Q,
      I2 => edo_1_6_Q,
      O => N23
    );
  inst_emif_iface_edo_ce3_or0024_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(7),
      I1 => edo_3_7_Q,
      I2 => edo_1_7_Q,
      O => N25
    );
  inst_emif_iface_edo_ce3_or0023_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(8),
      I1 => edo_3_8_Q,
      I2 => edo_1_8_Q,
      O => N27
    );
  inst_emif_iface_edo_ce3_or0022_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(9),
      I1 => edo_3_9_Q,
      I2 => edo_1_9_Q,
      O => N29
    );
  inst_emif_iface_edo_ce3_or0021_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(10),
      I1 => edo_3_10_Q,
      I2 => edo_1_10_Q,
      O => N31
    );
  inst_emif_iface_edo_ce3_or0020_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(11),
      I1 => edo_3_11_Q,
      I2 => edo_1_11_Q,
      O => N33
    );
  inst_emif_iface_edo_ce3_or0019_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(12),
      I1 => edo_3_12_Q,
      I2 => edo_1_12_Q,
      O => N35
    );
  inst_emif_iface_edo_ce3_or0018_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(13),
      I1 => edo_3_13_Q,
      I2 => edo_1_13_Q,
      O => N37
    );
  inst_emif_iface_edo_ce3_or0017_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(14),
      I1 => edo_3_14_Q,
      I2 => edo_1_14_Q,
      O => N39
    );
  inst_emif_iface_edo_ce3_or0016_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(15),
      I1 => edo_3_15_Q,
      I2 => edo_1_15_Q,
      O => N41
    );
  inst_emif_iface_edo_ce3_or0015_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(16),
      I1 => edo_3_16_Q,
      I2 => edo_1_16_Q,
      O => N43
    );
  inst_emif_iface_edo_ce3_or0014_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(17),
      I1 => edo_3_17_Q,
      I2 => edo_1_17_Q,
      O => N45
    );
  inst_emif_iface_edo_ce3_or0013_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(18),
      I1 => edo_3_18_Q,
      I2 => edo_1_18_Q,
      O => N47
    );
  inst_emif_iface_edo_ce3_or0012_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(19),
      I1 => edo_3_19_Q,
      I2 => edo_1_19_Q,
      O => N49
    );
  inst_emif_iface_edo_ce3_or0011_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(20),
      I1 => edo_3_20_Q,
      I2 => edo_1_20_Q,
      O => N51
    );
  inst_emif_iface_edo_ce3_or0010_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(21),
      I1 => edo_3_21_Q,
      I2 => edo_1_21_Q,
      O => N53
    );
  inst_emif_iface_edo_ce3_or0009_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(22),
      I1 => edo_3_22_Q,
      I2 => edo_1_22_Q,
      O => N55
    );
  inst_emif_iface_edo_ce3_or0008_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(23),
      I1 => edo_3_23_Q,
      I2 => edo_1_23_Q,
      O => N57
    );
  inst_emif_iface_edo_ce3_or0007_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(24),
      I1 => edo_3_24_Q,
      I2 => edo_1_24_Q,
      O => N59
    );
  inst_emif_iface_edo_ce3_or0006_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(25),
      I1 => edo_3_25_Q,
      I2 => edo_1_25_Q,
      O => N61
    );
  inst_emif_iface_edo_ce3_or0005_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(26),
      I1 => edo_3_26_Q,
      I2 => edo_1_26_Q,
      O => N63
    );
  inst_emif_iface_edo_ce3_or0004_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(27),
      I1 => edo_3_27_Q,
      I2 => edo_1_27_Q,
      O => N65
    );
  inst_emif_iface_edo_ce3_or0003_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(28),
      I1 => edo_3_28_Q,
      I2 => edo_1_28_Q,
      O => N67
    );
  inst_emif_iface_edo_ce3_or0002_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(29),
      I1 => edo_3_29_Q,
      I2 => edo_1_29_Q,
      O => N69
    );
  inst_emif_iface_edo_ce3_or0001_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(30),
      I1 => edo_3_30_Q,
      I2 => edo_1_30_Q,
      O => N71
    );
  inst_emif_iface_edo_ce3_or0000_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_base_module_o_DBus(31),
      I1 => edo_3_31_Q,
      I2 => edo_1_31_Q,
      O => N73
    );
  mb_altera_core_s_altera_fifo_rst_not0001_SW0 : LUT4
    generic map(
      INIT => X"EFFF"
    )
    port map (
      I0 => inst_emif_iface_addr_r(0),
      I1 => inst_emif_iface_addr_r(2),
      I2 => inst_emif_iface_wr_r_1474,
      I3 => inst_emif_iface_cs_r_8_Q,
      O => N78
    );
  mb_altera_core_s_altera_fifo_rst_not0001 : LUT4
    generic map(
      INIT => X"FF7F"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => inst_emif_iface_addr_r(3),
      I2 => inst_emif_iface_addr_r(1),
      I3 => N78,
      O => mb_altera_core_s_altera_fifo_rst_not0001_2550
    );
  mb_altera_core_s_dma_irq_clear_not000111 : LUT4
    generic map(
      INIT => X"FF7F"
    )
    port map (
      I0 => inst_emif_iface_addr_r(0),
      I1 => inst_emif_iface_addr_r(3),
      I2 => inst_emif_iface_addr_r(2),
      I3 => inst_emif_iface_addr_r(4),
      O => mb_altera_core_s_dma_irq_clear_not000111_2666
    );
  mb_altera_core_s_dma_irq_clear_not000123 : LUT4
    generic map(
      INIT => X"FF7F"
    )
    port map (
      I0 => inst_emif_iface_wr_r_1474,
      I1 => inst_emif_iface_cs_r_8_Q,
      I2 => inst_emif_iface_edi_r(0),
      I3 => inst_emif_iface_addr_r(1),
      O => mb_altera_core_s_dma_irq_clear_not000123_2667
    );
  mb_altera_core_s_dma_irq_clear_not000124 : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => mb_altera_core_s_dma_irq_clear_not000111_2666,
      I1 => mb_altera_core_s_dma_irq_clear_not000123_2667,
      O => mb_altera_core_s_dma_irq_clear_not0001
    );
  mb_altera_core_s_dma_timeout_threshold_and00001 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => N771,
      I2 => mb_altera_core_N12,
      I3 => inst_emif_iface_addr_r(3),
      O => mb_altera_core_s_dma_timeout_threshold_and0000
    );
  mb_altera_core_o_user_io_and00001 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => inst_emif_iface_addr_r(3),
      I1 => mb_altera_core_N15,
      I2 => mb_altera_core_N12,
      I3 => inst_emif_iface_addr_r(4),
      O => mb_altera_core_o_user_io_and0000
    );
  mb_altera_core_altera_ncs_and00001 : LUT4
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => mb_altera_core_N42,
      I1 => mb_altera_core_N15,
      I2 => inst_emif_iface_addr_r(2),
      I3 => inst_emif_iface_addr_r(3),
      O => mb_altera_core_altera_ncs_and0000
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_6_41 : LUT2
    generic map(
      INIT => X"7"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      O => inst_emif_iface_sdram_dma_std_dma_engine_N43
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_0_11 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      O => inst_emif_iface_sdram_dma_std_dma_engine_N41
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_17_51 : LUT3
    generic map(
      INIT => X"01"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      O => inst_emif_iface_sdram_dma_std_dma_engine_N38
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_0_21 : LUT3
    generic map(
      INIT => X"AE"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      O => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_37
    );
  mb_altera_core_s_dma_state_cmp_ge00001 : LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      I0 => mb_altera_core_s_altera_rdfifo_rd_cnt(12),
      I1 => mb_altera_core_s_altera_rdfifo_rd_cnt(11),
      I2 => mb_altera_core_s_altera_rdfifo_rd_cnt(10),
      I3 => mb_altera_core_s_altera_rdfifo_rd_cnt(9),
      O => mb_altera_core_s_dma_state_cmp_ge0000
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_In41 : LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_done_1301,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_arb_FSM_FFd2_1419,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_arb_FSM_FFd3_1420,
      O => inst_emif_iface_sdram_dma_std_dma_engine_N46
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sm_arb_FSM_FFd3_In_SW0 : LUT2
    generic map(
      INIT => X"D"
    )
    port map (
      I0 => mb_altera_core_s_dma_req_2694,
      I1 => i_bus_req_IBUF_497,
      O => N86
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sm_arb_FSM_FFd3_In_SW1 : LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_holda_r(1),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_holda_r(0),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_holda_r(3),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_holda_r(2),
      O => N87
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sm_arb_FSM_FFd3_In : LUT4
    generic map(
      INIT => X"8A8F"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_arb_FSM_FFd3_1420,
      I1 => N87,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_arb_FSM_FFd2_1419,
      I3 => N86,
      O => inst_emif_iface_sdram_dma_std_dma_engine_sm_arb_FSM_FFd3_In_1421
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_31_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(0),
      I3 => mb_altera_core_s_dma_timeout_cntr(0),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(31)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_0_4 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_N38,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(0),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_col(0),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_37,
      O => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_0_4_1238
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_0_17 : LUT4
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(4),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_col(0),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      O => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_0_17_1237
    );
  inst_base_module_div5_1_and00001 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => inst_base_module_div10_5_r_631,
      I1 => inst_base_module_div10_5(10),
      O => inst_base_module_div5_1_and0000
    );
  mb_altera_core_Read_Fifo_Data_Ready_and00001 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      I1 => inst_emif_iface_rd_r_1175,
      I2 => inst_emif_iface_cs_r_8_Q,
      O => mb_altera_core_Read_Fifo_Data_Ready_and0000
    );
  mb_altera_core_Flash_Cnt_or00001 : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => mb_altera_core_Flash_State_FSM_FFd4_1602,
      I1 => mb_altera_core_Flash_State_FSM_FFd1_1599,
      O => mb_altera_core_Flash_Cnt_or0000
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_In11 : LUT2
    generic map(
      INIT => X"D"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt(0),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt(1),
      O => inst_emif_iface_sdram_dma_std_dma_engine_N17
    );
  inst_base_module_key_state_mux0003_1_1 : LUT3
    generic map(
      INIT => X"08"
    )
    port map (
      I0 => inst_base_module_key_state_cmp_eq0003,
      I1 => inst_base_module_key_state(0),
      I2 => inst_emif_iface_edi_r(0),
      O => inst_base_module_key_state_mux0003(1)
    );
  inst_base_module_key_state_mux0003_0_1 : LUT3
    generic map(
      INIT => X"08"
    )
    port map (
      I0 => inst_base_module_key_state_cmp_eq0004,
      I1 => inst_base_module_key_state(1),
      I2 => inst_emif_iface_edi_r(0),
      O => inst_base_module_key_state_mux0003(0)
    );
  mb_altera_core_s_dma_num_samples_mux0000_9_2 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => mb_altera_core_s_dma_num_samples(0),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_N38,
      O => mb_altera_core_s_dma_num_samples_mux0000(9)
    );
  mb_altera_core_Flash_State_FSM_FFd6_In1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_Flash_State_FSM_FFd4_1602,
      I1 => mb_altera_core_Flash_State_cmp_eq0003,
      I2 => mb_altera_core_Flash_State_FSM_N3,
      I3 => mb_altera_core_Flash_Write_Reg_0_and0000,
      O => mb_altera_core_Flash_State_FSM_FFd6_In
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ce_n_mux0002_3_11 : LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_we_r(3),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_we_r(2),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_dma_we_r(1),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_we_r(0),
      O => inst_emif_iface_sdram_dma_std_dma_engine_N5
    );
  inst_base_module_o_DBus_mux0002_26_1 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0004,
      I1 => inst_base_module_irq_mask(26),
      I2 => inst_emif_iface_addr_r(4),
      I3 => inst_base_module_varindex0000(26),
      O => inst_base_module_o_DBus_mux0002_26_Q
    );
  inst_base_module_o_DBus_mux0002_25_1 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0004,
      I1 => inst_base_module_irq_mask(25),
      I2 => inst_emif_iface_addr_r(4),
      I3 => inst_base_module_varindex0000(25),
      O => inst_base_module_o_DBus_mux0002_25_Q
    );
  inst_base_module_o_DBus_mux0002_20_1 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0004,
      I1 => inst_base_module_irq_mask(20),
      I2 => inst_emif_iface_addr_r(4),
      I3 => inst_base_module_varindex0000(20),
      O => inst_base_module_o_DBus_mux0002_20_Q
    );
  inst_base_module_irq_mask_mux0000_0_11 : LUT4
    generic map(
      INIT => X"FF7F"
    )
    port map (
      I0 => inst_emif_iface_addr_r(2),
      I1 => inst_emif_iface_addr_r(0),
      I2 => inst_base_module_irq_mask_and0000,
      I3 => inst_emif_iface_addr_r(1),
      O => inst_base_module_N0
    );
  mb_altera_core_Flash_State_FSM_FFd5_In_SW0 : LUT3
    generic map(
      INIT => X"04"
    )
    port map (
      I0 => inst_emif_iface_addr_r(1),
      I1 => mb_altera_core_Flash_State_FSM_N3,
      I2 => inst_emif_iface_addr_r(2),
      O => N114
    );
  mb_altera_core_Flash_State_FSM_FFd5_In : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_Flash_State_FSM_FFd1_1599,
      I1 => mb_altera_core_Flash_State_cmp_eq0003,
      I2 => N114,
      I3 => mb_altera_core_N13,
      O => mb_altera_core_Flash_State_FSM_FFd5_In_1604
    );
  inst_base_module_key_state_cmp_eq000421 : LUT4
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => inst_emif_iface_edi_r(24),
      I1 => inst_emif_iface_edi_r(26),
      I2 => inst_emif_iface_edi_r(27),
      I3 => inst_emif_iface_edi_r(25),
      O => inst_base_module_key_state_cmp_eq000421_784
    );
  mb_altera_core_altera_ncon_and00001 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => inst_emif_iface_addr_r(0),
      I1 => N768,
      O => mb_altera_core_altera_ncon_and0000
    );
  mb_altera_core_altera_nce_and00001 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => inst_emif_iface_addr_r(0),
      I1 => mb_altera_core_N44,
      O => mb_altera_core_altera_nce_and0000
    );
  inst_base_module_bank_addr_0_and000021 : LUT2
    generic map(
      INIT => X"7"
    )
    port map (
      I0 => inst_emif_iface_cs_r_0_Q,
      I1 => inst_emif_iface_wr_r_1474,
      O => inst_base_module_and0000_inv
    );
  inst_base_module_led_enable_0_not00011 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => inst_emif_iface_addr_r(1),
      I1 => inst_emif_iface_addr_r(0),
      I2 => inst_base_module_N5,
      I3 => inst_base_module_and0000_inv,
      O => inst_base_module_led_enable_0_not0001
    );
  inst_base_module_wd_nmi_en_not00012 : LUT4
    generic map(
      INIT => X"7340"
    )
    port map (
      I0 => inst_base_module_wd_rst_r2_968,
      I1 => inst_base_module_and0000_inv,
      I2 => inst_base_module_wd_rst_r1_967,
      I3 => inst_base_module_o_DBus_cmp_eq0006,
      O => inst_base_module_wd_nmi_en_not0001
    );
  inst_base_module_wd_nmi_not000118 : LUT4
    generic map(
      INIT => X"0090"
    )
    port map (
      I0 => inst_base_module_wd_kick_r1_937,
      I1 => inst_base_module_wd_kick_r2_938,
      I2 => inst_base_module_wd_nmi_en_945,
      I3 => inst_base_module_wd_nmi_939,
      O => inst_base_module_wd_nmi_not000118_950
    );
  inst_base_module_wd_nmi_not000156 : LUT4
    generic map(
      INIT => X"0660"
    )
    port map (
      I0 => inst_base_module_wd_nmi_clr_r1_943,
      I1 => inst_base_module_wd_nmi_clr_r2_944,
      I2 => inst_base_module_wd_kick_r1_937,
      I3 => inst_base_module_wd_kick_r2_938,
      O => inst_base_module_wd_nmi_not000156_951
    );
  inst_base_module_wd_nmi_not000158 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_wd_nmi_not000156_951,
      I1 => inst_base_module_wd_counter_cmp_eq0000,
      I2 => inst_base_module_wd_nmi_not000118_950,
      O => inst_base_module_wd_nmi_not0001
    );
  inst_base_module_key_state_cmp_eq00041211 : LUT4
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => inst_emif_iface_edi_r(18),
      I1 => inst_emif_iface_edi_r(16),
      I2 => inst_emif_iface_edi_r(17),
      I3 => inst_emif_iface_edi_r(19),
      O => inst_base_module_key_state_cmp_eq00041211_783
    );
  inst_base_module_key_state_cmp_eq0004122 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => N769,
      I1 => inst_base_module_key_state_cmp_eq00041211_783,
      O => inst_base_module_N8
    );
  inst_base_module_wd_rst_not000360 : LUT4
    generic map(
      INIT => X"0660"
    )
    port map (
      I0 => inst_base_module_wd_rst_clr_r1_956,
      I1 => inst_base_module_wd_rst_clr_r2_957,
      I2 => inst_base_module_wd_kick_r1_937,
      I3 => inst_base_module_wd_kick_r2_938,
      O => inst_base_module_wd_rst_not000360_965
    );
  inst_base_module_wd_rst_not000362 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_wd_rst_not000360_965,
      I1 => inst_base_module_wd_counter_cmp_eq0000,
      I2 => inst_base_module_wd_rst_not000320,
      O => inst_base_module_wd_rst_not0003
    );
  mb_altera_core_Write_fifo_wr_Cnt_not00014 : LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      I0 => mb_altera_core_Write_fifo_wr_Cnt(7),
      I1 => mb_altera_core_Write_fifo_wr_Cnt(6),
      I2 => mb_altera_core_Write_fifo_wr_Cnt(5),
      I3 => mb_altera_core_Write_fifo_wr_Cnt(4),
      O => mb_altera_core_Write_fifo_wr_Cnt_not00014_2181
    );
  mb_altera_core_s_dma_state_FSM_Out31 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      O => mb_altera_core_s_dma_state_cmp_eq0003
    );
  mb_altera_core_s_altera_rdfifo_rd_en_and00001 : LUT2
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_done_1301,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_1302,
      O => mb_altera_core_s_altera_rdfifo_rd_en
    );
  mb_altera_core_Flash_State_cmp_eq00031 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_Flash_Cnt(1),
      I1 => mb_altera_core_Flash_Cnt(0),
      I2 => mb_altera_core_Flash_Cnt(2),
      O => mb_altera_core_Flash_State_cmp_eq0003
    );
  inst_base_module_o_DBus_cmp_eq00051 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => inst_emif_iface_addr_r(2),
      I1 => inst_emif_iface_addr_r(1),
      I2 => inst_base_module_irq_mask_and0000,
      I3 => inst_emif_iface_addr_r(0),
      O => inst_base_module_o_DBus_cmp_eq0005
    );
  inst_base_module_irq_mask_mux0000_0_31 : LUT4
    generic map(
      INIT => X"0800"
    )
    port map (
      I0 => inst_emif_iface_addr_r(0),
      I1 => inst_emif_iface_addr_r(2),
      I2 => inst_emif_iface_addr_r(1),
      I3 => N772,
      O => inst_base_module_o_DBus_cmp_eq0004
    );
  inst_base_module_o_DBus_mux0002_9_SW0 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => inst_base_module_varindex0000(9),
      I2 => inst_base_module_irq_mask(9),
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => N197
    );
  inst_base_module_o_DBus_mux0002_7_SW0 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => inst_base_module_varindex0000(7),
      I2 => inst_base_module_irq_mask(7),
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => N199
    );
  inst_base_module_o_DBus_mux0002_5_SW0 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => inst_base_module_varindex0000(5),
      I2 => inst_base_module_irq_mask(5),
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => N201
    );
  inst_base_module_o_DBus_mux0002_31_SW0 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => inst_base_module_varindex0000(31),
      I2 => inst_base_module_irq_mask(31),
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => N203
    );
  inst_base_module_o_DBus_mux0002_30_SW0 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => inst_base_module_varindex0000(30),
      I2 => inst_base_module_irq_mask(30),
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => N205
    );
  inst_base_module_o_DBus_mux0002_29_SW0 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => inst_base_module_varindex0000(29),
      I2 => inst_base_module_irq_mask(29),
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => N207
    );
  inst_base_module_o_DBus_mux0002_15_SW0 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => inst_base_module_varindex0000(15),
      I2 => inst_base_module_irq_mask(15),
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => N213
    );
  inst_base_module_o_DBus_mux0002_14_SW0 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => inst_base_module_varindex0000(14),
      I2 => inst_base_module_irq_mask(14),
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => N215
    );
  inst_base_module_o_DBus_mux0002_13_SW0 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => inst_base_module_varindex0000(13),
      I2 => inst_base_module_irq_mask(13),
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => N217
    );
  inst_base_module_o_DBus_mux0002_12_SW0 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => inst_base_module_varindex0000(12),
      I2 => inst_base_module_irq_mask(12),
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => N219
    );
  inst_base_module_o_DBus_mux0002_10_SW0 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => inst_base_module_varindex0000(10),
      I2 => inst_base_module_irq_mask(10),
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => N221
    );
  mb_altera_core_s_dma_num_samples_mux0000_7_SW0 : LUT3
    generic map(
      INIT => X"7F"
    )
    port map (
      I0 => N799,
      I1 => mb_altera_core_s_dma_num_samples(1),
      I2 => mb_altera_core_s_dma_num_samples(0),
      O => N223
    );
  mb_altera_core_Write_Fifo_Data_in_32_mux0000_SW0 : LUT4
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => mb_altera_core_N15,
      I1 => inst_emif_iface_addr_r(1),
      I2 => inst_emif_iface_addr_r(2),
      I3 => mb_altera_core_Write_Fifo_ff,
      O => N229
    );
  mb_altera_core_Write_Fifo_Data_in_32_mux0000 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_Data_in(32),
      I1 => mb_altera_core_Write_Fifo_Data_in_10_or0000,
      I2 => N229,
      I3 => mb_altera_core_N13,
      O => mb_altera_core_Write_Fifo_Data_in_32_mux0000_2121
    );
  mb_altera_core_s_dma_num_samples_mux0000_6_SW1 : LUT4
    generic map(
      INIT => X"D555"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I1 => mb_altera_core_s_dma_num_samples(1),
      I2 => mb_altera_core_s_dma_num_samples(0),
      I3 => mb_altera_core_s_dma_num_samples(2),
      O => N234
    );
  mb_altera_core_s_dma_num_samples_mux0000_3_SW1 : LUT4
    generic map(
      INIT => X"D555"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I1 => mb_altera_core_s_dma_num_samples(5),
      I2 => mb_altera_core_s_dma_num_samples(4),
      I3 => mb_altera_core_Madd_s_dma_num_samples_addsub0000_cy_3_Q,
      O => N237
    );
  inst_base_module_key_state_cmp_eq000321 : LUT4
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => inst_emif_iface_edi_r(22),
      I1 => inst_emif_iface_edi_r(20),
      I2 => inst_emif_iface_edi_r(21),
      I3 => inst_emif_iface_edi_r(23),
      O => inst_base_module_key_state_cmp_eq000321_780
    );
  inst_base_module_o_DBus_mux0002_28_18 : LUT4
    generic map(
      INIT => X"EA62"
    )
    port map (
      I0 => inst_emif_iface_addr_r(1),
      I1 => inst_emif_iface_addr_r(0),
      I2 => inst_base_module_irq_mask(28),
      I3 => inst_base_module_wd_rst_952,
      O => inst_base_module_o_DBus_mux0002_28_18_858
    );
  inst_base_module_o_DBus_mux0002_28_31 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_base_module_varindex0000(28),
      I1 => inst_emif_iface_addr_r(4),
      I2 => inst_base_module_o_DBus_mux0002_28_18_858,
      I3 => inst_base_module_o_DBus_mux0002_28_19_859,
      O => inst_base_module_o_DBus_mux0002_28_31_860
    );
  mb_altera_core_o_DBus_29_mux000015 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => mb_altera_core_o_DBus_29_mux000013_2365,
      I1 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      I2 => mb_altera_core_Read_Fifo_Data_in(29),
      O => mb_altera_core_o_DBus_29_mux000015_2366
    );
  mb_altera_core_o_DBus_29_mux000040 : LUT3
    generic map(
      INIT => X"08"
    )
    port map (
      I0 => mb_altera_core_N40,
      I1 => mb_altera_core_o_DBus(29),
      I2 => inst_emif_iface_addr_r(2),
      O => mb_altera_core_o_DBus_29_mux000040_2368
    );
  mb_altera_core_o_DBus_7_mux000215 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => mb_altera_core_o_DBus_7_mux000213_2427,
      I1 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      I2 => mb_altera_core_Read_Fifo_Data_in(7),
      O => mb_altera_core_o_DBus_7_mux000215_2428
    );
  mb_altera_core_o_DBus_7_mux000238 : LUT4
    generic map(
      INIT => X"9810"
    )
    port map (
      I0 => inst_emif_iface_addr_r(2),
      I1 => inst_emif_iface_addr_r(3),
      I2 => mb_altera_core_Flash_Read_Reg(7),
      I3 => mb_altera_core_s_dma_space_mask(7),
      O => mb_altera_core_o_DBus_7_mux000238_2429
    );
  mb_altera_core_o_DBus_7_mux000254 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => inst_emif_iface_addr_r(3),
      I1 => mb_altera_core_s_dma_timeout_threshold(7),
      I2 => mb_altera_core_o_user_io(7),
      O => mb_altera_core_o_DBus_7_mux000254_2430
    );
  mb_altera_core_o_DBus_7_mux000264 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => mb_altera_core_N42,
      I1 => mb_altera_core_o_DBus_7_mux000238_2429,
      I2 => mb_altera_core_o_DBus_7_mux000254_2430,
      I3 => mb_altera_core_o_DBus_4_mux00029,
      O => mb_altera_core_o_DBus_7_mux000264_2431
    );
  mb_altera_core_o_DBus_5_mux000215 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => mb_altera_core_o_DBus_5_mux000213_2411,
      I1 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      I2 => mb_altera_core_Read_Fifo_Data_in(5),
      O => mb_altera_core_o_DBus_5_mux000215_2412
    );
  mb_altera_core_o_DBus_5_mux000238 : LUT4
    generic map(
      INIT => X"9810"
    )
    port map (
      I0 => inst_emif_iface_addr_r(2),
      I1 => inst_emif_iface_addr_r(3),
      I2 => mb_altera_core_Flash_Read_Reg(5),
      I3 => mb_altera_core_s_dma_space_mask(5),
      O => mb_altera_core_o_DBus_5_mux000238_2413
    );
  mb_altera_core_o_DBus_5_mux000254 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => inst_emif_iface_addr_r(3),
      I1 => mb_altera_core_s_dma_timeout_threshold(5),
      I2 => mb_altera_core_o_user_io(5),
      O => mb_altera_core_o_DBus_5_mux000254_2414
    );
  mb_altera_core_o_DBus_5_mux000264 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => mb_altera_core_N42,
      I1 => mb_altera_core_o_DBus_5_mux000238_2413,
      I2 => mb_altera_core_o_DBus_5_mux000254_2414,
      I3 => mb_altera_core_o_DBus_4_mux00029,
      O => mb_altera_core_o_DBus_5_mux000264_2415
    );
  mb_altera_core_o_DBus_4_mux000239 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_N42,
      I1 => inst_emif_iface_addr_r(3),
      I2 => mb_altera_core_s_dma_space_mask(4),
      O => mb_altera_core_o_DBus_4_mux000239_2406
    );
  mb_altera_core_o_DBus_4_mux000293 : LUT4
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => mb_altera_core_N42,
      I1 => mb_altera_core_Flash_Read_Reg(4),
      I2 => inst_emif_iface_addr_r(3),
      I3 => inst_emif_iface_addr_r(2),
      O => mb_altera_core_o_DBus_4_mux000293_2409
    );
  mb_altera_core_o_DBus_4_mux0002108 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => mb_altera_core_o_DBus_4_mux000280_2407,
      I1 => mb_altera_core_s_dma_num_samples(2),
      I2 => mb_altera_core_o_DBus_10_cmp_eq0007,
      I3 => mb_altera_core_o_DBus_4_mux000293_2409,
      O => mb_altera_core_o_DBus_4_mux0002108_2400
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_30_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(1),
      I3 => mb_altera_core_s_dma_timeout_cntr(1),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(30)
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_29_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(2),
      I3 => mb_altera_core_s_dma_timeout_cntr(2),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(29)
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_28_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(3),
      I3 => mb_altera_core_s_dma_timeout_cntr(3),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(28)
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_27_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(4),
      I3 => mb_altera_core_s_dma_timeout_cntr(4),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(27)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_In1 : LUT4
    generic map(
      INIT => X"77F7"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N5,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(4),
      O => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_In1_1426
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_In2 : LUT4
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt(0),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt(1),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      O => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_In2_1427
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_In_f5 : MUXF5
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_In2_1427,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_In1_1426,
      S => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      O => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_In
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_2_4 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_col(2),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_37,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N38,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(2),
      O => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_2_4_1243
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_2_28 : LUT4
    generic map(
      INIT => X"2A6A"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_col(2),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_col(0),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_col(1),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      O => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_2_28_1241
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_26_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(5),
      I3 => mb_altera_core_s_dma_timeout_cntr(5),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(26)
    );
  mb_altera_core_ver_rd_not00011 : LUT3
    generic map(
      INIT => X"7F"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0000,
      I1 => inst_emif_iface_cs_r_8_Q,
      I2 => inst_emif_iface_rd_r_1175,
      O => mb_altera_core_ver_rd_not0001
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_25_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(6),
      I3 => mb_altera_core_s_dma_timeout_cntr(6),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(25)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_In31 : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_arb_FSM_FFd2_1419,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_arb_FSM_FFd3_1420,
      O => inst_emif_iface_sdram_dma_std_dma_engine_N42
    );
  mb_altera_core_o_DBus_31_cmp_eq00001 : LUT3
    generic map(
      INIT => X"08"
    )
    port map (
      I0 => mb_altera_core_N13,
      I1 => inst_emif_iface_addr_r(2),
      I2 => inst_emif_iface_addr_r(1),
      O => mb_altera_core_o_DBus_31_cmp_eq0000
    );
  mb_altera_core_o_DBus_10_cmp_eq00051 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => inst_emif_iface_addr_r(2),
      I1 => inst_emif_iface_addr_r(3),
      I2 => N773,
      O => mb_altera_core_o_DBus_10_cmp_eq0005
    );
  mb_altera_core_o_DBus_10_cmp_eq00041 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => inst_emif_iface_addr_r(1),
      I1 => inst_emif_iface_addr_r(2),
      I2 => N778,
      O => mb_altera_core_o_DBus_10_cmp_eq0004
    );
  mb_altera_core_o_DBus_0_cmp_eq00001 : LUT3
    generic map(
      INIT => X"04"
    )
    port map (
      I0 => inst_emif_iface_addr_r(2),
      I1 => N782,
      I2 => inst_emif_iface_addr_r(4),
      O => mb_altera_core_o_DBus_0_cmp_eq0000
    );
  inst_base_module_wd_counter_mux0000_7_1 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => inst_base_module_N1,
      I1 => inst_base_module_wd_counter_addsub0000(7),
      I2 => inst_base_module_wd_timeout(7),
      O => inst_base_module_wd_counter_mux0000(7)
    );
  inst_base_module_wd_counter_mux0000_6_1 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => inst_base_module_N1,
      I1 => inst_base_module_wd_counter_addsub0000(6),
      I2 => inst_base_module_wd_timeout(6),
      O => inst_base_module_wd_counter_mux0000(6)
    );
  inst_base_module_wd_counter_mux0000_5_1 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => inst_base_module_N1,
      I1 => inst_base_module_wd_counter_addsub0000(5),
      I2 => inst_base_module_wd_timeout(5),
      O => inst_base_module_wd_counter_mux0000(5)
    );
  inst_base_module_wd_counter_mux0000_4_1 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => inst_base_module_N1,
      I1 => inst_base_module_wd_counter_addsub0000(4),
      I2 => inst_base_module_wd_timeout(4),
      O => inst_base_module_wd_counter_mux0000(4)
    );
  inst_base_module_wd_counter_mux0000_3_1 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => inst_base_module_N1,
      I1 => inst_base_module_wd_counter_addsub0000(3),
      I2 => inst_base_module_wd_timeout(3),
      O => inst_base_module_wd_counter_mux0000(3)
    );
  inst_base_module_wd_counter_mux0000_2_1 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => inst_base_module_N1,
      I1 => inst_base_module_wd_counter_addsub0000(2),
      I2 => inst_base_module_wd_timeout(2),
      O => inst_base_module_wd_counter_mux0000(2)
    );
  inst_base_module_wd_counter_mux0000_1_1 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => inst_base_module_N1,
      I1 => inst_base_module_wd_counter_addsub0000(1),
      I2 => inst_base_module_wd_timeout(1),
      O => inst_base_module_wd_counter_mux0000(1)
    );
  inst_base_module_wd_counter_mux0000_0_2 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => inst_base_module_N1,
      I1 => inst_base_module_wd_counter_addsub0000(0),
      I2 => inst_base_module_wd_timeout(0),
      O => inst_base_module_wd_counter_mux0000(0)
    );
  mb_altera_core_o_DBus_10_cmp_eq00001 : LUT4
    generic map(
      INIT => X"0100"
    )
    port map (
      I0 => inst_emif_iface_addr_r(0),
      I1 => inst_emif_iface_addr_r(2),
      I2 => inst_emif_iface_addr_r(1),
      I3 => inst_base_module_irq_mask_and0000,
      O => inst_base_module_o_DBus_cmp_eq0000
    );
  mb_altera_core_o_DBus_0_mux000221 : LUT4
    generic map(
      INIT => X"EAC0"
    )
    port map (
      I0 => mb_altera_core_version_o_data_0_Q,
      I1 => inst_base_module_irq_mask_and0000,
      I2 => mb_altera_core_N12,
      I3 => inst_base_module_o_DBus_cmp_eq0000,
      O => mb_altera_core_N17
    );
  mb_altera_core_o_DBus_10_cmp_eq0003 : LUT4
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => inst_emif_iface_addr_r(3),
      I2 => inst_emif_iface_addr_r(1),
      I3 => N775,
      O => mb_altera_core_o_DBus_10_cmp_eq0003_2250
    );
  inst_base_module_key_state_mux0003_2_SW0 : LUT3
    generic map(
      INIT => X"AE"
    )
    port map (
      I0 => inst_emif_iface_edi_r(0),
      I1 => inst_base_module_key_state(0),
      I2 => N796,
      O => N248
    );
  inst_base_module_key_state_mux0003_2_Q : LUT4
    generic map(
      INIT => X"FFAE"
    )
    port map (
      I0 => N248,
      I1 => inst_base_module_key_state(1),
      I2 => inst_base_module_key_state_cmp_eq0004,
      I3 => inst_base_module_key_state(2),
      O => inst_base_module_key_state_mux0003(2)
    );
  inst_base_module_o_DBus_mux0002_8_4 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0006,
      I1 => inst_base_module_wd_timeout(8),
      I2 => inst_base_module_varindex0000(8),
      I3 => inst_emif_iface_addr_r(4),
      O => inst_base_module_o_DBus_mux0002_8_4_874
    );
  inst_base_module_o_DBus_mux0002_8_10 : LUT4
    generic map(
      INIT => X"AA80"
    )
    port map (
      I0 => inst_base_module_irq_mask(8),
      I1 => irq_map_5_0_Q,
      I2 => N774,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_o_DBus_mux0002_8_10_873
    );
  inst_base_module_o_DBus_mux0002_2_4 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0006,
      I1 => inst_base_module_wd_timeout(2),
      I2 => inst_emif_iface_addr_r(4),
      I3 => inst_base_module_varindex0000(2),
      O => inst_base_module_o_DBus_mux0002_2_4_863
    );
  inst_base_module_o_DBus_mux0002_11_4 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0006,
      I1 => inst_base_module_wd_timeout(11),
      I2 => inst_base_module_varindex0000(11),
      I3 => inst_emif_iface_addr_r(4),
      O => inst_base_module_o_DBus_mux0002_11_4_834
    );
  inst_base_module_o_DBus_mux0002_11_10 : LUT4
    generic map(
      INIT => X"AA80"
    )
    port map (
      I0 => inst_base_module_irq_mask(11),
      I1 => mb_altera_core_s_dma_irq_2663,
      I2 => inst_base_module_o_DBus_cmp_eq0003,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_o_DBus_mux0002_11_10_833
    );
  mb_altera_core_Flash_State_FSM_FFd7_In9 : LUT3
    generic map(
      INIT => X"7F"
    )
    port map (
      I0 => inst_emif_iface_cs_r_8_Q,
      I1 => inst_emif_iface_wr_r_1474,
      I2 => mb_altera_core_flash_enable_2235,
      O => mb_altera_core_Flash_State_FSM_FFd7_In9_1611
    );
  mb_altera_core_o_DBus_30_mux000014 : LUT3
    generic map(
      INIT => X"F8"
    )
    port map (
      I0 => mb_altera_core_version_o_data_2_Q,
      I1 => inst_base_module_o_DBus_cmp_eq0000,
      I2 => mb_altera_core_o_DBus_30_mux00008_2384,
      O => mb_altera_core_o_DBus_30_mux000014_2380
    );
  mb_altera_core_o_DBus_30_mux000033 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => inst_emif_iface_addr_r(3),
      I2 => mb_altera_core_s_dma_timeout_threshold(30),
      I3 => mb_altera_core_o_user_io(30),
      O => mb_altera_core_o_DBus_30_mux000033_2381
    );
  mb_altera_core_o_DBus_13_mux000033 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => inst_emif_iface_addr_r(3),
      I2 => mb_altera_core_s_dma_timeout_threshold(13),
      I3 => mb_altera_core_o_user_io(13),
      O => mb_altera_core_o_DBus_13_mux000033_2273
    );
  mb_altera_core_o_DBus_28_mux000052 : LUT4
    generic map(
      INIT => X"FEFC"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(28),
      I1 => mb_altera_core_o_DBus_28_mux000032_2360,
      I2 => mb_altera_core_o_DBus_28_mux000021_2359,
      I3 => mb_altera_core_o_DBus_10_cmp_eq0005,
      O => mb_altera_core_o_DBus_28_mux000052_2362
    );
  mb_altera_core_o_DBus_9_mux000213 : LUT4
    generic map(
      INIT => X"CA00"
    )
    port map (
      I0 => mb_altera_core_s_dma_start_addr(7),
      I1 => mb_altera_core_s_altera_rdfifo_rd_cnt(9),
      I2 => inst_emif_iface_addr_r(1),
      I3 => N770,
      O => mb_altera_core_o_DBus_9_mux000213_2443
    );
  mb_altera_core_o_DBus_9_mux000247 : LUT4
    generic map(
      INIT => X"FFEC"
    )
    port map (
      I0 => mb_altera_core_s_dma_num_samples(7),
      I1 => mb_altera_core_o_DBus_9_mux000232_2445,
      I2 => mb_altera_core_o_DBus_10_cmp_eq0007,
      I3 => mb_altera_core_o_DBus_9_mux000215_2444,
      O => mb_altera_core_o_DBus_9_mux000247_2446
    );
  mb_altera_core_o_DBus_9_mux000260 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => mb_altera_core_o_DBus_10_cmp_eq0004,
      I1 => mb_altera_core_s_dma_base_addr(7),
      I2 => mb_altera_core_o_DBus_10_cmp_eq0005,
      I3 => mb_altera_core_s_dma_space_mask(9),
      O => mb_altera_core_o_DBus_9_mux000260_2447
    );
  mb_altera_core_o_DBus_31_mux000013 : LUT4
    generic map(
      INIT => X"CA00"
    )
    port map (
      I0 => mb_altera_core_s_dma_start_addr(29),
      I1 => mb_altera_core_s_dma_irq_2663,
      I2 => inst_emif_iface_addr_r(1),
      I3 => mb_altera_core_N41,
      O => mb_altera_core_o_DBus_31_mux000013_2387
    );
  mb_altera_core_o_DBus_31_mux000047 : LUT4
    generic map(
      INIT => X"FFEC"
    )
    port map (
      I0 => mb_altera_core_o_DBus(31),
      I1 => mb_altera_core_o_DBus_31_mux000032_2389,
      I2 => mb_altera_core_o_DBus_10_cmp_eq0007,
      I3 => mb_altera_core_o_DBus_31_mux000015_2388,
      O => mb_altera_core_o_DBus_31_mux000047_2390
    );
  mb_altera_core_o_DBus_31_mux000060 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => mb_altera_core_o_DBus_10_cmp_eq0005,
      I1 => mb_altera_core_s_dma_space_mask(31),
      I2 => mb_altera_core_o_DBus_31_cmp_eq0000,
      I3 => mb_altera_core_s_err_invalid_rd_3014,
      O => mb_altera_core_o_DBus_31_mux000060_2391
    );
  mb_altera_core_o_DBus_26_mux000013 : LUT4
    generic map(
      INIT => X"CA00"
    )
    port map (
      I0 => mb_altera_core_s_dma_start_addr(24),
      I1 => mb_altera_core_Write_fifo_wr_Cnt(6),
      I2 => inst_emif_iface_addr_r(1),
      I3 => mb_altera_core_N41,
      O => mb_altera_core_o_DBus_26_mux000013_2348
    );
  mb_altera_core_o_DBus_26_mux000047 : LUT4
    generic map(
      INIT => X"FFEC"
    )
    port map (
      I0 => mb_altera_core_o_DBus(26),
      I1 => mb_altera_core_o_DBus_26_mux000032_2350,
      I2 => mb_altera_core_o_DBus_10_cmp_eq0007,
      I3 => mb_altera_core_o_DBus_26_mux000015_2349,
      O => mb_altera_core_o_DBus_26_mux000047_2351
    );
  mb_altera_core_o_DBus_26_mux000060 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => mb_altera_core_o_DBus_10_cmp_eq0004,
      I1 => mb_altera_core_s_dma_base_addr(24),
      I2 => mb_altera_core_o_DBus_10_cmp_eq0005,
      I3 => mb_altera_core_s_dma_space_mask(26),
      O => mb_altera_core_o_DBus_26_mux000060_2352
    );
  mb_altera_core_o_DBus_25_mux000013 : LUT4
    generic map(
      INIT => X"CA00"
    )
    port map (
      I0 => mb_altera_core_s_dma_start_addr(23),
      I1 => mb_altera_core_Write_fifo_wr_Cnt(5),
      I2 => inst_emif_iface_addr_r(1),
      I3 => mb_altera_core_N41,
      O => mb_altera_core_o_DBus_25_mux000013_2342
    );
  mb_altera_core_o_DBus_25_mux000047 : LUT4
    generic map(
      INIT => X"FFEC"
    )
    port map (
      I0 => mb_altera_core_o_DBus(25),
      I1 => mb_altera_core_o_DBus_25_mux000032_2344,
      I2 => mb_altera_core_o_DBus_10_cmp_eq0007,
      I3 => mb_altera_core_o_DBus_25_mux000015_2343,
      O => mb_altera_core_o_DBus_25_mux000047_2345
    );
  mb_altera_core_o_DBus_25_mux000060 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => mb_altera_core_o_DBus_10_cmp_eq0004,
      I1 => mb_altera_core_s_dma_base_addr(23),
      I2 => mb_altera_core_o_DBus_10_cmp_eq0005,
      I3 => mb_altera_core_s_dma_space_mask(25),
      O => mb_altera_core_o_DBus_25_mux000060_2346
    );
  mb_altera_core_o_DBus_23_mux000013 : LUT4
    generic map(
      INIT => X"CA00"
    )
    port map (
      I0 => mb_altera_core_s_dma_start_addr(21),
      I1 => mb_altera_core_Write_fifo_wr_Cnt(3),
      I2 => inst_emif_iface_addr_r(1),
      I3 => mb_altera_core_N41,
      O => mb_altera_core_o_DBus_23_mux000013_2331
    );
  mb_altera_core_o_DBus_23_mux000047 : LUT4
    generic map(
      INIT => X"FFEC"
    )
    port map (
      I0 => mb_altera_core_o_DBus(23),
      I1 => mb_altera_core_o_DBus_23_mux000032_2333,
      I2 => mb_altera_core_o_DBus_10_cmp_eq0007,
      I3 => mb_altera_core_o_DBus_23_mux000015_2332,
      O => mb_altera_core_o_DBus_23_mux000047_2334
    );
  mb_altera_core_o_DBus_23_mux000060 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => mb_altera_core_o_DBus_10_cmp_eq0004,
      I1 => mb_altera_core_s_dma_base_addr(21),
      I2 => mb_altera_core_o_DBus_10_cmp_eq0005,
      I3 => mb_altera_core_s_dma_space_mask(23),
      O => mb_altera_core_o_DBus_23_mux000060_2335
    );
  mb_altera_core_o_DBus_21_mux000013 : LUT4
    generic map(
      INIT => X"CA00"
    )
    port map (
      I0 => mb_altera_core_s_dma_start_addr(19),
      I1 => mb_altera_core_Write_fifo_wr_Cnt(1),
      I2 => inst_emif_iface_addr_r(1),
      I3 => mb_altera_core_N41,
      O => mb_altera_core_o_DBus_21_mux000013_2320
    );
  mb_altera_core_o_DBus_21_mux000047 : LUT4
    generic map(
      INIT => X"FFEC"
    )
    port map (
      I0 => mb_altera_core_o_DBus(21),
      I1 => mb_altera_core_o_DBus_21_mux000032_2322,
      I2 => mb_altera_core_o_DBus_10_cmp_eq0007,
      I3 => mb_altera_core_o_DBus_21_mux000015_2321,
      O => mb_altera_core_o_DBus_21_mux000047_2323
    );
  mb_altera_core_o_DBus_21_mux000060 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => mb_altera_core_o_DBus_10_cmp_eq0004,
      I1 => mb_altera_core_s_dma_base_addr(19),
      I2 => mb_altera_core_o_DBus_10_cmp_eq0005,
      I3 => mb_altera_core_s_dma_space_mask(21),
      O => mb_altera_core_o_DBus_21_mux000060_2324
    );
  mb_altera_core_o_DBus_20_mux000013 : LUT4
    generic map(
      INIT => X"CA00"
    )
    port map (
      I0 => mb_altera_core_s_dma_start_addr(18),
      I1 => mb_altera_core_Write_fifo_wr_Cnt(0),
      I2 => inst_emif_iface_addr_r(1),
      I3 => mb_altera_core_N41,
      O => mb_altera_core_o_DBus_20_mux000013_2314
    );
  mb_altera_core_o_DBus_20_mux000047 : LUT4
    generic map(
      INIT => X"FFEC"
    )
    port map (
      I0 => mb_altera_core_o_DBus(20),
      I1 => mb_altera_core_o_DBus_20_mux000032_2316,
      I2 => mb_altera_core_o_DBus_10_cmp_eq0007,
      I3 => mb_altera_core_o_DBus_20_mux000015_2315,
      O => mb_altera_core_o_DBus_20_mux000047_2317
    );
  mb_altera_core_o_DBus_20_mux000060 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => mb_altera_core_o_DBus_10_cmp_eq0004,
      I1 => mb_altera_core_s_dma_base_addr(18),
      I2 => mb_altera_core_o_DBus_10_cmp_eq0005,
      I3 => mb_altera_core_s_dma_space_mask(20),
      O => mb_altera_core_o_DBus_20_mux000060_2318
    );
  mb_altera_core_o_DBus_12_mux000013 : LUT4
    generic map(
      INIT => X"CA00"
    )
    port map (
      I0 => mb_altera_core_s_dma_start_addr(10),
      I1 => mb_altera_core_s_altera_rdfifo_rd_cnt(12),
      I2 => inst_emif_iface_addr_r(1),
      I3 => mb_altera_core_N41,
      O => mb_altera_core_o_DBus_12_mux000013_2266
    );
  mb_altera_core_o_DBus_12_mux000047 : LUT4
    generic map(
      INIT => X"FFEC"
    )
    port map (
      I0 => mb_altera_core_o_DBus(12),
      I1 => mb_altera_core_o_DBus_12_mux000032_2268,
      I2 => mb_altera_core_o_DBus_10_cmp_eq0007,
      I3 => mb_altera_core_o_DBus_12_mux000015_2267,
      O => mb_altera_core_o_DBus_12_mux000047_2269
    );
  mb_altera_core_o_DBus_12_mux000060 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => mb_altera_core_o_DBus_10_cmp_eq0004,
      I1 => mb_altera_core_s_dma_base_addr(10),
      I2 => mb_altera_core_o_DBus_10_cmp_eq0005,
      I3 => mb_altera_core_s_dma_space_mask(12),
      O => mb_altera_core_o_DBus_12_mux000060_2270
    );
  mb_altera_core_o_DBus_11_mux000213 : LUT4
    generic map(
      INIT => X"CA00"
    )
    port map (
      I0 => mb_altera_core_s_dma_start_addr(9),
      I1 => mb_altera_core_s_altera_rdfifo_rd_cnt(11),
      I2 => inst_emif_iface_addr_r(1),
      I3 => mb_altera_core_N41,
      O => mb_altera_core_o_DBus_11_mux000213_2260
    );
  mb_altera_core_o_DBus_11_mux000247 : LUT4
    generic map(
      INIT => X"FFEC"
    )
    port map (
      I0 => mb_altera_core_s_dma_num_samples(9),
      I1 => mb_altera_core_o_DBus_11_mux000232_2262,
      I2 => mb_altera_core_o_DBus_10_cmp_eq0007,
      I3 => mb_altera_core_o_DBus_11_mux000215_2261,
      O => mb_altera_core_o_DBus_11_mux000247_2263
    );
  mb_altera_core_o_DBus_11_mux000260 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => mb_altera_core_o_DBus_10_cmp_eq0004,
      I1 => mb_altera_core_s_dma_base_addr(9),
      I2 => mb_altera_core_o_DBus_10_cmp_eq0005,
      I3 => mb_altera_core_s_dma_space_mask(11),
      O => mb_altera_core_o_DBus_11_mux000260_2264
    );
  mb_altera_core_o_DBus_10_mux000213 : LUT4
    generic map(
      INIT => X"CA00"
    )
    port map (
      I0 => mb_altera_core_s_dma_start_addr(8),
      I1 => mb_altera_core_s_altera_rdfifo_rd_cnt(10),
      I2 => inst_emif_iface_addr_r(1),
      I3 => mb_altera_core_N41,
      O => mb_altera_core_o_DBus_10_mux000213_2254
    );
  mb_altera_core_o_DBus_10_mux000247 : LUT4
    generic map(
      INIT => X"FFEC"
    )
    port map (
      I0 => mb_altera_core_s_dma_num_samples(8),
      I1 => mb_altera_core_o_DBus_10_mux000232_2256,
      I2 => mb_altera_core_o_DBus_10_cmp_eq0007,
      I3 => mb_altera_core_o_DBus_10_mux000215_2255,
      O => mb_altera_core_o_DBus_10_mux000247_2257
    );
  mb_altera_core_o_DBus_10_mux000260 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => mb_altera_core_o_DBus_10_cmp_eq0004,
      I1 => mb_altera_core_s_dma_base_addr(8),
      I2 => mb_altera_core_o_DBus_10_cmp_eq0005,
      I3 => mb_altera_core_s_dma_space_mask(10),
      O => mb_altera_core_o_DBus_10_mux000260_2258
    );
  mb_altera_core_o_DBus_1_mux000213 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => inst_emif_iface_addr_r(3),
      I2 => mb_altera_core_s_dma_timeout_threshold(1),
      I3 => mb_altera_core_o_user_io(1),
      O => mb_altera_core_o_DBus_1_mux000213_2307
    );
  mb_altera_core_o_DBus_27_mux000013 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_N41,
      I1 => inst_emif_iface_addr_r(1),
      I2 => mb_altera_core_s_dma_start_addr(25),
      I3 => mb_altera_core_Write_fifo_wr_Cnt(7),
      O => mb_altera_core_o_DBus_27_mux000013_2354
    );
  mb_altera_core_o_DBus_27_mux000033 : LUT4
    generic map(
      INIT => X"ECA0"
    )
    port map (
      I0 => mb_altera_core_s_dma_base_addr(25),
      I1 => mb_altera_core_o_DBus(27),
      I2 => mb_altera_core_o_DBus_10_cmp_eq0004,
      I3 => N781,
      O => mb_altera_core_o_DBus_27_mux000033_2355
    );
  mb_altera_core_o_DBus_8_mux000222 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0000,
      I1 => mb_altera_core_s_dma_num_samples(6),
      I2 => mb_altera_core_o_DBus_10_cmp_eq0007,
      I3 => mb_altera_core_o_DBus_8_mux000212_2438,
      O => mb_altera_core_o_DBus_8_mux000222_2439
    );
  mb_altera_core_o_DBus_8_mux000257 : LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => inst_emif_iface_addr_r(2),
      I1 => inst_emif_iface_addr_r(0),
      I2 => mb_altera_core_Read_Fifo_Data_in(8),
      I3 => inst_emif_iface_addr_r(1),
      O => mb_altera_core_o_DBus_8_mux000257_2440
    );
  mb_altera_core_o_DBus_6_mux000224 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => mb_altera_core_o_DBus_6_mux000222_2419,
      I1 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      I2 => mb_altera_core_Read_Fifo_Data_in(6),
      O => mb_altera_core_o_DBus_6_mux000224_2420
    );
  mb_altera_core_o_DBus_6_mux000247 : LUT4
    generic map(
      INIT => X"9810"
    )
    port map (
      I0 => inst_emif_iface_addr_r(2),
      I1 => inst_emif_iface_addr_r(3),
      I2 => mb_altera_core_Flash_Read_Reg(6),
      I3 => mb_altera_core_s_dma_space_mask(6),
      O => mb_altera_core_o_DBus_6_mux000247_2422
    );
  mb_altera_core_o_DBus_6_mux000263 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => inst_emif_iface_addr_r(3),
      I1 => mb_altera_core_s_dma_timeout_threshold(6),
      I2 => mb_altera_core_o_user_io(6),
      O => mb_altera_core_o_DBus_6_mux000263_2423
    );
  mb_altera_core_o_DBus_6_mux000273 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => mb_altera_core_N42,
      I1 => mb_altera_core_o_DBus_6_mux000247_2422,
      I2 => mb_altera_core_o_DBus_6_mux000263_2423,
      I3 => mb_altera_core_o_DBus_4_mux00029,
      O => mb_altera_core_o_DBus_6_mux000273_2424
    );
  mb_altera_core_o_DBus_0_mux00022 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_Msub_s_dma_count_share0000_cy_0_Q,
      I1 => inst_emif_iface_addr_r(1),
      I2 => mb_altera_core_N41,
      O => mb_altera_core_o_DBus_0_mux00022_2243
    );
  mb_altera_core_o_DBus_0_mux000215 : LUT4
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => mb_altera_core_N42,
      I1 => mb_altera_core_Flash_Read_Reg(0),
      I2 => inst_emif_iface_addr_r(3),
      I3 => inst_emif_iface_addr_r(2),
      O => mb_altera_core_o_DBus_0_mux000215_2242
    );
  mb_altera_core_o_DBus_0_mux000259 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => N777,
      I1 => inst_emif_iface_addr_r(3),
      I2 => mb_altera_core_s_dma_timeout_threshold(0),
      I3 => mb_altera_core_o_user_io(0),
      O => mb_altera_core_o_DBus_0_mux000259_2245
    );
  mb_altera_core_o_DBus_0_mux0002104 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_o_DBus_0_cmp_eq0000,
      I1 => mb_altera_core_Flash_State_FSM_FFd7_1607,
      I2 => mb_altera_core_s_dma_space_mask(0),
      I3 => mb_altera_core_o_DBus_10_cmp_eq0005,
      O => mb_altera_core_o_DBus_0_mux0002104_2240
    );
  mb_altera_core_o_DBus_3_mux000013 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_N13,
      I1 => inst_emif_iface_addr_r(1),
      I2 => mb_altera_core_o_DBus(3),
      I3 => mb_altera_core_s_dma_base_addr(1),
      O => mb_altera_core_o_DBus_3_mux000013_2394
    );
  mb_altera_core_o_DBus_3_mux000016 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_N42,
      I1 => inst_emif_iface_addr_r(3),
      I2 => mb_altera_core_s_dma_space_mask(3),
      O => mb_altera_core_o_DBus_3_mux000016_2396
    );
  mb_altera_core_o_DBus_3_mux0000109 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_N40,
      I1 => inst_emif_iface_addr_r(4),
      I2 => i_altera_cdone_IBUF_485,
      I3 => mb_altera_core_s_dma_num_samples(1),
      O => mb_altera_core_o_DBus_3_mux0000109_2392
    );
  mb_altera_core_o_DBus_3_mux0000115 : LUT3
    generic map(
      INIT => X"08"
    )
    port map (
      I0 => mb_altera_core_N42,
      I1 => mb_altera_core_Flash_Read_Reg(3),
      I2 => inst_emif_iface_addr_r(3),
      O => mb_altera_core_o_DBus_3_mux0000115_2393
    );
  mb_altera_core_o_DBus_2_mux000213 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => inst_emif_iface_addr_r(3),
      I2 => mb_altera_core_s_dma_timeout_threshold(2),
      I3 => mb_altera_core_o_user_io(2),
      O => mb_altera_core_o_DBus_2_mux000213_2372
    );
  mb_altera_core_o_DBus_2_mux000230 : LUT3
    generic map(
      INIT => X"F8"
    )
    port map (
      I0 => mb_altera_core_s_dma_num_samples(0),
      I1 => mb_altera_core_o_DBus_10_cmp_eq0007,
      I2 => mb_altera_core_o_DBus_2_mux000222_2373,
      O => mb_altera_core_o_DBus_2_mux000230_2374
    );
  mb_altera_core_o_DBus_2_mux000243 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0000,
      I1 => mb_altera_core_version_o_data_2_Q,
      I2 => i_altera_data0_IBUF_487,
      I3 => mb_altera_core_o_DBus_0_cmp_eq0000,
      O => mb_altera_core_o_DBus_2_mux000243_2375
    );
  mb_altera_core_o_DBus_2_mux000267 : LUT4
    generic map(
      INIT => X"CA00"
    )
    port map (
      I0 => mb_altera_core_s_dma_start_addr(0),
      I1 => mb_altera_core_s_altera_rdfifo_rd_cnt(2),
      I2 => inst_emif_iface_addr_r(1),
      I3 => mb_altera_core_N41,
      O => mb_altera_core_o_DBus_2_mux000267_2376
    );
  inst_base_module_wd_counter_mux0000_8_1 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => inst_base_module_N1,
      I1 => inst_base_module_wd_counter_addsub0000(8),
      I2 => inst_base_module_wd_timeout(8),
      O => inst_base_module_wd_counter_mux0000(8)
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_24_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(7),
      I3 => mb_altera_core_s_dma_timeout_cntr(7),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(24)
    );
  inst_base_module_wd_counter_mux0000_9_1 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => inst_base_module_N1,
      I1 => inst_base_module_wd_counter_addsub0000(9),
      I2 => inst_base_module_wd_timeout(9),
      O => inst_base_module_wd_counter_mux0000(9)
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_23_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(8),
      I3 => mb_altera_core_s_dma_timeout_cntr(8),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(23)
    );
  inst_base_module_wd_counter_mux0000_10_1 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => inst_base_module_N1,
      I1 => inst_base_module_wd_counter_addsub0000(10),
      I2 => inst_base_module_wd_timeout(10),
      O => inst_base_module_wd_counter_mux0000(10)
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_22_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(9),
      I3 => mb_altera_core_s_dma_timeout_cntr(9),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(22)
    );
  inst_base_module_wd_counter_mux0000_11_1 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => inst_base_module_N1,
      I1 => inst_base_module_wd_counter_addsub0000(11),
      I2 => inst_base_module_wd_timeout(11),
      O => inst_base_module_wd_counter_mux0000(11)
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_21_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(10),
      I3 => mb_altera_core_s_dma_timeout_cntr(10),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(21)
    );
  inst_base_module_wd_counter_mux0000_12_1 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => inst_base_module_N1,
      I1 => inst_base_module_wd_counter_addsub0000(12),
      I2 => inst_base_module_wd_timeout(12),
      O => inst_base_module_wd_counter_mux0000(12)
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_20_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(11),
      I3 => mb_altera_core_s_dma_timeout_cntr(11),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(20)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_In211 : LUT4
    generic map(
      INIT => X"AA08"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(4),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_In7_1431,
      O => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_In21_1429
    );
  inst_base_module_wd_counter_mux0000_13_1 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => inst_base_module_N1,
      I1 => inst_base_module_wd_counter_addsub0000(13),
      I2 => inst_base_module_wd_timeout(13),
      O => inst_base_module_wd_counter_mux0000(13)
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_19_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(12),
      I3 => mb_altera_core_s_dma_timeout_cntr(12),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(19)
    );
  inst_base_module_wd_counter_mux0000_14_1 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => inst_base_module_N1,
      I1 => inst_base_module_wd_counter_addsub0000(14),
      I2 => inst_base_module_wd_timeout(14),
      O => inst_base_module_wd_counter_mux0000(14)
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_18_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(13),
      I3 => mb_altera_core_s_dma_timeout_cntr(13),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(18)
    );
  inst_base_module_wd_counter_not00021 : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => inst_base_module_clken100ms_610,
      I1 => N779,
      O => inst_base_module_wd_counter_not0002
    );
  inst_base_module_bank_addr_0_and0000_SW0 : LUT4
    generic map(
      INIT => X"1333"
    )
    port map (
      I0 => inst_base_module_N3,
      I1 => inst_emif_iface_edi_r(0),
      I2 => inst_base_module_key_state(2),
      I3 => inst_base_module_N8,
      O => N254
    );
  inst_base_module_bank_addr_0_and0000 : LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => inst_base_module_and0000_inv,
      I1 => inst_base_module_reset,
      I2 => inst_base_module_key_state_cmp_eq0002,
      I3 => N254,
      O => inst_base_module_bank_addr_0_and0000_607
    );
  inst_base_module_bank_addr_0_and0000110 : LUT4
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => inst_emif_iface_edi_r(28),
      I1 => inst_emif_iface_edi_r(30),
      I2 => inst_emif_iface_edi_r(31),
      I3 => inst_emif_iface_edi_r(29),
      O => inst_base_module_bank_addr_0_and0000110_608
    );
  inst_base_module_bank_addr_0_and0000121 : LUT4
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => inst_emif_iface_edi_r(25),
      I1 => inst_emif_iface_edi_r(27),
      I2 => inst_emif_iface_edi_r(26),
      I3 => inst_emif_iface_edi_r(24),
      O => inst_base_module_bank_addr_0_and0000121_609
    );
  inst_base_module_wd_counter_cmp_eq000025 : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => inst_base_module_wd_counter(4),
      I1 => inst_base_module_wd_counter(5),
      I2 => inst_base_module_wd_counter(6),
      I3 => inst_base_module_wd_counter(7),
      O => inst_base_module_wd_counter_cmp_eq000025_915
    );
  inst_base_module_wd_counter_cmp_eq000049 : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => inst_base_module_wd_counter(8),
      I1 => inst_base_module_wd_counter(9),
      I2 => inst_base_module_wd_counter(10),
      I3 => inst_base_module_wd_counter(11),
      O => inst_base_module_wd_counter_cmp_eq000049_916
    );
  inst_base_module_wd_counter_cmp_eq000062 : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => inst_base_module_wd_counter(12),
      I1 => inst_base_module_wd_counter(13),
      I2 => inst_base_module_wd_counter(14),
      I3 => inst_base_module_wd_counter(15),
      O => inst_base_module_wd_counter_cmp_eq000062_917
    );
  inst_base_module_wd_counter_cmp_eq000076 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => inst_base_module_wd_counter_cmp_eq000012_914,
      I1 => inst_base_module_wd_counter_cmp_eq000025_915,
      I2 => inst_base_module_wd_counter_cmp_eq000049_916,
      I3 => inst_base_module_wd_counter_cmp_eq000062_917,
      O => inst_base_module_wd_counter_cmp_eq0000
    );
  mb_altera_core_Msub_s_dma_count_share0000_cy_6_11 : LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      I0 => mb_altera_core_s_altera_rdfifo_rd_cnt(4),
      I1 => N783,
      I2 => mb_altera_core_s_altera_rdfifo_rd_cnt(5),
      I3 => mb_altera_core_s_altera_rdfifo_rd_cnt(6),
      O => mb_altera_core_Msub_s_dma_count_share0000_cy_6_Q
    );
  mb_altera_core_Madd_s_dma_num_samples_addsub0000_cy_6_11 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => mb_altera_core_s_dma_num_samples(6),
      I1 => mb_altera_core_s_dma_num_samples(5),
      I2 => mb_altera_core_s_dma_num_samples(4),
      I3 => N784,
      O => mb_altera_core_Madd_s_dma_num_samples_addsub0000_cy_6_Q
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_6_61 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(4),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(0),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N16,
      O => inst_emif_iface_sdram_dma_std_dma_engine_N54
    );
  mb_altera_core_s_dma_num_samples_mux0000_0_SW1 : LUT4
    generic map(
      INIT => X"D555"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I1 => mb_altera_core_s_dma_num_samples(8),
      I2 => mb_altera_core_s_dma_num_samples(7),
      I3 => mb_altera_core_Madd_s_dma_num_samples_addsub0000_cy_6_Q,
      O => N260
    );
  inst_base_module_o_DBus_mux0002_1_7 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0006,
      I1 => inst_base_module_wd_timeout(1),
      I2 => inst_emif_iface_addr_r(4),
      I3 => inst_base_module_varindex0000(1),
      O => inst_base_module_o_DBus_mux0002_1_7_849
    );
  inst_base_module_o_DBus_mux0002_3_0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => N785,
      I1 => inst_base_module_wd_timeout(3),
      O => inst_base_module_o_DBus_mux0002_3_0_866
    );
  mb_altera_core_o_DBus_15_mux00004 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0000,
      I1 => mb_altera_core_version_o_data_0_Q,
      I2 => mb_altera_core_o_DBus_10_cmp_eq0004,
      I3 => mb_altera_core_s_dma_base_addr(13),
      O => mb_altera_core_o_DBus_15_mux00004_2284
    );
  mb_altera_core_o_DBus_15_mux000024 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => inst_emif_iface_addr_r(3),
      I2 => mb_altera_core_s_dma_timeout_threshold(15),
      I3 => mb_altera_core_o_user_io(15),
      O => mb_altera_core_o_DBus_15_mux000024_2282
    );
  mb_altera_core_altera_asd_mux000025 : LUT4
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => mb_altera_core_Flash_State_FSM_FFd6_1605,
      I1 => mb_altera_core_Flash_State_FSM_FFd4_1602,
      I2 => mb_altera_core_Flash_State_FSM_FFd5_1603,
      I3 => mb_altera_core_Flash_State_FSM_FFd3_1601,
      O => mb_altera_core_altera_asd_mux000025_2215
    );
  mb_altera_core_altera_asd_mux000029 : LUT2
    generic map(
      INIT => X"1"
    )
    port map (
      I0 => mb_altera_core_Flash_State_FSM_FFd2_1600,
      I1 => mb_altera_core_Flash_State_FSM_FFd1_1599,
      O => mb_altera_core_altera_asd_mux000029_2216
    );
  inst_base_module_wd_counter_mux0000_15_1 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => inst_base_module_N1,
      I1 => inst_base_module_wd_counter_addsub0000(15),
      I2 => inst_base_module_wd_timeout(15),
      O => inst_base_module_wd_counter_mux0000(15)
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_17_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(14),
      I3 => mb_altera_core_s_dma_timeout_cntr(14),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(17)
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_16_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(15),
      I3 => mb_altera_core_s_dma_timeout_cntr(15),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(16)
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_15_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(16),
      I3 => mb_altera_core_s_dma_timeout_cntr(16),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(15)
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_14_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(17),
      I3 => mb_altera_core_s_dma_timeout_cntr(17),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(14)
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_13_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(18),
      I3 => mb_altera_core_s_dma_timeout_cntr(18),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(13)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_8_1 : LUT4
    generic map(
      INIT => X"F0F4"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N421,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_col_and0000,
      O => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_8_1_1470
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_8_2 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_N16,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(4),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      O => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_8_2_1471
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_8_f5 : MUXF5
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_8_2_1471,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_8_1_1470,
      S => inst_emif_iface_sdram_dma_std_dma_engine_word_count(0),
      O => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000(8)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_7_14 : LUT3
    generic map(
      INIT => X"B2"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      O => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_6_14
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_7_22 : LUT2
    generic map(
      INIT => X"1"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I1 => N789,
      O => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_6_27
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_7_68 : LUT4
    generic map(
      INIT => X"EEE4"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(1),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N54,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_6_14,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_7_31_1468,
      O => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000(7)
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_12_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(19),
      I3 => mb_altera_core_s_dma_timeout_cntr(19),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(12)
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_11_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(20),
      I3 => mb_altera_core_s_dma_timeout_cntr(20),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(11)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_0_61 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(3),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(4),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N35,
      O => inst_emif_iface_sdram_dma_std_dma_engine_N49
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_10_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(21),
      I3 => mb_altera_core_s_dma_timeout_cntr(21),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(10)
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_9_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(22),
      I3 => mb_altera_core_s_dma_timeout_cntr(22),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(9)
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_8_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(23),
      I3 => mb_altera_core_s_dma_timeout_cntr(23),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(8)
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_7_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(24),
      I3 => mb_altera_core_s_dma_timeout_cntr(24),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(7)
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_6_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(25),
      I3 => mb_altera_core_s_dma_timeout_cntr(25),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(6)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_11_25 : LUT4
    generic map(
      INIT => X"2AAA"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row(1),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_row(2),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_row(0),
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_11_25_1366
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_5_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(26),
      I3 => mb_altera_core_s_dma_timeout_cntr(26),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(5)
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_4_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(27),
      I3 => mb_altera_core_s_dma_timeout_cntr(27),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(4)
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_3_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(28),
      I3 => mb_altera_core_s_dma_timeout_cntr(28),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(3)
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_2_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(29),
      I3 => mb_altera_core_s_dma_timeout_cntr(29),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(2)
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_1_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(30),
      I3 => mb_altera_core_s_dma_timeout_cntr(30),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(1)
    );
  mb_altera_core_s_dma_timeout_cntr_mux0000_0_1 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_timeout_cntr_addsub0000(31),
      I3 => mb_altera_core_s_dma_timeout_cntr(31),
      O => mb_altera_core_s_dma_timeout_cntr_mux0000(0)
    );
  mb_altera_core_s_dma_count_mux0003_7_SW0 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => mb_altera_core_N7,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_count(1),
      O => N269
    );
  mb_altera_core_s_dma_count_mux0003_4_SW0 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => mb_altera_core_N7,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_count(4),
      O => N271
    );
  mb_altera_core_s_dma_count_mux0003_1_SW0 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => mb_altera_core_N7,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_dma_count(7),
      O => N273
    );
  mb_altera_core_s_dma_count_mux0003_0_3_SW0 : LUT4
    generic map(
      INIT => X"EFFF"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I1 => mb_altera_core_flash_enable_2235,
      I2 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I3 => mb_altera_core_s_dma_en_2662,
      O => N275
    );
  mb_altera_core_s_dma_count_mux0003_5_30 : LUT4
    generic map(
      INIT => X"AAA9"
    )
    port map (
      I0 => mb_altera_core_s_altera_rdfifo_rd_cnt(3),
      I1 => mb_altera_core_Msub_s_dma_count_share0000_cy_0_Q,
      I2 => mb_altera_core_s_altera_rdfifo_rd_cnt(1),
      I3 => mb_altera_core_s_altera_rdfifo_rd_cnt(2),
      O => mb_altera_core_s_dma_count_mux0003_5_30_2657
    );
  mb_altera_core_s_dma_count_mux0003_2_34 : LUT4
    generic map(
      INIT => X"AAA9"
    )
    port map (
      I0 => mb_altera_core_s_altera_rdfifo_rd_cnt(6),
      I1 => mb_altera_core_Msub_s_dma_count_share0000_cy_3_Q,
      I2 => mb_altera_core_s_altera_rdfifo_rd_cnt(4),
      I3 => mb_altera_core_s_altera_rdfifo_rd_cnt(5),
      O => mb_altera_core_s_dma_count_mux0003_2_34_2653
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_5_21 : LUT3
    generic map(
      INIT => X"10"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N9,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N51,
      O => inst_emif_iface_sdram_dma_std_dma_engine_N521
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_6 : LUT3
    generic map(
      INIT => X"08"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_col(0),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_col(2),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_col(3),
      O => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_6_1248
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_17_7 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(5),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row(4),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_row(6),
      I3 => N286,
      O => inst_emif_iface_sdram_dma_std_dma_engine_N47
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_15_18 : LUT3
    generic map(
      INIT => X"DF"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(6),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N24,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_row(5),
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_15_18_1381
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_13_8 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(2),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row(3),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_row(4),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_row(5),
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_13_8_1376
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_13_16 : LUT4
    generic map(
      INIT => X"ECA0"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(13),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_13_8_1376,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N38,
      I3 => N787,
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_13_16_1374
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_12_6 : LUT3
    generic map(
      INIT => X"08"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(2),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row(3),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_row(4),
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_12_6_1372
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_12_13 : LUT4
    generic map(
      INIT => X"ECA0"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(12),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_12_6_1372,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N38,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N33,
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_12_13_1369
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_12_29 : LUT4
    generic map(
      INIT => X"7FFF"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(0),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row(1),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_row(2),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_row(3),
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_12_29_1370
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_14_20 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(14),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N38,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_14_9,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N33,
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_14_20_1377
    );
  mb_altera_core_s_dma_state_FSM_FFd1_In5 : LUT3
    generic map(
      INIT => X"A2"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I1 => mb_altera_core_s_dma_irq_clear_2664,
      I2 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      O => mb_altera_core_s_dma_state_FSM_FFd1_In5_2874
    );
  mb_altera_core_s_dma_state_FSM_FFd1_In16 : LUT3
    generic map(
      INIT => X"08"
    )
    port map (
      I0 => mb_altera_core_s_dma_en_2662,
      I1 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I2 => mb_altera_core_flash_enable_2235,
      O => mb_altera_core_s_dma_state_FSM_FFd1_In16_2872
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_5_3 : LUT2
    generic map(
      INIT => X"D"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_col(4),
      I1 => N788,
      O => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_5_3_1253
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_16_21 : LUT4
    generic map(
      INIT => X"FF7F"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(7),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row(5),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_row(6),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N24,
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_16_21_1386
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_7_14 : LUT4
    generic map(
      INIT => X"FF7F"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_col(5),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_col(6),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_col(4),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N9,
      O => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_7_14_1258
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_17_17 : LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(17),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_17_17_1388
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_17_39 : LUT4
    generic map(
      INIT => X"7FFF"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(5),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row(6),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_row(7),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_row(8),
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_17_39_1390
    );
  mb_altera_core_Write_fifo_wr_Cnt_and000012 : LUT4
    generic map(
      INIT => X"7FFF"
    )
    port map (
      I0 => mb_altera_core_Write_fifo_wr_Cnt(0),
      I1 => mb_altera_core_Write_fifo_wr_Cnt(1),
      I2 => mb_altera_core_Write_fifo_wr_Cnt(2),
      I3 => mb_altera_core_Write_fifo_wr_Cnt(3),
      O => mb_altera_core_Write_fifo_wr_Cnt_and000012_2176
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_and00001 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(4),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N0,
      O => inst_emif_iface_sdram_dma_std_dma_engine_col_and0000
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_3_Q : LUT4
    generic map(
      INIT => X"EEE4"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(5),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N49,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N421,
      I3 => N299,
      O => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000(3)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_2_Q : LUT4
    generic map(
      INIT => X"EEE4"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(6),
      I1 => N301,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N421,
      I3 => N302,
      O => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000(2)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_1_3 : LUT2
    generic map(
      INIT => X"7"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(5),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(6),
      O => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_1_3_1454
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_0_12 : LUT4
    generic map(
      INIT => X"7FFF"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(4),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(5),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(6),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(7),
      O => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_0_12_1449
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_0_50 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(5),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(6),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(7),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(8),
      O => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_0_50_1451
    );
  mb_altera_core_s_dma_start_addr_mux0000_9_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(22),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(22),
      I3 => N790,
      O => mb_altera_core_s_dma_start_addr_mux0000(9)
    );
  mb_altera_core_s_dma_start_addr_mux0000_8_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(23),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(23),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(8)
    );
  mb_altera_core_s_dma_start_addr_mux0000_7_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(24),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(24),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(7)
    );
  mb_altera_core_s_dma_start_addr_mux0000_6_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(25),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(25),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(6)
    );
  mb_altera_core_s_dma_start_addr_mux0000_5_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(26),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(26),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(5)
    );
  mb_altera_core_s_dma_start_addr_mux0000_4_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(27),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(27),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(4)
    );
  mb_altera_core_s_dma_start_addr_mux0000_3_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(28),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(28),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(3)
    );
  mb_altera_core_s_dma_start_addr_mux0000_31_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(0),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(0),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(31)
    );
  mb_altera_core_s_dma_start_addr_mux0000_30_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(1),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(1),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(30)
    );
  mb_altera_core_s_dma_start_addr_mux0000_2_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(29),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(29),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(2)
    );
  mb_altera_core_s_dma_start_addr_mux0000_29_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(2),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(2),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(29)
    );
  mb_altera_core_s_dma_start_addr_mux0000_28_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(3),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(3),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(28)
    );
  mb_altera_core_s_dma_start_addr_mux0000_27_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(4),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(4),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(27)
    );
  mb_altera_core_s_dma_start_addr_mux0000_26_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(5),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(5),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(26)
    );
  mb_altera_core_s_dma_start_addr_mux0000_25_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(6),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(6),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(25)
    );
  mb_altera_core_s_dma_start_addr_mux0000_24_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(7),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(7),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(24)
    );
  mb_altera_core_s_dma_start_addr_mux0000_23_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(8),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(8),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(23)
    );
  mb_altera_core_s_dma_start_addr_mux0000_22_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(9),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(9),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(22)
    );
  mb_altera_core_s_dma_start_addr_mux0000_21_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(10),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(10),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(21)
    );
  mb_altera_core_s_dma_start_addr_mux0000_20_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(11),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(11),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(20)
    );
  mb_altera_core_s_dma_start_addr_mux0000_19_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(12),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(12),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(19)
    );
  mb_altera_core_s_dma_start_addr_mux0000_18_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(13),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(13),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(18)
    );
  mb_altera_core_s_dma_start_addr_mux0000_17_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(14),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(14),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(17)
    );
  mb_altera_core_s_dma_start_addr_mux0000_16_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(15),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(15),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(16)
    );
  mb_altera_core_s_dma_start_addr_mux0000_15_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(16),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(16),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(15)
    );
  mb_altera_core_s_dma_start_addr_mux0000_14_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(17),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(17),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(14)
    );
  mb_altera_core_s_dma_start_addr_mux0000_13_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(18),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(18),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(13)
    );
  mb_altera_core_s_dma_start_addr_mux0000_12_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(19),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(19),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(12)
    );
  mb_altera_core_s_dma_start_addr_mux0000_11_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(20),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(20),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(11)
    );
  mb_altera_core_s_dma_start_addr_mux0000_10_2 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(21),
      I1 => mb_altera_core_N9,
      I2 => mb_altera_core_s_dma_start_addr(21),
      I3 => mb_altera_core_N1,
      O => mb_altera_core_s_dma_start_addr_mux0000(10)
    );
  mb_altera_core_s_dma_space_addr_mux0000_9_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(22),
      I1 => mb_altera_core_s_dma_space_addr_mux0000_10_11_2727,
      O => N304
    );
  mb_altera_core_s_dma_space_addr_mux0000_8_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(23),
      I1 => N802,
      O => N306
    );
  mb_altera_core_s_dma_space_addr_mux0000_7_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(24),
      I1 => mb_altera_core_s_dma_space_addr_mux0000_10_11_2727,
      O => N308
    );
  mb_altera_core_s_dma_space_addr_mux0000_6_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(25),
      I1 => mb_altera_core_s_dma_space_addr_mux0000_10_11_2727,
      O => N310
    );
  mb_altera_core_s_dma_space_addr_mux0000_5_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(26),
      I1 => mb_altera_core_s_dma_space_addr_mux0000_10_11_2727,
      O => N312
    );
  mb_altera_core_s_dma_space_addr_mux0000_4_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(27),
      I1 => mb_altera_core_s_dma_space_addr_mux0000_10_11_2727,
      O => N314
    );
  mb_altera_core_s_dma_space_addr_mux0000_3_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(28),
      I1 => mb_altera_core_s_dma_space_addr_mux0000_10_11_2727,
      O => N316
    );
  mb_altera_core_s_dma_space_addr_mux0000_31_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(0),
      I1 => mb_altera_core_s_dma_space_addr_mux0000_10_11_2727,
      O => N318
    );
  mb_altera_core_s_dma_space_addr_mux0000_30_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(1),
      I1 => mb_altera_core_s_dma_space_addr_mux0000_10_11_2727,
      O => N320
    );
  mb_altera_core_s_dma_space_addr_mux0000_2_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(29),
      I1 => mb_altera_core_s_dma_space_addr_mux0000_10_11_2727,
      O => N322
    );
  mb_altera_core_s_dma_space_addr_mux0000_29_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(2),
      I1 => mb_altera_core_s_dma_space_addr_mux0000_10_11_2727,
      O => N324
    );
  mb_altera_core_s_dma_space_addr_mux0000_28_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(3),
      I1 => mb_altera_core_s_dma_space_addr_mux0000_10_11_2727,
      O => N326
    );
  mb_altera_core_s_dma_space_addr_mux0000_27_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(4),
      I1 => mb_altera_core_s_dma_space_addr_mux0000_10_11_2727,
      O => N328
    );
  mb_altera_core_s_dma_space_addr_mux0000_26_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(5),
      I1 => mb_altera_core_s_dma_space_addr_mux0000_10_11_2727,
      O => N330
    );
  mb_altera_core_s_dma_space_addr_mux0000_25_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(6),
      I1 => mb_altera_core_s_dma_space_addr_mux0000_10_11_2727,
      O => N332
    );
  mb_altera_core_s_dma_space_addr_mux0000_24_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(7),
      I1 => N791,
      O => N334
    );
  mb_altera_core_s_dma_space_addr_mux0000_23_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(8),
      I1 => mb_altera_core_N0,
      O => N336
    );
  mb_altera_core_s_dma_space_addr_mux0000_22_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(9),
      I1 => mb_altera_core_N0,
      O => N338
    );
  mb_altera_core_s_dma_space_addr_mux0000_21_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(10),
      I1 => mb_altera_core_N0,
      O => N340
    );
  mb_altera_core_s_dma_space_addr_mux0000_20_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(11),
      I1 => mb_altera_core_N0,
      O => N342
    );
  mb_altera_core_s_dma_space_addr_mux0000_19_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(12),
      I1 => mb_altera_core_N0,
      O => N344
    );
  mb_altera_core_s_dma_space_addr_mux0000_18_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(13),
      I1 => mb_altera_core_N0,
      O => N346
    );
  mb_altera_core_s_dma_space_addr_mux0000_17_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(14),
      I1 => mb_altera_core_N0,
      O => N348
    );
  mb_altera_core_s_dma_space_addr_mux0000_16_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(15),
      I1 => mb_altera_core_N0,
      O => N350
    );
  mb_altera_core_s_dma_space_addr_mux0000_15_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(16),
      I1 => mb_altera_core_N0,
      O => N352
    );
  mb_altera_core_s_dma_space_addr_mux0000_14_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(17),
      I1 => mb_altera_core_N0,
      O => N354
    );
  mb_altera_core_s_dma_space_addr_mux0000_13_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(18),
      I1 => mb_altera_core_N0,
      O => N356
    );
  mb_altera_core_s_dma_space_addr_mux0000_12_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(19),
      I1 => mb_altera_core_N0,
      O => N358
    );
  mb_altera_core_s_dma_space_addr_mux0000_11_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(20),
      I1 => mb_altera_core_N0,
      O => N360
    );
  mb_altera_core_s_dma_space_addr_mux0000_10_SW0 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(21),
      I1 => mb_altera_core_N0,
      O => N362
    );
  i_eth_rx_clk_IBUFG : IBUFG
    port map (
      I => i_eth_rx_clk,
      O => i_eth_rx_clk_IBUFG_508
    );
  i_clk25mhz_IBUFG : IBUFG
    port map (
      I => i_clk25mhz,
      O => i_clk25mhz_IBUFG_499
    );
  i_altera_cdone_IBUF : IBUF
    port map (
      I => i_altera_cdone,
      O => i_altera_cdone_IBUF_485
    );
  i_holda_n_IBUF : IBUF
    port map (
      I => i_holda_n,
      O => i_holda_n_IBUF_524
    );
  i_rs232_rcv_IBUF : IBUF
    port map (
      I => i_rs232_rcv,
      O => i_rs232_rcv_IBUF_526
    );
  i_bus_req_IBUF : IBUF
    port map (
      I => i_bus_req,
      O => i_bus_req_IBUF_497
    );
  i_altera_int_bar_IBUF : IBUF
    port map (
      I => i_altera_int_bar,
      O => i_altera_int_bar_IBUF_489
    );
  i_altera_write_ff_ff_IBUF : IBUF
    port map (
      I => i_altera_write_ff_ff,
      O => i_altera_write_ff_ff_IBUF_495
    );
  i_eth_def_IBUF : IBUF
    port map (
      I => i_eth_def,
      O => i_eth_def_IBUF_506
    );
  i_eth_tx_clk_IBUFG : IBUFG
    port map (
      I => i_eth_tx_clk,
      O => i_eth_tx_clk_IBUFG_522
    );
  i_altera_ns_IBUF : IBUF
    port map (
      I => i_altera_ns,
      O => i_altera_ns_IBUF_491
    );
  i_altera_read_Busy_bar_IBUF : IBUF
    port map (
      I => i_altera_read_Busy_bar,
      O => i_altera_read_Busy_bar_IBUF_493
    );
  i_eth_col_IBUF : IBUF
    port map (
      I => i_eth_col,
      O => i_eth_col_IBUF_502
    );
  i_altera_data0_IBUF : IBUF
    port map (
      I => i_altera_data0,
      O => i_altera_data0_IBUF_487
    );
  i_eth_crs_IBUF : IBUF
    port map (
      I => i_eth_crs,
      O => i_eth_crs_IBUF_504
    );
  i_eth_rx_dv_IBUF : IBUF
    port map (
      I => i_eth_rx_dv,
      O => i_eth_rx_dv_IBUF_518
    );
  i_eth_rx_er_IBUF : IBUF
    port map (
      I => i_eth_rx_er,
      O => i_eth_rx_er_IBUF_520
    );
  i_eth_rx_data_3_IBUF : IBUF
    port map (
      I => i_eth_rx_data(3),
      O => i_eth_rx_data_3_IBUF_516
    );
  i_eth_rx_data_2_IBUF : IBUF
    port map (
      I => i_eth_rx_data(2),
      O => i_eth_rx_data_2_IBUF_515
    );
  i_eth_rx_data_1_IBUF : IBUF
    port map (
      I => i_eth_rx_data(1),
      O => i_eth_rx_data_1_IBUF_514
    );
  i_eth_rx_data_0_IBUF : IBUF
    port map (
      I => i_eth_rx_data(0),
      O => i_eth_rx_data_0_IBUF_513
    );
  io_ce_n_0_OBUFT : OBUFT
    port map (
      I => inst_emif_iface_o_ce_n_0_Q,
      T => inst_emif_iface_t_emif_1472,
      O => io_ce_n(0)
    );
  io_ce_n_1_OBUFT : OBUFT
    port map (
      I => inst_emif_iface_o_ce_n_3_1_1158,
      T => inst_emif_iface_t_emif_1472,
      O => io_ce_n(1)
    );
  io_ce_n_2_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_o_ce_n_3_2_1159,
      T => inst_emif_iface_t_emif_1472,
      O => N364,
      IO => io_ce_n(2)
    );
  io_are_n_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_o_cas_n_1155,
      T => inst_emif_iface_t_emif_1472,
      O => N365,
      IO => io_are_n
    );
  io_ce_n_3_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_o_ce_n_3_Q,
      T => inst_emif_iface_t_emif_1472,
      O => N366,
      IO => io_ce_n(3)
    );
  io_aoe_n_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_o_ras_n_1173,
      T => inst_emif_iface_t_emif_1472,
      O => N367,
      IO => io_aoe_n
    );
  io_awe_n_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_o_awe_n_1153,
      T => inst_emif_iface_t_emif_1472,
      O => N368,
      IO => io_awe_n
    );
  io_eth_mdio_IOBUF : IOBUF
    port map (
      I => o_eth_mdio,
      T => io_eth_mdio_o_eth_mdio_not0000_inv,
      O => N369,
      IO => io_eth_mdio
    );
  io_dsp_rst_n_OBUFT : OBUFT
    port map (
      I => ea(15),
      T => wd_rst_inv,
      O => io_dsp_rst_n
    );
  io_be_n_3_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_o_be_n(3),
      T => inst_emif_iface_t_emif_1472,
      O => N402,
      IO => io_be_n(3)
    );
  io_be_n_2_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_o_be_n(3),
      T => inst_emif_iface_t_emif_1472,
      O => N403,
      IO => io_be_n(2)
    );
  io_be_n_1_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_o_be_n(3),
      T => inst_emif_iface_t_emif_1472,
      O => N404,
      IO => io_be_n(1)
    );
  io_be_n_0_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_o_be_n(3),
      T => inst_emif_iface_t_emif_1472,
      O => N405,
      IO => io_be_n(0)
    );
  io_altera_dsp_32_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(32),
      T => Mtrien_io_altera_dsp_33,
      O => N406,
      IO => io_altera_dsp(32)
    );
  io_altera_dsp_31_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(31),
      T => Mtrien_io_altera_dsp_33,
      O => N407,
      IO => io_altera_dsp(31)
    );
  io_altera_dsp_30_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(30),
      T => Mtrien_io_altera_dsp_33,
      O => N408,
      IO => io_altera_dsp(30)
    );
  io_altera_dsp_29_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(29),
      T => Mtrien_io_altera_dsp_33,
      O => N409,
      IO => io_altera_dsp(29)
    );
  io_altera_dsp_28_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(28),
      T => Mtrien_io_altera_dsp_33,
      O => N410,
      IO => io_altera_dsp(28)
    );
  io_altera_dsp_27_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(27),
      T => Mtrien_io_altera_dsp_33,
      O => N411,
      IO => io_altera_dsp(27)
    );
  io_altera_dsp_26_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(26),
      T => Mtrien_io_altera_dsp_33,
      O => N412,
      IO => io_altera_dsp(26)
    );
  io_altera_dsp_25_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(25),
      T => Mtrien_io_altera_dsp_33,
      O => N413,
      IO => io_altera_dsp(25)
    );
  io_altera_dsp_24_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(24),
      T => Mtrien_io_altera_dsp_33,
      O => N414,
      IO => io_altera_dsp(24)
    );
  io_altera_dsp_23_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(23),
      T => Mtrien_io_altera_dsp_33,
      O => N415,
      IO => io_altera_dsp(23)
    );
  io_altera_dsp_22_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(22),
      T => Mtrien_io_altera_dsp_33,
      O => N416,
      IO => io_altera_dsp(22)
    );
  io_altera_dsp_21_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(21),
      T => Mtrien_io_altera_dsp_33,
      O => N417,
      IO => io_altera_dsp(21)
    );
  io_altera_dsp_20_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(20),
      T => Mtrien_io_altera_dsp_33,
      O => N418,
      IO => io_altera_dsp(20)
    );
  io_altera_dsp_19_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(19),
      T => Mtrien_io_altera_dsp_33,
      O => N419,
      IO => io_altera_dsp(19)
    );
  io_altera_dsp_18_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(18),
      T => Mtrien_io_altera_dsp_33,
      O => N420,
      IO => io_altera_dsp(18)
    );
  io_altera_dsp_17_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(17),
      T => Mtrien_io_altera_dsp_33,
      O => N421,
      IO => io_altera_dsp(17)
    );
  io_altera_dsp_16_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(16),
      T => Mtrien_io_altera_dsp_33,
      O => N422,
      IO => io_altera_dsp(16)
    );
  io_altera_dsp_15_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(15),
      T => Mtrien_io_altera_dsp_33,
      O => N423,
      IO => io_altera_dsp(15)
    );
  io_altera_dsp_14_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(14),
      T => Mtrien_io_altera_dsp_33,
      O => N424,
      IO => io_altera_dsp(14)
    );
  io_altera_dsp_13_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(13),
      T => Mtrien_io_altera_dsp_33,
      O => N425,
      IO => io_altera_dsp(13)
    );
  io_altera_dsp_12_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(12),
      T => Mtrien_io_altera_dsp_33,
      O => N426,
      IO => io_altera_dsp(12)
    );
  io_altera_dsp_11_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(11),
      T => Mtrien_io_altera_dsp_33,
      O => N427,
      IO => io_altera_dsp(11)
    );
  io_altera_dsp_10_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(10),
      T => Mtrien_io_altera_dsp_33,
      O => N428,
      IO => io_altera_dsp(10)
    );
  io_altera_dsp_9_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(9),
      T => Mtrien_io_altera_dsp_33,
      O => N429,
      IO => io_altera_dsp(9)
    );
  io_altera_dsp_8_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(8),
      T => Mtrien_io_altera_dsp_33,
      O => N430,
      IO => io_altera_dsp(8)
    );
  io_altera_dsp_7_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(7),
      T => Mtrien_io_altera_dsp_33,
      O => N431,
      IO => io_altera_dsp(7)
    );
  io_altera_dsp_6_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(6),
      T => Mtrien_io_altera_dsp_33,
      O => N432,
      IO => io_altera_dsp(6)
    );
  io_altera_dsp_5_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(5),
      T => Mtrien_io_altera_dsp_33,
      O => N433,
      IO => io_altera_dsp(5)
    );
  io_altera_dsp_4_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(4),
      T => Mtrien_io_altera_dsp_33,
      O => N434,
      IO => io_altera_dsp(4)
    );
  io_altera_dsp_3_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(3),
      T => Mtrien_io_altera_dsp_33,
      O => N435,
      IO => io_altera_dsp(3)
    );
  io_altera_dsp_2_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(2),
      T => Mtrien_io_altera_dsp_33,
      O => N436,
      IO => io_altera_dsp(2)
    );
  io_altera_dsp_1_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(1),
      T => Mtrien_io_altera_dsp_33,
      O => N437,
      IO => io_altera_dsp(1)
    );
  io_altera_dsp_0_IOBUF : IOBUF
    port map (
      I => Mtridata_io_altera_dsp(0),
      T => Mtrien_io_altera_dsp_33,
      O => N438,
      IO => io_altera_dsp(0)
    );
  io_ea_15_OBUFT : OBUFT
    port map (
      I => ea(15),
      T => inst_emif_iface_t_emif_1472,
      O => io_ea(15)
    );
  io_ea_14_OBUFT : OBUFT
    port map (
      I => inst_emif_iface_o_ea(14),
      T => inst_emif_iface_t_emif_1472,
      O => io_ea(14)
    );
  io_ea_13_OBUFT : OBUFT
    port map (
      I => inst_emif_iface_o_ea(13),
      T => inst_emif_iface_t_emif_1472,
      O => io_ea(13)
    );
  io_ea_12_OBUFT : OBUFT
    port map (
      I => inst_emif_iface_o_ea(12),
      T => inst_emif_iface_t_emif_1472,
      O => io_ea(12)
    );
  io_ea_11_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_o_ea(11),
      T => inst_emif_iface_t_emif_1472,
      O => N439,
      IO => io_ea(11)
    );
  io_ea_10_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_o_ea(10),
      T => inst_emif_iface_t_emif_1472,
      O => N440,
      IO => io_ea(10)
    );
  io_ea_9_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_o_ea(9),
      T => inst_emif_iface_t_emif_1472,
      O => N441,
      IO => io_ea(9)
    );
  io_ea_8_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_o_ea(8),
      T => inst_emif_iface_t_emif_1472,
      O => N442,
      IO => io_ea(8)
    );
  io_ea_7_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_o_ea(7),
      T => inst_emif_iface_t_emif_1472,
      O => N443,
      IO => io_ea(7)
    );
  io_ea_6_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_o_ea(6),
      T => inst_emif_iface_t_emif_1472,
      O => N444,
      IO => io_ea(6)
    );
  io_ea_5_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_o_ea(5),
      T => inst_emif_iface_t_emif_1472,
      O => N445,
      IO => io_ea(5)
    );
  io_ea_4_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_o_ea(4),
      T => inst_emif_iface_t_emif_1472,
      O => N446,
      IO => io_ea(4)
    );
  io_ea_3_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_o_ea(3),
      T => inst_emif_iface_t_emif_1472,
      O => N447,
      IO => io_ea(3)
    );
  io_ea_2_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_o_ea(2),
      T => inst_emif_iface_t_emif_1472,
      O => N448,
      IO => io_ea(2)
    );
  o_altera_Clk_OBUF : OBUF
    port map (
      I => o_altera_Clk_OBUF_3025,
      O => o_altera_Clk
    );
  o_busy_OBUF : OBUF
    port map (
      I => o_busy_OBUF_3037,
      O => o_busy
    );
  o_fl_bank_sel_n_OBUFT : OBUFT
    generic map(
      IOSTANDARD => "LVCMOS25"
    )
    port map (
      I => ea(15),
      T => t_fl_bank_sel_n_inv,
      O => o_fl_bank_sel_n
    );
  o_ce1_en_n_OBUF : OBUF
    port map (
      I => ea(15),
      O => o_ce1_en_n
    );
  o_hold_n_OBUF : OBUF
    port map (
      I => o_hold_n_OBUF_3065,
      O => o_hold_n
    );
  o_altera_nce_OBUFT : OBUFT
    port map (
      I => mb_altera_core_altera_nce_2222,
      T => altera_flash_enable_inv,
      O => o_altera_nce
    );
  o_rs232_xmit_OBUF : OBUF
    port map (
      I => o_rs232_xmit_OBUF_3067,
      O => o_rs232_xmit
    );
  o_altera_write_busy_bar_OBUF : OBUF
    port map (
      I => o_altera_write_busy_bar_OBUF_3035,
      O => o_altera_write_busy_bar
    );
  o_altera_ncs_OBUFT : OBUFT
    port map (
      I => mb_altera_core_altera_ncs_2226,
      T => altera_flash_enable_inv,
      O => o_altera_ncs
    );
  o_altera_dclk_OBUFT : OBUFT
    port map (
      I => mb_altera_core_altera_dclk_2221,
      T => altera_flash_enable_inv,
      O => o_altera_dclk
    );
  o_eth_mdc_OBUF : OBUF
    port map (
      I => o_eth_mdc_OBUF_3047,
      O => o_eth_mdc
    );
  o_eth_rst_n_OBUF : OBUF
    port map (
      I => o_eth_rst_n_OBUF_3052,
      O => o_eth_rst_n
    );
  o_dsp_nmi_OBUF : OBUF
    port map (
      I => inst_base_module_wd_nmi_939,
      O => o_dsp_nmi
    );
  o_eth_ref_clk_OBUF : OBUF
    port map (
      I => o_eth_ref_clk_OBUF_3050,
      O => o_eth_ref_clk
    );
  o_altera_read_ff_ff_OBUF : OBUF
    port map (
      I => o_altera_read_ff_ff_OBUF_3033,
      O => o_altera_read_ff_ff
    );
  o_user_led_n_OBUF : OBUF
    generic map(
      IOSTANDARD => "LVCMOS33"
    )
    port map (
      I => o_user_led_n_OBUF_3069,
      O => o_user_led_n
    );
  o_altera_int_busy_OBUF : OBUF
    port map (
      I => mb_altera_core_s_altera_int_busy_2551,
      O => o_altera_int_busy
    );
  o_dsp_ext_int4_OBUF : OBUF
    port map (
      I => ea(15),
      O => o_dsp_ext_int4
    );
  o_dsp_ext_int5_OBUF : OBUF
    port map (
      I => o_dsp_ext_int5_OBUF_3041,
      O => o_dsp_ext_int5
    );
  o_dsp_ext_int6_OBUF : OBUF
    port map (
      I => ea(15),
      O => o_dsp_ext_int6
    );
  o_dsp_ext_int7_OBUF : OBUF
    port map (
      I => o_dsp_ext_int7_OBUF_3044,
      O => o_dsp_ext_int7
    );
  o_eth_tx_en_OBUF : OBUF
    port map (
      I => o_eth_tx_en_OBUF_3062,
      O => o_eth_tx_en
    );
  o_altera_ncon_OBUFT : OBUFT
    port map (
      I => mb_altera_core_altera_ncon_2224,
      T => altera_flash_enable_inv,
      O => o_altera_ncon
    );
  o_altera_asd_OBUFT : OBUFT
    port map (
      I => mb_altera_core_altera_asd_2212,
      T => altera_flash_enable_inv,
      O => o_altera_asd
    );
  io_altera_spare_2_OBUF : OBUF
    port map (
      I => mb_altera_core_s_dma_irq_2663,
      O => io_altera_spare(2)
    );
  io_altera_spare_1_OBUF : OBUF
    port map (
      I => ea(15),
      O => io_altera_spare(1)
    );
  io_altera_spare_0_OBUF : OBUF
    port map (
      I => mb_altera_core_s_dma_req_2694,
      O => io_altera_spare(0)
    );
  o_eth_tx_data_3_OBUF : OBUF
    port map (
      I => o_eth_tx_data_3_OBUF_3060,
      O => o_eth_tx_data(3)
    );
  o_eth_tx_data_2_OBUF : OBUF
    port map (
      I => o_eth_tx_data_2_OBUF_3059,
      O => o_eth_tx_data(2)
    );
  o_eth_tx_data_1_OBUF : OBUF
    port map (
      I => o_eth_tx_data_1_OBUF_3058,
      O => o_eth_tx_data(1)
    );
  o_eth_tx_data_0_OBUF : OBUF
    port map (
      I => o_eth_tx_data_0_OBUF_3057,
      O => o_eth_tx_data(0)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_1 : FDS
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_9_36,
      S => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_9_14_1396,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_row(1)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_2 : FDS
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_10_25,
      S => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_10_6_1364,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_row(2)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_3 : FDS
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_11_53,
      S => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_11_0_1365,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_row(3)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_4 : FDS
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_12_52,
      S => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_12_13_1369,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_row(4)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_5 : FDS
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_13_28,
      S => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_13_16_1374,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_row(5)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_6 : FDS
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_14_42,
      S => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_14_20_1377,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_row(6)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_7 : FDS
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_15_35,
      S => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_15_8_1384,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_row(7)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_8 : FDS
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_16_47,
      S => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_16_0_1385,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_row(8)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_9 : FDS
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_17_63_1391,
      S => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_17_19_1389,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_row(9)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_10 : FDS
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_18_32,
      S => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_18_16_1393,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_row(10)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_0 : FDS
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_0_17_1237,
      S => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_0_4_1238,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_col(0)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_1 : FDS
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_1_33,
      S => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_1_0_1239,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_col(1)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_2 : FDS
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_2_39_1242,
      S => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_2_4_1243,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_col(2)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_3 : FDS
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_73,
      S => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_0_1244,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_col(3)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_731 : LUT3
    generic map(
      INIT => X"F8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_col(3),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_53_1247,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_17_1245,
      O => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_73
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_4 : FDS
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_4_30,
      S => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_4_0_1250,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_col(4)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_5 : FDS
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_5_31,
      S => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_5_0_1252,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_col(5)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_6 : FDS
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_6_43,
      S => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_6_0_1255,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_col(6)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_7 : FDS
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_7_61,
      S => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_7_0_1257,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_col(7)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ed_t : FDS
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ed_t_mux00021,
      S => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_ed_t_1337
    );
  inst_emif_iface_sdram_dma_std_dma_engine_bank_0 : FDS
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_19_38,
      S => inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_19_17_1218,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_bank(0)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_bank_1 : FDS
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_20_55,
      S => inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_20_21_1221,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_bank(1)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_20_551 : LUT3
    generic map(
      INIT => X"A8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_bank(1),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_20_31_1222,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_19_26_1219,
      O => inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_20_55
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_done : FDR
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_N46,
      R => inst_emif_iface_sdram_dma_std_dma_engine_N43,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_dma_done_1301
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_7 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_1_47,
      S => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_1_44_1455,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_word_count(7)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_8 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_0_58,
      S => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_0_51_1452,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_word_count(8)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_0_581 : LUT3
    generic map(
      INIT => X"A8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(8),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N421,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_0_20_1450,
      O => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_0_58
    );
  inst_emif_iface_sdram_dma_std_dma_engine_be_n_0 : FDR
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      D => Mtrien_io_altera_dsp_mux0000_norst,
      R => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_be_n(0)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_emif_t : FDR
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_Out121,
      R => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_emif_t_1339
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_Out1211 : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      O => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_Out121
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_In64,
      S => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_In21_1429,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428
    );
  mb_altera_core_altera_state_FSM_FFd5 : FDS
    generic map(
      INIT => '1'
    )
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_altera_state_FSM_FFd5_In1_2234,
      S => mb_altera_core_altera_state_FSM_FFd1_2228,
      Q => mb_altera_core_altera_state_FSM_FFd5_2233
    );
  mb_altera_core_altera_state_FSM_FFd4 : FDR
    generic map(
      INIT => '0'
    )
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_altera_state_FSM_FFd4_In1,
      R => i_altera_write_ff_ff_IBUF_495,
      Q => mb_altera_core_altera_state_FSM_FFd4_2231
    );
  mb_altera_core_altera_state_FSM_FFd4_In11 : LUT3
    generic map(
      INIT => X"08"
    )
    port map (
      I0 => i_altera_read_Busy_bar_IBUF_493,
      I1 => mb_altera_core_altera_state_FSM_FFd5_2233,
      I2 => mb_altera_core_Write_Fifo_ef,
      O => mb_altera_core_altera_state_FSM_FFd4_In1
    );
  mb_altera_core_s_dma_state_FSM_FFd1 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_state_FSM_FFd1_In26,
      S => mb_altera_core_s_dma_state_FSM_FFd1_In5_2874,
      Q => mb_altera_core_s_dma_state_FSM_FFd1_2871
    );
  mb_altera_core_s_dma_state_FSM_FFd1_In261 : LUT3
    generic map(
      INIT => X"A8"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd1_In16_2872,
      I1 => mb_altera_core_s_dma_state_cmp_ge0000,
      I2 => mb_altera_core_s_dma_state_FSM_FFd1_In8_2875,
      O => mb_altera_core_s_dma_state_FSM_FFd1_In26
    );
  mb_altera_core_Flash_State_FSM_FFd7 : FDS
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_Flash_State_FSM_FFd7_In44,
      S => mb_altera_core_Flash_State_FSM_FFd7_In0_1608,
      Q => mb_altera_core_Flash_State_FSM_FFd7_1607
    );
  mb_altera_core_Flash_State_FSM_FFd7_In441 : LUT3
    generic map(
      INIT => X"A8"
    )
    port map (
      I0 => mb_altera_core_Flash_State_FSM_FFd7_1607,
      I1 => mb_altera_core_Flash_State_FSM_FFd7_In9_1611,
      I2 => mb_altera_core_Flash_State_FSM_FFd7_In20_1609,
      O => mb_altera_core_Flash_State_FSM_FFd7_In44
    );
  mb_altera_core_altera_dclk : FDS
    port map (
      C => emif_clk,
      D => mb_altera_core_Flash_State_FSM_Out91,
      S => mb_altera_core_Flash_State_FSM_FFd1_1599,
      Q => mb_altera_core_altera_dclk_2221
    );
  mb_altera_core_Flash_State_FSM_Out911 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => mb_altera_core_Flash_State_FSM_FFd2_1600,
      I1 => mb_altera_core_Flash_State_FSM_FFd6_1605,
      I2 => mb_altera_core_Flash_State_FSM_FFd7_1607,
      O => mb_altera_core_Flash_State_FSM_Out91
    );
  mb_altera_core_s_i_altera_int_r1 : FDR
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => Mtrien_io_altera_dsp_mux0000_norst,
      R => i_altera_int_bar_IBUF_489,
      Q => mb_altera_core_s_i_altera_int_r1_3016
    );
  mb_altera_core_s_altera_data_oen : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => o_altera_Clk_OBUF_3025,
      D => mb_altera_core_s_altera_data_oen_mux00001,
      S => mb_altera_core_altera_state_FSM_FFd3_2230,
      Q => mb_altera_core_s_altera_data_oen_2545
    );
  mb_altera_core_s_dma_space_addr_29 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_2_1_2747,
      S => N322,
      Q => mb_altera_core_s_dma_space_addr(29)
    );
  mb_altera_core_s_dma_space_addr_mux0000_2_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(29),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(31),
      O => mb_altera_core_s_dma_space_addr_mux0000_2_1_2747
    );
  mb_altera_core_s_dma_space_addr_28 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_3_1_2750,
      S => N316,
      Q => mb_altera_core_s_dma_space_addr(28)
    );
  mb_altera_core_s_dma_space_addr_mux0000_3_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(28),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(30),
      O => mb_altera_core_s_dma_space_addr_mux0000_3_1_2750
    );
  mb_altera_core_s_dma_space_addr_27 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_4_1_2751,
      S => N314,
      Q => mb_altera_core_s_dma_space_addr(27)
    );
  mb_altera_core_s_dma_space_addr_mux0000_4_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(27),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(29),
      O => mb_altera_core_s_dma_space_addr_mux0000_4_1_2751
    );
  mb_altera_core_s_dma_space_addr_26 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_5_1_2752,
      S => N312,
      Q => mb_altera_core_s_dma_space_addr(26)
    );
  mb_altera_core_s_dma_space_addr_mux0000_5_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(26),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(28),
      O => mb_altera_core_s_dma_space_addr_mux0000_5_1_2752
    );
  mb_altera_core_s_dma_space_addr_25 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_6_1_2753,
      S => N310,
      Q => mb_altera_core_s_dma_space_addr(25)
    );
  mb_altera_core_s_dma_space_addr_mux0000_6_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(25),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(27),
      O => mb_altera_core_s_dma_space_addr_mux0000_6_1_2753
    );
  mb_altera_core_s_dma_space_addr_24 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_7_1_2754,
      S => N308,
      Q => mb_altera_core_s_dma_space_addr(24)
    );
  mb_altera_core_s_dma_space_addr_mux0000_7_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(24),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(26),
      O => mb_altera_core_s_dma_space_addr_mux0000_7_1_2754
    );
  mb_altera_core_s_dma_space_addr_23 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_8_1_2755,
      S => N306,
      Q => mb_altera_core_s_dma_space_addr(23)
    );
  mb_altera_core_s_dma_space_addr_mux0000_8_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(23),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(25),
      O => mb_altera_core_s_dma_space_addr_mux0000_8_1_2755
    );
  mb_altera_core_s_dma_space_addr_22 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_9_1_2756,
      S => N304,
      Q => mb_altera_core_s_dma_space_addr(22)
    );
  mb_altera_core_s_dma_space_addr_mux0000_9_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(22),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(24),
      O => mb_altera_core_s_dma_space_addr_mux0000_9_1_2756
    );
  mb_altera_core_s_dma_space_addr_21 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_10_1_2726,
      S => N362,
      Q => mb_altera_core_s_dma_space_addr(21)
    );
  mb_altera_core_s_dma_space_addr_mux0000_10_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(21),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(23),
      O => mb_altera_core_s_dma_space_addr_mux0000_10_1_2726
    );
  mb_altera_core_s_dma_space_addr_20 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_11_1_2728,
      S => N360,
      Q => mb_altera_core_s_dma_space_addr(20)
    );
  mb_altera_core_s_dma_space_addr_mux0000_11_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(20),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(22),
      O => mb_altera_core_s_dma_space_addr_mux0000_11_1_2728
    );
  mb_altera_core_s_dma_space_addr_19 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_12_1_2729,
      S => N358,
      Q => mb_altera_core_s_dma_space_addr(19)
    );
  mb_altera_core_s_dma_space_addr_mux0000_12_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(19),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(21),
      O => mb_altera_core_s_dma_space_addr_mux0000_12_1_2729
    );
  mb_altera_core_s_dma_space_addr_18 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_13_1_2730,
      S => N356,
      Q => mb_altera_core_s_dma_space_addr(18)
    );
  mb_altera_core_s_dma_space_addr_mux0000_13_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(18),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(20),
      O => mb_altera_core_s_dma_space_addr_mux0000_13_1_2730
    );
  mb_altera_core_s_dma_space_addr_17 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_14_1_2731,
      S => N354,
      Q => mb_altera_core_s_dma_space_addr(17)
    );
  mb_altera_core_s_dma_space_addr_mux0000_14_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(17),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(19),
      O => mb_altera_core_s_dma_space_addr_mux0000_14_1_2731
    );
  mb_altera_core_s_dma_space_addr_16 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_15_1_2732,
      S => N352,
      Q => mb_altera_core_s_dma_space_addr(16)
    );
  mb_altera_core_s_dma_space_addr_mux0000_15_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(16),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(18),
      O => mb_altera_core_s_dma_space_addr_mux0000_15_1_2732
    );
  mb_altera_core_s_dma_space_addr_15 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_16_1_2733,
      S => N350,
      Q => mb_altera_core_s_dma_space_addr(15)
    );
  mb_altera_core_s_dma_space_addr_mux0000_16_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(15),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(17),
      O => mb_altera_core_s_dma_space_addr_mux0000_16_1_2733
    );
  mb_altera_core_s_dma_space_addr_14 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_17_1_2734,
      S => N348,
      Q => mb_altera_core_s_dma_space_addr(14)
    );
  mb_altera_core_s_dma_space_addr_mux0000_17_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(14),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(16),
      O => mb_altera_core_s_dma_space_addr_mux0000_17_1_2734
    );
  mb_altera_core_s_dma_space_addr_13 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_18_1_2735,
      S => N346,
      Q => mb_altera_core_s_dma_space_addr(13)
    );
  mb_altera_core_s_dma_space_addr_mux0000_18_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(13),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(15),
      O => mb_altera_core_s_dma_space_addr_mux0000_18_1_2735
    );
  mb_altera_core_s_dma_space_addr_12 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_19_1_2736,
      S => N344,
      Q => mb_altera_core_s_dma_space_addr(12)
    );
  mb_altera_core_s_dma_space_addr_mux0000_19_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(12),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(14),
      O => mb_altera_core_s_dma_space_addr_mux0000_19_1_2736
    );
  mb_altera_core_s_dma_space_addr_11 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_20_1_2737,
      S => N342,
      Q => mb_altera_core_s_dma_space_addr(11)
    );
  mb_altera_core_s_dma_space_addr_mux0000_20_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(11),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(13),
      O => mb_altera_core_s_dma_space_addr_mux0000_20_1_2737
    );
  mb_altera_core_s_dma_space_addr_10 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_21_1_2738,
      S => N340,
      Q => mb_altera_core_s_dma_space_addr(10)
    );
  mb_altera_core_s_dma_space_addr_mux0000_21_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(10),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(12),
      O => mb_altera_core_s_dma_space_addr_mux0000_21_1_2738
    );
  mb_altera_core_s_dma_space_addr_9 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_22_1_2739,
      S => N338,
      Q => mb_altera_core_s_dma_space_addr(9)
    );
  mb_altera_core_s_dma_space_addr_mux0000_22_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(9),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(11),
      O => mb_altera_core_s_dma_space_addr_mux0000_22_1_2739
    );
  mb_altera_core_s_dma_space_addr_8 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_23_1_2740,
      S => N336,
      Q => mb_altera_core_s_dma_space_addr(8)
    );
  mb_altera_core_s_dma_space_addr_mux0000_23_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(8),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(10),
      O => mb_altera_core_s_dma_space_addr_mux0000_23_1_2740
    );
  mb_altera_core_s_dma_space_addr_7 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_24_1_2741,
      S => N334,
      Q => mb_altera_core_s_dma_space_addr(7)
    );
  mb_altera_core_s_dma_space_addr_mux0000_24_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(7),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(9),
      O => mb_altera_core_s_dma_space_addr_mux0000_24_1_2741
    );
  mb_altera_core_s_dma_space_addr_6 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_25_1_2742,
      S => N332,
      Q => mb_altera_core_s_dma_space_addr(6)
    );
  mb_altera_core_s_dma_space_addr_mux0000_25_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(6),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(8),
      O => mb_altera_core_s_dma_space_addr_mux0000_25_1_2742
    );
  mb_altera_core_s_dma_space_addr_5 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_26_1_2743,
      S => N330,
      Q => mb_altera_core_s_dma_space_addr(5)
    );
  mb_altera_core_s_dma_space_addr_mux0000_26_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(5),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(7),
      O => mb_altera_core_s_dma_space_addr_mux0000_26_1_2743
    );
  mb_altera_core_s_dma_space_addr_4 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_27_1_2744,
      S => N328,
      Q => mb_altera_core_s_dma_space_addr(4)
    );
  mb_altera_core_s_dma_space_addr_mux0000_27_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(4),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(6),
      O => mb_altera_core_s_dma_space_addr_mux0000_27_1_2744
    );
  mb_altera_core_s_dma_space_addr_3 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_28_1_2745,
      S => N326,
      Q => mb_altera_core_s_dma_space_addr(3)
    );
  mb_altera_core_s_dma_space_addr_mux0000_28_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(3),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(5),
      O => mb_altera_core_s_dma_space_addr_mux0000_28_1_2745
    );
  mb_altera_core_s_dma_space_addr_2 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_29_1_2746,
      S => N324,
      Q => mb_altera_core_s_dma_space_addr(2)
    );
  mb_altera_core_s_dma_space_addr_mux0000_29_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(2),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(4),
      O => mb_altera_core_s_dma_space_addr_mux0000_29_1_2746
    );
  mb_altera_core_s_dma_space_addr_1 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_30_1_2748,
      S => N320,
      Q => mb_altera_core_s_dma_space_addr(1)
    );
  mb_altera_core_s_dma_space_addr_mux0000_30_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(1),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(3),
      O => mb_altera_core_s_dma_space_addr_mux0000_30_1_2748
    );
  mb_altera_core_s_dma_space_addr_0 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_space_addr_mux0000_31_1_2749,
      S => N318,
      Q => mb_altera_core_s_dma_space_addr(0)
    );
  mb_altera_core_s_dma_space_addr_mux0000_31_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => mb_altera_core_add0001(0),
      I1 => mb_altera_core_N8,
      I2 => mb_altera_core_s_dma_space_mask(2),
      O => mb_altera_core_s_dma_space_addr_mux0000_31_1_2749
    );
  mb_altera_core_s_dma_req : FDR
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_req_mux00011,
      R => inst_emif_iface_sdram_dma_std_dma_engine_dma_done_1301,
      Q => mb_altera_core_s_dma_req_2694
    );
  mb_altera_core_s_dma_req_mux000111 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I1 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      O => mb_altera_core_s_dma_req_mux00011
    );
  mb_altera_core_s_dma_count_8 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_count_mux0003_0_1_2650,
      S => mb_altera_core_N7,
      Q => mb_altera_core_s_dma_count(8)
    );
  mb_altera_core_s_dma_count_7 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_count_mux0003_1_1_2652,
      S => N273,
      Q => mb_altera_core_s_dma_count(7)
    );
  mb_altera_core_s_dma_count_mux0003_1_1 : LUT3
    generic map(
      INIT => X"90"
    )
    port map (
      I0 => mb_altera_core_s_altera_rdfifo_rd_cnt(7),
      I1 => mb_altera_core_Msub_s_dma_count_share0000_cy_6_Q,
      I2 => mb_altera_core_N10,
      O => mb_altera_core_s_dma_count_mux0003_1_1_2652
    );
  mb_altera_core_s_dma_count_6 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_count_mux0003_2_48,
      S => mb_altera_core_N7,
      Q => mb_altera_core_s_dma_count(6)
    );
  mb_altera_core_s_dma_count_5 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_count_mux0003_3_1_2655,
      S => mb_altera_core_N7,
      Q => mb_altera_core_s_dma_count(5)
    );
  mb_altera_core_s_dma_count_4 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_count_mux0003_4_1_2656,
      S => N271,
      Q => mb_altera_core_s_dma_count(4)
    );
  mb_altera_core_s_dma_count_mux0003_4_1 : LUT3
    generic map(
      INIT => X"90"
    )
    port map (
      I0 => mb_altera_core_s_altera_rdfifo_rd_cnt(4),
      I1 => mb_altera_core_Msub_s_dma_count_share0000_cy_3_Q,
      I2 => mb_altera_core_N10,
      O => mb_altera_core_s_dma_count_mux0003_4_1_2656
    );
  mb_altera_core_s_dma_count_3 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_count_mux0003_5_43,
      S => mb_altera_core_N7,
      Q => mb_altera_core_s_dma_count(3)
    );
  mb_altera_core_s_dma_count_2 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_count_mux0003_6_1_2659,
      S => mb_altera_core_N7,
      Q => mb_altera_core_s_dma_count(2)
    );
  mb_altera_core_s_dma_count_1 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_count_mux0003_7_1_2660,
      S => N269,
      Q => mb_altera_core_s_dma_count(1)
    );
  mb_altera_core_s_dma_count_mux0003_7_1 : LUT3
    generic map(
      INIT => X"90"
    )
    port map (
      I0 => mb_altera_core_s_altera_rdfifo_rd_cnt(1),
      I1 => mb_altera_core_Msub_s_dma_count_share0000_cy_0_Q,
      I2 => mb_altera_core_N10,
      O => mb_altera_core_s_dma_count_mux0003_7_1_2660
    );
  mb_altera_core_s_dma_count_0 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => mb_altera_core_s_dma_count_mux0003_8_1_2661,
      S => mb_altera_core_N7,
      Q => mb_altera_core_s_dma_count(0)
    );
  mb_altera_core_altera_asd : FDS
    port map (
      C => emif_clk,
      D => mb_altera_core_altera_asd_mux0000192,
      S => mb_altera_core_altera_asd_mux000039_2217,
      Q => mb_altera_core_altera_asd_2212
    );
  mb_altera_core_altera_asd_mux00001921 : LUT3
    generic map(
      INIT => X"A8"
    )
    port map (
      I0 => mb_altera_core_Flash_State_FSM_FFd6_1605,
      I1 => mb_altera_core_altera_asd_mux000096_2220,
      I2 => mb_altera_core_altera_asd_mux0000153_2213,
      O => mb_altera_core_altera_asd_mux0000192
    );
  inst_base_module_o_DBus_31 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_31_1_865,
      R => inst_base_module_i_cs_inv,
      S => N203,
      Q => inst_base_module_o_DBus(31)
    );
  inst_base_module_o_DBus_mux0002_31_1 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0006,
      I1 => inst_base_module_wd_nmi_en_945,
      O => inst_base_module_o_DBus_mux0002_31_1_865
    );
  inst_base_module_o_DBus_30 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_30_1_864,
      R => inst_base_module_i_cs_inv,
      S => N205,
      Q => inst_base_module_o_DBus(30)
    );
  inst_base_module_o_DBus_mux0002_30_1 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0006,
      I1 => inst_base_module_wd_rst_en_958,
      O => inst_base_module_o_DBus_mux0002_30_1_864
    );
  inst_base_module_o_DBus_29 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_29_1_861,
      R => inst_base_module_i_cs_inv,
      S => N207,
      Q => inst_base_module_o_DBus(29)
    );
  inst_base_module_o_DBus_mux0002_29_1 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0006,
      I1 => inst_base_module_wd_nmi_939,
      O => inst_base_module_o_DBus_mux0002_29_1_861
    );
  inst_base_module_o_DBus_28 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_28_31_860,
      R => inst_base_module_i_cs_inv,
      S => inst_base_module_o_DBus_cmp_eq0000,
      Q => inst_base_module_o_DBus(28)
    );
  inst_base_module_o_DBus_27 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_27_1_857,
      R => inst_base_module_i_cs_inv,
      S => inst_base_module_o_DBus_cmp_eq0000,
      Q => inst_base_module_o_DBus(27)
    );
  inst_base_module_o_DBus_24 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_24_1_854,
      R => inst_base_module_i_cs_inv,
      S => inst_base_module_o_DBus_cmp_eq0000,
      Q => inst_base_module_o_DBus(24)
    );
  inst_base_module_o_DBus_mux0002_24_1 : LUT3
    generic map(
      INIT => X"F8"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => inst_base_module_varindex0000(24),
      I2 => N252,
      O => inst_base_module_o_DBus_mux0002_24_1_854
    );
  inst_base_module_o_DBus_23 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_23_1_853,
      R => inst_base_module_i_cs_inv,
      S => inst_base_module_o_DBus_cmp_eq0005,
      Q => inst_base_module_o_DBus(23)
    );
  inst_base_module_o_DBus_22 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_22_1_852,
      R => inst_base_module_i_cs_inv,
      S => inst_base_module_o_DBus_cmp_eq0000,
      Q => inst_base_module_o_DBus(22)
    );
  inst_base_module_o_DBus_21 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_21_1_851,
      R => inst_base_module_i_cs_inv,
      S => inst_base_module_o_DBus_cmp_eq0005,
      Q => inst_base_module_o_DBus(21)
    );
  inst_base_module_o_DBus_19 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_19_1_846,
      R => inst_base_module_i_cs_inv,
      S => inst_base_module_o_DBus_cmp_eq0000,
      Q => inst_base_module_o_DBus(19)
    );
  inst_base_module_o_DBus_18 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_18_1,
      R => inst_base_module_i_cs_inv,
      S => inst_base_module_o_DBus_cmp_eq0000,
      Q => inst_base_module_o_DBus(18)
    );
  inst_base_module_o_DBus_17 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_17_1,
      R => inst_base_module_i_cs_inv,
      S => inst_base_module_o_DBus_cmp_eq0000,
      Q => inst_base_module_o_DBus(17)
    );
  inst_base_module_o_DBus_16 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_16_1_839,
      R => inst_base_module_i_cs_inv,
      S => inst_base_module_o_DBus_cmp_eq0000,
      Q => inst_base_module_o_DBus(16)
    );
  inst_base_module_o_DBus_15 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_15_1_838,
      R => inst_base_module_i_cs_inv,
      S => N213,
      Q => inst_base_module_o_DBus(15)
    );
  inst_base_module_o_DBus_mux0002_15_1 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0006,
      I1 => inst_base_module_wd_timeout(15),
      O => inst_base_module_o_DBus_mux0002_15_1_838
    );
  inst_base_module_o_DBus_14 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_14_1_837,
      R => inst_base_module_i_cs_inv,
      S => N215,
      Q => inst_base_module_o_DBus(14)
    );
  inst_base_module_o_DBus_mux0002_14_1 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0006,
      I1 => inst_base_module_wd_timeout(14),
      O => inst_base_module_o_DBus_mux0002_14_1_837
    );
  inst_base_module_o_DBus_13 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_13_1_836,
      R => inst_base_module_i_cs_inv,
      S => N217,
      Q => inst_base_module_o_DBus(13)
    );
  inst_base_module_o_DBus_mux0002_13_1 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0006,
      I1 => inst_base_module_wd_timeout(13),
      O => inst_base_module_o_DBus_mux0002_13_1_836
    );
  inst_base_module_o_DBus_12 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_12_1_835,
      R => inst_base_module_i_cs_inv,
      S => N219,
      Q => inst_base_module_o_DBus(12)
    );
  inst_base_module_o_DBus_mux0002_12_1 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0006,
      I1 => inst_base_module_wd_timeout(12),
      O => inst_base_module_o_DBus_mux0002_12_1_835
    );
  inst_base_module_o_DBus_11 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_11_10_833,
      R => inst_base_module_i_cs_inv,
      S => inst_base_module_o_DBus_mux0002_11_4_834,
      Q => inst_base_module_o_DBus(11)
    );
  inst_base_module_o_DBus_10 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_10_1_832,
      R => inst_base_module_i_cs_inv,
      S => N221,
      Q => inst_base_module_o_DBus(10)
    );
  inst_base_module_o_DBus_mux0002_10_1 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0006,
      I1 => inst_base_module_wd_timeout(10),
      O => inst_base_module_o_DBus_mux0002_10_1_832
    );
  inst_base_module_o_DBus_9 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_9_1_875,
      R => inst_base_module_i_cs_inv,
      S => N197,
      Q => inst_base_module_o_DBus(9)
    );
  inst_base_module_o_DBus_mux0002_9_1 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0006,
      I1 => inst_base_module_wd_timeout(9),
      O => inst_base_module_o_DBus_mux0002_9_1_875
    );
  inst_base_module_o_DBus_8 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_8_10_873,
      R => inst_base_module_i_cs_inv,
      S => inst_base_module_o_DBus_mux0002_8_4_874,
      Q => inst_base_module_o_DBus(8)
    );
  inst_base_module_o_DBus_7 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_7_1_872,
      R => inst_base_module_i_cs_inv,
      S => N199,
      Q => inst_base_module_o_DBus(7)
    );
  inst_base_module_o_DBus_mux0002_7_1 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0006,
      I1 => inst_base_module_wd_timeout(7),
      O => inst_base_module_o_DBus_mux0002_7_1_872
    );
  inst_base_module_o_DBus_6 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_6_1_871,
      R => inst_base_module_i_cs_inv,
      S => inst_base_module_o_DBus_cmp_eq0000,
      Q => inst_base_module_o_DBus(6)
    );
  inst_base_module_o_DBus_mux0002_6_1 : LUT3
    generic map(
      INIT => X"F8"
    )
    port map (
      I0 => inst_base_module_wd_timeout(6),
      I1 => inst_base_module_o_DBus_cmp_eq0006,
      I2 => N250,
      O => inst_base_module_o_DBus_mux0002_6_1_871
    );
  inst_base_module_o_DBus_5 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_5_1_870,
      R => inst_base_module_i_cs_inv,
      S => N201,
      Q => inst_base_module_o_DBus(5)
    );
  inst_base_module_o_DBus_mux0002_5_1 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0006,
      I1 => inst_base_module_wd_timeout(5),
      O => inst_base_module_o_DBus_mux0002_5_1_870
    );
  inst_base_module_o_DBus_4 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_4_1_869,
      R => inst_base_module_i_cs_inv,
      S => inst_base_module_o_DBus_cmp_eq0005,
      Q => inst_base_module_o_DBus(4)
    );
  inst_base_module_o_DBus_mux0002_4_1 : LUT3
    generic map(
      INIT => X"F8"
    )
    port map (
      I0 => inst_base_module_wd_timeout(4),
      I1 => inst_base_module_o_DBus_cmp_eq0006,
      I2 => N231,
      O => inst_base_module_o_DBus_mux0002_4_1_869
    );
  inst_base_module_o_DBus_3 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_3_20,
      R => inst_base_module_i_cs_inv,
      S => inst_base_module_o_DBus_cmp_eq0000,
      Q => inst_base_module_o_DBus(3)
    );
  inst_base_module_o_DBus_2 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_2_14,
      R => inst_base_module_i_cs_inv,
      S => inst_base_module_o_DBus_mux0002_2_4_863,
      Q => inst_base_module_o_DBus(2)
    );
  inst_base_module_o_DBus_1 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_1_16,
      R => inst_base_module_i_cs_inv,
      S => inst_base_module_o_DBus_cmp_eq0000,
      Q => inst_base_module_o_DBus(1)
    );
  inst_base_module_o_DBus_0 : FDRS
    port map (
      C => emif_clk,
      D => inst_base_module_o_DBus_mux0002_0_38,
      R => inst_base_module_i_cs_inv,
      S => inst_base_module_o_DBus_mux0002_0_7_831,
      Q => inst_base_module_o_DBus(0)
    );
  mb_altera_core_o_DBus_9 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_9_mux000260_2447,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_o_DBus_9_mux000247_2446,
      Q => mb_altera_core_o_DBus(9)
    );
  mb_altera_core_o_DBus_8 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_8_mux0002105,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_o_DBus_8_mux000222_2439,
      Q => mb_altera_core_o_DBus(8)
    );
  mb_altera_core_o_DBus_7 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_7_mux000292,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_o_DBus_7_mux000215_2428,
      Q => mb_altera_core_o_DBus(7)
    );
  mb_altera_core_o_DBus_7_mux0002921 : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => mb_altera_core_o_DBus_7_mux000264_2431,
      I1 => mb_altera_core_o_DBus_7_mux000290_2432,
      O => mb_altera_core_o_DBus_7_mux000292
    );
  mb_altera_core_o_DBus_6 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_6_mux000296,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_N17,
      Q => mb_altera_core_o_DBus(6)
    );
  mb_altera_core_o_DBus_5 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_5_mux000292,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_o_DBus_5_mux000215_2412,
      Q => mb_altera_core_o_DBus(5)
    );
  mb_altera_core_o_DBus_5_mux0002921 : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => mb_altera_core_o_DBus_5_mux000264_2415,
      I1 => mb_altera_core_o_DBus_5_mux000290_2416,
      O => mb_altera_core_o_DBus_5_mux000292
    );
  mb_altera_core_o_DBus_4 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_4_mux0002120,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_o_DBus_4_mux000218,
      Q => mb_altera_core_o_DBus(4)
    );
  mb_altera_core_o_DBus_3 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_3_mux0000150,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_N17,
      Q => mb_altera_core_o_DBus(3)
    );
  mb_altera_core_o_DBus_2 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_2_mux0002125,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_o_DBus_2_mux000230_2374,
      Q => mb_altera_core_o_DBus(2)
    );
  mb_altera_core_o_DBus_2_mux00021251 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => mb_altera_core_o_DBus_2_mux000243_2375,
      I1 => mb_altera_core_o_DBus_2_mux000295_2377,
      I2 => mb_altera_core_o_DBus_2_mux0002110_2370,
      O => mb_altera_core_o_DBus_2_mux0002125
    );
  mb_altera_core_o_DBus_1 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_1_mux000292,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_N17,
      Q => mb_altera_core_o_DBus(1)
    );
  mb_altera_core_o_DBus_29 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_29_mux000074,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_o_DBus_29_mux000015_2366,
      Q => mb_altera_core_o_DBus(29)
    );
  mb_altera_core_o_DBus_0 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_0_mux0002119,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_N17,
      Q => mb_altera_core_o_DBus(0)
    );
  mb_altera_core_o_DBus_0_mux00021191 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => mb_altera_core_o_DBus_0_mux000286_2247,
      I1 => mb_altera_core_o_DBus_0_mux000227_2244,
      I2 => mb_altera_core_o_DBus_0_mux0002104_2240,
      O => mb_altera_core_o_DBus_0_mux0002119
    );
  mb_altera_core_o_DBus_28 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_28_mux000063,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_N17,
      Q => mb_altera_core_o_DBus(28)
    );
  mb_altera_core_o_DBus_28_mux0000631 : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => mb_altera_core_o_DBus_28_mux00004_2361,
      I1 => mb_altera_core_o_DBus_28_mux000052_2362,
      O => mb_altera_core_o_DBus_28_mux000063
    );
  mb_altera_core_o_DBus_27 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_27_mux000072,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_N17,
      Q => mb_altera_core_o_DBus(27)
    );
  mb_altera_core_o_DBus_26 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_26_mux000060_2352,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_o_DBus_26_mux000047_2351,
      Q => mb_altera_core_o_DBus(26)
    );
  mb_altera_core_o_DBus_31 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_31_mux000060_2391,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_o_DBus_31_mux000047_2390,
      Q => mb_altera_core_o_DBus(31)
    );
  mb_altera_core_o_DBus_25 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_25_mux000060_2346,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_o_DBus_25_mux000047_2345,
      Q => mb_altera_core_o_DBus(25)
    );
  mb_altera_core_o_DBus_30 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_30_mux000062,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_o_DBus_30_mux000014_2380,
      Q => mb_altera_core_o_DBus(30)
    );
  mb_altera_core_o_DBus_30_mux0000621 : LUT3
    generic map(
      INIT => X"EC"
    )
    port map (
      I0 => mb_altera_core_o_DBus(30),
      I1 => mb_altera_core_o_DBus_30_mux000042_2382,
      I2 => mb_altera_core_o_DBus_13_or0000,
      O => mb_altera_core_o_DBus_30_mux000062
    );
  mb_altera_core_o_DBus_19 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_19_mux000072,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_N17,
      Q => mb_altera_core_o_DBus(19)
    );
  mb_altera_core_o_DBus_24 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_24_mux000072,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_N17,
      Q => mb_altera_core_o_DBus(24)
    );
  mb_altera_core_o_DBus_18 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_18_mux000072,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_N17,
      Q => mb_altera_core_o_DBus(18)
    );
  mb_altera_core_o_DBus_23 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_23_mux000060_2335,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_o_DBus_23_mux000047_2334,
      Q => mb_altera_core_o_DBus(23)
    );
  mb_altera_core_o_DBus_17 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_17_mux000072,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_N17,
      Q => mb_altera_core_o_DBus(17)
    );
  mb_altera_core_o_DBus_22 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_22_mux000072,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_N17,
      Q => mb_altera_core_o_DBus(22)
    );
  mb_altera_core_o_DBus_16 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_16_mux000072,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_N17,
      Q => mb_altera_core_o_DBus(16)
    );
  mb_altera_core_o_DBus_21 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_21_mux000060_2324,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_o_DBus_21_mux000047_2323,
      Q => mb_altera_core_o_DBus(21)
    );
  mb_altera_core_o_DBus_15 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_15_mux000065,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_o_DBus_15_mux00004_2284,
      Q => mb_altera_core_o_DBus(15)
    );
  mb_altera_core_o_DBus_20 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_20_mux000060_2318,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_o_DBus_20_mux000047_2317,
      Q => mb_altera_core_o_DBus(20)
    );
  mb_altera_core_o_DBus_14 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_14_mux000070,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_o_DBus_14_mux000014_2278,
      Q => mb_altera_core_o_DBus(14)
    );
  mb_altera_core_o_DBus_14_mux0000701 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => mb_altera_core_o_DBus_14_mux000048_2279,
      I1 => mb_altera_core_o_DBus(14),
      I2 => N780,
      O => mb_altera_core_o_DBus_14_mux000070
    );
  mb_altera_core_o_DBus_13 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_13_mux000062,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_o_DBus_13_mux000014_2272,
      Q => mb_altera_core_o_DBus(13)
    );
  mb_altera_core_o_DBus_13_mux0000621 : LUT3
    generic map(
      INIT => X"EC"
    )
    port map (
      I0 => mb_altera_core_o_DBus(13),
      I1 => mb_altera_core_o_DBus_13_mux000042_2274,
      I2 => mb_altera_core_o_DBus_13_or0000,
      O => mb_altera_core_o_DBus_13_mux000062
    );
  mb_altera_core_o_DBus_11 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_11_mux000260_2264,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_o_DBus_11_mux000247_2263,
      Q => mb_altera_core_o_DBus(11)
    );
  mb_altera_core_o_DBus_10 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_10_mux000260_2258,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_o_DBus_10_mux000247_2257,
      Q => mb_altera_core_o_DBus(10)
    );
  mb_altera_core_o_DBus_12 : FDRS
    port map (
      C => emif_clk,
      D => mb_altera_core_o_DBus_12_mux000060_2270,
      R => mb_altera_core_i_cs_inv,
      S => mb_altera_core_o_DBus_12_mux000047_2269,
      Q => mb_altera_core_o_DBus(12)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_hold_n : FDRS
    generic map(
      INIT => '1'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_hold_n_mux000311,
      R => inst_emif_iface_sdram_dma_std_dma_engine_N42,
      S => i_bus_req_IBUF_497,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_hold_n_1340
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sm_arb_FSM_FFd2 : FDSE
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      CE => inst_emif_iface_sdram_dma_std_dma_engine_dma_done_1301,
      D => ea(15),
      S => inst_emif_iface_sdram_dma_std_dma_engine_sm_arb_FSM_FFd3_1420,
      Q => inst_emif_iface_sdram_dma_std_dma_engine_sm_arb_FSM_FFd2_1419
    );
  inst_base_module_Msub_wd_counter_addsub0000_cy_0_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => inst_base_module_wd_counter(0),
      O => inst_base_module_Msub_wd_counter_addsub0000_cy_0_rt_536
    );
  mb_altera_core_Madd_add0001_Madd_cy_28_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(28),
      O => mb_altera_core_Madd_add0001_Madd_cy_28_rt_1687
    );
  mb_altera_core_Madd_add0001_Madd_cy_27_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(27),
      O => mb_altera_core_Madd_add0001_Madd_cy_27_rt_1685
    );
  mb_altera_core_Madd_add0001_Madd_cy_26_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(26),
      O => mb_altera_core_Madd_add0001_Madd_cy_26_rt_1683
    );
  mb_altera_core_Madd_add0001_Madd_cy_25_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(25),
      O => mb_altera_core_Madd_add0001_Madd_cy_25_rt_1681
    );
  mb_altera_core_Madd_add0001_Madd_cy_24_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(24),
      O => mb_altera_core_Madd_add0001_Madd_cy_24_rt_1679
    );
  mb_altera_core_Madd_add0001_Madd_cy_23_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(23),
      O => mb_altera_core_Madd_add0001_Madd_cy_23_rt_1677
    );
  mb_altera_core_Madd_add0001_Madd_cy_22_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(22),
      O => mb_altera_core_Madd_add0001_Madd_cy_22_rt_1675
    );
  mb_altera_core_Madd_add0001_Madd_cy_21_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(21),
      O => mb_altera_core_Madd_add0001_Madd_cy_21_rt_1673
    );
  mb_altera_core_Madd_add0001_Madd_cy_20_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(20),
      O => mb_altera_core_Madd_add0001_Madd_cy_20_rt_1671
    );
  mb_altera_core_Madd_add0001_Madd_cy_19_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(19),
      O => mb_altera_core_Madd_add0001_Madd_cy_19_rt_1667
    );
  mb_altera_core_Madd_add0001_Madd_cy_18_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(18),
      O => mb_altera_core_Madd_add0001_Madd_cy_18_rt_1665
    );
  mb_altera_core_Madd_add0001_Madd_cy_17_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(17),
      O => mb_altera_core_Madd_add0001_Madd_cy_17_rt_1663
    );
  mb_altera_core_Madd_add0001_Madd_cy_16_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(16),
      O => mb_altera_core_Madd_add0001_Madd_cy_16_rt_1661
    );
  mb_altera_core_Madd_add0001_Madd_cy_15_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(15),
      O => mb_altera_core_Madd_add0001_Madd_cy_15_rt_1659
    );
  mb_altera_core_Madd_add0001_Madd_cy_14_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(14),
      O => mb_altera_core_Madd_add0001_Madd_cy_14_rt_1657
    );
  mb_altera_core_Madd_add0001_Madd_cy_13_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(13),
      O => mb_altera_core_Madd_add0001_Madd_cy_13_rt_1655
    );
  mb_altera_core_Madd_add0001_Madd_cy_12_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(12),
      O => mb_altera_core_Madd_add0001_Madd_cy_12_rt_1653
    );
  mb_altera_core_Madd_add0001_Madd_cy_11_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(11),
      O => mb_altera_core_Madd_add0001_Madd_cy_11_rt_1651
    );
  mb_altera_core_Madd_add0001_Madd_cy_10_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(10),
      O => mb_altera_core_Madd_add0001_Madd_cy_10_rt_1649
    );
  mb_altera_core_Madd_add0001_Madd_cy_9_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(9),
      O => mb_altera_core_Madd_add0001_Madd_cy_9_rt_1703
    );
  mb_altera_core_Madd_add0001_Madd_cy_8_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(8),
      O => mb_altera_core_Madd_add0001_Madd_cy_8_rt_1701
    );
  mb_altera_core_Madd_add0001_Madd_cy_7_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(7),
      O => mb_altera_core_Madd_add0001_Madd_cy_7_rt_1699
    );
  mb_altera_core_Madd_add0001_Madd_cy_6_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(6),
      O => mb_altera_core_Madd_add0001_Madd_cy_6_rt_1697
    );
  mb_altera_core_Madd_add0001_Madd_cy_5_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(5),
      O => mb_altera_core_Madd_add0001_Madd_cy_5_rt_1695
    );
  mb_altera_core_Madd_add0001_Madd_cy_4_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(4),
      O => mb_altera_core_Madd_add0001_Madd_cy_4_rt_1693
    );
  mb_altera_core_Madd_add0001_Madd_cy_3_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(3),
      O => mb_altera_core_Madd_add0001_Madd_cy_3_rt_1691
    );
  mb_altera_core_Madd_add0001_Madd_cy_2_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(2),
      O => mb_altera_core_Madd_add0001_Madd_cy_2_rt_1689
    );
  mb_altera_core_Madd_add0001_Madd_cy_1_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(1),
      O => mb_altera_core_Madd_add0001_Madd_cy_1_rt_1669
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_30_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(30),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_30_rt_1837
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_29_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(29),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_29_rt_1833
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_28_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(28),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_28_rt_1831
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_27_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(27),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_27_rt_1829
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_26_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(26),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_26_rt_1827
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_25_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(25),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_25_rt_1825
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_24_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(24),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_24_rt_1823
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_23_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(23),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_23_rt_1821
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_22_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(22),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_22_rt_1819
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_21_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(21),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_21_rt_1817
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_20_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(20),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_20_rt_1815
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_19_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(19),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_19_rt_1811
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_18_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(18),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_18_rt_1809
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_17_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(17),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_17_rt_1807
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_16_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(16),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_16_rt_1805
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_15_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(15),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_15_rt_1803
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_14_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(14),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_14_rt_1801
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_13_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(13),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_13_rt_1799
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_12_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(12),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_12_rt_1797
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_11_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(11),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_11_rt_1795
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_10_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(10),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_10_rt_1793
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_9_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(9),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_9_rt_1851
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_8_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(8),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_8_rt_1849
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_7_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(7),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_7_rt_1847
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_6_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(6),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_6_rt_1845
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_5_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(5),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_5_rt_1843
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_4_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(4),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_4_rt_1841
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_3_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(3),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_3_rt_1839
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_2_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(2),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_2_rt_1835
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_1_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(1),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_cy_1_rt_1813
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_29_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(29),
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_29_rt_1788
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_28_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(28),
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_28_rt_1786
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_27_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(27),
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_27_rt_1784
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_26_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(26),
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_26_rt_1782
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_25_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(25),
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_25_rt_1780
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_24_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(24),
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_24_rt_1778
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_23_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(23),
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_23_rt_1776
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_22_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(22),
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_22_rt_1774
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_21_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(21),
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_21_rt_1772
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_20_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(20),
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_20_rt_1770
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_19_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(19),
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_19_rt_1768
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_18_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(18),
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_18_rt_1766
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_17_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(17),
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_17_rt_1764
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_16_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(16),
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_16_rt_1762
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_15_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(15),
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_15_rt_1760
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_14_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(14),
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_14_rt_1758
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_13_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(13),
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_13_rt_1756
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_12_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(12),
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_12_rt_1754
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_11_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(11),
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_11_rt_1752
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_cy_10_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(10),
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_cy_10_rt_1750
    );
  mb_altera_core_Madd_add0001_Madd_xor_29_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_addr(29),
      O => mb_altera_core_Madd_add0001_Madd_xor_29_rt_1705
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_31_rt : LUT1
    generic map(
      INIT => X"2"
    )
    port map (
      I0 => mb_altera_core_s_dma_timeout_cntr(31),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_xor_31_rt_1853
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_17_19 : LUT4
    generic map(
      INIT => X"F4F0"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(9),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row(8),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_17_17_1388,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N60,
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_17_19_1389
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_17_4_SW0 : LUT3
    generic map(
      INIT => X"EC"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(10),
      I1 => N786,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N36,
      O => N290
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_and00001 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(0),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row(1),
      I2 => N541,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N47,
      O => inst_emif_iface_sdram_dma_std_dma_engine_N36
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_17_4_SW1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(0),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row(7),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      O => N543
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_0_51 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(3),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(4),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N35,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_0_50_1451,
      O => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_0_51_1452
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_0_4_SW0_SW0 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(0),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(2),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      O => N545
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_0_4 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => N797,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(4),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(1),
      I3 => N545,
      O => inst_emif_iface_sdram_dma_std_dma_engine_N35
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_0_61_SW0 : LUT3
    generic map(
      INIT => X"DF"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(4),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(7),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(6),
      O => N549
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_1_44 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_N35,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(3),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(5),
      I3 => N549,
      O => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_1_44_1455
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_11_531 : LUT4
    generic map(
      INIT => X"E4A0"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(3),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row(2),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_11_36_1367,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N33,
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_11_53
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_15_8 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(15),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N38,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_15_3_1382,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N33,
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_15_8_1384
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_10_6 : LUT4
    generic map(
      INIT => X"B3A0"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(10),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row(2),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N38,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N33,
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_10_6_1364
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_2_32 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(4),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N0,
      O => inst_emif_iface_sdram_dma_std_dma_engine_N51
    );
  mb_altera_core_s_dma_count_mux0003_0_1 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => mb_altera_core_s_dma_count(8),
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => N556,
      I3 => mb_altera_core_N10,
      O => mb_altera_core_s_dma_count_mux0003_0_1_2650
    );
  mb_altera_core_s_dma_count_mux0003_3_SW0_SW0 : LUT3
    generic map(
      INIT => X"C9"
    )
    port map (
      I0 => mb_altera_core_Msub_s_dma_count_share0000_cy_3_Q,
      I1 => mb_altera_core_s_altera_rdfifo_rd_cnt(5),
      I2 => mb_altera_core_s_altera_rdfifo_rd_cnt(4),
      O => N558
    );
  mb_altera_core_s_dma_count_mux0003_3_1 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => mb_altera_core_s_dma_count(5),
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => N558,
      I3 => mb_altera_core_N10,
      O => mb_altera_core_s_dma_count_mux0003_3_1_2655
    );
  mb_altera_core_s_dma_count_mux0003_6_SW0_SW0 : LUT3
    generic map(
      INIT => X"C9"
    )
    port map (
      I0 => mb_altera_core_s_altera_rdfifo_rd_cnt(1),
      I1 => mb_altera_core_s_altera_rdfifo_rd_cnt(2),
      I2 => mb_altera_core_Msub_s_dma_count_share0000_cy_0_Q,
      O => N560
    );
  mb_altera_core_s_dma_count_mux0003_6_1 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => mb_altera_core_s_dma_count(2),
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => N560,
      I3 => mb_altera_core_N10,
      O => mb_altera_core_s_dma_count_mux0003_6_1_2659
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_5_1_SW0_SW0 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(1),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(0),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(2),
      O => N562
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_In641 : LUT4
    generic map(
      INIT => X"5702"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I2 => N564,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N46,
      O => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_In64
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_13_281 : LUT4
    generic map(
      INIT => X"AA80"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(5),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N24,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N52,
      I3 => N795,
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_13_28
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_12_521 : LUT4
    generic map(
      INIT => X"AA80"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(4),
      I1 => N794,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_12_29_1370,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N431,
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_12_52
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_15_351 : LUT4
    generic map(
      INIT => X"AA80"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(7),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N52,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_15_18_1381,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N431,
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_15_35
    );
  mb_altera_core_o_DBus_1_mux0002921 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => mb_altera_core_o_DBus_1_mux000267_2308,
      I1 => i_altera_ns_IBUF_491,
      I2 => mb_altera_core_o_DBus_0_cmp_eq0000,
      I3 => N566,
      O => mb_altera_core_o_DBus_1_mux000292
    );
  inst_base_module_o_DBus_mux0002_3_201 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_o_DBus_mux0002_3_0_866,
      I1 => N801,
      I2 => inst_base_module_irq_enable(7),
      I3 => inst_base_module_o_DBus_mux0002_3_9_868,
      O => inst_base_module_o_DBus_mux0002_3_20
    );
  inst_base_module_o_DBus_mux0002_1_161 : LUT4
    generic map(
      INIT => X"FFF8"
    )
    port map (
      I0 => inst_base_module_irq_enable(5),
      I1 => inst_base_module_o_DBus_cmp_eq0001,
      I2 => inst_base_module_o_DBus_mux0002_1_7_849,
      I3 => inst_base_module_o_DBus_mux0002_1_0_847,
      O => inst_base_module_o_DBus_mux0002_1_16
    );
  mb_altera_core_o_DBus_6_mux0002961 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => mb_altera_core_o_DBus_6_mux00024_2421,
      I1 => mb_altera_core_o_DBus_6_mux000224_2420,
      I2 => mb_altera_core_o_DBus_6_mux000273_2424,
      O => mb_altera_core_o_DBus_6_mux000296
    );
  mb_altera_core_o_DBus_15_mux0000651 : LUT4
    generic map(
      INIT => X"FFEC"
    )
    port map (
      I0 => mb_altera_core_o_DBus(15),
      I1 => mb_altera_core_o_DBus_15_mux000048_2285,
      I2 => mb_altera_core_o_DBus_13_or0000,
      I3 => mb_altera_core_o_DBus_15_mux000033_2283,
      O => mb_altera_core_o_DBus_15_mux000065
    );
  inst_base_module_o_DBus_mux0002_27_1 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_base_module_varindex0000(27),
      I1 => inst_emif_iface_addr_r(4),
      I2 => inst_base_module_o_DBus_cmp_eq0004,
      I3 => inst_base_module_irq_mask(27),
      O => inst_base_module_o_DBus_mux0002_27_1_857
    );
  inst_base_module_o_DBus_mux0002_23_1 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_base_module_varindex0000(23),
      I1 => inst_emif_iface_addr_r(4),
      I2 => inst_base_module_o_DBus_cmp_eq0004,
      I3 => inst_base_module_irq_mask(23),
      O => inst_base_module_o_DBus_mux0002_23_1_853
    );
  inst_base_module_o_DBus_mux0002_22_1 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_base_module_varindex0000(22),
      I1 => inst_emif_iface_addr_r(4),
      I2 => inst_base_module_o_DBus_cmp_eq0004,
      I3 => inst_base_module_irq_mask(22),
      O => inst_base_module_o_DBus_mux0002_22_1_852
    );
  inst_base_module_o_DBus_mux0002_21_1 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_base_module_varindex0000(21),
      I1 => inst_emif_iface_addr_r(4),
      I2 => inst_base_module_o_DBus_cmp_eq0004,
      I3 => inst_base_module_irq_mask(21),
      O => inst_base_module_o_DBus_mux0002_21_1_851
    );
  inst_base_module_o_DBus_mux0002_19_1 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_base_module_varindex0000(19),
      I1 => inst_emif_iface_addr_r(4),
      I2 => inst_base_module_o_DBus_cmp_eq0004,
      I3 => inst_base_module_irq_mask(19),
      O => inst_base_module_o_DBus_mux0002_19_1_846
    );
  inst_base_module_o_DBus_mux0002_16_1 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_base_module_varindex0000(16),
      I1 => inst_emif_iface_addr_r(4),
      I2 => inst_base_module_o_DBus_cmp_eq0004,
      I3 => inst_base_module_irq_mask(16),
      O => inst_base_module_o_DBus_mux0002_16_1_839
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_6_82 : LUT4
    generic map(
      INIT => X"EC20"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(1),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(2),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N54,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_6_44_1466,
      O => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000(6)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_6_30 : LUT4
    generic map(
      INIT => X"FF7F"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(0),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(1),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(4),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_6_27,
      O => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_6_30_1465
    );
  inst_base_module_o_DBus_mux0002_0_381 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_o_DBus_mux0002_0_25_828,
      I1 => inst_base_module_key_state_cmp_eq0002,
      I2 => inst_base_module_bank_addr(0),
      I3 => inst_base_module_o_DBus_mux0002_0_28_829,
      O => inst_base_module_o_DBus_mux0002_0_38
    );
  mb_altera_core_altera_asd_mux000039 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => mb_altera_core_altera_asd_mux000025_2215,
      I1 => mb_altera_core_altera_asd_mux000029_2216,
      I2 => mb_altera_core_altera_asd_2212,
      I3 => mb_altera_core_altera_asd_mux00006,
      O => mb_altera_core_altera_asd_mux000039_2217
    );
  mb_altera_core_s_dma_count_mux0003_2_481 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => mb_altera_core_s_dma_count(6),
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_N10,
      I3 => mb_altera_core_s_dma_count_mux0003_2_34_2653,
      O => mb_altera_core_s_dma_count_mux0003_2_48
    );
  mb_altera_core_s_dma_count_mux0003_5_431 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => mb_altera_core_s_dma_count(3),
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_N10,
      I3 => mb_altera_core_s_dma_count_mux0003_5_30_2657,
      O => mb_altera_core_s_dma_count_mux0003_5_43
    );
  inst_base_module_o_DBus_mux0002_2_141 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0004,
      I1 => inst_base_module_irq_mask(2),
      I2 => inst_base_module_o_DBus_cmp_eq0001,
      I3 => inst_base_module_irq_enable(6),
      O => inst_base_module_o_DBus_mux0002_2_14
    );
  mb_altera_core_o_DBus_4_mux00021201 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_addr_r(2),
      I1 => mb_altera_core_o_DBus_4_mux000236_2405,
      I2 => mb_altera_core_o_DBus_4_mux0002108_2400,
      I3 => mb_altera_core_o_DBus_4_mux000239_2406,
      O => mb_altera_core_o_DBus_4_mux0002120
    );
  mb_altera_core_o_DBus_3_mux00001501 : LUT4
    generic map(
      INIT => X"FFA8"
    )
    port map (
      I0 => inst_emif_iface_addr_r(2),
      I1 => mb_altera_core_o_DBus_3_mux000013_2394,
      I2 => mb_altera_core_o_DBus_3_mux000016_2396,
      I3 => N574,
      O => mb_altera_core_o_DBus_3_mux0000150
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_1_471_SW0 : LUT4
    generic map(
      INIT => X"FFF7"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(3),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(4),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_1_3_1454,
      I3 => N792,
      O => N578
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_1_471 : LUT4
    generic map(
      INIT => X"A8A0"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(7),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I2 => N776,
      I3 => N578,
      O => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_1_47
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_4_Q : LUT4
    generic map(
      INIT => X"EC20"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(3),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(4),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N35,
      I3 => N580,
      O => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000(4)
    );
  mb_altera_core_s_dma_count_mux0003_8_1 : LUT4
    generic map(
      INIT => X"F222"
    )
    port map (
      I0 => N793,
      I1 => mb_altera_core_Msub_s_dma_count_share0000_cy_0_Q,
      I2 => mb_altera_core_s_dma_count(0),
      I3 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      O => mb_altera_core_s_dma_count_mux0003_8_1_2661
    );
  mb_altera_core_altera_state_FSM_FFd5_In1 : LUT4
    generic map(
      INIT => X"AAA2"
    )
    port map (
      I0 => mb_altera_core_altera_state_FSM_FFd5_2233,
      I1 => i_altera_read_Busy_bar_IBUF_493,
      I2 => mb_altera_core_Write_Fifo_ef,
      I3 => i_altera_write_ff_ff_IBUF_495,
      O => mb_altera_core_altera_state_FSM_FFd5_In1_2234
    );
  mb_altera_core_o_DBus_27_mux0000721 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => mb_altera_core_o_DBus_27_mux000013_2354,
      I1 => mb_altera_core_Read_Fifo_Data_in(27),
      I2 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      I3 => N588,
      O => mb_altera_core_o_DBus_27_mux000072
    );
  mb_altera_core_o_DBus_19_mux0000721_SW0 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => mb_altera_core_o_DBus_19_mux000045_2305,
      I1 => mb_altera_core_o_DBus_10_cmp_eq0005,
      I2 => mb_altera_core_s_dma_space_mask(19),
      I3 => mb_altera_core_o_DBus_19_mux000033_2304,
      O => N590
    );
  mb_altera_core_o_DBus_19_mux0000721 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => mb_altera_core_o_DBus_19_mux000013_2303,
      I1 => mb_altera_core_Read_Fifo_Data_in(19),
      I2 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      I3 => N590,
      O => mb_altera_core_o_DBus_19_mux000072
    );
  mb_altera_core_o_DBus_24_mux0000721_SW0 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => mb_altera_core_o_DBus_24_mux000045_2339,
      I1 => mb_altera_core_o_DBus_10_cmp_eq0005,
      I2 => mb_altera_core_s_dma_space_mask(24),
      I3 => mb_altera_core_o_DBus_24_mux000033_2338,
      O => N592
    );
  mb_altera_core_o_DBus_24_mux0000721 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => mb_altera_core_o_DBus_24_mux000013_2337,
      I1 => mb_altera_core_Read_Fifo_Data_in(24),
      I2 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      I3 => N592,
      O => mb_altera_core_o_DBus_24_mux000072
    );
  mb_altera_core_o_DBus_18_mux0000721_SW0 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => mb_altera_core_o_DBus_18_mux000045_2300,
      I1 => mb_altera_core_o_DBus_10_cmp_eq0005,
      I2 => mb_altera_core_s_dma_space_mask(18),
      I3 => mb_altera_core_o_DBus_18_mux000033_2299,
      O => N594
    );
  mb_altera_core_o_DBus_18_mux0000721 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => mb_altera_core_o_DBus_18_mux000013_2298,
      I1 => mb_altera_core_Read_Fifo_Data_in(18),
      I2 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      I3 => N594,
      O => mb_altera_core_o_DBus_18_mux000072
    );
  mb_altera_core_o_DBus_17_mux0000721_SW0 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => mb_altera_core_o_DBus_17_mux000045_2295,
      I1 => mb_altera_core_o_DBus_10_cmp_eq0005,
      I2 => mb_altera_core_s_dma_space_mask(17),
      I3 => mb_altera_core_o_DBus_17_mux000033_2294,
      O => N596
    );
  mb_altera_core_o_DBus_17_mux0000721 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => mb_altera_core_o_DBus_17_mux000013_2293,
      I1 => mb_altera_core_Read_Fifo_Data_in(17),
      I2 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      I3 => N596,
      O => mb_altera_core_o_DBus_17_mux000072
    );
  mb_altera_core_o_DBus_22_mux0000721_SW0 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => mb_altera_core_o_DBus_22_mux000045_2328,
      I1 => mb_altera_core_o_DBus_10_cmp_eq0005,
      I2 => mb_altera_core_s_dma_space_mask(22),
      I3 => mb_altera_core_o_DBus_22_mux000033_2327,
      O => N598
    );
  mb_altera_core_o_DBus_22_mux0000721 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => mb_altera_core_o_DBus_22_mux000013_2326,
      I1 => mb_altera_core_Read_Fifo_Data_in(22),
      I2 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      I3 => N598,
      O => mb_altera_core_o_DBus_22_mux000072
    );
  mb_altera_core_o_DBus_16_mux0000721_SW0 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => mb_altera_core_o_DBus_16_mux000045_2290,
      I1 => mb_altera_core_o_DBus_10_cmp_eq0005,
      I2 => mb_altera_core_s_dma_space_mask(16),
      I3 => mb_altera_core_o_DBus_16_mux000033_2289,
      O => N600
    );
  mb_altera_core_o_DBus_16_mux0000721 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => mb_altera_core_o_DBus_16_mux000013_2288,
      I1 => mb_altera_core_Read_Fifo_Data_in(16),
      I2 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      I3 => N600,
      O => mb_altera_core_o_DBus_16_mux000072
    );
  mb_altera_core_Write_fifo_wr_Cnt_and000057_SW0 : LUT3
    generic map(
      INIT => X"EF"
    )
    port map (
      I0 => mb_altera_core_Write_fifo_wr_Cnt_and000037_2177,
      I1 => mb_altera_core_Write_fifo_wr_Cnt_and000012_2176,
      I2 => mb_altera_core_Write_fifo_wr_Cnt(5),
      O => N602
    );
  mb_altera_core_Write_fifo_wr_Cnt_and000057 : LUT4
    generic map(
      INIT => X"0A02"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_Write_En_2128,
      I1 => mb_altera_core_Write_fifo_wr_Cnt(4),
      I2 => mb_altera_core_s_altera_write_busy_2612,
      I3 => N602,
      O => mb_altera_core_Write_fifo_wr_Cnt_and0000
    );
  mb_altera_core_Write_fifo_wr_Cnt_not000130_SW0 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => mb_altera_core_Write_fifo_wr_Cnt(9),
      I1 => mb_altera_core_Write_fifo_wr_Cnt_not000110_2180,
      I2 => mb_altera_core_Write_fifo_wr_Cnt_not00014_2181,
      O => N604
    );
  inst_base_module_o_DBus_mux0002_0_28 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => inst_base_module_irq_enable(4),
      I1 => inst_emif_iface_addr_r(1),
      I2 => inst_base_module_N5,
      I3 => inst_emif_iface_addr_r(0),
      O => inst_base_module_o_DBus_mux0002_0_28_829
    );
  mb_altera_core_s_dma_space_mask_and00001 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => inst_emif_iface_addr_r(2),
      I1 => inst_emif_iface_addr_r(3),
      I2 => mb_altera_core_N42,
      I3 => mb_altera_core_N15,
      O => mb_altera_core_s_dma_space_mask_and0000
    );
  mb_altera_core_s_dma_base_addr_and00001 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => inst_emif_iface_addr_r(1),
      I1 => inst_emif_iface_addr_r(2),
      I2 => mb_altera_core_N13,
      I3 => mb_altera_core_N15,
      O => mb_altera_core_s_dma_base_addr_and0000
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut_0_Q : LUT2
    generic map(
      INIT => X"6"
    )
    port map (
      I0 => mb_altera_core_Write_fifo_wr_Cnt(0),
      I1 => mb_altera_core_Write_fifo_wr_Cnt_and0000,
      O => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(0)
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut_1_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_Write_fifo_wr_Cnt(1),
      I1 => mb_altera_core_Write_fifo_wr_Cnt_and0000,
      O => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(1)
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut_2_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_Write_fifo_wr_Cnt(2),
      I1 => mb_altera_core_Write_fifo_wr_Cnt_and0000,
      O => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(2)
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut_3_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_Write_fifo_wr_Cnt(3),
      I1 => mb_altera_core_Write_fifo_wr_Cnt_and0000,
      O => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(3)
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut_5_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_Write_fifo_wr_Cnt(5),
      I1 => mb_altera_core_Write_fifo_wr_Cnt_and0000,
      O => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(5)
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut_6_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_Write_fifo_wr_Cnt(6),
      I1 => mb_altera_core_Write_fifo_wr_Cnt_and0000,
      O => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(6)
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut_7_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_Write_fifo_wr_Cnt(7),
      I1 => mb_altera_core_Write_fifo_wr_Cnt_and0000,
      O => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(7)
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut_8_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_Write_fifo_wr_Cnt(8),
      I1 => mb_altera_core_Write_fifo_wr_Cnt_and0000,
      O => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(8)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_2_39 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(4),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N0,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_2_28_1241,
      O => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_2_39_1242
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut_9_Q : LUT2
    generic map(
      INIT => X"9"
    )
    port map (
      I0 => mb_altera_core_Write_fifo_wr_Cnt(9),
      I1 => mb_altera_core_Write_fifo_wr_Cnt_and0000,
      O => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(9)
    );
  inst_base_module_irq_enable_not00011 : LUT4
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => inst_emif_iface_addr_r(1),
      I1 => inst_base_module_N5,
      I2 => inst_base_module_and0000_inv,
      I3 => inst_emif_iface_addr_r(0),
      O => inst_base_module_irq_enable_not0001
    );
  mb_altera_core_Flash_State_FSM_FFd7_In20 : LUT4
    generic map(
      INIT => X"77AF"
    )
    port map (
      I0 => inst_emif_iface_addr_r(1),
      I1 => inst_base_module_irq_mask_and0000,
      I2 => mb_altera_core_N13,
      I3 => inst_emif_iface_addr_r(2),
      O => mb_altera_core_Flash_State_FSM_FFd7_In20_1609
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_53_SW0 : LUT4
    generic map(
      INIT => X"2AAA"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_col(0),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_col(1),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_col(2),
      O => N608
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_53 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_37,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(4),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N0,
      I3 => N608,
      O => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_53_1247
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_6_13_SW0 : LUT4
    generic map(
      INIT => X"A2AA"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_col(4),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N9,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_col(5),
      O => N610
    );
  inst_base_module_key_state_cmp_eq000334_SW0 : LUT4
    generic map(
      INIT => X"EFFF"
    )
    port map (
      I0 => inst_emif_iface_edi_r(18),
      I1 => inst_emif_iface_edi_r(16),
      I2 => inst_emif_iface_edi_r(17),
      I3 => inst_emif_iface_edi_r(19),
      O => N612
    );
  mb_altera_core_o_DBus_0_mux000286_SW0 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => mb_altera_core_s_dma_en_2662,
      I1 => inst_emif_iface_addr_r(2),
      I2 => mb_altera_core_N13,
      I3 => inst_emif_iface_addr_r(1),
      O => N614
    );
  mb_altera_core_o_DBus_0_mux000286 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => mb_altera_core_o_DBus_0_mux000259_2245,
      I2 => N614,
      I3 => mb_altera_core_o_DBus_0_mux000265_2246,
      O => mb_altera_core_o_DBus_0_mux000286_2247
    );
  inst_base_module_o_DBus_mux0002_3_9_SW0 : LUT4
    generic map(
      INIT => X"6040"
    )
    port map (
      I0 => inst_emif_iface_addr_r(0),
      I1 => inst_emif_iface_addr_r(1),
      I2 => inst_emif_iface_addr_r(2),
      I3 => inst_base_module_irq_mask(3),
      O => N616
    );
  inst_base_module_key_state_cmp_eq000434_SW0 : LUT4
    generic map(
      INIT => X"EFFF"
    )
    port map (
      I0 => inst_emif_iface_edi_r(30),
      I1 => inst_emif_iface_edi_r(28),
      I2 => inst_emif_iface_edi_r(29),
      I3 => inst_emif_iface_edi_r(31),
      O => N618
    );
  inst_base_module_key_state_cmp_eq000434 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => inst_base_module_key_state_cmp_eq0004110_782,
      I1 => inst_base_module_key_state_cmp_eq00041211_783,
      I2 => inst_base_module_key_state_cmp_eq000421_784,
      I3 => N618,
      O => inst_base_module_key_state_cmp_eq0004
    );
  mb_altera_core_o_DBus_29_mux0000741_SW0 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => inst_emif_iface_addr_r(2),
      I1 => inst_emif_iface_addr_r(3),
      I2 => mb_altera_core_N42,
      I3 => mb_altera_core_s_dma_space_mask(29),
      O => N620
    );
  mb_altera_core_o_DBus_29_mux0000741 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => mb_altera_core_o_DBus_29_mux000034_2367,
      I2 => N620,
      I3 => mb_altera_core_o_DBus_29_mux000040_2368,
      O => mb_altera_core_o_DBus_29_mux000074
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ea_12_mux00011 : LUT4
    generic map(
      INIT => X"9080"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_row(10),
      O => inst_emif_iface_sdram_dma_std_dma_engine_ea_12_mux0001
    );
  inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt_mux0000_1_1 : LUT4
    generic map(
      INIT => X"AA90"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt(0),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      O => inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt_mux0000(1)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_6_210_SW0 : LUT4
    generic map(
      INIT => X"7FFF"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_col(4),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_col(5),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_col(6),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_col(7),
      O => N622
    );
  inst_base_module_o_DBus_mux0002_0_25_SW0 : LUT4
    generic map(
      INIT => X"A7F7"
    )
    port map (
      I0 => inst_emif_iface_addr_r(2),
      I1 => inst_base_module_irq_mask(0),
      I2 => inst_emif_iface_addr_r(1),
      I3 => inst_base_module_led_enable(0),
      O => N624
    );
  mb_altera_core_o_DBus_7_mux000255 : LUT4
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => inst_emif_iface_addr_r(0),
      I1 => inst_emif_iface_addr_r(4),
      I2 => inst_emif_iface_addr_r(1),
      I3 => inst_emif_iface_addr_r(2),
      O => mb_altera_core_o_DBus_4_mux00029
    );
  inst_base_module_wd_rst_clr_not00021 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0006,
      I1 => inst_base_module_wd_rst_952,
      I2 => inst_emif_iface_edi_r(28),
      I3 => inst_base_module_and0000_inv,
      O => inst_base_module_wd_rst_clr_not0002
    );
  inst_base_module_wd_nmi_clr_not00021 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0006,
      I1 => inst_base_module_wd_nmi_939,
      I2 => inst_emif_iface_edi_r(29),
      I3 => inst_base_module_and0000_inv,
      O => inst_base_module_wd_nmi_clr_not0002
    );
  mb_altera_core_Flash_State_FSM_N31 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => inst_emif_iface_cs_r_8_Q,
      I1 => inst_emif_iface_wr_r_1474,
      I2 => mb_altera_core_Flash_State_FSM_FFd7_1607,
      I3 => mb_altera_core_flash_enable_2235,
      O => mb_altera_core_Flash_State_FSM_N3
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_In7 : LUT4
    generic map(
      INIT => X"32FF"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_arb_FSM_FFd2_1419,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_arb_FSM_FFd3_1420,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      O => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_In7_1431
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_18_0 : LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(18),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_18_0_1392
    );
  mb_altera_core_s_err_clr_and00001 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => mb_altera_core_N15,
      I1 => inst_emif_iface_addr_r(2),
      I2 => mb_altera_core_N13,
      I3 => inst_emif_iface_addr_r(1),
      O => mb_altera_core_s_err_clr_and0000
    );
  mb_altera_core_flash_enable_and00001 : LUT4
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => mb_altera_core_N40,
      I1 => mb_altera_core_N15,
      I2 => inst_emif_iface_addr_r(2),
      I3 => inst_emif_iface_addr_r(4),
      O => mb_altera_core_flash_enable_and0000
    );
  inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_20_31 : LUT4
    generic map(
      INIT => X"54F4"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_bank(0),
      O => inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_20_31_1222
    );
  inst_base_module_o_DBus_mux0002_28_19 : LUT3
    generic map(
      INIT => X"04"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => inst_emif_iface_addr_r(2),
      I2 => inst_emif_iface_addr_r(3),
      O => inst_base_module_o_DBus_mux0002_28_19_859
    );
  inst_base_module_clken4ms_not00011 : LUT2
    generic map(
      INIT => X"D"
    )
    port map (
      I0 => inst_base_module_div10_5(10),
      I1 => inst_base_module_div10_5_r_631,
      O => inst_base_module_clken4ms_not0001
    );
  mb_altera_core_s_err_invalid_rd_and00001 : LUT3
    generic map(
      INIT => X"04"
    )
    port map (
      I0 => mb_altera_core_s_altera_rdfifo_valid,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_1302,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_dma_done_1301,
      O => mb_altera_core_s_err_invalid_rd_and0000
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_11_0 : LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(11),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_11_0_1365
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_4_0 : LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(4),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      O => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_4_0_1250
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_1_0 : LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(1),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      O => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_1_0_1239
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_0 : LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(3),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      O => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_0_1244
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_5_0 : LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(5),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      O => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_5_0_1252
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_6_0 : LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(6),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      O => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_6_0_1255
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_16_0 : LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(16),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_16_0_1385
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_7_0 : LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(7),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      O => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_7_0_1257
    );
  inst_base_module_and00001 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => inst_emif_iface_cs_r_0_Q,
      I1 => inst_emif_iface_wr_r_1474,
      O => inst_base_module_and0000
    );
  mb_altera_core_Flash_Read_Reg_0_not00011 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => mb_altera_core_Flash_Cnt(1),
      I1 => mb_altera_core_Flash_Cnt(0),
      I2 => mb_altera_core_Flash_Cnt(2),
      I3 => mb_altera_core_Flash_State_FSM_FFd3_1601,
      O => mb_altera_core_Flash_Read_Reg_0_not0001
    );
  inst_base_module_i_ABus_4_1 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => inst_emif_iface_cs_r_0_Q,
      I1 => inst_emif_iface_wr_r_1474,
      I2 => inst_emif_iface_addr_r(4),
      O => inst_base_module_i_ABus_4_0
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ea_14_mux00011 : LUT4
    generic map(
      INIT => X"8A08"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_bank(1),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      O => inst_emif_iface_sdram_dma_std_dma_engine_ea_14_mux0001
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ea_13_mux00011 : LUT4
    generic map(
      INIT => X"8A08"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_bank(0),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      O => inst_emif_iface_sdram_dma_std_dma_engine_ea_13_mux0001
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_Out21 : LUT3
    generic map(
      INIT => X"08"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      O => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_cmp_eq0004
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ea_11_mux00011 : LUT4
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row(9),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      O => inst_emif_iface_sdram_dma_std_dma_engine_ea_11_mux0001
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ea_10_mux00011 : LUT4
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row(8),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      O => inst_emif_iface_sdram_dma_std_dma_engine_ea_10_mux0001
    );
  inst_base_module_wd_rst_en_mux00001 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => inst_emif_iface_cs_r_0_Q,
      I1 => inst_emif_iface_wr_r_1474,
      I2 => inst_emif_iface_edi_r(30),
      O => inst_base_module_wd_rst_en_mux0000
    );
  inst_base_module_wd_nmi_en_mux00001 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => inst_emif_iface_cs_r_0_Q,
      I1 => inst_emif_iface_wr_r_1474,
      I2 => inst_emif_iface_edi_r(31),
      O => inst_base_module_wd_nmi_en_mux0000
    );
  mb_altera_core_Flash_Cnt_mux0000_2_1 : LUT4
    generic map(
      INIT => X"3236"
    )
    port map (
      I0 => mb_altera_core_Flash_State_FSM_FFd1_1599,
      I1 => mb_altera_core_Flash_Cnt(0),
      I2 => mb_altera_core_Flash_State_FSM_FFd4_1602,
      I3 => mb_altera_core_Flash_State_FSM_FFd7_1607,
      O => mb_altera_core_Flash_Cnt_mux0000(2)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ed_t_mux000211 : LUT4
    generic map(
      INIT => X"FB55"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt(0),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt(1),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      O => inst_emif_iface_sdram_dma_std_dma_engine_ed_t_mux00021
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_18_16_SW0 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_row(10),
      O => N626
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_18_16 : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_18_0_1392,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(4),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N36,
      I3 => N626,
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_18_16_1393
    );
  inst_base_module_key_state_and00011 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => inst_emif_iface_cs_r_0_Q,
      I1 => inst_emif_iface_wr_r_1474,
      I2 => N798,
      O => inst_base_module_key_state_and0001
    );
  inst_emif_iface_edo_ce3_or00001 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(31),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N73,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00001_1121
    );
  inst_emif_iface_edo_ce3_or00011 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(30),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N71,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00011_1122
    );
  inst_emif_iface_edo_ce3_or00021 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(29),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N69,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00021_1123
    );
  inst_emif_iface_edo_ce3_or00031 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(28),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N67,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00031_1124
    );
  inst_emif_iface_edo_ce3_or00041 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(27),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N65,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00041_1125
    );
  inst_emif_iface_edo_ce3_or00051 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(26),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N63,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00051_1126
    );
  inst_emif_iface_edo_ce3_or00061 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(25),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N61,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00061_1127
    );
  inst_emif_iface_edo_ce3_or00071 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(24),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N59,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00071_1128
    );
  inst_emif_iface_edo_ce3_or00081 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(23),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N57,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00081_1129
    );
  inst_emif_iface_edo_ce3_or00091 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(22),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N55,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00091_1130
    );
  inst_emif_iface_edo_ce3_or00101 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(21),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N53,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00101_1131
    );
  inst_emif_iface_edo_ce3_or00111 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(20),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N51,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00111_1132
    );
  inst_emif_iface_edo_ce3_or00121 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(19),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N49,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00121_1133
    );
  inst_emif_iface_edo_ce3_or00131 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(18),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N47,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00131_1134
    );
  inst_emif_iface_edo_ce3_or00141 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(17),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N45,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00141_1135
    );
  inst_emif_iface_edo_ce3_or00151 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(16),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N43,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00151_1136
    );
  inst_emif_iface_edo_ce3_or00161 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(15),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N41,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00161_1137
    );
  inst_emif_iface_edo_ce3_or00171 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(14),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N39,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00171_1138
    );
  inst_emif_iface_edo_ce3_or00181 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(13),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N37,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00181_1139
    );
  inst_emif_iface_edo_ce3_or00191 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(12),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N35,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00191_1140
    );
  inst_emif_iface_edo_ce3_or00201 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(11),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N33,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00201_1141
    );
  inst_emif_iface_edo_ce3_or00211 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(10),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N31,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00211_1142
    );
  inst_emif_iface_edo_ce3_or00221 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(9),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N29,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00221_1143
    );
  inst_emif_iface_edo_ce3_or00231 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(8),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N27,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00231_1144
    );
  inst_emif_iface_edo_ce3_or00241 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(7),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N25,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00241_1145
    );
  inst_emif_iface_edo_ce3_or00251 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(6),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N23,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00251_1146
    );
  inst_emif_iface_edo_ce3_or00261 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(5),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N21,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00261_1147
    );
  inst_emif_iface_edo_ce3_or00271 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(4),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N19,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00271_1148
    );
  inst_emif_iface_edo_ce3_or00281 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(3),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N17,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00281_1149
    );
  inst_emif_iface_edo_ce3_or00291 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(2),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N15,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00291_1150
    );
  inst_emif_iface_edo_ce3_or00301 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(1),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N13,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00301_1151
    );
  inst_emif_iface_edo_ce3_or00311 : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_din(0),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r1_1304,
      I2 => N11,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_r2_1305,
      O => inst_emif_iface_edo_ce3_or00311_1152
    );
  inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_20_21_SW0 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_bank(0),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_row(10),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_bank(1),
      O => N628
    );
  mb_altera_core_o_DBus_14_mux000041_SW0 : LUT4
    generic map(
      INIT => X"2A7A"
    )
    port map (
      I0 => inst_emif_iface_addr_r(3),
      I1 => mb_altera_core_o_user_io(14),
      I2 => inst_emif_iface_addr_r(4),
      I3 => mb_altera_core_s_dma_timeout_threshold(14),
      O => N630
    );
  mb_altera_core_o_DBus_30_mux00008_SW0 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => mb_altera_core_Read_Fifo_Data_in(30),
      I1 => inst_emif_iface_addr_r(3),
      I2 => inst_emif_iface_addr_r(4),
      I3 => N241,
      O => N632
    );
  mb_altera_core_o_DBus_13_mux00008_SW0 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => mb_altera_core_Read_Fifo_Data_in(13),
      I1 => inst_emif_iface_addr_r(3),
      I2 => inst_emif_iface_addr_r(4),
      I3 => N241,
      O => N634
    );
  mb_altera_core_o_DBus_14_mux00008_SW0 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => mb_altera_core_Read_Fifo_Data_in(14),
      I1 => inst_emif_iface_addr_r(3),
      I2 => inst_emif_iface_addr_r(4),
      I3 => N241,
      O => N636
    );
  mb_altera_core_o_DBus_1_mux000267 : LUT4
    generic map(
      INIT => X"AA80"
    )
    port map (
      I0 => inst_emif_iface_addr_r(1),
      I1 => mb_altera_core_N41,
      I2 => mb_altera_core_s_altera_rdfifo_rd_cnt(1),
      I3 => N638,
      O => mb_altera_core_o_DBus_1_mux000267_2308
    );
  mb_altera_core_o_DBus_28_mux000032 : LUT4
    generic map(
      INIT => X"0E04"
    )
    port map (
      I0 => inst_emif_iface_addr_r(3),
      I1 => mb_altera_core_s_dma_timeout_threshold(28),
      I2 => N640,
      I3 => mb_altera_core_o_user_io(28),
      O => mb_altera_core_o_DBus_28_mux000032_2360
    );
  mb_altera_core_o_DBus_27_mux000045 : LUT4
    generic map(
      INIT => X"0E04"
    )
    port map (
      I0 => inst_emif_iface_addr_r(3),
      I1 => mb_altera_core_s_dma_timeout_threshold(27),
      I2 => N640,
      I3 => mb_altera_core_o_user_io(27),
      O => mb_altera_core_o_DBus_27_mux000045_2356
    );
  mb_altera_core_o_DBus_24_mux000045 : LUT4
    generic map(
      INIT => X"0E04"
    )
    port map (
      I0 => inst_emif_iface_addr_r(3),
      I1 => mb_altera_core_s_dma_timeout_threshold(24),
      I2 => N640,
      I3 => mb_altera_core_o_user_io(24),
      O => mb_altera_core_o_DBus_24_mux000045_2339
    );
  mb_altera_core_o_DBus_22_mux000045 : LUT4
    generic map(
      INIT => X"0E04"
    )
    port map (
      I0 => inst_emif_iface_addr_r(3),
      I1 => mb_altera_core_s_dma_timeout_threshold(22),
      I2 => N640,
      I3 => mb_altera_core_o_user_io(22),
      O => mb_altera_core_o_DBus_22_mux000045_2328
    );
  mb_altera_core_o_DBus_19_mux000045 : LUT4
    generic map(
      INIT => X"0E04"
    )
    port map (
      I0 => inst_emif_iface_addr_r(3),
      I1 => mb_altera_core_s_dma_timeout_threshold(19),
      I2 => N640,
      I3 => mb_altera_core_o_user_io(19),
      O => mb_altera_core_o_DBus_19_mux000045_2305
    );
  mb_altera_core_o_DBus_18_mux000045 : LUT4
    generic map(
      INIT => X"0E04"
    )
    port map (
      I0 => inst_emif_iface_addr_r(3),
      I1 => mb_altera_core_s_dma_timeout_threshold(18),
      I2 => N640,
      I3 => mb_altera_core_o_user_io(18),
      O => mb_altera_core_o_DBus_18_mux000045_2300
    );
  mb_altera_core_o_DBus_17_mux000045 : LUT4
    generic map(
      INIT => X"0E04"
    )
    port map (
      I0 => inst_emif_iface_addr_r(3),
      I1 => mb_altera_core_s_dma_timeout_threshold(17),
      I2 => N640,
      I3 => mb_altera_core_o_user_io(17),
      O => mb_altera_core_o_DBus_17_mux000045_2295
    );
  mb_altera_core_o_DBus_16_mux000045 : LUT4
    generic map(
      INIT => X"0E04"
    )
    port map (
      I0 => inst_emif_iface_addr_r(3),
      I1 => mb_altera_core_s_dma_timeout_threshold(16),
      I2 => N640,
      I3 => mb_altera_core_o_user_io(16),
      O => mb_altera_core_o_DBus_16_mux000045_2290
    );
  mb_altera_core_o_DBus_3_mux000080 : LUT4
    generic map(
      INIT => X"00AC"
    )
    port map (
      I0 => mb_altera_core_o_user_io(3),
      I1 => mb_altera_core_s_dma_timeout_threshold(3),
      I2 => inst_emif_iface_addr_r(3),
      I3 => N800,
      O => mb_altera_core_o_DBus_3_mux000080_2398
    );
  mb_altera_core_o_DBus_9_mux000232 : LUT4
    generic map(
      INIT => X"0E04"
    )
    port map (
      I0 => inst_emif_iface_addr_r(3),
      I1 => mb_altera_core_s_dma_timeout_threshold(9),
      I2 => N640,
      I3 => mb_altera_core_o_user_io(9),
      O => mb_altera_core_o_DBus_9_mux000232_2445
    );
  mb_altera_core_o_DBus_31_mux000032 : LUT4
    generic map(
      INIT => X"0E04"
    )
    port map (
      I0 => inst_emif_iface_addr_r(3),
      I1 => mb_altera_core_s_dma_timeout_threshold(31),
      I2 => N640,
      I3 => mb_altera_core_o_user_io(31),
      O => mb_altera_core_o_DBus_31_mux000032_2389
    );
  mb_altera_core_o_DBus_26_mux000032 : LUT4
    generic map(
      INIT => X"0E04"
    )
    port map (
      I0 => inst_emif_iface_addr_r(3),
      I1 => mb_altera_core_s_dma_timeout_threshold(26),
      I2 => N640,
      I3 => mb_altera_core_o_user_io(26),
      O => mb_altera_core_o_DBus_26_mux000032_2350
    );
  mb_altera_core_o_DBus_25_mux000032 : LUT4
    generic map(
      INIT => X"0E04"
    )
    port map (
      I0 => inst_emif_iface_addr_r(3),
      I1 => mb_altera_core_s_dma_timeout_threshold(25),
      I2 => N640,
      I3 => mb_altera_core_o_user_io(25),
      O => mb_altera_core_o_DBus_25_mux000032_2344
    );
  mb_altera_core_o_DBus_23_mux000032 : LUT4
    generic map(
      INIT => X"0E04"
    )
    port map (
      I0 => inst_emif_iface_addr_r(3),
      I1 => mb_altera_core_s_dma_timeout_threshold(23),
      I2 => N640,
      I3 => mb_altera_core_o_user_io(23),
      O => mb_altera_core_o_DBus_23_mux000032_2333
    );
  mb_altera_core_o_DBus_21_mux000032 : LUT4
    generic map(
      INIT => X"0E04"
    )
    port map (
      I0 => inst_emif_iface_addr_r(3),
      I1 => mb_altera_core_s_dma_timeout_threshold(21),
      I2 => N640,
      I3 => mb_altera_core_o_user_io(21),
      O => mb_altera_core_o_DBus_21_mux000032_2322
    );
  mb_altera_core_o_DBus_20_mux000032 : LUT4
    generic map(
      INIT => X"0E04"
    )
    port map (
      I0 => inst_emif_iface_addr_r(3),
      I1 => mb_altera_core_s_dma_timeout_threshold(20),
      I2 => N640,
      I3 => mb_altera_core_o_user_io(20),
      O => mb_altera_core_o_DBus_20_mux000032_2316
    );
  mb_altera_core_o_DBus_12_mux000032 : LUT4
    generic map(
      INIT => X"0E04"
    )
    port map (
      I0 => inst_emif_iface_addr_r(3),
      I1 => mb_altera_core_s_dma_timeout_threshold(12),
      I2 => N640,
      I3 => mb_altera_core_o_user_io(12),
      O => mb_altera_core_o_DBus_12_mux000032_2268
    );
  mb_altera_core_o_DBus_11_mux000232 : LUT4
    generic map(
      INIT => X"0E04"
    )
    port map (
      I0 => inst_emif_iface_addr_r(3),
      I1 => mb_altera_core_s_dma_timeout_threshold(11),
      I2 => N640,
      I3 => mb_altera_core_o_user_io(11),
      O => mb_altera_core_o_DBus_11_mux000232_2262
    );
  mb_altera_core_o_DBus_10_mux000232 : LUT4
    generic map(
      INIT => X"0E04"
    )
    port map (
      I0 => inst_emif_iface_addr_r(3),
      I1 => mb_altera_core_s_dma_timeout_threshold(10),
      I2 => N640,
      I3 => mb_altera_core_o_user_io(10),
      O => mb_altera_core_o_DBus_10_mux000232_2256
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_6_431_SW0 : LUT4
    generic map(
      INIT => X"EFFF"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_N9,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_col(4),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_col(5),
      O => N678
    );
  mb_altera_core_o_DBus_15_mux000048 : LUT4
    generic map(
      INIT => X"3222"
    )
    port map (
      I0 => N680,
      I1 => inst_emif_iface_addr_r(1),
      I2 => mb_altera_core_N41,
      I3 => mb_altera_core_s_dma_start_addr(13),
      O => mb_altera_core_o_DBus_15_mux000048_2285
    );
  inst_base_module_irq_mask_mux0000_0_2 : LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => inst_emif_iface_addr_r(0),
      I1 => inst_emif_iface_addr_r(2),
      I2 => inst_emif_iface_addr_r(1),
      I3 => N688,
      O => inst_base_module_N2
    );
  mb_altera_core_Flash_State_FSM_FFd7_In0 : LUT3
    generic map(
      INIT => X"A8"
    )
    port map (
      I0 => mb_altera_core_Flash_State_cmp_eq0003,
      I1 => mb_altera_core_Flash_State_FSM_FFd4_1602,
      I2 => mb_altera_core_Flash_State_FSM_FFd1_1599,
      O => mb_altera_core_Flash_State_FSM_FFd7_In0_1608
    );
  mb_altera_core_o_DBus_3_mux00001501_SW0_SW0 : LUT4
    generic map(
      INIT => X"FEFC"
    )
    port map (
      I0 => mb_altera_core_Read_Fifo_Data_in(3),
      I1 => mb_altera_core_o_DBus_3_mux000080_2398,
      I2 => mb_altera_core_o_DBus_3_mux000069_2397,
      I3 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      O => N692
    );
  mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut_4_Q : LUT4
    generic map(
      INIT => X"6545"
    )
    port map (
      I0 => mb_altera_core_Write_fifo_wr_Cnt(4),
      I1 => mb_altera_core_s_altera_write_busy_2612,
      I2 => mb_altera_core_Write_Fifo_Write_En_2128,
      I3 => N602,
      O => mb_altera_core_Mcount_Write_fifo_wr_Cnt_lut(4)
    );
  mb_altera_core_Write_fifo_wr_Cnt_and0000_inv2 : LUT4
    generic map(
      INIT => X"AEFF"
    )
    port map (
      I0 => mb_altera_core_s_altera_write_busy_2612,
      I1 => mb_altera_core_Write_fifo_wr_Cnt(4),
      I2 => N602,
      I3 => mb_altera_core_Write_Fifo_Write_En_2128,
      O => mb_altera_core_Write_fifo_wr_Cnt_and0000_inv
    );
  mb_altera_core_s_dma_num_samples_mux0000_9_11 : LUT4
    generic map(
      INIT => X"A2AA"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_1302,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_dma_done_1301,
      I3 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      O => mb_altera_core_N38
    );
  inst_base_module_o_DBus_mux0002_0_7_SW0 : LUT4
    generic map(
      INIT => X"8101"
    )
    port map (
      I0 => inst_emif_iface_addr_r(0),
      I1 => inst_emif_iface_addr_r(1),
      I2 => inst_emif_iface_addr_r(2),
      I3 => inst_base_module_wd_timeout(0),
      O => N694
    );
  mb_altera_core_Flash_State_cmp_eq000011 : LUT4
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => inst_emif_iface_addr_r(1),
      I1 => inst_emif_iface_addr_r(2),
      I2 => inst_emif_iface_addr_r(4),
      I3 => inst_emif_iface_addr_r(3),
      O => mb_altera_core_Flash_Write_Reg_0_and0000
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_18_321 : LUT4
    generic map(
      INIT => X"AA08"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(10),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N36,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N431,
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_18_32
    );
  inst_base_module_o_DBus_mux0002_0_25 : LUT4
    generic map(
      INIT => X"0010"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => inst_emif_iface_addr_r(3),
      I2 => inst_emif_iface_addr_r(0),
      I3 => N624,
      O => inst_base_module_o_DBus_mux0002_0_25_828
    );
  inst_base_module_wd_nmi_en_not000111 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => inst_emif_iface_cs_r_0_Q,
      I1 => inst_emif_iface_wr_r_1474,
      I2 => inst_base_module_o_DBus_cmp_eq0006,
      O => inst_base_module_wd_timeout_not0001
    );
  inst_base_module_o_DBus_mux0002_3_9 : LUT4
    generic map(
      INIT => X"CE02"
    )
    port map (
      I0 => N616,
      I1 => inst_emif_iface_addr_r(4),
      I2 => inst_emif_iface_addr_r(3),
      I3 => inst_base_module_varindex0000(3),
      O => inst_base_module_o_DBus_mux0002_3_9_868
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_17_63 : LUT4
    generic map(
      INIT => X"AA80"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(9),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I2 => N700,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N431,
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_17_63_1391
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_10_251 : LUT4
    generic map(
      INIT => X"AA80"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(2),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I2 => N702,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N431,
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_10_25
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_14_421 : LUT4
    generic map(
      INIT => X"AA80"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(6),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I2 => N704,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N431,
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_14_42
    );
  mb_altera_core_s_dma_num_samples_mux0000_1_SW1 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => mb_altera_core_Madd_s_dma_num_samples_addsub0000_cy_6_Q,
      I1 => mb_altera_core_s_altera_rdfifo_rd_en,
      I2 => mb_altera_core_s_dma_num_samples(7),
      I3 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      O => N706
    );
  mb_altera_core_s_dma_num_samples_mux0000_1_Q : LUT4
    generic map(
      INIT => X"083B"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I1 => mb_altera_core_s_dma_num_samples(8),
      I2 => N706,
      I3 => N256,
      O => mb_altera_core_s_dma_num_samples_mux0000(1)
    );
  mb_altera_core_s_dma_num_samples_mux0000_7_SW1 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => mb_altera_core_s_altera_rdfifo_rd_en,
      I1 => mb_altera_core_s_dma_num_samples(0),
      I2 => mb_altera_core_s_dma_num_samples(1),
      I3 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      O => N708
    );
  mb_altera_core_s_dma_num_samples_mux0000_7_Q : LUT4
    generic map(
      INIT => X"083B"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I1 => mb_altera_core_s_dma_num_samples(2),
      I2 => N708,
      I3 => N223,
      O => mb_altera_core_s_dma_num_samples_mux0000(7)
    );
  mb_altera_core_s_dma_num_samples_mux0000_4_SW1 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => mb_altera_core_Madd_s_dma_num_samples_addsub0000_cy_3_Q,
      I1 => mb_altera_core_s_altera_rdfifo_rd_en,
      I2 => mb_altera_core_s_dma_num_samples(4),
      I3 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      O => N710
    );
  mb_altera_core_s_dma_num_samples_mux0000_4_Q : LUT4
    generic map(
      INIT => X"083B"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I1 => mb_altera_core_s_dma_num_samples(5),
      I2 => N710,
      I3 => N226,
      O => mb_altera_core_s_dma_num_samples_mux0000(4)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_10_251_SW0 : LUT2
    generic map(
      INIT => X"7"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(1),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row(0),
      O => N702
    );
  inst_base_module_key_state_cmp_eq000211 : LUT3
    generic map(
      INIT => X"01"
    )
    port map (
      I0 => inst_emif_iface_addr_r(2),
      I1 => inst_emif_iface_addr_r(4),
      I2 => inst_emif_iface_addr_r(3),
      O => inst_base_module_N5
    );
  mb_altera_core_Write_Fifo_Data_in_10_or000022 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => inst_emif_iface_cs_r_8_Q,
      I1 => inst_emif_iface_wr_r_1474,
      I2 => mb_altera_core_Write_Fifo_Data_in_10_or000014,
      I3 => mb_altera_core_Write_Fifo_ff,
      O => mb_altera_core_Write_Fifo_Data_in_10_or0000
    );
  mb_altera_core_Write_Fifo_Write_En_not00011 : LUT4
    generic map(
      INIT => X"FF7F"
    )
    port map (
      I0 => inst_emif_iface_cs_r_8_Q,
      I1 => inst_emif_iface_wr_r_1474,
      I2 => mb_altera_core_Write_Fifo_Data_in_10_or000014,
      I3 => mb_altera_core_Write_Fifo_ff,
      O => mb_altera_core_Write_Fifo_Write_En_not0001
    );
  inst_base_module_o_DBus_mux0002_0_7 : LUT4
    generic map(
      INIT => X"CE02"
    )
    port map (
      I0 => N694,
      I1 => inst_emif_iface_addr_r(4),
      I2 => inst_emif_iface_addr_r(3),
      I3 => inst_base_module_varindex0000(0),
      O => inst_base_module_o_DBus_mux0002_0_7_831
    );
  inst_base_module_Msub_wd_counter_addsub0000_lut_15_INV_0 : INV
    port map (
      I => inst_base_module_wd_counter(15),
      O => inst_base_module_Msub_wd_counter_addsub0000_lut(15)
    );
  inst_base_module_Msub_wd_counter_addsub0000_lut_14_INV_0 : INV
    port map (
      I => inst_base_module_wd_counter(14),
      O => inst_base_module_Msub_wd_counter_addsub0000_lut(14)
    );
  inst_base_module_Msub_wd_counter_addsub0000_lut_13_INV_0 : INV
    port map (
      I => inst_base_module_wd_counter(13),
      O => inst_base_module_Msub_wd_counter_addsub0000_lut(13)
    );
  inst_base_module_Msub_wd_counter_addsub0000_lut_12_INV_0 : INV
    port map (
      I => inst_base_module_wd_counter(12),
      O => inst_base_module_Msub_wd_counter_addsub0000_lut(12)
    );
  inst_base_module_Msub_wd_counter_addsub0000_lut_11_INV_0 : INV
    port map (
      I => inst_base_module_wd_counter(11),
      O => inst_base_module_Msub_wd_counter_addsub0000_lut(11)
    );
  inst_base_module_Msub_wd_counter_addsub0000_lut_10_INV_0 : INV
    port map (
      I => inst_base_module_wd_counter(10),
      O => inst_base_module_Msub_wd_counter_addsub0000_lut(10)
    );
  inst_base_module_Msub_wd_counter_addsub0000_lut_9_INV_0 : INV
    port map (
      I => inst_base_module_wd_counter(9),
      O => inst_base_module_Msub_wd_counter_addsub0000_lut(9)
    );
  inst_base_module_Msub_wd_counter_addsub0000_lut_8_INV_0 : INV
    port map (
      I => inst_base_module_wd_counter(8),
      O => inst_base_module_Msub_wd_counter_addsub0000_lut(8)
    );
  inst_base_module_Msub_wd_counter_addsub0000_lut_7_INV_0 : INV
    port map (
      I => inst_base_module_wd_counter(7),
      O => inst_base_module_Msub_wd_counter_addsub0000_lut(7)
    );
  inst_base_module_Msub_wd_counter_addsub0000_lut_6_INV_0 : INV
    port map (
      I => inst_base_module_wd_counter(6),
      O => inst_base_module_Msub_wd_counter_addsub0000_lut(6)
    );
  inst_base_module_Msub_wd_counter_addsub0000_lut_5_INV_0 : INV
    port map (
      I => inst_base_module_wd_counter(5),
      O => inst_base_module_Msub_wd_counter_addsub0000_lut(5)
    );
  inst_base_module_Msub_wd_counter_addsub0000_lut_4_INV_0 : INV
    port map (
      I => inst_base_module_wd_counter(4),
      O => inst_base_module_Msub_wd_counter_addsub0000_lut(4)
    );
  inst_base_module_Msub_wd_counter_addsub0000_lut_3_INV_0 : INV
    port map (
      I => inst_base_module_wd_counter(3),
      O => inst_base_module_Msub_wd_counter_addsub0000_lut(3)
    );
  inst_base_module_Msub_wd_counter_addsub0000_lut_2_INV_0 : INV
    port map (
      I => inst_base_module_wd_counter(2),
      O => inst_base_module_Msub_wd_counter_addsub0000_lut(2)
    );
  inst_base_module_Msub_wd_counter_addsub0000_lut_1_INV_0 : INV
    port map (
      I => inst_base_module_wd_counter(1),
      O => inst_base_module_Msub_wd_counter_addsub0000_lut(1)
    );
  mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut_30_INV_0 : INV
    port map (
      I => mb_altera_core_Madd_s_dma_start_addr_add0000_cy(29),
      O => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_lut(30)
    );
  mb_altera_core_Madd_add0001_Madd_lut_0_INV_0 : INV
    port map (
      I => mb_altera_core_s_dma_space_addr(0),
      O => mb_altera_core_Madd_add0001_Madd_lut(0)
    );
  mb_altera_core_Mcompar_s_dma_state_cmp_gt0000_lut_3_INV_0 : INV
    port map (
      I => mb_altera_core_s_altera_rdfifo_rd_cnt(12),
      O => mb_altera_core_Mcompar_s_dma_state_cmp_gt0000_lut(3)
    );
  mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_lut_0_INV_0 : INV
    port map (
      I => mb_altera_core_s_dma_timeout_cntr(0),
      O => mb_altera_core_Madd_s_dma_timeout_cntr_addsub0000_lut(0)
    );
  mb_altera_core_Madd_s_dma_start_addr_add0000_lut_9_INV_0 : INV
    port map (
      I => mb_altera_core_s_dma_space_addr(9),
      O => mb_altera_core_Madd_s_dma_start_addr_add0000_lut(9)
    );
  o_eth_rst_n1_INV_0 : INV
    port map (
      I => phy_rst,
      O => o_eth_rst_n_OBUF_3052
    );
  inst_emif_iface_reset_inv1_INV_0 : INV
    port map (
      I => inst_emif_iface_reset,
      O => inst_emif_iface_reset_inv
    );
  wd_rst_inv1_INV_0 : INV
    port map (
      I => inst_base_module_o_wd_rst_878,
      O => wd_rst_inv
    );
  t_fl_bank_sel_n_inv1_INV_0 : INV
    port map (
      I => inst_base_module_bank_addr(0),
      O => t_fl_bank_sel_n_inv
    );
  o_user_led_n1_INV_0 : INV
    port map (
      I => inst_base_module_led_enable(0),
      O => o_user_led_n_OBUF_3069
    );
  altera_flash_enable_inv1_INV_0 : INV
    port map (
      I => mb_altera_core_flash_enable_2235,
      O => altera_flash_enable_inv
    );
  mb_altera_core_version_N011_INV_0 : INV
    port map (
      I => mb_altera_core_version_rd_toggle_3023,
      O => mb_altera_core_version_N01
    );
  inst_base_module_wd_rst_clr_not00031_INV_0 : INV
    port map (
      I => inst_base_module_wd_rst_clr_953,
      O => inst_base_module_wd_rst_clr_not0003
    );
  inst_base_module_wd_nmi_clr_not00031_INV_0 : INV
    port map (
      I => inst_base_module_wd_nmi_clr_940,
      O => inst_base_module_wd_nmi_clr_not0003
    );
  inst_base_module_wd_kick_not00011_INV_0 : INV
    port map (
      I => inst_base_module_wd_kick_935,
      O => inst_base_module_wd_kick_not0001
    );
  mb_altera_core_s_altera_write_busy_not00011_INV_0 : INV
    port map (
      I => mb_altera_core_altera_state_FSM_FFd3_2230,
      O => mb_altera_core_s_altera_write_busy_not0001
    );
  mb_altera_core_i_cs_inv321_INV_0 : INV
    port map (
      I => inst_emif_iface_cs_r_8_Q,
      O => mb_altera_core_i_cs_inv
    );
  inst_base_module_i_cs_inv1_INV_0 : INV
    port map (
      I => inst_emif_iface_cs_r_0_Q,
      O => inst_base_module_i_cs_inv
    );
  inst_emif_iface_sdram_dma_std_dma_engine_hold_n_mux0003111_INV_0 : INV
    port map (
      I => mb_altera_core_s_dma_req_2694,
      O => inst_emif_iface_sdram_dma_std_dma_engine_hold_n_mux000311
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_4_301 : MUXF5
    port map (
      I0 => N714,
      I1 => N715,
      S => inst_emif_iface_sdram_dma_std_dma_engine_N51,
      O => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_4_30
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_4_301_F : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_col(4),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_37,
      O => N714
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_4_301_G : LUT4
    generic map(
      INIT => X"A8AD"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_col(4),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_37,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N9,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      O => N715
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_1_331 : MUXF5
    port map (
      I0 => N716,
      I1 => N717,
      S => inst_emif_iface_sdram_dma_std_dma_engine_N51,
      O => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_1_33
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_1_331_F : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_col(1),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_37,
      O => N716
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_1_331_G : LUT4
    generic map(
      INIT => X"8ADA"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_col(1),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_37,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_col(0),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      O => N717
    );
  mb_altera_core_o_DBus_8_mux000292 : MUXF5
    port map (
      I0 => N718,
      I1 => N719,
      S => inst_emif_iface_addr_r(3),
      O => mb_altera_core_o_DBus_8_mux000292_2441
    );
  mb_altera_core_o_DBus_8_mux000292_F : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => mb_altera_core_N12,
      I2 => mb_altera_core_s_dma_timeout_threshold(8),
      O => N718
    );
  mb_altera_core_o_DBus_8_mux000292_G : LUT4
    generic map(
      INIT => X"AA80"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => mb_altera_core_N12,
      I2 => mb_altera_core_o_user_io(8),
      I3 => mb_altera_core_o_DBus_8_mux000257_2440,
      O => N719
    );
  mb_altera_core_altera_asd_mux0000153 : MUXF5
    port map (
      I0 => N720,
      I1 => N721,
      S => mb_altera_core_Flash_Cnt(1),
      O => mb_altera_core_altera_asd_mux0000153_2213
    );
  mb_altera_core_altera_asd_mux0000153_F : LUT4
    generic map(
      INIT => X"0E04"
    )
    port map (
      I0 => mb_altera_core_Flash_Cnt(2),
      I1 => mb_altera_core_Flash_Write_Reg(7),
      I2 => mb_altera_core_Flash_Cnt(0),
      I3 => mb_altera_core_Flash_Write_Reg(3),
      O => N720
    );
  mb_altera_core_altera_asd_mux0000153_G : LUT4
    generic map(
      INIT => X"0E04"
    )
    port map (
      I0 => mb_altera_core_Flash_Cnt(2),
      I1 => mb_altera_core_Flash_Write_Reg(5),
      I2 => mb_altera_core_Flash_Cnt(0),
      I3 => mb_altera_core_Flash_Write_Reg(1),
      O => N721
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ce_n_mux0002_3_Q : MUXF5
    port map (
      I0 => N722,
      I1 => N723,
      S => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      O => inst_emif_iface_sdram_dma_std_dma_engine_ce_n_mux0002(3)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ce_n_mux0002_3_F : LUT4
    generic map(
      INIT => X"FEFF"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt(0),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt(1),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      O => N722
    );
  inst_emif_iface_sdram_dma_std_dma_engine_ce_n_mux0002_3_G : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N5,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(4),
      O => N723
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_7_611 : MUXF5
    port map (
      I0 => N724,
      I1 => N725,
      S => inst_emif_iface_sdram_dma_std_dma_engine_col(7),
      O => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_7_61
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_7_611_F : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_N521,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_col(4),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_col(5),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_col(6),
      O => N724
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_7_611_G : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_37,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N51,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_7_14_1258,
      O => N725
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_9_361 : MUXF5
    port map (
      I0 => N726,
      I1 => N727,
      S => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_9_36
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_9_361_F : LUT3
    generic map(
      INIT => X"A8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(1),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      O => N726
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_9_361_G : LUT3
    generic map(
      INIT => X"A2"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(1),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row(0),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N3,
      O => N727
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_16_471 : MUXF5
    port map (
      I0 => N728,
      I1 => N729,
      S => inst_emif_iface_sdram_dma_std_dma_engine_row(8),
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_16_47
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_16_471_F : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => N543,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N47,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_row(1),
      I3 => N290,
      O => N728
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_16_471_G : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_N431,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N52,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_16_21_1386,
      O => N729
    );
  mb_altera_core_s_dma_num_samples_mux0000_3_Q : MUXF5
    port map (
      I0 => N730,
      I1 => N731,
      S => mb_altera_core_s_dma_num_samples(6),
      O => mb_altera_core_s_dma_num_samples_mux0000(3)
    );
  mb_altera_core_s_dma_num_samples_mux0000_3_F : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => mb_altera_core_N8,
      I1 => mb_altera_core_Madd_s_dma_num_samples_addsub0000_cy_3_Q,
      I2 => mb_altera_core_s_dma_num_samples(5),
      I3 => mb_altera_core_s_dma_num_samples(4),
      O => N730
    );
  mb_altera_core_s_dma_num_samples_mux0000_3_G : LUT4
    generic map(
      INIT => X"AEFF"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_cmp_eq0003,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_altera_rdfifo_rd_en,
      I3 => N237,
      O => N731
    );
  mb_altera_core_s_dma_num_samples_mux0000_6_Q : MUXF5
    port map (
      I0 => N732,
      I1 => N733,
      S => mb_altera_core_s_dma_num_samples(3),
      O => mb_altera_core_s_dma_num_samples_mux0000(6)
    );
  mb_altera_core_s_dma_num_samples_mux0000_6_F : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => mb_altera_core_N8,
      I1 => mb_altera_core_s_dma_num_samples(2),
      I2 => mb_altera_core_s_dma_num_samples(1),
      I3 => mb_altera_core_s_dma_num_samples(0),
      O => N732
    );
  mb_altera_core_s_dma_num_samples_mux0000_6_G : LUT4
    generic map(
      INIT => X"AEFF"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_cmp_eq0003,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_altera_rdfifo_rd_en,
      I3 => N234,
      O => N733
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_5_311 : MUXF5
    port map (
      I0 => N734,
      I1 => N735,
      S => inst_emif_iface_sdram_dma_std_dma_engine_col(5),
      O => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_5_31
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_5_311_F : LUT4
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_N51,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_col(4),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N9,
      O => N734
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_5_311_G : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_37,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N51,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_5_3_1253,
      O => N735
    );
  mb_altera_core_s_dma_num_samples_mux0000_0_Q : MUXF5
    port map (
      I0 => N736,
      I1 => N737,
      S => mb_altera_core_s_dma_num_samples(9),
      O => mb_altera_core_s_dma_num_samples_mux0000(0)
    );
  mb_altera_core_s_dma_num_samples_mux0000_0_F : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => mb_altera_core_N8,
      I1 => mb_altera_core_s_dma_num_samples(8),
      I2 => mb_altera_core_s_dma_num_samples(7),
      I3 => mb_altera_core_Madd_s_dma_num_samples_addsub0000_cy_6_Q,
      O => N736
    );
  mb_altera_core_s_dma_num_samples_mux0000_0_G : LUT4
    generic map(
      INIT => X"AEFF"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_cmp_eq0003,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_altera_rdfifo_rd_en,
      I3 => N260,
      O => N737
    );
  mb_altera_core_o_DBus_2_mux000295 : MUXF5
    port map (
      I0 => N738,
      I1 => N739,
      S => inst_emif_iface_addr_r(2),
      O => mb_altera_core_o_DBus_2_mux000295_2377
    );
  mb_altera_core_o_DBus_2_mux000295_F : LUT4
    generic map(
      INIT => X"FF08"
    )
    port map (
      I0 => mb_altera_core_N42,
      I1 => mb_altera_core_Flash_Read_Reg(2),
      I2 => inst_emif_iface_addr_r(3),
      I3 => mb_altera_core_o_DBus_2_mux000267_2376,
      O => N738
    );
  mb_altera_core_o_DBus_2_mux000295_G : LUT4
    generic map(
      INIT => X"FF08"
    )
    port map (
      I0 => mb_altera_core_s_altera_int_en_2553,
      I1 => mb_altera_core_N13,
      I2 => inst_emif_iface_addr_r(1),
      I3 => mb_altera_core_o_DBus_2_mux000267_2376,
      O => N739
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_8_Q : MUXF5
    port map (
      I0 => N740,
      I1 => N741,
      S => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001(8)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_8_F : LUT4
    generic map(
      INIT => X"ABA8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(0),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(8),
      O => N740
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_8_G : LUT4
    generic map(
      INIT => X"6AAA"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(0),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(4),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      O => N741
    );
  inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_19_381 : MUXF5
    port map (
      I0 => N742,
      I1 => N743,
      S => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      O => inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_19_38
    );
  inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_19_381_F : LUT3
    generic map(
      INIT => X"A8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_bank(0),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      O => N742
    );
  inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_19_381_G : LUT4
    generic map(
      INIT => X"A2AA"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_bank(0),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row(10),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N3,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N36,
      O => N743
    );
  mb_altera_core_Write_fifo_wr_Cnt_not000140 : MUXF5
    port map (
      I0 => N744,
      I1 => N745,
      S => mb_altera_core_s_altera_write_busy_2612,
      O => mb_altera_core_Write_fifo_wr_Cnt_not0001
    );
  mb_altera_core_Write_fifo_wr_Cnt_not000140_F : LUT3
    generic map(
      INIT => X"A2"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_Write_En_2128,
      I1 => mb_altera_core_Write_fifo_wr_Cnt(4),
      I2 => N602,
      O => N744
    );
  mb_altera_core_Write_fifo_wr_Cnt_not000140_G : LUT3
    generic map(
      INIT => X"54"
    )
    port map (
      I0 => mb_altera_core_Write_Fifo_Write_En_2128,
      I1 => mb_altera_core_Write_fifo_wr_Cnt(8),
      I2 => N604,
      O => N745
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_In : MUXF5
    port map (
      I0 => N746,
      I1 => N747,
      S => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      O => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_In_1423
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_In_F : LUT3
    generic map(
      INIT => X"2A"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(4),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N0,
      O => N746
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_In_G : LUT4
    generic map(
      INIT => X"FAF8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_arb_FSM_FFd2_1419,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_arb_FSM_FFd3_1420,
      O => N747
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_6_431 : MUXF5
    port map (
      I0 => N748,
      I1 => N749,
      S => inst_emif_iface_sdram_dma_std_dma_engine_col(6),
      O => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_6_43
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_6_431_F : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(4),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N0,
      I3 => N678,
      O => N748
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_6_431_G : LUT4
    generic map(
      INIT => X"EAAA"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_37,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(4),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N0,
      I3 => N610,
      O => N749
    );
  mb_altera_core_o_DBus_14_mux000048 : MUXF5
    port map (
      I0 => N750,
      I1 => N751,
      S => inst_emif_iface_addr_r(2),
      O => mb_altera_core_o_DBus_14_mux000048_2279
    );
  mb_altera_core_o_DBus_14_mux000048_F : LUT3
    generic map(
      INIT => X"04"
    )
    port map (
      I0 => inst_emif_iface_addr_r(1),
      I1 => inst_emif_iface_addr_r(0),
      I2 => N630,
      O => N750
    );
  mb_altera_core_o_DBus_14_mux000048_G : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => inst_emif_iface_addr_r(3),
      I1 => mb_altera_core_N42,
      I2 => mb_altera_core_s_dma_space_mask(14),
      O => N751
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_5_Q : MUXF5
    port map (
      I0 => N752,
      I1 => N753,
      S => inst_emif_iface_sdram_dma_std_dma_engine_word_count(3),
      O => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000(5)
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_5_F : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_N16,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(4),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(1),
      I3 => N545,
      O => N752
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_5_G : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_N421,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N30,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      O => N753
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_mux0002 : MUXF5
    port map (
      I0 => N754,
      I1 => N755,
      S => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      O => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_mux0002_1303
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_mux0002_F : LUT4
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt(0),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt(1),
      O => N754
    );
  inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_mux0002_G : LUT3
    generic map(
      INIT => X"04"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_col_and0000,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      O => N755
    );
  mb_altera_core_o_DBus_13_mux000014 : MUXF5
    port map (
      I0 => N756,
      I1 => N757,
      S => inst_emif_iface_addr_r(1),
      O => mb_altera_core_o_DBus_13_mux000014_2272
    );
  mb_altera_core_o_DBus_13_mux000014_F : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => N634,
      I1 => mb_altera_core_N41,
      I2 => mb_altera_core_s_dma_start_addr(11),
      O => N756
    );
  mb_altera_core_o_DBus_13_mux000014_G : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => inst_emif_iface_addr_r(2),
      I1 => mb_altera_core_N13,
      I2 => mb_altera_core_s_dma_base_addr(11),
      O => N757
    );
  mb_altera_core_o_DBus_14_mux000014 : MUXF5
    port map (
      I0 => N758,
      I1 => N759,
      S => inst_emif_iface_addr_r(1),
      O => mb_altera_core_o_DBus_14_mux000014_2278
    );
  mb_altera_core_o_DBus_14_mux000014_F : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => N636,
      I1 => mb_altera_core_N41,
      I2 => mb_altera_core_s_dma_start_addr(12),
      O => N758
    );
  mb_altera_core_o_DBus_14_mux000014_G : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => inst_emif_iface_addr_r(2),
      I1 => mb_altera_core_N13,
      I2 => mb_altera_core_s_dma_base_addr(12),
      O => N759
    );
  inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_20_21 : MUXF5
    port map (
      I0 => N760,
      I1 => N761,
      S => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      O => inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_20_21_1221
    );
  inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_20_21_F : LUT3
    generic map(
      INIT => X"04"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(20),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      O => N760
    );
  inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_20_21_G : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => N628,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N36,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(4),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      O => N761
    );
  inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_19_17 : MUXF5
    port map (
      I0 => N762,
      I1 => N763,
      S => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      O => inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_19_17_1218
    );
  inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_19_17_F : LUT3
    generic map(
      INIT => X"04"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(19),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      O => N762
    );
  inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_19_17_G : LUT4
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_N36,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row(10),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_bank(0),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N3,
      O => N763
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_9_14 : MUXF5
    port map (
      I0 => N764,
      I1 => N765,
      S => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_9_14_1396
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_9_14_F : LUT3
    generic map(
      INIT => X"04"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sdram_addr(9),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      O => N764
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_9_14_G : LUT3
    generic map(
      INIT => X"04"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(1),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row(0),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N3,
      O => N765
    );
  mb_altera_core_altera_asd_mux000096 : MUXF5
    port map (
      I0 => N766,
      I1 => N767,
      S => mb_altera_core_Flash_Cnt(1),
      O => mb_altera_core_altera_asd_mux000096_2220
    );
  mb_altera_core_altera_asd_mux000096_F : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_Flash_Cnt(0),
      I1 => mb_altera_core_Flash_Cnt(2),
      I2 => mb_altera_core_Flash_Write_Reg(6),
      I3 => mb_altera_core_Flash_Write_Reg(2),
      O => N766
    );
  mb_altera_core_altera_asd_mux000096_G : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_Flash_Cnt(0),
      I1 => mb_altera_core_Flash_Cnt(2),
      I2 => mb_altera_core_Flash_Write_Reg(4),
      I3 => mb_altera_core_Flash_Write_Reg(0),
      O => N767
    );
  io_ed_31_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(31),
      T => io_ed_o_ed_not0000_inv,
      O => N370,
      IO => io_ed(31)
    );
  inst_emif_iface_edo_ce3_31 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00001_1121,
      S => mb_altera_core_o_DBus(31),
      Q => inst_emif_iface_edo_ce3(31)
    );
  io_ed_30_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(30),
      T => io_ed_o_ed_not0000_inv,
      O => N371,
      IO => io_ed(30)
    );
  inst_emif_iface_edo_ce3_30 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00011_1122,
      S => mb_altera_core_o_DBus(30),
      Q => inst_emif_iface_edo_ce3(30)
    );
  io_ed_29_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(29),
      T => io_ed_o_ed_not0000_inv,
      O => N372,
      IO => io_ed(29)
    );
  inst_emif_iface_edo_ce3_29 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00021_1123,
      S => mb_altera_core_o_DBus(29),
      Q => inst_emif_iface_edo_ce3(29)
    );
  io_ed_28_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(28),
      T => io_ed_o_ed_not0000_inv,
      O => N373,
      IO => io_ed(28)
    );
  inst_emif_iface_edo_ce3_28 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00031_1124,
      S => mb_altera_core_o_DBus(28),
      Q => inst_emif_iface_edo_ce3(28)
    );
  io_ed_27_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(27),
      T => io_ed_o_ed_not0000_inv,
      O => N374,
      IO => io_ed(27)
    );
  inst_emif_iface_edo_ce3_27 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00041_1125,
      S => mb_altera_core_o_DBus(27),
      Q => inst_emif_iface_edo_ce3(27)
    );
  io_ed_26_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(26),
      T => io_ed_o_ed_not0000_inv,
      O => N375,
      IO => io_ed(26)
    );
  inst_emif_iface_edo_ce3_26 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00051_1126,
      S => mb_altera_core_o_DBus(26),
      Q => inst_emif_iface_edo_ce3(26)
    );
  io_ed_25_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(25),
      T => io_ed_o_ed_not0000_inv,
      O => N376,
      IO => io_ed(25)
    );
  inst_emif_iface_edo_ce3_25 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00061_1127,
      S => mb_altera_core_o_DBus(25),
      Q => inst_emif_iface_edo_ce3(25)
    );
  io_ed_24_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(24),
      T => io_ed_o_ed_not0000_inv,
      O => N377,
      IO => io_ed(24)
    );
  inst_emif_iface_edo_ce3_24 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00071_1128,
      S => mb_altera_core_o_DBus(24),
      Q => inst_emif_iface_edo_ce3(24)
    );
  io_ed_23_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(23),
      T => io_ed_o_ed_not0000_inv,
      O => N378,
      IO => io_ed(23)
    );
  inst_emif_iface_edo_ce3_23 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00081_1129,
      S => mb_altera_core_o_DBus(23),
      Q => inst_emif_iface_edo_ce3(23)
    );
  io_ed_22_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(22),
      T => io_ed_o_ed_not0000_inv,
      O => N379,
      IO => io_ed(22)
    );
  inst_emif_iface_edo_ce3_22 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00091_1130,
      S => mb_altera_core_o_DBus(22),
      Q => inst_emif_iface_edo_ce3(22)
    );
  io_ed_21_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(21),
      T => io_ed_o_ed_not0000_inv,
      O => N380,
      IO => io_ed(21)
    );
  inst_emif_iface_edo_ce3_21 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00101_1131,
      S => mb_altera_core_o_DBus(21),
      Q => inst_emif_iface_edo_ce3(21)
    );
  io_ed_20_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(20),
      T => io_ed_o_ed_not0000_inv,
      O => N381,
      IO => io_ed(20)
    );
  inst_emif_iface_edo_ce3_20 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00111_1132,
      S => mb_altera_core_o_DBus(20),
      Q => inst_emif_iface_edo_ce3(20)
    );
  io_ed_19_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(19),
      T => io_ed_o_ed_not0000_inv,
      O => N382,
      IO => io_ed(19)
    );
  inst_emif_iface_edo_ce3_19 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00121_1133,
      S => mb_altera_core_o_DBus(19),
      Q => inst_emif_iface_edo_ce3(19)
    );
  io_ed_18_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(18),
      T => io_ed_o_ed_not0000_inv,
      O => N383,
      IO => io_ed(18)
    );
  inst_emif_iface_edo_ce3_18 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00131_1134,
      S => mb_altera_core_o_DBus(18),
      Q => inst_emif_iface_edo_ce3(18)
    );
  io_ed_17_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(17),
      T => io_ed_o_ed_not0000_inv,
      O => N384,
      IO => io_ed(17)
    );
  inst_emif_iface_edo_ce3_17 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00141_1135,
      S => mb_altera_core_o_DBus(17),
      Q => inst_emif_iface_edo_ce3(17)
    );
  io_ed_16_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(16),
      T => io_ed_o_ed_not0000_inv,
      O => N385,
      IO => io_ed(16)
    );
  inst_emif_iface_edo_ce3_16 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00151_1136,
      S => mb_altera_core_o_DBus(16),
      Q => inst_emif_iface_edo_ce3(16)
    );
  io_ed_15_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(15),
      T => io_ed_o_ed_not0000_inv,
      O => N386,
      IO => io_ed(15)
    );
  inst_emif_iface_edo_ce3_15 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00161_1137,
      S => mb_altera_core_o_DBus(15),
      Q => inst_emif_iface_edo_ce3(15)
    );
  io_ed_14_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(14),
      T => io_ed_o_ed_not0000_inv,
      O => N387,
      IO => io_ed(14)
    );
  inst_emif_iface_edo_ce3_14 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00171_1138,
      S => mb_altera_core_o_DBus(14),
      Q => inst_emif_iface_edo_ce3(14)
    );
  io_ed_13_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(13),
      T => io_ed_o_ed_not0000_inv,
      O => N388,
      IO => io_ed(13)
    );
  inst_emif_iface_edo_ce3_13 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00181_1139,
      S => mb_altera_core_o_DBus(13),
      Q => inst_emif_iface_edo_ce3(13)
    );
  io_ed_12_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(12),
      T => io_ed_o_ed_not0000_inv,
      O => N389,
      IO => io_ed(12)
    );
  inst_emif_iface_edo_ce3_12 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00191_1140,
      S => mb_altera_core_o_DBus(12),
      Q => inst_emif_iface_edo_ce3(12)
    );
  io_ed_11_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(11),
      T => io_ed_o_ed_not0000_inv,
      O => N390,
      IO => io_ed(11)
    );
  inst_emif_iface_edo_ce3_11 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00201_1141,
      S => mb_altera_core_o_DBus(11),
      Q => inst_emif_iface_edo_ce3(11)
    );
  io_ed_10_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(10),
      T => io_ed_o_ed_not0000_inv,
      O => N391,
      IO => io_ed(10)
    );
  inst_emif_iface_edo_ce3_10 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00211_1142,
      S => mb_altera_core_o_DBus(10),
      Q => inst_emif_iface_edo_ce3(10)
    );
  io_ed_9_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(9),
      T => io_ed_o_ed_not0000_inv,
      O => N392,
      IO => io_ed(9)
    );
  inst_emif_iface_edo_ce3_9 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00221_1143,
      S => mb_altera_core_o_DBus(9),
      Q => inst_emif_iface_edo_ce3(9)
    );
  io_ed_8_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(8),
      T => io_ed_o_ed_not0000_inv,
      O => N393,
      IO => io_ed(8)
    );
  inst_emif_iface_edo_ce3_8 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00231_1144,
      S => mb_altera_core_o_DBus(8),
      Q => inst_emif_iface_edo_ce3(8)
    );
  io_ed_7_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(7),
      T => io_ed_o_ed_not0000_inv,
      O => N394,
      IO => io_ed(7)
    );
  inst_emif_iface_edo_ce3_7 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00241_1145,
      S => mb_altera_core_o_DBus(7),
      Q => inst_emif_iface_edo_ce3(7)
    );
  io_ed_6_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(6),
      T => io_ed_o_ed_not0000_inv,
      O => N395,
      IO => io_ed(6)
    );
  inst_emif_iface_edo_ce3_6 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00251_1146,
      S => mb_altera_core_o_DBus(6),
      Q => inst_emif_iface_edo_ce3(6)
    );
  io_ed_5_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(5),
      T => io_ed_o_ed_not0000_inv,
      O => N396,
      IO => io_ed(5)
    );
  inst_emif_iface_edo_ce3_5 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00261_1147,
      S => mb_altera_core_o_DBus(5),
      Q => inst_emif_iface_edo_ce3(5)
    );
  io_ed_4_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(4),
      T => io_ed_o_ed_not0000_inv,
      O => N397,
      IO => io_ed(4)
    );
  inst_emif_iface_edo_ce3_4 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00271_1148,
      S => mb_altera_core_o_DBus(4),
      Q => inst_emif_iface_edo_ce3(4)
    );
  io_ed_3_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(3),
      T => io_ed_o_ed_not0000_inv,
      O => N398,
      IO => io_ed(3)
    );
  inst_emif_iface_edo_ce3_3 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00281_1149,
      S => mb_altera_core_o_DBus(3),
      Q => inst_emif_iface_edo_ce3(3)
    );
  io_ed_2_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(2),
      T => io_ed_o_ed_not0000_inv,
      O => N399,
      IO => io_ed(2)
    );
  inst_emif_iface_edo_ce3_2 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00291_1150,
      S => mb_altera_core_o_DBus(2),
      Q => inst_emif_iface_edo_ce3(2)
    );
  io_ed_1_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(1),
      T => io_ed_o_ed_not0000_inv,
      O => N400,
      IO => io_ed(1)
    );
  inst_emif_iface_edo_ce3_1 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00301_1151,
      S => mb_altera_core_o_DBus(1),
      Q => inst_emif_iface_edo_ce3(1)
    );
  io_ed_0_IOBUF : IOBUF
    port map (
      I => inst_emif_iface_edo_ce3(0),
      T => io_ed_o_ed_not0000_inv,
      O => N401,
      IO => io_ed(0)
    );
  inst_emif_iface_edo_ce3_0 : FDS
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_edo_ce3_or00311_1152,
      S => mb_altera_core_o_DBus(0),
      Q => inst_emif_iface_edo_ce3(0)
    );
  inst_base_module_o_irq_output_4_5_and00001 : LUT4
    generic map(
      INIT => X"AA80"
    )
    port map (
      I0 => inst_base_module_irq_enable(5),
      I1 => inst_base_module_irq_mask(8),
      I2 => irq_map_5_0_Q,
      I3 => inst_base_module_irq_mask(11),
      O => inst_base_module_o_irq_output_4_5_and0000
    );
  inst_base_module_o_irq_output_4_5_and00002 : LUT3
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => inst_base_module_irq_enable(5),
      I1 => inst_base_module_irq_mask(8),
      I2 => irq_map_5_0_Q,
      O => inst_base_module_o_irq_output_4_5_and00001_877
    );
  inst_base_module_o_irq_output_4_5_and0000_f5 : MUXF5
    port map (
      I0 => inst_base_module_o_irq_output_4_5_and00001_877,
      I1 => inst_base_module_o_irq_output_4_5_and0000,
      S => mb_altera_core_s_dma_irq_2663,
      O => o_dsp_ext_int5_OBUF_3041
    );
  mb_altera_core_s_dma_count_mux0003_0_21 : LUT4
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_cmp_ge0000,
      I1 => mb_altera_core_s_dma_en_2662,
      I2 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I3 => mb_altera_core_flash_enable_2235,
      O => mb_altera_core_s_dma_count_mux0003_0_2
    );
  mb_altera_core_s_dma_count_mux0003_0_2_f5 : MUXF5
    port map (
      I0 => ea(15),
      I1 => mb_altera_core_s_dma_count_mux0003_0_2,
      S => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      O => mb_altera_core_N7
    );
  mb_altera_core_Flash_Write_Reg_7_mux00001 : LUT4
    generic map(
      INIT => X"E4FF"
    )
    port map (
      I0 => inst_emif_iface_addr_r(0),
      I1 => inst_emif_iface_edi_r(7),
      I2 => inst_emif_iface_edi_r(0),
      I3 => mb_altera_core_Flash_Write_Reg_0_and0000,
      O => mb_altera_core_Flash_Write_Reg_7_mux00001_1645
    );
  mb_altera_core_Flash_Write_Reg_7_mux00002 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_Flash_Write_Reg_0_and0000,
      I1 => inst_emif_iface_addr_r(0),
      I2 => inst_emif_iface_edi_r(7),
      I3 => inst_emif_iface_edi_r(0),
      O => mb_altera_core_Flash_Write_Reg_7_mux00002_1646
    );
  mb_altera_core_Flash_Write_Reg_7_mux0000_f5 : MUXF5
    port map (
      I0 => mb_altera_core_Flash_Write_Reg_7_mux00002_1646,
      I1 => mb_altera_core_Flash_Write_Reg_7_mux00001_1645,
      S => mb_altera_core_Flash_Write_Reg(7),
      O => mb_altera_core_Flash_Write_Reg_7_mux0000
    );
  mb_altera_core_Flash_Write_Reg_6_mux00001 : LUT4
    generic map(
      INIT => X"E4FF"
    )
    port map (
      I0 => inst_emif_iface_addr_r(0),
      I1 => inst_emif_iface_edi_r(6),
      I2 => inst_emif_iface_edi_r(1),
      I3 => mb_altera_core_Flash_Write_Reg_0_and0000,
      O => mb_altera_core_Flash_Write_Reg_6_mux00001_1641
    );
  mb_altera_core_Flash_Write_Reg_6_mux00002 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_Flash_Write_Reg_0_and0000,
      I1 => inst_emif_iface_addr_r(0),
      I2 => inst_emif_iface_edi_r(6),
      I3 => inst_emif_iface_edi_r(1),
      O => mb_altera_core_Flash_Write_Reg_6_mux00002_1642
    );
  mb_altera_core_Flash_Write_Reg_6_mux0000_f5 : MUXF5
    port map (
      I0 => mb_altera_core_Flash_Write_Reg_6_mux00002_1642,
      I1 => mb_altera_core_Flash_Write_Reg_6_mux00001_1641,
      S => mb_altera_core_Flash_Write_Reg(6),
      O => mb_altera_core_Flash_Write_Reg_6_mux0000
    );
  mb_altera_core_Flash_Write_Reg_5_mux00001 : LUT4
    generic map(
      INIT => X"E4FF"
    )
    port map (
      I0 => inst_emif_iface_addr_r(0),
      I1 => inst_emif_iface_edi_r(5),
      I2 => inst_emif_iface_edi_r(2),
      I3 => mb_altera_core_Flash_Write_Reg_0_and0000,
      O => mb_altera_core_Flash_Write_Reg_5_mux00001_1637
    );
  mb_altera_core_Flash_Write_Reg_5_mux00002 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_Flash_Write_Reg_0_and0000,
      I1 => inst_emif_iface_addr_r(0),
      I2 => inst_emif_iface_edi_r(5),
      I3 => inst_emif_iface_edi_r(2),
      O => mb_altera_core_Flash_Write_Reg_5_mux00002_1638
    );
  mb_altera_core_Flash_Write_Reg_5_mux0000_f5 : MUXF5
    port map (
      I0 => mb_altera_core_Flash_Write_Reg_5_mux00002_1638,
      I1 => mb_altera_core_Flash_Write_Reg_5_mux00001_1637,
      S => mb_altera_core_Flash_Write_Reg(5),
      O => mb_altera_core_Flash_Write_Reg_5_mux0000
    );
  mb_altera_core_Flash_Write_Reg_4_mux00001 : LUT4
    generic map(
      INIT => X"E4FF"
    )
    port map (
      I0 => inst_emif_iface_addr_r(0),
      I1 => inst_emif_iface_edi_r(4),
      I2 => inst_emif_iface_edi_r(3),
      I3 => mb_altera_core_Flash_Write_Reg_0_and0000,
      O => mb_altera_core_Flash_Write_Reg_4_mux00001_1633
    );
  mb_altera_core_Flash_Write_Reg_4_mux00002 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_Flash_Write_Reg_0_and0000,
      I1 => inst_emif_iface_addr_r(0),
      I2 => inst_emif_iface_edi_r(4),
      I3 => inst_emif_iface_edi_r(3),
      O => mb_altera_core_Flash_Write_Reg_4_mux00002_1634
    );
  mb_altera_core_Flash_Write_Reg_4_mux0000_f5 : MUXF5
    port map (
      I0 => mb_altera_core_Flash_Write_Reg_4_mux00002_1634,
      I1 => mb_altera_core_Flash_Write_Reg_4_mux00001_1633,
      S => mb_altera_core_Flash_Write_Reg(4),
      O => mb_altera_core_Flash_Write_Reg_4_mux0000
    );
  mb_altera_core_Flash_Write_Reg_3_mux00001 : LUT4
    generic map(
      INIT => X"E4FF"
    )
    port map (
      I0 => inst_emif_iface_addr_r(0),
      I1 => inst_emif_iface_edi_r(3),
      I2 => inst_emif_iface_edi_r(4),
      I3 => mb_altera_core_Flash_Write_Reg_0_and0000,
      O => mb_altera_core_Flash_Write_Reg_3_mux00001_1629
    );
  mb_altera_core_Flash_Write_Reg_3_mux00002 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_Flash_Write_Reg_0_and0000,
      I1 => inst_emif_iface_addr_r(0),
      I2 => inst_emif_iface_edi_r(3),
      I3 => inst_emif_iface_edi_r(4),
      O => mb_altera_core_Flash_Write_Reg_3_mux00002_1630
    );
  mb_altera_core_Flash_Write_Reg_3_mux0000_f5 : MUXF5
    port map (
      I0 => mb_altera_core_Flash_Write_Reg_3_mux00002_1630,
      I1 => mb_altera_core_Flash_Write_Reg_3_mux00001_1629,
      S => mb_altera_core_Flash_Write_Reg(3),
      O => mb_altera_core_Flash_Write_Reg_3_mux0000
    );
  mb_altera_core_Flash_Write_Reg_2_mux00001 : LUT4
    generic map(
      INIT => X"E4FF"
    )
    port map (
      I0 => inst_emif_iface_addr_r(0),
      I1 => inst_emif_iface_edi_r(2),
      I2 => inst_emif_iface_edi_r(5),
      I3 => mb_altera_core_Flash_Write_Reg_0_and0000,
      O => mb_altera_core_Flash_Write_Reg_2_mux00001_1625
    );
  mb_altera_core_Flash_Write_Reg_2_mux00002 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_Flash_Write_Reg_0_and0000,
      I1 => inst_emif_iface_addr_r(0),
      I2 => inst_emif_iface_edi_r(2),
      I3 => inst_emif_iface_edi_r(5),
      O => mb_altera_core_Flash_Write_Reg_2_mux00002_1626
    );
  mb_altera_core_Flash_Write_Reg_2_mux0000_f5 : MUXF5
    port map (
      I0 => mb_altera_core_Flash_Write_Reg_2_mux00002_1626,
      I1 => mb_altera_core_Flash_Write_Reg_2_mux00001_1625,
      S => mb_altera_core_Flash_Write_Reg(2),
      O => mb_altera_core_Flash_Write_Reg_2_mux0000
    );
  mb_altera_core_Flash_Write_Reg_1_mux00001 : LUT4
    generic map(
      INIT => X"E4FF"
    )
    port map (
      I0 => inst_emif_iface_addr_r(0),
      I1 => inst_emif_iface_edi_r(1),
      I2 => inst_emif_iface_edi_r(6),
      I3 => mb_altera_core_Flash_Write_Reg_0_and0000,
      O => mb_altera_core_Flash_Write_Reg_1_mux00001_1621
    );
  mb_altera_core_Flash_Write_Reg_1_mux00002 : LUT4
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_Flash_Write_Reg_0_and0000,
      I1 => inst_emif_iface_addr_r(0),
      I2 => inst_emif_iface_edi_r(1),
      I3 => inst_emif_iface_edi_r(6),
      O => mb_altera_core_Flash_Write_Reg_1_mux00002_1622
    );
  mb_altera_core_Flash_Write_Reg_1_mux0000_f5 : MUXF5
    port map (
      I0 => mb_altera_core_Flash_Write_Reg_1_mux00002_1622,
      I1 => mb_altera_core_Flash_Write_Reg_1_mux00001_1621,
      S => mb_altera_core_Flash_Write_Reg(1),
      O => mb_altera_core_Flash_Write_Reg_1_mux0000
    );
  mb_altera_core_Flash_Write_Reg_0_mux00001 : LUT3
    generic map(
      INIT => X"E4"
    )
    port map (
      I0 => inst_emif_iface_addr_r(0),
      I1 => inst_emif_iface_edi_r(0),
      I2 => inst_emif_iface_edi_r(7),
      O => mb_altera_core_Flash_Write_Reg_0_mux00001_1618
    );
  mb_altera_core_Flash_Write_Reg_0_mux0000_f5 : MUXF5
    port map (
      I0 => mb_altera_core_Flash_Write_Reg(0),
      I1 => mb_altera_core_Flash_Write_Reg_0_mux00001_1618,
      S => mb_altera_core_Flash_Write_Reg_0_and0000,
      O => mb_altera_core_Flash_Write_Reg_0_mux0000
    );
  mb_altera_core_Flash_Cnt_mux0000_0_1 : LUT4
    generic map(
      INIT => X"2888"
    )
    port map (
      I0 => mb_altera_core_Flash_Cnt_or0000,
      I1 => mb_altera_core_Flash_Cnt(2),
      I2 => mb_altera_core_Flash_Cnt(1),
      I3 => mb_altera_core_Flash_Cnt(0),
      O => mb_altera_core_Flash_Cnt_mux0000_0_1_1578
    );
  mb_altera_core_Flash_Cnt_mux0000_0_2 : LUT4
    generic map(
      INIT => X"6AAA"
    )
    port map (
      I0 => mb_altera_core_Flash_Cnt(2),
      I1 => mb_altera_core_Flash_Cnt_or0000,
      I2 => mb_altera_core_Flash_Cnt(1),
      I3 => mb_altera_core_Flash_Cnt(0),
      O => mb_altera_core_Flash_Cnt_mux0000_0_2_1579
    );
  mb_altera_core_Flash_Cnt_mux0000_0_f5 : MUXF5
    port map (
      I0 => mb_altera_core_Flash_Cnt_mux0000_0_2_1579,
      I1 => mb_altera_core_Flash_Cnt_mux0000_0_1_1578,
      S => mb_altera_core_Flash_State_FSM_FFd7_1607,
      O => mb_altera_core_Flash_Cnt_mux0000(0)
    );
  inst_base_module_wd_rst_not0003201 : LUT4
    generic map(
      INIT => X"9909"
    )
    port map (
      I0 => inst_base_module_wd_kick_r1_937,
      I1 => inst_base_module_wd_kick_r2_938,
      I2 => inst_base_module_wd_nmi_en_945,
      I3 => inst_base_module_wd_nmi_939,
      O => inst_base_module_wd_rst_not0003201_963
    );
  inst_base_module_wd_rst_not0003202 : LUT3
    generic map(
      INIT => X"41"
    )
    port map (
      I0 => inst_base_module_wd_nmi_en_945,
      I1 => inst_base_module_wd_kick_r2_938,
      I2 => inst_base_module_wd_kick_r1_937,
      O => inst_base_module_wd_rst_not0003202_964
    );
  inst_base_module_wd_rst_not000320_f5 : MUXF5
    port map (
      I0 => inst_base_module_wd_rst_not0003202_964,
      I1 => inst_base_module_wd_rst_not0003201_963,
      S => inst_base_module_wd_rst_en_958,
      O => inst_base_module_wd_rst_not000320
    );
  inst_base_module_irq_mask_mux0000_9_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(22),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_9_1_772
    );
  inst_base_module_irq_mask_mux0000_9_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(22),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_9_2_773
    );
  inst_base_module_irq_mask_mux0000_9_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_9_2_773,
      I1 => inst_base_module_irq_mask_mux0000_9_1_772,
      S => inst_emif_iface_edi_r(22),
      O => inst_base_module_irq_mask_mux0000(9)
    );
  inst_base_module_irq_mask_mux0000_8_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(23),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_8_1_769
    );
  inst_base_module_irq_mask_mux0000_8_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(23),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_8_2_770
    );
  inst_base_module_irq_mask_mux0000_8_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_8_2_770,
      I1 => inst_base_module_irq_mask_mux0000_8_1_769,
      S => inst_emif_iface_edi_r(23),
      O => inst_base_module_irq_mask_mux0000(8)
    );
  inst_base_module_irq_mask_mux0000_7_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(24),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_7_1_766
    );
  inst_base_module_irq_mask_mux0000_7_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(24),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_7_2_767
    );
  inst_base_module_irq_mask_mux0000_7_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_7_2_767,
      I1 => inst_base_module_irq_mask_mux0000_7_1_766,
      S => inst_emif_iface_edi_r(24),
      O => inst_base_module_irq_mask_mux0000(7)
    );
  inst_base_module_irq_mask_mux0000_6_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(25),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_6_1_763
    );
  inst_base_module_irq_mask_mux0000_6_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(25),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_6_2_764
    );
  inst_base_module_irq_mask_mux0000_6_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_6_2_764,
      I1 => inst_base_module_irq_mask_mux0000_6_1_763,
      S => inst_emif_iface_edi_r(25),
      O => inst_base_module_irq_mask_mux0000(6)
    );
  inst_base_module_irq_mask_mux0000_5_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(26),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_5_1_760
    );
  inst_base_module_irq_mask_mux0000_5_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(26),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_5_2_761
    );
  inst_base_module_irq_mask_mux0000_5_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_5_2_761,
      I1 => inst_base_module_irq_mask_mux0000_5_1_760,
      S => inst_emif_iface_edi_r(26),
      O => inst_base_module_irq_mask_mux0000(5)
    );
  inst_base_module_irq_mask_mux0000_4_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(27),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_4_1_757
    );
  inst_base_module_irq_mask_mux0000_4_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(27),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_4_2_758
    );
  inst_base_module_irq_mask_mux0000_4_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_4_2_758,
      I1 => inst_base_module_irq_mask_mux0000_4_1_757,
      S => inst_emif_iface_edi_r(27),
      O => inst_base_module_irq_mask_mux0000(4)
    );
  inst_base_module_irq_mask_mux0000_3_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(28),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_3_1_754
    );
  inst_base_module_irq_mask_mux0000_3_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(28),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_3_2_755
    );
  inst_base_module_irq_mask_mux0000_3_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_3_2_755,
      I1 => inst_base_module_irq_mask_mux0000_3_1_754,
      S => inst_emif_iface_edi_r(28),
      O => inst_base_module_irq_mask_mux0000(3)
    );
  inst_base_module_irq_mask_mux0000_31_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(0),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_31_1_751
    );
  inst_base_module_irq_mask_mux0000_31_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(0),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_31_2_752
    );
  inst_base_module_irq_mask_mux0000_31_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_31_2_752,
      I1 => inst_base_module_irq_mask_mux0000_31_1_751,
      S => inst_emif_iface_edi_r(0),
      O => inst_base_module_irq_mask_mux0000(31)
    );
  inst_base_module_irq_mask_mux0000_30_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(1),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_30_1_748
    );
  inst_base_module_irq_mask_mux0000_30_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(1),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_30_2_749
    );
  inst_base_module_irq_mask_mux0000_30_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_30_2_749,
      I1 => inst_base_module_irq_mask_mux0000_30_1_748,
      S => inst_emif_iface_edi_r(1),
      O => inst_base_module_irq_mask_mux0000(30)
    );
  inst_base_module_irq_mask_mux0000_2_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(29),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_2_1_745
    );
  inst_base_module_irq_mask_mux0000_2_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(29),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_2_2_746
    );
  inst_base_module_irq_mask_mux0000_2_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_2_2_746,
      I1 => inst_base_module_irq_mask_mux0000_2_1_745,
      S => inst_emif_iface_edi_r(29),
      O => inst_base_module_irq_mask_mux0000(2)
    );
  inst_base_module_irq_mask_mux0000_29_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(2),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_29_1_742
    );
  inst_base_module_irq_mask_mux0000_29_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(2),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_29_2_743
    );
  inst_base_module_irq_mask_mux0000_29_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_29_2_743,
      I1 => inst_base_module_irq_mask_mux0000_29_1_742,
      S => inst_emif_iface_edi_r(2),
      O => inst_base_module_irq_mask_mux0000(29)
    );
  inst_base_module_irq_mask_mux0000_28_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(3),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_28_1_739
    );
  inst_base_module_irq_mask_mux0000_28_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(3),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_28_2_740
    );
  inst_base_module_irq_mask_mux0000_28_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_28_2_740,
      I1 => inst_base_module_irq_mask_mux0000_28_1_739,
      S => inst_emif_iface_edi_r(3),
      O => inst_base_module_irq_mask_mux0000(28)
    );
  inst_base_module_irq_mask_mux0000_27_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(4),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_27_1_736
    );
  inst_base_module_irq_mask_mux0000_27_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(4),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_27_2_737
    );
  inst_base_module_irq_mask_mux0000_27_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_27_2_737,
      I1 => inst_base_module_irq_mask_mux0000_27_1_736,
      S => inst_emif_iface_edi_r(4),
      O => inst_base_module_irq_mask_mux0000(27)
    );
  inst_base_module_irq_mask_mux0000_26_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(5),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_26_1_733
    );
  inst_base_module_irq_mask_mux0000_26_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(5),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_26_2_734
    );
  inst_base_module_irq_mask_mux0000_26_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_26_2_734,
      I1 => inst_base_module_irq_mask_mux0000_26_1_733,
      S => inst_emif_iface_edi_r(5),
      O => inst_base_module_irq_mask_mux0000(26)
    );
  inst_base_module_irq_mask_mux0000_25_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(6),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_25_1_730
    );
  inst_base_module_irq_mask_mux0000_25_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(6),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_25_2_731
    );
  inst_base_module_irq_mask_mux0000_25_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_25_2_731,
      I1 => inst_base_module_irq_mask_mux0000_25_1_730,
      S => inst_emif_iface_edi_r(6),
      O => inst_base_module_irq_mask_mux0000(25)
    );
  inst_base_module_irq_mask_mux0000_24_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(7),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_24_1_727
    );
  inst_base_module_irq_mask_mux0000_24_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(7),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_24_2_728
    );
  inst_base_module_irq_mask_mux0000_24_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_24_2_728,
      I1 => inst_base_module_irq_mask_mux0000_24_1_727,
      S => inst_emif_iface_edi_r(7),
      O => inst_base_module_irq_mask_mux0000(24)
    );
  inst_base_module_irq_mask_mux0000_23_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(8),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_23_1_724
    );
  inst_base_module_irq_mask_mux0000_23_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(8),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_23_2_725
    );
  inst_base_module_irq_mask_mux0000_23_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_23_2_725,
      I1 => inst_base_module_irq_mask_mux0000_23_1_724,
      S => inst_emif_iface_edi_r(8),
      O => inst_base_module_irq_mask_mux0000(23)
    );
  inst_base_module_irq_mask_mux0000_22_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(9),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_22_1_721
    );
  inst_base_module_irq_mask_mux0000_22_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(9),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_22_2_722
    );
  inst_base_module_irq_mask_mux0000_22_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_22_2_722,
      I1 => inst_base_module_irq_mask_mux0000_22_1_721,
      S => inst_emif_iface_edi_r(9),
      O => inst_base_module_irq_mask_mux0000(22)
    );
  inst_base_module_irq_mask_mux0000_21_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(10),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_21_1_718
    );
  inst_base_module_irq_mask_mux0000_21_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(10),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_21_2_719
    );
  inst_base_module_irq_mask_mux0000_21_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_21_2_719,
      I1 => inst_base_module_irq_mask_mux0000_21_1_718,
      S => inst_emif_iface_edi_r(10),
      O => inst_base_module_irq_mask_mux0000(21)
    );
  inst_base_module_irq_mask_mux0000_20_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(11),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_20_1_715
    );
  inst_base_module_irq_mask_mux0000_20_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(11),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_20_2_716
    );
  inst_base_module_irq_mask_mux0000_20_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_20_2_716,
      I1 => inst_base_module_irq_mask_mux0000_20_1_715,
      S => inst_emif_iface_edi_r(11),
      O => inst_base_module_irq_mask_mux0000(20)
    );
  inst_base_module_irq_mask_mux0000_1_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(30),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_1_1_712
    );
  inst_base_module_irq_mask_mux0000_1_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(30),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_1_2_713
    );
  inst_base_module_irq_mask_mux0000_1_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_1_2_713,
      I1 => inst_base_module_irq_mask_mux0000_1_1_712,
      S => inst_emif_iface_edi_r(30),
      O => inst_base_module_irq_mask_mux0000(1)
    );
  inst_base_module_irq_mask_mux0000_19_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(12),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_19_1_709
    );
  inst_base_module_irq_mask_mux0000_19_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(12),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_19_2_710
    );
  inst_base_module_irq_mask_mux0000_19_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_19_2_710,
      I1 => inst_base_module_irq_mask_mux0000_19_1_709,
      S => inst_emif_iface_edi_r(12),
      O => inst_base_module_irq_mask_mux0000(19)
    );
  inst_base_module_irq_mask_mux0000_18_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(13),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_18_1_706
    );
  inst_base_module_irq_mask_mux0000_18_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(13),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_18_2_707
    );
  inst_base_module_irq_mask_mux0000_18_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_18_2_707,
      I1 => inst_base_module_irq_mask_mux0000_18_1_706,
      S => inst_emif_iface_edi_r(13),
      O => inst_base_module_irq_mask_mux0000(18)
    );
  inst_base_module_irq_mask_mux0000_17_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(14),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_17_1_703
    );
  inst_base_module_irq_mask_mux0000_17_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(14),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_17_2_704
    );
  inst_base_module_irq_mask_mux0000_17_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_17_2_704,
      I1 => inst_base_module_irq_mask_mux0000_17_1_703,
      S => inst_emif_iface_edi_r(14),
      O => inst_base_module_irq_mask_mux0000(17)
    );
  inst_base_module_irq_mask_mux0000_16_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(15),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_16_1_700
    );
  inst_base_module_irq_mask_mux0000_16_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(15),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_16_2_701
    );
  inst_base_module_irq_mask_mux0000_16_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_16_2_701,
      I1 => inst_base_module_irq_mask_mux0000_16_1_700,
      S => inst_emif_iface_edi_r(15),
      O => inst_base_module_irq_mask_mux0000(16)
    );
  inst_base_module_irq_mask_mux0000_15_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(16),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_15_1_697
    );
  inst_base_module_irq_mask_mux0000_15_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(16),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_15_2_698
    );
  inst_base_module_irq_mask_mux0000_15_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_15_2_698,
      I1 => inst_base_module_irq_mask_mux0000_15_1_697,
      S => inst_emif_iface_edi_r(16),
      O => inst_base_module_irq_mask_mux0000(15)
    );
  inst_base_module_irq_mask_mux0000_14_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(17),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_14_1_694
    );
  inst_base_module_irq_mask_mux0000_14_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(17),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_14_2_695
    );
  inst_base_module_irq_mask_mux0000_14_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_14_2_695,
      I1 => inst_base_module_irq_mask_mux0000_14_1_694,
      S => inst_emif_iface_edi_r(17),
      O => inst_base_module_irq_mask_mux0000(14)
    );
  inst_base_module_irq_mask_mux0000_13_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(18),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_13_1_691
    );
  inst_base_module_irq_mask_mux0000_13_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(18),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_13_2_692
    );
  inst_base_module_irq_mask_mux0000_13_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_13_2_692,
      I1 => inst_base_module_irq_mask_mux0000_13_1_691,
      S => inst_emif_iface_edi_r(18),
      O => inst_base_module_irq_mask_mux0000(13)
    );
  inst_base_module_irq_mask_mux0000_12_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(19),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_12_1_688
    );
  inst_base_module_irq_mask_mux0000_12_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(19),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_12_2_689
    );
  inst_base_module_irq_mask_mux0000_12_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_12_2_689,
      I1 => inst_base_module_irq_mask_mux0000_12_1_688,
      S => inst_emif_iface_edi_r(19),
      O => inst_base_module_irq_mask_mux0000(12)
    );
  inst_base_module_irq_mask_mux0000_11_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(20),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_11_1_685
    );
  inst_base_module_irq_mask_mux0000_11_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(20),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_11_2_686
    );
  inst_base_module_irq_mask_mux0000_11_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_11_2_686,
      I1 => inst_base_module_irq_mask_mux0000_11_1_685,
      S => inst_emif_iface_edi_r(20),
      O => inst_base_module_irq_mask_mux0000(11)
    );
  inst_base_module_irq_mask_mux0000_10_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(21),
      I2 => inst_base_module_N0,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_10_1_682
    );
  inst_base_module_irq_mask_mux0000_10_2 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_irq_mask(21),
      I2 => inst_base_module_N0,
      O => inst_base_module_irq_mask_mux0000_10_2_683
    );
  inst_base_module_irq_mask_mux0000_10_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_10_2_683,
      I1 => inst_base_module_irq_mask_mux0000_10_1_682,
      S => inst_emif_iface_edi_r(21),
      O => inst_base_module_irq_mask_mux0000(10)
    );
  inst_base_module_irq_mask_mux0000_0_1 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_N0,
      I2 => inst_base_module_irq_mask(31),
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      O => inst_base_module_irq_mask_mux0000_0_1_679
    );
  inst_base_module_irq_mask_mux0000_0_3 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_N2,
      I1 => inst_base_module_N0,
      I2 => inst_base_module_irq_mask(31),
      O => inst_base_module_irq_mask_mux0000_0_2_680
    );
  inst_base_module_irq_mask_mux0000_0_f5 : MUXF5
    port map (
      I0 => inst_base_module_irq_mask_mux0000_0_2_680,
      I1 => inst_base_module_irq_mask_mux0000_0_1_679,
      S => inst_emif_iface_edi_r(31),
      O => inst_base_module_irq_mask_mux0000(0)
    );
  mb_altera_core_o_DBus_4_mux0002181 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      I1 => mb_altera_core_Read_Fifo_Data_in(4),
      I2 => mb_altera_core_o_user_io(4),
      I3 => mb_altera_core_o_DBus_4_mux00029,
      O => mb_altera_core_o_DBus_4_mux0002181_2403
    );
  mb_altera_core_o_DBus_4_mux0002182 : LUT4
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      I1 => mb_altera_core_Read_Fifo_Data_in(4),
      I2 => mb_altera_core_s_dma_timeout_threshold(4),
      I3 => mb_altera_core_o_DBus_4_mux00029,
      O => mb_altera_core_o_DBus_4_mux0002182_2404
    );
  mb_altera_core_o_DBus_4_mux000218_f5 : MUXF5
    port map (
      I0 => mb_altera_core_o_DBus_4_mux0002182_2404,
      I1 => mb_altera_core_o_DBus_4_mux0002181_2403,
      S => inst_emif_iface_addr_r(3),
      O => mb_altera_core_o_DBus_4_mux000218
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_13_11 : LUT4
    generic map(
      INIT => X"7FFF"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(3),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row(2),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_row(1),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_row(0),
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_13_1
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_13_1_f5 : MUXF5
    port map (
      I0 => Mtrien_io_altera_dsp_mux0000_norst,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_13_1,
      S => inst_emif_iface_sdram_dma_std_dma_engine_row(4),
      O => inst_emif_iface_sdram_dma_std_dma_engine_N24
    );
  mb_altera_core_altera_asd_mux000061 : LUT4
    generic map(
      INIT => X"FFFE"
    )
    port map (
      I0 => mb_altera_core_Flash_State_FSM_FFd1_1599,
      I1 => mb_altera_core_Flash_State_FSM_FFd4_1602,
      I2 => mb_altera_core_Flash_State_FSM_FFd3_1601,
      I3 => mb_altera_core_Flash_State_FSM_FFd2_1600,
      O => mb_altera_core_altera_asd_mux000061_2219
    );
  mb_altera_core_altera_asd_mux00006_f5 : MUXF5
    port map (
      I0 => mb_altera_core_altera_asd_mux000061_2219,
      I1 => Mtrien_io_altera_dsp_mux0000_norst,
      S => mb_altera_core_Flash_State_FSM_FFd5_1603,
      O => mb_altera_core_altera_asd_mux00006
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_14_91 : LUT4
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(2),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row(4),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_row(5),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_row(3),
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_14_91_1380
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_14_9_f5 : MUXF5
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_14_91_1380,
      I1 => ea(15),
      S => inst_emif_iface_sdram_dma_std_dma_engine_row(6),
      O => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_14_9
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_2_SW11 : LUT4
    generic map(
      INIT => X"A2AA"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(4),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N30,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(3),
      O => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_2_SW1
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_2_SW1_f5 : MUXF5
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_2_SW1,
      S => inst_emif_iface_sdram_dma_std_dma_engine_word_count(5),
      O => N302
    );
  mb_altera_core_s_altera_data_oen_mux000011 : LUT4
    generic map(
      INIT => X"FEFF"
    )
    port map (
      I0 => mb_altera_core_altera_state_FSM_FFd1_2228,
      I1 => mb_altera_core_altera_state_FSM_FFd2_2229,
      I2 => mb_altera_core_altera_state_FSM_FFd4_2231,
      I3 => mb_altera_core_altera_state_FSM_FFd5_2233,
      O => mb_altera_core_s_altera_data_oen_mux000011_2547
    );
  mb_altera_core_s_altera_data_oen_mux000012 : LUT3
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => mb_altera_core_altera_state_FSM_FFd1_2228,
      I1 => mb_altera_core_altera_state_FSM_FFd2_2229,
      I2 => mb_altera_core_altera_state_FSM_FFd4_2231,
      O => mb_altera_core_s_altera_data_oen_mux000012_2548
    );
  mb_altera_core_s_altera_data_oen_mux00001_f5 : MUXF5
    port map (
      I0 => mb_altera_core_s_altera_data_oen_mux000012_2548,
      I1 => mb_altera_core_s_altera_data_oen_mux000011_2547,
      S => mb_altera_core_s_altera_data_oen_2545,
      O => mb_altera_core_s_altera_data_oen_mux00001
    );
  inst_base_module_o_DBus_mux0002_18_11 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0005,
      I1 => inst_base_module_o_DBus_cmp_eq0004,
      I2 => inst_base_module_irq_mask(18),
      I3 => inst_base_module_varindex0000(18),
      O => inst_base_module_o_DBus_mux0002_18_11_844
    );
  inst_base_module_o_DBus_mux0002_18_12 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0005,
      I1 => inst_base_module_o_DBus_cmp_eq0004,
      I2 => inst_base_module_irq_mask(18),
      O => inst_base_module_o_DBus_mux0002_18_12_845
    );
  inst_base_module_o_DBus_mux0002_18_1_f5 : MUXF5
    port map (
      I0 => inst_base_module_o_DBus_mux0002_18_12_845,
      I1 => inst_base_module_o_DBus_mux0002_18_11_844,
      S => inst_emif_iface_addr_r(4),
      O => inst_base_module_o_DBus_mux0002_18_1
    );
  inst_base_module_o_DBus_mux0002_17_11 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0005,
      I1 => inst_base_module_o_DBus_cmp_eq0004,
      I2 => inst_base_module_irq_mask(17),
      I3 => inst_base_module_varindex0000(17),
      O => inst_base_module_o_DBus_mux0002_17_11_841
    );
  inst_base_module_o_DBus_mux0002_17_12 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => inst_base_module_o_DBus_cmp_eq0005,
      I1 => inst_base_module_o_DBus_cmp_eq0004,
      I2 => inst_base_module_irq_mask(17),
      O => inst_base_module_o_DBus_mux0002_17_12_842
    );
  inst_base_module_o_DBus_mux0002_17_1_f5 : MUXF5
    port map (
      I0 => inst_base_module_o_DBus_mux0002_17_12_842,
      I1 => inst_base_module_o_DBus_mux0002_17_11_841,
      S => inst_emif_iface_addr_r(4),
      O => inst_base_module_o_DBus_mux0002_17_1
    );
  mb_altera_core_o_DBus_8_mux000210511 : LUT4
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => mb_altera_core_o_DBus_10_cmp_eq0005,
      I1 => mb_altera_core_o_DBus_10_cmp_eq0004,
      I2 => mb_altera_core_s_dma_base_addr(6),
      I3 => mb_altera_core_o_DBus_8_mux000292_2441,
      O => mb_altera_core_o_DBus_8_mux00021051
    );
  mb_altera_core_o_DBus_8_mux000210512 : LUT3
    generic map(
      INIT => X"EA"
    )
    port map (
      I0 => mb_altera_core_o_DBus_8_mux000292_2441,
      I1 => mb_altera_core_o_DBus_10_cmp_eq0004,
      I2 => mb_altera_core_s_dma_base_addr(6),
      O => mb_altera_core_o_DBus_8_mux000210511_2437
    );
  mb_altera_core_o_DBus_8_mux00021051_f5 : MUXF5
    port map (
      I0 => mb_altera_core_o_DBus_8_mux000210511_2437,
      I1 => mb_altera_core_o_DBus_8_mux00021051,
      S => mb_altera_core_s_dma_space_mask(8),
      O => mb_altera_core_o_DBus_8_mux0002105
    );
  mb_altera_core_s_dma_num_samples_mux0000_8_1 : LUT4
    generic map(
      INIT => X"2AAA"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I1 => mb_altera_core_s_dma_num_samples(0),
      I2 => mb_altera_core_s_altera_rdfifo_rd_en,
      I3 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      O => mb_altera_core_s_dma_num_samples_mux0000_8_1_2691
    );
  mb_altera_core_s_dma_num_samples_mux0000_8_2 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_s_dma_num_samples(0),
      I1 => mb_altera_core_N8,
      O => mb_altera_core_s_dma_num_samples_mux0000_8_2_2692
    );
  mb_altera_core_s_dma_num_samples_mux0000_8_f5 : MUXF5
    port map (
      I0 => mb_altera_core_s_dma_num_samples_mux0000_8_2_2692,
      I1 => mb_altera_core_s_dma_num_samples_mux0000_8_1_2691,
      S => mb_altera_core_s_dma_num_samples(1),
      O => mb_altera_core_s_dma_num_samples_mux0000(8)
    );
  mb_altera_core_s_dma_num_samples_mux0000_5_1 : LUT4
    generic map(
      INIT => X"2AAA"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I1 => mb_altera_core_Madd_s_dma_num_samples_addsub0000_cy_3_Q,
      I2 => mb_altera_core_s_altera_rdfifo_rd_en,
      I3 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      O => mb_altera_core_s_dma_num_samples_mux0000_5_1_2686
    );
  mb_altera_core_s_dma_num_samples_mux0000_5_2 : LUT2
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => mb_altera_core_Madd_s_dma_num_samples_addsub0000_cy_3_Q,
      I1 => mb_altera_core_N8,
      O => mb_altera_core_s_dma_num_samples_mux0000_5_2_2687
    );
  mb_altera_core_s_dma_num_samples_mux0000_5_f5 : MUXF5
    port map (
      I0 => mb_altera_core_s_dma_num_samples_mux0000_5_2_2687,
      I1 => mb_altera_core_s_dma_num_samples_mux0000_5_1_2686,
      S => mb_altera_core_s_dma_num_samples(4),
      O => mb_altera_core_s_dma_num_samples_mux0000(5)
    );
  mb_altera_core_Write_Fifo_Data_in_10_or0000141 : LUT4
    generic map(
      INIT => X"0600"
    )
    port map (
      I0 => inst_emif_iface_addr_r(0),
      I1 => inst_emif_iface_addr_r(1),
      I2 => inst_emif_iface_addr_r(2),
      I3 => inst_emif_iface_addr_r(3),
      O => mb_altera_core_Write_Fifo_Data_in_10_or0000141_2096
    );
  mb_altera_core_Write_Fifo_Data_in_10_or000014_f5 : MUXF5
    port map (
      I0 => mb_altera_core_Write_Fifo_Data_in_10_or0000141_2096,
      I1 => ea(15),
      S => inst_emif_iface_addr_r(4),
      O => mb_altera_core_Write_Fifo_Data_in_10_or000014
    );
  mb_altera_core_s_dma_num_samples_mux0000_2_1 : LUT4
    generic map(
      INIT => X"EA62"
    )
    port map (
      I0 => mb_altera_core_s_dma_num_samples(7),
      I1 => mb_altera_core_Madd_s_dma_num_samples_addsub0000_cy_6_Q,
      I2 => mb_altera_core_N8,
      I3 => mb_altera_core_N38,
      O => mb_altera_core_s_dma_num_samples_mux0000_2_1_2681
    );
  mb_altera_core_s_dma_num_samples_mux0000_2_2 : LUT4
    generic map(
      INIT => X"EC20"
    )
    port map (
      I0 => mb_altera_core_Madd_s_dma_num_samples_addsub0000_cy_6_Q,
      I1 => mb_altera_core_s_dma_num_samples(7),
      I2 => mb_altera_core_N8,
      I3 => mb_altera_core_N38,
      O => mb_altera_core_s_dma_num_samples_mux0000_2_2_2682
    );
  mb_altera_core_s_dma_num_samples_mux0000_2_f5 : MUXF5
    port map (
      I0 => mb_altera_core_s_dma_num_samples_mux0000_2_2_2682,
      I1 => mb_altera_core_s_dma_num_samples_mux0000_2_1_2681,
      S => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      O => mb_altera_core_s_dma_num_samples_mux0000(2)
    );
  mb_altera_core_o_DBus_1_mux000267_SW01 : LUT4
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(1),
      I1 => inst_emif_iface_addr_r(2),
      I2 => inst_emif_iface_addr_r(0),
      I3 => inst_emif_iface_addr_r(4),
      O => mb_altera_core_o_DBus_1_mux000267_SW0
    );
  mb_altera_core_o_DBus_1_mux000267_SW02 : LUT4
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => inst_emif_iface_addr_r(0),
      I1 => mb_altera_core_Flash_Read_Reg(1),
      I2 => inst_emif_iface_addr_r(2),
      I3 => inst_emif_iface_addr_r(4),
      O => mb_altera_core_o_DBus_1_mux000267_SW01_2310
    );
  mb_altera_core_o_DBus_1_mux000267_SW0_f5 : MUXF5
    port map (
      I0 => mb_altera_core_o_DBus_1_mux000267_SW01_2310,
      I1 => mb_altera_core_o_DBus_1_mux000267_SW0,
      S => inst_emif_iface_addr_r(3),
      O => N638
    );
  mb_altera_core_s_dma_state_FSM_FFd2_In1 : LUT4
    generic map(
      INIT => X"04AE"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I1 => mb_altera_core_s_dma_en_2662,
      I2 => mb_altera_core_flash_enable_2235,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_done_1301,
      O => mb_altera_core_s_dma_state_FSM_FFd2_In1_2878
    );
  mb_altera_core_s_dma_state_FSM_FFd2_In2 : LUT4
    generic map(
      INIT => X"CE02"
    )
    port map (
      I0 => mb_altera_core_s_dma_en_2662,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_flash_enable_2235,
      I3 => mb_altera_core_s_dma_irq_clear_2664,
      O => mb_altera_core_s_dma_state_FSM_FFd2_In2_2879
    );
  mb_altera_core_s_dma_state_FSM_FFd2_In_f5 : MUXF5
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_In2_2879,
      I1 => mb_altera_core_s_dma_state_FSM_FFd2_In1_2878,
      S => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      O => mb_altera_core_s_dma_state_FSM_FFd2_In
    );
  inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt_mux0000_0_1 : LUT4
    generic map(
      INIT => X"FAF2"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt(0),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      O => inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt_mux0000_0_1_1437
    );
  inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt_mux0000_0_2 : LUT4
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt(0),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      O => inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt_mux0000_0_2_1438
    );
  inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt_mux0000_0_f5 : MUXF5
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt_mux0000_0_2_1438,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt_mux0000_0_1_1437,
      S => inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt(1),
      O => inst_emif_iface_sdram_dma_std_dma_engine_tmp_cnt_mux0000(0)
    );
  mb_altera_core_altera_nce_and000021 : LUT4_D
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => inst_emif_iface_addr_r(2),
      I1 => mb_altera_core_N15,
      I2 => inst_base_module_irq_mask_and0000,
      I3 => inst_emif_iface_addr_r(1),
      LO => N768,
      O => mb_altera_core_N44
    );
  inst_base_module_key_state_cmp_eq0004110 : LUT4_D
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => inst_emif_iface_edi_r(23),
      I1 => inst_emif_iface_edi_r(21),
      I2 => inst_emif_iface_edi_r(20),
      I3 => inst_emif_iface_edi_r(22),
      LO => N769,
      O => inst_base_module_key_state_cmp_eq0004110_782
    );
  mb_altera_core_Write_fifo_wr_Cnt_not000110 : LUT4_L
    generic map(
      INIT => X"FFFE"
    )
    port map (
      I0 => mb_altera_core_Write_fifo_wr_Cnt(3),
      I1 => mb_altera_core_Write_fifo_wr_Cnt(2),
      I2 => mb_altera_core_Write_fifo_wr_Cnt(1),
      I3 => mb_altera_core_Write_fifo_wr_Cnt(0),
      LO => mb_altera_core_Write_fifo_wr_Cnt_not000110_2180
    );
  mb_altera_core_o_DBus_10_cmp_eq000211 : LUT4_D
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => inst_emif_iface_addr_r(0),
      I2 => inst_emif_iface_addr_r(2),
      I3 => inst_emif_iface_addr_r(3),
      LO => N770,
      O => mb_altera_core_N41
    );
  mb_altera_core_s_dma_num_samples_mux0000_4_SW0 : LUT3_L
    generic map(
      INIT => X"7F"
    )
    port map (
      I0 => mb_altera_core_N8,
      I1 => mb_altera_core_Madd_s_dma_num_samples_addsub0000_cy_3_Q,
      I2 => mb_altera_core_s_dma_num_samples(4),
      LO => N226
    );
  inst_base_module_o_DBus_mux0002_4_SW0 : LUT4_L
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => inst_base_module_varindex0000(4),
      I2 => inst_base_module_irq_mask(4),
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      LO => N231
    );
  mb_altera_core_o_DBus_29_mux000013 : LUT4_L
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_N41,
      I1 => inst_emif_iface_addr_r(1),
      I2 => mb_altera_core_s_dma_start_addr(27),
      I3 => mb_altera_core_Read_Fifo_Data_Ready_2024,
      LO => mb_altera_core_o_DBus_29_mux000013_2365
    );
  mb_altera_core_o_DBus_29_mux000034 : LUT4_L
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_N12,
      I1 => inst_emif_iface_addr_r(3),
      I2 => mb_altera_core_s_dma_timeout_threshold(29),
      I3 => mb_altera_core_o_user_io(29),
      LO => mb_altera_core_o_DBus_29_mux000034_2367
    );
  mb_altera_core_o_DBus_7_mux000213 : LUT4_L
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_N41,
      I1 => inst_emif_iface_addr_r(1),
      I2 => mb_altera_core_s_dma_start_addr(5),
      I3 => mb_altera_core_s_altera_rdfifo_rd_cnt(7),
      LO => mb_altera_core_o_DBus_7_mux000213_2427
    );
  mb_altera_core_o_DBus_7_mux000290 : LUT4_L
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => mb_altera_core_s_dma_num_samples(5),
      I1 => mb_altera_core_o_DBus_10_cmp_eq0007,
      I2 => mb_altera_core_o_DBus_10_cmp_eq0004,
      I3 => mb_altera_core_s_dma_base_addr(5),
      LO => mb_altera_core_o_DBus_7_mux000290_2432
    );
  mb_altera_core_o_DBus_5_mux000213 : LUT4_L
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_N41,
      I1 => inst_emif_iface_addr_r(1),
      I2 => mb_altera_core_s_dma_start_addr(3),
      I3 => mb_altera_core_s_altera_rdfifo_rd_cnt(5),
      LO => mb_altera_core_o_DBus_5_mux000213_2411
    );
  mb_altera_core_o_DBus_5_mux000290 : LUT4_L
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => mb_altera_core_s_dma_num_samples(3),
      I1 => mb_altera_core_o_DBus_10_cmp_eq0007,
      I2 => mb_altera_core_o_DBus_10_cmp_eq0004,
      I3 => mb_altera_core_s_dma_base_addr(3),
      LO => mb_altera_core_o_DBus_5_mux000290_2416
    );
  mb_altera_core_o_DBus_4_mux000236 : LUT4_L
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_N13,
      I1 => inst_emif_iface_addr_r(1),
      I2 => mb_altera_core_s_err_clr_3012,
      I3 => mb_altera_core_s_dma_base_addr(2),
      LO => mb_altera_core_o_DBus_4_mux000236_2405
    );
  mb_altera_core_o_DBus_4_mux000280 : LUT4_L
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_N41,
      I1 => inst_emif_iface_addr_r(1),
      I2 => mb_altera_core_s_dma_start_addr(2),
      I3 => mb_altera_core_s_altera_rdfifo_rd_cnt(4),
      LO => mb_altera_core_o_DBus_4_mux000280_2407
    );
  mb_altera_core_Write_Fifo_Data_in_10_or000021 : LUT2_D
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => inst_emif_iface_cs_r_8_Q,
      I1 => inst_emif_iface_wr_r_1474,
      LO => N771,
      O => mb_altera_core_N15
    );
  mb_altera_core_altera_nce_and0000111 : LUT2_D
    generic map(
      INIT => X"1"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => inst_emif_iface_addr_r(3),
      LO => N772,
      O => inst_base_module_irq_mask_and0000
    );
  mb_altera_core_o_DBus_0_cmp_eq000111 : LUT3_D
    generic map(
      INIT => X"40"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => inst_emif_iface_addr_r(0),
      I2 => inst_emif_iface_addr_r(1),
      LO => N773,
      O => mb_altera_core_N42
    );
  inst_base_module_o_DBus_cmp_eq00031 : LUT4_D
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => inst_emif_iface_addr_r(2),
      I1 => inst_base_module_irq_mask_and0000,
      I2 => inst_emif_iface_addr_r(0),
      I3 => inst_emif_iface_addr_r(1),
      LO => N774,
      O => inst_base_module_o_DBus_cmp_eq0003
    );
  mb_altera_core_o_DBus_10_cmp_eq0003_SW0 : LUT2_D
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => inst_emif_iface_addr_r(0),
      I1 => inst_emif_iface_addr_r(2),
      LO => N775,
      O => N241
    );
  inst_base_module_o_DBus_mux0002_6_SW0 : LUT4_L
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => inst_base_module_varindex0000(6),
      I1 => inst_emif_iface_addr_r(4),
      I2 => inst_base_module_irq_mask(6),
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      LO => N250
    );
  inst_base_module_o_DBus_mux0002_24_SW0 : LUT4_L
    generic map(
      INIT => X"AA80"
    )
    port map (
      I0 => inst_base_module_irq_mask(24),
      I1 => irq_map_7_0_Q,
      I2 => inst_base_module_o_DBus_cmp_eq0003,
      I3 => inst_base_module_o_DBus_cmp_eq0004,
      LO => N252
    );
  mb_altera_core_o_DBus_30_mux000042 : LUT4_L
    generic map(
      INIT => X"EAC0"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(30),
      I1 => mb_altera_core_o_DBus_30_mux000033_2381,
      I2 => mb_altera_core_N12,
      I3 => mb_altera_core_o_DBus_10_cmp_eq0005,
      LO => mb_altera_core_o_DBus_30_mux000042_2382
    );
  mb_altera_core_o_DBus_13_mux000042 : LUT4_L
    generic map(
      INIT => X"EAC0"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(13),
      I1 => mb_altera_core_o_DBus_13_mux000033_2273,
      I2 => mb_altera_core_N12,
      I3 => mb_altera_core_o_DBus_10_cmp_eq0005,
      LO => mb_altera_core_o_DBus_13_mux000042_2274
    );
  mb_altera_core_o_DBus_28_mux00004 : LUT4_L
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => mb_altera_core_o_DBus(28),
      I1 => mb_altera_core_o_DBus_10_cmp_eq0007,
      I2 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      I3 => mb_altera_core_Read_Fifo_Data_in(28),
      LO => mb_altera_core_o_DBus_28_mux00004_2361
    );
  mb_altera_core_o_DBus_28_mux000021 : LUT4_L
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_N41,
      I1 => inst_emif_iface_addr_r(1),
      I2 => mb_altera_core_s_dma_start_addr(26),
      I3 => mb_altera_core_Write_fifo_wr_Cnt(8),
      LO => mb_altera_core_o_DBus_28_mux000021_2359
    );
  mb_altera_core_o_DBus_9_mux000215 : LUT3_L
    generic map(
      INIT => X"EC"
    )
    port map (
      I0 => mb_altera_core_Read_Fifo_Data_in(9),
      I1 => mb_altera_core_o_DBus_9_mux000213_2443,
      I2 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      LO => mb_altera_core_o_DBus_9_mux000215_2444
    );
  mb_altera_core_o_DBus_31_mux000015 : LUT3_L
    generic map(
      INIT => X"EC"
    )
    port map (
      I0 => mb_altera_core_Read_Fifo_Data_in(31),
      I1 => mb_altera_core_o_DBus_31_mux000013_2387,
      I2 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      LO => mb_altera_core_o_DBus_31_mux000015_2388
    );
  mb_altera_core_o_DBus_26_mux000015 : LUT3_L
    generic map(
      INIT => X"EC"
    )
    port map (
      I0 => mb_altera_core_Read_Fifo_Data_in(26),
      I1 => mb_altera_core_o_DBus_26_mux000013_2348,
      I2 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      LO => mb_altera_core_o_DBus_26_mux000015_2349
    );
  mb_altera_core_o_DBus_25_mux000015 : LUT3_L
    generic map(
      INIT => X"EC"
    )
    port map (
      I0 => mb_altera_core_Read_Fifo_Data_in(25),
      I1 => mb_altera_core_o_DBus_25_mux000013_2342,
      I2 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      LO => mb_altera_core_o_DBus_25_mux000015_2343
    );
  mb_altera_core_o_DBus_23_mux000015 : LUT3_L
    generic map(
      INIT => X"EC"
    )
    port map (
      I0 => mb_altera_core_Read_Fifo_Data_in(23),
      I1 => mb_altera_core_o_DBus_23_mux000013_2331,
      I2 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      LO => mb_altera_core_o_DBus_23_mux000015_2332
    );
  mb_altera_core_o_DBus_21_mux000015 : LUT3_L
    generic map(
      INIT => X"EC"
    )
    port map (
      I0 => mb_altera_core_Read_Fifo_Data_in(21),
      I1 => mb_altera_core_o_DBus_21_mux000013_2320,
      I2 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      LO => mb_altera_core_o_DBus_21_mux000015_2321
    );
  mb_altera_core_o_DBus_20_mux000015 : LUT3_L
    generic map(
      INIT => X"EC"
    )
    port map (
      I0 => mb_altera_core_Read_Fifo_Data_in(20),
      I1 => mb_altera_core_o_DBus_20_mux000013_2314,
      I2 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      LO => mb_altera_core_o_DBus_20_mux000015_2315
    );
  mb_altera_core_o_DBus_12_mux000015 : LUT3_L
    generic map(
      INIT => X"EC"
    )
    port map (
      I0 => mb_altera_core_Read_Fifo_Data_in(12),
      I1 => mb_altera_core_o_DBus_12_mux000013_2266,
      I2 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      LO => mb_altera_core_o_DBus_12_mux000015_2267
    );
  mb_altera_core_o_DBus_11_mux000215 : LUT3_L
    generic map(
      INIT => X"EC"
    )
    port map (
      I0 => mb_altera_core_Read_Fifo_Data_in(11),
      I1 => mb_altera_core_o_DBus_11_mux000213_2260,
      I2 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      LO => mb_altera_core_o_DBus_11_mux000215_2261
    );
  mb_altera_core_o_DBus_10_mux000215 : LUT3_L
    generic map(
      INIT => X"EC"
    )
    port map (
      I0 => mb_altera_core_Read_Fifo_Data_in(10),
      I1 => mb_altera_core_o_DBus_10_mux000213_2254,
      I2 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      LO => mb_altera_core_o_DBus_10_mux000215_2255
    );
  mb_altera_core_o_DBus_24_mux000013 : LUT4_L
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_N41,
      I1 => inst_emif_iface_addr_r(1),
      I2 => mb_altera_core_s_dma_start_addr(22),
      I3 => mb_altera_core_Write_fifo_wr_Cnt(4),
      LO => mb_altera_core_o_DBus_24_mux000013_2337
    );
  mb_altera_core_o_DBus_24_mux000033 : LUT4_L
    generic map(
      INIT => X"ECA0"
    )
    port map (
      I0 => mb_altera_core_s_dma_base_addr(22),
      I1 => mb_altera_core_o_DBus(24),
      I2 => mb_altera_core_o_DBus_10_cmp_eq0004,
      I3 => mb_altera_core_o_DBus_10_cmp_eq0007,
      LO => mb_altera_core_o_DBus_24_mux000033_2338
    );
  mb_altera_core_o_DBus_22_mux000013 : LUT4_L
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_N41,
      I1 => inst_emif_iface_addr_r(1),
      I2 => mb_altera_core_s_dma_start_addr(20),
      I3 => mb_altera_core_Write_fifo_wr_Cnt(2),
      LO => mb_altera_core_o_DBus_22_mux000013_2326
    );
  mb_altera_core_o_DBus_22_mux000033 : LUT4_L
    generic map(
      INIT => X"ECA0"
    )
    port map (
      I0 => mb_altera_core_s_dma_base_addr(20),
      I1 => mb_altera_core_o_DBus(22),
      I2 => mb_altera_core_o_DBus_10_cmp_eq0004,
      I3 => mb_altera_core_o_DBus_10_cmp_eq0007,
      LO => mb_altera_core_o_DBus_22_mux000033_2327
    );
  mb_altera_core_o_DBus_19_mux000013 : LUT4_L
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_N41,
      I1 => inst_emif_iface_addr_r(1),
      I2 => mb_altera_core_s_dma_start_addr(17),
      I3 => mb_altera_core_Write_Fifo_ff,
      LO => mb_altera_core_o_DBus_19_mux000013_2303
    );
  mb_altera_core_o_DBus_19_mux000033 : LUT4_L
    generic map(
      INIT => X"ECA0"
    )
    port map (
      I0 => mb_altera_core_s_dma_base_addr(17),
      I1 => mb_altera_core_o_DBus(19),
      I2 => mb_altera_core_o_DBus_10_cmp_eq0004,
      I3 => mb_altera_core_o_DBus_10_cmp_eq0007,
      LO => mb_altera_core_o_DBus_19_mux000033_2304
    );
  mb_altera_core_o_DBus_18_mux000013 : LUT4_L
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_N41,
      I1 => inst_emif_iface_addr_r(1),
      I2 => mb_altera_core_s_dma_start_addr(16),
      I3 => mb_altera_core_Write_Fifo_ef,
      LO => mb_altera_core_o_DBus_18_mux000013_2298
    );
  mb_altera_core_o_DBus_18_mux000033 : LUT4_L
    generic map(
      INIT => X"ECA0"
    )
    port map (
      I0 => mb_altera_core_s_dma_base_addr(16),
      I1 => mb_altera_core_o_DBus(18),
      I2 => mb_altera_core_o_DBus_10_cmp_eq0004,
      I3 => mb_altera_core_o_DBus_10_cmp_eq0007,
      LO => mb_altera_core_o_DBus_18_mux000033_2299
    );
  mb_altera_core_o_DBus_17_mux000013 : LUT4_L
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_N41,
      I1 => inst_emif_iface_addr_r(1),
      I2 => mb_altera_core_s_dma_start_addr(15),
      I3 => o_altera_read_ff_ff_OBUF_3033,
      LO => mb_altera_core_o_DBus_17_mux000013_2293
    );
  mb_altera_core_o_DBus_17_mux000033 : LUT4_L
    generic map(
      INIT => X"ECA0"
    )
    port map (
      I0 => mb_altera_core_s_dma_base_addr(15),
      I1 => mb_altera_core_o_DBus(17),
      I2 => mb_altera_core_o_DBus_10_cmp_eq0004,
      I3 => mb_altera_core_o_DBus_10_cmp_eq0007,
      LO => mb_altera_core_o_DBus_17_mux000033_2294
    );
  mb_altera_core_o_DBus_16_mux000013 : LUT4_L
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_N41,
      I1 => inst_emif_iface_addr_r(1),
      I2 => mb_altera_core_s_dma_start_addr(14),
      I3 => mb_altera_core_s_altera_rdfifo_empty,
      LO => mb_altera_core_o_DBus_16_mux000013_2288
    );
  mb_altera_core_o_DBus_16_mux000033 : LUT4_L
    generic map(
      INIT => X"ECA0"
    )
    port map (
      I0 => mb_altera_core_s_dma_base_addr(14),
      I1 => mb_altera_core_o_DBus(16),
      I2 => mb_altera_core_o_DBus_10_cmp_eq0004,
      I3 => mb_altera_core_o_DBus_10_cmp_eq0007,
      LO => mb_altera_core_o_DBus_16_mux000033_2289
    );
  mb_altera_core_o_DBus_8_mux000212 : LUT4_L
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_N41,
      I1 => inst_emif_iface_addr_r(1),
      I2 => mb_altera_core_s_dma_start_addr(6),
      I3 => mb_altera_core_s_altera_rdfifo_rd_cnt(8),
      LO => mb_altera_core_o_DBus_8_mux000212_2438
    );
  mb_altera_core_o_DBus_6_mux00024 : LUT4_L
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => mb_altera_core_s_dma_num_samples(4),
      I1 => mb_altera_core_o_DBus_10_cmp_eq0007,
      I2 => mb_altera_core_o_DBus_10_cmp_eq0004,
      I3 => mb_altera_core_s_dma_base_addr(4),
      LO => mb_altera_core_o_DBus_6_mux00024_2421
    );
  mb_altera_core_o_DBus_6_mux000222 : LUT4_L
    generic map(
      INIT => X"A820"
    )
    port map (
      I0 => mb_altera_core_N41,
      I1 => inst_emif_iface_addr_r(1),
      I2 => mb_altera_core_s_dma_start_addr(4),
      I3 => mb_altera_core_s_altera_rdfifo_rd_cnt(6),
      LO => mb_altera_core_o_DBus_6_mux000222_2419
    );
  mb_altera_core_o_DBus_0_mux000227 : LUT4_L
    generic map(
      INIT => X"FFEC"
    )
    port map (
      I0 => mb_altera_core_Read_Fifo_Data_in(0),
      I1 => mb_altera_core_o_DBus_0_mux000215_2242,
      I2 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      I3 => mb_altera_core_o_DBus_0_mux00022_2243,
      LO => mb_altera_core_o_DBus_0_mux000227_2244
    );
  mb_altera_core_o_DBus_0_mux000265 : LUT3_L
    generic map(
      INIT => X"08"
    )
    port map (
      I0 => mb_altera_core_N40,
      I1 => inst_emif_iface_addr_r(2),
      I2 => i_eth_def_IBUF_506,
      LO => mb_altera_core_o_DBus_0_mux000265_2246
    );
  mb_altera_core_o_DBus_3_mux000069 : LUT4_L
    generic map(
      INIT => X"CA00"
    )
    port map (
      I0 => mb_altera_core_s_dma_start_addr(1),
      I1 => mb_altera_core_s_altera_rdfifo_rd_cnt(3),
      I2 => inst_emif_iface_addr_r(1),
      I3 => mb_altera_core_N41,
      LO => mb_altera_core_o_DBus_3_mux000069_2397
    );
  mb_altera_core_o_DBus_2_mux000222 : LUT4_L
    generic map(
      INIT => X"EAC0"
    )
    port map (
      I0 => mb_altera_core_Read_Fifo_Data_in(2),
      I1 => mb_altera_core_o_DBus_2_mux000213_2372,
      I2 => mb_altera_core_N12,
      I3 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      LO => mb_altera_core_o_DBus_2_mux000222_2373
    );
  mb_altera_core_o_DBus_2_mux0002110 : LUT4_L
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => mb_altera_core_o_DBus_10_cmp_eq0004,
      I1 => mb_altera_core_s_dma_base_addr(0),
      I2 => mb_altera_core_o_DBus_10_cmp_eq0005,
      I3 => mb_altera_core_s_dma_space_mask(2),
      LO => mb_altera_core_o_DBus_2_mux0002110_2370
    );
  inst_emif_iface_sdram_dma_std_dma_engine_N4211 : LUT4_D
    generic map(
      INIT => X"8CEE"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(4),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      LO => N776,
      O => inst_emif_iface_sdram_dma_std_dma_engine_N421
    );
  mb_altera_core_Write_Fifo_Data_in_10_or000011 : LUT3_D
    generic map(
      INIT => X"04"
    )
    port map (
      I0 => inst_emif_iface_addr_r(1),
      I1 => inst_emif_iface_addr_r(0),
      I2 => inst_emif_iface_addr_r(2),
      LO => N777,
      O => mb_altera_core_N12
    );
  mb_altera_core_Write_Fifo_Data_in_10_and000111 : LUT3_D
    generic map(
      INIT => X"10"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => inst_emif_iface_addr_r(0),
      I2 => inst_emif_iface_addr_r(3),
      LO => N778,
      O => mb_altera_core_N13
    );
  inst_base_module_wd_counter_mux0000_0_11 : LUT4_D
    generic map(
      INIT => X"FFF6"
    )
    port map (
      I0 => inst_base_module_wd_kick_r2_938,
      I1 => inst_base_module_wd_kick_r1_937,
      I2 => inst_base_module_wd_rst_not0002_inv,
      I3 => inst_base_module_wd_counter_cmp_eq0000,
      LO => N779,
      O => inst_base_module_N1
    );
  inst_base_module_bank_addr_0_and0000122 : LUT2_L
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => inst_base_module_bank_addr_0_and0000110_608,
      I1 => inst_base_module_bank_addr_0_and0000121_609,
      LO => inst_base_module_N3
    );
  inst_base_module_wd_counter_cmp_eq000012 : LUT4_L
    generic map(
      INIT => X"0001"
    )
    port map (
      I0 => inst_base_module_wd_counter(0),
      I1 => inst_base_module_wd_counter(1),
      I2 => inst_base_module_wd_counter(2),
      I3 => inst_base_module_wd_counter(3),
      LO => inst_base_module_wd_counter_cmp_eq000012_914
    );
  mb_altera_core_o_DBus_13_or00001 : LUT3_D
    generic map(
      INIT => X"F8"
    )
    port map (
      I0 => inst_emif_iface_addr_r(1),
      I1 => mb_altera_core_N41,
      I2 => mb_altera_core_o_DBus_10_cmp_eq0007,
      LO => N780,
      O => mb_altera_core_o_DBus_13_or0000
    );
  mb_altera_core_o_DBus_10_cmp_eq00071 : LUT3_D
    generic map(
      INIT => X"20"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => inst_emif_iface_addr_r(2),
      I2 => mb_altera_core_N40,
      LO => N781,
      O => mb_altera_core_o_DBus_10_cmp_eq0007
    );
  mb_altera_core_o_DBus_0_mux000232 : LUT3_D
    generic map(
      INIT => X"10"
    )
    port map (
      I0 => inst_emif_iface_addr_r(0),
      I1 => inst_emif_iface_addr_r(3),
      I2 => inst_emif_iface_addr_r(1),
      LO => N782,
      O => mb_altera_core_N40
    );
  mb_altera_core_Msub_s_dma_count_share0000_cy_3_11 : LUT4_D
    generic map(
      INIT => X"FFFE"
    )
    port map (
      I0 => mb_altera_core_s_altera_rdfifo_rd_cnt(1),
      I1 => mb_altera_core_Msub_s_dma_count_share0000_cy_0_Q,
      I2 => mb_altera_core_s_altera_rdfifo_rd_cnt(2),
      I3 => mb_altera_core_s_altera_rdfifo_rd_cnt(3),
      LO => N783,
      O => mb_altera_core_Msub_s_dma_count_share0000_cy_3_Q
    );
  mb_altera_core_Madd_s_dma_num_samples_addsub0000_cy_3_11 : LUT4_D
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => mb_altera_core_s_dma_num_samples(3),
      I1 => mb_altera_core_s_dma_num_samples(2),
      I2 => mb_altera_core_s_dma_num_samples(1),
      I3 => mb_altera_core_s_dma_num_samples(0),
      LO => N784,
      O => mb_altera_core_Madd_s_dma_num_samples_addsub0000_cy_3_Q
    );
  inst_base_module_o_DBus_cmp_eq00061 : LUT4_D
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => inst_emif_iface_addr_r(0),
      I1 => inst_emif_iface_addr_r(1),
      I2 => inst_emif_iface_addr_r(2),
      I3 => inst_base_module_irq_mask_and0000,
      LO => N785,
      O => inst_base_module_o_DBus_cmp_eq0006
    );
  mb_altera_core_s_dma_num_samples_mux0000_1_SW0 : LUT3_L
    generic map(
      INIT => X"7F"
    )
    port map (
      I0 => mb_altera_core_N8,
      I1 => mb_altera_core_s_dma_num_samples(7),
      I2 => mb_altera_core_Madd_s_dma_num_samples_addsub0000_cy_6_Q,
      LO => N256
    );
  inst_base_module_o_DBus_mux0002_1_0 : LUT2_L
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => inst_base_module_irq_mask(1),
      I1 => inst_base_module_o_DBus_cmp_eq0004,
      LO => inst_base_module_o_DBus_mux0002_1_0_847
    );
  mb_altera_core_o_DBus_15_mux000033 : LUT4_L
    generic map(
      INIT => X"EAC0"
    )
    port map (
      I0 => mb_altera_core_s_dma_space_mask(15),
      I1 => mb_altera_core_o_DBus_15_mux000024_2282,
      I2 => mb_altera_core_N12,
      I3 => mb_altera_core_o_DBus_10_cmp_eq0005,
      LO => mb_altera_core_o_DBus_15_mux000033_2283
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_7_31 : LUT4_L
    generic map(
      INIT => X"CC4C"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(0),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(4),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_6_27,
      LO => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_7_31_1468
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_17_7_SW0 : LUT2_L
    generic map(
      INIT => X"8"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(3),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row(2),
      LO => N286
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_15_3 : LUT2_L
    generic map(
      INIT => X"4"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(7),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N47,
      LO => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_15_3_1382
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_17_11 : LUT3_D
    generic map(
      INIT => X"7F"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(4),
      LO => N786,
      O => inst_emif_iface_sdram_dma_std_dma_engine_N3
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_17_4 : LUT4_D
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(1),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row(0),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I3 => N290,
      LO => N787,
      O => inst_emif_iface_sdram_dma_std_dma_engine_N33
    );
  mb_altera_core_Write_fifo_wr_Cnt_and000037 : LUT4_L
    generic map(
      INIT => X"7FFF"
    )
    port map (
      I0 => mb_altera_core_Write_fifo_wr_Cnt(6),
      I1 => mb_altera_core_Write_fifo_wr_Cnt(7),
      I2 => mb_altera_core_Write_fifo_wr_Cnt(8),
      I3 => mb_altera_core_Write_fifo_wr_Cnt(9),
      LO => mb_altera_core_Write_fifo_wr_Cnt_and000037_2177
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_4_18 : LUT4_D
    generic map(
      INIT => X"7FFF"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_col(3),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_col(2),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_col(1),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_col(0),
      LO => N788,
      O => inst_emif_iface_sdram_dma_std_dma_engine_N9
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_6_1 : LUT4_D
    generic map(
      INIT => X"FF7F"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_col(7),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_col(6),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_col(5),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_5_3_1253,
      LO => N789,
      O => inst_emif_iface_sdram_dma_std_dma_engine_N0
    );
  mb_altera_core_s_dma_start_addr_mux0000_10_11 : LUT4_D
    generic map(
      INIT => X"F2A2"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I1 => mb_altera_core_s_dma_irq_clear_2664,
      I2 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I3 => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(30),
      LO => N790,
      O => mb_altera_core_N1
    );
  mb_altera_core_s_dma_space_addr_mux0000_10_11 : LUT4_D
    generic map(
      INIT => X"6E4C"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_altera_rdfifo_rd_en,
      I3 => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(30),
      LO => N791,
      O => mb_altera_core_N0
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_and00001_SW0_SW0 : LUT3_L
    generic map(
      INIT => X"80"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row(9),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row(8),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_row(7),
      LO => N541
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_17_91 : LUT4_L
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => N543,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N47,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_row(1),
      I3 => N290,
      LO => inst_emif_iface_sdram_dma_std_dma_engine_N60
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_2_SW0 : LUT4_L
    generic map(
      INIT => X"8000"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(5),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(3),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(4),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N35,
      LO => N301
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_0_20 : LUT4_L
    generic map(
      INIT => X"CCC4"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(3),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_0_12_1449,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N30,
      LO => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_0_20_1450
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_3_SW0 : LUT4_L
    generic map(
      INIT => X"F070"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(3),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(4),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N30,
      LO => N299
    );
  mb_altera_core_s_dma_count_mux0003_0_SW0_SW0 : LUT3_L
    generic map(
      INIT => X"C9"
    )
    port map (
      I0 => mb_altera_core_Msub_s_dma_count_share0000_cy_6_Q,
      I1 => mb_altera_core_s_altera_rdfifo_rd_cnt(8),
      I2 => mb_altera_core_s_altera_rdfifo_rd_cnt(7),
      LO => N556
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_5_1 : LUT4_D
    generic map(
      INIT => X"3777"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I1 => N562,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(4),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N0,
      LO => N792,
      O => inst_emif_iface_sdram_dma_std_dma_engine_N30
    );
  mb_altera_core_s_dma_count_mux0003_0_3 : LUT4_D
    generic map(
      INIT => X"1110"
    )
    port map (
      I0 => mb_altera_core_Mcompar_s_dma_state_cmp_gt0000_cy(3),
      I1 => N275,
      I2 => mb_altera_core_s_dma_state_cmp_ge0001,
      I3 => mb_altera_core_s_altera_int_busy_2551,
      LO => N793,
      O => mb_altera_core_N10
    );
  inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_In641_SW0 : LUT4_L
    generic map(
      INIT => X"2A7F"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(4),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N0,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N17,
      LO => N564
    );
  mb_altera_core_o_DBus_1_mux0002921_SW0 : LUT4_L
    generic map(
      INIT => X"F888"
    )
    port map (
      I0 => mb_altera_core_o_DBus_10_cmp_eq0003_2250,
      I1 => mb_altera_core_Read_Fifo_Data_in(1),
      I2 => mb_altera_core_o_DBus_1_mux000213_2307,
      I3 => mb_altera_core_N12,
      LO => N566
    );
  mb_altera_core_s_dma_state_FSM_FFd1_In8 : LUT3_L
    generic map(
      INIT => X"54"
    )
    port map (
      I0 => mb_altera_core_Mcompar_s_dma_state_cmp_gt0000_cy(3),
      I1 => mb_altera_core_s_altera_int_busy_2551,
      I2 => mb_altera_core_s_dma_state_cmp_ge0001,
      LO => mb_altera_core_s_dma_state_FSM_FFd1_In8_2875
    );
  inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_17 : LUT4_L
    generic map(
      INIT => X"2000"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_col(1),
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_6_1248,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N51,
      LO => inst_emif_iface_sdram_dma_std_dma_engine_col_mux0000_3_17_1245
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_4_SW2 : LUT4_L
    generic map(
      INIT => X"FAF2"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_word_count(3),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N421,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N30,
      LO => N580
    );
  mb_altera_core_o_DBus_27_mux0000721_SW0 : LUT4_L
    generic map(
      INIT => X"FFEA"
    )
    port map (
      I0 => mb_altera_core_o_DBus_27_mux000045_2356,
      I1 => mb_altera_core_o_DBus_10_cmp_eq0005,
      I2 => mb_altera_core_s_dma_space_mask(27),
      I3 => mb_altera_core_o_DBus_27_mux000033_2355,
      LO => N588
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_10_21 : LUT3_D
    generic map(
      INIT => X"2A"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row(10),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N36,
      LO => N794,
      O => inst_emif_iface_sdram_dma_std_dma_engine_N52
    );
  inst_emif_iface_sdram_dma_std_dma_engine_N4311 : LUT4_D
    generic map(
      INIT => X"7FEE"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_Mcompar_sm_sdram_cmp_ne0001_cy(4),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      LO => N795,
      O => inst_emif_iface_sdram_dma_std_dma_engine_N431
    );
  inst_base_module_key_state_cmp_eq000334 : LUT4_D
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => inst_base_module_bank_addr_0_and0000110_608,
      I1 => inst_base_module_bank_addr_0_and0000121_609,
      I2 => inst_base_module_key_state_cmp_eq000321_780,
      I3 => N612,
      LO => N796,
      O => inst_base_module_key_state_cmp_eq0003
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_6_210 : LUT4_D
    generic map(
      INIT => X"5F4E"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N9,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I3 => N622,
      LO => N797,
      O => inst_emif_iface_sdram_dma_std_dma_engine_N16
    );
  inst_base_module_key_state_cmp_eq00021 : LUT4_D
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => inst_base_module_irq_mask_and0000,
      I1 => inst_emif_iface_addr_r(0),
      I2 => inst_emif_iface_addr_r(2),
      I3 => inst_emif_iface_addr_r(1),
      LO => N798,
      O => inst_base_module_key_state_cmp_eq0002
    );
  mb_altera_core_s_dma_num_samples_mux0000_0_11 : LUT4_D
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_dma_rd_stb_1302,
      I1 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I2 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_dma_done_1301,
      LO => N799,
      O => mb_altera_core_N8
    );
  inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_19_26 : LUT4_L
    generic map(
      INIT => X"A2AA"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row(10),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N3,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N36,
      LO => inst_emif_iface_sdram_dma_std_dma_engine_bank_mux0000_19_26_1219
    );
  inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_6_44 : LUT4_L
    generic map(
      INIT => X"FED4"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd2_1424,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd3_1428,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_sm_sdram_FSM_FFd1_1422,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_6_30_1465,
      LO => inst_emif_iface_sdram_dma_std_dma_engine_word_count_mux0000_6_44_1466
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_11_36 : LUT4_L
    generic map(
      INIT => X"FF2A"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_11_25_1366,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row(10),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N36,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_N431,
      LO => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_11_36_1367
    );
  mb_altera_core_o_DBus_30_mux00008 : LUT4_L
    generic map(
      INIT => X"3320"
    )
    port map (
      I0 => mb_altera_core_s_dma_start_addr(28),
      I1 => inst_emif_iface_addr_r(1),
      I2 => mb_altera_core_N41,
      I3 => N632,
      LO => mb_altera_core_o_DBus_30_mux00008_2384
    );
  mb_altera_core_o_DBus_28_mux000032_SW0 : LUT4_D
    generic map(
      INIT => X"FFF7"
    )
    port map (
      I0 => inst_emif_iface_addr_r(4),
      I1 => inst_emif_iface_addr_r(0),
      I2 => inst_emif_iface_addr_r(2),
      I3 => inst_emif_iface_addr_r(1),
      LO => N800,
      O => N640
    );
  mb_altera_core_o_DBus_15_mux000048_SW0 : LUT4_L
    generic map(
      INIT => X"0080"
    )
    port map (
      I0 => mb_altera_core_Read_Fifo_Data_in(15),
      I1 => inst_emif_iface_addr_r(3),
      I2 => inst_emif_iface_addr_r(4),
      I3 => N241,
      LO => N680
    );
  inst_base_module_irq_mask_mux0000_0_2_SW1 : LUT3_L
    generic map(
      INIT => X"FE"
    )
    port map (
      I0 => inst_emif_iface_addr_r(3),
      I1 => inst_emif_iface_addr_r(4),
      I2 => inst_emif_iface_edi_r(31),
      LO => N688
    );
  mb_altera_core_o_DBus_3_mux00001501_SW0 : LUT4_L
    generic map(
      INIT => X"FF32"
    )
    port map (
      I0 => mb_altera_core_o_DBus_3_mux0000109_2392,
      I1 => inst_emif_iface_addr_r(2),
      I2 => mb_altera_core_o_DBus_3_mux0000115_2393,
      I3 => N692,
      LO => N574
    );
  inst_base_module_o_DBus_cmp_eq00011 : LUT4_D
    generic map(
      INIT => X"0008"
    )
    port map (
      I0 => inst_emif_iface_addr_r(1),
      I1 => inst_base_module_irq_mask_and0000,
      I2 => inst_emif_iface_addr_r(2),
      I3 => inst_emif_iface_addr_r(0),
      LO => N801,
      O => inst_base_module_o_DBus_cmp_eq0001
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_17_63_SW0 : LUT4_L
    generic map(
      INIT => X"3F2A"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_N24,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_N36,
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_row(10),
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_17_39_1390,
      LO => N700
    );
  inst_emif_iface_sdram_dma_std_dma_engine_row_mux0001_14_421_SW0 : LUT4_L
    generic map(
      INIT => X"51F3"
    )
    port map (
      I0 => inst_emif_iface_sdram_dma_std_dma_engine_N36,
      I1 => inst_emif_iface_sdram_dma_std_dma_engine_row(5),
      I2 => inst_emif_iface_sdram_dma_std_dma_engine_N24,
      I3 => inst_emif_iface_sdram_dma_std_dma_engine_row(10),
      LO => N704
    );
  mb_altera_core_s_dma_space_addr_mux0000_10_11_1 : LUT4_D
    generic map(
      INIT => X"6E4C"
    )
    port map (
      I0 => mb_altera_core_s_dma_state_FSM_FFd2_2876,
      I1 => mb_altera_core_s_dma_state_FSM_FFd1_2871,
      I2 => mb_altera_core_s_altera_rdfifo_rd_en,
      I3 => mb_altera_core_Mcompar_s_dma_start_addr_cmp_gt0000_cy(30),
      LO => N802,
      O => mb_altera_core_s_dma_space_addr_mux0000_10_11_2727
    );
  inst_emif_iface_o_ce_n_3_1 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => Mtrien_io_altera_dsp_mux0000_norst,
      Q => inst_emif_iface_o_ce_n_3_1_1158
    );
  inst_emif_iface_o_ce_n_3_2 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => Mtrien_io_altera_dsp_mux0000_norst,
      Q => inst_emif_iface_o_ce_n_3_2_1159
    );
  inst_base_module_Mshreg_div10_5_10 : SRL16E
    generic map(
      INIT => X"0001"
    )
    port map (
      A0 => ea(15),
      A1 => ea(15),
      A2 => ea(15),
      A3 => Mtrien_io_altera_dsp_mux0000_norst,
      CE => inst_base_module_div10_5_and0000,
      CLK => o_eth_ref_clk_OBUF_3050,
      D => inst_base_module_div10_5(10),
      Q => inst_base_module_Mshreg_div10_5_10_532
    );
  inst_base_module_div10_5_10 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      CE => inst_base_module_div10_5_and0000,
      D => inst_base_module_Mshreg_div10_5_10_532,
      Q => inst_base_module_div10_5(10)
    );
  inst_emif_iface_Mshreg_o_ce_n_0 : SRL16
    generic map(
      INIT => X"0001"
    )
    port map (
      A0 => ea(15),
      A1 => ea(15),
      A2 => ea(15),
      A3 => ea(15),
      CLK => emif_clk,
      D => inst_emif_iface_sdram_dma_std_dma_engine_ce_n_mux0002(3),
      Q => inst_emif_iface_Mshreg_o_ce_n_0_986
    );
  inst_emif_iface_o_ce_n_0 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => emif_clk,
      D => inst_emif_iface_Mshreg_o_ce_n_0_986,
      Q => inst_emif_iface_o_ce_n_0_Q
    );
  inst_base_module_Mshreg_div10_4_10 : SRL16E
    generic map(
      INIT => X"0001"
    )
    port map (
      A0 => ea(15),
      A1 => ea(15),
      A2 => ea(15),
      A3 => Mtrien_io_altera_dsp_mux0000_norst,
      CE => inst_base_module_div10_4_and0000,
      CLK => o_eth_ref_clk_OBUF_3050,
      D => inst_base_module_div10_4(10),
      Q => inst_base_module_Mshreg_div10_4_10_531
    );
  inst_base_module_div10_4_10 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      CE => inst_base_module_div10_4_and0000,
      D => inst_base_module_Mshreg_div10_4_10_531,
      Q => inst_base_module_div10_4(10)
    );
  inst_base_module_Mshreg_dcm_reset_shift_15 : SRL16E
    generic map(
      INIT => X"7FFF"
    )
    port map (
      A0 => ea(15),
      A1 => Mtrien_io_altera_dsp_mux0000_norst,
      A2 => Mtrien_io_altera_dsp_mux0000_norst,
      A3 => Mtrien_io_altera_dsp_mux0000_norst,
      CE => inst_base_module_dcm_reset_shift_or0000,
      CLK => o_eth_ref_clk_OBUF_3050,
      D => inst_base_module_dcm_reset_shift(15),
      Q => inst_base_module_Mshreg_dcm_reset_shift_15_527
    );
  inst_base_module_dcm_reset_shift_15 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      CE => inst_base_module_dcm_reset_shift_or0000,
      D => inst_base_module_Mshreg_dcm_reset_shift_15_527,
      Q => inst_base_module_dcm_reset_shift(15)
    );
  inst_base_module_Mshreg_div10_3_10 : SRL16E
    generic map(
      INIT => X"0001"
    )
    port map (
      A0 => ea(15),
      A1 => ea(15),
      A2 => ea(15),
      A3 => Mtrien_io_altera_dsp_mux0000_norst,
      CE => inst_base_module_div10_3_and0000,
      CLK => o_eth_ref_clk_OBUF_3050,
      D => inst_base_module_div10_3(10),
      Q => inst_base_module_Mshreg_div10_3_10_530
    );
  inst_base_module_div10_3_10 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      CE => inst_base_module_div10_3_and0000,
      D => inst_base_module_Mshreg_div10_3_10_530,
      Q => inst_base_module_div10_3(10)
    );
  inst_base_module_Mshreg_div10_2_10 : SRL16E
    generic map(
      INIT => X"0001"
    )
    port map (
      A0 => ea(15),
      A1 => ea(15),
      A2 => ea(15),
      A3 => Mtrien_io_altera_dsp_mux0000_norst,
      CE => inst_base_module_div10_2_and0000,
      CLK => o_eth_ref_clk_OBUF_3050,
      D => inst_base_module_div10_2(10),
      Q => inst_base_module_Mshreg_div10_2_10_529
    );
  inst_base_module_div10_2_10 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      CE => inst_base_module_div10_2_and0000,
      D => inst_base_module_Mshreg_div10_2_10_529,
      Q => inst_base_module_div10_2(10)
    );
  inst_base_module_Mshreg_div10_1_10 : SRL16
    generic map(
      INIT => X"0000"
    )
    port map (
      A0 => Mtrien_io_altera_dsp_mux0000_norst,
      A1 => Mtrien_io_altera_dsp_mux0000_norst,
      A2 => Mtrien_io_altera_dsp_mux0000_norst,
      A3 => ea(15),
      CLK => o_eth_ref_clk_OBUF_3050,
      D => inst_base_module_div10_1_1_Q,
      Q => inst_base_module_Mshreg_div10_1_10_528
    );
  inst_base_module_div10_1_10 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      D => inst_base_module_Mshreg_div10_1_10_528,
      Q => inst_base_module_div10_1_10_Q
    );
  inst_base_module_Mshreg_div5_2_5 : SRL16E
    generic map(
      INIT => X"0001"
    )
    port map (
      A0 => Mtrien_io_altera_dsp_mux0000_norst,
      A1 => Mtrien_io_altera_dsp_mux0000_norst,
      A2 => ea(15),
      A3 => ea(15),
      CE => inst_base_module_div5_2_and0000,
      CLK => o_eth_ref_clk_OBUF_3050,
      D => inst_base_module_div5_2(5),
      Q => inst_base_module_Mshreg_div5_2_5_534
    );
  inst_base_module_div5_2_5 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      CE => inst_base_module_div5_2_and0000,
      D => inst_base_module_Mshreg_div5_2_5_534,
      Q => inst_base_module_div5_2(5)
    );
  inst_base_module_Mshreg_div5_1_5 : SRL16E
    generic map(
      INIT => X"0001"
    )
    port map (
      A0 => Mtrien_io_altera_dsp_mux0000_norst,
      A1 => Mtrien_io_altera_dsp_mux0000_norst,
      A2 => ea(15),
      A3 => ea(15),
      CE => inst_base_module_div5_1_and0000,
      CLK => o_eth_ref_clk_OBUF_3050,
      D => inst_base_module_div5_1(5),
      Q => inst_base_module_Mshreg_div5_1_5_533
    );
  inst_base_module_div5_1_5 : FDE
    generic map(
      INIT => '0'
    )
    port map (
      C => o_eth_ref_clk_OBUF_3050,
      CE => inst_base_module_div5_1_and0000,
      D => inst_base_module_Mshreg_div5_1_5_533,
      Q => inst_base_module_div5_1(5)
    );
  mb_altera_core_Altera_ReadFifo_Inst : Altera_ReadFifo
    port map (
      rd_en => mb_altera_core_s_altera_rdfifo_rd_en,
      rst => mb_altera_core_s_altera_fifo_rst_2549,
      empty => mb_altera_core_s_altera_rdfifo_empty,
      wr_en => mb_altera_core_s_altera_rdfifo_wr_en_2610,
      rd_clk => emif_clk,
      valid => mb_altera_core_s_altera_rdfifo_valid,
      full => o_altera_read_ff_ff_OBUF_3033,
      wr_clk => o_altera_Clk_OBUF_3025,
      dout(31) => dma_data(0, 31),
      dout(30) => dma_data(0, 30),
      dout(29) => dma_data(0, 29),
      dout(28) => dma_data(0, 28),
      dout(27) => dma_data(0, 27),
      dout(26) => dma_data(0, 26),
      dout(25) => dma_data(0, 25),
      dout(24) => dma_data(0, 24),
      dout(23) => dma_data(0, 23),
      dout(22) => dma_data(0, 22),
      dout(21) => dma_data(0, 21),
      dout(20) => dma_data(0, 20),
      dout(19) => dma_data(0, 19),
      dout(18) => dma_data(0, 18),
      dout(17) => dma_data(0, 17),
      dout(16) => dma_data(0, 16),
      dout(15) => dma_data(0, 15),
      dout(14) => dma_data(0, 14),
      dout(13) => dma_data(0, 13),
      dout(12) => dma_data(0, 12),
      dout(11) => dma_data(0, 11),
      dout(10) => dma_data(0, 10),
      dout(9) => dma_data(0, 9),
      dout(8) => dma_data(0, 8),
      dout(7) => dma_data(0, 7),
      dout(6) => dma_data(0, 6),
      dout(5) => dma_data(0, 5),
      dout(4) => dma_data(0, 4),
      dout(3) => dma_data(0, 3),
      dout(2) => dma_data(0, 2),
      dout(1) => dma_data(0, 1),
      dout(0) => dma_data(0, 0),
      rd_data_count(12) => mb_altera_core_s_altera_rdfifo_rd_cnt(12),
      rd_data_count(11) => mb_altera_core_s_altera_rdfifo_rd_cnt(11),
      rd_data_count(10) => mb_altera_core_s_altera_rdfifo_rd_cnt(10),
      rd_data_count(9) => mb_altera_core_s_altera_rdfifo_rd_cnt(9),
      rd_data_count(8) => mb_altera_core_s_altera_rdfifo_rd_cnt(8),
      rd_data_count(7) => mb_altera_core_s_altera_rdfifo_rd_cnt(7),
      rd_data_count(6) => mb_altera_core_s_altera_rdfifo_rd_cnt(6),
      rd_data_count(5) => mb_altera_core_s_altera_rdfifo_rd_cnt(5),
      rd_data_count(4) => mb_altera_core_s_altera_rdfifo_rd_cnt(4),
      rd_data_count(3) => mb_altera_core_s_altera_rdfifo_rd_cnt(3),
      rd_data_count(2) => mb_altera_core_s_altera_rdfifo_rd_cnt(2),
      rd_data_count(1) => mb_altera_core_s_altera_rdfifo_rd_cnt(1),
      rd_data_count(0) => mb_altera_core_Msub_s_dma_count_share0000_cy_0_Q,
      din(31) => mb_altera_core_s_altera_rdfifo_din(31),
      din(30) => mb_altera_core_s_altera_rdfifo_din(30),
      din(29) => mb_altera_core_s_altera_rdfifo_din(29),
      din(28) => mb_altera_core_s_altera_rdfifo_din(28),
      din(27) => mb_altera_core_s_altera_rdfifo_din(27),
      din(26) => mb_altera_core_s_altera_rdfifo_din(26),
      din(25) => mb_altera_core_s_altera_rdfifo_din(25),
      din(24) => mb_altera_core_s_altera_rdfifo_din(24),
      din(23) => mb_altera_core_s_altera_rdfifo_din(23),
      din(22) => mb_altera_core_s_altera_rdfifo_din(22),
      din(21) => mb_altera_core_s_altera_rdfifo_din(21),
      din(20) => mb_altera_core_s_altera_rdfifo_din(20),
      din(19) => mb_altera_core_s_altera_rdfifo_din(19),
      din(18) => mb_altera_core_s_altera_rdfifo_din(18),
      din(17) => mb_altera_core_s_altera_rdfifo_din(17),
      din(16) => mb_altera_core_s_altera_rdfifo_din(16),
      din(15) => mb_altera_core_s_altera_rdfifo_din(15),
      din(14) => mb_altera_core_s_altera_rdfifo_din(14),
      din(13) => mb_altera_core_s_altera_rdfifo_din(13),
      din(12) => mb_altera_core_s_altera_rdfifo_din(12),
      din(11) => mb_altera_core_s_altera_rdfifo_din(11),
      din(10) => mb_altera_core_s_altera_rdfifo_din(10),
      din(9) => mb_altera_core_s_altera_rdfifo_din(9),
      din(8) => mb_altera_core_s_altera_rdfifo_din(8),
      din(7) => mb_altera_core_s_altera_rdfifo_din(7),
      din(6) => mb_altera_core_s_altera_rdfifo_din(6),
      din(5) => mb_altera_core_s_altera_rdfifo_din(5),
      din(4) => mb_altera_core_s_altera_rdfifo_din(4),
      din(3) => mb_altera_core_s_altera_rdfifo_din(3),
      din(2) => mb_altera_core_s_altera_rdfifo_din(2),
      din(1) => mb_altera_core_s_altera_rdfifo_din(1),
      din(0) => mb_altera_core_s_altera_rdfifo_din(0)
    );
  mb_altera_core_Altera_WriteFifo_Inst : Altera_WriteFifo
    port map (
      rd_en => mb_altera_core_s_altera_write_busy_2612,
      wr_en => mb_altera_core_Write_Fifo_Write_En_2128,
      full => mb_altera_core_Write_Fifo_ff,
      empty => mb_altera_core_Write_Fifo_ef,
      wr_clk => emif_clk,
      rst => mb_altera_core_s_altera_fifo_rst_2549,
      rd_clk => o_altera_Clk_OBUF_3025,
      dout(32) => mb_altera_core_Write_Fifo_data_out(32),
      dout(31) => mb_altera_core_Write_Fifo_data_out(31),
      dout(30) => mb_altera_core_Write_Fifo_data_out(30),
      dout(29) => mb_altera_core_Write_Fifo_data_out(29),
      dout(28) => mb_altera_core_Write_Fifo_data_out(28),
      dout(27) => mb_altera_core_Write_Fifo_data_out(27),
      dout(26) => mb_altera_core_Write_Fifo_data_out(26),
      dout(25) => mb_altera_core_Write_Fifo_data_out(25),
      dout(24) => mb_altera_core_Write_Fifo_data_out(24),
      dout(23) => mb_altera_core_Write_Fifo_data_out(23),
      dout(22) => mb_altera_core_Write_Fifo_data_out(22),
      dout(21) => mb_altera_core_Write_Fifo_data_out(21),
      dout(20) => mb_altera_core_Write_Fifo_data_out(20),
      dout(19) => mb_altera_core_Write_Fifo_data_out(19),
      dout(18) => mb_altera_core_Write_Fifo_data_out(18),
      dout(17) => mb_altera_core_Write_Fifo_data_out(17),
      dout(16) => mb_altera_core_Write_Fifo_data_out(16),
      dout(15) => mb_altera_core_Write_Fifo_data_out(15),
      dout(14) => mb_altera_core_Write_Fifo_data_out(14),
      dout(13) => mb_altera_core_Write_Fifo_data_out(13),
      dout(12) => mb_altera_core_Write_Fifo_data_out(12),
      dout(11) => mb_altera_core_Write_Fifo_data_out(11),
      dout(10) => mb_altera_core_Write_Fifo_data_out(10),
      dout(9) => mb_altera_core_Write_Fifo_data_out(9),
      dout(8) => mb_altera_core_Write_Fifo_data_out(8),
      dout(7) => mb_altera_core_Write_Fifo_data_out(7),
      dout(6) => mb_altera_core_Write_Fifo_data_out(6),
      dout(5) => mb_altera_core_Write_Fifo_data_out(5),
      dout(4) => mb_altera_core_Write_Fifo_data_out(4),
      dout(3) => mb_altera_core_Write_Fifo_data_out(3),
      dout(2) => mb_altera_core_Write_Fifo_data_out(2),
      dout(1) => mb_altera_core_Write_Fifo_data_out(1),
      dout(0) => mb_altera_core_Write_Fifo_data_out(0),
      din(32) => mb_altera_core_Write_Fifo_Data_in(32),
      din(31) => mb_altera_core_Write_Fifo_Data_in(31),
      din(30) => mb_altera_core_Write_Fifo_Data_in(30),
      din(29) => mb_altera_core_Write_Fifo_Data_in(29),
      din(28) => mb_altera_core_Write_Fifo_Data_in(28),
      din(27) => mb_altera_core_Write_Fifo_Data_in(27),
      din(26) => mb_altera_core_Write_Fifo_Data_in(26),
      din(25) => mb_altera_core_Write_Fifo_Data_in(25),
      din(24) => mb_altera_core_Write_Fifo_Data_in(24),
      din(23) => mb_altera_core_Write_Fifo_Data_in(23),
      din(22) => mb_altera_core_Write_Fifo_Data_in(22),
      din(21) => mb_altera_core_Write_Fifo_Data_in(21),
      din(20) => mb_altera_core_Write_Fifo_Data_in(20),
      din(19) => mb_altera_core_Write_Fifo_Data_in(19),
      din(18) => mb_altera_core_Write_Fifo_Data_in(18),
      din(17) => mb_altera_core_Write_Fifo_Data_in(17),
      din(16) => mb_altera_core_Write_Fifo_Data_in(16),
      din(15) => mb_altera_core_Write_Fifo_Data_in(15),
      din(14) => mb_altera_core_Write_Fifo_Data_in(14),
      din(13) => mb_altera_core_Write_Fifo_Data_in(13),
      din(12) => mb_altera_core_Write_Fifo_Data_in(12),
      din(11) => mb_altera_core_Write_Fifo_Data_in(11),
      din(10) => mb_altera_core_Write_Fifo_Data_in(10),
      din(9) => mb_altera_core_Write_Fifo_Data_in(9),
      din(8) => mb_altera_core_Write_Fifo_Data_in(8),
      din(7) => mb_altera_core_Write_Fifo_Data_in(7),
      din(6) => mb_altera_core_Write_Fifo_Data_in(6),
      din(5) => mb_altera_core_Write_Fifo_Data_in(5),
      din(4) => mb_altera_core_Write_Fifo_Data_in(4),
      din(3) => mb_altera_core_Write_Fifo_Data_in(3),
      din(2) => mb_altera_core_Write_Fifo_Data_in(2),
      din(1) => mb_altera_core_Write_Fifo_Data_in(1),
      din(0) => mb_altera_core_Write_Fifo_Data_in(0),
      wr_data_count(9) => NLW_mb_altera_core_Altera_WriteFifo_Inst_wr_data_count_9_UNCONNECTED,
      wr_data_count(8) => NLW_mb_altera_core_Altera_WriteFifo_Inst_wr_data_count_8_UNCONNECTED,
      wr_data_count(7) => NLW_mb_altera_core_Altera_WriteFifo_Inst_wr_data_count_7_UNCONNECTED,
      wr_data_count(6) => NLW_mb_altera_core_Altera_WriteFifo_Inst_wr_data_count_6_UNCONNECTED,
      wr_data_count(5) => NLW_mb_altera_core_Altera_WriteFifo_Inst_wr_data_count_5_UNCONNECTED,
      wr_data_count(4) => NLW_mb_altera_core_Altera_WriteFifo_Inst_wr_data_count_4_UNCONNECTED,
      wr_data_count(3) => NLW_mb_altera_core_Altera_WriteFifo_Inst_wr_data_count_3_UNCONNECTED,
      wr_data_count(2) => NLW_mb_altera_core_Altera_WriteFifo_Inst_wr_data_count_2_UNCONNECTED,
      wr_data_count(1) => NLW_mb_altera_core_Altera_WriteFifo_Inst_wr_data_count_1_UNCONNECTED,
      wr_data_count(0) => NLW_mb_altera_core_Altera_WriteFifo_Inst_wr_data_count_0_UNCONNECTED
    );

end Structure;

-- synthesis translate_on
