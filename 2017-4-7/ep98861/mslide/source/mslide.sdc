## Generated SDC file "AD5453.sdc"

## Copyright (C) 1991-2010 Altera Corporation
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, Altera MegaCore Function License 
## Agreement, or other applicable license agreement, including, 
## without limitation, that your use is for the sole purpose of 
## programming logic devices manufactured by Altera and sold by 
## Altera or its authorized distributors.  Please refer to the 
## applicable agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 10.0 Build 218 06/27/2010 SJ Full Version"

## DATE    "Wed Sep 22 08:19:27 2010"

##
## DEVICE  "EP3C40U484C7"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {CLK50}  -period 20.000 -waveform { 0.000 10.000 } [get_ports {CLK50}]

#**************************************************************
# Create Generated Clock
#**************************************************************



#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************

derive_clock_uncertainty


#**************************************************************
# Set Input Delay
#**************************************************************



set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {Enable}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {Enable}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {MSlide_New_Pos}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {MSlide_New_Pos}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {Auto_Retract_Dis}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {Auto_Retract_Dis}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {ms1_tc}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {ms1_tc}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {Det_Sat}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {Det_Sat}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {Vel_Max[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {Vel_Max[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {Vel_Min[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {Vel_Min[*]}]

# set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {Accel[*]}]
# set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {Accel[*]}]

# set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {DeAccel[*]}]
# set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {DeAccel[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {Tar_Pos[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {Tar_Pos[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {CPS[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {CPS[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {HC_RATE[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {HC_RATE[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {HC_THRESHOLD[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {HC_THRESHOLD[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {CMD_STB[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {CMD_STB[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {CMD_WD}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {CMD_WD}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {CMD_RETRACT}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {CMD_RETRACT}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {Enc[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {Enc[*]}]

#**************************************************************
# Set Output Delay
#**************************************************************
		
set_output_delay -clock { CLK50 } -max 5.000 [get_ports {Move_Enable}]
set_output_delay -clock { CLK50 } -min 3.000 [get_ports {Move_Enable}]

set_output_delay -clock { CLK50 } -max 5.000 [get_ports {Home_Status}]
set_output_delay -clock { CLK50 } -min 3.000 [get_ports {Home_Status}]

set_output_delay -clock { CLK50 } -max 5.000 [get_ports {Move_Time[*]}]
set_output_delay -clock { CLK50 } -min 3.000 [get_ports {Move_Time[*]}]

set_output_delay -clock { CLK50 } -max 5.000 [get_ports {MSlide_Dir}]
set_output_delay -clock { CLK50 } -min 3.000 [get_ports {MSlide_Dir}]

set_output_delay -clock { CLK50 } -max 5.000 [get_ports {MSlide_Clk}]
set_output_delay -clock { CLK50 } -min 3.000 [get_ports {MSlide_Clk}]

set_output_delay -clock { CLK50 } -max 5.000 [get_ports {MSlide_En}]
set_output_delay -clock { CLK50 } -min 3.000 [get_ports {MSlide_En}]

set_output_delay -clock { CLK50 } -max 5.000 [get_ports {MSlide_Pos[*]}]
set_output_delay -clock { CLK50 } -min 3.000 [get_ports {MSlide_Pos[*]}]

set_output_delay -clock { CLK50 } -max 5.000 [get_ports {Analyze_Pos[*]}]
set_output_delay -clock { CLK50 } -min 3.000 [get_ports {Analyze_Pos[*]}]

set_output_delay -clock { CLK50 } -max 5.000 [get_ports {MSlide_State[*]}]
set_output_delay -clock { CLK50 } -min 3.000 [get_ports {MSlide_State[*]}]

#**************************************************************
# Set Clock Groups
#**************************************************************



#**************************************************************
# Set False Path
#**************************************************************



#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

