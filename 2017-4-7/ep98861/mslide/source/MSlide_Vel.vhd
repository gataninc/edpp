----------------------------------------------------------------------------------------------
-- 	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	Artemis.vhd
----------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : September 21, 2007
--   Board          : XScope Instrument Control Board
--   Schematic      : 4035.007.27000 Rev D & E ( Same but different layer order )
--
--   Top Level      : 4035.009.99330, S/W, XScope, Instrument Control
--
--   Description:	
--
--   History:       <date> - <Author> - Ver
--		09062502: 
-----------------------------------------------------------------------------------------------

library LPM;
     use lpm.lpm_components.all;
library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;
----------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------
entity MSlide_Vel is
     port(
		CLK50			: in		std_logic;
		Vel_Clk			: in		std_logic;
		Startup			: in		std_logic;
		Enable			: in		std_logic;
		Move_Analyze		: in		std_logic;
		Move_Slow			: in		std_Logic;
		Vel_Max			: in		std_logic_Vector( 15 downto 0 );		-- Slide Velocity
		Vel_Min			: in		std_logic_Vector( 15 downto 0 );		-- Slide Velocity
		Vel_Out			: buffer	std_Logic_vector( 15 downto 0 ) );
end MSlide_Vel;
----------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------
architecture behavioral of MSlide_Vel is
	constant Vel_Startup	: integer := -8;

	signal Neg_Vel_Max		: std_logic_Vector( 15 downto 0 );
	signal Neg_Vel_Min		: std_logic_Vector( 15 downto 0 );

begin
	----------------------------------------------------------------------------------------------
	-- Negatve Vel_Max Calculation
	Neg_Vel_Max_Inst : lpm_add_Sub
		generic map(
			LPM_WIDTH			=> 16,
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_DIRECTION		=> "SUB",
			LPM_PIPELINE		=> 1 )
		port map(
			clock			=> CLK50,
			Cin				=> '1',
			dataa			=> (others=>'0'),
			datab			=> Vel_Max,
			result			=> Neg_Vel_Max );
	-- Negatve Vel_Max Calculation
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Negatve Vel_Max Calculation
	Neg_Vel_Min_Inst : lpm_add_Sub
		generic map(
			LPM_WIDTH			=> 16,
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_DIRECTION		=> "SUB",
			LPM_PIPELINE		=> 1 )
		port map(
			clock			=> CLK50,
			Cin				=> '1',
			dataa			=> (others=>'0'),
			datab			=> Vel_Min,
			result			=> Neg_Vel_Min );
	-- Negatve Vel_Max Calculation
	----------------------------------------------------------------------------------------------
	
	----------------------------------------------------------------------------------------------
	clock_proc : process( CLK50 )
	begin
		if rising_edge( CLK50 ) then
			if( Enable = '0' )
				then Vel_Out <= (others=>'0');
			
			elsif( Startup = '1' )
				then Vel_Out <= conv_std_logic_Vector( Vel_Startup, 16 );
				
			elsif(( Move_Analyze = '1' ) and ( Move_Slow = '1' ))
				then Vel_Out	<= Vel_Min;
				
			elsif(( Move_Analyze = '1' ) and ( Move_Slow = '0' ))
				then Vel_Out	<= Vel_Max;
				
			elsif(( Move_Analyze = '0' ) and ( Move_Slow = '1' ))
				then Vel_Out	<= Neg_Vel_Min;
				
			elsif(( Move_Analyze = '0' ) and ( Move_Slow = '0' ))
				then Vel_Out	<= Neg_Vel_Max;
			end if;
		end if;
	end process;
	----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------
end behavioral;			-- mslide.vhd
----------------------------------------------------------------------------------------------




