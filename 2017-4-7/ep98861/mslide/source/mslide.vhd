----------------------------------------------------------------------------------------------
-- 	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	Artemis.vhd
----------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : September 21, 2007
--   Board          : XScope Instrument Control Board
--   Schematic      : 4035.007.27000 Rev D & E ( Same but different layer order )
--
--   Top Level      : 4035.009.98861, S/W, Artemis, DPP Board
--
--   Description:	
--
--   History:       <date> - <Author> - Ver
--		07/12/11 Rev 0.1 - MCS
--			Added AutoRetractTargetPosition to 250um
--			Added Retract Detection to < 500um
--		06/13/11 Rev 0.1 - MCS
--			Module: MSLide_IMS.vhd
--				Gated off IMS_EN and IMS_CLK when MOVE_ENABLE = 0
--		05/16/11 - Noise at Startup
--			Module: MSlide.Vhd 
--				Changed kDecellStartPos from 5mm to 10mm
--				In ST_MOVE_START changed MODE_SLOW <= '1' to a dependencey of "MOVE_SLOW_ENC"--		11040810: MSLide
--			Clean-up with AutoRetract/Startup and "CLICKING"
--		110309xx:
--			Added to MSlide_Enc an "Enable" to allow movement. When a new position is entered, it must be greater than 100um
--			from the current position to start a movement. This will prevent the slide trying to move to the same position
--			causing potential viberations to the column.
--		11030701:
--			Added three state LATCH_DET_SAT, LATCH_WDOG, LATCH_HI_CNT
--			Fixed a bug in the AutoRetract High Count Retraction Logic
--		10123001: 
--			Changed Delay_Cnt_Max from 100 to 3000 (4sec at 1ms)
--			Added a Wait for DELAY_CNT_MAX from Move_Done
-----------------------------------------------------------------------------------------------

library LPM;
     use lpm.lpm_components.all;
library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;
----------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------
entity mslide is
	generic(
		STB_SLIDE_STOP		: integer := 7;
		STB_SLIDE_MR		: integer := 8;
		STB_SLIDE_FA		: integer := 10;
		STB_SLIDE_FR		: integer := 11 );
     port(
		CLK50			: in		std_logic;
		Enable			: in		std_logic;
		MSlide_New_Pos		: in		std_logic;
		Auto_Retract_Dis	: in		std_logic;
		ms1_tc			: in		std_logic;
		Det_Sat			: in		std_logic;
		Vel_Max			: in		std_logic_Vector( 31 downto 0 );		-- Slide Velocity
		Vel_Min			: in		std_logic_Vector( 31 downto 0 );		-- Slide Velocity
		Tar_Pos			: in		std_logic_Vector( 31 downto 0 );		-- 100% Cnt Value
		CPS				: in		std_logic_Vector( 31 downto 0 );		-- 100% Cnt Value
		HC_RATE			: in		std_logic_Vector( 31 downto 0 );		-- 100% Cnt Value
		HC_THRESHOLD	: in		std_logic_Vector( 31 downto 0 );		-- 100% Cnt Value		
		CMD_STB			: in		std_logic_Vector( 31 downto 0 );
		CMD_WD			: in		std_logic;
		CMD_RETRACT		: in		std_logic;						-- Retract due to other meansf
		Enc				: in		std_logic_Vector( 1 downto 0 );
		Move_Enable		: buffer	std_logic;						-- Movement in progress
		Home_Status		: buffer	std_logic;
		Move_Time			: buffer	std_logic_Vector( 31 downto 0 );
		MSlide_Dir		: buffer	std_logic;						-- Direction
		MSlide_Clk		: buffer	std_logic;						-- Steps
		MSlide_En			: buffer	std_logic;
		MSlide_Pos		: buffer	std_logic_Vector( 31 downto 0 );
		Analyze_Pos		: buffer	std_logic_Vector( 31 downto 0 );
		MSlide_State   	: buffer	std_logic_vector(  4 downto 0 )); 		-- RTEM State
end mslide;
----------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------
architecture behavioral of mslide is
	-- Constant Declarations ---------------------------------------------------------------------
	constant CLK_PER				: integer := 20;					-- 20 ns period
	constant PIV_CLK_CNT_MAX			: integer := ( 1000000 / CLK_PER )-1;	-- 1 ms
	constant Delay_Cnt_Max			: integer := 1000;					-- 3 sec @ 1 ms

	
	-- Lead Screw pitch is 0.05" (or 1/20)" and 25.4mm/in conversion factor
	-- Motor as a 500 line encoder in quadrature (resulting in 2000 lines)
	-- Lead Screw pitch is 8 Rev/Inch
	constant kPitch				: integer := 8;						-- 8 Rev / Inch
	constant kEncoder				: integer := 4* 1000;					-- 1000 Line Encoder
	constant kRetractPos			: integer := 500;						-- 500 micron position - to detect retracted
	constant kAutoRetractTargetPos	: integer := 250;						-- 250 micron position auto-retract target position
	constant kDecellStartPos			: integer := 10000;						-- 10mm position
--	constant kDecellStartPos			: integer := 5000;						-- 5mm position
	constant kMoveEnable			: integer := 100;						-- 100 micron position
	
	constant mmRetract				: integer := ( kRetractPos * kPitch * kEncoder ) / 25400;	-- Calculate Encoder Count for "AutoRetract Position
	constant mmAutoRetract			: integer := ( kAutoRetractTargetPos * kPitch * kEncoder ) / 25400;	-- Calculate Encoder Count for "AutoRetract Position
	constant mmDecell				: integer := ( kDecellStartPos * kPitch * kEncoder ) / 25400;	-- Calculate Encoder Count for "AutoRetract Position
	constant mmMoveEnable			: integer := ( kMoveEnable 	 * kPitch * kEncoder ) / 25400;	-- Calculate Encoder Count for "AutoRetract Position

	-- State machine State declarations
	constant ST_Disabled			: integer := 0;
	constant ST_Idle				: integer := 1;
	constant St_Home_Start			: integer := 2;
	constant St_Home_Retract_Wait		: integer := 3;
	constant St_Home_Clr			: integer := 4;	
	constant St_Find_Analyze_Start	: integer := 5;
	constant St_Find_Analyze_Wait		: integer := 6;
	constant St_Find_Analyze_Latch	: integer := 7;
	constant St_Move_Start			: integer := 8;
	constant St_Move_Wait			: integer := 9;
	constant St_Move_Done			: integer := 10;
	constant St_AutoRetract_Start		: integer := 11;
	constant St_AutoRetract_Wait		: integer := 12;
	constant St_AutoRetract_Done		: integer := 13;
	constant St_Stalled				: integer := 14;	
	constant St_HiCountDetected		: integer := 15;
	constant St_Latch_Det_Sat		: integer := 16;
	constant St_Latch_WDog			: integer := 17;
	constant St_Latch_HC			: integer := 18;
	-- Constant Declarations ---------------------------------------------------------------------

     -- Signal Declarations -----------------------------------------------------------------------     
	
	signal Cmd_Home_Latch			: std_logic := '0';
	signal Cmd_Analyze_Latch			: std_logic := '0';
	signal Cmd_AutoRetract_Latch		: std_logic := '0';
	signal Current_Vel				: std_logic_Vector( 15 downto 0 );

	signal Delay_Cnt				: integer range 0 to Delay_Cnt_Max;

	signal Enc_Stalled				: std_logic;
		
	signal Home_Clr				: std_logic;
	signal Home_Stop				: std_logic;
	signal HC_Threshold_Cntr			: std_logic_vector( 31 downto 0 ) := (others=>'0');
	signal HC_CPS_Detected 			: std_logic;
	signal HC_THR_Detected			: std_logic;
	
	signal Int_Enable				: std_logic;

	signal Latch_Det_Sat			: std_logic;
	signal Latch_WDog				: std_logic;
	signal Latch_HC				: std_logic;
	
	signal Move_Analyze				: std_logic;
	signal Move_Cnt				: std_logic_Vector( 31 downto 0 );
	signal Move_Slow				: std_logic;
	signal Move_Slow_Enc			: std_logic;
	signal Move_Stop				: std_logic;
	signal Move_Stalled				: std_logic;
	signal MSlide_New_Pos_Vec		: std_logic_Vector( 3 downto 0 );
	signal MSlide_Retracted 			: std_logic;			
	
	signal New_Pos_Enable			: std_logic;


	signal PIV_CLK_CNT				: integer range 0 to PIV_CLK_CNT_MAX;
	signal PIV_CLK					: std_Logic;

	signal Pos_Changed				: std_logic := '0';
	signal ST_MSlide				: integer range 0 to St_Latch_HC := 0;
	signal ST_MSlide_Del			: integer range 0 to St_Latch_HC := 0;
	signal Startup					: std_logic := '1';

	signal Tar_Pos_Mux				: std_logic_Vector( 31 downto 0 );

	signal Vel_Tar					: std_logic_Vector( 15 downto 0 );
	---------------------------------------------------------------------------------------------------
	component MSlide_Enc is 
		generic(
			mmDecell			: in		integer;
			mmMoveEnable		: in		integer );
		port( 
			CLK50      	 	: in      std_logic;                         	-- Master Clock
			PIV_CLK			: in		std_Logic;
			MSlide_Clk		: in		std_logic;
			MSlide_Mr			: in		std_Logic;
			Enc_Clr 			: in		std_logic;
			ENC_Ch			: in		std_logic_vector( 1 downto 0 );		-- A and B Phases
			Move_Enable		: in		std_logic;
			Tar_Pos			: in		std_logic_vector( 31 downto 0 );
			Stalled			: buffer	std_logic;
			Move_Stop 		: buffer	std_logic;
			Move_Slow 		: buffer	std_logic;		
			New_Pos_Enable		: buffer	std_logic;
			Enc_Cnt			: buffer	std_logic_vector( 31 downto 0 );
			Vel				: buffer	std_logic_Vector( 15 downto 0 ) );
	end component MSlide_Enc;
	---------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------
	component MSlide_IMS is 
		port( 
			CLK50			: in		std_logic;					-- Master Clock
			Move_Enable		: in		std_Logic;
			Enc_Stalled		: in		std_logic;
			Vel_Clk			: in		std_logic;
			Vel_In			: in		std_logic_Vector( 15 downto 0 );
			Vel_Tar			: in		std_logic_Vector( 15 downto 0 );
			IMS_CLK			: buffer	std_logic;
			IMS_DIR			: buffer	std_logic;
			Move_Stalled		: buffer	std_logic );
	end component MSlide_IMS;
	---------------------------------------------------------------------------------------------------	
	
	---------------------------------------------------------------------------------------------------	
	component MSlide_Vel is
		port(
			CLK50			: in		std_logic;			
			Vel_Clk			: in		std_logic;
			Startup			: in		std_logic;
			Enable			: in		std_logic;
			Move_Analyze		: in		std_logic;
			Move_Slow			: in		std_Logic;
			Vel_Max			: in		std_logic_Vector( 15 downto 0 );		-- Slide Velocity
			Vel_Min			: in		std_logic_Vector( 15 downto 0 );		-- Slide Velocity
			Vel_Out			: buffer	std_Logic_vector( 15 downto 0 ) );
	end component MSlide_Vel;
	---------------------------------------------------------------------------------------------------	
	
begin
	MSlide_En <= Enable;
	---------------------------------------------------------------------------------------------------
	-- "Listen to the Encoder and generate the position accordingly. Also have the ability to clear the 
	-- position and detect a "Stalled" condition and determine the velocity (every MSlide_Clk )
	Home_Clr <= '1' when (( ST_MSlide = St_Home_Clr ) and ( ST_MSlide_Del /= St_Home_Clr )) else '0';

	MSlide_Enc_Inst : MSlide_Enc
		generic map(
			mmDecell			=> mmDecell,
			mmMoveEnable		=> mmMoveEnable )
		port map(
			CLK50      	 	=> CLK50,
			PIV_CLK			=> PIV_CLK,
			MSlide_Clk		=> MSlide_Clk,				-- Step Clock - used for Stall Detection
			MSlide_Mr			=> CMD_STB( STB_SLIDE_MR ),
			Enc_Clr 			=> Home_Clr,				-- Reset Encoder to 0
			ENC_Ch			=> Enc,					-- Encoder Inputs
			Move_Enable		=> Move_Enable,			-- Move Enable ( used to gate-off Stalled)
			Tar_Pos			=> Tar_Pos_Mux,
			Stalled			=> Enc_Stalled,			-- Stall Detected
			Move_Stop 		=> Move_Stop,
			Move_Slow 		=> Move_Slow_Enc,
			New_Pos_Enable		=> New_Pos_Enable,
			Enc_Cnt			=> MSlide_Pos,				-- Current Slide or Encoder Position 
			Vel				=> Current_Vel );			-- Currenet Slide Velocity 	
	---------------------------------------------------------------------------------------------------

	---------------------------------------------------------------------------------------------------	
	MSlide_Vel_Inst : MSlide_Vel
		port map(
			CLK50			=> CLK50,
			Vel_Clk			=> PIV_Clk,
			Startup			=> Startup,
			Enable			=> Move_Enable,
			Move_Analyze		=> Move_Analyze,			-- Direction
			Move_Slow			=> Move_Slow,				-- Use Low Speed
			Vel_Max			=> Vel_Max( 15 downto 0 ),	-- MAximum Velocity
			Vel_Min			=> Vel_Min( 15 downto 0 ),	-- Minimum Velocity
			Vel_Out			=> Vel_Tar );				-- Target Velocity
	---------------------------------------------------------------------------------------------------	
	
	---------------------------------------------------------------------------------------------------
	-- Take a "Motor" Command (Signed 10 bit value) and generate a step clock and direction
	-- to the motor						   
	MSlide_IMS_Inst : MSlide_IMS
		port map(
			CLK50			=> CLK50,
			Move_Enable		=> Move_Enable,
			Enc_Stalled		=> Enc_Stalled,
			Vel_Clk			=> PIV_Clk,
			Vel_In			=> Current_Vel,			-- Current Velocity
			Vel_Tar			=> Vel_Tar,				-- Target Velocity
			IMS_CLK			=> MSlide_Clk,
			IMS_DIR			=> MSlide_Dir,
			Move_Stalled		=> Move_Stalled );
				
	---------------------------------------------------------------------------------------------------

	---------------------------------------------------------------------------------------------------
	-- Compare input count rate with the input count rate threhold limit
	Hi_CPS_Cmp_Inst : lpm_compare
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "UNSIGNED",
			LPM_PIPELINE		=> 1 )
		port map(
			clock			=> clk50,
			dataa			=> CPS,
			datab			=> HC_Rate,
			agb			=> HC_CPS_Detected );
	-- Compare input count rate with the input count rate threhold limit
	---------------------------------------------------------------------------------------------------

	---------------------------------------------------------------------------------------------------
	-- Compare how long the input count rate (HC_Threshold_Cntr) has been active against the threhold
	Hi_Threshold_Cmp_Inst : lpm_compare
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "UNSIGNED",
			LPM_PIPELINE		=> 1 )
		port map(
			clock			=> clk50,
			dataa			=> HC_Threshold_Cntr,
			datab			=> HC_Threshold,
			ageb			=> HC_THR_Detected );
	-- Compare how long the input count rate (HC_Threshold_Cntr) has been active against the threhold
	---------------------------------------------------------------------------------------------------

	mSlide_Retracted_Compare : lpm_compare
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_PIPELINE		=> 1 )
		port map(
			clock			=> clk50,
			dataa 			=> MSlide_Pos,
			datab			=> conv_std_logic_vector( mmRetract, 32 ),
			aleb				=> MSlide_Retracted );

	
	---------------------------------------------------------------------------------------------------
	clk_proc : process( CLK50 )
	begin
		if rising_edge( CLK50 ) then
			ST_MSlide_Del	<= St_MSlide;
			if(( Startup = '1' ) or ( Enable = '1' ))
				then Int_Enable <= '1';
				else Int_Enable <= '0';
			end if;
		
			if( St_MSlide = St_Home_Clr )
				then Startup <= '0';
			end if;
		
			---------------------------------------------------------------------------------------------------------------------------------
			-- AutoRetraction Logic 
			-- if CPS < HiCountRate - reset threshold counter
			if( HC_CPS_Detected = '0' )
				then HC_Threshold_Cntr <= (others=>'0');
			
			elsif( Auto_Retract_Dis = '1' )
				then HC_Threshold_Cntr <= (others=>'0');			
			
			-- otherwise, increment threshold counter with every msec
			elsif(( HC_CPS_Detected = '1' ) and ( ms1_tc = '1' ) and ( HC_Threshold_Cntr /=  x"FFFFFFFF" ))
				then HC_Threshold_Cntr <= HC_Threshold_Cntr + 1;
			end if;
			-- High Count Rate Detection -------------------------------------------------------
			
		
			---------------------------------------------------------------------------------------------------------------------------------
			-- Generate a Timer (1Khz) for the PIV Clock 
			if( PIV_CLK_CNT = PIV_CLK_CNT_MAX ) then
				PIV_CLK		<= '1';
				PIV_CLK_CNT 	<= 0;
			else 
				PIV_CLK		<= '0';
				PIV_CLK_CNT 	<= PIV_CLK_CNT + 1;
			end if;
			-- Generate a Timer (40Khz) for the PIV Clock 
			---------------------------------------------------------------------------------------------------------------------------------
			
			---------------------------------------------------------------------------------------------------------------------------------
			-- Sense Change in Target Position - To move to the new position
			if(( Int_Enable = '0' ) or ( CMD_STB( STB_SLIDE_MR ) = '1' ) or ( CMD_STB( STB_SLIDE_STOP ) = '1' ))
				then Pos_Changed <= '0';
			elsif(( Auto_Retract_Dis = '0' ) and ( HC_THR_Detected = '1' ))
				then Pos_Changed <= '0';
			elsif(( Home_Status = '1' ) and ( New_Pos_Enable = '1' ) and ( MSlide_New_Pos_Vec(3) = '1' ))
				then Pos_Changed <= '1';
			elsif( ST_MSlide = St_Move_Done )
				then Pos_Changed <= '0';
			end if;
			-- Sense Change in Target Position - To move to the new position
			---------------------------------------------------------------------------------------------------------------------------------			

			MSlide_New_Pos_Vec <= MSlide_New_Pos_Vec( 2 downto 0 ) & MSlide_New_Pos;
			---------------------------------------------------------------------------------------------------------------------------------
			-- Home Command Latch - to move to the retract hard-stop and zero the position counter
			if(( CMD_STB( STB_SLIDE_MR ) = '1' ) or ( CMD_STB( STB_SLIDE_STOP ) = '1' ))
				then Cmd_Home_Latch <= '0';
			elsif( CMD_STB( STB_SLIDE_FR ) = '1' )
				then Cmd_Home_Latch <= '1';
			elsif( ST_MSlide = St_Home_Start )
				then Cmd_Home_Latch <= '0';
			end if;			
			---------------------------------------------------------------------------------------------------------------------------------
			
			---------------------------------------------------------------------------------------------------------------------------------
			if(( Int_Enable = '0' ) or ( CMD_STB( STB_SLIDE_MR ) = '1' ) or ( CMD_STB( STB_SLIDE_STOP ) = '1' ))
				then Cmd_Analyze_Latch <= '0';
			elsif(( Auto_Retract_Dis = '0' ) and ( HC_THR_Detected = '1' ))
				then Cmd_Analyze_Latch <= '0';
			elsif(( Home_Status = '1' ) and ( CMD_STB( STB_SLIDE_FA ) = '1' ))
				then Cmd_Analyze_Latch <= '1';
			elsif( St_MSlide = St_Find_Analyze_Start )
				then Cmd_Analyze_Latch <= '0';
			end if;
			---------------------------------------------------------------------------------------------------------------------------------

			---------------------------------------------------------------------------------------------------------------------------------
			-- AutoRetraction Logic and what caused it
			if(( Int_Enable = '0' ) or ( CMD_STB( STB_SLIDE_MR ) = '1' ) or ( CMD_STB( STB_SLIDE_STOP ) = '1' ) or ( Auto_Retract_Dis = '1' )) then
				Cmd_AutoRetract_Latch 	<= '0';
				Latch_Det_Sat 			<= '0';
				Latch_WDog 			<= '0';
				Latch_HC 				<= '0';
				
			elsif(( Home_Status = '1' ) and ( MSlide_Retracted = '1' )) then 
				Cmd_AutoRetract_LAtch 	<= '0';				
				
			elsif(( Det_Sat = '1' ) and ( Home_Status = '1' ) and ( MSlide_Retracted = '0' )) then	-- Not Retracted
				Cmd_AutoRetract_Latch 	<= '1';
				Latch_Det_Sat			<= '1';
				
			elsif(( Cmd_WD = '1' ) and ( Home_Status = '1' )and ( MSlide_Retracted = '0' ))	then -- Not Retracted
				Cmd_AutoRetract_Latch 	<= '1';
				Latch_WDog			<= '1';
				
			elsif(( ms1_tc = '1' ) and ( HC_THR_Detected = '1' ) and ( Home_Status = '1' )and ( MSlide_Retracted = '0' ))	then -- Not Retracted
				Cmd_AutoRetract_Latch 	<= '1';
				Latch_HC				<= '1';
				
			elsif( St_MSlide = St_AutoRetract_Start ) then
				Cmd_AutoRetract_Latch <= '0';
				
			elsif( St_MSlide = St_Latch_Det_Sat )
				then Latch_Det_Sat <= '0';

			elsif( St_MSlide = St_Latch_WDog )				
				then Latch_WDog <= '0';
				
			elsif( St_MSlide = St_Latch_HC )
				then Latch_HC <= '0';
				
			end if;
			-- AutoRetraction Logic and what caused it
			---------------------------------------------------------------------------------------------------------------------------------

			---------------------------------------------------------------------------------------------------------------------------------
			-- Home Status (1 = Homed)
			if(( Int_Enable = '1' ) and ( CMD_STB( STB_SLIDE_FR ) = '1' ))
				then Home_Status <= '0';								-- Not Homed
			elsif( St_MSlide = St_Home_Clr )
				then Home_Status <= '1';								-- Has been homed
			end if;
			---------------------------------------------------------------------------------------------------------------------------------					
			
			---------------------------------------------------------------------------------------------------------------------------------
			-- Determine how long a movement takes
			if(( St_MSlide = St_Move_Start ) or ( St_MSlide = St_AutoRetract_Start ))
				then Move_Cnt <= (others=>'0');
			elsif(( Move_Enable = '1' ) and ( ms1_tc = '1' ))
				then Move_Cnt <= Move_Cnt + 1;
			end if;
			-- Determine how long a movement takes
			---------------------------------------------------------------------------------------------------------------------------------
			
			---------------------------------------------------------------------------------------------------------------------------------
			-- Motorized Slide State Machine ---------------------------------------------------
			MSlide_State( 4 downto 0 ) 	<= conv_Std_logic_Vector( ST_MSlide, 5 ); -- State String Output 
			case ST_MSlide is
				-- Slide is Disabled -------------------------------------------------------------------------------------------------------
				when ST_Disabled =>
					Move_Enable 	<= '0';
					Delay_Cnt		<= 0;

					if( Int_Enable = '1' )
						then ST_MSlide <= St_Idle;
					end if;
				-- Slide is Disabled -------------------------------------------------------------------------------------------------------
					
				-- Slide is Idle -----------------------------------------------------------------------------------------------------------
				when St_Idle =>				
					Move_Enable 	<= '0';
					Delay_Cnt		<= 0;
					Tar_Pos_Mux 	<= Tar_Pos;					
					
					if( Int_Enable = '0' )
						then ST_MSlide <= ST_Disabled;						

					elsif(( Cmd_Home_Latch = '1' ) or ( Startup = '1' ))				-- Find the Retract Hard-Stop
						then ST_MSlide <= St_Home_Start;
															
					elsif(( Cmd_AutoRetract_Latch = '1' ) and ( Home_Status = '1' ) and ( MSlide_Retracted = '0' ))	-- Not retracted					
						then St_MSlide <= St_AutoRetract_Start;						

					elsif(( Auto_Retract_Dis = '0' ) and ( HC_THR_Detected = '1' ) and ( Home_Status = '1' ) and ( MSlide_Retracted = '0' )) -- Not Retracted
						then ST_MSlide <= St_AutoRetract_Start;						
						
					elsif( Latch_Det_Sat = '1' )
						then St_MSlide <= St_Latch_Det_Sat;
						
					elsif( Latch_WDog = '1' )
						then St_MSlide <= St_latch_WDog;
					
					elsif( Latch_HC = '1' )
						then St_MSlide <= St_Latch_HC;					
												
					elsif(( Auto_Retract_Dis = '0' ) and ( HC_THR_Detected = '1' ) and ( Home_Status = '1' ) and ( MSlide_Retracted = '1' )) -- Already Retracted
						then ST_MSlide <= St_HiCountDetected;						
															
					elsif( Cmd_Analyze_Latch = '1' )								-- Find the Analyze Hard-Stop
						then St_MSlide <= St_Find_Analyze_Start;
						
					elsif( Pos_Changed = '1' ) 
						then St_MSlide <= St_Move_Start;
					end if;
				-- Slide is Idle -----------------------------------------------------------------------------------------------------------

					
				-- HiCounts Detected -------------------------------------------------------------------------------------------------------
				when St_HiCountDetected =>
					Move_Enable 	<= '0';					
					Delay_Cnt		<= 0;
					Tar_Pos_Mux 	<= conv_std_logic_vector( mmAutoRetract, 32 );					
					
					if( Int_Enable = '0' )
						then ST_MSlide <= ST_Disabled;

					elsif(( Auto_Retract_Dis = '1' ) or ( HC_THR_Detected = '0' ) or ( Cmd_Home_Latch = '1' ) or ( Startup = '1' ))
						then St_MSlide <= St_Idle;											
					
--					elsif( MSlide_Pos > conv_std_logic_vector( mmAutoRetract, 32 ) )	-- Not retracted
--						then St_MSlide <= St_AutoRetract_Start;
						
					end if;					
				-- HiCounts Detected -------------------------------------------------------------------------------------------------------
					
				--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				-- Home the Slide - Retract, wait for hard-stop and finish
				-- Start moving, wait for the "Encoder Stalled Flag to be in-active" (Must be TIME based)
				when St_Home_Start =>
					Move_Enable 	<= '1';
					Move_Slow		<= '1';
					Move_Analyze	<= '0';

					if( St_MSlide_Del /= St_Home_Start )
						then Delay_Cnt <= 0;			
						else Delay_Cnt <= Delay_Cnt + 1;
					end if;
					
					if( Int_Enable = '0' )
						then ST_MSlide <= ST_Disabled;
					
					elsif( CMD_STB( STB_Slide_Stop ) = '1' )
						then ST_MSlide <= St_Idle;
						
					elsif( Delay_Cnt = Delay_Cnt_Max )
						then St_MSlide <= St_Home_Retract_Wait;
					end if;
				
				when St_Home_Retract_Wait =>
					Move_Enable 	<= '1';
					Move_Slow		<= '1';
					Move_Analyze	<= '0';
					Delay_Cnt		<= 0;

					if( Int_Enable = '0' )
						then ST_MSlide <= ST_Disabled;
						
					elsif( CMD_STB( STB_Slide_Stop ) = '1' )					
						then ST_MSlide <= St_Idle;
					
					elsif( Enc_Stalled = '1' ) 
						then ST_MSlide <= St_Home_Clr;
					end if;
				-- Slide is Moving until it hits the retract hard-stop Idle ----------------------------------------------------------------
				
				-- Slide is At the Retract Hardstop - Clear the Counter --------------------------------------------------------------------
				when St_Home_Clr =>
					Move_Enable 	<= '0';
					Move_Slow		<= '1';
					Move_Analyze	<= '0';
					
					if( St_MSlide_Del /= St_Home_Clr )
						then Delay_Cnt <= 0;			
					elsif( ms1_tc = '1' )
						then Delay_Cnt <= Delay_Cnt + 1;
					end if;					
					
					
					if( Int_Enable = '0' )
						then ST_MSlide <= ST_Disabled;

					elsif(( ms1_tc = '1' ) and ( Delay_Cnt = Delay_Cnt_Max ))
						then St_MSlide 	<= St_Idle;
					end if;					
				-- Home the Slide - Retract, wait for hard-stop and finish
				--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////				
				
				--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				-- Find Analyze Position Hard-Stop
				when St_Find_Analyze_Start	=>
					Move_Enable 	<= '1';
					Move_Slow		<= '1';
					Move_Analyze	<= '1';
					
					if( St_MSlide_Del /= St_Find_Analyze_Start )
						then Delay_Cnt <= 0;			
						else Delay_Cnt <= Delay_Cnt + 1;
					end if;					
					
					if( Int_Enable = '0' )
						then ST_MSlide <= ST_Disabled;
						
					elsif( CMD_STB( STB_Slide_Stop ) = '1' )
						then ST_MSlide <= St_Idle;
						
					elsif( Delay_Cnt = Delay_Cnt_Max )
						then St_MSlide <= St_Find_Analyze_Wait;
					end if;
				
				when St_Find_Analyze_Wait =>
					Move_Enable 	<= '1';
					Move_Slow		<= '1';
					Move_Analyze	<= '1';
					Delay_Cnt		<= 0;
					
					if( Int_Enable = '0' )
						then ST_MSlide <= ST_Disabled;
						
					elsif( CMD_STB( STB_Slide_Stop ) = '1' )
						then ST_MSlide <= St_Idle;
						
					elsif( Enc_Stalled = '1' )
						then St_MSlide <= St_Find_Analyze_Latch;
					end if;
					
				when St_Find_Analyze_Latch	=>
					Move_Enable 	<= '0';
					Move_Slow		<= '1';
					Move_Analyze	<= '1';
					Analyze_Pos 	<= MSlide_Pos;
					
					if( St_MSlide_Del /= St_Find_Analyze_Latch )
						then Delay_Cnt <= 0;			
					elsif( ms1_tc = '1' )
						then Delay_Cnt <= Delay_Cnt + 1;
					end if;					

					
					if( Int_Enable = '0' )
						then ST_MSlide <= ST_Disabled;
					elsif(( ms1_tc = '1' ) and ( Delay_Cnt = Delay_Cnt_Max ))
						then St_MSlide 	<= St_Idle;
					end if;										
				-- Find Analyze Position Hard-Stop
				--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				-- Start of Movement, 1st Calculate the Movement profile BEFORE Starting any movement				
				when St_Move_Start =>
					Move_Enable	<= '1';
					Tar_Pos_Mux 	<= Tar_Pos;					
					
					if( St_MSlide_Del /= St_Move_Start )
						then Delay_Cnt <= 0;			
						else Delay_Cnt <= Delay_Cnt + 1;
					end if;					
									
					if( Tar_Pos > MSlide_Pos )
						then Move_Analyze <= '1';
						else Move_Analyze <= '0';
					end if;
					
					if( Move_Slow_Enc = '1' )
						then Move_Slow <= '1';
						else Move_Slow <= '0';
					end if;
					
													
					if( Int_Enable = '0' )
						then ST_MSlide <= ST_Disabled;
						
					elsif( CMD_STB( STB_Slide_Stop ) = '1' )
						then ST_MSlide <= St_Idle;

					elsif( Delay_Cnt = Delay_Cnt_Max )
						then St_MSlide <= St_Move_Wait;
					end if;						
					
				when St_Move_Wait =>
					if( Move_Stop = '1' ) 
						then Move_Enable	<= '0';
						else Move_Enable	<= '1';						
					end if;
					Delay_Cnt 	<= 0;
					
					if( Move_Stop = '1' )
						then Move_Slow	<= '1';
					elsif( Move_Slow_Enc = '1' )
						then Move_Slow <= '1';
						else Move_Slow <= '0';
					end if;
					
					if( Int_Enable = '0' )
						then ST_MSlide <= ST_Disabled;
						
					elsif( CMD_STB( STB_Slide_Stop ) = '1' )
						then ST_MSlide <= St_Idle;
						
					elsif( Move_Stalled = '1' ) 
						then ST_MSlide <= St_Stalled;
												
					elsif( Move_Stop = '1' )
						then St_MSlide	<= St_Move_Done;
					end if;					
					
				when St_Move_Done =>
					Move_Enable	<= '0';
					Move_Slow		<= '1';
					Move_Time 	<= Move_Cnt;	

					if( St_MSlide_Del /= St_Move_Done )
						then Delay_Cnt <= 0;			
					elsif( ms1_tc = '1' )
						then Delay_Cnt <= Delay_Cnt + 1;
					end if;					
					
				
					if( Int_Enable = '0' )
						then ST_MSlide <= ST_Disabled;

					elsif(( ms1_tc = '1' ) and ( Delay_Cnt = Delay_Cnt_Max ))
						then St_MSlide 	<= St_Idle;
					end if;					
				-- End of Movement 
				--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				
				--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				-- Start of Movement, 1st Calculate the Movement profile BEFORE Starting any movement				
				when St_AutoRetract_Start =>
					Move_Enable	<= '1';
					Move_Slow		<= '0';									
					Tar_Pos_Mux 	<= conv_std_logic_vector( mmAutoRetract, 32 );					
					
					if( St_MSlide_Del /= St_AutoRetract_Start )
						then Delay_Cnt <= 0;			
						else Delay_Cnt <= Delay_Cnt + 1;
					end if;										

					if( conv_std_logic_vector( mmAutoRetract, 32 ) > MSlide_Pos )
						then Move_Analyze <= '1';
						else Move_Analyze <= '0';
					end if;

					Delay_Cnt <= Delay_Cnt + 1;

					if( Int_Enable = '0' )
						then ST_MSlide <= ST_Disabled;
						
					elsif( CMD_STB( STB_Slide_Stop ) = '1' )
						then ST_MSlide <= St_Idle;
					
					elsif( Delay_Cnt = Delay_Cnt_Max )
						then St_MSlide <= St_AutoRetract_Wait;
					end if;						
					
										
				when St_AutoRetract_Wait =>				
					Move_Enable	<= '1';					
					Delay_Cnt 	<= 0;

					if( Move_Slow_Enc = '1' )
						then Move_Slow <= '1';
						else Move_Slow <= '0';
					end if;
					
					if( Int_Enable = '0' )
						then ST_MSlide <= ST_Disabled;
						
					elsif( CMD_STB( STB_Slide_Stop ) = '1' )
						then ST_MSlide <= St_Idle;
						
					elsif( Move_Stalled = '1' ) 
						then ST_MSlide <= St_Stalled;

					elsif( Move_Stop = '1' )
						then St_MSlide	<= St_AutoRetract_Done;
					end if;
					
				when St_AutoRetract_Done =>
					Move_Enable	<= '0';
					Move_Slow		<= '0';
					Move_Time 	<= Move_Cnt;

					if( St_MSlide_Del /= St_AutoRetract_Done )
						then Delay_Cnt <= 0;			
					elsif( ms1_tc = '1' )
						then Delay_Cnt <= Delay_Cnt + 1;
					end if;					
									
					if( Int_Enable = '0' )
						then ST_MSlide <= ST_Disabled;
					elsif(( ms1_tc = '1' ) and ( Delay_Cnt = Delay_Cnt_Max ))
						then St_MSlide <= St_Idle;
					end if;					
				-- End of Movement 
				--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

				-- Detector Saturated Latch ------------------------------------------------------------------------------------------------
				when St_Latch_Det_Sat =>
					Move_Enable	<= '0';
					Move_Slow		<= '0';
					Move_Time 	<= Move_Cnt;										
					
					if( St_MSlide_Del /= St_Latch_Det_Sat )
						then Delay_Cnt <= 0;			
					elsif( ms1_tc = '1' )
						then Delay_Cnt <= Delay_Cnt + 1;
					end if;					
					
					if( Int_Enable = '0' )
						then St_MSlide <= St_Disabled;
					
					elsif( CMD_STB( STB_SLIDE_MR ) = '1' )
						then St_MSlide 	<= St_Idle;
						
					elsif(( ms1_tc = '1' ) and ( Delay_Cnt = Delay_Cnt_Max ))
						then St_MSlide <= St_Idle;											
					end if;
				-- Detector Saturated Latch ------------------------------------------------------------------------------------------------
					
				-- Watch-Dog Saturated Latch -----------------------------------------------------------------------------------------------
				when St_Latch_WDog =>
					Move_Enable	<= '0';
					Move_Slow		<= '0';
					Move_Time 	<= Move_Cnt;	

					if( St_MSlide_Del /= St_Latch_WDog )
						then Delay_Cnt <= 0;			
					elsif( ms1_tc = '1' )
						then Delay_Cnt <= Delay_Cnt + 1;
					end if;

					
					if( Int_Enable = '0' )
						then St_MSlide <= St_Disabled;
					
					elsif( CMD_STB( STB_SLIDE_MR ) = '1' )
						then St_MSlide 	<= St_Idle;

					elsif(( ms1_tc = '1' ) and ( Delay_Cnt = Delay_Cnt_Max ))
						then St_MSlide <= St_Idle;																	
					end if;				
				-- Watch-Dog Saturated Latch -----------------------------------------------------------------------------------------------
					
				-- High Counts Saturated Latch ---------------------------------------------------------------------------------------------
				when St_Latch_HC =>
					Move_Enable	<= '0';
					Move_Slow		<= '0';
					Move_Time 	<= Move_Cnt;

					if( St_MSlide_Del /= St_Latch_HC )
						then Delay_Cnt <= 0;			
					elsif( ms1_tc = '1' )
						then Delay_Cnt <= Delay_Cnt + 1;
					end if;
					
					
					if( Int_Enable = '0' )
						then St_MSlide <= St_Disabled;
					
					elsif( CMD_STB( STB_SLIDE_MR ) = '1' )
						then St_MSlide 	<= St_Idle;
						
					elsif(( ms1_tc = '1' ) and ( Delay_Cnt = Delay_Cnt_Max ))
						then St_MSlide <= St_Idle;											
					end if;				
				-- High Counts Saturated Latch ---------------------------------------------------------------------------------------------
				
				-- Stalled Latch -----------------------------------------------------------------------------------------------------------
				when St_Stalled =>
					Move_Enable	<= '0';
					
					if( St_MSlide_Del /= St_Stalled )
						then Delay_Cnt <= 0;			
					elsif( ms1_tc = '1' )
						then Delay_Cnt <= Delay_Cnt + 1;
					end if;
					
					if( Int_Enable = '0' )
						then St_MSlide <= St_Disabled;

					elsif( CMD_STB( STB_SLIDE_MR ) = '1' )
						then St_MSlide <= St_Idle;
						
					elsif(( ms1_tc = '1' ) and ( Delay_Cnt = Delay_Cnt_Max ))
						then St_MSlide <= St_Idle;											
					end if;
				-- Stalled Latch -----------------------------------------------------------------------------------------------------------
				
				when others	=>
					Move_Enable	<= '0';
					ST_MSlide 	<= St_Idle;
			end case;
			-- Motorized Slide State Machine ---------------------------------------------------
			---------------------------------------------------------------------------------------------------------------------------------
		end if;
	end process;
	-----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------
end behavioral;			-- mslide.vhd
----------------------------------------------------------------------------------------------



