---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	MSlide_Enc.vhd
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : September 21, 2007
--   Board          : XScope Instrument Control Board
--   Schematic      : 4035.007.27000
--
--   Top Level      : 4035.009.99330, S/W, XScope, Instrument Control
--
--   Description:
--	The Renishaw encoders generate two channels, A and B channels are which are
--	square waves that are 90 degrees out of phase. THis results in at-most only 
--	one bit changing at a time. ( Gray Code ).
--
--	This module monitors the A and B Channels from the Renishaw encoders
--	to form the positon count.
--
--	This allows not only the transitions to be detected but also the direction.

--	History
--		10/4/16: Inserted a 200 nSec glitch filter on the encoder inputs to fix slide drift issue.
--						EncPipe0(9 downto 0), EncPipe1(9 downto 0), and EncPiped(1 downto 0) 
--		090129xx - 
--			Module: PAxis_Encoder
--			Added Enc_Clr to Clear the Count Value
--			Added Enc_Set_PosD2 to Set the Count Value to 1/2 the current value

---------------------------------------------------------------------------------------------------

library LPM;
     use lpm.lpm_components.all;

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

---------------------------------------------------------------------------------------------------
entity MSlide_Enc is 
	generic (
		mmDecell			: in		integer;
		mmMoveEnable		: in		integer );
	port( 
		CLK50      	 	: in      std_logic;                         	-- Master Clock
		PIV_CLK			: in		std_Logic;
		MSlide_Clk		: in		std_logic;
		MSlide_Mr			: in		std_Logic;
		Enc_Clr 			: in		std_logic;
		ENC_Ch			: in		std_logic_vector( 1 downto 0 );		-- A and B Phases
		Move_Enable		: in		std_logic;
		Tar_Pos			: in		std_logic_vector( 31 downto 0 );
		Stalled			: buffer	std_logic;
		Move_Stop 		: buffer	std_logic;
		Move_Slow 		: buffer	std_logic;
		New_Pos_Enable		: buffer	std_logic;
		Enc_Cnt			: buffer	std_logic_vector( 31 downto 0 );
		Vel				: buffer	std_logic_Vector( 15 downto 0 ) );
end MSlide_Enc;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture Behavioral of MSlide_Enc is
	constant Stall_Cnt_Max 			: integer := 50;
	constant Startup_Cnt_Max 		: integer := 5000000;		-- 50 msec

	-- Signal Declarations -----------------------------------------------------------------------
	signal EncPipe0				   : std_logic_vector(9 downto 0); 
	signal EncPipe1					: std_logic_vector(9 downto 0); 
	signal EncPiped					: std_logic_vector(1 downto 0); 
	
	signal Enc_Ch_Del0				: std_logic_vector( 1 downto 0 );
	signal Enc_Ch_Del1				: std_logic_Vector( 1 downto 0 );
	signal Enc_Last				: std_logic_Vector( 1 downto 0 );
	
	signal Enc_Cnt_Last				: std_logic_Vector( 31 downto 0 );
	signal Enc_Diff				: std_logic_Vector( 31 downto 0 );
	
	signal Move_Slow_Cmp 			: std_logic;
	signal MSlide_Clk_Del 			: std_logic;
	signal Move_Enable_Del			: std_logic;

	signal Pos_Slide_Dir 			: std_logic;
	signal Pos_Slide_Dir_Cmp			: std_logic;
	signal Stop_Pos				: std_logic;
	signal Stop_Neg				: std_logic;
	
	signal Stall_Cnt				: integer range 0 to Stall_Cnt_Max;
	signal Startup_En 				: std_logic;
	signal Startup_Cnt				: integer range 0 to Startup_Cnt_max;
	signal Stalled_Cmp 				: std_logic;
	signal Stall_Pos				: std_logic_Vector( 31 downto 0 );			
	signal Stalled_Diff				: std_logic_Vector( 31 downto 0 );			
	signal Stalled_Diff_Abs			: std_logic_Vector( 31 downto 0 );			
	signal Stalled_Over				: std_logic;
	
	signal Target_Error				: std_logic_Vector( 31 downto 0 );
	signal Target_Error_abs			: std_logic_Vector( 31 downto 0 );
begin	
	---------------------------------------------------------------------------------------------------
	-- Determine if Target > Enc Count (=> Move in positive direction)
	Positive_Slide_Dir_Cmp_Inst : lpm_compare
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			dataa 			=> Tar_Pos,			-- Target Position
			datab			=> Enc_Cnt,			-- Current Position
			agb				=> Pos_Slide_Dir_Cmp );
	---------------------------------------------------------------------------------------------------

	---------------------------------------------------------------------------------------------------
	-- Determine when to stop (from which direction)
	Move_Stop_Compare : lpm_compare
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			dataa			=> Enc_Cnt,			-- Current Position
			datab 			=> Tar_Pos,			-- Target Position
			ageb				=> Stop_Pos,			-- Stop in Positive Direction
			aleb				=> Stop_Neg );			-- Stop in Negative Direction
	---------------------------------------------------------------------------------------------------
		
	---------------------------------------------------------------------------------------------------
	Vel_Add_Sub : lpm_add_Sub
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION 	=> "SIGNED",
			LPM_DIRECTION		=> "SUB" )
		port map(
			cin				=> '1',
			dataa			=> Enc_Cnt,
			datab			=> Enc_Cnt_Last,
			result			=> Enc_Diff );
	---------------------------------------------------------------------------------------------------
						
	---------------------------------------------------------------------------------------------------
	-- Determine Error to Target
	Target_Error_Inst : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 32,
			LPM_DIRECTION		=> "SUB",
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			Cin				=> '1',	-- Subtract
			dataa 			=> Tar_Pos,
			datab			=> Enc_Cnt,
			result			=> Target_Error );
	---------------------------------------------------------------------------------------------------

	---------------------------------------------------------------------------------------------------
	-- Take the absolute value of the Target Error 
	Absolute_Target_Error_Inst : lpm_abs
		generic map(
			LPM_WIDTH			=> 32 )
		port map(
			data				=> Target_Error,
			result			=> Target_Error_Abs );
	-- Take the absolute value of the Target Error 
	----------------------------------------------------------------------------------------------
	
	----------------------------------------------------------------------------------------------
	-- Determine when to Allow Movement
	Move_Stop_Inst : lpm_compare
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			dataa			=> Target_Error_Abs,
			datab			=> conv_Std_logic_vector( mmMoveEnable, 32 ),
			agb				=> New_Pos_Enable );
	-- Determine when to Stop
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Determine when to Stop
	Move_Slow_Inst : lpm_compare
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			dataa			=> Target_Error_Abs,
			datab			=> conv_Std_logic_vector( mmDecell, 32 ),
			aleb				=> Move_Slow_Cmp );
	-- Determine when to Stop
	----------------------------------------------------------------------------------------------			

	Stalled_Cmp <= '1' when ( Stall_Cnt = Stall_Cnt_Max ) else '0';
	
	Stall_Diff_Inst : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION 	=> "SIGNED",
			LPM_DIRECTION		=> "SUB" )
		port map(
			cin				=> '1',		
			dataa 			=> Enc_Cnt,
			datab 			=> Stall_Pos,
			result			=> Stalled_Diff );
			
	Stalled_Diff_Abs_Inst : lpm_abs
		generic map(
			LPM_WIDTH			=> 32 )
		port map(
			data				=> Stalled_Diff,
			result			=> Stalled_Diff_Abs );
			
	Stalled_Over_Inst : lpm_compare
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			dataa			=> Stalled_Diff_Abs,
			datab			=> conv_Std_logic_Vector( 50, 32 ),
			agb				=> Stalled_Over );
	
     ----------------------------------------------------------------------------------------------
	clock_proc : process( CLK50 )
	begin
		if( rising_edge( CLK50 )) then
			------------------------------------------------------------------------------------
			-- Encoder Input 		
			EncPipe0		<= Enc_Ch(0) & EncPipe0(9 downto 1); 
			EncPipe1		<= Enc_Ch(1) & EncPipe1(9 downto 1);
			
			--debouncing
			if EncPipe0 = b"1111111111" then 
				EncPiped(0) <= '1'; 
			elsif EncPipe0 = b"0000000000" then 
				EncPiped(0) <= '0'; 
			end if; 
			
			if EncPipe1 = b"1111111111" then 
				EncPiped(1) <= '1'; 
			elsif EncPipe1 = b"0000000000" then 
				EncPiped(1) <= '0'; 
			end if; 			
			
			Enc_Ch_Del0	<= EncPiped;
			Enc_Ch_Del1	<= Enc_Ch_Del0;

			------------------------------------------------------------------------------------
			-- Encoder Counter 
			-- At Enc_clr 	Clear the Counter
			if( Enc_Clr = '1' )
				then Enc_Cnt <= (others=>'0');
			elsif( Enc_Ch_Del0 /= Enc_Ch_Del1 ) then
				Enc_Last	<= Enc_Ch_Del0;		-- Latch the Encoder Input
				case Enc_Ch_Del0 is
					when "00" =>
						if( Enc_Last = "10" )
							then Enc_Cnt <= Enc_Cnt + 1;
						elsif( Enc_Last = "01" )
							then Enc_Cnt <= Enc_Cnt - 1;
						end if;			
						
					when "01" =>
						if( Enc_Last = "00" )
							then Enc_Cnt <= Enc_Cnt + 1;
						elsif( Enc_Last = "11" )
							then Enc_Cnt <= Enc_Cnt - 1;
						end if;											
						
					when "11" =>
						if( Enc_Last = "01" )
							then Enc_Cnt <= Enc_Cnt + 1;
						elsif( Enc_Last = "10" )
							then Enc_Cnt <= Enc_Cnt - 1;
						end if;											
					
					when "10" =>
						if( Enc_Last = "11" )
							then Enc_Cnt <= Enc_Cnt + 1;
						elsif( Enc_Last = "00" )
							then Enc_Cnt <= Enc_Cnt - 1;
						end if;																
					when others =>
				end case;
			end if;
			-- Encoder Input 
			------------------------------------------------------------------------------------

			------------------------------------------------------------------------------------
			-- Stall Detection			
			MSlide_Clk_Del <= MSlide_Clk;
			
			if( Move_Enable = '0' )
				then Stall_Cnt <= 0;
			elsif( Enc_Ch_Del0 /= Enc_Ch_Del1 ) 
				then Stall_Cnt <= 0;
			elsif(( MSlide_Clk = '1' ) and ( MSlide_Clk_Del = '0' ) and ( Stalled_Cmp = '0'  ))
				then Stall_Cnt <= Stall_Cnt + 1;
			end if;
			
			if( Move_Enable = '0' ) 
				then Stalled <= '0';
			elsif( MSlide_MR = '1' )
				then Stalled <= '0';
			elsif(( Enc_Ch_Del0 /= Enc_Ch_Del1 ) and ( Stalled_over = '1' ))
				then Stalled <= '0';
			elsif( Stalled_Cmp = '1' )
				then Stalled <= '1';
			end if;
			
			if(( Stalled = '0' ) and ( Stalled_Cmp = '1' ))		-- 1st Time Stall is detected
				then Stall_Pos <= Enc_Cnt;
			end if;
			
			------------------------------------------------------------------------------------
			
			------------------------------------------------------------------------------------
			-- Now Determine the velocity at the PIV_CLK intervals
			if( PIV_CLK = '1' ) then
				Enc_Cnt_Last 	<= Enc_Cnt;					-- Pipeline the Last Encoder Cnt
				Vel 			<= Enc_Diff( 15 downto 0 );		-- Now the Encoder Count Difference is the Velocity
			end if;
			-- Now Determine the velocity at the PIV_CLK intervals
			------------------------------------------------------------------------------------
			
			------------------------------------------------------------------------------------
			Move_Enable_Del <= Move_Enable;
			
			if(( Move_Enable = '1' ) and ( Move_Enable_del = '0' ))	-- Leading Edge of Move Enable
				then Pos_Slide_Dir <= Pos_Slide_Dir_Cmp;
			end if;

			if( Move_Enable = '0' )
				then Move_Stop <= '0';
			elsif(( Pos_Slide_Dir = '1' ) and ( Stop_Pos = '1' ))
				then Move_Stop <= '1';
			elsif(( Pos_Slide_Dir = '0' ) and ( Stop_Neg = '1' ))
				then Move_Stop <= '1';
				else Move_Stop <= '0';
			end if;
			
			if(( Move_Enable = '1' ) and ( Move_Enable_Del = '0' ))
				then Startup_En <= '1';
			elsif( Startup_Cnt = Startup_Cnt_Max )
				then Startup_En <= '0';
			end if;
			
			if( Startup_en = '0' )
				then Startup_Cnt <= 0;
			elsif( Startup_Cnt < Startup_Cnt_Max )
				then Startup_Cnt <= Startup_Cnt + 1;
			end if;			
			------------------------------------------------------------------------------------
			
			------------------------------------------------------------------------------------
			if(( Move_Slow_Cmp = '1' ) or ( Startup_En = '1' ))
				then Move_Slow <= '1';
				else Move_Slow <= '0';
			end if;
			------------------------------------------------------------------------------------
						
		end if;
	end process;
     ----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
end Behavioral;			-- MSlide_Enc.VHD
----------------------------------------------------------------------------------------------------


