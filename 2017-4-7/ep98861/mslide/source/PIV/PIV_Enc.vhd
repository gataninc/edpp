---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	PIV_Enc.VHD 
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : September 21, 2007
--   Board          : XScope Instrument Control Board
--   Schematic      : 4035.007.27000
--
--   Top Level      : 4035.009.99330, S/W, XScope, Instrument Control
--
--   Description:
--	The Renishaw encoders generate two channels, A and B channels are which are
--	square waves that are 90 degrees out of phase. THis results in at-most only 
--	one bit changing at a time. ( Gray Code ).
--
--	This module monitors the A and B Channels from the Renishaw encoders
--	to form the positon count.
--
--	This allows not only the transitions to be detected but also the direction.

--	History
--		090129xx - 
--			Module: PAxis_Encoder
--			Added Enc_Clr to Clear the Count Value
--			Added Enc_Set_PosD2 to Set the Count Value to 1/2 the current value
---------------------------------------------------------------------------------------------------

library LPM;
     use lpm.lpm_components.all;

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

---------------------------------------------------------------------------------------------------
entity PIV_Enc is 
	port( 
		imr       		: in      std_logic;				     	-- Master Reset
		CLK40      	 	: in      std_logic;                         	-- Master Clock
		Enc_Clr 			: in		std_logic;
		Enc_Set_Posd2 		: in		std_logic;
		ENC_Ch			: in		std_logic_vector( 1 downto 0 );		-- A and B Phases
		Target			: in		std_logic_Vector( 19 downto 0 );
		Motor_Stop_Val		: in		std_logic_Vector( 19 downto 0 );
		Motor_Start_Val	: in		std_logic_Vector( 19 downto 0 );
		Motor_Stop		: buffer	std_logic;
		Motor_Start		: buffer	std_logic;
		Up_Dir			: buffer	std_logic;
		Enc_Cnt			: buffer	std_logic_vector( 19 downto 0 ) );
end PIV_Enc;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture Behavioral of PIV_Enc is


	-- Signal Declarations -----------------------------------------------------------------------
	signal Enc_Ch_Del0				: std_logic_vector( 1 downto 0 );
	signal Enc_Ch_Del1				: std_logic_Vector( 1 downto 0 );
	signal Enc_Last				: std_logic_Vector( 1 downto 0 );
	signal Cnt_Up					: std_logic;
	signal Cnt_En					: std_logic;
	signal Pos_Error				: std_logic_Vector( 19 downto 0 );
	signal Abs_Pos_Error			: std_logic_Vector( 19 downto 0 );
	-- Signal Declarations -----------------------------------------------------------------------

begin
	----------------------------------------------------------------------------------------------
	-- Determine the Target Error ( ie the distance from the target )
	Target_Error_Inst : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 20,
			LPM_DIRECTION		=> "SUB",
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			cin				=> '1',
			dataa			=> Target,			-- Target Position
			datab			=> Enc_Cnt,			-- Current Position
			result			=> Pos_Error );
	-- Determine the Target Error ( ie the distance from the target )
	----------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------
	-- Determine the Target Error ( ie the distance from the target )
	Up_Dir_Compare : lpm_compare
		generic map(
			LPM_WIDTH			=> 20,
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_PIPELINE		=> 1 )
		port map(
			aclr				=> imr,
			clock			=> clk40,
			dataa			=> Target,			-- Target Position
			datab			=> Enc_Cnt,			-- Current Position
			ageb				=> Up_Dir );
	-- Determine the Target Error ( ie the distance from the target )
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Take the absolute value of the Target Error 
	Absolute_Target_Error_Inst : lpm_abs
		generic map(
			LPM_WIDTH			=> 20 )
		port map(
			data				=> Pos_Error,
			result			=> Abs_Pos_Error );
	-- Take the absolute value of the Target Error 
	----------------------------------------------------------------------------------------------
	
	----------------------------------------------------------------------------------------------
	Motor_Stop_Inst : lpm_compare
		generic map(
			LPM_WIDTH			=> 20,
			LPM_REPRESENTATION	=> "UNSIGNED",
			LPM_PIPELINE		=> 1 )
		port map(
			aclr				=> imr,
			clock			=> clk40,
			dataa			=> Abs_Pos_Error,
			datab			=> Motor_Stop_Val,
			aleb				=> Motor_Stop );
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	Motor_Start_Inst : lpm_compare
		generic map(
			LPM_WIDTH			=> 20,
			LPM_REPRESENTATION	=> "UNSIGNED",
			LPM_PIPELINE		=> 1 )
		port map(
			aclr				=> imr,
			clock			=> clk40,
			dataa			=> Abs_Pos_Error,
			datab			=> Motor_Start_Val,
			agb				=> Motor_Start );
	----------------------------------------------------------------------------------------------
	
     ----------------------------------------------------------------------------------------------
	clock_proc : process( imr, CLK40 )
	begin
		if( imr = '1' ) then
			Enc_Ch_Del0	<= "00";
			Enc_CH_Del1	<= "00";
			Enc_Last		<= "00";
			Cnt_Up		<= '0';
			Cnt_En		<= '0';
			Enc_Cnt 		<= conv_Std_Logic_Vector( 0, 20 );
			
		elsif(( CLK40'event ) and ( CLK40 = '1' )) then
			------------------------------------------------------------------------------------
			-- Encoder Input 		
			Enc_Ch_Del0	<= Enc_Ch;
			Enc_Ch_Del1	<= Enc_Ch_Del0;
			
			-- if Any of the Encoder bits changed then Count
			if( Enc_Ch_Del0 /= Enc_Ch_Del1 ) then
				Cnt_En	<= '1';				-- Asser signal to count
				Enc_Last	<= Enc_Ch_Del0;		-- Latch the Encoder Input
				
				-- Based on Encoder Input and Last Input Determine which direction to count
				if(   ( Enc_Ch_Del0 = "00" ) and ( Enc_Last = "10" ))  then Cnt_Up <= '1';
				elsif(( Enc_Ch_Del0 = "01" ) and ( Enc_Last = "00" ))	then Cnt_Up <= '1';
				elsif(( Enc_Ch_Del0 = "11" ) and ( Enc_Last = "01" ))	then Cnt_Up <= '1';
				elsif(( Enc_Ch_Del0 = "10" ) and ( Enc_Last = "11" ))	then Cnt_Up <= '1';
															else Cnt_Up <= '0';
				end if;
			else
				Cnt_En 	<= '0';
			end if;
			-- Encoder Input 
			------------------------------------------------------------------------------------

			------------------------------------------------------------------------------------
			-- Encoder Counter 
			-- At Enc_clr 	Clear the Counter
			if( Enc_Clr = '1' )
				then Enc_Cnt <= conv_Std_Logic_Vector( 0, 20 );
			
			-- at Enc_set_posd2 set the counter to 1/2 its current value
			elsif( Enc_Set_Posd2 = '1' )
				then Enc_Cnt <= sxt( Enc_Cnt( 19 downto 1 ), 20 );
			
			-- if Count Enabled and count up increment counter
			elsif(( Cnt_En = '1' ) and ( Cnt_Up = '1' ))
				then Enc_Cnt <= Enc_Cnt + 1;

			-- if Count Enabled and not count up decrement counter
			elsif(( Cnt_En = '1' ) and ( Cnt_Up = '0' ))
				then Enc_Cnt 	<= Enc_Cnt - 1;
			end if;
			-- Encoder Counter 
			------------------------------------------------------------------------------------
		end if;
	end process;
     ----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
end Behavioral;			-- PIV_Enc.VHD
----------------------------------------------------------------------------------------------------


