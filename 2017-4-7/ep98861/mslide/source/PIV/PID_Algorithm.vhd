---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	PID_Algorithm.VHD 
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : September 21, 2007
--   Board          : XScope Instrument Control Board
--   Schematic      : 4035.007.27000
--
--   Top Level      : 4035.009.99330, S/W, XScope, Instrument Control
--
--   Description:
--
--Target Position is DP (Desired Position)
--Position is PS
--Position Error = PE
--
--PE = DP - PS
--VC = PE*KP + (DP-DPlast)*65536;
--
--	History
--		090205: Created
---------------------------------------------------------------------------------------------------

library LPM;
     use lpm.lpm_components.all;
     
library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

---------------------------------------------------------------------------------------------------
entity PID_Algorithm is 
	port( 
		imr       		: in      std_logic;				     	-- Master Reset
		CLK40      	 	: in      std_logic;                         	-- Master Clock
		Move_Clk			: in		std_logic;
		Move_Start		: in		std_logic;
		Move_Enable		: in		std_logic;
		Up_Dir			: in		std_logic;
		KI_Init			: in		std_logic_Vector( 31 downto 0 );
		DP				: in		std_logic_Vector( 19 downto 0 );		-- Target Positon
		PS				: in		std_Logic_Vector( 19 downto 0 );		-- Current Positin
		KP				: in		std_Logic_Vector( 31 downto 0 );		-- Postion loop Gain 
		KI				: in		std_Logic_Vector( 31 downto 0 );		-- Velocity loop Integral Gain
		KD				: in		std_logic_Vector( 31 downto 0 );		-- Velocity Loop Overall Gain
		PE_OUT			: buffer	std_logic_vector( 19 downto 0 );
		PE_DIF_OUT		: buffer	std_Logic_Vector( 19 downto 0 );		
		PS_Out			: buffer	std_logic_Vector( 19 downto 0 );
		Cmd_Out			: buffer	std_logic_Vector( 10 downto 0 );		-- Position Error
		Stalled			: buffer	std_logic );
end PID_Algorithm;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture Behavioral of PID_Algorithm is
	-- Constant Declratrions ---------------------------------------------------------------------
	constant LSB					: integer := 16;			-- Of Accumulator to CMD Out
	constant Cmd_Init	 			: integer :=  512 * 65536;	-- Based on LSB
	constant Cmd_Out_Max 			: integer :=  1020;	
	constant Cmd_Out_Min 			: integer := -1020;
	constant Move_Clk_Max			: integer := 3;
	
	constant kStall_Threshold		: integer := 2000;
	
	constant Stall_Inhibit_Cnt_Max	: integer := 100000 / 250;		-- 100 @ 250 us

	constant PID_Idle 				: integer := 0;
	constant PID_KPPE				: integer := 1;
	constant PID_KDDPE				: integer := 2;
	constant PID_KIPE				: integer := 3;
	constant PID_KPKIKD				: integer := 4;
	constant PID_Done				: integer := 5;
	-- Constant Declratrions ---------------------------------------------------------------------

	-- Signal Declratrions -----------------------------------------------------------------------
	signal Abs_PE					: std_logic_vector( 19 downto 0 );

	signal Clamp_Hi				: std_logic;
	signal Clamp_Lo				: std_logic;


	signal Move_Start_Del			: std_logic;

	signal PE_Last					: std_Logic_Vector( 19 downto 0 );
	signal PE_Acc_Sum				: std_logic_Vector( 27 downto 0 );				
	signal PE_Acc_Sum_over			: std_logic;
	signal PE_Acc_Sum_Cout			: std_logic;
	signal PE_ACC 					: std_logic_Vector( 27 downto 0 );				
	signal PE_Sub_Over 				: std_logic;
	signal PE_Sub_Cout				: std_logic;
	signal PE_Sub					: std_logic_Vector( 19 downto 0 );
	signal PE_Last_Sub				: std_logic_Vector( 19 downto 0 );
	signal PE_Last_Over				: std_logic;
	signal PE_Last_Cout				: std_logic;
	signal PE						: std_logic_vector( 19 downto 0 );
	signal PE_DIF					: std_Logic_Vector( 19 downto 0 );		

	signal PID_State 				: integer range 0 to PID_Done;
	signal Multa					: std_logic_Vector( 23 downto 0 );
	signal Multb					: std_logic_Vector( 27 downto 0 );
	signal Multr					: std_logic_Vector( 51 downto 0 );
	signal KPKIKD_Acc 				: std_Logic_Vector( 51 downto 0 );
	signal Mult_Acc_Add				: std_logic_Vector( 51 downto 0 );
	signal Mult_Acc_Over			: std_logic;
	signal Mult_Acc_cout			: std_logic;
	
	signal Stall_Inhibit_Cnt			: integer range 0 to Stall_Inhibit_Cnt_Max;
	signal Stall_Inhibit_En			: std_logic;
	signal Stall_Inhibit_Tc			: std_logic;
	signal Stall_Cmp				: std_logic;
	
	-- Signal Declratrions -----------------------------------------------------------------------

	
begin
	-- Calculate the Positon Error based on Desired and Current Position -------------------------
	PE_Add_Sub_Inst : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 20,
			LPM_REPRESENTATION 	=> "SIGNED",
			LPM_DIRECTION		=> "SUB" )
		port map(
			cin				=> '1',
			dataa			=> DP,						-- Desired Position
			datab			=> PS,						-- Current Position
			result			=> PE_Sub,					-- Position Error
			Overflow			=> PE_Sub_Over,				-- For Subtract: Overflow = 1 when "Result" < -2^(Width-1)
			Cout				=> PE_Sub_Cout );				-- Cout: when 0 - Full Scale Positive
	-- Calculate the Positon Error based on Desired and Current Position -------------------------
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Calculte change in Position Error ---------------------------------------------------------
	PE_DIF_Add_Sub_Inst : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 20,
			LPM_REPRESENTATION 	=> "SIGNED",
			LPM_DIRECTION		=> "SUB" )
		port map(
			cin				=> '1',
			dataa			=> PE,						-- Current Position Error
			datab			=> PE_Last,					-- Last Position Error
			result			=> PE_Last_SUB,				-- Position Error Differences
			Overflow			=> PE_Last_Over,				-- For Subtract: Overflow = 1 when "Result" < -2^(Width-1)
			Cout				=> PE_Last_Cout );

	PE_Dif <= not( '1' & conv_std_logic_Vector( 0, 19 )) when (( PE_Last_Over = '1' ) and ( PE_Last_Cout = '0' )) else	-- Clamp Positive
			   ( '1' & conv_std_logic_Vector( 0, 19 )) when (( PE_Last_Over = '1' ) and ( PE_Last_Cout = '1' )) else	-- Clamp Negative
			PE_Last_SUB;
	-- Calculte change in Position Error ---------------------------------------------------------
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Integral Portion
	-- Position Error Accumulator used for the Integral portion of the PID ------------------------	
	PE_Adder_Inst : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 28,
			LPM_DIRECTION		=> "ADD",
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			cin				=> '0',
			dataa			=> PE_Acc,
			datab			=> sxt( PE, 28 ),			-- Sign Extend Position Error to 32 bits
			result			=> PE_Acc_Sum,
			overflow			=> PE_Acc_Sum_Over,
			Cout				=> PE_Acc_Sum_Cout );
	-----------------------------------------------------------------------------------------

	
	with PID_State select
		Multa <= KP( 23 downto 0 ) when PID_KPPE,
			    KD( 23 downto 0 ) when PID_KDDPE,
			    KI( 23 downto 0 ) when PID_KIPE,
			    conv_Std_logic_Vector( 0, 24 ) when others;
			    

	with PID_State select
		Multb <= sxt( PE, 28 )  				when PID_KPPE,
			    sxt( PE_DIF, 28 ) 			when PID_KDDPE,
			    PE_Acc  					when PID_KIPE,
			    conv_Std_logic_Vector( 0, 28 ) when others;
	
	Mult_Inst : lpm_mult
		generic map(
			LPM_WIDTHA		=> 24,
			LPM_WIDTHB		=> 28,
			LPM_WIDTHS		=> 52,
			LPM_WIDTHP		=> 52,
			LPM_REPRESENTATION	=> "SIGNED",			
			LPM_HINT			=> "MAXIMIZE_SPEED=5",
			LPM_PIPELINE		=> 1 )
		port map(
			aclr				=> imr,
			clock			=> clk40,
			dataa			=> multa,			
			datab			=> multb, 						
			sum				=> conv_std_logic_Vector( 0, 52 ),
			result			=> multr );				
	-- Proportional Gain Calculation -------------------------------------------------------------
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Multiplier Accumulator Adder
	-- Proportional & Deravitive Adder -----------------------------------------------------------
	Mult_Acc_Add_Inst : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 52,
			LPM_REPRESENTATION 	=> "SIGNED",
			LPM_DIRECTION		=> "ADD" )
		port map(
			cin				=> '0',
			dataa			=> KPKIKD_Acc,	
			datab			=> Multr,		
			result			=> Mult_Acc_Add,
			Overflow			=> Mult_Acc_Over,
			Cout				=> Mult_Acc_Cout );
			
	-- Proportional & Deravitive Adder -----------------------------------------------------------


     ----------------------------------------------------------------------------------------------
	-- Check the final output for Over/Under of Max Voltage to Motor -----------------------------
	KPKDKI_SUM_Clamp_hi_Compare : lpm_compare
		generic map(
			LPM_WIDTH			=> 52-LSB,
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			dataa			=> KPKIKD_Acc( 51 downto LSB ),
			datab			=> conv_std_logic_Vector( Cmd_Out_Max, 52-LSB ),
			ageb				=> Clamp_Hi );

	KPKDKI_SUM_Clamp_Lo_Compare : lpm_compare
		generic map(
			LPM_WIDTH			=> 52-LSB,
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			dataa			=> KPKIKD_Acc( 51 downto LSB ),
			datab			=> conv_std_logic_Vector( Cmd_Out_Min, 52-LSB ),
			aleb				=> Clamp_Lo );		             
	-- Check the final output for Over/Under of Max Voltage to Motor -----------------------------
     ----------------------------------------------------------------------------------------------
		

	----------------------------------------------------------------------------------------------
	-- Logic for Stall Detection
	-- Determine ABS of PE > Some threshoold
	
	ABS_PE_INST : lpm_abs
		generic map(
			LPM_WIDTH			=> 20 )
		port map(
			data				=> PE,
			result			=> Abs_PE );
			
	Abs_Pe_Compare_Inst : lpm_compare
		generic map(
			LPM_WIDTH			=> 20,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			dataa			=> Abs_PE,
			datab			=> Conv_Std_logic_Vector( kStall_Threshold, 20 ),
			agb				=> Stall_Cmp );

	Stall_Inhibit_Tc <= '1' when ( Stall_Inhibit_Cnt = Stall_Inhibit_Cnt_Max ) else '0';
			
	----------------------------------------------------------------------------------------------
		
	-----------------------------------------------------------------------------------------
	-- PID Integrator
	--	At Start of Move - INitialize to 1/2 CMD_OUT scale
	--	At change of direction - Initialize to 1/2 Cmd_Out scale
     ----------------------------------------------------------------------------------------------
	Cmd_Out_Proc : process( imr, CLK40 )
	begin
		if( imr = '1' ) then
			PE_Acc 			<= conv_std_logic_Vector( 0, 28 );
			Move_Start_Del		<= '0';
			PE				<= conv_Std_Logic_Vector( 0, 20 );
			PE_Last			<= conv_Std_Logic_Vector( 0, 20 );			
			Cmd_Out 			<= conv_std_logic_vector( 0, 11 );			
			PE_Out			<= conv_Std_logic_Vector( 0, 20 );
			PE_DIF_Out		<= conv_Std_logic_Vector( 0, 20 );
			Stall_Inhibit_En 	<= '0';
			Stall_Inhibit_Cnt	<= 0;
			Stalled			<= '0';
		
		elsif(( CLK40'event ) and ( CLK40 = '1' )) then
			Move_Start_Del 	<= Move_Start;

			-- Not Moving - Force to Zero
			if( Move_Enable = '0' )
				then PE_Acc <= conv_std_logic_Vector( 0, 28 );
		
			-- Use Delayed Move Start to get accurate "Direction" bit
			elsif( Move_Start_Del = '1' ) then
				if( Up_Dir = '1' )
					then PE_Acc <=    ( KI_Init( 27 downto 0 ));			-- Positive Initial Value
					else PE_Acc <= not( KI_Init( 27 downto 0 ));			-- Negative Initial Value
				end if;
			
			elsif( Move_CLk = '1' ) then
				-- Normal Movement 
				if( PE_Acc_Sum_Over = '0' )
					then PE_Acc <= PE_Acc_Sum;
			
				-- Accumlator at Max so clamp it positive
				elsif(( PE_Acc_Sum_Over = '1' ) and ( PE_Acc_Sum_Cout = '0' ))
					then PE_Acc <= not( '1' & conv_std_logic_Vector( 0, 27 ));

				-- Accumlator at Min so clamp it negative
				elsif(( PE_Acc_Sum_Over = '1' ) and ( PE_Acc_Sum_Cout = '1' ))
					then PE_Acc <= '1' & conv_std_logic_Vector( 0, 27 );
				end if;
			end if;

			-- Position Error Register ---------------------------------------------------------
			if( Move_CLk = '1' ) then
				PE_Last 	<= PE;
				
				if( PE_Sub_Over = '0' )
					then PE <= PE_Sub;
				elsif( Pe_Sub_Cout = '0' )
					then PE <= not( '1' & conv_std_logic_Vector( 0, 19 ));
					else PE <=    ( '1' & conv_std_logic_Vector( 0, 19 ));
				end if;
			end if;
			-- Position Error Register ---------------------------------------------------------
			
			-- Stall Detection -----------------------------------------------------------------
			if( Move_Start_Del = '1' ) 
				then Stall_Inhibit_En <= '1';
			elsif( Stall_Inhibit_Tc = '1' )
				then Stall_Inhibit_En <= '0';
			end if;

			if( Stall_Inhibit_En = '0' )
				then Stall_Inhibit_Cnt <= 0;
			elsif( Move_CLk = '1' ) 
				then Stall_Inhibit_Cnt <= Stall_Inhibit_Cnt + 1;
			end if;
			
			If(( Move_Enable = '0' ) or ( Move_Start = '1' ) or ( Move_Start_Del = '1' ))
				then Stalled <= '0';
			elsif(( Stall_Inhibit_En = '0' ) and ( Move_Clk = '1' ) and ( Stall_Cmp = '1' ))
				then Stalled <= '1';
			end if;
			-- Stall Detection -----------------------------------------------------------------

			-- PID Sequencer to share resouces -------------------------------------------------
			case PID_State is
				when  PID_Idle =>
					if( Move_Clk = '1' )
						then PID_State <= PID_KPPE;
					end if;
					
				-- Set up the DataA and DAtab to the Mult for the P Term
				when PID_KPPE =>
					PID_State <= PID_KDDPE;
				
				when PID_KDDPE =>
					-- Now Set the Product to the KPKIID Accumulator
					KPKIKD_Acc	<= Multr;					
					PID_State 	<= PID_KIPE;
					
				when PID_KIPE =>
					--	And setup DataA and DataB Term for the I Term
				
					-- Set the KPKIKD_Acc to the output of the adder
					if( Mult_Acc_Over = '0' )
						then KPKIKD_Acc <= Mult_Acc_Add;
					elsif( Mult_Acc_Cout = '0' )
						then KPKIKD_Acc <= not( '1' & conv_std_logic_Vector( 0, 51 ));
						else KPKIKD_Acc <=    ( '1' & conv_std_logic_Vector( 0, 51 ));
					end if;
					PID_State <= PID_KPKIKD;
					
				when PID_KPKIKD =>
					-- Set the KPKIKD_Acc to the output of the adder
					if( Mult_Acc_Over = '0' )
						then KPKIKD_Acc <= Mult_Acc_Add;
					elsif( Mult_Acc_Cout = '0' )
						then KPKIKD_Acc <= not( '1' & conv_std_logic_Vector( 0, 51 ));
						else KPKIKD_Acc <=    ( '1' & conv_std_logic_Vector( 0, 51 ));
					end if;								
					PID_State <= PID_DONE;
					
				when PID_DONE =>
					PID_State 	<= PID_Idle;
			
				when others =>
					PID_State <= PID_Idle;
			end case;
			-- PID Sequencer to share resouces -------------------------------------------------
			
			if( Move_Enable = '0' )
				then Cmd_Out <= conv_Std_Logic_Vector( 0, 11 );
			elsif( PID_State = PID_Done ) then
				PE_Out		<= PE;
				PE_DIF_Out	<= PE_DIf;	
				PS_Out		<= PS;	

				if( Clamp_Hi = '1' )
					then Cmd_Out <= conv_Std_logic_Vector( Cmd_Out_Max, 11 );
				elsif( Clamp_Lo = '1' )
					then Cmd_Out <= conv_std_logic_Vector( Cmd_Out_Min, 11 );
					else Cmd_Out <= KPKIKD_Acc( 10 + LSB downto LSB );										
				end if;		
			end if;
		
		end if;
	end process;
     ----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Proportional Gain Calculation -------------------------------------------------------------
	


----------------------------------------------------------------------------------------------------
end Behavioral;			-- PIV_Algorithm.VHD
----------------------------------------------------------------------------------------------------


