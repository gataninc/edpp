--------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	PIV_AB4SPI.VHD 
---------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : September 21, 2007
--   Board          : XScope Instrument Control Board
--   Schematic      : 4035.007.27000
--
--   Top Level      : 4035.009.99330, S/W, XScope, Instrument Control
--
--   Description:
--		This module taksa an 11-bit signed value for a velocity command (U), and creats
--		a 10-bit unsigned, plus direction, for the command to the Nanomotion AB4 amplifier 
--		with SPI interface.
--
--		The output data is to change on the rising edge of the clock ( where the amplifier samples it on the falling edge )
--		THe clock period is to be between 250 and 500 ns.
--		
--		16 bits are transmitted, MSB to LSB where only 10 bits plus a sign bit are used.
--		the 1st bit is the Sign bit, followed by the other 9 bits. Finally 5 don't care bits are sent
--		for a total of 16 bits
--
--		The Data format to the motor is always positive, the amplitude data is zero based, 
--		the MSB just indicates the direction.
--		
--   History:       <date> - <Author> - Ver
---------------------------------------------------------------------------------------------------

library LPM;
   use lpm.lpm_components.all;
   
library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;
---------------------------------------------------------------------------------------------------
entity PIV_AB4SPI is 
	port( 
		IMR       	: in      std_logic;					-- Master Reset
		CLK40		: in		std_logic;					-- Master Clock
		Piezo_En		: in		std_logic;
		Piezo_Cmd		: in		std_logic_Vector( 10 downto 0 );
		SPI_CS		: buffer	std_logic;
		SPI_CLOCK		: buffer	std_logic;
		SPI_DATA		: buffer	std_logic );
end PIV_AB4SPI;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture Behavioral of PIV_AB4SPI is

	-- Constant Declarations ---------------------------------------------------------------------
	constant CLK_CNT_MAX 	: integer := 15;
	constant BIT_CNT_MAX	: integer := 15;

	constant ST_IDLE 		: integer := 0;
	constant ST_DATA		: integer := 1;
	constant ST_DATA_DONE	: integer := 2;
	constant ST_ENABLE		: integer := 3;
	constant ST_ENABLE_DONE	: integer := 4;
	constant ST_DISABLE		: integer := 5;
	constant ST_DISABLE_DONE	: integer := 6;
	-- Constant Declarations ---------------------------------------------------------------------

	-- Signal Declarations -----------------------------------------------------------------------
	signal AB4_State		: integer range 0 to ST_DISABLE_DONE;
	signal Abs_Piezo_Cmd 	: std_logic_Vector( 10 downto 0 );

	signal BIT_CNT			: integer := 0;
	signal BIT_CNT_TC		: std_logic;

	signal CLK_CNT 		: integer range 0 to CLK_CNT_MAX;
	signal CLK_CNT_TC 		: std_Logic;	
	signal Cmd_Latch		: std_logic_vector( BIT_CNT_MAX downto 0 );

	signal Disable_Latch	: std_logic;
	
	signal Enable_Latch		: std_logic;

	signal Load_Latch		: std_Logic;
	
	signal Piezo_En_Del		: std_logic;
	signal Piezo_Cmd_Del	: std_logic_Vector( 10 downto 0 );
	
	signal SReg			: std_logic_vector( BIT_CNT_MAX downto 0 );
	

	-- Signal Declarations -----------------------------------------------------------------------
	

begin
	BIT_CNT_TC	<= '1' when ( BIT_CNT = 0 ) else '0';
	CLK_CNT_TC 	<= '1' when ( CLK_CNT = CLK_CNT_MAX ) else '0';
	
	ABS_PIEZO_CMD_INST : LPM_ABS
		generic map(
			LPM_WIDTH		=> 11 )
		port map(
			data			=> Piezo_Cmd,
			result		=> Abs_Piezo_Cmd );

     ----------------------------------------------------------------------------------------------	
	clock_Proc : process( imr, CLK40 )
	begin
		if( imr = '1' ) then
			Piezo_En_Del	<= '0';
			Piezo_Cmd_Del 	<= conv_Std_logic_Vector( 0, 11 );	
			Load_Latch	<= '0';
			SReg			<= conv_std_logic_vector( 0, BIT_CNT_MAX+1 );
			Cmd_Latch		<= conv_std_logic_vector( 0, BIT_CNT_MAX+1 );
			Enable_Latch	<= '0';
			Disable_Latch	<= '0';
			AB4_State		<= ST_IDLE;
			CLK_CNT 		<= 0;
			
			
			SPI_CS		<= '1';		-- Inactive
			SPI_CLOCK		<= '0';			
		elsif(( CLK40'event ) and ( CLK40 = '1' )) then
			Piezo_En_Del	<= Piezo_En;
			Piezo_Cmd_Del	<= Piezo_Cmd;

			if( Piezo_Cmd /= Piezo_Cmd_Del ) 
				then Load_Latch <= '1';
			elsif( AB4_State = St_Data_Done )
				then Load_Latch <= '0';
			end if;

			if(( Piezo_En = '1' ) and ( Piezo_En_Del = '0' ))
				then Enable_Latch 	<= '1';
			elsif( AB4_State = ST_Enable_Done )
				then Enable_Latch	<= '0';
			end if;
			
			if(( Piezo_En = '0' ) and ( Piezo_En_Del = '1' ))
				then Disable_latch <= '1';
			elsif( AB4_State = ST_DISABLE_Done )
				then Disable_Latch <= '0';
			end if;

			-- If the Command Changed - Update SREG
			if( Piezo_Cmd /= Piezo_Cmd_Del ) 
				then Cmd_Latch	 <= Piezo_Cmd( 10 ) & Abs_Piezo_Cmd( 9 downto 0 ) & "00000";
--				then Cmd_Latch	 <= Piezo_Dir & Piezo_Cmd & "00000";
			elsif(( Piezo_En = '0' ) and ( Piezo_En_Del = '1' ))
				then Cmd_Latch	<= conv_std_logic_Vector( 0, BIT_CNT_MAX + 1 );
			end if;

			-- TIme Base for clock to AB4 Amplifiers -------------------------------------------
			if(( AB4_State = ST_Data ) and ( CLK_CNT_TC = '0' ))
				then CLK_CNT <= CLK_CNT + 1;
			elsif(( AB4_State = ST_DISABLE ) and ( CLK_CNT_TC = '0' ))
				then CLK_CNT <= CLK_CNT + 1;
			elsif(( AB4_State = ST_ENABLE ) and ( CLK_CNT_TC = '0' ))
				then CLK_CNT <= CLK_CNT + 1;
				else CLK_CNT <= 0;
			end if;
			
			if(( AB4_State = ST_DATA ) and ( CLK_CNT <= 7 ))
				then SPI_CLOCK <= '1';
			elsif(( AB4_State = ST_DISABLE ) and ( CLK_CNT <= 7 ))
				then SPI_CLOCK	<= '1';
			elsif(( AB4_State = ST_ENABLE ) and ( CLK_CNT <= 7 ))
				then SPI_CLOCK	<= '1';
				else SPI_CLOCK	<= '0';
			end if;
			-- TIme Base for clock to AB4 Amplifiers -------------------------------------------
				
			-- Bit Counter for AB4 Amplifiers --------------------------------------------------
			if( AB4_State = ST_Idle )
				then BIT_CNT <= BIT_CNT_MAX;
			elsif(( AB4_State = ST_Data ) and ( CLK_CNT_TC = '1' ))
				then BIT_CNT <= BIT_CNT - 1;
			elsif(( AB4_State = ST_DISABLE ) and ( CLK_CNT_TC = '1' ))
				then BIT_CNT <= BIT_CNT - 1;
			elsif(( AB4_State = ST_ENABLE ) and ( CLK_CNT_TC = '1' ))
				then BIT_CNT <= BIT_CNT - 1;
			end if;
			-- Bit Counter for AB4 Amplifiers --------------------------------------------------

			-- SPI Data Output -----------------------------------------------------------------
			if( AB4_State = ST_IDLE )
				then SPI_DATA <= '0';
			elsif(( AB4_State = ST_DATA ) and ( CLK_CNT = 0 ))
				then SPI_DATA <= SReg( BIT_CNT );
			elsif(( AB4_State = ST_ENABLE ) and ( CLK_CNT = 0 ))
				then SPI_DATA <= SReg( BIT_CNT );
			elsif(( AB4_State = ST_DISABLE ) and ( CLK_CNT = 0 ))
				then SPI_DATA <= '0';
			end if;
			-- SPI Data Output -----------------------------------------------------------------				

			------------------------------------------------------------------------------------
			-- State Machine 
			case AB4_State is
				when ST_IDLE =>
					SPI_CS	<= '1';		-- Inactive
					if( Load_Latch = '1' ) then
						AB4_State <= ST_Data;
						SReg		<= Cmd_Latch;
					elsif( Enable_Latch = '1' ) then
						AB4_State <= ST_Enable;
						SReg		<= Cmd_Latch;
					elsif( Disable_Latch = '1' ) then
						AB4_State <= ST_DISABLE;
						SReg		<= conv_std_Logic_Vector( 0, BIT_CNT_MAX + 1 );
					end if;
				
				-------------------------------------------------------------------------------
				-- Data States
				when ST_Data =>
					SPI_CS 	<= '0';		-- Active
					if(( CLK_CNT_TC = '1' ) and ( BIT_CNT_TC = '1' ))
						then AB4_State <= ST_DATA_DONE;
					end if;
				
				when ST_Data_Done =>
					SPI_CS	<= '1';
					AB4_State <= St_Idle;
				-- Data States
				-------------------------------------------------------------------------------
					
				-------------------------------------------------------------------------------
				-- Enable States
				when ST_ENABLE =>
					SPI_CS 	<= '0';		-- Active
					if(( CLK_CNT_TC = '1' ) and ( BIT_CNT_TC = '1' ))
						then AB4_State <= ST_ENABLE_Done;
					end if;
				
				when ST_ENABLE_Done =>
					SPI_CS	<= '1';
					AB4_State <= St_Idle;
				-- Disable States
				-------------------------------------------------------------------------------

				-------------------------------------------------------------------------------
				-- Disable States
				when ST_DISABLE =>
					SPI_CS 	<= '0';		-- Active
					if(( CLK_CNT_TC = '1' ) and ( BIT_CNT_TC = '1' ))
						then AB4_State <= ST_DISABLE_Done;
					end if;
				
				when ST_DISABLE_Done =>
					SPI_CS	<= '1';
					AB4_State <= St_Idle;
				-- Disable States
				-------------------------------------------------------------------------------
					
				when others	=> 
					SPI_CS 	<= '1';		-- Inactive
					AB4_State <= St_Idle;
			end case;
			-- State Machine 
			------------------------------------------------------------------------------------
		

		end if;
	end process;
     ----------------------------------------------------------------------------------------------	
---------------------------------------------------------------------------------------------------
end Behavioral;				-- AB4SPI.VHD
---------------------------------------------------------------------------------------------------
