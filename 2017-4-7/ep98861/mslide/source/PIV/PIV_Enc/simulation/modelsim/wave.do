onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic /tb/imr
add wave -noupdate -format Logic /tb/clk40
add wave -noupdate -format Logic /tb/enc_clr
add wave -noupdate -format Logic /tb/enc_set_posd2
add wave -noupdate -format Literal /tb/enc_ch
add wave -noupdate -format Literal -radix decimal /tb/target
add wave -noupdate -format Literal -radix decimal /tb/target_cmp_val
add wave -noupdate -format Logic -radix decimal /tb/target_cmp
add wave -noupdate -format Analog-Step -height 74 -max 75.0 -radix decimal /tb/enc_cnt
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {71507 ns} 0}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {2964 ns} {21908 ns}
