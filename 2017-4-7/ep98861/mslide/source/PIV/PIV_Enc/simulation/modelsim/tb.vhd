---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	PIV_Enc.VHD 
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : September 21, 2007
--   Board          : XScope Instrument Control Board
--   Schematic      : 4035.007.27000
--
--   Top Level      : 4035.009.99330, S/W, XScope, Instrument Control
--
--   Description:
--	The Renishaw encoders generate two channels, A and B channels are which are
--	square waves that are 90 degrees out of phase. THis results in at-most only 
--	one bit changing at a time. ( Gray Code ).
--
--	This module monitors the A and B Channels from the Renishaw encoders
--	to form the positon count.
--
--	This allows not only the transitions to be detected but also the direction.

--	History
--		090129xx - 
--			Module: PAxis_Encoder
--			Added Enc_Clr to Clear the Count Value
--			Added Enc_Set_PosD2 to Set the Count Value to 1/2 the current value


library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;
entity tb is
end tb;

architecture test_bench of tb is


	signal imr       		: std_logic := '1';				     	-- Master Reset
	signal CLK40      	 	: std_logic;                         	-- Master Clock
	signal Enc_Clr 		: std_logic;
	signal Enc_Set_Posd2 	: std_logic;
	signal ENC_Ch			: std_logic_vector( 1 downto 0 );		-- A and B Phases
	signal Target			: std_logic_Vector( 22 downto 0 );
	signal Target_Cmp_Val	: std_logic_Vector( 22 downto 0 );
	signal Target_Cmp		: std_logic;
	signal Enc_Cnt			: std_logic_vector( 22 downto 0 );

	---------------------------------------------------------------------------------------------------
	component PIV_Enc is 
		port( 
			imr       		: in      std_logic;				     	-- Master Reset
			CLK40      	 	: in      std_logic;                         	-- Master Clock
			Enc_Clr 			: in		std_logic;
			Enc_Set_Posd2 		: in		std_logic;
			ENC_Ch			: in		std_logic_vector( 1 downto 0 );		-- A and B Phases
			Target			: in		std_logic_Vector( 22 downto 0 );
			Target_Cmp_Val		: in		std_logic_Vector( 22 downto 0 );
			Target_Cmp		: buffer	std_logic;
			Enc_Cnt			: buffer	std_logic_vector( 22 downto 0 ) );
	end component PIV_Enc;
	---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
begin
	imr <= '0' after 555 ns;

	---------------------------------------------------------------------------------------------------
	U : PIV_Enc
		port map(
			imr			=> imr,
			CLK40      	=> clk40,
			Enc_Clr 		=> Enc_Clr,
			Enc_Set_Posd2 	=> Enc_Set_Posd2,
			ENC_Ch		=> Enc_Ch,
			Target		=> Target,
			Target_Cmp_Val	=> Target_Cmp_Val,
			Target_Cmp	=> Target_Cmp,
			Enc_Cnt		=> Enc_Cnt );
	---------------------------------------------------------------------------------------------------
	---------------------------------------------------------------------------------------------------
	clock40_proc : process
	begin
		clk40 <= '0';
		wait for 12 ns;
		clk40 <= '1';
		wait for 13 ns;
	end process;
	---------------------------------------------------------------------------------------------------

	---------------------------------------------------------------------------------------------------
	Enc_Proc : process

	begin
		for i in 0 to 15 loop
			wait until rising_Edge( clk40 );
		end loop;
		wait for 5 ns;
		case Enc_Ch is
			when "00" 	=> Enc_Ch <= "01";
			when "01" 	=> Enc_Ch <= "11";
			when "11" 	=> Enc_Ch <= "10";
			when "10" 	=> Enc_Ch <= "00";
			when others	=> Enc_Ch <= "00";
		end case;
	end process;
	---------------------------------------------------------------------------------------------------

	---------------------------------------------------------------------------------------------------
	tb_proc : process
		variable save_enc : integer;
	begin
			Target_Cmp_Val		<= conv_Std_logic_Vector( 50, 23 );
			Enc_Clr			<= '0';
			Enc_Set_Posd2		<= '0';
			
			Target		<= conv_Std_logic_Vector( 100, 23 );
			
			wait for 10 us;
			assert false report "Set Enc to 1/2 Value" severity note;
			wait until rising_Edge( clk40 );
			
			Save_Enc := conv_integer( Enc_Cnt( 22 downto 1 ) );
			Enc_Set_Posd2 <= '1' after 5 ns;
			wait until rising_Edge( clk40 );
			Enc_Set_Posd2 <= '0' after 5 ns;
			wait until rising_Edge( clk40 );
			assert ( Save_Enc = conv_integer( Enc_Cnt ))
				report "Enc /2 did not work"
				severity error;
				
			wait for 5 us;
			assert false report "Clear Enc " severity note;
			wait until rising_Edge( clk40 );
			Enc_Clr <= '1' after 5 ns;
			wait until rising_Edge( clk40 );
			Enc_Clr <= '0' after 5 ns;
			wait until rising_Edge( clk40 );
			assert( conv_integer( Enc_Cnt ) = 0 )
				report "Enc CLear did not work"
				severity error;
			assert false report "Wait until Enc reaches 100" severity note;
			wait until rising_Edge( Target_Cmp );
			
			wait for 10 us;
			
			assert false
				report "End of Simulations"
				severity failure;
	end process;
			
			

	---------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
end Test_Bench;			-- PIV_Enc.VHD
----------------------------------------------------------------------------------------------------


