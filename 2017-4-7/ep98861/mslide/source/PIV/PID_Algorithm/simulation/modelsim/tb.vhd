     
library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

entity tb is
end tb;

architecture test_bench of tb is
	constant P_Gen : integer := 1;
	constant D_Gen : integer := 1;
	constant I_Gen : integer := 1;

	signal imr 			: std_logic := '1';
	signal CLK40      	 	: std_logic;                         	-- Master Clock
	signal Move_Clk		: std_logic;
	signal Move_Start		: std_logic;
	signal Move_Enable		: std_logic;
	signal KI_Init			: std_logic_Vector( 31 downto 0 );
	signal DP				: std_logic_Vector( 19 downto 0 );		-- Target Positon
	signal PS				: std_Logic_Vector( 19 downto 0 );		-- Current Positin
	signal KP				: std_Logic_Vector( 31 downto 0 );		-- Postion loop Gain 
	signal KI				: std_Logic_Vector( 31 downto 0 );		-- Velocity loop Integral Gain
	signal KD				: std_logic_Vector( 31 downto 0 );		-- Velocity Loop Overall Gain
	signal PE				: std_logic_vector( 19 downto 0 );
	signal PE_DIF			: std_logic_vector( 19 downto 0 );
	signal int_Cmd_Out			: std_logic_Vector( 10 downto 0 );
	signal Up_Dir			: std_logic;
	
	signal Cmd_Out			: std_logic_Vector( 10 downto 0 );
	signal dp_del 			: std_logic_Vector( 19 downto 0 );
	signal Move_Disable		: std_logic;
	signal Stalled			: std_logic;
	signal	PID_State 	: std_logic_Vector( 2 downto 0 );
	signal	Multa		: std_logic_Vector( 23 downto 0 );
	signal	Multb		: std_logic_Vector( 27 downto 0 );
	signal	Multr		: std_logic_Vector( 51 downto 0 );
	signal	KPKIKD_Acc 	: std_Logic_Vector( 51 downto 0 );

	---------------------------------------------------------------------------------------------------
	component PID_Algorithm is 
		port( 
			imr       		: in      std_logic;				     	-- Master Reset
			CLK40      	 	: in      std_logic;                         	-- Master Clock
			Move_Clk			: in		std_logic;
			Move_Start		: in		std_logic;
			Move_Enable		: in		std_logic;
			Up_Dir			: in		std_logic;
			KI_Init			: in		std_logic_Vector( 31 downto 0 );
			DP				: in		std_logic_Vector( 19 downto 0 );		-- Target Positon
			PS				: in		std_Logic_Vector( 19 downto 0 );		-- Current Positin
			KP				: in		std_Logic_Vector( 31 downto 0 );		-- Postion loop Gain 
			KI				: in		std_Logic_Vector( 31 downto 0 );		-- Velocity loop Integral Gain
			KD				: in		std_logic_Vector( 31 downto 0 );		-- Velocity Loop Overall Gain
			PE_Out			: buffer	std_logic_vector( 19 downto 0 );
			Cmd_Out			: buffer	std_logic_Vector( 10 downto 0 );		-- Position Error
			PE_DIF_Out		: buffer	std_logic_vector( 19 downto 0 );
			Stalled			: buffer	std_logic );
--			PID_State_Out 		: buffer  std_logic_Vector(  2 downto 0 );
--			Multa			: buffer  std_logic_Vector( 23 downto 0 );
--			Multb			: buffer  std_logic_Vector( 27 downto 0 );
--			Multr			: buffer  std_logic_Vector( 51 downto 0 );
--			KPKIKD_Acc 		: buffer  std_Logic_Vector( 51 downto 0 ) );
			
	end component PID_Algorithm;
	---------------------------------------------------------------------------------------------------
begin
	imr <= '0' after 555 ns;

	---------------------------------------------------------------------------------------------------
	clk40_Proc : process
	begin
		clk40 <= '0';
		wait for 12 ns;
		clk40 <= '1';
		wait for 13 ns;
	end process;
	---------------------------------------------------------------------------------------------------

     ----------------------------------------------------------------------------------------------
	Move_Clk_Proc : process
	begin
		Move_Clk <= '0';
		wait until rising_edge( clk40 );
		Move_Clk <= '1' after 5 ns;
		wait until rising_edge( clk40 );
		Move_Clk <= '0' after 5 ns;
		for i in 0 to 7 loop
			wait until rising_edge( clk40 );
		end loop;
	end process;
     ----------------------------------------------------------------------------------------------
	dir_proc : process( imr, clk40 )
	begin
		if( imr = '1' )
			then Up_Dir <= '0';
		elsif(( clk40'event ) and ( clk40 = '1' )) then
			if( Move_Start = '1' ) then
				if( DP > PS )
					then Up_Dir <= '1';
					else Up_Dir <= '0';
				end if;
			end if;
		end if;
	end process;
     ----------------------------------------------------------------------------------------------

     ----------------------------------------------------------------------------------------------
	U : PID_Algorithm
		port map(
			imr				=> imr,
			clk40			=> clk40,
			Move_Clk			=> Move_Clk,
			Move_Start		=> Move_Start,
			Move_Enable		=> Move_Enable,
			KI_Init			=> KI_Init,
			Up_Dir			=> Up_Dir,
			DP				=> DP,
			PS				=> PS,
			KP				=> KP,
			KI				=> KI,
			KD				=> KD,
			PE_Out			=> PE,
			PE_DIF_Out		=> PE_DIf,
			Cmd_Out			=> int_Cmd_Out,
			stalled			=> Stalled );
--			PID_State_Out 		=> PID_State,
--			Multa			=> Multa,
--			Multb			=> Multb,
--			Multr			=> Multr,
--			KPKIKD_Acc 		=> KPKIKD_Acc );
			
     ----------------------------------------------------------------------------------------------
			
	cmd_out_proc : process( clk40 )
	begin
		if( falling_edge( clk40 )) then
			Cmd_Out <= Int_Cmd_Out;
		end if;
	end process;

--     ----------------------------------------------------------------------------------------------
--	Move_Start_Proc : process( imr, clk40 )
--	begin
--		if( imr = '1' ) then
--			Move_Start	 <= '0';
--			DP_Del		<= conv_Std_Logic_Vector( 0, 20 );
--		elsif(( clk40'event ) and ( clk40 = '1' )) then
--			DP_DEL	<= DP after 5 ns;
--			
--			if( DP /= DP_Del )
--				then Move_Start <= '1' after 5 ns;
--				else Move_Start <= '0' after 5 ns;
--			end if;
--		end if;
--	end process;		
--     ----------------------------------------------------------------------------------------------
	

--     ----------------------------------------------------------------------------------------------
--	enc_proc : process( imr, clk40 )
--	begin
--		if( imr = '1' )
--			then ps <= conv_Std_logic_Vector( 0, 20 );	
--		elsif( rising_Edge( clk40 )) then
--			if(( Move_Disable = '0' ) and ( Move_Enable = '1' )) then
--				if( Cmd_Out(10) = '0' )	-- Positive
--					then PS <= PS + 1;
--					else PS <= PS - 1;
--				end if;
--			end if;		
--		end if;
--	end process;
--     ----------------------------------------------------------------------------------------------

     ----------------------------------------------------------------------------------------------
	tb_proc : process
		variable Target_Cmd : integer;
		variable PE_Last 	: integer;
		variable step_size 	: integer;
		variable Out_Cmd 	: integer range -4096 to 4095;
	begin
		Move_Enable	<= '0';
		Move_Disable	<= '0';
		Move_Start	<= '0';
		
		DP			<= conv_Std_logic_Vector( 0, 20 );			-- Desired Position
		KP			<= conv_Std_logic_Vector( 0, 32 );
		KI			<= conv_Std_logic_Vector( 0, 32 );
		KD			<= conv_Std_logic_Vector( 0, 32 );
		KI_Init 		<= conv_Std_Logic_Vector( 0, 32 );
		ps 			<= conv_Std_logic_Vector( 0, 20 );		
		
		wait for 1 us;
		wait until rising_Edge( clk40 );
		
		assert false report "---------------------------------------------------------------------------------------------------------" severity note;
		if( P_Gen > 0 ) then
			-- To check P, Setr I and D to zero
			-- apply a constant error, ( DP-PS ) = constant
			-- and the output should be constant.
			assert false report "0) Check P and Apply a constant PE" severity note;
			for i in -5 to 5 loop
					for j in 1 to 8 loop
						wait until rising_Edge( clk40 );
						Move_Start	<= '1' after 5 ns;
						Move_Enable	<= '1' after 5 ns;
						DP 			<= conv_Std_logic_Vector(      200, 20 ) after 5 ns;
						PS			<= conv_Std_logic_Vector(     i*10, 20 ) after 5 ns;
						KP 			<= conv_Std_logic_Vector(   8*j*256, 32 ) after 5 ns;		
						KI 			<= conv_Std_logic_Vector(        0, 32 ) after 5 ns;	
						KI_Init 		<= conv_Std_Logic_Vector(        0, 32 ) after 5 ns;	
						KD 			<= conv_Std_logic_Vector(    0*256, 32 ) after 5 ns;		
						wait until rising_Edge( clk40 );
						Move_Start <= '0' after 5 ns;
						
						wait for 2 us;
						Target_Cmd := (conv_integer( dp - ps ) * conv_integer( kp )) / 65536;
						assert( conv_integer( Cmd_Out ) = Target_Cmd )
							report "Bad Command Out"
							severity error;
						wait until rising_Edge( clk40 );
						Move_Enable <= '0' after 4 ns;
						
						wait for 2 us;
					end loop;
			end loop;
		end if;
		assert false report "---------------------------------------------------------------------------------------------------------" severity note;
		
		assert false report "---------------------------------------------------------------------------------------------------------" severity note;
		if( D_Gen > 0 ) then
			-- To check D, Set P and I to zero
			-- apply an error that has a constant change
			-- and the output should remain constant
			assert false report "1) Check D and Apply a constant changing PE" severity note;
			for j in 8 to 16 loop
				for i in -5 to 5 loop
					wait until rising_Edge( clk40 );
					Move_Start	<= '1' after 5 ns;
					Move_Enable	<= '1' after 5 ns;
					DP 			<= conv_Std_logic_Vector(      00, 20 ) after 5 ns;
					PS			<= conv_Std_logic_Vector(        0, 20 ) after 5 ns;
					KP 			<= conv_Std_logic_Vector(    0*256, 32 ) after 5 ns;		
					KI 			<= conv_Std_logic_Vector(        0, 32 ) after 5 ns;	
					KI_Init 		<= conv_Std_Logic_Vector(        0, 32 ) after 5 ns;	
					KD 			<= conv_Std_logic_Vector(  j*8*256, 32 ) after 5 ns;		
					wait until rising_Edge( clk40 );
					Move_Start <= '0' after 5 ns;
					step_size := 20*i;
						
					for k in 0 to 12 loop
						wait until( rising_Edge( clk40 ) and ( Move_CLk = '1' ));
						PS <= PS - conv_Std_logic_Vector( step_size, 20 ) after 5 ns;
						Target_Cmd := (step_size * conv_integer( KD )) / 65536;
						Out_Cmd := conv_integer( Cmd_Out );
						if( Cmd_Out(10) = '1' ) 							-- Negative Number
							then Out_Cmd := conv_integer( Cmd_Out ) - 2048;
						end if;
						
						if( k > 2 ) then
							if( Target_Cmd >= Out_Cmd ) then
								assert(( Target_Cmd - Out_Cmd ) < 2 )
									report "Bad Command Out"
									severity error;
							else
								assert(( Out_Cmd -Target_Cmd ) < 2 )
									report "Bad Command Out"
									severity error;
							end if;
						end if;
					wait until rising_Edge( clk40 );
					end loop;
					Move_Enable <= '0' after 4 ns;
					wait for 100 ns;
				end loop;
			end loop;
		end if;
		assert false report "---------------------------------------------------------------------------------------------------------" severity note;

		assert false report "---------------------------------------------------------------------------------------------------------" severity note;
		if( I_Gen > 0 ) then
			-- To check I, Set P and D to zero
			-- apply a constant
			-- and the output should increase
			assert false report "1) Check I and Apply a constant PE" severity note;
			for j in 8 to 8 loop
				for i in -5 to 5 loop
					wait until rising_Edge( clk40 );
					Move_Start	<= '1' after 5 ns;
					Move_Enable	<= '1' after 5 ns;
					DP 			<= conv_Std_logic_Vector(      200, 20 ) after 5 ns;
					PS			<= conv_Std_logic_Vector(        0, 20 ) after 5 ns;
					KP 			<= conv_Std_logic_Vector(    0*256, 32 ) after 5 ns;		
					KI 			<= conv_Std_logic_Vector(      256, 32 ) after 5 ns;		-- Set to X1
					KI_Init 		<= conv_Std_Logic_Vector(        0, 32 ) after 5 ns;	
					KD 			<= conv_Std_logic_Vector(    0*256, 32 ) after 5 ns;		
					wait until rising_Edge( clk40 );
					Move_Start <= '0' after 5 ns;
					step_size := 20*i;
						
					for k in 0 to 12 loop
						wait until( rising_Edge( clk40 ) and ( Move_CLk = '1' ));
						Target_Cmd := (step_size * conv_integer( KD )) / 65536;
						Out_Cmd := conv_integer( Cmd_Out );
						if( Cmd_Out(10) = '1' ) 							-- Negative Number
							then Out_Cmd := conv_integer( Cmd_Out ) - 2048;
						end if;
						
--						if( k > 2 ) then
--							if( Target_Cmd >= Out_Cmd ) then
--								assert(( Target_Cmd - Out_Cmd ) < 2 )
--									report "Bad Command Out"
--									severity error;
--							else
--								assert(( Out_Cmd -Target_Cmd ) < 2 )
--									report "Bad Command Out"
--									severity error;
--							end if;
--						end if;
					wait until rising_Edge( clk40 );
					end loop;
					Move_Enable <= '0' after 4 ns;
					wait for 100 ns;
				end loop;
			end loop;
		end if;
		assert false report "---------------------------------------------------------------------------------------------------------" severity note;

		assert false report "---------------------------------------------------------------------------------------------------------" severity note;
		assert false report "Premature End of Simulation" severity failure;
		
--		for j in 0 to 10 loop
--			
--			wait until rising_Edge( clk40 );
--			Move_Enable 	<= '1' after 5 ns;
--			case j is
--				when 0 => 
--						
--				when 1 => assert false report "1) Check only 1/2 KP" severity note;
--						KP 		<= conv_Std_logic_Vector(  128*256, 32 ) after 5 ns;		-- Set to X1
--						KD 		<= conv_Std_logic_Vector(    0*256, 32 ) after 5 ns;		
--						KI 		<= conv_Std_logic_Vector(        0, 32 ) after 5 ns;		
--						KI_Init 	<= conv_Std_Logic_Vector(        0, 32 ) after 5 ns;	
--						DP 		<= conv_Std_logic_Vector(      400, 20 ) after 5 ns;
--				when 2 => assert false report "2) Check only 1/4 for KP" severity note;
--						KP 		<= conv_Std_logic_Vector(   64*256, 32 ) after 5 ns;		-- Set to X1
--						KD 		<= conv_Std_logic_Vector(    0*256, 32 ) after 5 ns;		
--						KI 		<= conv_Std_logic_Vector(        0, 32 ) after 5 ns;		
--						KI_Init 	<= conv_Std_Logic_Vector(        0, 32 ) after 5 ns;	
--						DP 		<= conv_Std_logic_Vector(      200, 20 ) after 5 ns;
--				when 3 => assert false report "3) Check only 1/8 for KP" severity note;
--						KP 		<= conv_Std_logic_Vector(   32*256, 32 ) after 5 ns;		-- Set to X1
--						KD 		<= conv_Std_logic_Vector(    0*256, 32 ) after 5 ns;		
--						KI 		<= conv_Std_logic_Vector(        0, 32 ) after 5 ns;		
--						KI_Init 	<= conv_Std_Logic_Vector(        0, 32 ) after 5 ns;	
--						DP 		<= conv_Std_logic_Vector(        0, 20 ) after 5 ns;
--				when 4 => assert false report "4) Check only 1/256 for KI" severity note;
--						KP 		<= conv_Std_logic_Vector(        0, 32 ) after 5 ns;		-- Set to X1
--						KD 		<= conv_Std_logic_Vector(    0*256, 32 ) after 5 ns;		
--						KI 		<= conv_Std_logic_Vector(      256, 32 ) after 5 ns;		-- Set to X1
--						KI_Init 	<= conv_Std_Logic_Vector(   131702, 32 ) after 5 ns;	
--						DP 		<= conv_Std_logic_Vector(      200, 20 ) after 5 ns;
--				when 5 => assert false report "5) Check only 1x for KI" severity note;
--						KP 		<= conv_Std_logic_Vector(        0, 32 ) after 5 ns;		-- Set to X1
--						KD 		<= conv_Std_logic_Vector(    0*256, 32 ) after 5 ns;		
--						KI 		<= conv_Std_logic_Vector(      128, 32 ) after 5 ns;		-- Set to X1
--						KI_Init 	<= conv_Std_Logic_Vector(   262144, 32 ) after 5 ns;	
--						DP 		<= conv_Std_logic_Vector(        0, 20 ) after 5 ns;
--						
--				when 6 => assert false report "6) Check only Unity for KD" severity note;
--						KP 		<= conv_Std_logic_Vector(       0, 32 ) after 5 ns;		-- Set to 1
--						KD 		<= conv_Std_logic_Vector(     256, 32 ) after 5 ns;		
--						KI 		<= conv_Std_logic_Vector(       0, 32 ) after 5 ns;	
--						KI_Init 	<= conv_Std_Logic_Vector(       0, 32 ) after 5 ns;	
--						DP 		<= conv_Std_logic_Vector(     200, 20 ) after 5 ns;
--						
--				when 7=> assert false report "7) Check only for KP to Clamp at +1020" severity note;
--						Move_Disable	<= '1' after 5 ns;
--						KP 		<= conv_Std_logic_Vector(1024*256, 32 ) after 5 ns;		-- Set to 1
--						KD 		<= conv_Std_logic_Vector(   0*256, 32 ) after 5 ns;		
--						KI 		<= conv_Std_logic_Vector(       0, 32 ) after 5 ns;	
--						KI_Init 	<= conv_Std_Logic_Vector(       0, 32 ) after 5 ns;	
--						DP 		<= conv_Std_logic_Vector(  100000, 20 ) after 5 ns;
--				when 8 => assert false report "8) Check only for KP to Clamp at -1020" severity note;
--						Move_Disable	<= '1' after 5 ns;
--						KP 		<= conv_Std_logic_Vector(1024*256, 32 ) after 5 ns;		-- Set to 1
--						KD 		<= conv_Std_logic_Vector(   0*256, 32 ) after 5 ns;		
--						KI 		<= conv_Std_logic_Vector(       0, 32 ) after 5 ns;	
--						KI_Init 	<= conv_Std_Logic_Vector(       0, 32 ) after 5 ns;	
--						DP 		<= conv_Std_logic_Vector( -100000, 20 ) after 5 ns;
--
--				when 9 => assert false report "9) Check only for KI to Clamp at +1020" severity note;
--						Move_Disable	<= '1' after 5 ns;
--						KP 		<= conv_Std_logic_Vector(       0, 32 ) after 5 ns;		-- Set to 1
--						KD 		<= conv_Std_logic_Vector(   0*256, 32 ) after 5 ns;		
--						KI 		<= conv_Std_logic_Vector(     256, 32 ) after 5 ns;	
--						KI_Init 	<= conv_Std_Logic_Vector(  131702, 32 ) after 5 ns;	
--						DP 		<= conv_Std_logic_Vector(  100000, 20 ) after 5 ns;
--				when 10 => assert false report "10) Check only for KI to Clamp at -1020" severity note;
--						Move_Disable	<= '1' after 5 ns;
--						KP 		<= conv_Std_logic_Vector(       0, 32 ) after 5 ns;		-- Set to 1
--						KD 		<= conv_Std_logic_Vector(   0*256, 32 ) after 5 ns;		
--						KI 		<= conv_Std_logic_Vector(     256, 32 ) after 5 ns;	
--						KI_Init 	<= conv_Std_Logic_Vector(  131702, 32 ) after 5 ns;	
--						DP 		<= conv_Std_logic_Vector( -100000, 20 ) after 5 ns;
--						
--						
----				when 6 => assert false report "Check only 1x for KI" severity note;
----						KP <= conv_Std_logic_Vector(   256, 32 ) after 5 ns;		-- Set to X1
----						KI <= conv_Std_logic_Vector(   256, 32 ) after 5 ns;		-- Set to X1
----						KD <= conv_Std_logic_Vector(    256, 32 ) after 5 ns;		
----						DP <= conv_Std_logic_Vector( 150000, 23 ) after 5 ns;
--				when others => 
--			end case;
--
--			
--			wait for 100 ns;
--			case j is
--				when 7 		=> wait until (( Cmd_Out'event ) and ( conv_integer( Cmd_Out ) > 512 )); wait for 10 us;
--				when 8 		=> wait until (( Cmd_Out'event ) and ( conv_integer( Cmd_Out ) < 16#500# )); wait for 10 us;
--				when 9 		=> wait until (( Cmd_Out'event ) and ( conv_integer( Cmd_Out ) > 512 )); wait for 10 us;
--				when 10 		=> wait until (( Cmd_Out'event ) and ( conv_integer( Cmd_Out ) < 16#500# )); wait for 10 us;
--				when others 	=> wait until (( PE'event ) and ( conv_integer( PE ) < 10 ) and ( conv_integer( PE ) > -10 ));
--			end case;
--			wait until rising_Edge( clk40 );
--			Move_Enable <= '0' after 5 ns;
--			
--				
--			wait for 5 us;
--		end loop;
--		
--		wait for 100 us;
		assert false
			report "End of Simulation"
			severity failure;
	end process;
     ----------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
end test_Bench;			-- PIV_Algorithm.VHD
----------------------------------------------------------------------------------------------------


