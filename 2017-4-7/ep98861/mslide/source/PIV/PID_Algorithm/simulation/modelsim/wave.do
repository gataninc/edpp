onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic /tb/imr
add wave -noupdate -format Logic /tb/clk40
add wave -noupdate -format Logic /tb/move_clk
add wave -noupdate -format Logic /tb/move_start
add wave -noupdate -format Logic /tb/move_enable
add wave -noupdate -format Logic /tb/up_dir
add wave -noupdate -format Literal -radix decimal /tb/dp
add wave -noupdate -format Analog-Step -height 74 -max 1300.0 -min -1300.0 -radix decimal /tb/ps
add wave -noupdate -format Literal -radix decimal /tb/kp
add wave -noupdate -format Literal -radix decimal /tb/ki
add wave -noupdate -format Literal -radix decimal /tb/kd
add wave -noupdate -format Literal -radix decimal /tb/ki_init
add wave -noupdate -format Analog-Step -height 74 -max 1399.9999999999486 -min -524068.0 -radix decimal /tb/pe
add wave -noupdate -format Literal -radix decimal /tb/pe_dif
add wave -noupdate -format Analog-Step -height 74 -max 1023.0 -min -1024.0 -radix decimal /tb/cmd_out
add wave -noupdate -format Literal -radix decimal /tb/PID_State
add wave -noupdate -format Literal -radix decimal /tb/Multa
add wave -noupdate -format Literal -radix decimal /tb/Multb
add wave -noupdate -format Literal -radix decimal /tb/Multr
add wave -noupdate -format Literal -radix decimal /tb/KPKIKD_Acc

TreeUpdate [SetDefaultTree]

WaveRestoreCursors {{Cursor 1} {2042 ns} 0}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {12104 ns}
