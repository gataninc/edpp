-------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	Piezo_PIV.vhd
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : September 21, 2007
--   Board          : XScope Instrument Control Board
--   Schematic      : 4035.007.27000
--
--   Top Level      : 4035.009.99330, S/W, XScope, Instrument Control
--   Description:
--		Top level module to control a Piezo-electric axis for the XSCope
--
--	History
--		09042101:
--			Home changed to use PID to move to Full Minimum
--			Once at Full Min, Home Cmd is used to hold it there to register the positon
--			Then PID is used to move to Full Maximum
--			Once at Full Maximum, HOME_CMD is used to hold it there to register the positions
--			Then Home resumes normal operation
--			
--			Stall Det has a 100ms time-out to get the axis to start moving.
--			Once that has expired, if the Positin Error > 1mm, a stall is indicated
--		09040601: 
--			Constants AB4_POS_HOME_CMD and AB4_NEG_HOME_CMD changed from 750 to 850
--			to guarantee to hit the hard stops
--		090123xx - MCS
--		Eliminated Enc_Index
--			Module: PAxis.vhd
--				During Home
--				1st Move to Min sets Pos to 0
--				Then Move to Max sets Pos to Pos/2
--				Remainder the same
--				Cleanup to the State Machine
--				Motor_Off Not used, so eliminated
--				Motor_On no-longer visible outside of module
--				Motor_Slow no longer visible outside of module
--				Following HOME, axis is moved to position 0
--			Module: PAxis_Encoder
--				Added Enc_Clr to Clear the Count Value
--				Added Enc_Set_PosD2 to Set the Count Value to 1/2 the current value
--				Removed MOTOR_OFF and any supporting signals
--				Changed Target to +/- 1um for CMP_TAR
--				Left "Motor_On" to +/- 2.5 and 5um depending on mode
---		09012205 - MCS
--			Stretch Count was changed to 32 bits @ 25ns
---		09012202 - MCS
--		* CHanged Move Stretch COunt Max to 12 bits ( from 8 )
--		* Move Time is the "Move Flag" not Move Active
---		09012201 - MCS
--		* Added Retry Counter
--			Target bits 26: 23 contain the maximum number of retries for an auto-correct
--			If AUTO_CORRECT_DISABLE = 0 - Will try infinite retries
--			if Auto_Correct_DISABLE = 1 - Will try the amount of retries specified by The Retry Cnt Max in the Target Register
--		* Added "Move_Stretch" which stretches the "Move_Flag" by multiples of 1ms after the "Move_Cmd" is terminated
--		* Added "Move_Error" for any states which need additional attention
--		090116 - MCS
--			Added TAR_AUTO_CORRECT_DISABLE to the Target Register bit to prevent auto-correction for positon drift
--			Added Move_Flag = 0 to Move_Time Register Enable ( Move_Flag = 0 OR State = Idle )

---------------------------------------------------------------------------------------------------
library LPM;
     use lpm.lpm_components.all;

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;
---------------------------------------------------------------------------------------------------
entity PIV_Piezo is 
	port( 
		IMR       			: in      std_logic;					-- Master Reset
		CLK40				: in		std_logic;					-- Master Clock
		Axis					: in		std_logic_vector( 1 downto 0 );
		PIV_CLK				: in		std_Logic;
		ms_clk				: in		std_logic;
		Cmd_Home				: in		std_logic;
		Door_Closed 			: in		std_logic_vector( 1 downto 0 );
		Service_Mode			: in		std_logic;
		Crash				: in		std_Logic;
		Fault				: in		std_logic;
		Enc_Ch				: in		std_Logic_Vector( 1 downto 0 );	-- Encoder Channel
		PIV_Index				: in		std_logic_Vector( 2 downto 0 );
		Target				: in		std_Logic_Vector( 31 downto 0 );	-- Encoder Target
		Slope_Acc				: in		std_Logic_Vector( 31 downto 0 );		-- Acceleration Slope
		Slope_DeAcc			: in		std_logic_Vector( 31 downto 0 );		-- DeCelleration Slope		
		Vel_Max				: in		std_logic_Vector( 31 downto 0 );		-- DeCelleration Slope
		Vel_Min				: in		std_logic_Vector( 31 downto 0 );		-- DeCelleration Slope
		KP					: in		std_Logic_Vector( 31 downto 0 );		-- Postion loop Gain 
		KI					: in		std_Logic_Vector( 31 downto 0 );		-- Velocity loop Integral Gain
		KD					: in		std_logic_Vector( 31 downto 0 );		-- Velocity Loop Overall Gain
		KI_Init				: in		std_logic_Vector( 31 downto 0 );
		Settle_Time			: in		std_logic_Vector( 31 downto 0 );
		Home_Cmd				: in		std_logic_Vector( 31 downto 0 );
		Enc_Pos				: buffer	std_logic_vector( 31 downto 0 );
		SPI_CS				: buffer	std_Logic;
		SPI_CLOCK				: buffer	std_logic;
		SPI_DATA				: buffer	std_Logic;
		Motor_Enable			: buffer	std_logic;
		Motor_Error			: buffer	std_logic;
		Move_Flag				: buffer	std_logic;
		Move_Time				: buffer	std_logic_Vector( 31 downto 0 );
		Idle_time 			: buffer	std_logic_Vector( 31 downto 0 );
		Max_Pos				: buffer	std_logic_Vector( 31 downto 0 );
		Min_Pos				: buffer	std_logic_Vector( 31 downto 0 );
		Dss_Data				: buffer	std_logic_Vector( 15 downto 0 ) );	
end PIV_Piezo;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
architecture Behavioral of PIV_Piezo is
	-- Constant Declarations ---------------------------------------------------------------------
	
	constant XYR_ENC_LSB 			: integer := 500;	-- 0.5um for XY and R 
	constant T_ENC_LSB				: integer := 400;	-- 0.4um for Turret 

	-- During a Home, When the Encoder is to be Read, this is the Command Voltage applied to the motor to hold it into positoin
	constant AB4_POS_HOME_CMD		: integer :=  850;	
	constant AB4_NEG_HOME_CMD		: integer := -850;
	
	--										  m  u  n	
	constant Motor_Stop_XYR_Val 		: integer :=        1000 / XYR_ENC_LSB;	-- 1.0um
	constant Motor_Stop_T_Val		: integer :=        1000 / T_ENC_LSB;	-- 1.0um
	constant Motor_Start_XYR_Val 		: integer :=        2500 / XYR_ENC_LSB;	-- 2.5um
	constant Motor_Start_T_Val		: integer :=        2500 / T_ENC_LSB;	-- 2.5um

	-- Offset to use after a Home
	--										  m  u  n	
	constant mm_XYR_MM_Offset		: integer :=      250000 / XYR_ENC_LSB;	-- 0.25mm
	constant mm_T_MM_Offset			: integer :=      250000 /  T_ENC_LSB;	-- 0.25mm

		-- Use the finer encoder reso for mm_Max_pos/mm_min_pos, only used for "Home"
	--										  m  u  n	
	constant mm_Min_Pos				: integer :=  -110000000 / T_ENC_LSB;	-- in units of nm
	constant mm_Max_Pos				: integer :=   110000000 / T_ENC_LSB;

	constant Pause_Cnt_Max			: integer := 250;	-- @ 1ms_Clk
	constant Home_Cnt_Max			: integer := 2000;	-- @ 1ms clock
	
	-- Axis Definitions --------------------------------------------------------------------------
	-- Copy to PAxis_Encoder.VhD
	constant AXIS_X				: integer := 0;
	constant AXIS_Y				: integer := 1;
	constant AXIS_R				: integer := 2;
	constant AXIS_T				: integer := 3;
	-- Axis Definitions --------------------------------------------------------------------------	
	

	-- Target Register Bit Definitions -----------------------------------------------------------
	constant TAR_MOTOR_EN				: integer := 31;
	constant TAR_MOTOR_BI				: integer := 30;
	constant TAR_MOTOR_HOME				: integer := 29;
	constant TAR_MOTOR_DIRECT			: integer := 28;		-- Normally 0, 1 use ENC_TAR between +/-400
--	constant TAR_MOTOR_STOP_MODE1 		: integer := 27;
--	constant TAR_MOTOR_STOP_MODE0 		: integer := 26;
--	constant TAR_FINE_MODE				: integer := 27;
--	constant TAR_RETRY_MSB				: integer := 26;
--	constant TAR_RETRY_LSB				: integer := 23;
	constant TAR_POS_MSB 				: integer := 19;
	constant TAR_POS_LSB 				: integer := 0;
	constant TAR_POS_WIDTH				: integer := TAR_POS_MSB - TAR_POS_LSB + 1;
	-- Target Register Bit Definitions -----------------------------------------------------------

--	constant Int_Move_Cnt_max 		: integer := 120000000;

	-- Axis State Machine Definitions ------------------------------------------------------------
	constant ST_IDLE				: integer := 0;
	constant ST_Disabled			: integer := 1;

	constant ST_CRASH				: integer := 2;
	constant ST_Fault				: integer := 3;	
	constant ST_Stalled				: integer := 4;
	constant ST_Interlock			: integer := 5;


	constant ST_Move_Start			: integer := 6;
	constant St_Move_Mid			: integer := 7;
	constant ST_Move_Settle			: integer := 8;
	constant ST_Move_Restart			: integer := 9;
	
	constant ST_Home_Start			: integer := 10;	-- Drive AB4 Negative
	constant St_Home_Move_Min 		: integer := 12;	-- Drive AB4 Negative
	constant St_Home_Move_Min_Done 	: integer := 13;	-- Drive AB4 Off
	constant St_Home_Clear			: integer := 14;	-- Drive AB4 Negative

--	constant St_Home_Min_Param 		: integer := 11;
	constant St_Home_Max_Param 		: integer := 15;
	constant St_Home_Move_Max 		: integer := 16;
	constant St_Home_Set_Enc 		: integer := 18;
	constant St_Home_Set_Max 		: integer := 19;	
	constant St_Home_Move_Max_Done 	: integer := 17;
	
--	constant St_Home_Set_Min 		: integer := 20;
	constant St_Home_Zero_Param 		: integer := 21;
	constant St_Home_Zero 			: integer := 22;
	constant ST_Home_Done			: integer := 23;
	constant ST_BI_POS_CLR			: integer := 24;
	constant ST_BI_POS_MOVE 			: integer := 25;
	constant ST_BI_POS_WAIT			: integer := 26;
	constant ST_BI_NEG_CLR			: integer := 27;
	constant ST_BI_NEG_MOVE			: integer := 28;
	constant ST_BI_NEG_WAIT			: integer := 29;
	constant ST_MANUAL				: integer := 30;
	-- Axis State Machine Definitions ------------------------------------------------------------
	
	-- Constant Declarations ---------------------------------------------------------------------

	-- Signal Declarations -----------------------------------------------------------------------
	signal Axis_Seq				: integer range 0 to 31;

	signal BI_Wait_Cnt 				: std_Logic_vector( 31 downto 0 );

	signal Cmd_Home_Latch			: std_logic;

	signal Enc_Clr					: std_Logic;
	signal Enc_Set_Pos2d			: std_logic;
	signal Enc_Cnt					: std_logic_Vector( TAR_POS_WIDTH-1 downto 0 );
	signal Enc_Cnt_Latch			: std_logic_Vector( TAR_POS_WIDTH-1 downto 0 );

	signal Home_Status				: std_logic;
	signal idle_time_cntr 			: std_Logic_Vector( 31 downto 0 );


	signal PID_Start_Cmd			: std_logic;
	signal PIV_Move_Flag			: std_logic;
	signal Move_Flag_Del			: std_logic;
	signal Move_Time_Cntr			: std_logic_Vector( 31 downto 0 );

	signal Profile_Pos				: std_logic_Vector( TAR_POS_WIDTH-1 downto 0 );
	signal Profile_Start_Cmd			: std_logic;
	signal Profile_Vel				: std_logic_Vector( 38 downto 0 );
	signal Piezo_PE				: std_logic_vector( TAR_POS_WIDTH-1 downto 0 );
	signal Piezo_PE_Diff			: std_logic_vector( TAR_POS_WIDTH-1 downto 0 );
	signal Pause_Cnt				: integer range 0 to Home_Cnt_Max;
	signal Pause_Tc				: std_logic;
	signal Home_Tc					: std_logic;
	signal Piezo_Cmd				: std_logic_Vector( 10 downto 0 );
	signal Profile_Pos_Mux			: std_logic_Vector( TAR_POS_WIDTH-1 downto 0 );
	
	signal Stall_Det				: std_logic;

	signal Target_Home_Del			: std_logic;
	signal Target_Mux				: std_logic_Vector( 31 downto 0 );
	signal Target_Pos				: std_logic_Vector( 31 downto 0 );
	signal Target_Error 			: std_logic_vector( TAR_POS_WIDTH-1 downto 0 );
	signal Target_Abs_Error 			: std_logic_Vector( TAR_POS_WIDTH-1 downto 0 );
	signal Motor_Stop				: std_Logic;
	signal Motor_Start				: std_logic;
	signal Motor_Stop_Mux 			: std_logic_Vector( TAR_POS_WIDTH-1 downto 0 );
	signal Motor_Start_Mux			: std_logic_Vector( Tar_Pos_Width-1 downto 0 );
	signal Piezo_Cmd_Mux			: std_Logic_Vector( 10 downto 0 );
	signal Settle_Tc 				: std_Logic;
	signal Settle_Cnt 				: std_logic_Vector( 31 downto 0 );
	signal PID_Enable				: std_logic;
	signal Target_Up_Dir			: std_logic;
	signal Profile_Active			: std_Logic;

	-- Signal Declarations -----------------------------------------------------------------------
	----------------------------------------------------------------------------------------------
	component PIV_AB4SPI is 
		port( 
			IMR       	: in      std_logic;					-- Master Reset
			CLK40		: in		std_logic;					-- Master Clock
			Piezo_En		: in		std_logic;
			Piezo_Cmd		: in		std_logic_Vector( 10 downto 0 );
			SPI_CS		: buffer	std_logic;
			SPI_CLOCK		: buffer	std_logic;
			SPI_DATA		: buffer	std_logic );
	end component PIV_AB4SPI;
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	component PIV_Enc is 
		port( 
			imr       		: in      std_logic;				     	-- Master Reset
			CLK40      	 	: in      std_logic;                         	-- Master Clock
			Enc_Clr 			: in		std_logic;
			Enc_Set_Posd2 		: in		std_logic;
			ENC_Ch			: in		std_logic_vector(  1 downto 0 );		-- A and B Phases
			Target			: in		std_logic_Vector( 19 downto 0 );
			Motor_Stop_Val		: in		std_logic_Vector( 19 downto 0 );
			Motor_Start_Val	: in		std_logic_Vector( 19 downto 0 );
			Motor_Stop		: buffer	std_logic;
			Motor_Start		: buffer	std_logic;
			Up_Dir			: buffer	std_logic;
			Enc_Cnt			: buffer	std_logic_vector( 19 downto 0 ) );
	end component PIV_Enc;
	----------------------------------------------------------------------------------------------
	
	----------------------------------------------------------------------------------------------
	component PIV_Profile is 
		port( 
			imr       			: in      std_logic;				     	-- Master Reset
			CLK40      	 		: in      std_logic;                         	-- Master Clock
			Move_Clk				: in		std_Logic;						-- Movement Clock
			Move_Enable			: in		std_logic;
			Move_Start			: in		std_logic;						-- Start of Movement		
			Move_Stop				: in		std_logic;
			Pos_Start				: in		std_logic_Vector( 19 downto 0 );		-- Start Position
			Pos_Target			: in		std_logic_Vector( 19 downto 0 );		-- Target Position
			Pos_Max				: in		std_logic_Vector( 19 downto 0 );		-- Target Position
			Pos_Min				: in		std_logic_Vector( 19 downto 0 );		-- Target Position
			Slope_Acc				: in		std_Logic_Vector( 31 downto 0 );		-- Acceleration Slope
			Slope_Decell			: in		std_logic_Vector( 31 downto 0 );		-- DeCelleration Slope
			Vel_Max				: in		std_logic_Vector( 31 downto 0 );		-- DeCelleration Slope		
			Vel_Min				: in		std_logic_Vector( 31 downto 0 );		-- DeCelleration Slope		
			Profile_Active			: buffer	std_Logic;
			Pos_Out				: buffer	std_logic_Vector( 19 downto 0 );		-- Position based on Profile
			vel_out				: buffer	std_logic_Vector( 38 downto 0 ) );		-- Position based on ProfileProfile
	end component PIV_Profile;	
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	component PID_Algorithm is 
		port( 
			imr       		: in      std_logic;				     	-- Master Reset
			CLK40      	 	: in      std_logic;                         	-- Master Clock
			Move_Clk			: in		std_logic;
			Move_Start		: in		std_logic;
			Move_Enable		: in		std_logic;
			Up_Dir			: in 	std_logic;
			KI_Init			: in		std_logic_Vector( 31 downto 0 );
			DP				: in		std_logic_Vector( 19 downto 0 );		-- Target Positon
			PS				: in		std_Logic_Vector( 19 downto 0 );		-- Current Positin
			KP				: in		std_Logic_Vector( 31 downto 0 );		-- Postion loop Gain 
			KI				: in		std_Logic_Vector( 31 downto 0 );		-- Velocity loop Integral Gain
			KD				: in		std_logic_Vector( 31 downto 0 );		-- Velocity Loop Overall Gain
			PE_Out			: buffer	std_logic_Vector( 19 downto 0 );	
			PE_Dif_Out		: buffer	std_logic_Vector( 19 downto 0 );	
			PS_Out			: buffer	std_logic_Vector( 19 downto 0 );
			Cmd_Out			: buffer	std_logic_Vector( 10 downto 0 );	
			Stalled			: buffer	std_logic );
	end component PID_Algorithm;
	----------------------------------------------------------------------------------------------
	
begin
	Motor_Enable 	<= Target( TAR_MOTOR_EN );

	Settle_Tc 	<= '1' when ( Settle_Cnt = Settle_Time ) else '0';

	Pause_Tc		<= '1' when ( PAuse_Cnt = Pause_Cnt_Max ) else '0';
	Home_Tc		<= '1' when ( PAuse_Cnt = Home_Cnt_Max ) else '0';

	Motor_Stop_Mux <= conv_std_Logic_Vector( Motor_Stop_T_Val, TAR_POS_WIDTH ) when ( conv_Integer( Axis ) = Axis_T ) else
				   conv_std_Logic_Vector( Motor_Stop_XYR_Val, TAR_POS_WIDTH );

	Motor_Start_Mux <= conv_std_Logic_Vector( Motor_Start_T_Val, TAR_POS_WIDTH ) when ( conv_Integer( Axis ) = Axis_T ) else
				    conv_std_Logic_Vector( Motor_Start_XYR_Val, TAR_POS_WIDTH );


	-- Form the Mux for the Positional Targets based on the Axis Sequence States
	Target_Mux( 31 downto TAR_POS_MSB+1 ) <= Target( 31 downto TAR_POS_MSB+1 );

	with Axis_Seq select
		Target_Mux( TAR_POS_WIDTH-1 downto 0 ) <= 	
			conv_std_logic_Vector( mm_Min_Pos, TAR_POS_WIDTH ) 	when ST_Home_Start,
			conv_std_logic_Vector( mm_Min_Pos, TAR_POS_WIDTH ) 	when St_Home_Move_Min,
			conv_std_logic_Vector( mm_Min_Pos, TAR_POS_WIDTH ) 	when St_Home_Clear,
			conv_std_logic_Vector( mm_Max_Pos, TAR_POS_WIDTH ) 	when St_Home_Move_Min_Done,
			
			conv_Std_logic_Vector( mm_Max_Pos, TAR_POS_WIDTH ) 	when St_Home_Max_Param,
			conv_Std_logic_Vector( mm_Max_Pos, TAR_POS_WIDTH ) 	when St_Home_Move_Max,
			conv_Std_logic_Vector( mm_Max_Pos, TAR_POS_WIDTH ) 	when St_Home_Set_Enc,
			conv_Std_logic_Vector( mm_Max_Pos, TAR_POS_WIDTH ) 	when St_Home_Set_Max,
			conv_Std_logic_Vector( mm_Max_Pos, TAR_POS_WIDTH ) 	when St_Home_Move_Max_Done,
			
			conv_Std_Logic_Vector(          0, TAR_POS_WIDTH ) 	when St_Home_Zero_Param,
			conv_Std_Logic_Vector(          0, TAR_POS_WIDTH ) 	when St_Home_Zero,
			conv_Std_Logic_Vector(          0, TAR_POS_WIDTH ) 	when ST_Home_Done,
			Max_Pos( Tar_Pos_MSB downto TAR_POS_LSB ) 			when ST_BI_POS_CLR,
			Max_Pos( Tar_Pos_MSB downto TAR_POS_LSB ) 			when ST_BI_POS_MOVE,
			Max_Pos( Tar_Pos_MSB downto TAR_POS_LSB ) 			when ST_BI_POS_WAIT,
			Min_Pos( Tar_Pos_MSB downto TAR_POS_LSB ) 			when ST_BI_NEG_CLR,
			Min_Pos( Tar_Pos_MSB downto TAR_POS_LSB ) 			when ST_BI_NEG_MOVE,
			Min_Pos( Tar_Pos_MSB downto TAR_POS_LSB ) 			when ST_BI_NEG_WAIT,
			Target_Pos( TAR_POS_MSB downto TAR_POS_LSB )			when others;

	----------------------------------------------------------------------------------------------
	Enc_Clr <= '1' when ( Axis_Seq = St_Home_Start ) else
		      '1' when (( Axis_Seq = St_Home_Clear ) and ( ms_clk = '1' ) and ( Pause_Tc = '1' )) else
			 '0';
			      
	Enc_Set_Pos2d <= '1' when (( Axis_Seq = St_Home_Set_Enc ) and ( ms_clk = '1' ) and ( Pause_Tc = '1' )) else
				  '0';
			   
	PIV_Enc_Inst : PIV_Enc
		port map(
			imr       			=> imr,
			CLK40      	 		=> clk40,
			Enc_Clr 				=> Enc_Clr,
			Enc_Set_Posd2 			=> Enc_Set_Pos2d,			
			ENC_Ch				=> Enc_Ch,
			Target				=> Target_Mux( Tar_Pos_MSB downto TAR_POS_LSB ),
			Motor_Stop_Val			=> Motor_Stop_Mux,
			Motor_Start_Val		=> Motor_Start_Mux,
			Motor_Stop			=> Motor_Stop,
			Motor_Start			=> Motor_Start,
			Up_Dir				=> Target_Up_Dir,
			Enc_Cnt				=> Enc_Cnt );
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	PIV_Profile_Inst : PIV_Profile
		port map(
			imr       			=> imr,
			CLK40      	 		=> clk40,
			Move_Clk				=> PIV_CLK,
			Move_Enable			=> PIV_Move_Flag,
			Move_Start			=> Profile_Start_Cmd,
			Move_Stop				=> Motor_Stop,
			Pos_Start				=> Enc_Cnt(    TAR_POS_MSB downto TAR_POS_LSB ),		-- Start Pos is the Current Position
			Pos_Target			=> Target_Mux( TAR_POS_MSB downto TAR_POS_LSB ),					
			Pos_Max				=> Max_Pos(    TAR_POS_MSB downto TAR_POS_LSB ),
			Pos_Min				=> Min_Pos(    TAR_POS_MSB downto TAR_POS_LSB ),
			Slope_Acc				=> Slope_Acc,
			Slope_Decell			=> Slope_DeAcc,
			Vel_Max				=> Vel_Max,
			Vel_MIn				=> Vel_Min,
			Profile_Active			=> Profile_Active,
			Pos_Out				=> Profile_Pos,
			Vel_Out				=> Profile_Vel );
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	PID_Algorithm_Inst : PID_Algorithm 
		port map(
			imr       			=> imr,
			CLK40      	 		=> clk40,
			Move_Clk				=> PIV_Clk,
			Move_Start			=> PID_Start_Cmd,
			Move_Enable			=> PID_Enable,
			Up_Dir				=> Target_Up_Dir,
			KI_Init				=> KI_Init,
			DP					=> Profile_Pos, 							-- Target Position
			PS					=> Enc_Cnt,								-- Current Position
			PS_Out				=> Enc_Cnt_Latch,
			KP					=> KP, 									-- Postion loop Gain 
			KI					=> KI, 									-- Velocity loop Integral Gain
			KD					=> KD, 									-- Velocity Loop Overall Gain
			PE_Out				=> Piezo_PE,
			PE_Dif_Out			=> Piezo_PE_Diff,
			Cmd_Out				=> Piezo_Cmd,
			Stalled				=> Stall_Det );
	----------------------------------------------------------------------------------------------			

	----------------------------------------------------------------------------------------------
	with Axis_Seq select
		Piezo_Cmd_Mux <= 	Target( 10 downto 0 ) 			when St_Manual,	
						Home_Cmd( 10+16 downto 16 )		when St_Home_Clear,
						conv_Std_logic_Vector(  0, 11 ) 	when St_Home_Move_Min_Done,
						Home_Cmd( 10 downto 0 ) 			when St_Home_Set_Enc,
						Home_Cmd( 10 downto 0 ) 			when St_Home_Set_Max,
						conv_std_logic_vector(  0, 11 ) 	when St_Home_Move_Max_Done,
						Piezo_Cmd  					when others;



	PIV_AB4SPI_INST : PIV_AB4SPI
		port map(
			imr					=> imr,
			clk40				=> clk40,
			Piezo_En				=> Target( TAR_MOTOR_EN ),
			Piezo_Cmd				=> Piezo_Cmd_Mux,
			SPI_CS				=> SPI_CS,				-- Motor Amp Chip Select
			SPI_CLOCK				=> SPI_CLOCK,				-- Motor Amp Clock
			SPI_DATA				=> SPI_DATA );				-- Motor Amp Data			
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	clock_proc : process( imr, CLK40 )
	begin
		if( imr = '1' ) then
			Dss_Data			<= conv_std_logic_Vector( 0, 16 );
			Enc_Pos			<= conv_std_Logic_Vector( 0, 32 );
			Idle_time 		<= conv_std_logic_Vector( 0, 32 );
			idle_time_cntr 	<= conv_std_logic_Vector( 0, 32 );			
			Target_Pos		<= conv_std_Logic_Vector( 0, 32 );			
			Target_Home_Del	<= '0';
			Home_Status 		<= '0';
			Cmd_Home_Latch		<= '0';
			PID_Start_Cmd		<= '0';
			Profile_Start_Cmd	<= '0';
			Move_Flag			<= '0';
			PIV_Move_Flag		<= '0';
			PID_Enable		<= '0';
			Motor_Error		<= '0';
			Settle_Cnt		<= conv_Std_logic_Vector( 0, 32 );
			Axis_Seq			<= St_Idle;
			Max_Pos			<= conv_std_logic_vector( mm_Max_Pos, 32 );
			Min_Pos			<= conv_Std_logic_Vector( mm_Min_Pos, 32 );
			
		elsif(( CLK40'event ) and ( CLK40 = '1' )) then

			case conv_integer( PIV_Index ) is
				when 0 		=> Dss_Data <= Profile_Pos( 15 downto 0 ); 		-- when 1,		-- 1 Target Position based on Profile
				when 1 		=> Dss_Data <= Profile_Vel( 24 downto 24-15 );	-- when 2,		-- 2 Target Velocity based on Profile
				when 2 		=> Dss_Data <= Enc_Cnt_Latch(  15 downto 0 );	-- when 3,		-- 3 Measured Position
				when 3 		=> Dss_Data <= Piezo_PE_Diff( 15 downto 0 );		-- when 4,		-- 4 Diff Position Error
				when 4 		=> Dss_Data <= Piezo_PE( 15 downto 0 ); 		-- when 5,		-- 5 Position Error
				when 5 		=> Dss_Data <= sxt( Piezo_Cmd, 16 );			-- when 6,		-- 6 Piezo Motor Commnad
				when others 	=> Dss_Data <= x"0000";						-- when others;
			end case;

			Enc_Pos( 31 downto 24 ) 	<= "00" & conv_Std_logic_vector( Axis_Seq, 6 );		-- State
			Enc_Pos( 23           )  <= Home_Status;								-- Home Status
			Enc_Pos( 22 downto 0  )  <= sxt( Enc_Cnt, 23 );							-- Position Count
	

			-- Pipeline Target to detect changes -----------------------------------------------
			Target_Home_Del		<= Target( Tar_Motor_Home );
			------------------------------------------------------------------------------------
			-- Home Status, Cleared at power-up and when home is started, Set when Home is Done
			if( Axis_Seq = ST_HOME_START )
				then Home_Status <= '0';

			elsif( Axis_Seq = St_Home_Done ) 
				then Home_Status <= '1';
			end if;
			-- Home Status, Cleared at power-up and when home is started, Set when Home is Done
			------------------------------------------------------------------------------------

			------------------------------------------------------------------------------------
			-- If a Home Command, Latch it and keep it latched until done
			if((( Target( Tar_Motor_Home ) = '1' ) and ( Target_Home_Del = '0' )) or ( Cmd_Home = '1' ))
				then Cmd_Home_Latch <= '1';
			
			elsif( Axis_Seq = ST_HOME_START )
				then Cmd_Home_Latch <= '0';
			end if;
			------------------------------------------------------------------------------------

			
			-- Main State Machine --------------------------------------------------------------
			case Axis_Seq is
				-------------------------------------------------------------------------------
				-- Idle State -----------------------------------------------------------------			
				when ST_Idle =>
					PID_Start_Cmd		<= '0';
					Profile_Start_Cmd	<= '0';
					Move_Flag			<= '0';
					PIV_Move_Flag		<= '0';
					PID_Enable		<= '0';
					Motor_Error		<= '0';
					Pause_Cnt			<= 0;
					Settle_Cnt		<= conv_Std_logic_Vector( 0, 32 );
					Move_Time			<= Move_Time_Cntr;

				
					-- Axis is not enabled don't move
					if( Target( Tar_Motor_En ) = '0' ) then 
						Idle_Time		<= Idle_Time_Cntr;
						Idle_Time_Cntr <= conv_Std_logic_Vector( 0, 32 );						
						Axis_Seq		<= St_Disabled;

					elsif( Target( TAR_MOTOR_DIRECT ) = '1' ) then 
						Idle_Time		<= Idle_Time_Cntr;
						Idle_Time_Cntr <= conv_Std_logic_Vector( 0, 32 );						
						Axis_Seq		<= St_Manual;

					-- Door Not closed and Not Service Mode
					elsif(( Door_Closed /= "11" ) and ( Service_Mode = '0' )) then 
						Idle_Time		<= Idle_Time_Cntr;
						Idle_Time_Cntr <= conv_Std_logic_Vector( 0, 32 );						
						Axis_Seq 		<= St_Interlock;
					
					elsif( Crash = '1' ) then 
						Idle_Time		<= Idle_Time_Cntr;
						Idle_Time_Cntr <= conv_Std_logic_Vector( 0, 32 );						
						Axis_Seq		<= St_Crash;
					
					elsif( Fault = '1' ) then 
						Idle_Time		<= Idle_Time_Cntr;
						Idle_Time_Cntr <= conv_Std_logic_Vector( 0, 32 );						
						Axis_Seq		<= St_Fault;
			
					-- Home ( from Power-Up ) or user command
					elsif( Cmd_Home_Latch = '1' ) then
						Idle_Time		<= Idle_Time_Cntr;
						Idle_Time_Cntr <= conv_Std_logic_Vector( 0, 32 );						
						Axis_Seq		<= ST_HOME_START;

					-- Burn-In Mode ( must be after Tar_Motor_En check
					elsif( Target( TAR_MOTOR_BI  ) = '1' ) then
						Idle_Time		<= Idle_Time_Cntr;
						Idle_Time_Cntr <= conv_Std_logic_Vector( 0, 32 );						
						Axis_Seq		<= ST_BI_NEG_CLR;

					-- Position Changed
					elsif( Target( TAR_POS_MSB downto TAR_POS_LSB ) /= Target_Pos( TAR_POS_MSB downto TAR_POS_LSB )) then
						Idle_Time		<= Idle_Time_Cntr;
						Idle_Time_Cntr <= conv_Std_logic_Vector( 0, 32 );						
						Target_Pos 	<= Target;			
						Axis_Seq		<= St_Move_Start;		
						
					-- If the position drifts outside of the desired position window	
					elsif( Motor_Start = '1' ) then
						Idle_Time		<= Idle_Time_Cntr;
						Idle_Time_Cntr <= conv_Std_logic_Vector( 0, 32 );						
						Axis_Seq		<= St_Move_Restart;
						
					elsif(( ms_clk = '1' ) and ( idle_time_Cntr /= x"FFFFFFFF" )) then
						idle_time_cntr <= idle_time_cntr + 1;
						

					end if;				
				-- Idle State -----------------------------------------------------------------			
				-------------------------------------------------------------------------------

				-------------------------------------------------------------------------------
				-- Non Moving States ----------------------------------------------------------
				-- With any of the followign non-moving states, when re-enabling the motor, 
				-- must assume position is not at the target positon. When movement is to 
				-- restart, the motion profile must be recalculated must be recalculated 
				-- between the current position and the desired target position for 
				-- roper repositioning.
				-- Axis is Disabled, Indicate such --------------------------------------------
				when ST_Disabled =>
					PID_Start_Cmd		<= '0';
					Profile_Start_Cmd	<= '0';
					Move_Flag			<= '0';
					PIV_Move_Flag		<= '0';
					PID_Enable		<= '0';
					Motor_Error		<= '0';
					
					if( Target( Tar_Motor_En ) = '1' ) then
						Target_Pos 	<= Target;			
						Axis_Seq		<= St_Move_Start;							
					end if;
				-- Axis is Disabled, Indicate such --------------------------------------------
				
				-- Crash Sense ----------------------------------------------------------------
				when ST_CRASH =>
					PID_Start_Cmd		<= '0';
					Profile_Start_Cmd	<= '0';
					Move_Flag			<= '0';
					PIV_Move_Flag		<= '0';
					PID_Enable		<= '0';
					Motor_Error		<= '1';
					
					if( Target( Tar_Motor_En ) = '0' )
						then Axis_Seq	<= St_Disabled;
					
					-- New position must be updated before Crash is disabled
					elsif( Crash = '0' ) then
						Target_Pos 	<= Target;			
						Axis_Seq		<= St_Move_Start;	
					end if;
				-- Crash Sense ----------------------------------------------------------------

				-- Interlock Violation --------------------------------------------------------
				when ST_Interlock =>
					PID_Start_Cmd		<= '0';
					Profile_Start_Cmd	<= '0';
					Move_Flag			<= '0';
					PIV_Move_Flag		<= '0';
					PID_Enable		<= '0';
					Motor_Error		<= '1';
					
					if( Target( Tar_Motor_En ) = '0' )
						then Axis_Seq	<= St_Disabled;
					
					-- Door Not closed and Not Service Mode
					elsif(( Door_Closed = "11" ) or ( Service_Mode = '1' )) then
						Target_Pos 	<= Target;			
						Axis_Seq		<= St_Move_Start;	
					end if;
				-- Interlock Violation --------------------------------------------------------

				-- Motor Fault ----------------------------------------------------------------
				when ST_Fault =>
					PID_Start_Cmd		<= '0';
					Profile_Start_Cmd	<= '0';
					Move_Flag			<= '0';
					PIV_Move_Flag		<= '0';
					PID_Enable		<= '0';
					Motor_Error		<= '1';

					if(( Fault = '0' ) and ( Target( Tar_Motor_En ) = '0' ))
						then Axis_Seq	<= St_Disabled;
					
--					elsif( Fault = '0' ) then
--						Target_Pos 	<= Target;			
--						Axis_Seq		<= St_Move_Start;	
					end if;
				-- Motor Fault ----------------------------------------------------------------
				
				-- Motor Stalled --------------------------------------------------------------
				when ST_Stalled =>
					PID_Start_Cmd		<= '0';
					Profile_Start_Cmd	<= '0';
					Move_Flag			<= '0';
					PIV_Move_Flag		<= '0';
					PID_Enable		<= '0';
					Motor_Error		<= '1';

					if( Target( Tar_Motor_En ) = '0' )
						then Axis_Seq	<= St_Disabled;					
					end if;
					
				when ST_MANUAL =>
					PID_Start_Cmd		<= '0';
					Profile_Start_Cmd	<= '0';
					Move_Flag			<= '0';
					PIV_Move_Flag		<= '0';
					PID_Enable		<= '0';
					Motor_Error		<= '0';
					
					if( Target( Tar_Motor_En ) = '0' )
						then Axis_Seq	<= St_Disabled;

					elsif( Crash = '1' ) then 
						Idle_Time		<= Idle_Time_Cntr;
						Idle_Time_Cntr <= conv_Std_logic_Vector( 0, 32 );						
						Axis_Seq		<= St_Crash;

					-- Door Not closed and Not Service Mode
					elsif(( Door_Closed /= "11" ) and ( Service_Mode = '0' )) then 
						Idle_Time		<= Idle_Time_Cntr;
						Idle_Time_Cntr <= conv_Std_logic_Vector( 0, 32 );						
						Axis_Seq 		<= St_Interlock;

					elsif( Fault = '1' ) then 
						Idle_Time		<= Idle_Time_Cntr;
						Idle_Time_Cntr <= conv_Std_logic_Vector( 0, 32 );						
						Axis_Seq		<= St_Fault;

					elsif( Target( TAR_MOTOR_DIRECT ) = '0' ) then
						Target_Pos 	<= Target;			
						Axis_Seq		<= St_Move_Start;							
					end if;
					
				-- Motor Stalled --------------------------------------------------------------
				-- Non Moving States ----------------------------------------------------------
				-------------------------------------------------------------------------------
				
				-------------------------------------------------------------------------------
				-- Setup to Start Moving ------------------------------------------------------
				when ST_Move_Start =>
					PID_Start_Cmd		<= '1';
					Profile_Start_Cmd	<= '1';
					Move_Flag			<= '1';
					PIV_Move_Flag		<= '1';
					PID_Enable		<= '1';
					Motor_Error		<= '0';
					Settle_Cnt		<= conv_Std_logic_Vector( 0, 32 );
					Move_Time_Cntr		<= conv_Std_logic_vector( 0, 32 );	
					Axis_Seq 			<= St_Move_Mid;
				-- Setup to Start Moving ------------------------------------------------------

				-- Setup to ReStart Moving ( ie reposition )-----------------------------------
				when St_Move_Restart =>
					PID_Start_Cmd		<= '1';
					Profile_Start_Cmd	<= '0';
					Move_Flag			<= '1';
					PIV_Move_Flag		<= '1';
					PID_Enable		<= '1';
					Motor_Error		<= '0';
					Move_Time_Cntr		<= Move_Time_Cntr + 1;
					Axis_Seq			<= St_Move_Mid;
				-- Setup to ReStart Moving ( ie reposition )-----------------------------------
								
				-- Now Move -------------------------------------------------------------------
				when St_Move_Mid =>
					PID_Start_Cmd		<= '0';
					Profile_Start_Cmd	<= '0';
					Move_Flag			<= '1';
					PIV_Move_Flag		<= '1';
					PID_Enable		<= '1';
					Motor_Error		<= '0';
					Move_Time_Cntr		<= Move_Time_Cntr + 1;
					Settle_Cnt		<= conv_Std_logic_Vector( 0, 32 );
															
					if( Target( Tar_Motor_En ) = '0' )
						then Axis_Seq	<= St_Disabled;

					-- Door Not closed and Not Service Mode
					elsif(( Door_Closed /= "11" ) and ( Service_Mode = '0' ))
						then Axis_Seq <= St_Interlock;
			
					-- Crash Detected
					elsif( Crash = '1' )
						then Axis_Seq	<= St_Crash;

					elsif( Fault = '1' )
						then Axis_Seq	<= St_Fault;
					
					elsif( Stall_Det = '1' )
						then Axis_Seq	<= St_Stalled;
					
					-- 3.5 Minutes of movement
					elsif( Move_Time_Cntr = x"FFFFFFFF" )	
						then Axis_Seq	<= St_Stalled;

					-- Stop moving when reaches the target position
					elsif(( PIV_CLK = '1' ) and ( Motor_Stop = '1' ))
						then Axis_Seq	<= St_Move_Settle;
					end if;
				-- Now Move -------------------------------------------------------------------
				-------------------------------------------------------------------------------

				-------------------------------------------------------------------------------
				-- Move_Settle which wait the Settling time
				when St_Move_Settle =>
					PID_Start_Cmd		<= '0';
					Profile_Start_Cmd	<= '0';
					Move_Flag			<= '1';
					PIV_Move_Flag		<= '1';
					PID_Enable		<= '0';
					Motor_Error		<= '0';
					Move_Time_Cntr		<= Move_Time_Cntr + 1;
					Settle_Cnt		<= Settle_Cnt + 1;
										
					if( Target( Tar_Motor_En ) = '0' )
						then Axis_Seq	<= St_Disabled;

					-- Door Not closed and Not Service Mode
					elsif(( Door_Closed /= "11" ) and ( Service_Mode = '0' ))
						then Axis_Seq <= St_Interlock;
			
					-- Crash Detected
					elsif( Crash = '1' )
						then Axis_Seq	<= St_Crash;

					elsif( Fault = '1' )
						then Axis_Seq	<= St_Fault;
					
					elsif(( Settle_tc = '1' ) and ( Motor_Start = '0' ))
						then Axis_Seq		<= St_Idle;
					
					elsif(( Settle_Tc = '1' ) and ( Motor_Start = '1' ))
						then Axis_Seq		<= St_Move_Restart;
					end if;								
				-- Move_Settle which wait the Settling time
				-------------------------------------------------------------------------------

				-------------------------------------------------------------------------------
				-- ***** Home Cycle -----------------------------------------------------------
				-- Start of Home Routine ------------------------------------------------------
				-- Want to Clear the Encoder Here also
				when ST_Home_Start =>
					-- 04/21/09: Changed HOME back to PID
					PID_Start_Cmd		<= '1';
					Profile_Start_Cmd	<= '1';
					Move_Flag			<= '1';
					PIV_Move_Flag		<= '1';
					PID_Enable		<= '1';
					Motor_Error		<= '0';
					Pause_Cnt			<= 0;
					Max_Pos			<= conv_std_logic_vector( mm_Max_Pos, 32 );
					Min_Pos			<= conv_Std_logic_Vector( mm_Min_Pos, 32 );
					Move_Time_Cntr		<= conv_Std_logic_Vector( 0, 32 );

					if( Target( Tar_Motor_En ) = '0' )
						then Axis_Seq	<= St_Disabled;

					-- Door Not closed and Not Service Mode
					elsif(( Door_Closed /= "11" ) and ( Service_Mode = '0' ))
						then Axis_Seq <= St_Interlock;

					-- Crash Detected
					elsif( Crash = '1' )
						then Axis_Seq	<= St_Crash;

					elsif( Fault = '1' )
						then Axis_Seq	<= St_Fault;
			
						else Axis_Seq <= St_Home_Move_Min;
					end if;
				-------------------------------------------------------------------------------
					
				-------------------------------------------------------------------------------
				-- Using Direct Motor Commands Move to Min for 2 seconds
				when St_Home_Move_Min =>			
					-- 04/21/09: Changed HOME back to PID
					PID_Start_Cmd		<= '0';
					Profile_Start_Cmd	<= '0';
					Move_Flag			<= '1';
					PIV_Move_Flag		<= '1';
					PID_Enable		<= '1';
					Motor_Error		<= '0';
					Move_Time_Cntr		<= Move_Time_Cntr + 1;
					Pause_Cnt 		<= 0;

					if( Target( Tar_Motor_En ) = '0' )
						then Axis_Seq	<= St_Disabled;

					-- Door Not closed and Not Service Mode
					elsif(( Door_Closed /= "11" ) and ( Service_Mode = '0' ))
						then Axis_Seq <= St_Interlock;

					-- Crash Detected
					elsif( Crash = '1' )
						then Axis_Seq	<= St_Crash;

					-- Fault Detected
					elsif( Fault = '1' )
						then Axis_Seq	<= St_Fault;
					
					-- Stall Detected
					elsif( Stall_Det = '1' )
						then Axis_Seq	<= St_Home_Clear;
			
					end if;
				-- Using Direct Motor Commands Move to Min for 2 seconds
				
				-- Clear the Encoder while Driving the Motor Negative -------------------------
				when St_Home_Clear =>
					PID_Start_Cmd		<= '0';
					Profile_Start_Cmd	<= '0';
					Move_Flag			<= '1';
					PIV_Move_Flag		<= '1';
					PID_Enable		<= '0';
					Motor_Error		<= '0';
					Move_Time_Cntr		<= Move_Time_Cntr + 1;

					if(( ms_clk = '1' ) and ( Pause_Tc = '0' ))
						then Pause_Cnt <= Pause_Cnt + 1;
					elsif(( ms_clk = '1' ) and ( Pause_Tc = '1' ))
						then Pause_Cnt <= 0;
					end if;
					
					if( Target( Tar_Motor_En ) = '0' )
						then Axis_Seq	<= St_Disabled;
					elsif(( ms_clk = '1' ) and ( Pause_Tc = '1' ))
						then Axis_Seq	<= St_Home_Move_Min_Done;
					end if;
				-- Set Parameter for full Max Move - 1 clock ----------------------------------
	
				-- At Full Min - Wait for 2 seconds Motor Command Off -------------------------
				when St_Home_Move_Min_Done =>
					PID_Start_Cmd		<= '0';
					Profile_Start_Cmd	<= '0';
					Move_Flag			<= '1';
					PIV_Move_Flag		<= '0';
					PID_Enable		<= '0';
					Motor_Error		<= '0';
					Move_Time_Cntr		<= Move_Time_Cntr + 1;
					
					if(( ms_clk = '1' ) and ( Home_Tc = '0' ))
						then Pause_Cnt <= Pause_Cnt + 1;
					elsif(( ms_clk = '1' ) and ( Home_Tc = '1' ))
						then Pause_Cnt <= 0;
					end if;
					
					if( Target( Tar_Motor_En ) = '0' )
						then Axis_Seq <= St_Disabled;
					elsif(( ms_clk = '1' ) and ( Home_Tc = '1' ))
						then Axis_Seq	<= St_Home_Max_Param;
					end if;
				-- At Full Min - Wait for 2 seconds -------------------------------------------
				
				-- Set Parameter for full Max Move - 1 clock ----------------------------------
				when St_Home_Max_Param =>
					PID_Start_Cmd		<= '1';
					Profile_Start_Cmd	<= '1';
					Move_Flag			<= '1';
					PIV_Move_Flag		<= '1';
					PID_Enable		<= '1';
					Motor_Error		<= '0';
					Pause_Cnt			<= 0;
					Move_Time_Cntr		<= Move_Time_Cntr + 1;
					Axis_Seq 			<= St_Home_Move_Max;
				-- Set Parameter for full Max Move - 1 clock ----------------------------------

				-- MOve to Full Max -----------------------------------------------------------
				when St_Home_Move_Max =>
					PID_Start_Cmd		<= '0';
					Profile_Start_Cmd	<= '0';
					Move_Flag			<= '1';
					PIV_Move_Flag		<= '1';
					PID_Enable		<= '1';
					Motor_Error		<= '0';
					Move_Time_Cntr		<= Move_Time_Cntr + 1;
					Pause_Cnt 		<= 0;

					if( Target( Tar_Motor_En ) = '0' )
						then Axis_Seq	<= St_Disabled;

					-- Door Not closed and Not Service Mode
					elsif(( Door_Closed /= "11" ) and ( Service_Mode = '0' ))
						then Axis_Seq <= St_Interlock;
			
					-- Crash Detected
					elsif( Crash = '1' )
						then Axis_Seq	<= St_Crash;

					elsif( Fault = '1' )
						then Axis_Seq	<= St_Fault;

					elsif( Stall_Det = '1' )
						then Axis_Seq	<= St_Home_Set_Enc;
					end if;
				-- MOve to Full Max -----------------------------------------------------------
			
				-- Set Max to Enc - while holding the motor enabled ---------------------------
				when St_Home_Set_Enc =>
					PID_Start_Cmd		<= '0';
					Profile_Start_Cmd	<= '0';
					Move_Flag			<= '1';
					PIV_Move_Flag		<= '1';
					PID_Enable		<= '0';
					Motor_Error		<= '0';
					Move_Time_Cntr		<= Move_Time_Cntr + 1;

					if(( ms_clk = '1' ) and ( Pause_Tc = '0' ))
						then Pause_Cnt <= Pause_Cnt + 1;
					elsif(( ms_clk = '1' ) and ( Pause_Tc = '1' ))
						then Pause_Cnt <= 0;
					end if;

					if( Target( Tar_Motor_En ) = '0' )
						then Axis_Seq <= St_Disabled;
					elsif(( ms_clk = '1' ) and ( Pause_Tc = '1' ))
						then Axis_Seq	<= St_Home_Set_Max;
					end if;
				-- Set Max to Enc - while holding the motor enabled ---------------------------
				
				-- At Full Max - Wait ---------------------------------------------------------
				when St_Home_Set_Max =>
					PID_Start_Cmd		<= '0';
					Profile_Start_Cmd	<= '0';
					Move_Flag			<= '1';
					PIV_Move_Flag		<= '1';
					PID_Enable		<= '0';
					Motor_Error		<= '0';
					Pause_Cnt 		<= 0;
					Move_Time_Cntr		<= Move_Time_Cntr +1;

					if( conv_Integer( Axis ) = Axis_T ) then
						Max_Pos	<=      sxt( Enc_Cnt, 32 ) - conv_std_logic_vector( mm_T_MM_Offset, 32 );
						Min_Pos	<= not( sxt( Enc_Cnt, 32 ) - conv_std_logic_vector( mm_T_MM_Offset, 32 ) ) + conv_std_logic_vector( 1, 32 );
					else 
						Max_Pos	<=      sxt( Enc_Cnt, 32 ) - conv_std_logic_vector( mm_XYR_MM_Offset, 32 );
						Min_Pos	<= not( sxt( Enc_Cnt, 32 ) - conv_std_logic_vector( mm_XYR_MM_Offset, 32 ) ) + conv_std_logic_vector( 1, 32 );
					end if;

					Axis_Seq	<= St_Home_Move_Max_Done;

				-- At Full Min - Wait ---------------------------------------------------------
				
				-- At Full Min - Wait for 2 seconds How keep the Amplifier off ----------------
				when St_Home_Move_Max_Done =>
					PID_Start_Cmd		<= '0';
					Profile_Start_Cmd	<= '0';
					Move_Flag			<= '1';
					PIV_Move_Flag		<= '0';
					PID_Enable		<= '0';
					Motor_Error		<= '0';
					Move_Time_Cntr		<= Move_Time_Cntr + 1;
					
					if(( ms_clk = '1' ) and ( Home_Tc = '0' ))
						then Pause_Cnt <= Pause_Cnt + 1;
					elsif(( ms_clk = '1' ) and ( Home_Tc = '1' ))
						then Pause_Cnt <= 0;
					end if;
					
					if( Target( Tar_Motor_En ) = '0' )
						then Axis_Seq <= St_Disabled;
					elsif(( ms_clk = '1' ) and ( Home_Tc = '1' ))
						then Axis_Seq	<= St_Home_Zero_Param;
					end if;
				-- At Full Min - Wait for 2 seconds -------------------------------------------
								
				-- Set Parameters to Move to 0 ------------------------------------------------
				when St_Home_Zero_Param =>
					PID_Start_Cmd		<= '1';
					Profile_Start_Cmd	<= '1';
					Move_Flag			<= '1';
					PIV_Move_Flag		<= '1';
					PID_Enable		<= '1';
					Motor_Error		<= '0';
					Pause_Cnt			<= 0;
					Move_Time_Cntr		<= Move_Time_Cntr + 1;

					Axis_Seq 			<= St_Home_Zero;
				-- Set Parameters to Move to 0 ------------------------------------------------
				
				-- Move to Zero ---------------------------------------------------------------
				when St_Home_Zero =>
					PID_Start_Cmd		<= '0';
					Profile_Start_Cmd	<= '0';
					Move_Flag			<= '1';
					PIV_Move_Flag		<= '1';
					PID_Enable		<= '1';
					Motor_Error		<= '0';
					Pause_Cnt			<= 0;
					Move_Time_Cntr		<= Move_Time_Cntr + 1;
					
					if( Target( Tar_Motor_En ) = '0' )
						then Axis_Seq	<= St_Disabled;

					-- Door Not closed and Not Service Mode
					elsif(( Door_Closed /= "11" ) and ( Service_Mode = '0' ))
						then Axis_Seq <= St_Interlock;
			
					-- Crash Detected
					elsif( Crash = '1' )
						then Axis_Seq	<= St_Crash;

					elsif( Fault = '1' )
						then Axis_Seq	<= St_Fault;

					elsif( Motor_Stop = '1' )
						then Axis_Seq	<= St_Home_Done;
					end if;
				-- Move to Zero ---------------------------------------------------------------
				
				-- Home Routine Done ----------------------------------------------------------
				when St_Home_Done	=>
					PID_Start_Cmd		<= '0';
					Profile_Start_Cmd	<= '0';
					Move_Flag			<= '1';
					PIV_Move_Flag		<= '0';
					PID_Enable		<= '0';
					Motor_Error		<= '0';		
					Move_Time 		<= Move_Time_Cntr;

					if(( ms_clk = '1' ) and ( Home_Tc = '0' ))
						then Pause_Cnt <= Pause_Cnt + 1;
					elsif(( ms_clk = '1' ) and ( Home_Tc = '1' ))
						then Pause_Cnt <= 0;
					end if;

					if( Target( Tar_Motor_En ) = '0' )
						then Axis_Seq		<= St_Disabled;
					elsif(( ms_clk = '1' ) and ( Home_Tc = '1' ))
						then Axis_Seq		<= St_Idle;
					end if;
				-- End of of Home Routine -----------------------------------------------------
				-- ***** Home Cycle -----------------------------------------------------------
				-------------------------------------------------------------------------------

				-------------------------------------------------------------------------------
				-- ***** Burn-In Cycle ***********************************************
				-- Burn-In Move Negative -----------------------------------------------
				when ST_BI_NEG_CLR	=> 
					PID_Start_Cmd		<= '1';
					Profile_Start_Cmd	<= '1';
					Move_Flag			<= '1';
					PIV_Move_Flag		<= '1';
					PID_Enable		<= '1';
					Motor_Error		<= '0';		
					Move_Time_Cntr		<= conv_Std_logic_Vector( 0, 32 );
					BI_Wait_Cnt		<= x"00000000";
					
					if( Target( Tar_Motor_En ) = '0' )
						then Axis_Seq		<= St_Disabled;

					elsif( Target( Tar_Motor_BI ) = '0' )
						then Axis_Seq	<= St_Idle;
			
					elsif( Fault = '1' )
						then Axis_Seq	<= ST_Fault;
					
					elsif( Crash = '1' )
						then Axis_Seq	<= St_Crash;

					elsif(( Door_Closed /= "11" ) and ( Service_Mode = '0' ))
						then Axis_Seq <= St_Interlock;

						else Axis_Seq	<= ST_BI_NEG_MOVE;
					end if;
					
				when ST_BI_NEG_MOVE	=> 	
					PID_Start_Cmd		<= '0';
					Profile_Start_Cmd	<= '0';
					Move_Flag			<= '1';
					PIV_Move_Flag		<= '1';
					PID_Enable		<= '1';
					Motor_Error		<= '0';
					Move_Time_Cntr		<= Move_Time_Cntr + 1;
		
					BI_Wait_Cnt 		<= BI_Wait_Cnt + 1;	

					if( Target( Tar_Motor_En ) = '0' )
						then Axis_Seq		<= St_Disabled;

					elsif( Target( Tar_Motor_BI ) = '0' )
						then Axis_Seq	<= St_Idle;
			
					elsif( Fault = '1' )
						then Axis_Seq	<= ST_Fault;
					
					elsif( Crash = '1' )
						then Axis_Seq	<= St_Crash;

					elsif(( Door_Closed /= "11" ) and ( Service_Mode = '0' ))
						then Axis_Seq <= St_Interlock;
			
					elsif(( Motor_Stop = '1' ) or ( Stall_Det = '1' ))
						then Axis_Seq	<= St_BI_NEG_WAIT;
					end if;

			 	when ST_BI_NEG_WAIT => 
					PID_Start_Cmd		<= '0';
					Profile_Start_Cmd	<= '0';
					Move_Flag			<= '1';
					PIV_Move_Flag		<= '0';
					PID_Enable		<= '0';
					Motor_Error		<= '0';		
					BI_Wait_Cnt 		<= BI_Wait_Cnt - 1;	
					Move_Time 		<= Move_Time_Cntr;

					if( Target( Tar_Motor_En ) = '0' )
						then Axis_Seq		<= St_Disabled;

					elsif( Target( Tar_Motor_BI ) = '0' )
						then Axis_Seq	<= St_Idle;
			
					elsif( Fault = '1' )
						then Axis_Seq	<= ST_Fault;
					
					elsif( Crash = '1' )
						then Axis_Seq	<= St_Crash;

					elsif(( Door_Closed /= "11" ) and ( Service_Mode = '0' ))
						then Axis_Seq <= St_Interlock;

					elsif( conv_integer( BI_Wait_Cnt ) = 0 )
						then Axis_Seq	<= ST_BI_POS_CLR; 
					end if;			
				-- Burn-In Move Negative -----------------------------------------------


				
				-- Burn-In Move Positive -----------------------------------------------
				when ST_BI_POS_CLR	=> 
					PID_Start_Cmd		<= '1';
					Profile_Start_Cmd	<= '1';
					Move_Flag			<= '1';
					PIV_Move_Flag		<= '1';
					PID_Enable		<= '1';
					Motor_Error		<= '0';		
					Move_Time_Cntr		<= conv_Std_logic_Vector( 0, 32 );
					BI_Wait_Cnt		<= x"00000000";
					
					if( Target( Tar_Motor_En ) = '0' )
						then Axis_Seq		<= St_Disabled;

					elsif( Target( Tar_Motor_BI ) = '0' )
						then Axis_Seq	<= St_Idle;
			
					elsif( Fault = '1' )
						then Axis_Seq	<= ST_Fault;
					
					elsif( Crash = '1' )
						then Axis_Seq	<= St_Crash;

					elsif(( Door_Closed /= "11" ) and ( Service_Mode = '0' ))
						then Axis_Seq <= St_Interlock;

						else Axis_Seq	<= ST_BI_POS_MOVE;
					end if;
					
				when ST_BI_POS_MOVE	=> 	
					PID_Start_Cmd		<= '0';
					Profile_Start_Cmd	<= '0';
					Move_Flag			<= '1';
					PIV_Move_Flag		<= '1';
					PID_Enable		<= '1';
					Motor_Error		<= '0';		
					BI_Wait_Cnt 		<= BI_Wait_Cnt + 1;	
					Move_Time_Cntr		<= Move_Time_Cntr + 1;

					if( Target( Tar_Motor_En ) = '0' )
						then Axis_Seq		<= St_Disabled;

					elsif( Target( Tar_Motor_BI ) = '0' )
						then Axis_Seq	<= St_Idle;
			
					elsif( Fault = '1' )
						then Axis_Seq	<= ST_Fault;
					
					elsif( Crash = '1' )
						then Axis_Seq	<= St_Crash;

					elsif(( Door_Closed /= "11" ) and ( Service_Mode = '0' ))
						then Axis_Seq <= St_Interlock;
			
					elsif(( Motor_Stop = '1' ) or ( Stall_Det = '1' ))
						then Axis_Seq	<= St_BI_POS_WAIT;
					end if;

			 	when St_BI_POS_WAIT => 
					PID_Start_Cmd		<= '0';
					Profile_Start_Cmd	<= '0';
					Move_Flag			<= '1';
					PIV_Move_Flag		<= '0';
					PID_Enable		<= '0';
					Motor_Error		<= '0';		
					BI_Wait_Cnt 		<= BI_Wait_Cnt - 1;	
					Move_Time 		<= Move_Time_Cntr;

					if( Target( Tar_Motor_En ) = '0' )
						then Axis_Seq	<= St_Disabled;

					elsif( Target( Tar_Motor_BI ) = '0' )
						then Axis_Seq	<= St_Idle;
			
					elsif( Fault = '1' )
						then Axis_Seq	<= ST_Fault;
					
					elsif( Crash = '1' )
						then Axis_Seq	<= St_Crash;

					elsif(( Door_Closed /= "11" ) and ( Service_Mode = '0' ))
						then Axis_Seq <= St_Interlock;

					elsif( conv_integer( BI_Wait_Cnt ) = 0 )
						then Axis_Seq	<= ST_BI_NEG_CLR; 
					end if;			
				-- Burn-In Move Positive -----------------------------------------------

				-- ***** Burn-In Cycle ***********************************************
				-------------------------------------------------------------------------------
				
				when others =>
					PID_Start_Cmd		<= '0';
					Profile_Start_Cmd	<= '0';
					Move_Flag			<= '0';
					PIV_Move_Flag		<= '0';
					PID_Enable		<= '0';
					Motor_Error		<= '0';		
					Axis_Seq			<= ST_Idle;
			end case;
			-- Main State Machine --------------------------------------------------------------
		end if;
	end process;
     ----------------------------------------------------------------------------------------------
	
---------------------------------------------------------------------------------------------------
end Behavioral;				-- Piezo_PIV.vhd
---------------------------------------------------------------------------------------------------
