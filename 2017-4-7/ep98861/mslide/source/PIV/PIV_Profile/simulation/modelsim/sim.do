# vcom -reportprogress 300 -work work {..\..\..\fir_shdisc\simulation\modelsim\fir_shdisc.vho}
# vcom -reportprogress 300 -work work {..\..\..\fir_edisc\simulation\modelsim\fir_edisc.vho}
# vcom -reportprogress 300 -work work {..\..\source\fir_psteer_enable.vhd}
# vcom -reportprogress 300 -work work {..\..\source\fir_psteer.vhd}
vcom -reportprogress 300 -work work {PIV_Profile.vho}
vcom -reportprogress 300 -work work {tb.vhd}
# vsim -sdftyp /SHDisc_Inst=fir_shdisc_vhd.sdo -sdftyp /blmdisc_Inst=fir_edisc_vhd.sdo work.tb
vsim -sdftype /u=PIV_Profile_vhd.sdo work.tb
do wave.do
run 2 ms
