
library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

---------------------------------------------------------------------------------------------------
entity tb is
end tb;

architecture Test_Bench of tb is

	signal imr       			: std_logic := '1';			     	-- Master Reset
	signal CLK40      	 		: std_logic;                         	-- Master Clock
	signal Move_Clk			: std_Logic;						-- Movement Clock
	signal Move_Enable			: std_logic;
	signal Move_Start			: std_logic;						-- Start of Movement		
	signal Pos_Start			: std_logic_Vector( 19 downto 0 );		-- Start Position
	signal Pos_Target			: std_logic_Vector( 19 downto 0 );		-- Target Position
	signal Slope_Acc			: std_Logic_Vector( 31 downto 0 );		-- Acceleration Slope
	signal Slope_Decell			: std_logic_Vector( 31 downto 0 );		-- DeCelleration Slope
	signal Vel_Max				: std_logic_Vector( 31 downto 0 );		-- DeCelleration Slope		
	signal Vel_Min				: std_logic_Vector( 31 downto 0 );		-- DeCelleration Slope		
	signal Move_Active			: std_Logic;						-- Move Active Indication
	signal Time_Count			: std_logic_Vector( 31 downto 0 );
	signal Pos_Out				: std_logic_Vector( 19 downto 0 );		-- Position based on Profile
	signal Vel_Out				: std_logic_Vector( 38 downto 0 );		-- Velocity , based on Profile
	signal Enable_Acc			: std_Logic;
	signal Enable_Decell		: std_logic;
	signal Decell_Distance		: std_logic_vector( 38 downto 0 );
	signal Decell_Stop			: std_logic;
	signal Move_Dir_Up			: std_logic;
	signal Pos_Clr				: std_logic;
	signal Enable_Decell_Pos 	: std_Logic;
	signal Enable_Decell_Vel 	: std_logic;
	signal Pos_Error			: std_logic_Vector( 19 downto 0 );
	signal Pos_Max				: std_logic_Vector( 19 downto 0 );		-- Target Position
	signal Pos_Min				: std_logic_Vector( 19 downto 0 );		-- Target Position
	signal Decell_Vel_Acc 		: std_logic_Vector( 31 downto 0 );
	signal Decell_Pos_Acc 		: std_logic_Vector( 38 downto 0 );		
	signal Decell_Start_Pos 		: std_Logic_Vector( 38 downto 0 );
	signal Enable_Decell_Vel_Done : std_logic;
	signal Vel_Acc_Cmp			: std_logic;
	signal Profile_Active		: std_Logic;
	signal Move_Stop			: std_logic;
	signal Decell_Start_Up		: std_logic;
	signal Decell_Start_Down		: std_logic;

	component PIV_Profile 
		port( 
			imr       			: in      std_logic;				     	-- Master Reset
			CLK40      	 		: in      std_logic;                         	-- Master Clock
			Move_Clk				: in		std_Logic;						-- Movement Clock
			Move_Enable			: in		std_logic;
			Move_Start			: in		std_logic;						-- Start of Movement		
			Move_Stop				: in		std_logic;
			Pos_Start				: in		std_logic_Vector( 19 downto 0 );		-- Start Position
			Pos_Target			: in		std_logic_Vector( 19 downto 0 );		-- Target Position
			Pos_Max				: in		std_logic_Vector( 19 downto 0 );		-- Target Position
			Pos_Min				: in		std_logic_Vector( 19 downto 0 );		-- Target Position
			Slope_Acc				: in		std_Logic_Vector( 31 downto 0 );		-- Acceleration Slope
			Slope_Decell			: in		std_logic_Vector( 31 downto 0 );		-- DeCelleration Slope
			Vel_Max				: in		std_logic_Vector( 31 downto 0 );		-- DeCelleration Slope		
			Vel_Min				: in		std_logic_Vector( 31 downto 0 );		-- DeCelleration Slope		
			Profile_Active			: buffer	std_Logic;
			Pos_Out				: buffer	std_logic_Vector( 19 downto 0 );		-- Position based on Profile
			vel_out				: buffer	std_logic_Vector( 38 downto 0 ) );		-- Position based on Profile
--			Pos_Error				: buffer	std_logic_vector( 19 downto 0 );
--			Move_Dir_Up			: buffer	std_logic;
--			Enable_Decell_Vel 		: buffer 	std_logic;
--			Enable_Decell_Pos 		: buffer 	std_Logic;
--			Enable_Decell_Vel_Done 	: buffer 	std_logic;
--			Enable_Acc			: buffer 	std_Logic;
--			Enable_Decell			: buffer 	std_logic;
--			Vel_Acc_Cmp			: buffer	std_logic;
--			Decell_Start_Up		: buffer	std_logic;
--			Decell_Start_Down		: buffer	std_logic;
--			Decell_Distance		: buffer 	std_logic_Vector( 38 downto 0 );
--			Decell_Start_Pos 		: buffer	std_Logic_Vector( 38 downto 0 );
--			Decell_Vel_Acc 		: buffer	std_logic_Vector( 31 downto 0 );
--			Decell_Pos_Acc 		: buffer	std_logic_Vector( 38 downto 0 ) );		
	end component PIV_Profile;
---------------------------------------------------------------------------------------------------
begin

	U : PIV_Profile
		port map(
			imr       			=> imr,
			CLK40      	 		=> clk40,
			Move_Clk				=> Move_CLk,
			Move_Enable			=> Move_Enable,
			Move_Start			=> Move_Start,
			Move_Stop				=> Move_Stop,
			Pos_Start				=> Pos_Start,
			Pos_Target			=> Pos_Target,
			Pos_Max				=> Pos_Max,
			Pos_Min				=> Pos_Min,
			Slope_Acc				=> Slope_Acc,
			Slope_Decell			=> Slope_Decell,
			Vel_Max				=> Vel_Max,
			Vel_Min				=> Vel_Min,
			Profile_Active			=> Profile_Active,
			Pos_Out				=> Pos_Out,
			Vel_out				=> Vel_Out );
--			Pos_Error				=> Pos_Error,
--			Move_Dir_Up			=> Move_Dir_Up,
--			Enable_Decell_Vel 		=> Enable_Decell_Vel,
--			Enable_Decell_Pos 		=> Enable_Decell_Pos,
--			Enable_Decell_Vel_Done 	=> Enable_Decell_Vel_DOne,
--			Vel_Acc_Cmp			=> Vel_Acc_Cmp,
--			Enable_Acc			=> Enable_Acc,
--			Enable_Decell			=> Enable_Decell,
--			Decell_Start_Up		=> Decell_Start_Up,
--			Decell_Start_Down		=> Decell_Start_Down,
-- 			Decell_Distance		=> Decell_Distance,
--			Decell_Start_Pos 		=> Decell_Start_Pos,
--			Decell_Vel_Acc 		=> Decell_Vel_Acc,
--			Decell_Pos_Acc 		=> Decell_Pos_Acc ); 						
		
	IMR <= '0' after 555 ns;

     ----------------------------------------------------------------------------------------------
	clock40_proc : process
	begin
		clk40 <= '0';
		wait for 12 ns;
		clk40 <= '1';
		wait for 13 ns;
	end process;
     ----------------------------------------------------------------------------------------------

     ----------------------------------------------------------------------------------------------
	Move_Clk_Proc : process
	begin
		Move_Clk <= '0';
		wait until rising_edge( clk40 );
		Move_Clk <= '1' after 5 ns;
		wait until rising_edge( clk40 );
		Move_Clk <= '0' after 5 ns;
		for i in 0 to 7 loop
			wait until rising_edge( clk40 );
		end loop;
	end process;
     ----------------------------------------------------------------------------------------------

	Encoder_Pos_Proc : process( imr, clk40 )
	begin
		if( imr = '1' )
			then Pos_Start <= conv_std_logic_Vector( 0, 20 );
		elsif( rising_Edge( clk40 )) then
			if( Pos_Clr = '1' )
				then Pos_Start <= conv_std_Logic_Vector( 0, 20 );
				else Pos_Start <= Pos_Out after 5 ns;
			end if;
		end if;
	end process;
     ----------------------------------------------------------------------------------------------

     ----------------------------------------------------------------------------------------------
	tb_proc : process
	begin
		Move_Enable		<= '0';
		Move_Start		<= '0';
		Move_Stop			<= '0';
		Pos_Clr 			<= '0';
		
		Pos_Target		<= conv_Std_Logic_Vector( 0, 20 );
		Slope_Acc			<= conv_Std_Logic_Vector( 1999, 32 );
		Slope_Decell		<= conv_Std_Logic_Vector( 1999, 32 );
		Vel_Max			<= conv_Std_Logic_Vector( 157286, 32 );
		Vel_Min			<= conv_Std_Logic_Vector(  31457, 32 );
		Pos_Max			<= conv_Std_Logic_Vector( 500, 20 );
		Pos_Min			<= conv_Std_Logic_Vector( -500, 20 );
		
		
		wait for 1 us;		
		wait until rising_edge( clk40 );
		
		wait for 1 us;
		for j in 0 to 10 loop
			wait until rising_edge( clk40 );
			case j is
				when 0 => 
					assert false report "0) Test +100um movement" severity note;
					Slope_Acc			<= conv_Std_Logic_Vector( 8192, 32 );
					Slope_Decell		<= conv_Std_Logic_Vector( 8192, 32 );
					Vel_Max			<= conv_Std_Logic_Vector( 327680, 32 );
					Vel_Min			<= conv_Std_Logic_Vector(  8192, 32 );
					Pos_Max			<= conv_Std_Logic_Vector( 500, 20 );
					Pos_Min			<= conv_Std_Logic_Vector( -500, 20 );
					Pos_Target 			<= conv_Std_logic_Vector( 200, 20  ) after 5 ns;
--
--					assert false report "0) Test +100um movement" severity note;
--					Slope_Acc			<= conv_Std_Logic_Vector( 8359, 32 );
--					Slope_Decell		<= conv_Std_Logic_Vector( 8359, 32 );
--					Vel_Max			<= conv_Std_Logic_Vector( 234052, 32 );
--					Vel_Max			<= conv_Std_Logic_Vector( 234057, 32 );
--					Vel_Max			<= conv_Std_Logic_Vector( 234057-100, 32 );
--					Vel_Max			<= conv_Std_Logic_Vector( 217334, 32 );
--					Vel_Min			<= conv_Std_Logic_Vector(  8192, 32 );
--					Pos_Max			<= conv_Std_Logic_Vector( 500, 20 );
--					Pos_Min			<= conv_Std_Logic_Vector( -500, 20 );
--					Pos_Target 			<= conv_Std_logic_Vector( 100, 20  ) after 5 ns;

				when 1 => 
					assert false	
						report "Premature End of Simulation"
						severity failure;
					assert false report "0) Test +100um movement" severity note;
					Slope_Acc			<= conv_Std_Logic_Vector( 8359, 32 );
					Slope_Decell		<= conv_Std_Logic_Vector( 8359, 32 );
--					Vel_Max			<= conv_Std_Logic_Vector( 234052, 32 );
					Vel_Max			<= conv_Std_Logic_Vector( 234057, 32 );
--					Vel_Max			<= conv_Std_Logic_Vector( 234057-100, 32 );
--					Vel_Max			<= conv_Std_Logic_Vector( 217334, 32 );
					Vel_Min			<= conv_Std_Logic_Vector(  8192, 32 );
					Pos_Max			<= conv_Std_Logic_Vector( 500, 20 );
					Pos_Min			<= conv_Std_Logic_Vector( -500, 20 );
					Pos_Target 			<= conv_Std_logic_Vector( -100, 20  ) after 5 ns;

				when 2 => 
					assert false report "0) Test +100um movement" severity note;
					Slope_Acc			<= conv_Std_Logic_Vector( 1999, 32 );
					Slope_Decell		<= conv_Std_Logic_Vector( 1999, 32 );
					Vel_Max			<= conv_Std_Logic_Vector( 157286, 32 );
					Vel_Min			<= conv_Std_Logic_Vector(  31457, 32 );
					Pos_Max			<= conv_Std_Logic_Vector( 500, 20 );
					Pos_Min			<= conv_Std_Logic_Vector( -500, 20 );
					Pos_Target 			<= conv_Std_logic_Vector( 200, 20  ) after 5 ns;
				when 3 => 
--					assert false report "Premature End of Simulation" severity failure;
					assert false report "1) Test -100um movement" severity note;
					Pos_Target 			<= conv_Std_logic_Vector( 0, 20  ) after 5 ns;
				when 4 => 
					assert false report "2) Test -100um movement" severity note;
					Pos_Target 			<= conv_Std_logic_Vector( -200, 20  ) after 5 ns;
				when 5 =>
					assert false report "3) Test +100um movement with 50mm/sec VelMax " severity note;
					Vel_Max				<= conv_Std_Logic_Vector( 16#190000#, 32 );					
					Pos_Target 			<= conv_Std_logic_Vector( 0, 20  ) after 5 ns;
				when 6 =>
					assert false report "4) Test -100um movement with 50mm/sec VelMax " severity note;
					Vel_Max				<= conv_Std_Logic_Vector( 16#190000#, 32 )  after 5 ns;			
					Pos_Target 			<= conv_Std_logic_Vector( -200, 20  ) after 5 ns;

				when 7 =>
					assert false report "5) Test Min Position with 100um Move/ Faster Accel" severity note;
					Vel_Max				<= conv_Std_Logic_Vector( 157286, 32 ) after 5 ns;
					Slope_Acc				<= conv_Std_Logic_Vector( 3999, 32 ) after 5 ns;
					Slope_Decell			<= conv_Std_Logic_Vector( 1999, 32 ) after 5 ns;
					Pos_Target 			<= conv_Std_logic_Vector( 0, 20  ) after 5 ns;

				when 8 =>
					assert false report "6) Test Min Position with 100um Move/ Faster decell" severity note;
					Vel_Max				<= conv_Std_Logic_Vector( 157286, 32 ) after 5 ns;
					Slope_Acc				<= conv_Std_Logic_Vector( 1999, 32 ) after 5 ns;
					Slope_Decell			<= conv_Std_Logic_Vector( 3999, 32 )  after 5 ns;
					Pos_Target 			<= conv_Std_logic_Vector( 200, 20  ) after 5 ns;
					
				when 9 =>
					assert false report "7) Test Max Position with 50mm/sec VelMax " severity note;
					Slope_Acc				<= conv_Std_Logic_Vector( 1999, 32 ) after 5 ns;
					Slope_Decell			<= conv_Std_Logic_Vector( 1999, 32 )  after 5 ns;
					Vel_Max				<= conv_Std_Logic_Vector( 16#190000#, 32 ) after 5 ns;			
					Pos_Target 			<= conv_Std_logic_Vector( 600, 20  ) after 5 ns;
					
				when 10 =>
					assert false report "8) Test Min Position with 50mm/sec VelMax " severity note;
					Slope_Acc				<= conv_Std_Logic_Vector( 1999, 32 ) after 5 ns;
					Slope_Decell			<= conv_Std_Logic_Vector( 1999, 32 ) after 5 ns;
					Vel_Max				<= conv_Std_Logic_Vector( 16#190000#, 32 )after 5 ns;
					Pos_Target 			<= conv_Std_logic_Vector( -600, 20  ) after 5 ns;
				
				when others => 
								
			end case;

			Move_Enable			<= '1' after 5 ns;
			Move_Start			<= '1' after 5 ns;
			wait until rising_edge( clk40 );
			Move_Start		<= '0' after 5 ns;
			
--			wait for 40 us;
--			wait until rising_edge( clk40 );
--			Move_Stop <= '1' after  5 ns;
--			wait until rising_edge( clk40 );
--			Move_Stop <= '0' after 5 ns;
			
			wait until ( rising_edge( clk40 ) and ( Profile_Active = '0' ));
			Move_Enable		<= '0' after 5 ns;
			wait for 10 us;			
		end loop;
		
		wait for 10 us;
		wait until rising_edge( clk40 );
		Pos_Clr <= '1' after 5 ns;
		wait until rising_edge( clk40 );
		Pos_Clr <= '0' after 5 ns;
		
		wait for 10 us;

		assert false
			report "End of Simulations"
			severity failure;
	end process;	
----------------------------------------------------------------------------------------------------
end Test_Bench;			-- PIV_Profile.VHD
----------------------------------------------------------------------------------------------------


		
