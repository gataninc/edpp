onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic /tb/imr
add wave -noupdate -format Logic /tb/clk40
add wave -noupdate -format Logic /tb/move_clk
add wave -noupdate -format Logic /tb/move_enable
add wave -noupdate -format Logic /tb/move_start
add wave -noupdate -format Logic /tb/move_stop
add wave -noupdate -format Logic /tb/enable_decell_vel
add wave -noupdate -format Logic /tb/enable_decell_pos
add wave -noupdate -format Logic /tb/enable_decell_vel_done
add wave -noupdate -format Literal -radix decimal /tb/decell_vel_acc
add wave -noupdate -format Literal -radix decimal /tb/decell_pos_acc
add wave -noupdate -format Logic -radix decimal /tb/decell_start_up
add wave -noupdate -format Logic -radix decimal /tb/decell_start_down
add wave -noupdate -format Logic /tb/enable_acc
add wave -noupdate -format Logic /tb/vel_acc_cmp
add wave -noupdate -format Logic /tb/enable_decell
add wave -noupdate -format Literal -radix decimal /tb/pos_max
add wave -noupdate -format Literal -radix decimal /tb/pos_min
add wave -noupdate -format Literal -radix decimal /tb/pos_start
add wave -noupdate -format Literal -radix decimal /tb/pos_target
add wave -noupdate -format Logic /tb/move_dir_up
add wave -noupdate -format Literal -radix decimal /tb/slope_acc
add wave -noupdate -format Literal -radix decimal /tb/slope_decell
add wave -noupdate -format Literal -radix decimal /tb/vel_max
add wave -noupdate -format Literal -radix decimal /tb/decell_distance
add wave -noupdate -format Literal -radix decimal /tb/decell_start_pos
add wave -noupdate -format Literal -radix decimal {/tb/decell_start_pos(38 downto 16)}
add wave -noupdate -format Analog-Step -height 74 -max 231900.0 -radix decimal /tb/vel_out
add wave -noupdate -format Analog-Step -height 74 -max 600.0 -min -600.0 -radix decimal /tb/pos_out
add wave -noupdate -format Logic -radix decimal /tb/profile_active
add wave -noupdate -format Literal -radix decimal /tb/pos_error
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {455741 ns} 0} {{Cursor 2} {9502 ns} 0}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {26578 ns}
