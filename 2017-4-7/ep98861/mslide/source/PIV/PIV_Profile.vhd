---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	PIV_Profile.VHD 
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : September 21, 2007
--   Board          : XScope Instrument Control Board
--   Schematic      : 4035.007.27000
--
--   Top Level      : 4035.009.99330, S/W, XScope, Instrument Control
--
--   Description:
--		This module is to get from the host some parameters that specify a trapazoidal movement profile.
--		It requires the Time to end Acceleration ( Time_Acc ),
--		the TIme to start Decelleleration ( Time_Decell )
--		and the time to end movement ( Time_End )
--		as well as the Acceleration Constant ( Slope_Acc ) and then
--		Decelleration constant ( Slope_Decell ).
--
--		The Acceleration/Decelleleration constants are full 32 bit values, but the LSB ( whole encoder pulse )
--		is between bits 15 and 16, ie LSB is 16.
--
--		It will recreate a movement profile, first the Velocity vs time profile by:
--		At time0 start accumulating the acceleration constant up to Time_Acc. 
--		Then accumulate nothing until the decelleration start time, Time_Decell.
--		Then accumulate the Decelleration constant. This will result in the trapazoidal 
--		velocity vs time profile.
--		It can be made to triangular by setting Time_Acc to one less then Time_Decell.
--
--		Next the velocity profile will be accumulated resulting in the position profile. 
--		This position profile will be used by the PIV algorithm 
--
--		The Acceleration and Decelleration Constants are always a positive number, the additions
--		and subtractions are taken care of by the "Move_Up_Direction" indication
-- 
--		The Position is calculated by:
--		Pos(i) <= Pos(i-1) + Vel(i-1) + Acc(i)/2
--				Pos(i-1) + Vel(i-1) + ( Vel(i) - Vel(i-1))/2
--				Pos(i-1) + Vel(i-1)/2 + Vel(i)/2
--
--	History
--		090205: Created
---------------------------------------------------------------------------------------------------
library LPM;
     use lpm.lpm_components.all;

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

---------------------------------------------------------------------------------------------------
entity PIV_Profile is 
	port( 
		imr       			: in      std_logic;				     	-- Master Reset
		CLK40      	 		: in      std_logic;                         	-- Master Clock
		Move_Clk				: in		std_Logic;						-- Movement Clock
		Move_Enable			: in		std_logic;
		Move_Start			: in		std_logic;						-- Start of Movement		
		Move_Stop				: in		std_logic;
		Pos_Start				: in		std_logic_Vector( 19 downto 0 );		-- Start Position
		Pos_Target			: in		std_logic_Vector( 19 downto 0 );		-- Target Position
		Pos_Max				: in		std_logic_Vector( 19 downto 0 );		-- Target Position
		Pos_Min				: in		std_logic_Vector( 19 downto 0 );		-- Target Position
		Slope_Acc				: in		std_Logic_Vector( 31 downto 0 );		-- Acceleration Slope
		Slope_Decell			: in		std_logic_Vector( 31 downto 0 );		-- DeCelleration Slope
		Vel_Max				: in		std_logic_Vector( 31 downto 0 );		-- DeCelleration Slope		
		Vel_Min				: in		std_logic_Vector( 31 downto 0 );		-- DeCelleration Slope		
		Profile_Active			: buffer	std_Logic;
		Pos_Out				: buffer	std_logic_Vector( 19 downto 0 );		-- Position based on Profile
		vel_out				: buffer	std_logic_Vector( 38 downto 0 ) );		-- Position based on Profile
end PIV_Profile;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture Behavioral of PIV_Profile is
	-- Constant Declarations ---------------------------------------------------------------------
	constant POS_LSB			: integer := 16;
	constant Acc_Width			: integer := 39;

	constant ST_Idle 			: integer := 0;
	constant St_Calc_Decell		: integer := 1;
	constant St_Calc_Decell_Done	: integer := 3;
	constant St_Accel			: integer := 4;
	constant St_Slew			: integer := 5;
	constant St_Decell			: integer := 6;
	constant St_Done			: integer := 7;
	-- Constant Declarations ---------------------------------------------------------------------

	signal Pos_Error			: std_logic_vector( 19 downto 0 );
	signal Move_Dir_Up			: std_logic;
	signal Enable_Decell_Vel 	: std_logic;
	signal Enable_Decell_Pos 	: std_Logic;
	signal Enable_Decell_Vel_Done : std_logic;
	signal Enable_Acc			: std_Logic;
	signal Enable_Decell		: std_logic;
	signal Vel_Acc_Cmp			: std_logic;
	signal Decell_Start_Up		: std_logic;
	signal Decell_Start_Down		: std_logic;
	signal Decell_Distance		: std_logic_Vector( 38 downto 0 );
	signal Decell_Start_Pos 		: std_Logic_Vector( 38 downto 0 );
	signal Decell_Vel_Acc 		: std_logic_Vector( 31 downto 0 );
	signal Decell_Pos_Acc 		: std_logic_Vector( 38 downto 0 );		


	-- Signal Declarations -----------------------------------------------------------------------
	signal Decell_Vel_acc_Add 	: std_logic_Vector( 31 downto 0 );
	signal Decell_Vel_Acc_Over	: std_logic;
	signal Decell_Vel_Acc_Cout	: std_logic;
	signal Decell_pos_Acc_Add1 	: std_logic_Vector( Acc_Width-1 downto 0 );
	signal Decell_pos_Acc_Add2 	: std_logic_Vector( Acc_Width-1 downto 0 );
	signal Decell_pos_acc_over2	: std_logic;
	signal Decell_pos_acc_cout2	: std_logic;

	signal Max_Travel_Cmp		: std_logic;
	signal Min_Travel_Cmp 		: std_logic;
	signal Move_Dir_Up_Cmp 		: std_logic;

	signal Neg_Pos_Error 		: std_Logic;

	signal PIVP_State			: integer range 0 to St_Done;
	signal Pos_Acc				: std_logic_Vector( Acc_Width-1 downto 0 );
	signal Pos_Cout			: std_logic;	
	signal Pos_Dif 			: std_logic_Vector( 19 downto 0 );
	signal Pos_Over			: std_logic;
	signal Pos_Acc_Add1			: std_logic_Vector( Acc_Width-1 downto 0 );
	signal Pos_Acc_Add2			: std_logic_Vector( Acc_Width-1 downto 0 );
	signal Pos_Acc_over2 		: std_logic;
	signal Pos_Acc_Cout2 		: std_logic;
	signal Pos_Pos_Error		: std_logic;
	signal Pos_Acc_Add_En		: std_Logic;
	
	signal Vel_acc				: std_logic_Vector( 38 downto 0 );		-- Velocity , based on Profile
	signal Vel_Acc_Add 			: std_logic_Vector( Acc_Width-1 downto 0 );
	signal Vel_Acc_Over			: std_logic;
	signal Vel_Acc_Cout 		: std_logic;
	signal Vel_Add_Over 		: std_logic;
	signal Vel_Add_Cout			: std_logic;
	signal Vel_Add_Mux 			: std_logic_Vector( Acc_Width-1 downto 0 );
	signal Vel_Sub_Enable		: std_Logic;
	signal Vel_slope_Mux 		: std_logic_Vector( Acc_Width-1 downto 0 );
	signal Vel_Acc_Pos_Cmp		: std_logic;
	-- Signal Declarations -----------------------------------------------------------------------

begin
	-- Velocity Output ( for Test Purposes 
	-- At 50 mm/sec or 50um/ms * 0.25/0.5 enc/clk * 65535
	-- Yields 190,000 hex which translastes to bit 24 being active
	--	Therefore since vel is limited to 50mm/sec, vel_out max is at bit 24
	vel_out <= Vel_acc;		-- Bit 25 is MSB for 50mm/sec
	Pos_Out <= Pos_Acc( 19+Pos_LSB downto Pos_LSB );			

	Max_Travel_Inst : lpm_compare
		generic map(
			LPM_WIDTH			=> 20,
			LPM_REPRESENTATION 	=> "SIGNED" )
		port map(
			dataa			=> Pos_Acc( 19+Pos_LSB downto Pos_LSB ),
			datab			=> Pos_Max,
			agb				=> Max_Travel_Cmp ); 
			
	Min_Travel_Inst : lpm_compare
		generic map(
			LPM_WIDTH			=> 20,
			LPM_REPRESENTATION 	=> "SIGNED" )
		port map(
			dataa			=> Pos_Acc( 19+Pos_LSB downto Pos_LSB ),
			datab			=> Pos_Min,
			alb				=> Min_Travel_Cmp ); 
			
	----------------------------------------------------------------------------------------------
	-- Determine which direction the Target movement is to take
	Move_Dir_Up_Compare : LPM_COMPARE
		generic map(
			LPM_WIDTH			=> 20,
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			dataa			=> Pos_Target,
			datab			=> Pos_Start,
			agb				=> Move_Dir_Up_Cmp );
	-- Determine which direction the Target movement is to take
	----------------------------------------------------------------------------------------------
	
	----------------------------------------------------------------------------------------------
	-- Determine the Positonal Error, ie the difference from current positon to the target position
	Pos_Error_Inst : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 20,
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_DIRECTION		=> "SUB",
			LPM_PIPELINE		=> 1 )
		port map(
			aclr				=> imr,
			clock			=> clk40,
			cin				=> '1',
			dataa			=> Pos_Target,
			datab			=> Pos_Out,
			result			=> Pos_Dif,
			Overflow			=> Pos_Over,
			Cout				=> Pos_Cout );
			
	Pos_Error <= conv_Std_logic_Vector( 16#7FFFF#, 20 ) when (( Pos_Over = '1' ) and ( Pos_Cout = '0' )) else
		        conv_std_logic_Vector( 16#80000#, 20 ) when (( Pos_Over = '1' ) and ( Pos_Cout = '1' )) else
		        Pos_Dif;
		        
	Pos_Error_Sign : lpm_compare
		generic map(
			LPM_WIDTH			=> 20,
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			dataa			=> Pos_Error,
			datab			=> conv_Std_logic_Vector( 0, 20 ),
			ageb				=> Pos_Pos_Error,	-- For Down Direction
			aleb				=> Neg_Pos_Error );	-- For Up Direction			
	-- Determine the Positonal Error, ie the difference from current positon to the target position
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Determine when to Start the Decelleration 
	Decell_Start_Pos_Inst : lpm_add_sub
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			add_Sub			=> not( Move_Dir_Up ),		-- 1 : Add, 0 : Sub
			cin				=>      Move_Dir_Up,			-- for Subtract
			dataa			=> sxt( Pos_Target & conv_std_logic_Vector( 0, Pos_LSB ), Acc_Width ),	-- Target Pos
			datab			=> Decell_Distance,												-- Decell Distance
			result			=> Decell_Start_Pos );											-- Decell Starting Positon

	Decell_Start_Pos_Compare_Inst : lpm_compare
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			dataa			=> Pos_Acc,
			datab			=> Decell_Start_Pos,
			agb				=> Decell_Start_Up,
			alb				=> Decell_Start_Down );
	-- Determine when to Start the Decelleration 
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Decelleration Velocity Profile Accumulator 
	Decell_Vel_Acc_Add_inst : lpm_add_Sub
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_DIRECTION		=> "ADD" )
		port map(
			cin				=> '0',
			dataa			=> Decell_Vel_Acc,
			datab			=> Slope_Decell,	
			result			=> Decell_Vel_acc_Add,
			overflow			=> decell_vel_acc_over,
			cout				=> decell_Vel_acc_cout );
	-- Decelleration Velocity Profile Accumulator 
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Decelleleration Position Determination -----------------------------------------------------
	Decellel_Vel_Calc_Cmp_Compare : lpm_compare
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			dataa			=> Decell_Vel_Acc,
			datab			=> Vel_Max,	
			agb				=> Enable_Decell_Vel_Done );
	-- Decelleleration Position Determination -----------------------------------------------------
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Decelleration Velocity Profile Accumulator 
	-- Decellerateion Position Accumulator
	-- Pos = Pos + Vel + Decell/2
	
	Decell_Pos_Acc_Add1_inst : lpm_add_Sub
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_DIRECTION		=> "ADD" )
		port map(
			cin				=> '0',
			dataa			=> Decell_Pos_Acc,					-- Pos
			datab			=> sxt( Decell_Vel_Acc, Acc_Width ),	-- Vel
			result			=> Decell_Pos_Acc_Add1 );

	Decell_Pos_Acc_Add2_inst : lpm_add_Sub
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_DIRECTION		=> "ADD" )
		port map(
			cin				=> '0',
			dataa			=> Decell_Pos_Acc_Add1,						-- Pos + Vel
			datab			=> sxt( Slope_Decell( 31 downto 1 ), Acc_Width ),	-- Accel/2
			result			=> Decell_Pos_Acc_Add2,
			overflow			=> decell_Pos_acc_over2,
			cout				=> decell_Pos_acc_cout2 );

	-- Decelleration Velocity Profile Accumulator 
	----------------------------------------------------------------------------------------------


	----------------------------------------------------------------------------------------------
	-- Velocity Accumulator 
	Vel_Acc_Add_Inst : lpm_add_Sub
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_REPRESENTATION 	=> "SIGNED" )
		port map(
			add_Sub			=> not( Enable_Decell ),				-- 1: Add, 0: Subtract
			cin				=> Enable_Decell,
			dataa			=> Vel_Acc,
			datab			=> Vel_Slope_Mux,
			result			=> Vel_Acc_Add,
			overflow			=> Vel_Acc_Over,
			cout				=> Vel_Acc_Cout );
	-- Velocuty Accumulator 
	----------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------
	-- Maximum Velocity Test
	Vel_Acc_Cmp_Inst : lpm_compare
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			dataa			=> Vel_Acc,
			datab			=> sxt( Vel_Max, Acc_Width ), 
			ageb				=> Vel_Acc_Cmp );
			
	Vel_Acc_Pos_Inst : lpm_compare
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			dataa			=> Vel_Acc,
			datab			=> sxt( Vel_Min, Acc_Width ),
			aleb				=> Vel_Acc_Pos_Cmp );
	-- Velocity Accumulator ---------------------------------------------------------------------
	----------------------------------------------------------------------------------------------
			
	----------------------------------------------------------------------------------------------
	Pos_Acc_Add1_Inst : lpm_add_Sub
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_REPRESENTATION => "SIGNED" )
		port map(
			add_Sub			=> Move_Dir_Up,
			cin				=> not( Move_Dir_Up ),
			dataa			=> Pos_Acc,							-- Position Accumulator
			datab			=> Vel_Acc( Acc_WIdth-1 downto 0 ), 		-- Velocity Accumulator
			result			=> Pos_Acc_Add1 );
			
	Pos_Acc_Add_En <= '1' when (( Move_Dir_Up = '1' ) and ( PIVP_State = St_Accel )) else
				   '1' when (( Move_Dir_Up = '1' ) and ( PIVP_State = St_Slew  )) else
				   '1' when (( Move_Dir_Up = '0' ) and ( PIVP_State = St_Decell )) else
				   '1' when (( Move_Dir_Up = '0' ) and ( PIVP_State = St_Done )) else
				   '0';

	Pos_Acc_Add2_Inst : lpm_add_Sub
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_REPRESENTATION => "SIGNED" )
		port map(
			add_Sub			=> Pos_Acc_Add_En,
			cin				=> not( Pos_Acc_Add_En ),
			dataa			=> Pos_Acc_Add1,									-- Position + Velocity 
			datab			=> sxt( Vel_Slope_Mux( Acc_Width-1 downto 1 ), Acc_Width ), -- + 1/2 Acceleration
			result			=> Pos_Acc_Add2,
			overflow			=> Pos_Acc_over2,
			cout				=> Pos_Acc_Cout2 );

	----------------------------------------------------------------------------------------------
	with PIVP_State select
		Enable_Decell_Vel <= 	'1' when St_Calc_Decell,
							'0' when others;
				  				 
	with PIVP_State select
		Enable_Acc 		<= 	'1' when St_Accel,
							'0' when others;
	with PIVP_State select
		Enable_Decell 		<= 	'1' when St_Decell,			
							'0' when others;
							
	with PIVP_State select
		Profile_Active 	<= '1' when St_Calc_Decell,
						   '1' when St_Calc_Decell_Done,
						   '1' when St_Accel,
						   '1' when St_Slew,
						   '1' when St_Decell,
						   '0' when others;
						   
	with PIVP_State select
		Vel_Slope_Mux		<= sxt( Slope_Acc, 	 	Acc_WIdth ) when St_Accel,
						   sxt( Slope_Acc, 	 	Acc_WIdth ) when St_Slew,
						   sxt( Slope_Decell, 	Acc_WIdth ) when St_Decell,	-- Convert to Negative
						   sxt( Slope_Decell, 	Acc_WIdth ) when St_Done,		-- Convert to Negative
						   conv_Std_logic_Vector( 0,  	Acc_Width ) when others;

	----------------------------------------------------------------------------------------------
	Decellel_calc_clock_proc : process( imr, clk40 )
	begin
		if( imr = '1' ) then
			Move_Dir_Up			<= '0';
			Decell_Distance 		<= conv_Std_logic_Vector( 0, Acc_Width );
			PIVP_State			<= St_Idle;


			Decell_Vel_Acc 		<= conv_Std_Logic_Vector( 0, 32 );
			Decell_Pos_Acc 		<= conv_Std_Logic_Vector( 0, Acc_Width );
			Vel_Acc 				<= conv_Std_logic_Vector( 0, Acc_Width );
			Pos_Acc 				<= conv_Std_logic_Vector( 0, Acc_Width );
		elsif(( clk40'event ) and ( clk40 = '1' )) then		

			case PIVP_State is
				when ST_IDLE =>
					Move_Dir_Up			<= Move_Dir_Up_Cmp;
					if(( Move_Enable = '1' ) and ( Move_Start = '1' ))
						then PIVP_State <= St_Calc_Decell;
					end if;

				-- Calculate Decelleration Distance -------------------------------------------
				when St_Calc_Decell =>
					if( Move_Enable = '0' ) 
						then PIVP_State <= St_Idle;
					elsif( Enable_Decell_Vel_Done = '1' ) then
						Decell_Distance 		<= Decell_Pos_Acc;
						PIVP_State 			<= St_Calc_Decell_Done;
					end if;

				when St_Calc_Decell_Done =>
					if( Move_Clk = '1' ) 
						then PIVP_State 	<= St_Accel;
					end if;
				-- Calculate Decelleration Distance -------------------------------------------

				-- Create Acceleration Profile ------------------------------------------------
				when St_Accel	=>
					if( Move_Enable = '0' )
						then PIVP_State 	<= St_Idle;
					elsif( Move_Stop = '1' )
						then PIVP_State	<= St_Done;
					elsif( Move_Clk = '1' ) then
						if(( Move_Dir_Up = '0' ) and ( Pos_Pos_Error = '1' ))
							then PIVP_State 	<= St_Done;
						elsif(( Move_Dir_Up = '1' ) and ( Neg_Pos_Error = '1' ))
							then PIVP_State 	<= St_Done;
						elsif(( Move_Dir_Up = '0' ) and ( Min_Travel_Cmp = '1' ))
							then PIVP_State 	<= St_Done;
						elsif(( Move_Dir_Up = '1' ) and ( Max_Travel_Cmp = '1' ))
							then PIVP_State 	<= St_Done;	
						elsif(( Move_Dir_Up = '1' ) and ( Decell_Start_Up = '1' ))
							then PIVP_State <= St_Decell;
						elsif(( Move_Dir_Up = '0' ) and ( Decell_Start_Down = '1' ))
							then PIVP_State <= St_Decell;
						elsif( Vel_Acc_Cmp = '1' ) 
							then PIVP_State <= St_Slew;				
						end if;
					end if;
				-- Create Acceleration Profile ------------------------------------------------
				
				-- Create Slew Profile --------------------------------------------------------
				when St_Slew	=>
					if( Move_Enable = '0' )
						then PIVP_State <= St_Idle;
					elsif( Move_Stop = '1' )
						then PIVP_State	<= St_Done;
					elsif( Move_Clk = '1' ) then
						if(( Move_Dir_Up = '0' ) and ( Pos_Pos_Error = '1' ))
							then PIVP_State 	<= St_Done;
						elsif(( Move_Dir_Up = '1' ) and ( Neg_Pos_Error = '1' ))
							then PIVP_State 	<= St_Done;
						elsif(( Move_Dir_Up = '0' ) and ( Min_Travel_Cmp = '1' ))
							then PIVP_State 	<= St_Done;
						elsif(( Move_Dir_Up = '1' ) and ( Max_Travel_Cmp = '1' ))
							then PIVP_State 	<= St_Done;	
						elsif(( Move_Dir_Up = '1' ) and ( Decell_Start_Up = '1' ))
							then PIVP_State <= St_Decell;
						elsif(( Move_Dir_Up = '0' ) and ( Decell_Start_Down = '1' ))
							then PIVP_State <= St_Decell;
						end if;
					end if;
				-- Create Slew Profile --------------------------------------------------------

				-- Create Decelleration Profile -----------------------------------------------
				when St_Decell	=>					
					if( Move_Enable = '0' )
						then PIVP_State <= St_Idle;
					elsif( Move_Stop = '1' )
						then PIVP_State	<= St_Done;
					elsif( Move_Clk = '1' ) then
						if(( Move_Dir_Up = '0' ) and ( Pos_Pos_Error = '1' ))
							then PIVP_State 	<= St_Done;
						elsif(( Move_Dir_Up = '1' ) and ( Neg_Pos_Error = '1' ))
							then PIVP_State 	<= St_Done;
						elsif(( Move_Dir_Up = '0' ) and ( Min_Travel_Cmp = '1' ))
							then PIVP_State 	<= St_Done;
						elsif(( Move_Dir_Up = '1' ) and ( Max_Travel_Cmp = '1' ))
							then PIVP_State 	<= St_Done;					
						end if;
					end if;
				-- Create Decelleration Profile -----------------------------------------------
				when St_Done =>
					if( Move_Enable = '0' )
						then PIVP_State	<= St_Idle;
					end if;
				
				when others =>
					PIVP_State 			<= St_Idle;
			end case;

			-- Decelleration Velocity Profile --------------------------------------------------	
			if( Enable_Decell_Vel = '0' ) then 
				Decell_Vel_Acc <= conv_std_Logic_Vector( 0, 32 );
				Decell_Pos_Acc <= conv_Std_Logic_Vector( 0, Acc_Width );			-- Set Pos to 0
			else
				if( Enable_Decell_Vel_Done = '0' ) then
					if( decell_Vel_acc_over = '0'  )
						then Decell_Vel_Acc <=  Decell_Vel_acc_Add;	-- Accumulate by the Decelleleration Slope until it reaches VelMax
					elsif( Decell_Vel_Acc_Cout = '0' )
						then decell_Vel_Acc <= not( '1' & conv_Std_logic_Vector( 0, 31 ));
						else decell_vel_Acc <=      '1' & conv_Std_logic_Vector( 0, 31 );
					end if;
					if( decell_pos_acc_over2 = '0' )
						then Decell_Pos_Acc <= Decell_Pos_Acc_Add2;	-- Now Accumulate the Velocity
					elsif( decell_Pos_acc_cout2 = '0' )
						then Decell_Pos_Acc <= not( '1' & conv_Std_logic_Vector( 0, Acc_Width-1 ) );
						else Decell_Pos_Acc <=      '1' & conv_Std_logic_Vector( 0, Acc_Width-1 );
					end if;
				end if;				
			end if;
			-- Decelleration Velocity Profile --------------------------------------------------
			------------------------------------------------------------------------------------

			------------------------------------------------------------------------------------
			if( Move_Enable = '0' )
				then Vel_Acc <= conv_Std_logic_Vector( 0, Acc_Width );
			elsif(( PIVP_State = ST_Done ) and ( Move_Enable = '1' ))
				then Vel_Acc <= conv_Std_logic_Vector( 0, Acc_Width );
			elsif(( Enable_Acc = '1' ) and ( Vel_Acc_Cmp = '1' ))
				then Vel_Acc <= sxt( Vel_Max, Acc_WIdth );
			elsif(( Move_Clk = '1' ) and ( Enable_Acc = '1' )) then
				if( Vel_Acc_Over = '0' ) 
					then Vel_Acc <= Vel_Acc_Add;
				elsif( Vel_Acc_Cout = '0' )
					then Vel_Acc <= not( '1' & conv_Std_logic_Vector( 0, Acc_Width-1 ) );
					else Vel_Acc <=      '1' & conv_Std_logic_Vector( 0, Acc_Width-1 );
				end if;
			elsif(( Move_Clk = '1' ) and ( Enable_Decell = '1' )) then
				if( Vel_Acc_Pos_Cmp = '1' ) 
					then Vel_Acc <= sxt( Vel_Min, Acc_Width );
				elsif( Vel_Acc_Over = '0' ) 
					then Vel_Acc <= Vel_Acc_Add;
				elsif( Vel_Acc_Cout = '0' )
					then Vel_Acc <= not( '1' & conv_Std_logic_Vector( 0, Acc_Width-1 ) );
					else Vel_Acc <=      '1' & conv_Std_logic_Vector( 0, Acc_Width-1 );
				end if;
			end if;
			------------------------------------------------------------------------------------
			
			-- Pipeline The Velocity Output to be used as input to the postion accumulator -----
			-- Position Accumulator ( based on Velocity ) --------------------------------------
			if(( Move_Enable = '1' ) and ( Move_Start = '1' ))
				then Pos_Acc	<= sxt( Pos_Start & conv_Std_logic_Vector( 0, Pos_LSB ), Acc_Width );
			elsif(( PIVP_State = ST_Done ) and ( Move_Enable = '1' ))
				then Pos_Acc	<= sxt( Pos_Target & conv_Std_logic_Vector( 0, POS_LSB ), Acc_Width );
			elsif( Move_Clk = '1' ) then
				if( Pos_Acc_Over2 = '0' ) 
					then Pos_Acc <= Pos_Acc_Add2;		
				elsif(( Pos_Acc_Over2 = '1' ) and ( Pos_Acc_Cout2 = '0' ))
					then Pos_Acc <= not( '1' & conv_Std_logic_Vector( 0, Acc_Width-1 ));
				elsif(( Pos_Acc_Over2 = '1' ) and ( Pos_Acc_Cout2 = '1' ))
					then Pos_Acc <=    ( '1' & conv_Std_logic_Vector( 0, Acc_Width-1 ));
				end if;
			end if;
			-- Position Accumulator ( based on Velocity ) --------------------------------------

		end if;
	end process;
	----------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
end Behavioral;			-- PIV_Profile.VHD
----------------------------------------------------------------------------------------------------

					

		
