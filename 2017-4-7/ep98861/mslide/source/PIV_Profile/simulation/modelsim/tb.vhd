---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	PIV_Profile.VHD 
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : September 21, 2007
--   Board          : XScope Instrument Control Board
--   Schematic      : 4035.007.27000
--
--   Top Level      : 4035.009.99330, S/W, XScope, Instrument Control
--
--   Description:
--		This module is to get from the host some parameters that specify a trapazoidal movement profile.
--		It requires the Time to end Acceleration ( Time_Acc ),
--		the TIme to start Decelleleration ( Time_Decell )
--		and the time to end movement ( Time_End )
--		as well as the Acceleration Constant ( Slope_Acc ) and then
--		Decelleration constant ( Slope_Decell ).
--
--		The Acceleration/Decelleleration constants are full 32 bit values, but the LSB ( whole encoder pulse )
--		is between bits 15 and 16, ie LSB is 16.
--
--		It will recreate a movement profile, first the Velocity vs time profile by:
--		At time0 start accumulating the acceleration constant up to Time_Acc. 
--		Then accumulate nothing until the decelleration start time, Time_Decell.
--		Then accumulate the Decelleration constant. This will result in the trapazoidal 
--		velocity vs time profile.
--		It can be made to triangular by setting Time_Acc to one less then Time_Decell.
--
--		Next the velocity profile will be accumulated resulting in the position profile. 
--		This position profile will be used by the PIV algorithm 
--
--		The Acceleration and Decelleration Constants are always a positive number, the additions
--		and subtractions are taken care of by the "Move_Up_Direction" indication
-- 
--		The Position is calculated by:
--		Pos(i) <= Pos(i-1) + Vel(i-1) + Acc(i)/2
--				Pos(i-1) + Vel(i-1) + ( Vel(i) - Vel(i-1))/2
--				Pos(i-1) + Vel(i-1)/2 + Vel(i)/2
--
--	History
--		090205: Created
---------------------------------------------------------------------------------------------------

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;
	
entity tb is
end tb;

architecture test_bench of tb is

	signal CLK50      	 		: std_logic := '0';                      	-- Master Clock
	signal Move_Clk			: std_Logic := '0';						-- Movement Clock
	signal Move_Enable			: std_logic;
	signal Profile_Start		: std_logic;						-- Start of Movement		
	signal Pos_Start			: std_logic_Vector( 31 downto 0 );		-- Start Position
	signal Pos_Target			: std_logic_Vector( 31 downto 0 );		-- Target Position
	signal Slope_Acc			: std_Logic_Vector( 31 downto 0 );		-- Acceleration Slope
	signal Slope_Decell			: std_logic_Vector( 31 downto 0 );		-- DeCelleration Slope
	signal Vel_Max				: std_logic_Vector( 31 downto 0 );		-- DeCelleration Slope		
	signal Vel_Min				: std_logic_Vector( 31 downto 0 );		-- DeCelleration Slope		
	signal Profile_Active		: std_Logic;
	signal Pos_Out				: std_logic_Vector( 31 downto 0 );		-- Position based on Profilesignal 
	signal vel_out				: std_logic_Vector( 31 downto 0 );		-- Position based on Profile


	---------------------------------------------------------------------------------------------------
	component PIV_Profile is 
		port( 
			CLK50      	 		: in      std_logic;                         	-- Master Clock
			Move_Clk				: in		std_Logic;						-- Movement Clock
			Move_Enable			: in		std_logic;
			Profile_Start			: in		std_logic;						-- Start of Movement		
			Pos_Start				: in		std_logic_Vector( 31 downto 0 );		-- Start Position
			Pos_Target			: in		std_logic_Vector( 31 downto 0 );		-- Target Position
			Slope_Acc				: in		std_Logic_Vector( 31 downto 0 );		-- Acceleration Slope
			Slope_Decell			: in		std_logic_Vector( 31 downto 0 );		-- DeCelleration Slope
			Vel_Max				: in		std_logic_Vector( 31 downto 0 );		-- DeCelleration Slope		
			Vel_Min				: in		std_logic_Vector( 31 downto 0 );		-- DeCelleration Slope		
			Profile_Active			: buffer	std_Logic;
			Pos_Out				: buffer	std_logic_Vector( 31 downto 0 );		-- Position based on Profile
			vel_out				: buffer	std_logic_Vector( 31 downto 0 ) );		-- Position based on Profile
	end component PIV_Profile;
	---------------------------------------------------------------------------------------------------
begin
	CLK50 <= not( CLK50 ) after 10 ns;
	
	U : PIV_Profile
		port map(
			CLK50      	 		=> CLK50,
			Move_Clk				=> Move_Clk,
			Move_Enable			=> Move_Enable,
			Profile_Start			=> Profile_Start,
			Pos_Start				=> Pos_Start,
			Pos_Target			=> Pos_Target,
			Slope_Acc				=> Slope_Acc,
			Slope_Decell			=> Slope_DeCell,
			Vel_Max				=> Vel_Max,
			Vel_Min				=> Vel_Min,
			Profile_Active			=> Profile_Active,
			Pos_Out				=> Pos_Out,
			vel_out				=> Vel_Out );
		
	---------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------
	Move_Clk_Proc : process
	begin
		wait until rising_edge( CLK50 );		
		MOVE_CLK <= '1' after 1 ns;
		wait until rising_edge( CLK50 );
		MOVE_CLK <= '0' after 1 ns;
		wait for 1 us;
	end process;
	---------------------------------------------------------------------------------------------------
		
	---------------------------------------------------------------------------------------------------
	tb_proc : process
	begin
		Move_Enable			<= '0';
		Profile_Start			<= '0';	
		Slope_Acc				<= conv_Std_logic_Vector( 100, 32 );
		Slope_Decell			<= conv_Std_logic_Vector( 100, 32 );
		Vel_Max				<= conv_Std_logic_vector( 1000, 32 );		
		Vel_Min				<= conv_Std_logic_vector( 10, 32 );

		Pos_Start				<= conv_Std_logic_vector( 100, 32 );
		Pos_Target			<= conv_Std_logic_vector( 100, 32 );
	
		wait for 1 us;
		assert false
			report "------------------------------------------------------------------------------"
			severity note;
		assert false
			report "Changed Target to 1000--------------------------------------------------------"
			severity note;
			
		wait until rising_edge( CLK50 );
		Pos_Target <= conv_Std_logic_vector( 20000, 32 );
		Move_Enable <= '1';
		Profile_Start <= '1';
		wait until rising_edge( CLK50 );
		Profile_Start <= '0';
		
		wait until falling_edge( Profile_Active );
		wait for 10 us;
		
		
		assert false
			report "End of Simulation"
			severity failure;
	end process;
	---------------------------------------------------------------------------------------------------


end test_bench;
