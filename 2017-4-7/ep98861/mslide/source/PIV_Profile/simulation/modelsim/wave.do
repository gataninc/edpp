onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix decimal /tb/clk50
add wave -noupdate -radix decimal /tb/move_clk
add wave -noupdate -radix decimal /tb/move_enable
add wave -noupdate -radix decimal /tb/profile_start
add wave -noupdate -radix decimal /tb/pos_start
add wave -noupdate -radix decimal /tb/pos_target
add wave -noupdate -radix decimal /tb/slope_acc
add wave -noupdate -radix decimal /tb/slope_decell
add wave -noupdate -radix decimal /tb/vel_max
add wave -noupdate -radix decimal /tb/vel_min
add wave -noupdate -radix decimal /tb/profile_active
add wave -noupdate -radix decimal /tb/move_stop
add wave -noupdate -radix decimal /tb/pos_out
add wave -noupdate -radix decimal /tb/vel_out
add wave -noupdate -radix decimal /tb/U/PIVP_State
add wave -noupdate -radix decimal /tb/U/Decell_Vel_Acc
add wave -noupdate -radix decimal /tb/U/Decell_Pos_Acc
add wave -noupdate -radix decimal /tb/U/Vel_Acc
add wave -noupdate -radix decimal /tb/U/Pos_Acc
add wave -noupdate -radix decimal /tb/U/Pos_Acc(63:16)
add wave -noupdate -radix decimal /tb/U/Target_Error_Abs
add wave -noupdate -radix decimal /tb/U/Decell_Distance
add wave -noupdate -radix decimal /tb/U/Decell_Start

TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {11748 ns} 0}
configure wave -namecolwidth 120
configure wave -valuecolwidth 53
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {13262 ns}
