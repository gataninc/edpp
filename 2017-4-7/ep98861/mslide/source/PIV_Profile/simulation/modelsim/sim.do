setenv ALTERA_PATH "C:/Modeltech_pe_6.6c/ALtera/Vhdl"

vcom -reportprogress 300 -work work {PIV_Profile.vho}
vcom -reportprogress 300 -work work {tb.vhd}
vsim -sdftyp {/U=PIV_Profile_vhd.sdo} work.tb
do wave.do
run 1 ms
