--------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	MSlide_IMS.VHD
---------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : September 21, 2007
--   Board          : XScope Instrument Control Board
--   Schematic      : 4035.007.27000
--
--   Top Level      : 4035.009.99330, S/W, XScope, Instrument Control
--
--   Description:
--		This module taksa an 11-bit signed value for a velocity command (U), and creats
--		a 10-bit unsigned, plus direction, for the command to the IMS Stepper Motor
--
--		The command is then used as the address to a lookup table (PIV_IMS_ROM.VHD) from file in the support folder, 
--		MotorVelocity.xlsx file. Use the column A on the MIF tab to create the .MIF file used as the initialization file
--		for the ROM.
--		
--   History:       <date> - <Author> - Ver
--		06/13/11 Rev 0.1 - MCS
--			Module: MSLide_IMS.vhd
--				Gated off IMS_EN and IMS_CLK when MOVE_ENABLE = 0
--		05/16/11 - Noise at Startup
--			Changed constant IMS_MAX_SLOW	: integer := 1;		-- from 4
---------------------------------------------------------------------------------------------------

library LPM;
   use lpm.lpm_components.all;
   
library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;
---------------------------------------------------------------------------------------------------
entity MSlide_IMS is 
	port( 
		CLK50		: in		std_logic;					-- Master Clock
		Move_Enable	: in		std_Logic;
		Enc_Stalled	: in		std_logic;
		Vel_Clk		: in		std_logic;
		Vel_In		: in		std_logic_Vector( 15 downto 0 );
		Vel_Tar		: in		std_logic_Vector( 15 downto 0 );
		IMS_CLK		: buffer	std_logic;
		IMS_DIR		: buffer	std_logic;
		Move_Stalled	: buffer	std_logic );
end MSlide_IMS;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture Behavioral of MSlide_IMS is

	-- Lookup ROM to map input command from 0 to 1023 to an output period corresponding to a linear freq
	component MSlide_IMS_ROM
		port( 
		clock		: in		std_logic := '1';
		address		: in		std_logic_Vector( 9 downto 0 );
		q			: out 	std_logic_Vector (13 downto 0 ) );
	end component MSlide_IMS_ROM;
	-- Lookup ROM to map input command from 0 to 1023 to an output period corresponding to a linear freq
	
	constant IMS_LSB		: integer := 2;
	constant IMS_MAX_SLOW	: integer := 1;		-- 2^IMS_LSB
--	constant IMS_MAX_SLOW	: integer := 4;		-- 2^IMS_LSB
	-- Signal Declarations -----------------------------------------------------------------------
	signal Abs_IMS_Cmd 		: std_logic_Vector( 10 downto 0 );
	
	signal CLKD2_PER		: std_logic_Vector( 13 downto 0 );
	
	signal CLKD2_CNT		: std_logic_Vector( 13 downto 0 );

	signal IMS_Cmd			: std_logic_Vector( 10+IMS_LSB downto 0 );

	-- Signal Declarations -----------------------------------------------------------------------

	signal Vel_In_Abs		: std_logic_Vector( 15 downto 0 );
	signal Vel_Tar_Abs		: std_logic_Vector( 15 downto 0 );		
	signal Vel_Pos			: std_logic;
	signal Vel_Too_Fast		: std_logic;
	signal Vel_Too_Slow		: std_logic;

begin
	---------------------------------------------------------------------------------------------------
	-- Positive Velocity Direction
	Vel_Dir_Compare_Inst : lpm_compare
		generic map(
			LPM_WIDTH			=> 16,
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			dataa			=> Vel_Tar,		-- Target Velocity
			datab			=> (others=>'0'),
			agb				=> Vel_Pos );
	-- Positive Velocity Direction
	---------------------------------------------------------------------------------------------------

	---------------------------------------------------------------------------------------------------
	-- Absolute Value of the Target Velocity
	abs_target_vel_inst : lpm_abs
		generic map(
			LPM_WIDTH			=> 16 )
		port map(
			data				=> Vel_Tar,
			result			=> Vel_Tar_Abs );
	-- Absolute Value of the Target Velocity
	---------------------------------------------------------------------------------------------------

	---------------------------------------------------------------------------------------------------
	-- Absolute Value of the Input Velocity
	abs_vel_inst : lpm_abs
		generic map(
			LPM_WIDTH			=> 16 )
		port map(
			data				=> Vel_In,
			result			=> Vel_In_Abs );
	-- Absolute Value of the Input Velocity
	---------------------------------------------------------------------------------------------------

	---------------------------------------------------------------------------------------------------
	-- Compare Abs Input vs,. Target Velocity to determine too fast or too slow
	Pos_Vel_Compare_Inst : lpm_compare
		generic map(
			LPM_WIDTH			=> 16,
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			dataa			=> Vel_In_Abs,			-- Absolute Value of Present Velocity
			datab			=> Vel_Tar_Abs,		-- Absolute Value of Target Velocity
			agb				=> Vel_Too_Fast,	
			alb				=> Vel_Too_Slow );
	-- Compare Abs Input vs,. Target Velocity to determine too fast or too slow
	---------------------------------------------------------------------------------------------------
					
	---------------------------------------------------------------------------------------------------
	-- Take the Command and return the absolute value 
	ABS_IMS_Cmd_INST : LPM_ABS
		generic map(
			LPM_WIDTH			=> 11 )
		port map(
			data				=> IMS_Cmd( 10+IMS_LSB downto IMS_LSB ),		
			result			=> Abs_IMS_Cmd );
	-- Take the Command and return the absolute value 
	---------------------------------------------------------------------------------------------------

	---------------------------------------------------------------------------------------------------
	-- Lookup the command for the 1/2 the clock period
	MSLide_IMS_ROM_INST : MSlide_IMS_ROM
		port map(
			clock			=> CLK50,
			address			=> Abs_IMS_Cmd( 9 downto 0 ),
			q				=> CLKD2_PER );
	-- Lookup the command for the 1/2 the clock period
	---------------------------------------------------------------------------------------------------
			
	---------------------------------------------------------------------------------------------------
	clock_Proc : process( clk50 )
	begin
		if( rising_edge( CLK50 )) then
	          -----------------------------------------------------------------------------------------
			-- Encoder is stalled and the command to the motor has dropped too low
			if( Move_Enable = '0' )
				then Move_Stalled <= '0';
			elsif(( Enc_Stalled = '1' ) and ( Abs_IMS_Cmd( 9 downto 0 ) < 2 ))
				then Move_Stalled <= '1';
			end if;
	          -----------------------------------------------------------------------------------------
			
	          -----------------------------------------------------------------------------------------
			if( Move_Enable = '0' )
				then IMS_Cmd <= (others=>'0');
				
			-- Moving Up (Towards the analyze position) Positive IMS_Cmd
			elsif(( Vel_Clk = '1' ) and ( Vel_Pos = '1' )) then
				-- Moving Up, Encoder has stalled so drop the velocity
				if( Enc_Stalled = '1' ) 
					then IMS_Cmd <= IMS_Cmd - IMS_MAX_SLOW;
					
				-- Too Fast - Need to Slow Down				
				elsif( Vel_Too_Fast = '1' )
					then IMS_Cmd <= IMS_Cmd - IMS_MAX_SLOW;						
					
				-- Too Slow - Need to Speed Up
				elsif( Vel_Too_Slow = '1' )
					then IMS_Cmd <= IMS_Cmd + 1;
				end if;									
				
			-- Moveing Down (towards the retract position) Negative IMS_Cmd
			elsif(( Vel_Clk = '1' ) and ( Vel_Pos = '0' )) then					
				-- Moving Down, Encoder has stalled so drop the velocity	
				if( Enc_Stalled = '1' )
					then IMS_Cmd <= IMS_Cmd + IMS_MAX_SLOW;
					
				-- Too Fast - Need to Slow Down
				elsif( Vel_Too_Fast = '1' )
					then IMS_Cmd <= IMS_Cmd + IMS_MAX_SLOW;
				
				-- Too Slow - Need to Speed Up
				elsif( Vel_Too_Slow = '1' ) 
					then IMS_Cmd <= IMS_Cmd - 1;
				end if;																						
			end if;				
	          -----------------------------------------------------------------------------------------					
								
	          -----------------------------------------------------------------------------------------
			if( Move_Enable = '0' ) then						-- June 13, 2011
				IMS_CLK 		<= '0';
				CLKD2_CNT		<= (others=>'0');
			else
				IMS_Dir 		<= IMS_Cmd(10+IMS_LSB);			-- Direction is based on MSD of IMS_Cmd
				if( CLKD2_CNT = CLKD2_PER ) then
					CLKD2_CNT 	<= (others=>'0');			
					IMS_CLK 		<= not( IMS_CLK );
				else 
					CLKD2_CNT		<= CLKD2_CNT + 1;
				end if;
			end if;
	          -----------------------------------------------------------------------------------------			
		end if;
	end process;
     ----------------------------------------------------------------------------------------------	
---------------------------------------------------------------------------------------------------
end Behavioral;				-- MSlide_IMS.VHD
---------------------------------------------------------------------------------------------------
