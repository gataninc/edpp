
library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;
entity tb is
end tb;

architecture test_bench of tb is

	signal clk100			: std_logic := '0';
	signal Dss_Enable		: std_logic;		-- dss_reg( kDss_Enable )
	signal Time_Enable		: std_logic;
	signal Spec_Clear		: std_logic; 		-- ( Cmd_Stb( Stb_Spec_Clr ) = '1' )
	signal Spec_Update		: std_logic;
	signal Meas_Done		: std_logic;
	signal CLIP_MAXMIN		: std_logic_Vector( 31 downto 0 );
	signal Cpeak			: std_logic_Vector( 11 downto 0 );
	signal MCA_Data		: std_logic_Vector( 31 downto 0 );
	signal MCA_Write		: std_logic;
	signal MCA_Rd_CPeak_Addr	: std_logic_vector( 11 downto 0 );
	signal MCA_Wr_CPeak_Addr	: std_logic_vector( 11 downto 0 );
	signal MCA_FF_CPeak		: std_logic_vector( 11 downto 0 );
	signal Mca_FF_Din		: std_logic_vector( 11 downto 0 );		
	signal MCA_FF_Write		: std_logic;
	signal MCA_FF_Read		: std_logic;
	signal MCA_FF_EF		: std_logic;
	

	---------------------------------------------------------------------------------------------------
	component MCA is 
		port( 
			clk100			: in		std_logic;
			Dss_Enable		: in		std_logic;		-- dss_reg( kDss_Enable )
			Time_Enable		: in		std_logic;
			Spec_Clear		: in		std_logic; 		-- ( Cmd_Stb( Stb_Spec_Clr ) = '1' )
			Spec_Update		: in		std_logic;
			Meas_Done			: in		std_logic;
			CLIP_MAXMIN		: in		std_logic_Vector( 31 downto 0 );
			Cpeak			: in		std_logic_Vector( 11 downto 0 );
			MCA_Data			: buffer	std_logic_Vector( 31 downto 0 );
			Mca_FF_Din		: buffer	std_logic_vector( 11 downto 0 );		
			MCA_FF_Write		: buffer	std_logic;
			MCA_FF_Read		: buffer	std_logic;			
			MCA_FF_EF			: buffer	std_logic;
			MCA_FF_CPeak		: buffer  std_logic_vector( 11 downto 0 );
			MCA_Write			: buffer	std_logic;
			MCA_Rd_CPeak_Addr	: buffer	std_logic_vector( 11 downto 0 );
			MCA_Wr_CPeak_Addr	: buffer	std_logic_vector( 11 downto 0 ) );

	end component MCA;
	---------------------------------------------------------------------------------------------------

begin
	clk100 <= not( clk100 ) after 5 ns;
	
	---------------------------------------------------------------------------------------------------
	U : MCA 
		port map(
			clk100			=> clk100,
			Dss_Enable		=> Dss_Enable,
			Time_Enable		=> Time_Enable,
			Spec_Clear		=> Spec_Clear,
			Spec_Update		=> Spec_Update,
			Meas_Done			=> Meas_Done,
			CLIP_MAXMIN		=> Clip_MaxMin,
			Cpeak			=> CPeak,
			MCA_Data			=> MCA_DAta,
			Mca_FF_Din		=> MCA_FF_Din,
			MCA_FF_Write		=> MCA_FF_Write,
			MCA_FF_Read		=> MCA_FF_Read,
			MCA_FF_EF			=> MCA_FF_EF,
			MCA_FF_CPeak		=> MCA_FF_CPeak,
			MCA_Write			=> MCA_Write,
			MCA_Rd_CPeak_Addr	=> MCA_Rd_CPeak_Addr,
			MCA_Wr_CPeak_Addr	=> MCA_Wr_Cpeak_Addr );
			
			
	---------------------------------------------------------------------------------------------------

	tb_proc : process
	begin
		Dss_Enable		<= '0';
		Time_Enable		<= '0';
		Spec_Clear		<= '0';
		Spec_Update		<= '0';
		Meas_Done			<= '0';
		CLIP_MAXMIN( 15 downto 0 )	<= (others=>'0');
		CLIP_MAXMIN( 31 downto 16 ) <= conv_Std_logic_Vector( 4000, 16 );
		Cpeak			<= (others=>'0');
		
		assert false
			report "Clear the Memory"
			severity note;
		wait for 1 us;
		wait until rising_Edge( clk100 );
		Spec_Clear <= '1' after 2 ns;
		wait until rising_Edge( clk100 );
		Spec_Clear <= '0' after 2 ns;
		
		wait until falling_edge( MCA_DATA(31));
		wait for 1 us;
		
		
		assert false
			report "Add Data"
			severity note;
		wait until rising_Edge( clk100 );
		Time_Enable <= '1' after 2 ns;
		CLIP_MAXMIN( 15 downto 0 )	<= conv_Std_logic_Vector(  3, 16 ) after 2 ns;
		CLIP_MAXMIN( 31 downto 16 )	<= conv_Std_logic_Vector( 20, 16 ) after 2 ns;
			
		for i in 1 to 100 loop
			wait until rising_edge( clk100 );
			wait for 3 ns;
			Cpeak 	<= x"005";
			wait until rising_edge( clk100 );
			wait for 3 ns;
			Meas_Done <= '1';
			wait until rising_edge( clk100 );
			wait for 3 ns;
			Meas_Done <= '0';
			--Cpeak 	<= x"000";
			wait for 1 us;
		end loop;
		
		wait for 1 us;
		
		assert false
			report "Process the Memory"
			severity note;
		wait until rising_edge( clk100 );
		wait for  3 ns;
		CLIP_MAXMIN( 15 downto 0 )	<= conv_Std_logic_Vector(  5, 16 );
		CLIP_MAXMIN( 31 downto 16 )	<= conv_Std_logic_Vector( 20, 16 );
		Cpeak 					<= x"000";
		wait until rising_edge( clk100 );
		wait for  3 ns;
		Spec_Update				<= '1';
		
		wait until rising_edge( clk100 );
		wait for  3 ns;
		Spec_Update	<= '0';
		
		wait for 50 us;
		
		assert false
			report "End of Simulation"
			severity failure;
					
	end process;
end test_bench;