setenv ALTERA_PATH "C:/Modeltech_pe_6.6c/ALtera/Vhdl"
vcom -reportprogress 300 -work work {MCA.vho}
vcom -reportprogress 300 -work work {tb.vhd}
vsim -sdftyp {/U=mca_vhd.sdo} work.tb
do wave.do
run 1 ms
