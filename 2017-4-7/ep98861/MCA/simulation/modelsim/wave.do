onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix unsigned /tb/clk100
add wave -noupdate -format Logic -radix unsigned /tb/Dss_Enable
add wave -noupdate -format Logic -radix unsigned /tb/Time_Enable
add wave -noupdate -format Logic -radix unsigned /tb/Spec_Clear
add wave -noupdate -format Logic -radix unsigned /tb/Spec_Update
add wave -noupdate -format Logic -radix unsigned /tb/Meas_Done
add wave -noupdate -format Logic -radix unsigned /tb/Clip_MaxMin(11:0)
add wave -noupdate -format Logic -radix unsigned /tb/Clip_MaxMin(29:16)
add wave -noupdate -format Logic -radix unsigned /tb/Cpeak
add wave -noupdate -format Logic -radix unsigned /tb/MCA_DAta(11:0)
add wave -noupdate -format Logic -radix unsigned /tb/MCA_DAta(29:12)
add wave -noupdate -format Logic -radix unsigned /tb/MCA_DAta(30)
add wave -noupdate -format Logic -radix unsigned /tb/MCA_DAta(31)
add wave -noupdate -format Logic -radix unsigned /tb/u/CPeak_Addr
add wave -noupdate -format Logic -radix unsigned /tb/u/MCA_Dout
add wave -noupdate -format Logic -radix unsigned /tb/u/mca_state
add wave -noupdate -format Logic -radix unsigned /tb/u/MCA_FF_Din
add wave -noupdate -format Logic -radix unsigned /tb/u/MCA_FF_Write
add wave -noupdate -format Logic -radix unsigned /tb/u/MCA_FF_Read
add wave -noupdate -format Logic -radix unsigned /tb/u/MCA_FF_CPeak
add wave -noupdate -format Logic -radix unsigned /tb/u/MCA_FF_EF
add wave -noupdate -format Logic -radix unsigned /tb/u/MCA_Write
add wave -noupdate -format Logic -radix unsigned /tb/u/MCA_Rd_CPeak_Addr
add wave -noupdate -format Logic -radix unsigned /tb/u/MCA_Wr_CPeak_Addr



TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {179785364 ps} 0}
configure wave -namecolwidth 120
configure wave -valuecolwidth 53
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {80282265 ps}
