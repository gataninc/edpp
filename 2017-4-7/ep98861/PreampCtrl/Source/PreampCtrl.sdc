## Generated SDC file "AD5453.sdc"

## Copyright (C) 1991-2010 Altera Corporation
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, Altera MegaCore Function License 
## Agreement, or other applicable license agreement, including, 
## without limitation, that your use is for the sole purpose of 
## programming logic devices manufactured by Altera and sold by 
## Altera or its authorized distributors.  Please refer to the 
## applicable agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 10.0 Build 218 06/27/2010 SJ Full Version"

## DATE    "Wed Sep 22 08:19:27 2010"

##
## DEVICE  "EP3C40U484C7"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {CLK50} -period 20.000 -waveform { 0.000 10.000 } [get_ports {CLK50}]


#**************************************************************
# Create Generated Clock
#**************************************************************



#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************

derive_clock_uncertainty


#**************************************************************
# Set Input Delay
#**************************************************************

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {Pot_Val[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {Pot_Val[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {Det_Ready}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {Det_Ready}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {ms1_tc}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {ms1_tc}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {Ext_Enable}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {Ext_Enable}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {CMD_WD}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {CMD_WD}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {i_SDOUT}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {i_SDOUT}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {Det_Sat}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {Det_Sat}]
          
set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {TEC2_DELAY[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {TEC2_DELAY[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {DPP_Temp[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {DPP_Temp[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {HV_CWORD[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {HV_CWORD[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {PA_TEMP_TARGET[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {PA_TEMP_TARGET[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {PA_TEMP_OFFSET[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {PA_TEMP_OFFSET[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {PA_TEC2_KIKP[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {PA_TEC2_KIKP[*]}]


#**************************************************************
# Set Output Delay
#**************************************************************

set_output_delay -clock { CLK50 } -min 3.00 [get_ports {PA_TEC2_CMD[*]}]
set_output_delay -clock { CLK50 } -max 4.00 [get_ports {PA_TEC2_CMD[*]}]

set_output_delay -clock { CLK50 } -min 3.00 [get_ports {o_SCLK}]
set_output_delay -clock { CLK50 } -max 4.00 [get_ports {o_SCLK}]

set_output_delay -clock { CLK50 } -min 3.00 [get_ports {o_nAEN}]
set_output_delay -clock { CLK50 } -max 4.00 [get_ports {o_nAEN}]

set_output_delay -clock { CLK50 } -min 3.00 [get_ports {o_nDEN}]
set_output_delay -clock { CLK50 } -max 4.00 [get_ports {o_nDEN}]

set_output_delay -clock { CLK50 } -min 3.00 [get_ports {o_SDIN}]
set_output_delay -clock { CLK50 } -max 4.00 [get_ports {o_SDIN}]

set_output_delay -clock { CLK50 } -min 3.00 [get_ports {o_SPARE}]
set_output_delay -clock { CLK50 } -max 4.00 [get_ports {o_SPARE}]

set_output_delay -clock { CLK50 } -min 3.00 [get_ports {LTC1867_Data[*]}]
set_output_delay -clock { CLK50 } -max 4.00 [get_ports {LTC1867_Data[*]}]

set_output_delay -clock { CLK50 } -min 3.00 [get_ports {PA_TEMP[*]}]
set_output_delay -clock { CLK50 } -max 4.00 [get_ports {PA_TEMP[*]}]

set_output_delay -clock { CLK50 } -min 3.00 [get_ports {En_Vent}]
set_output_delay -clock { CLK50 } -max 4.00 [get_ports {En_Vent}]

set_output_delay -clock { CLK50 } -min 3.00 [get_ports {En_Almost_Ready}]
set_output_delay -clock { CLK50 } -max 4.00 [get_ports {En_Almost_Ready}]

set_output_delay -clock { CLK50 } -min 3.00 [get_ports {En_Ready}]
set_output_delay -clock { CLK50 } -max 4.00 [get_ports {En_Ready}]

set_output_delay -clock { CLK50 } -min 3.00 [get_ports {Force_Reset}]
set_output_delay -clock { CLK50 } -max 4.00 [get_ports {Force_Reset}]

set_output_delay -clock { CLK50 } -min 3.00 [get_ports {Enable_Reset}]
set_output_delay -clock { CLK50 } -max 4.00 [get_ports {Enable_Reset}]

set_output_delay -clock { CLK50 } -min 3.00 [get_ports {TEC_Ver_Info[*]}]
set_output_delay -clock { CLK50 } -max 4.00 [get_ports {TEC_Ver_Info[*]}]

set_output_delay -clock { CLK50 } -min 3.00 [get_ports {Standby}]
set_output_delay -clock { CLK50 } -max 4.00 [get_ports {Standby}]

set_output_delay -clock { CLK50 } -min 3.00 [get_ports {Status_LED1[*]}]
set_output_delay -clock { CLK50 } -max 4.00 [get_ports {Status_LED1[*]}]

set_output_delay -clock { CLK50 } -min 3.00 [get_ports {HV_Mr}]
set_output_delay -clock { CLK50 } -max 4.00 [get_ports {HV_Mr}]

set_output_delay -clock { CLK50 } -min 3.00 [get_ports {HV_CLK}]
set_output_delay -clock { CLK50 } -max 4.00 [get_ports {HV_CLK}]

set_output_delay -clock { CLK50 } -min 3.00 [get_ports {HV_State_Code[*]}]
set_output_delay -clock { CLK50 } -max 4.00 [get_ports {HV_State_Code[*]}]
         

#**************************************************************
# Set Clock Groups
#**************************************************************



#**************************************************************
# Set False Path
#**************************************************************



#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

