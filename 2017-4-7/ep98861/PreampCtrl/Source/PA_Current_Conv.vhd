---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	PA_Current_COnv
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
-- 	Author         : Michael C. Solazzi
--   Created        : January 5, 2010
--   Board          : Artemis DPP Board
--   Schematic      : 4035.007.25500 Group 130
--
--	Top Level		: 4035.003.99860 S/W, Artemis, Preamp
--
--   Description:	
--			Converts the ADC value for TEC1 and TEC2 current into a reading in mA.
--	History
--		04/27/11 - MCS - Ver 1.00
--			Initial Creation
---------------------------------------------------------------------------------------------------
library LPM;
     use lpm.lpm_components.all;
     
     
library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

---------------------------------------------------------------------------------------------------
entity PA_Current_Conv is
	port(
		CLK50			: in		std_logic;
		SLOPE			: in		std_logic_Vector( 31 downto 0 );
		INTERCEPT			: in		std_logic_Vector( 31 downto 0 );
		Din				: in		std_logic_Vector( 15 downto 0 );
		Iout				: buffer	std_logic_Vector( 15 downto 0 ) );
end PA_Current_Conv;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture behavioral of PA_Current_Conv is 	
--	constant ADC_SLOPE				: integer := 1966;			-- 9 3e-5 * 1000 * 2^16
--	constant ADC_INTERCEPT 			: integer := -924058;		-- -0.0141 * 1000 * 2^16

	-- Temperature Thresholds --------------------------------------------------------------------
	
	-- Signal Declarations -----------------------------------------------------------------------
	signal Pa_Temp_Product			: std_logic_Vector( 31 downto 0 );
	signal PA_Temp_Sum				: std_logic_Vector( 31 downto 0 );
	
	
	----------------------------------------------------------------------------------------------
	component PA_Temp_Mult
		port( 
			clock				: IN STD_LOGIC ;
			dataa				: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
			datab				: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
			result				: OUT STD_LOGIC_VECTOR (31 DOWNTO 0) );
	end component;	
	----------------------------------------------------------------------------------------------
	
begin
	----------------------------------------------------------------------------------------------
	-- Multiply ADC Channel 0 by the Diode ADC Slope constant
	PA_Temp_Mult_Inst : PA_Temp_Mult
		port map(
			clock			=> CLK50,
			dataa			=> Din,
--			datab			=> conv_std_logic_vector( SLOPE, 16 ),
			datab			=> Slope( 15 downto 0 ),
			result			=> Pa_Temp_Product );
			
	PA_Temp_Adder_Inst : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION 	=> "SIGNED",
			LPM_DIRECTION		=> "ADD",
			LPM_PIPELINE		=> 1 )
		port map(
			clock			=> CLK50,
			cin				=> '0',
			dataa			=> PA_Temp_Product,
			datab			=> Intercept,
--			datab			=> conv_std_logic_Vector( INTERCEPT, 32 ),
			result			=> PA_Temp_Sum );
	
	----------------------------------------------------------------------------------------------
	Iout_Proc : process( clk50 )
	begin
		if( rising_edge( clk50 )) then
			Iout <= PA_Temp_Sum( 31 downto 16 );
		end if;
	end process;
	----------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
end behavioral;			-- PA_Current_Conv.vhd
---------------------------------------------------------------------------------------------------

