---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	PA_Temp_Conv
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
-- 	Author         : Michael C. Solazzi
--   Created        : January 5, 2010
--   Board          : Artemis DPP Board
--   Schematic      : 4035.007.25500 Group 130
--
--	Top Level		: 4035.003.99860 S/W, Artemis, Preamp
--
--   Description:	
--			This module takes the commands to program the:
--				Serial Pots
--				HV Enable sequencer
--				Serial A/D Converter
--			on the TEC Control Board
--	History
--		01/05/10 - MCS - Ver 1.00
--			Initial Creation
---------------------------------------------------------------------------------------------------
library LPM;
     use lpm.lpm_components.all;
     
     
library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

---------------------------------------------------------------------------------------------------
entity PA_Temp_Conv is
	port(
		CLK50			: in		std_logic;
		Din				: in		std_logic_Vector( 15 downto 0 );
		PA_TEMP_OFFSET		: in		std_logic_vector( 31 downto 0 );
		PA_TEMP_TARGET		: in		std_logic_vector( 31 downto 0 );
		PA_TEMP			: buffer	std_logic_Vector( 31 downto 0 );
		En_Vent			: buffer	std_logic;
		En_Hv			: buffer	std_logic;
		En_Almost_Ready	: buffer	std_logic;
		En_Ready			: buffer	std_logic;
		Temp_Min_Err_Cmp	: buffer	std_logic;
		Temp_Max_Err_Cmp	: buffer	std_logic;
		Temp_Error		: buffer	std_logic_Vector( 31 downto 0 ) );
end PA_Temp_Conv;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture behavioral of PA_Temp_Conv is 	
	-- Temperature Measurements ------------------------------------------------------------------
	-- 
	--	m = -15 - 22
	--     ------------ = -18.5
	--       8.3 - 6.3
	-- and
	--
	-- b = 22 - m*6.3 = -116.55
	-- 
	-- Diode Temp to ADC had a 1K * 3K Voltage Divider = 1/4
	-- ADC Slope 		= 2.5 * 4.0 * Diode Slope = -185
	-- ADC Intercept 	= 22 - Diode Intercept = 138.55	
	--
	-- Now Diode Temp to ADC had a 1K * 4.7K Voltage Divider = 1/5.7
	-- ADC Slope (Now)	= 2.5 * 5.7 * Diode Slope = -263
	-- ADC Intercept 	= 22 - Diode Intercept = 138.55	
	
	-- Now take ADC Channel 0 Mult by ADC Slope ( two 16-bit numbers => 32 bit number with DP between bits 15 and 16 )
	-- Add the ADC Offeset (DP in the same location
	-- however ADC_ Slope wants to be negative, but to keep an unsigned multiplier
	-- ADC_INTERCEPT - ( ADC_SLOPE * ADC Input)
	constant ADC_SLOPE				: integer := 263;
	constant ADC_INTERCEPT 			: integer := (( 13855 * 65536 ) / 100 ) + ( 15 * 65536 );	-- Always add a 15dec C offset

	constant Thr_Warm				: integer :=  15;	-- ADC < Thr_Warm 
	constant Thr_HV				: integer :=  -15;	-- ADC > Thr_HV   
	
	constant	Thr_Min_Val 			: integer :=   -45;	-- too Cold - something wrong (Less than this value)
		
	constant	Thr_Max_Val			: integer :=	40;	-- Too warm - something wrong 
	constant 	Almost_Ready_Temp_Error	: integer := ( 65536 * 5 ) / 1;	-- 1/1 degree
	constant 	Ready_Temp_Error		: integer := ( 65536 * 1 ) / 1;	-- 1/1 degree
	-- Temperature Thresholds --------------------------------------------------------------------
	
	-- Signal Declarations -----------------------------------------------------------------------
	signal Pa_Temp_Product			: std_logic_Vector( 31 downto 0 );
	signal PA_Temp_Sum				: std_logic_Vector( 31 downto 0 );
	
	signal Temp_Error_Abs		: std_logic_Vector( 31 downto 0 );		
	
	----------------------------------------------------------------------------------------------
	component PA_Temp_Mult
		port( 
			clock				: IN STD_LOGIC ;
			dataa				: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
			datab				: IN STD_LOGIC_VECTOR (15 DOWNTO 0);
			result				: OUT STD_LOGIC_VECTOR (31 DOWNTO 0) );
	end component;	
	----------------------------------------------------------------------------------------------
	
begin
	----------------------------------------------------------------------------------------------
	-- Multiply ADC Channel 0 by the Diode ADC Slope constant
	PA_Temp_Mult_Inst : PA_Temp_Mult
		port map(
			clock			=> CLK50,
			dataa			=> Din,
			datab			=> conv_std_logic_vector( ADC_SLOPE, 16 ),
			result			=> Pa_Temp_Product );
			
	PA_Temp_Adder_Inst : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION 	=> "SIGNED",
			LPM_DIRECTION		=> "SUB",
			LPM_PIPELINE		=> 1 )
		port map(
			clock			=> CLK50,
			cin				=> '1',
			dataa			=> conv_std_logic_Vector( ADC_INTERCEPT, 32 ),
			datab			=> PA_Temp_Product,
			result			=> PA_Temp_Sum );

	PA_Temp_Inst : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION 	=> "SIGNED",
			LPM_DIRECTION		=> "ADD",
			LPM_PIPELINE		=> 1 )
		port map(
			clock			=> CLK50,
			cin				=> '0',
			dataa			=> PA_Temp_Sum,
			datab			=> PA_TEMP_OFFSET,
			result			=> PA_Temp );
	-- Multiply ADC Channel 0 by the Diode ADC Slope constant			
	----------------------------------------------------------------------------------------------


	-- If Temp (Dout) > Warm Threshold Allow Vent ------------------------------------------------
	En_Vent_Compare : lpm_compare
		generic map(
			LPM_WIDTH			=> 16,
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_PIPELINE		=> 1 )
		port map(
			clock			=> CLK50,
			dataa			=> PA_Temp( 31 downto 16 ),
			datab			=> conv_std_logic_Vector( Thr_Warm, 16 ),
			agb				=> En_Vent );
	-- If Temp (Dout) > Warm Threshold Allow Vent ------------------------------------------------

	-- If Temp (Dout) < HV Threshold - Allow to turn-on HV ---------------------------------------
	En_HV_Compare : lpm_compare
		generic map(
			LPM_WIDTH			=> 16,
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_PIPELINE		=> 1 )
		port map(
			clock			=> CLK50,
			dataa			=> PA_Temp( 31 downto 16 ),
			datab			=> conv_std_logic_Vector( Thr_HV, 16 ),
			alb				=> En_Hv );
	-- If Temp (Dout) < HV Threshold - Allow to turn-on HV ---------------------------------------

	-- if Temp (Dout) < some min threshold - error -----------------------------------------------
	Min_HV_Compare : lpm_compare
		generic map(
			LPM_WIDTH			=> 16,
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_PIPELINE		=> 1 )
		port map(
			clock			=> CLK50,
			dataa			=> PA_Temp( 31 downto 16 ),
			datab			=> conv_std_logic_Vector( Thr_Min_Val, 16 ),
			alb				=> Temp_Min_Err_Cmp );
	-- if Temp (Dout) < some min threshold - error -----------------------------------------------

	-- if Temp (Dout) > some max threshold - error -----------------------------------------------
	Max_HV_Compare : lpm_compare
		generic map(
			LPM_WIDTH			=> 16,
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_PIPELINE		=> 1 )
		port map(
			clock			=> CLK50,
			dataa			=> PA_Temp( 31 downto 16 ),
			datab			=> conv_std_logic_Vector( Thr_Max_Val, 16 ),
			agb				=> Temp_Max_Err_Cmp );
	-- if Temp (Dout) > some max threshold - error -----------------------------------------------
	
	Target_Temp_Diff_Add_Sub : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION 	=> "SIGNED",
			LPM_DIRECTION		=> "SUB" )
		port map(
			cin				=> '1',
			dataa			=> PA_Temp,			-- measurement
			datab			=> PA_TEMP_TARGET,		-- Setpoint
			result			=> Temp_Error );
	
	Target_Temp_Diff_Aba : lpm_abs
		generic map(
			LPM_WIDTH			=> 32 )
		port map(
			data				=> Temp_Error,
			result			=> Temp_Error_Abs );

	En_Almost_Ready_Compare : lpm_compare
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "UNSIGNED",
			LPM_PIPELINE		=> 1 )
		port map(
			clock			=> CLK50,
			dataa			=> Temp_Error_Abs,
			datab			=> conv_std_Logic_Vector( Almost_Ready_Temp_Error, 32 ),
			alb				=> En_Almost_Ready );
			
	En_Ready_Compare : lpm_compare
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "UNSIGNED",
			LPM_PIPELINE		=> 1 )
		port map(
			clock			=> CLK50,
			dataa			=> Temp_Error_Abs,
			datab			=> conv_std_Logic_Vector( Ready_Temp_Error, 32 ),
			alb				=> En_Ready );
	-- If Temp (Dout) < Operation Setpoint -------------------------------------------------------

---------------------------------------------------------------------------------------------------
end behavioral;			-- PreampCtrl.vhd
---------------------------------------------------------------------------------------------------

