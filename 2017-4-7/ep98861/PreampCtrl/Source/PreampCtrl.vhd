---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	ep98880.vhd
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
-- 	Author         : Michael C. Solazzi
--   Created        : January 5, 2010
--   Board          : Artemis DPP Board
--   Schematic      : 4035.007.25500 Group 130
--
--	Top Level		: 4035.003.99860 S/W, Artemis, Preamp
--
--   Description:	
--			This module takes the commands to program the:
--				Serial Pots
--				HV Enable sequencer
--				Serial A/D Converter
--			on the TEC Control Board
--	History
--		7/1/13: Modified loigc around New_TEC1_Val to llok at more bits to be sure the size of the TEC2*TEC1_Factor < max pgm value.  If not set New_TEC1_Val to Max
--				:changed time (Delay_IGR) after IG is turned ON from 1 s to 4 s.  This is in support of 31200 TEC board.  give voltages time to stablize.
--		3/26/13:  Added CW_FORCE_LV.  CW_FORCE_LV	will turn on IG, R1, FD and RD on rev 3 TEC boards.  REV 3 TEC boards only turn on FD once IG is turned on.
--					 The SD3+ modules oscillate with FD on and module warm.  power up sequence has been adjusted in Rev 3 preAMP.

--    3/22/13:  Max TEC current is dependent on the TEC revision level.  
--              Increased current limit for rev 3 TEC board:  TEC1_MAX_CURRENT_rev3 =1.9A, TEC1_MAX2_CURRENT_rev3 = 2A and TEC2_MAX_CURRENT_rev3 = 2A.  
--
--		09/21/11 - Rev 1.1
--		06/30/11 - Rev 1.0 - MCS
--			Module: PreampCtrl
--				Added "PID Control" to TEC1 is now a TEC1_FACTOR of TEC2 during PID control (provided the MSB of the POT_VAL register is set to a 1.
--		11040701:
--			Changed Temp Error Checking to Idle and Cooling is enabled
--		11030701: 
--			Used error checking only in the Offline State
--		11021601:
--			Add ST_OFF_TEC2_START
--			and ST_OFF_TEC2_DELAY
--			which will use the same TEC2 delay time and 1st turn off TEC1, keep TEC2 on for that amount of time
--			and then turn off TEC2
--		11011401:
--			Swapped turn on sequence for TECs such that:
--			TEC1 turns on
--			wait 30 seconds
--			TEC2 turns on
--		10123002: 
--			Generate Status_LED1 from within this module, export 2-bit code for LED
--			which responds to temperature and states.
---------------------------------------------------------------------------------------------------
library LPM;
     use lpm.lpm_components.all;
     
     
library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

---------------------------------------------------------------------------------------------------
entity PreampCtrl is
	port(
		CLK50					: in		std_logic;
		Pot_Val				: in		std_logic_vector( 31 downto 0 );
		Det_Ready			: in		std_logic;
		ms1_tc				: in		std_logic;
		Ext_Enable			: in		std_logic;					-- Interlock from External Source - Must be enabled		
		CMD_WD				: in      std_logic;
		i_SDOUT				: in		std_logic;		
		Det_Sat				: in		std_logic;
		TEC1_Factor			: in		std_logic_Vector( 31 downto 0 );
		TEC2_DELAY			: in		std_logic_vector( 31 downto 0 );
		DPP_Temp				: in		std_logic_Vector( 31 downto 0 );	-- Only use lower 16-bits and divide by 256 for degrees C		
		HV_CWORD				: in		std_logic_vector( 31 downto 0 );
		PA_TEMP_TARGET		: in		std_logic_vector( 31 downto 0 );
		PA_TEMP_OFFSET		: in		std_logic_vector( 31 downto 0 );
		PA_TEC2_KIKP		: in		std_logic_vector( 31 downto 0 );		
		o_SCLK		: buffer	std_logic;
		o_nAEN		: buffer	std_logic;
		o_nDEN		: buffer	std_logic;
		o_SDIN		: buffer	std_logic;
		o_SPARE		: buffer	std_logic;
		LTC1867_Data		: buffer	std_logic_Vector( 127 downto 0 );
		PA_TEMP				: buffer	std_logic_Vector( 31 downto 0 );
		En_Vent				: buffer	std_logic;
		En_Almost_Ready	: buffer  std_logic;
		En_Ready				: buffer	std_logic;		
		Force_Reset			: buffer	std_logic;
		Enable_Reset		: buffer	std_logic;
      TEC_Ver_Info   	: buffer  std_logic_Vector( 31 downto 0 ):= (others=>'0');
      Standby        	: buffer  std_logic;
		Status_LED1			: buffer	std_logic_Vector( 1 downto 0 );
		HV_Mr					: buffer	std_logic;
		HV_CLK				: buffer	std_logic;					-- Clock to HV Generator - Constant Frequency
		HV_State_Code		: buffer	std_logic_Vector( 4 downto 0 );		
		TEC_Current			: buffer	std_logic_Vector( 31 downto 0 ) );
end PreampCtrl;
---------------------------------------------------------------------------------------------------
                    

---------------------------------------------------------------------------------------------------
architecture behavioral of PreampCtrl is 
	
	constant CLOCK_PER						: integer := 20;				-- 20ns => 50Mhz
	constant SAT_CNT_MAX						: integer := 10000;				-- 10 sec @ 1ms
	constant Flash_Cnt_Max					: integer := 500;
	constant kPID_TEC1						: integer := 31;
	constant New_TEC1_Val_LSB 				: integer := 16;
	-- LED Color Constants for front-panel Bi-Color LED (both Top and Bottom) --------------------
	constant LED_OFF				: std_logic_Vector( 1 downto 0 ) := "00";
	constant LED_RED				: std_logic_Vector( 1 downto 0 ) := "01";
	constant LED_GRN				: std_logic_Vector( 1 downto 0 ) := "10";
	constant LED_YEL				: std_logic_Vector( 1 downto 0 ) := "11";
	
     -- HV Register Bit Positions ----------------------------------------------------------------
	constant kHV_TEC2				: integer := 0;
	constant kHV_TEC1				: integer := 1;
	constant kHV_R1				: integer := 2;
	constant kHV_IGR				: integer := 3;
	constant kHV_BCBRRX				: integer := 4;
	constant kHV_EN_RST				: integer := 5;
	constant kHV_EN_6				: integer := 6;	
     -- HV Register Bit Positions ----------------------------------------------------------------

	-- Temperature Thresholds --------------------------------------------------------------------	
	constant Low_Dpp_Temp_Val_Hi 		: integer := 50 * 256;			-- If DPP Temp falls below this threshold set TEC1 Max Current in half
	constant Low_Dpp_Temp_Val_Lo 		: integer := 48 * 256;			-- If DPP Temp falls below this threshold set TEC1 Max Current in half
	-- Temperature Thresholds --------------------------------------------------------------------	
	
	-- Preamp Control Address constants ----------------------------------------------------------
	constant iADR_HVEN 				: integer := 0;
	constant iADR_AD				: integer := 1;
	constant iADR_POT0				: integer := 2;		-- Not used
	constant iADR_POT1				: integer := 3;		-- Max TEC1 Current
	constant iADR_POT2				: integer := 4;		-- Max TEC2 Current
	constant iADR_POT3				: integer := 5;		-- Not used
     constant iADR_VER                  : integer := 6;
	-- Preamp Control Address constants ----------------------------------------------------------

	-- LTC1867 Control Register Bit Positions ----------------------------------------------------
	constant BIT_SD 				: integer := 0;		-- Single End 		- Set to 1
	constant BIT_OS 				: integer := 1;		-- Odd/Even		- A0
	constant BIT_S1 				: integer := 2;		-- Address Select 1	- A2
	constant BIT_S0 				: integer := 3;		-- Address Select 0	- A1
	constant BIT_COM				: integer := 4;		-- Com Config 		- Set to 1
	constant BIT_UNI 				: integer := 5;		-- Unipolar Mode 	- Set to 1
	constant BIT_SLP 				: integer := 6;		-- Sleep Mode		- Set to 0
	-- LTC1867 Control Register Bit Positions ----------------------------------------------------

     -- ADC Channel Definitions -------------------------------------------------------------------
	constant ADC_DTEMP				: integer := 0;		-- ADC Channel Positions	
     constant ADC_P24VFUSE              : integer := 1;          -- Rev C
	constant ADC_TEC1_VOLT			: integer := 2;
	constant ADC_TEC1_CURR			: integer := 3;
	constant ADC_TEC2_VOLT			: integer := 4;
	constant ADC_TEC2_CURR			: integer := 5;	
     constant ADC_P9V                   : integer := 6;
     constant ADC_N9V                   : integer := 7;
	-- LTC1867 Control Register Bit Positions ----------------------------------------------------
	
     -- TEC Control Board Interface State Machine -------------------------------------------------
	constant ST_Idle				: integer := 0;
     constant ST_VerInfo                : integer := 1;     
	constant St_HvEn				: integer := 2;
	constant St_SADC				: integer := 3;
	constant St_Pot1				: integer := 4;
	constant St_Pot2				: integer := 5;
	constant St_Addr				: integer := 6;
	constant St_Data				: integer := 7;
	constant St_Done				: integer := 8;
	constant St_LTC1867				: integer := 9;
	constant St_LTC1867_Conv1		: integer := 10;
	constant St_LTC1867_Data1		: integer := 11;
	constant St_LTC1867_Done1 		: integer := 12;
     constant St_LTC1867_Addr2          : integer := 13;
	constant St_LTC1867_Conv2		: integer := 14;
	constant St_LTC1867_Data2		: integer := 15;
	constant St_LTC1867_Done2 		: integer := 16;
     constant St_Ver_Data_Conv          : integer := 17;
	constant St_Ver_Data               : integer := 18;
	constant St_Ver_Done               : integer := 19;         -- Must be last
     -- TEC Control Board Interface State Machine -------------------------------------------------
          
	-- HV Control Word Bit Positions -------------------------------------------------------------
	constant CW_ONLINE 			     : integer := 8; 	-- HV_OnOff		: in		std_logic;
	constant CW_FORCE_COOL 			: integer := 9; 	-- Force_Cool		: in		std_logic;					-- Force the Cooling to be on
	constant CW_FORCE_HV			: integer := 10; 	-- Force_HV		: in		std_logic;					-- Force the HV to be on
	
	constant CW_FORCE_LV			: integer := 11; 	-- Force the Low Voltages (IG, R1, FD and RD) to be on.  This is needed to set diode offset temp.
	
	constant CW_FORCE_RESET			: integer := 12;
	constant CW_ENABLE_RESET			: integer := 13;
	constant CW_TEC1_DISABLE			: integer := 16;
	constant CW_TEC2_DISABLE			: integer := 17;
	constant CW_MANUAL				: integer := 31;	-- ( Then uses bits 7:0)
	-- HV Control Word Bit Positions -------------------------------------------------------------

	constant Force_Reset_Cnt_Max		: integer := 1000 / CLOCK_PER;
	
	-- HV/On Off Control Constants ---------------------------------------------------------------
	constant ST_Off				: integer := 0;
	constant ST_Force_Cool			: integer := 1;
	constant St_Force_HV			: integer := 2;
	constant St_Force_LV			: integer := 3;
	constant St_Full_TEC2_Delay		: integer := 12;
	constant St_Full_TEC1_Delay		: integer := 15;
	constant ST_R1					: integer := 16;
	constant ST_IGR				: integer := 17;
	constant ST_BCBRBX				: integer := 18;
	constant ST_EnRESET				: integer := 19;
	constant ST_LAst				: integer := 20;
	constant ST_On					: integer := 21;
	constant St_No_Ext_En			: integer := 22;
	constant St_Error				: integer := 23;
     constant St_WDog                   : integer := 24;
	constant St_Manual				: integer := 25;
	constant St_Det_Sat				: integer := 26;
	constant St_Off_TEC2_Start 		: integer := 27;
	constant St_Off_TEC2_DELAY 		: integer := 28;		-- Must be last	
	-- HV/On Off Control Constants ---------------------------------------------------------------
	
	--                               
	constant Delay_TEC_Min			: integer :=   1000;	-- 1 sec
	constant NO_TEMP_TIMEOUT			: integer := 180000;	-- 3 minutes to reach temperature
	constant Delay_R1				: integer :=     10;	-- 1 ms
--	constant Delay_IGR				: integer := 	1000;	-- 1 sec
	constant Delay_IGR				: integer := 	4000;	-- 4 sec
	
	constant Delay_BCBRBX			: integer := 	1000;	-- 1 sec
	
	
	constant Delay_EnReset			: integer := 	  10;	-- 10 ms
	constant Delay_Last				: integer := 	  10;	-- 10 ms
	constant Delay_Max				: integer := 180000;	
	
	-- HV Clock measured from Spectro board is a 50% duty cycle with a period of 2977.2ns
	
	-- HV/On Off Control Constants ---------------------------------------------------------------
	constant Clk_Cnt_Max			: integer := ( 250 / CLOCK_PER )-1;	-- 250ns at 10ns/clock
	constant HV_Clk_Cnt_Max 			: integer := ( 2977 /( 2 * CLOCK_PER ))-1;			-- 240ns square wave - 50% duty cycle @ 100Mhz
	constant LTC1867_Conv_Max		: integer := 399 / CLK_CNT_MAX;		-- 4 us at 250 ns
	-- HV/On Off Control Constants ---------------------------------------------------------------

	-- TEC1/TEC2 Max Currents
	-- 9/20/11 - Original Calibration Curves for Rev 1.0 TEC Control Board
	constant REV10_TEC1_SLOPE		: integer := 1966;			--  3e-5 * 1000 * 2^16
	constant REV10_TEC1_INTERCEPT 	: integer := -924058;		-- -0.0141 * 1000 * 2^16	
	
	constant REV10_TEC2_SLOPE		: integer := 3932;			--  6e-5 * 1000 * 2^16
	constant REV10_TEC2_INTERCEPT 	: integer := -5747508;		-- -0.0877 * 1000 * 2^16	
	-- 9/20/11 - Original Calibration Curves for Rev 1.0 TEC Control Board

	-- 9/21/11 - Updated Curves from Mahwah for Rev 1.1 and greater TEC Control Board.
	constant REV11_TEC1_SLOPE		: integer := 2510;			--  4e-5 * 1000 * 2^16
	constant REV11_TEC1_INTERCEPT 	: integer := -8802;		-- -0.0595 * 1000 * 2^16	
	
	constant REV11_TEC2_SLOPE		: integer := 2523;			--  4e-5 * 1000 * 2^16
	constant REV11_TEC2_INTERCEPT 	: integer := -76533;		-- -0.022 * 1000 * 2^16		
	-- 9/21/11 - Updated Curves from Mahwah for Rev 1.1 and greater TEC Control Board.

--	-- 9/20/11 - New Calibration Curves for Rev 1.1 and Greater TEC Control Board
--	constant REV11_TEC1_SLOPE		: integer := 2611;			--  4e-5 * 1000 * 2^16
--	constant REV11_TEC1_INTERCEPT 	: integer := 3899392;		-- -0.0595 * 1000 * 2^16	
--	
--	constant REV11_TEC2_SLOPE		: integer := 2611;			--  4e-5 * 1000 * 2^16
--	constant REV11_TEC2_INTERCEPT 	: integer := 1441792;		-- -0.022 * 1000 * 2^16	
--	-- 9/20/11 - New Calibration Curves for Rev 1.1 and Greater TEC Control Board


--------------
-------------TEC current limits

--	constant TEC1_MAX_CURRENT_b4_rev3		: integer := 1000;			-- mA
--	constant TEC1_MAX2_CURRENT_b4_rev3		: integer := 1500;			-- mA
--	constant TEC1_MAX_CURRENT_rev3			: integer := 1900;			-- mA
--	constant TEC1_MAX2_CURRENT_rev3			: integer := 2000;			-- mA


	
--	signal TEC1_MAX_CURRENT					: integer range TEC1_MAX_CURRENT_b4_rev3 to TEC1_MAX_CURRENT_rev3 := TEC1_MAX_CURRENT_b4_rev3;			-- mA
--	signal TEC1_MAX2_CURRENT					: integer range TEC1_MAX2_CURRENT_b4_rev3 to TEC1_MAX2_CURRENT_rev3 := TEC1_MAX2_CURRENT_b4_rev3;			-- mA
	constant TEC1_MAX_CURRENT					: integer := 1000;			-- mA
	constant TEC1_MAX2_CURRENT					: integer  := 1200;			-- mA
	constant TEC2_MAX_CURRENT					: integer := 2000;			-- mA

-------------TEC current limits
------------------

	constant TEC1_Busy_Cnt_Max 		: integer := 8;
	-- Signal Declarations -----------------------------------------------------------------------
	signal addr 						: std_logic_Vector( 3 downto 0 ) := (others=>'0');
	
	signal Clk_Cnt						: integer range 0 to Clk_Cnt_Max := 0;
	signal Clk_Cnt_Tc					: std_logic := '0';
	signal Cnt							: integer range 0 to 31 := 0;
	signal Cnt_Max						: integer range 0 to 31 := 0;
     signal CMD_WDog_Latch              : std_logic := '0';
	
	signal data							: std_logic_Vector( 15 downto 0 ):= (others=>'0');
	signal Delay_Cnt					: std_logic_Vector( 31 downto 0 );

	signal En_Hv						: std_logic;
	
	signal Force_Reset_Cnt 			: integer range 0 to Force_Reset_Cnt_Max;
	signal Force_Reset_Del			: std_logic;
	signal Flash_Cnt					: integer range 0 to Flash_Cnt_Max;
	signal Flash						: std_logic;
	signal Force_Cool_Del			: std_logic;
		
	signal Hv_En_Reg					: std_logic_Vector( 7 downto 0 ):= (others=>'0');	
	signal HV_State 					: integer range 0 to St_Off_TEC2_DELAY := 0;
	signal HV_Clk_Cnt 				: integer range 0 to HV_CLk_Cnt_Max := 0;	
	signal HV_En						: std_logic_Vector( 7 downto 0 );

--	signal ld_pot1_val				: std_logic_vector( 7 downto 0 ) := (others=>'0');
	signal ld_pot2_val				: std_logic_vector( 7 downto 0 ) := (others=>'0');	
	signal Local_TEC1_Val			: std_logic_vector( 7 downto 0 );
	
	signal LTC1867_Adr				: std_logic_vector( 2 downto 0 ):= (others=>'0');
     signal LTC1867_Addr_Reg            : std_logic_Vector( 2 downto 0 ):= (others=>'0');

	signal LTC1867_CReg				: std_logic_Vector( BIT_SLP downto 0 ):= (others=>'0');
	signal Ld_LTC1867					: std_logic := '0';
	signal LTC_1867_Conv_Cnt			: integer range 0 to LTC1867_Conv_Max := 0;
	signal ld_hven						: std_logic := '0';
	signal Ld_Pot						: std_logic_Vector( 2 downto 1 ):= (others=>'0');
     signal Ld_Tec_Ver_Info   	: std_logic := '0';

	signal New_TEC1_Val				: std_logic_Vector(  39 downto 0 );
	
	signal PA_TEC2_CMD				: std_logic_Vector(  7 downto 0 );
	signal PA_State 					: integer range 0 to St_Ver_Done := 0;
	signal PID_En 						: std_logic;	
	
	signal Sat_Cnt 					: integer range 0 to Sat_Cnt_Max;
	
	signal Temp_Error					: std_logic_Vector( 31 downto 0 );
	signal Temp_Min_Err_Cmp			: std_logic;
	signal Temp_Max_Err_Cmp			: std_Logic;
	
	signal TEC1_VAL_DEL				: std_logic_vector( 7 downto 0 ) := (others=>'0');
	signal TEC2_VAL_DEL				: std_logic_vector( 7 downto 0 ) := (others=>'0');
	signal TEC2_PID_VAL				: std_logic_vector( 7 downto 0 ) := (others=>'0');
	signal TEC2_PID_VAL_DEL			: std_logic_vector( 7 downto 0 ) := (others=>'0');
	signal TEC1_Max_Cmp 				: std_logic;
	signal TEC1_Max2_Cmp				: std_logic;
--	signal TEC2_Max_Cmp				: std_logic;
	signal TEC1_Busy					: std_logic;
	signal TEC1_Busy_Cnt 			: integer range 0 to TEC1_BUSY_CNT_MAX;
     
    signal TEC_Ver_SReg          : std_logic_Vector( 31 downto 0 ) := (others=>'0');
     
	signal TEC1_SLOPE					: std_logic_vector( 31 downto 0 );
	signal TEC1_INTERCEPT 			: std_logic_vector( 31 downto 0 );
	
	signal TEC2_SLOPE					: std_logic_vector( 31 downto 0 );
	signal TEC2_INTERCEPT 			: std_logic_vector( 31 downto 0 );

	-- Signal Declarations -----------------------------------------------------------------------
	
	----------------------------------------------------------------------------------------------
	component PA_Temp_Conv is
		port(
			CLK50							: in		std_logic;
			Din							: in		std_logic_Vector( 15 downto 0 );
			PA_TEMP_OFFSET				: in		std_logic_vector( 31 downto 0 );
			PA_TEMP_TARGET				: in		std_logic_vector( 31 downto 0 );
			PA_TEMP						: buffer	std_logic_Vector( 31 downto 0 );
			En_Vent						: buffer	std_logic;
			En_Hv							: buffer	std_logic;
			En_Almost_Ready			: buffer	std_logic;
			En_Ready						: buffer	std_logic;
			Temp_Min_Err_Cmp			: buffer	std_logic;
			Temp_Max_Err_Cmp			: buffer	std_logic;
			Temp_Error					: buffer	std_logic_Vector( 31 downto 0 ) );		
	end component PA_Temp_Conv;	
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	component PID_Algorithm is 
		port( 
			CLK50      	 	: in      std_logic;                         	-- Master Clock
			Temp_Clk			: in		std_logic;
			Temp_Enable		: in		std_logic;
			Temp_Error		: in		std_logic_vector( 31 downto 0 );
			KIKP				: in		std_Logic_Vector( 31 downto 0 );		-- Postion loop Gain 
			Cmd_Max			: in		std_logic_Vector(  7 downto 0 );
			TEC2_DELAY		: in		std_logic_Vector(  31 downto 0 );
			Delay_Cnt		: in		std_logic_Vector(  31 downto 0 );
			Cmd_Out			: buffer	std_logic_Vector(  7 downto 0 ) );		-- Position Error
	end component PID_Algorithm;
	----------------------------------------------------------------------------------------------
	

	---------------------------------------------------------------------------------------------------
	component PA_Current_Conv is
		port(		
			CLK50			: in		std_logic;
			SLOPE			: in		std_logic_Vector( 31 downto 0 );
			INTERCEPT			: in		std_logic_Vector( 31 downto 0 );
			Din				: in		std_logic_Vector( 15 downto 0 );
			Iout				: buffer	std_logic_Vector( 15 downto 0 ) );
	end component PA_Current_Conv;
	---------------------------------------------------------------------------------------------------	
	
	---------------------------------------------------------------------------------------------------	
	component TEC1_PID_MULT is
		port( 
			dataa		: in 		std_logic_Vector(  7 downto 0 );
			datab		: in			std_logic_Vector( 31 downto 0 );
			result		: out 		std_logic_Vector( 39 downto 0 ) );
	end component;
	---------------------------------------------------------------------------------------------------			
begin
	---------------------------------------------------------------------------------------------------
	TEC1_Current_Inst : PA_Current_Conv
		port map(
			CLK50		=> CLK50,
			SLOPE		=> TEC1_SLOPE,
			INTERCEPT		=> TEC1_INTERCEPT,
			Din			=> LTC1867_Data(  63 downto  48 ),
			Iout			=> TEC_Current( 15 downto 0 ));
			

	TEC2_Current_Inst : PA_Current_Conv
		port map(
			CLK50		=> CLK50,
			SLOPE		=> TEC2_SLOPE,
			INTERCEPT		=> TEC2_INTERCEPT,
			Din			=> LTC1867_Data(  95 downto  80 ),
			Iout			=> TEC_Current( 31 downto 16 ));


	TEC1_MAX_Current_Compare : lpm_compare
		generic map(
			LPM_WIDTH			=> 16,
			LPM_REPRESENTATION 	=> "SIGNED",
			LPM_PIPELINE		=> 1 )
		port map(
			clock			=> CLK50,
			dataa			=> TEC_Current( 15 downto 0 ),
			datab			=> conv_Std_logic_vector( TEC1_MAX_CURRENT, 16 ),
			ageb				=> TEC1_Max_Cmp );

	TEC1_MAX2_Current_Compare : lpm_compare
		generic map(
			LPM_WIDTH			=> 16,
			LPM_REPRESENTATION 	=> "SIGNED",
			LPM_PIPELINE		=> 1 )
		port map(
			clock			=> CLK50,
			dataa			=> TEC_Current( 15 downto 0 ),
			datab			=> conv_Std_logic_vector( TEC1_MAX2_CURRENT, 16 ),
			ageb				=> TEC1_Max2_Cmp );
			
--	TEC2_MAX_Current_Compare : lpm_compare
--		generic map(
--			LPM_WIDTH			=> 16,
--			LPM_REPRESENTATION 	=> "SIGNED",
--			LPM_PIPELINE		=> 1 )
--		port map(
--			clock			=> CLK50,
--			dataa			=> TEC_Current( 31 downto 16 ),
--			datab			=> conv_Std_logic_vector( TEC2_MAX_CURRENT, 16 ),
--			ageb				=> TEC2_Max_Cmp );

	----------------------------------------------------------------------------------------------
	-- Logic to convert the ADC Channel 0 Information into Diode Temperature Reading
	PA_Temp_Conv_Inst : PA_Temp_Conv 
		port map(
			CLK50			=> CLK50,
			Din				=> LTC1867_Data(  15 downto   0 ),
			PA_TEMP_OFFSET		=> PA_Temp_Offset,
			PA_TEMP_TARGET		=> PA_TEMP_TARGET,
			PA_TEMP			=> PA_Temp,
			En_Vent			=> En_Vent,
			En_Hv			=> En_HV,
			En_Almost_Ready	=> En_Almost_Ready,
			En_Ready			=> En_Ready,
			Temp_Min_Err_Cmp	=> Temp_Min_Err_Cmp,
			Temp_Max_Err_Cmp	=> Temp_Max_Err_Cmp,
			Temp_Error		=> Temp_Error );
	-- Logic to convert the ADC Channel 0 Information into Diode Temperature Reading
	----------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------
	PID_En <= '1' when (( HV_En( kHV_TEC2 ) = '1' ) and ( HV_CWORD( CW_FORCE_COOL) = '0' )) else '0';

	PID_Algorithm_Inst : PID_Algorithm 
		port map(
			CLK50			=> CLK50,
			Temp_Clk			=> ms1_tc,
			Temp_Enable		=> PID_En, 
			Temp_Error		=> Temp_Error,
			KIKP				=> PA_TEC2_KIKP,
			Cmd_Max			=> Pot_Val( 23 downto 16 ),
			Delay_Cnt		=> Delay_Cnt,
			TEC2_DELAY		=> TEC2_DELAY,			
			Cmd_Out			=> PA_TEC2_CMD );					
	----------------------------------------------------------------------------------------------	

	Clk_Cnt_Tc	<= '1' when ( Clk_Cnt = Clk_Cnt_Max ) else '0';

	-- IF HV is disabled - Force HV_MR active
	-- HV_MR will force everything on the TEC board off (including the cooling)
	HV_MR	<= '1' when ( conv_integer( HV_En ) = 0 ) else '0';
--	HV_MR	<= '1' when ( conv_integer( HV_En( kHV_EN_RST downto kHV_R1 )) = 0 ) else '0';
	----------------------------------------------------------------------------------------------
	
	----------------------------------------------------------------------------------------------
	-- The New TEC1 Value is some multiple of PA_TEC2_CMD;
	TEC1_PID_MULT_INST : TEC1_PID_MULT
		port map(
			dataa			=> PA_TEC2_CMD,		-- TEC2 Program Value
			datab			=> TEC1_Factor,		-- Factor to convert to TEC1
			result			=> New_TEC1_Val );
	----------------------------------------------------------------------------------------------
	
	
	clock_proc : process( CLK50 )
	begin
		if rising_edge( CLK50 ) then		
			
			-- TEC_Ver_Info
			--		Minor Version are bits 3:0
			--		Major Version are bits 7:4
			if( conv_integer( TEC_Ver_Info( 7 downto 4 )) = 0 ) then					-- Ver 0.x (Major)
				TEC1_SLOPE 	<= conv_std_logic_Vector( REV10_TEC1_SLOPE, 32 );
				TEC1_INTERCEPT <= conv_std_logic_Vector( REV10_TEC1_INTERCEPT, 32 );
				TEC2_SLOPE	<= conv_std_logic_Vector( REV10_TEC2_SLOPE, 32 );
				TEC2_INTERCEPT <= conv_std_logic_Vector( REV10_TEC2_INTERCEPT, 32 );
			elsif( conv_integer( TEC_Ver_Info( 7 downto 4 )) = 1 ) and ( conv_integer( TEC_Ver_Info( 3 downto 0 )) = 0 ) then	-- Ver 1.0
				TEC1_SLOPE 	<= conv_std_logic_Vector( REV10_TEC1_SLOPE, 32 );
				TEC1_INTERCEPT <= conv_std_logic_Vector( REV10_TEC1_INTERCEPT, 32 );
				TEC2_SLOPE	<= conv_std_logic_Vector( REV10_TEC2_SLOPE, 32 );
				TEC2_INTERCEPT <= conv_std_logic_Vector( REV10_TEC2_INTERCEPT, 32 );			
			else																-- Ver 1.1 and Greater
				TEC1_SLOPE 	<= conv_std_logic_Vector( REV11_TEC1_SLOPE, 32 );
				TEC1_INTERCEPT <= conv_std_logic_Vector( REV11_TEC1_INTERCEPT, 32 );
				TEC2_SLOPE	<= conv_std_logic_Vector( REV11_TEC2_SLOPE, 32 );
				TEC2_INTERCEPT <= conv_std_logic_Vector( REV11_TEC2_INTERCEPT, 32 );
			end if;
			----------------------------
			
			
--			-------------------------		
--			----set TEC current limit
--			if( conv_integer( TEC_Ver_Info( 7 downto 4 )) >= 3 ) then					-- Ver 0.x (Major)
--					TEC1_MAX_CURRENT		 	<= TEC1_MAX_CURRENT_rev3;			
--					TEC1_MAX2_CURRENT			<= TEC1_MAX2_CURRENT_rev3;	
--					
--			else		
--					TEC1_MAX_CURRENT		 	<= TEC1_MAX_CURRENT_b4_rev3;			
--					TEC1_MAX2_CURRENT			<= TEC1_MAX2_CURRENT_b4_rev3;						
--	
--			end if;
--			----set TEC current limit
--			-------------------------	

	
			if(( HV_CWORD( CW_ENABLE_RESET ) = '1' ) and ( HV_En( kHV_EN_RST ) = '1' ))					-- Enable Reset
				then Enable_Reset <= '1';
				else Enable_Reset <= '0';
			end if;
			Force_Reset_Del <= HV_CWORD( CW_FORCE_RESET );
               
			-- Force a Reset to the Preamp -----------------------------------------------------
			if(( HV_CWORD( CW_FORCE_RESET ) = '1' ) and ( Force_Reset_Del = '0' ))
				then Force_Reset <= '1';
			elsif( Force_Reset_Cnt >= Force_Reset_Cnt_Max )
				then Force_Reset <= '0';
			end if;
			
			if( Force_Reset = '0' )
				then Force_Reset_Cnt <= 0;
				else Force_Reset_Cnt <= Force_Reset_Cnt + 1;
			end if;
			-- Force a Reset to the Preamp -----------------------------------------------------
		
			-- Look for a change in the HVEN Register ------------------------------------------
			hv_en_Reg <= hv_en;
			if( hv_en_reg /= hv_en )
				then ld_hven <= '1';
			elsif( PA_State = St_Hven )
				then ld_hven <= '0';
			end if;
			-- Look for a change in the HVEN Register ------------------------------------------

			-- TEC1 Max Current ----------------------------------------------------------------			
			-- Pot_Val( 15 downto 8 ) is the "Max Programmed value for TEC1
			-- TEC1_VAL is only used to sense when "Pot_Val" has changed
			-- Local_TEC1_VAL is a local value for TEC1
			TEC1_VAL_DEL	<= Pot_Val( 15 downto 8 );
			
			if( Ld_Pot(1) = '1' ) then
				TEC1_Busy 	<= '1';
				TEC1_Busy_Cnt 	<= 0;
			elsif( ms1_tc = '1' ) then
				if( TEC1_Busy_Cnt = TEC1_Busy_Cnt_Max )
					then TEC1_Busy 	<= '0';
					else TEC1_Busy_Cnt 	<= TEC1_Busy_Cnt + 1;
				end if;
			end if;
			
			if( Pot_Val( kPID_TEC1) = '0' ) then   ---when DTM is off
				-- Normal Constant Current TEC1 Current Driver --------------------------------------
				-- if Max Current changed, update with new value
				if( Pot_Val( 15 downto 8 ) /= TEC1_VAL_DEL ) then
					ld_pot(1) 	<= '1';
					Local_TEC1_Val <= Pot_Val( 15 downto 8 );
					
--------------------					
				elsif( Local_TEC1_Val /= Pot_Val( 15 downto 8 )) then
					ld_pot(1) 	<= '1';
					Local_TEC1_Val <= Pot_Val( 15 downto 8 );
---------------------					
					
				-- after currnet has changed, and if measured current exceeded "max Currnet, drop programmed value
				elsif(( TEC1_Max_Cmp = '1' ) and ( TEC1_Busy = '0' ) and ( Local_TEC1_Val > 4 )) then
					ld_pot(1) 	<= '1';
					if( TEC1_Max2_Cmp = '1' ) 				
						then Local_TEC1_Val <= Local_TEC1_Val - 1;		
						else Local_TEC1_Val <= Local_TEC1_Val - 4;		
					end if;
				elsif( PA_State = St_Pot1 )
					then Ld_Pot(1) <= '0';
			
				end if;
				-- Normal Constant Current TEC1 Current Driver --------------------------------------
			else				
				-- Leading edge of Force_Cool - Load "Pot_Val" 
				if(( HV_CWORD(CW_FORCE_COOL) = '1' ) and ( FORCE_COOL_DEL = '0' )) then
					ld_pot(1) 	<= '1';
					Local_TEC1_Val <= Pot_Val( 15 downto 8 );
				-- During Force Cool and Max Current (Pot_Val) changed
				elsif(( HV_CWORD( CW_FORCE_COOL ) = '1' ) and ( Pot_Val( 15 downto 8 ) /= TEC1_VAL_DEL )) then
					ld_pot(1) 	<= '1';
					Local_TEC1_Val <= Pot_Val( 15 downto 8 );
					
				-- Not Forced Cooling and the TEC2 CMD has changed
				elsif(( HV_CWORD( CW_FORCE_COOL ) = '0' ) and ( PA_TEC2_CMD /= TEC2_PID_VAL )) then
					ld_pot(1) 	<= '1';
--					if( New_TEC1_Val( 7 + New_TEC1_Val_LSB downto New_TEC1_Val_LSB ) > Pot_Val( 15 downto 8 ))
					if( New_TEC1_Val ( 8 + New_TEC1_Val_LSB downto New_TEC1_Val_LSB ) > Pot_Val( 15 downto 8 ))
						then Local_TEC1_Val <= Pot_Val( 15 downto 8 );
						else Local_TEC1_Val <= New_TEC1_Val( 7 + New_TEC1_Val_LSB downto New_TEC1_Val_LSB );
					end if;					
				elsif( PA_State = St_Pot1 ) 
					then Ld_Pot(1)	<= '0';
				end if;												
			end if;
			-- TEC1 Max Current ----------------------------------------------------------------						
			
			TEC2_VAL_DEL			<= Pot_Val( 23 downto 16 );			
			TEC2_PID_VAL			<= PA_TEC2_CMD;			
			
			Force_Cool_Del <= HV_CWORD( CW_FORCE_COOL );
			
			-- Leading edge of Force_Cool - Load "Pot_Val"
			if(( HV_CWORD(CW_FORCE_COOL) = '1' ) and ( FORCE_COOL_DEL = '0' )) then
				ld_pot(2) 	<= '1';
				ld_pot2_val	<= Pot_Val( 23 downto 16 );
			elsif(( HV_CWORD( CW_FORCE_COOL ) = '1' ) and ( Pot_Val( 23 downto 16 ) /= TEC2_VAL_DEL )) then
				ld_pot(2) 	<= '1';
				ld_pot2_val	<= Pot_Val( 23 downto 16 );
		
			elsif(( HV_CWORD( CW_FORCE_COOL ) = '0' ) and ( PA_TEC2_CMD /= TEC2_PID_VAL )) then
				ld_pot(2) 	<= '1';
				ld_pot2_val 	<= PA_TEC2_CMD;				
			elsif( PA_State = St_Pot2 ) 
				then Ld_Pot(2)	<= '0';
			end if;
			-- TEC2 Max Current ----------------------------------------------------------------
						
			-- Time Base Geneator - Create a 20Mhz clock to the preamp -------------------------
			if( PA_State = St_Idle )
				then Clk_Cnt <= 0;
			elsif( Clk_Cnt_Tc = '1' )
				then Clk_Cnt <= 0;
				else Clk_Cnt <= Clk_Cnt + 1;
			end if;
			-- Time Base Geneator - Create a 20Mhz clock to the preamp -------------------------

			-- Look for the Start Conv signal for the LTC1867 A/D converter --------------------
			
			if( ms1_tc = '1' )
				then Ld_LTC1867 <= '1';
			elsif( PA_State <= St_LTC1867_Done2 )
				then Ld_LTC1867 <= '0';
			end if;
			-- Look for the Start Conv signal for the LTC1867 A/D converter --------------------
               
               -- When the "Last State of the A/D on the TEC Board is finished - Setup to read the Version Information
               if(( PA_State = St_LTC1867_Done2 ) and ( conv_integer( LTC1867_Adr )   = 7 ))               
                    then Ld_Tec_Ver_Info <= '1';
               elsif( PA_State = St_Ver_Done )
                    then Ld_Tec_Ver_Info <= '0';
               end if;
               -- When the "Last State of the A/D on the TEC Board is finished - Setup to read the Version Information

			-- Main State Machine to serialze the data to the preamp ---------------------------
			case PA_State is
				when ST_Idle =>
					o_SCLK				<= '0';
					o_nAEN				<= '1';
					o_nDEN				<= '1';
					o_SDIN				<= '0';
					o_SPARE				<= '0';
					Cnt 					<= 0;
					LTC1867_Creg( BIT_SD ) 	<= '1';	-- Single Ended
					LTC1867_CReg( BIT_OS )	<= LTC1867_Adr(0);
					LTC1867_CReg( BIT_S1 )	<= LTC1867_Adr(2);
					LTC1867_CReg( BIT_S0 )	<= LTC1867_Adr(1);
					LTC1867_CReg( BIT_COM)	<= '0';	-- Channel 7 is an input
					LTC1867_CReg( BIT_UNI)	<= '1';	-- Unipolar Mode
					LTC1867_CReg( BIT_SLP)	<= '0';	-- No Sleep Mode
                         
					
					if( ld_hven 	= '1' 				) then PA_State <= St_HvEn;                         
					elsif( LD_Pot(1)  	= '1' 			) then PA_State <= St_Pot1;
					elsif( LD_Pot(2)  	= '1' 			) then PA_State <= St_Pot2;
                         elsif( Ld_Tec_Ver_Info = '1'            ) then PA_State <= St_VerInfo;
					elsif( Ld_LTC1867 	= '1' 			) then PA_State <= St_LTC1867;
					end if;
                         
                    -- Setup to readback the Version Info from the TEC Control Board -------------
                    when ST_VerInfo =>
                         Cnt       <= 0;
                         Addr      <= conv_Std_logic_Vector( iADR_VER, 4 );
                         data      <= (others=>'0');
                         Cnt_Max   <= 31;
                         
                         if( Clk_Cnt_Tc = '1' )
                              then PA_State <= St_Addr;
                         end if;
                    -- Setup to readback the Version Info from the TEC Control Board -------------
                         
				-- HVEN State changed - so send it to the preamp ------------------------------
				when ST_HvEn =>
					Cnt 		<= 0;
					Addr		<= conv_std_logic_Vector( iADR_HVEN, 4 );
					data		<= ext( Hv_En, 16 );
					Cnt_Max	<= 15;
					if( Clk_Cnt_Tc = '1' ) 
						then PA_State <= St_Addr;
					end if;
				-- HVEN State changed - so send it to the preamp ------------------------------
				
				-- LTC1867 State changed - so send it to the preamp ------------------------------
				when St_LTC1867 =>
					Cnt 		          <= 0;
					Addr		          <= conv_std_logic_Vector( iADR_AD, 4 );					
					data		          <= ext( LTC1867_CReg, 16 );
					Cnt_Max	          <= 15;
                         LTC1867_Addr_Reg    <= LTC1867_CReg( BIT_S1 ) & LTC1867_CReg( BIT_S0 ) & LTC1867_CReg( BIT_OS );    -- Save the address 
                         
					if( CLk_Cnt_Tc = '1' )
						then PA_State <= St_Addr;
					end if;
				-- LTC1867 State changed - so send it to the preamp ------------------------------
				
				-- AD5290 Pot 1 ------------------------------------------------------------------
				-- TEC1 Max Current
				when St_Pot1 =>
					Cnt 		<= 0;
					Addr		<= conv_std_logic_Vector( iADR_POT1, 4 );					
					Cnt_Max	<= 7;
					for i in 0 to 7 loop
						-- data(i)	<= ld_pot1_val(7-i);
						data(i)	<= Local_TEC1_Val( 7 - i );
					end loop;
					if( CLk_Cnt_Tc = '1' )
						then PA_State <= St_Addr;
					end if;
				-- AD5290 Pot 1 ------------------------------------------------------------------

				-- AD5290 Pot 2 ------------------------------------------------------------------
				-- TEC2 Max Current
				when St_Pot2 =>
					Cnt 		<= 0;
					Addr		<= conv_std_logic_Vector( iADR_POT2, 4 );					
					Cnt_Max	<= 7;
					for i in 0 to 7 loop
						data(i)	<= ld_pot2_val(7-i);
					end loop;
					if( CLk_Cnt_Tc = '1' )
						then PA_State <= St_Addr;
					end if;
				-- AD5290 Pot 2 ------------------------------------------------------------------

				-- Output the 4-bit address to the preamp -------------------------------------
				when St_Addr =>
					o_nAEN			<= '0';
					o_nDEN			<= '1';
					o_SDIN			<= Addr(Cnt);
					LTC_1867_Conv_Cnt	<= 0;
					if( Clk_Cnt_Tc = '1' )
						then o_SCLK <= not( o_SCLK );
					end if;
					
					if(( Clk_Cnt_Tc = '1' ) and ( o_SCLK = '1' )) then
						if( Cnt = 3 ) then
							Cnt 		<= 0;
                                   case conv_integer( addr ) is
                                        when iADR_AD   => PA_State <= St_LTC1867_Conv1;
                                        when iADR_VER  => PA_State <= ST_Ver_Data_Conv;
                                        when others    => PA_State <= St_Data;
                                   end case;
						else
							Cnt <= Cnt + 1;
						end if;
					end if;
				-- Output the 4-bit address to the preamp -------------------------------------

				-- Output the 16-bit Data to the preamp ---------------------------------------
				when St_Data =>
					o_nAEN		<= '1';
					o_nDEN		<= '0';
					o_SDIN		<= Data(Cnt);
					if( Clk_Cnt_Tc = '1' )
						then o_SCLK <= not( o_SCLK );
					end if;
					
					if(( Clk_Cnt_Tc = '1' ) and ( o_SCLK = '1' )) then
						if( Cnt = Cnt_Max ) then
							Cnt 		<= 0;
							PA_State 	<= St_Done;
						else
							Cnt 		<= Cnt + 1;
						end if;
					end if;
				-- Output the 16-bit Data to the preamp ---------------------------------------
			
				when St_Done =>
					PA_State	<= St_Idle;
				
				-- THESE STATES CONTROL THE LTC1867 Octal A/D Converter -----------------------
				-- Output Convert strobe (4 us, on DEN line ) ---------------------------------
				when St_LTC1867_Conv1 =>
					o_nAEN		<= '1';
					o_nDEN		<= '0';
					o_SDIN		<= '0';
					Cnt			<= 0;
					
					if( Clk_Cnt_Tc = '1' ) then						
						if( LTC_1867_Conv_Cnt = LTC1867_Conv_Max )
							then PA_State 			<= St_LTC1867_Data1;
							else LTC_1867_Conv_Cnt 	<= LTC_1867_Conv_Cnt + 1;
						end if;
					end if;
				-- Output Convert strobe (4 us, on DEN line ) ---------------------------------
                                        
				-- Output Next Command Sequece while recieving input data ---------------------
				when St_LTC1867_Data1 =>
					o_nAEN		<= '1';
					o_nDEN		<= '1';
					o_SPARE		<= '0';

					-- Output Control Register - only 7 bits the rest are zero's -------------
					if( Cnt < 7 ) 
						then o_SDIN		<= LTC1867_CReg(Cnt);
						else o_SDIN		<= '0';
					end if;
					-- Output Control Register - only 7 bits the rest are zero's -------------

					-- Generate the Serial Clock ---------------------------------------------
					if( Clk_Cnt_Tc = '1' )
						then o_SCLK <= not( o_SCLK );
					end if;
					-- Generate the Serial Clock ---------------------------------------------
					
					-- At the falling edge of o_SCLK -----------------------------------------
					if(( Clk_Cnt_Tc = '1' ) and ( o_SCLK = '1' )) then
						Data( 15-cnt) <= i_SDOUT;					-- Latch the A/D output
						
						if( Cnt = 15 ) then
							Cnt 		<= 0;
							PA_State 	<= St_LTC1867_Done1;
						else
							Cnt <= Cnt + 1;
						end if;
					end if;							
					-- At the falling edge of o_SCLK -----------------------------------------
				-- Output Next Command Sequece while recieving input data ---------------------

                    -- Setup to Output another "Command Register" to start a 2nd conversion cycle of the same address
                    -- to force the output data for the "SAME" address
                    When St_LTC1867_Done1 =>
					Cnt 		          <= 0;
					Addr		          <= conv_std_logic_Vector( iADR_AD, 4 );					
					data		          <= ext( LTC1867_CReg, 16 );
					Cnt_Max	          <= 15;
                         LTC1867_Addr_Reg    <= LTC1867_CReg( BIT_S1 ) & LTC1867_CReg( BIT_S0 ) & LTC1867_CReg( BIT_OS );    -- Save the address 
                         
					if( CLk_Cnt_Tc = '1' )
						then PA_State <= St_LTC1867_Addr2;
					end if;

                   -- Send the address back to the TEC board (started conversion cycle)
                    when St_LTC1867_Addr2 =>
					o_nAEN			<= '0';
					o_nDEN			<= '1';
					o_SDIN			<= Addr(Cnt);
					LTC_1867_Conv_Cnt	<= 0;
					if( Clk_Cnt_Tc = '1' )
						then o_SCLK <= not( o_SCLK );
					end if;
					
					if(( Clk_Cnt_Tc = '1' ) and ( o_SCLK = '1' )) then
						if( Cnt = 3 ) then
							Cnt 		<= 0;
                                   PA_State <= St_LTC1867_Conv2;
						else
							Cnt <= Cnt + 1;
						end if;
					end if;
     
                    -- THESE STATES CONTROL THE LTC1867 Octal A/D Converter -----------------------
				-- Output Convert strobe (4 us, on DEN line ) ---------------------------------
				when St_LTC1867_Conv2 =>
					o_nAEN		<= '1';
					o_nDEN		<= '0';
					o_SDIN		<= '0';
					Cnt			<= 0;
					
					if( Clk_Cnt_Tc = '1' ) then						
						if( LTC_1867_Conv_Cnt = LTC1867_Conv_Max )
							then PA_State 			<= St_LTC1867_Data2;
							else LTC_1867_Conv_Cnt 	<= LTC_1867_Conv_Cnt + 1;
						end if;
					end if;
				-- Output Convert strobe (4 us, on DEN line ) ---------------------------------
                                        
				-- Output Next Command Sequece while recieving input data ---------------------
				when St_LTC1867_Data2 =>
					o_nAEN		<= '1';
					o_nDEN		<= '1';
					o_SPARE		<= '0';

					-- Output Control Register - only 7 bits the rest are zero's -------------
					if( Cnt < 7 ) 
						then o_SDIN		<= LTC1867_CReg(Cnt);
						else o_SDIN		<= '0';
					end if;
					-- Output Control Register - only 7 bits the rest are zero's -------------

					-- Generate the Serial Clock ---------------------------------------------
					if( Clk_Cnt_Tc = '1' )
						then o_SCLK <= not( o_SCLK );
					end if;
					-- Generate the Serial Clock ---------------------------------------------
					
					-- At the falling edge of o_SCLK -----------------------------------------
					if(( Clk_Cnt_Tc = '1' ) and ( o_SCLK = '1' )) then
						Data( 15-cnt) <= i_SDOUT;					-- Latch the A/D output
						
						if( Cnt = 15 ) then
							Cnt 		<= 0;
							PA_State 	<= St_LTC1867_Done2;
						else
							Cnt <= Cnt + 1;
						end if;
					end if;							
					-- At the falling edge of o_SCLK -----------------------------------------
				-- Output Next Command Sequece while recieving input data ---------------------

				-- All Input data has been recieved, so update approprite slide of output register
				-- Keep in mind, value of address is the next data point ( so it lags by one )
				When St_LTC1867_Done2 =>
					o_SCLK				<= '0';
					o_nAEN				<= '1';
					o_nDEN				<= '1';
					o_SDIN				<= '0';
					o_SPARE				<= '0';
					Cnt 					<= 0;
                         
                         if( conv_integer( LTC1867_Adr )   = 7 )
                              then LTC1867_Adr <= (others=>'0');
                              else LTC1867_Adr <= LTC1867_Adr + 1;
                         end if;

                         -- From the Address Register to the A/D Converter ( Bits 2:0 )
					case Conv_Integer( LTC1867_Addr_Reg) is                                                
						when 0 		=> LTC1867_Data(  15 downto   0 ) <= Data; 	-- Temperature Diode
						when 1 		=> LTC1867_Data(  31 downto  16 ) <= Data; 	-- +24V
						when 2 		=> LTC1867_Data(  47 downto  32 ) <= Data; 	-- TEC1 Voltage
						when 3 		=> LTC1867_Data(  63 downto  48 ) <= Data; 	-- TEC1 Current
						when 4 		=> LTC1867_Data(  79 downto  64 ) <= Data; 	-- TEC2 Voltage
						when 5 		=> LTC1867_Data(  95 downto  80 ) <= Data; 	-- TEC2 Current
						when 6 		=> LTC1867_Data( 111 downto  96 ) <= Data; 	-- +9V on Preamp
						when 7 		=> LTC1867_Data( 127 downto 112 ) <= Data; 	-- -9V on Preamp
						when others	=> 
					end case;						                         
					PA_State				<= St_Idle;					
				-- All Input data has been recieved, so update approprite slide of output register

                    -- Input the Version Data from the TEC Control Board -------------------------------------------
                    when St_Ver_Data_Conv =>
					o_nAEN		<= '1';        -- Inactive
					o_nDEN		<= '1';        -- Active
					o_SDIN		<= '0';        -- Don't Care                         
					Cnt			<= 0;
                         PA_State       <= St_Ver_Data;
                    
                    when St_Ver_Data =>
					o_nAEN		<= '1';        -- Inactive
					o_nDEN		<= '0';        -- Active
					o_SDIN		<= '0';        -- Don't Care                         
                        
					-- Generate the Serial Clock ---------------------------------------------
					if( Clk_Cnt_Tc = '1' )
						then o_SCLK <= not( o_SCLK );
					end if;
					-- Generate the Serial Clock ---------------------------------------------
                         					-- At the falling edge of o_SCLK -----------------------------------------
					if(( Clk_Cnt_Tc = '1' ) and ( o_SCLK = '1' )) then
						TEC_Ver_SReg(cnt) <= i_SDOUT;					-- Latch the A/D output
						
						if( Cnt = 31 ) then
							Cnt 		<= 0;
							PA_State 	<= ST_Ver_Done;
						else
							Cnt <= Cnt + 1;
						end if;
					end if;							
                    -- Input the Version Data from the TEC Control Board -------------------------------------------

                    -- Latch the Version Data from the TEC COntrol Board -------------------------------------------
                    when St_Ver_Done =>
                         o_nAEN         <= '1';   -- Inactive
                         o_nDEN         <= '1';   -- Inactive
                         o_SPARE        <= '0';   -- Inactive
                         TEC_Ver_Info   <= TEC_Ver_SReg;
                         PA_State       <= St_Idle;
                    -- Latch the Version Data from the TEC COntrol Board -------------------------------------------
                    
				when others =>
					o_SCLK		<= '0';
					o_nAEN		<= '1';
					o_nDEN		<= '1';
					o_SDIN		<= '0';
					o_SPARE		<= '0';
					PA_State		<= St_idle;
			end case;
			-- Main State Machine to serialze the data to the preamp ---------------------------
			
			HV_State_Code <= conv_std_logic_Vector( HV_State, 5 );
							
			-- Create Clock to Generate the HV Bias --------------------------------------------
			if( HV_En(4) = '1' ) then
				if( HV_Clk_Cnt >= HV_Clk_Cnt_Max ) then 
					HV_Clk_Cnt 	<= 0;
					HV_CLK 		<= not( HV_CLK );				
				else 
					HV_Clk_Cnt <= HV_Clk_Cnt + 1;
				end if;
			else
				HV_Clk_Cnt 	<= 0;
				HV_Clk		<= '0';
			end if;
			-- Create Clock to Generate the HV Bias --------------------------------------------

			-- Create a Timer to Flash the LEDs ------------------------------------------------
			if( ms1_tc = '1' ) then
				if( Flash_Cnt = Flash_Cnt_Max ) then
					Flash 	<= not( flash);
					Flash_Cnt <= 0;
				else
					Flash_Cnt <= Flash_Cnt + 1;
				end if;
			end if;
			-- Create a Timer to Flash the LEDs ------------------------------------------------
			
               -- If Watch-Dog Trigger occurs latch it until enters watch-dog state --------------
               if( Det_Sat = '1' )
				then Cmd_WDog_Latch <= '0';
			elsif( CMD_WD = '1' )
                    then Cmd_WDog_Latch <= '1';
               elsif(( HV_State = ST_WDog ) or ( HV_State = ST_Off ) or ( HV_STate = ST_Error ) or ( HV_State = St_Det_Sat ))
                    then Cmd_WDog_Latch <= '0';
               end if;
               -- If Watch-Dog Trigger occurs latch it until enters watch-dog state --------------
               
               if( HV_CWORD( CW_ONLINE ) = '0' )
                    then Standby <= '1';
                    else Standby <= '0';
               end if;			
				
			if( HV_State = St_Det_Sat ) then
				if(( ms1_tc = '1' ) and ( Sat_Cnt < Sat_Cnt_Max ))
					then Sat_Cnt <= Sat_Cnt + 1;
				end if;
			else
				Sat_Cnt				<= 0;				
			end if;
			
    
			-------------------------------------------------------------------------------
			-- HV State Machine
			case HV_State is
				-------------------------------------------------------------------------------
				-- Everything off
				when St_Off =>
					HV_En 		<= (others=>'0');
					Delay_Cnt 	<= (others=>'0');
					--------------------------------------------------------------------------
					-- Status LED1
					-- When OK to Vent - LED will be RED (Steady )
					if( En_vent = '1' )
						then Status_LED1	<= LED_Red;
					-- Not Ok to Vent (ie warming up ) Flash RED
					elsif( Flash = '1' ) 
						then Status_LED1 	<= LED_Red;
						else Status_LED1	<= LED_Off;
					end if;															
					-- Status LED1
					--------------------------------------------------------------------------
								
					if( HV_CWORD( CW_MANUAL ) = '1' )
						then HV_State <= St_Manual;
						
					elsif(( HV_CWORD( CW_FORCE_HV ) = '1' ) and ( HV_CWORD( CW_FORCE_COOL ) = '1' ))
						then HV_State <= St_Error;
						
					elsif(( HV_CWORD( CW_FORCE_LV ) = '1' ) and ( HV_CWORD( CW_FORCE_COOL ) = '1' ))
						then HV_State <= St_Error;	
						
					elsif(( HV_CWORD( CW_FORCE_LV ) = '1' ) and ( HV_CWORD( CW_FORCE_HV ) = '1' ))
						then HV_State <= St_Error;	
										
					elsif( HV_CWORD( CW_FORCE_HV ) = '1' )
						then HV_State <= St_Force_HV;
						
					elsif( HV_CWORD( CW_FORCE_LV ) = '1' )
						then HV_State <= St_Force_LV;	

					elsif( HV_CWORD( CW_FORCE_COOL ) = '1' )
						then HV_State <= St_Force_Cool;
																	
					elsif( Ext_Enable = '0' )
						then HV_State <= St_No_Ext_En;
						
					elsif(( HV_CWORD( CW_ONLINE ) = '1' ) and ( Temp_Min_Err_Cmp = '1' ))
						then HV_State <= St_Error;				
						
					elsif(( HV_CWORD( CW_ONLINE ) = '1' ) and ( Temp_Max_Err_Cmp = '1' ))
						then HV_State <= St_Error;
									
					elsif(( HV_CWORD( CW_ONLINE ) = '1' ) and ( Temp_Min_Err_Cmp = '0' ) and ( Temp_Max_Err_Cmp = '0' ))
						then HV_State 	<= St_Full_TEC1_Delay; 
					end if;
				-------------------------------------------------------------------------------

				--/////////////////////////////////////////////////////////////////////////////
				--///// SEQUENCE to TURN ON THE TECS 
				--/////////////////////////////////////////////////////////////////////////////				
								
				-------------------------------------------------------------------------------
				-- Wait for TEC to be at full power for some time before bring TEC1 up to full power
				when St_Full_TEC1_Delay =>
					HV_En( kHV_TEC1 ) 		<= '1';
					HV_En( kHV_TEC2 ) 		<= '0';
					--------------------------------------------------------------------------
					-- Status LED1 - Blink YEL until Ring Voltage go on
					if( Flash = '1' ) 
						then Status_LED1	<= LED_YEL;
						else Status_LED1	<= LED_Off;
					end if;
					-- Status LED1
					--------------------------------------------------------------------------

					-- No External Enable - Go to No Ext Enable State					
					if(( Ext_Enable = '0' ) or ( HV_CWord( CW_ONLINE ) = '0' ))
						then HV_State <= St_Off_TEC2_Start;
					
					elsif( En_HV = '1' )
						then HV_State <= St_Full_TEC2_Delay;
						
					elsif( ms1_tc = '1' ) then
						if( Delay_Cnt = TEC2_DELAY ) then
							Delay_Cnt 	<= (others=>'0');
							HV_State 		<= St_Full_TEC2_Delay;
						else
							Delay_Cnt 	<= Delay_Cnt + 1;
						end if;
					end if;		
				-------------------------------------------------------------------------------

				-------------------------------------------------------------------------------
				-- Wait for TEC to be at full power for some time before bring TEC1 up to full power
				when St_Full_TEC2_Delay =>
					HV_En( kHV_TEC1 	) <= '1';
					HV_En( kHV_TEC2 	) <= '1';
					--------------------------------------------------------------------------
					-- Status LED1 - Blink YEL until Ring Voltage go on
					if( Flash = '1' ) 
						then Status_LED1	<= LED_YEL;
						else Status_LED1	<= LED_Off;
					end if;
					-- Status LED1
					--------------------------------------------------------------------------					

					-- No External Enable - Go to No Ext Enable State					
					if(( Ext_Enable = '0' ) or ( HV_CWord( CW_ONLINE ) = '0' ))
						then HV_State <= St_Off_TEC2_Start;
						
					elsif( En_HV = '1' ) then 	-- Reached Temp (-15C) Start turning on Ring Voltages
						Delay_Cnt <= (others=>'0');
						HV_State 	<= St_R1;
												
					elsif( ms1_tc = '1' ) then
						if( conv_integer( Delay_Cnt ) = NO_TEMP_TIMEOUT ) then
							Delay_Cnt 	<= (others=>'0');
							HV_State 		<= St_Error; 
						else
							Delay_Cnt 	<= Delay_Cnt + 1;
						end if;
					end if;		
				-- Wait for TEC to be at full power for some time before bring TEC1 up to full power
				-------------------------------------------------------------------------------

				--/////////////////////////////////////////////////////////////////////////////
				--///// SEQUENCE to TURN ON THE TECS 
				--/////////////////////////////////////////////////////////////////////////////


				--/////////////////////////////////////////////////////////////////////////////
				--///// SEQUENCE to TURN ON THE Ring Voltages
				--/////////////////////////////////////////////////////////////////////////////
				-------------------------------------------------------------------------------
				when St_R1 =>
					HV_En( kHV_TEC1 	) 	<= '1';
					HV_En( kHV_TEC2 	) 	<= '1';
					HV_En( kHV_R1		)	<= '1';						-- Turn on R1

					--------------------------------------------------------------------------
					-- Status LED1 - Yellow - HV is ON
					if( Flash = '1' ) 
						then Status_LED1	<= LED_YEL;
						else Status_LED1	<= LED_Off;
					end if;
					-- Status LED1 - Yellow - HV is ON
					--------------------------------------------------------------------------					

					-- No External Enable - Go to No Ext Enable State					
					if(( Ext_Enable = '0' ) or ( HV_CWord( CW_ONLINE ) = '0' ))
						then HV_State <= St_Off_TEC2_Start;
						
					elsif( ms1_tc = '1' ) then
						if( conv_integer( Delay_Cnt ) = Delay_R1 ) then  -- wait for TEC2 delay before turning on TEC1
							Delay_Cnt 	<= (others=>'0');
							HV_State 		<= St_IGR;
						else
							Delay_Cnt 	<= Delay_Cnt + 1;
						end if;
					end if;
				-------------------------------------------------------------------------------

				-------------------------------------------------------------------------------
				when St_IGR =>
					HV_En( kHV_TEC1 	) 	<= '1';
					HV_En( kHV_TEC2 	) 	<= '1';
					HV_En( kHV_R1		)	<= '1';						-- Turn on R1
					HV_En( kHV_IGR 	)	<= '1';						-- Turn on IGR

					--------------------------------------------------------------------------
					-- Status LED1 - Yellow - HV is ON
					if( Flash = '1' ) 
						then Status_LED1	<= LED_YEL;
						else Status_LED1	<= LED_Off;
					end if;
					-- Status LED1 - Yellow - HV is ON
					--------------------------------------------------------------------------
					-- No External Enable - Go to No Ext Enable State					
					if(( Ext_Enable = '0' ) or ( HV_CWord( CW_ONLINE ) = '0' ))
						then HV_State <= St_Off_TEC2_Start;
						
					elsif( ms1_tc = '1' ) then
						if( conv_integer( Delay_Cnt ) = Delay_IGR ) then  -- wait for TEC2 delay before turning on TEC1
							Delay_Cnt 	<= (others=>'0');
							HV_State 		<= ST_BCBRBX;
						else
							Delay_Cnt 	<= Delay_Cnt + 1;
						end if;
					end if;
				-------------------------------------------------------------------------------

				-------------------------------------------------------------------------------
				when St_BCBRBX =>
					HV_En( kHV_TEC1 	) 	<= '1';
					HV_En( kHV_TEC2 	) 	<= '1';
					HV_En( kHV_R1		)	<= '1';						-- Turn on R1
					HV_En( kHV_IGR 	)	<= '1';						-- Turn on IGR
					HV_En( kHV_BCBRRX	)  	<= '1';						-- Turn on BC/BR/RX

					--------------------------------------------------------------------------
					-- Status LED1 - Yellow - HV is ON
					if( Flash = '1' ) 
						then Status_LED1	<= LED_YEL;
						else Status_LED1	<= LED_Off;
					end if;
					-- Status LED1 - Yellow - HV is ON
					--------------------------------------------------------------------------

					-- No External Enable - Go to No Ext Enable State					
					if(( Ext_Enable = '0' ) or ( HV_CWord( CW_ONLINE ) = '0' ))
						then HV_State <= St_Off_TEC2_Start;
						
					elsif( ms1_tc = '1' ) then
						if( conv_integer( Delay_Cnt ) = Delay_BCBRBX ) then  -- wait for TEC2 delay before turning on TEC1
							Delay_Cnt 	<= (others=>'0');
							HV_State 		<= St_EnReset ;
						else
							Delay_Cnt 	<= Delay_Cnt + 1;
						end if;
					end if;
				-------------------------------------------------------------------------------

				
				-------------------------------------------------------------------------------
				when St_EnReset =>
					HV_En( kHV_TEC1 	) <= '1';
					HV_En( kHV_TEC2 	) <= '1';
					HV_En( kHV_R1		) <= '1';						-- Turn on R1
					HV_En( kHV_IGR 	) <= '1';						-- Turn on IGR
					HV_En( kHV_BCBRRX	) <= '1';						-- Turn on BC/BR/RX
					HV_En( kHV_EN_RST 	) <= '1';						-- Enable Reset

					--------------------------------------------------------------------------
					-- Status LED1 - Yellow - HV is ON
					if( Flash = '1' ) 
						then Status_LED1	<= LED_YEL;
						else Status_LED1	<= LED_Off;
					end if;
					-- Status LED1 - Yellow - HV is ON
					--------------------------------------------------------------------------

					-- No External Enable - Go to No Ext Enable State					
					if(( Ext_Enable = '0' ) or ( HV_CWord( CW_ONLINE ) = '0' ))
						then HV_State <= St_Off_TEC2_Start;
						
					elsif( ms1_tc = '1' ) then
						if( conv_integer( Delay_Cnt ) = Delay_EnReset ) then  -- wait for TEC2 delay before turning on TEC1
							Delay_Cnt <= (others=>'0');
							HV_State 	<= St_Last ;
						else
							Delay_Cnt <= Delay_Cnt + 1;
						end if;
					end if;
				-------------------------------------------------------------------------------

				-------------------------------------------------------------------------------
				when St_Last =>
					HV_En( kHV_TEC1 	) <= '1';
					HV_En( kHV_TEC2 	) <= '1';
					HV_En( kHV_R1		) <= '1';						-- Turn on R1
					HV_En( kHV_IGR 	) <= '1';						-- Turn on IGR
					HV_En( kHV_BCBRRX	) <= '1';						-- Turn on BC/BR/RX
					HV_En( kHV_EN_RST 	) <= '1';						-- Enable Reset
					HV_En( kHV_EN_6	) <= '1';

					--------------------------------------------------------------------------
					-- Status LED1 - Yellow - HV is ON
					if( Flash = '1' ) 
						then Status_LED1	<= LED_YEL;
						else Status_LED1	<= LED_Off;
					end if;
					-- Status LED1 - Yellow - HV is ON
					--------------------------------------------------------------------------

					-- No External Enable - Go to No Ext Enable State					
					if(( Ext_Enable = '0' ) or ( HV_CWord( CW_ONLINE ) = '0' ))
						then HV_State <= St_Off_TEC2_Start;
						
					elsif( ms1_tc = '1' ) then
						if( conv_integer( Delay_Cnt ) = Delay_Last ) then  -- wait for TEC2 delay before turning on TEC1
							Delay_Cnt 	<= (others=>'0');
							HV_State 		<= St_On ;
						else
							Delay_Cnt 	<= Delay_Cnt + 1;
						end if;
					end if;
				-------------------------------------------------------------------------------

				-------------------------------------------------------------------------------
				when St_On =>
					HV_En( kHV_TEC1 	) <= '1';
					HV_En( kHV_TEC2 	) <= '1';
					HV_En( kHV_R1		) <= '1';						-- Turn on R1
					HV_En( kHV_IGR 	) <= '1';						-- Turn on IGR
					HV_En( kHV_BCBRRX	) <= '1';						-- Turn on BC/BR/RX
					HV_En( kHV_EN_RST 	) <= '1';						-- Enable Reset
					HV_En( kHV_EN_6	) <= '1';
					
					--------------------------------------------------------------------------
					-- Status LED1 
					if( En_HV = '0' ) and ( Flash = '1' )
						then Status_LED1 <= LED_YEL;
					elsif(( Det_Ready = '0' )  and ( Flash = '1' ))
						then Status_LED1 <= LED_YEL;
					elsif(( Det_Ready = '1' ) and ( En_Almost_Ready = '0' ) and ( En_Ready = '0' ))
						then Status_LED1 <= LED_YEL;
					elsif(( En_Almost_Ready = '1' ) and ( En_Ready = '0' ) and ( Flash = '1' ))
						then Status_LED1 <= LED_GRN;
					elsif(( En_Almost_Ready = '1' ) and ( En_Ready = '1' ))
						then Status_LED1 <= LED_GRN;
						else Status_LED1 <= LED_Off;
					end if;
					-- Status LED1
					--------------------------------------------------------------------------										
					
					-- No External Enable - Go to No Ext Enable State					
					if(( Ext_Enable = '0' ) or ( HV_CWord( CW_ONLINE ) = '0' ))
						then HV_State <= St_Off_TEC2_Start;
						
					elsif( Det_Sat = '1' ) 
						then HV_State <= St_Det_Sat;		
						
					elsif( CMD_WDog_Latch = '1' )           -- Watch-Dog Timer Fired
                              then HV_State <= St_Off_TEC2_Start;                             

					end if;					
				--/////////////////////////////////////////////////////////////////////////////
				--///// SEQUENCE to TURN ON THE Ring Voltages				
				--/////////////////////////////////////////////////////////////////////////////
				
				when St_Off_TEC2_Start =>
					HV_En( kHV_TEC1 	) 	<= '0';
					HV_En( kHV_TEC2 	) 	<= '1';
					HV_En( kHV_R1		) 	<= '0';	-- Turn off R1
					HV_En( kHV_IGR		) 	<= '0';	-- Turn off IGR
					HV_En( kHV_BCBRRX	) 	<= '0';	-- Turn off BCBRRX
					HV_En( kHV_EN_RST	) 	<= '0';	-- Turn off Reset
					HV_En( kHV_EN_6	) 	<= '0';	-- Turn Off "En_6"								
					Delay_Cnt 			<= (others=>'0');
					
					if( En_HV = '1' )
						then HV_State <= St_Off_TEC2_Delay;
						else HV_State <= St_OFf;
					end if;
					
				when St_Off_TEC2_DELAY =>
					HV_En( kHV_TEC1 	) <= '0';
					HV_En( kHV_TEC2 	) <= '1';
					HV_En( kHV_R1		) <= '0';	-- Turn off R1
					HV_En( kHV_IGR		) <= '0';	-- Turn off IGR
					HV_En( kHV_BCBRRX	) <= '0';	-- Turn off BCBRRX
					HV_En( kHV_EN_RST	) <= '0';	-- Turn off Reset
					HV_En( kHV_EN_6	) <= '0';	-- Turn Off "En_6"					
					-- Status LED1 - Blink YEL until Ring Voltage go on
					if( Flash = '1' ) 
						then Status_LED1	<= LED_RED;
						else Status_LED1	<= LED_Off;
					end if;
					-- Status LED1
					--------------------------------------------------------------------------			
												
					if(( Ext_Enable = '1' ) and ( HV_CWord( CW_ONLINE ) = '1' )) then
						Delay_Cnt <= (others=>'0');
						HV_State 	<= St_Full_TEC1_Delay;
					
					elsif( ms1_tc = '1' ) then
						if( Delay_Cnt = TEC2_DELAY ) then
							Delay_Cnt 	<= (others=>'0');
							if( Ext_Enable = '0' )
								then HV_State <= St_No_Ext_En;		
								
							elsif( CMD_WDog_Latch = '1' )           -- Watch-Dog Timer Fired
								then HV_State <= St_WDog;                             								
							else HV_State 	    <= St_off;
							end if;
						else
							Delay_Cnt 	<= Delay_Cnt + 1;
						end if;
					end if;						
				-------------------------------------------------------------------------------
				-- Detector Saturated				
				when St_Det_Sat =>
					HV_En( kHV_TEC1 	) <= '1';
					HV_En( kHV_TEC2 	) <= '1';
					HV_En( kHV_R1		) <= '1';	-- Leave R1 On
					HV_En( kHV_IGR		) <= '0';	-- Turn off IGR
					HV_En( kHV_BCBRRX	) <= '0';	-- Turn off BCBRRX
					HV_En( kHV_EN_RST	) <= '0';	-- Turn off Reset
					HV_En( kHV_EN_6	) <= '0';	-- Turn Off "En_6"

					--------------------------------------------------------------------------
					-- Status LED1 
					if( En_HV = '0' ) and ( Flash = '1' )
						then Status_LED1 <= LED_YEL;
					elsif(( Det_Ready = '0' )  and ( Flash = '1' ))
						then Status_LED1 <= LED_YEL;
					elsif(( Det_Ready = '1' ) and ( En_Almost_Ready = '0' ) and ( En_Ready = '0' ))
						then Status_LED1 <= LED_YEL;
					elsif(( En_Almost_Ready = '1' ) and ( En_Ready = '0' ) and ( Flash = '1' ))
						then Status_LED1 <= LED_GRN;
					elsif(( En_Almost_Ready = '1' ) and ( En_Ready = '1' ))
						then Status_LED1 <= LED_GRN;
						else Status_LED1 <= LED_Off;
					end if;
					-- Status LED1
					--------------------------------------------------------------------------
					
					-- wait here for 10 seconds then 
					if(( Ext_Enable = '0' ) or ( HV_CWord( CW_ONLINE ) = '0' ))
						then HV_State <= St_Off_TEC2_Start;
					elsif(( ms1_tc = '1' ) and ( Sat_Cnt = Sat_Cnt_Max ))
						then HV_State 	<= St_R1;
					end if;
				
				-------------------------------------------------------------------------------

				-------------------------------------------------------------------------------
				-- Force Cooling only ( enables only the 2 LSBs )
				when St_Force_Cool =>			
					HV_En( kHV_R1		) <= '0';						-- Turn on R1
					HV_En( kHV_IGR 	) <= '0';						-- Turn on IGR
					HV_En( kHV_BCBRRX	) <= '0';						-- Turn on BC/BR/RX
					HV_En( kHV_EN_RST 	) <= '0';						-- Enable Reset
					HV_En( kHV_EN_6	) <= '0';
				
					if( Flash = '1' ) 
						then Status_LED1 	<= LED_YEL;
						else Status_LED1	<= LED_Off;
					end if;

					if( HV_CWORD( CW_TEC1_DISABLE ) = '1' )
						then HV_En( kHV_TEC1 ) <= '0';
						else HV_En( kHV_TEC1 ) <= '1';
					end if;

					if( HV_CWORD( CW_TEC2_DISABLE ) = '1' )
						then HV_En( kHV_TEC2 ) <= '0';
						else HV_En( kHV_TEC2 ) <= '1';
					end if;
					
					if( HV_CWORD( CW_FORCE_COOL ) = '0' )
						then HV_State <= St_Off_TEC2_Start;

					elsif( CMD_WDog_Latch = '1' )           -- Watch-Dog Timer Fired
                              then HV_State <= St_Off_TEC2_Start;                             
					
					elsif( HV_CWORD( CW_FORCE_HV ) = '1' )
						then HV_State <= St_Error;
						
					elsif( HV_CWORD( CW_FORCE_LV ) = '1' )
						then HV_State <= St_Error;	
						
					end if;
				-- Force Cooling only ( enables only the 2 LSBs )
				-------------------------------------------------------------------------------

				-------------------------------------------------------------------------------
				-- Force HV Bias  ( Enables bits 2 to 5 )
				when St_Force_HV =>
					HV_En( kHV_TEC1 	) <= '0';
					HV_En( kHV_TEC2 	) <= '0';
					HV_En( kHV_R1		) <= '1';						-- Turn on R1
					HV_En( kHV_IGR 	) <= '1';						-- Turn on IGR
					HV_En( kHV_BCBRRX	) <= '1';						-- Turn on BC/BR/RX
					HV_En( kHV_EN_RST 	) <= '1';						-- Enable Reset
					HV_En( kHV_EN_6	) <= '1';
				
				
					Delay_Cnt 	<= (others=>'0');
					if( Flash = '1' ) 
						then Status_LED1 	<= LED_YEL;
						else Status_LED1	<= LED_Off;
					end if;
					
					if( HV_CWORD( CW_FORCE_HV ) = '0' )
						then HV_State <= St_off;
										
                elsif( CMD_WDog_Latch = '1' )           -- Watch-Dog Timer Fired
						then HV_State <= St_WDog;                             
					
					elsif( HV_CWORD( CW_FORCE_COOL ) = '1' )
						then HV_State <= St_Error;
						
					elsif( HV_CWORD( CW_FORCE_LV ) = '1' )
						then HV_State <= St_Error;
						
					end if;
				-- Force Cooling only ( enables only the 2 LSBs )
				-------------------------------------------------------------------------------	

-------------------------------------------------------------------------------
				-- Force LV Bias  
				when St_Force_LV =>
					HV_En( kHV_TEC1 	) <= '0';
					HV_En( kHV_TEC2 	) <= '0';
					HV_En( kHV_R1		) <= '1';						-- Turn on R1
					HV_En( kHV_IGR 	) <= '1';						-- Turn on IGR
					HV_En( kHV_BCBRRX	) <= '0';						
					HV_En( kHV_EN_RST 	) <= '0';						
					HV_En( kHV_EN_6	) <= '0';
					Delay_Cnt 	<= (others=>'0');
	
					if( Flash = '1' ) 
						then Status_LED1 	<= LED_YEL;
						else Status_LED1	<= LED_Off;
					end if;
					
					if( HV_CWORD( CW_FORCE_LV ) = '0' )
						then HV_State <= St_off;
										
                elsif( CMD_WDog_Latch = '1' )           -- Watch-Dog Timer Fired
						then HV_State <= St_WDog;                             
					
					elsif( HV_CWORD( CW_FORCE_COOL ) = '1' )
						then HV_State <= St_Error;
						
					elsif( HV_CWORD( CW_FORCE_HV ) = '1' )
						then HV_State <= St_Error;
						
					end if;
				-- Force Cooling only ( enables only the 2 LSBs )
				-------------------------------------------------------------------------------		
			

				-------------------------------------------------------------------------------
				-- Allows manually enabling differnet voltages
				when St_Manual =>					
					if( Flash = '1' ) 
						then Status_LED1 	<= LED_YEL;
						else Status_LED1	<= LED_Off;
					end if;

					if( HV_CWORD( CW_MANUAL ) = '0' ) 
                              then HV_State <= St_Idle;
                              
                         elsif( CMD_WDog_Latch = '1' )           -- Watch-Dog Timer Fired
                              then HV_State <= St_WDog;                             
                              else HV_En	<= HV_CWORD( 7 downto 0 );
					end if;

				--/////////////////////////////////////////////////////////////////////////////
				--///// Error States
				--/////////////////////////////////////////////////////////////////////////////
						
				-------------------------------------------------------------------------------
				when St_No_Ext_En =>
					HV_En 	<= (others=>'0');
					if( Flash = '1' ) 
						then Status_LED1 	<= LED_RED;
						else Status_LED1	<= LED_Off;
					end if;

					Delay_Cnt 	<= (others=>'0');
					if( Ext_Enable = '1' )
						then HV_State <= St_Off;
					end if;
				-------------------------------------------------------------------------------
					
				-------------------------------------------------------------------------------
				-- Error State
				when St_Error =>
					if( Flash = '1' ) 
						then Status_LED1 	<= LED_RED;
						else Status_LED1	<= LED_Off;
					end if;
					HV_En 	<= (others=>'0');					
									
					if(( HV_CWORD( CW_ONLINE ) = '0' ) and ( HV_CWORD( CW_FORCE_HV ) = '0' ) and ( HV_CWORD( CW_FORCE_COOL ) = '0' ) and ( HV_CWORD( CW_FORCE_LV ) = '0' ))
						then HV_State <= St_Off;					
						else HV_State <= St_Error;
					end if;
				-------------------------------------------------------------------------------
                    
				-------------------------------------------------------------------------------
                    when St_WDog =>
					if( Flash = '1' ) 
						then Status_LED1 	<= LED_RED;
						else Status_LED1	<= LED_Off;
					end if;
                         HV_En     <= (others=>'0');
                         if( HV_CWord( CW_ONLINE) = '0' )
                              then HV_State <= St_Off;
                         end if;
				-------------------------------------------------------------------------------
                    
				--/////////////////////////////////////////////////////////////////////////////
				--///// Error States
				--/////////////////////////////////////////////////////////////////////////////
                    
				
				when others =>
					if( Flash = '1' ) 
						then Status_LED1 	<= LED_RED;
						else Status_LED1	<= LED_Off;
					end if;
					HV_En 		<= (others=>'0');
					HV_State 		<= St_Off;
			end case;			
			-- HV State Machine
			-------------------------------------------------------------------------------
		end if;
	end process;
---------------------------------------------------------------------------------------------------
end behavioral;			-- PreampCtrl.vhd
---------------------------------------------------------------------------------------------------

