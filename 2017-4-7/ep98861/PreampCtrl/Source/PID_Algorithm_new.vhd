---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	PI(D__Algorithm.VHD 
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : July 28, 2010
--   Board          : Artemis DPP Board
--   Schematic      : 4035.007.25500
--
--   Top Level      : 4035.009.98860, S/W, XScope, Instrument Control
--
--   Description:
--		Module implements a PID (no D) algorithm for temperature control
--	
--		Temp_Error is the error from the Current Temp and the Set Point
--
--Target Position is DP (Desired Position)
--Position is PS
--Position Error = PE
--
--PE = DP - PS
--VC = PE*KP + (DP-DPlast)*65536;
--
--	History
--		090205: Created
--		6/21/13: When detector is cooled then DTM is turned off and the detector is cooled; TEC1 does not turn on
--					Changed logic around: if(( Temp_Enable = '0' ) and (Delay_Cnt > TEC2_DELAY)) to fix the TEC1 bug (DTM is turned off).    
---
---------------------------------------------------------------------------------------------------

library LPM;
     use lpm.lpm_components.all;
     
library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

---------------------------------------------------------------------------------------------------
entity PID_Algorithm is 
	port( 
		CLK50      	 	: in      std_logic;                         	-- Master Clock
		Temp_Clk			: in		std_logic;
		Temp_Enable		: in		std_logic;
		Temp_Error		: in		std_logic_vector( 31 downto 0 );
		KIKP				: in		std_Logic_Vector( 31 downto 0 );		-- Postion loop Gain 
		Cmd_Max			: in		std_logic_vector(  7 downto 0 );
--		KD				: in		std_logic_Vector( 31 downto 0 );		-- Velocity Loop Overall Gain

		TEC2_DELAY				: in		std_logic_vector( 31 downto 0 );
		Delay_Cnt				:in  std_logic_Vector( 31 downto 0 );


		Cmd_Out			: buffer	std_logic_Vector(  7 downto 0 ));		-- Position Error
		
end PID_Algorithm;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture Behavioral of PID_Algorithm is
	-- Constant Declratrions ---------------------------------------------------------------------
--	constant LSB					: integer := 16+4;			-- Of Accumulator to CMD Out
	constant LSB					: integer := 16+4+4;		-- Of Accumulator to CMD Out
	-- Constant Declratrions ---------------------------------------------------------------------

	-- Signal Declratrions -----------------------------------------------------------------------
	signal Clamp_Lo				: std_logic;
	signal Clamp_Hi				: std_logic;

	signal KP_PE					: std_logic_Vector( 63 downto 0 );
	signal KI_PE_ACC				: std_logic_Vector( 63 downto 0 );
	signal KPKI_OUT				: std_logic_Vector( 63 downto 0 );

	signal PE_ACC 					: std_logic_Vector( 31 downto 0 );				
	signal PE_Acc_Sum				: std_logic_Vector( 31 downto 0 );						
	signal PE_Acc_Sum_over			: std_logic;
	signal PE_Acc_Sum_Cout			: std_logic;
	signal start_count				: integer range 0 to 1;
	
	-- Signal Declratrions -----------------------------------------------------------------------
	
begin
	----------------------------------------------------------------------------------------------
	Mult_Inst0 : lpm_mult
		generic map(
			LPM_WIDTHA		=> 32,
			LPM_WIDTHB		=> 32,
			LPM_WIDTHS		=> 64,
			LPM_WIDTHP		=> 64,
			LPM_REPRESENTATION	=> "SIGNED",			
			LPM_HINT			=> "MAXIMIZE_SPEED=5",
			LPM_PIPELINE		=> 1 )
		port map(
			clock			=> CLK50,
			dataa			=> ext( KIKP( 15 downto 0 ), 32 ),
			datab			=> Temp_Error,
			sum				=> (others=>'0'),
			result			=> KP_PE );				
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Integral Portion
	-- Position Error Accumulator used for the Integral portion of the PID -----------------------
	PE_Adder_Inst : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 32,
			LPM_DIRECTION		=> "ADD",
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			cin				=> '0',
			dataa			=> PE_Acc,
			datab			=> sxt( Temp_Error( 31 downto 8 ), 32 ),			-- Use the "Integer" portion of the Temperature Error
			result			=> PE_Acc_Sum,
			overflow			=> PE_Acc_Sum_Over,
			Cout				=> PE_Acc_Sum_Cout );
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	PE_Acc_Clock_Proc : process( clk50 )
	begin
		if( rising_edge( clk50 )) then
			if( Temp_Enable = '0' ) then
				PE_Acc 	<= (others=>'0');
			elsif( Temp_Clk = '1' ) then
					
				-- Normal Movement 				
				if( PE_Acc_Sum_Over = '0' ) then
					-- Decrease Pe_Acc
					if( Pe_Acc_Sum <= Pe_Acc )
						then PE_Acc <= PE_Acc_Sum;
					-- Increase Pe_Acc but not clamped
					elsif( Clamp_Hi = '0' )
						then PE_Acc <= PE_Acc_Sum;
					end if;
			
				-- Accumlator at Max so clamp it positive
				elsif(( PE_Acc_Sum_Over = '1' ) and ( PE_Acc_Sum_Cout = '0' ))
					then PE_Acc <= not( '1' & conv_std_logic_Vector( 0, 31 ));

				-- Accumlator at Min so clamp it negative
				elsif(( PE_Acc_Sum_Over = '1' ) and ( PE_Acc_Sum_Cout = '1' ))
					then PE_Acc <= '1' & conv_std_logic_Vector( 0, 31 );
				end if;
				
			end if;
		end if;
	end process;
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	Mult_Inst1 : lpm_mult
		generic map(
			LPM_WIDTHA		=> 32,
			LPM_WIDTHB		=> 32,
			LPM_WIDTHS		=> 64,
			LPM_WIDTHP		=> 64,
			LPM_REPRESENTATION	=> "SIGNED",			
			LPM_HINT			=> "MAXIMIZE_SPEED=5",
			LPM_PIPELINE		=> 1 )
		port map(
			clock			=> CLK50,
			dataa			=> ext( KIKP( 31 downto 16 ), 32 ),
			datab			=> sxt( PE_Acc, 32 ),
			sum				=> (others=>'0'),
			result			=> KI_PE_ACC );				
	----------------------------------------------------------------------------------------------
	
	----------------------------------------------------------------------------------------------
	-- Multiplier Accumulator Adder
	-- Proportional & Deravitive Adder ----------------------------------------------------------
	Mult_Acc_Add_Inst : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 64,
			LPM_REPRESENTATION 	=> "SIGNED",
			LPM_DIRECTION		=> "ADD" )
		port map(
			cin				=> '0',
			dataa			=> KP_PE,	
			datab			=> KI_PE_ACC,		
			result			=> KPKI_OUT );
	----------------------------------------------------------------------------------------------
			
	----------------------------------------------------------------------------------------------
	-- Check the final output for Over/Under of Max Voltage to Motor -----------------------------
	KPKDKI_SUM_Clamp_hi_Compare : lpm_compare
		generic map(
			LPM_WIDTH			=> 64-LSB,
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			dataa			=> KPKI_OUT( 63 downto LSB ),
			datab			=> ext( Cmd_Max, 64-LSB ),
			ageb				=> Clamp_Hi );

	KPKDKI_SUM_Clamp_Lo_Compare : lpm_compare
		generic map(
			LPM_WIDTH			=> 64,
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			dataa			=> KPKI_OUT,
			datab			=> (others=>'0'),
			aleb				=> Clamp_Lo );		             
	-- Check the final output for Over/Under of Max Voltage to Motor -----------------------------
	----------------------------------------------------------------------------------------------


	
	----------------------------------------------------------------------------------------------
	Cmd_Out_Proc : process( CLK50 )
	begin
		if(( CLK50'event ) and ( CLK50 = '1' )) then
		
--		if(start_count < 1 ) then
--				Cmd_Out <= Cmd_Max;
--				start_count <= start_count+1;
--		else
--		
			-- Not Moving - Force to Zero
			if( Temp_Enable = '0' ) then
--			if(( Temp_Enable = '0' ) and (Delay_Cnt > TEC2_DELAY))  then
				Cmd_Out	<= (others=>'0');		
			elsif( Temp_Clk = '1' ) then
				if( Clamp_Hi = '1' )
					then Cmd_Out <= Cmd_Max;
				elsif( Clamp_Lo = '1' )
					then Cmd_Out <= (others=>'0');
				else Cmd_Out <= KPKI_OUT( 7 + LSB downto LSB );
				end if;
			end if;
--		end if;
		end if;
	end process;
	----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
end Behavioral;			-- PI(D)_Algorithm.VHD
----------------------------------------------------------------------------------------------------


