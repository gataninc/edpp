#**************************************************************************************************
# _pin_assign_revb1.tcl
#
# This script allows you to make:
#	pin assignments 
#	pin io_standards
#	Timing assignments
#
#
# Written by: Michael Solazzi
# Rev 1.0
# Febuary 3, 2010
#
#
# You can run this script from Quartus by observing the following steps:
# 1. Place this TCL script in your project directory
# 2. Open your project
# 3. Go to the View Menu and Auxilary Windows -> TCL console
# 4. In the TCL console type:
#		source <this filename.tcl>
# 5. The script will assign pins and return an "assignment made" message.
#**************************************************************************************************


#************** Open a project if one does not exist **********************************************
set project_name ep98860
set top_name ep98860

# if { ![project exists /$project_name] } {
# 	project create /$project_name
# }
# project open $project_name

set cmp_settings_group $top_name
if { ![project cmp_exists $cmp_settings_group] } {
        project create_cmp $top_name
}
project set_active_cmp $top_name

# Remove all assignments ( to prevent over-lap )
# cmp remove_assignment $top_name "*" "" "" ""
# Assign the device to be used with this project
cmp add_assignment $top_name "" "" DEVICE EP3C40F484C7
# set_global_assignment -name DEVICE_MIGRATION_LIST EP3C55F484C7
# set_global_assignment -name DEVICE_MIGRATION_LIST EP3C120F484C7

# manually add migration devices of
#	EP3C55F484C7
#	EP3C120F484C7
#***** Pin and I/O Standard Assignments **********************************************************
	#---- Master Reset ( From MityDSP ) ----------------------------------------------------------
	cmp add_assignment $top_name "" i_OMR					IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" i_OMR 					LOCATION "Pin_Y22";
	#---- Master Reset ( From MityDSP ) ----------------------------------------------------------
	
	#---- LVTTL Input Clocks ---------------------------------------------------------------------
	cmp add_assignment $top_name "" i_Mity_Clk				IO_STANDARD "3.3-V LVTTL";
 	cmp add_assignment $top_name "" i_Mity_Clk				LOCATION "Pin_T22";
	#---- LVTTL Input Clocks ---------------------------------------------------------------------

	#---- 2.5V Input Clocks ---------------------------------------------------------------------
	cmp add_assignment $top_name "" i_ADC_100				IO_STANDARD "Differential 2.5-V SSTL Class I";
 	cmp add_assignment $top_name "" i_ADC_100				LOCATION "Pin_AA12";
	cmp add_assignment $top_name "" i_ADC_100(n)				LOCATION "Pin_AB12";
	cmp add_assignment $top_name "" i_ADC_100(n)				IO_STANDARD "Differential 2.5-V SSTL Class I";
	#---- 2.5V Input Clocks ---------------------------------------------------------------------

	#-- Fast ADC (LTC2207) -----------------------------------------------------------------------
	cmp add_assignment $top_name "" o_ADC_RAND				IO_STANDARD "2.5V";
	cmp add_assignment $top_name "" o_ADC_DITHER				IO_STANDARD "2.5V";
	cmp add_assignment $top_name "" i_ADC_OTR				IO_STANDARD "2.5V";
	cmp add_assignment $top_name "" o_ADC_PGA				IO_STANDARD "2.5V";
	cmp add_assignment $top_name "" i_ADC_DATA\[0\]			IO_STANDARD "2.5V";
	cmp add_assignment $top_name "" i_ADC_DATA\[1\]			IO_STANDARD "2.5V";
	cmp add_assignment $top_name "" i_ADC_DATA\[2\]			IO_STANDARD "2.5V";
	cmp add_assignment $top_name "" i_ADC_DATA\[3\]			IO_STANDARD "2.5V";
	cmp add_assignment $top_name "" i_ADC_DATA\[4\]			IO_STANDARD "2.5V";
	cmp add_assignment $top_name "" i_ADC_DATA\[5\]			IO_STANDARD "2.5V";
	cmp add_assignment $top_name "" i_ADC_DATA\[6\]			IO_STANDARD "2.5V";
	cmp add_assignment $top_name "" i_ADC_DATA\[7\]			IO_STANDARD "2.5V";
	cmp add_assignment $top_name "" i_ADC_DATA\[8\]			IO_STANDARD "2.5V";
	cmp add_assignment $top_name "" i_ADC_DATA\[9\]			IO_STANDARD "2.5V";
	cmp add_assignment $top_name "" i_ADC_DATA\[10\]			IO_STANDARD "2.5V";
	cmp add_assignment $top_name "" i_ADC_DATA\[11\]			IO_STANDARD "2.5V";
	cmp add_assignment $top_name "" i_ADC_DATA\[12\]			IO_STANDARD "2.5V";
	cmp add_assignment $top_name "" i_ADC_DATA\[13\]			IO_STANDARD "2.5V";
	cmp add_assignment $top_name "" i_ADC_DATA\[14\]			IO_STANDARD "2.5V";
	cmp add_assignment $top_name "" i_ADC_DATA\[15\]			IO_STANDARD "2.5V";


	cmp add_assignment $top_name "" i_ADC_DATA\[0\]			LOCATIOn "Pin_AB20";
	cmp add_assignment $top_name "" i_ADC_DATA\[1\]			LOCATIOn "Pin_AB19";
	cmp add_assignment $top_name "" i_ADC_DATA\[2\]			LOCATIOn "Pin_AB18";
	cmp add_assignment $top_name "" i_ADC_DATA\[3\]			LOCATIOn "Pin_AB17";
	cmp add_assignment $top_name "" i_ADC_DATA\[4\]			LOCATIOn "Pin_AA20";
	cmp add_assignment $top_name "" i_ADC_DATA\[5\]			LOCATIOn "Pin_AA19";
	cmp add_assignment $top_name "" i_ADC_DATA\[6\]			LOCATIOn "Pin_AA18";
	cmp add_assignment $top_name "" i_ADC_DATA\[7\]			LOCATIOn "Pin_AA17";
	cmp add_assignment $top_name "" i_ADC_DATA\[8\]			LOCATIOn "Pin_AB16";
	cmp add_assignment $top_name "" i_ADC_DATA\[9\]			LOCATIOn "Pin_AB15";
	cmp add_assignment $top_name "" i_ADC_DATA\[10\]			LOCATIOn "Pin_AB14";
	cmp add_assignment $top_name "" i_ADC_DATA\[11\]			LOCATIOn "Pin_AB13";
	cmp add_assignment $top_name "" i_ADC_DATA\[12\]			LOCATIOn "Pin_AA16";
	cmp add_assignment $top_name "" i_ADC_DATA\[13\]			LOCATIOn "Pin_AA15";
	cmp add_assignment $top_name "" i_ADC_DATA\[14\]			LOCATIOn "Pin_AA14";
	cmp add_assignment $top_name "" i_ADC_DATA\[15\]			LOCATIOn "Pin_AA13";
	
	cmp add_assignment $top_name "" i_ADC_OTR				LOCATION "Pin_T15";
	cmp add_assignment $top_name "" o_ADC_RAND				LOCATION "Pin_U14";
	cmp add_assignment $top_name "" o_ADC_PGA				LOCATION "Pin_U12";
	cmp add_assignment $top_name "" o_ADC_DITHER				LOCATION "Pin_V13"
	#-- Fast ADC (LTC2207) ----------------------------------------------------------------------
		
	#-- Mity DSP Interface (TVTTL) ---------------------------------------------------------------
	cmp add_assignment $top_name "" o_DSP_WFF_FF				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" i_DSP_RFF_FF				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[0\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[1\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[2\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[3\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[4\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[5\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[6\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[7\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[8\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[9\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[10\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[11\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[12\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[13\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[14\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[15\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[16\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[17\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[18\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[19\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[20\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[21\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[22\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[23\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[24\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[25\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[26\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[27\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[28\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[29\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[30\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[31\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_DSP_IRQ_BAR			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" i_DSP_DMA_RD				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" i_DSP_WRITE_BUSY_BAR		IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_DSP_READ_BUSY_BAR		IO_STANDARD "3.3-V LVTTL";	
	cmp add_assignment $top_name "" io_DSP_DATA\[32\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" io_DSP_DATA\[0\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" i_DSP_SPARE\[1\]			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" i_DSP_SPARE\[2\]			IO_STANDARD "3.3-V LVTTL";

	cmp add_assignment $top_name "" i_DSP_RFF_FF				LOCATION "Pin_AA21";
	cmp add_assignment $top_name "" o_DSP_WFF_FF				LOCATION "Pin_AA22";

	cmp add_assignment $top_name "" io_DSP_DATA\[31\]			LOCATION "Pin_V21";
	cmp add_assignment $top_name "" io_DSP_DATA\[30\]			LOCATION "Pin_V22";
	cmp add_assignment $top_name "" io_DSP_DATA\[29\]			LOCATION "Pin_U20";
	cmp add_assignment $top_name "" io_DSP_DATA\[28\]			LOCATION "Pin_U21";
	cmp add_assignment $top_name "" io_DSP_DATA\[27\]			LOCATION "Pin_U22";
	cmp add_assignment $top_name "" io_DSP_DATA\[26\]			LOCATION "Pin_T17";
	cmp add_assignment $top_name "" io_DSP_DATA\[25\]			LOCATION "Pin_T18";
	cmp add_assignment $top_name "" io_DSP_DATA\[24\]			LOCATION "Pin_R17";
	cmp add_assignment $top_name "" io_DSP_DATA\[23\]			LOCATION "Pin_R19";
	cmp add_assignment $top_name "" io_DSP_DATA\[22\]			LOCATION "Pin_R20";
	cmp add_assignment $top_name "" io_DSP_DATA\[21\]			LOCATION "Pin_R21";
	cmp add_assignment $top_name "" io_DSP_DATA\[20\]			LOCATION "Pin_R22";
	cmp add_assignment $top_name "" io_DSP_DATA\[19\]			LOCATION "Pin_P20";
	cmp add_assignment $top_name "" io_DSP_DATA\[18\]			LOCATION "Pin_P21";
	cmp add_assignment $top_name "" io_DSP_DATA\[17\]			LOCATION "Pin_P22";
	cmp add_assignment $top_name "" io_DSP_DATA\[16\]			LOCATION "Pin_N18";
	cmp add_assignment $top_name "" io_DSP_DATA\[15\]			LOCATION "Pin_N19";
	cmp add_assignment $top_name "" io_DSP_DATA\[14\]			LOCATION "Pin_N20";
	cmp add_assignment $top_name "" io_DSP_DATA\[13\]			LOCATION "Pin_M16";
	cmp add_assignment $top_name "" io_DSP_DATA\[12\]			LOCATION "Pin_M19";
	cmp add_assignment $top_name "" io_DSP_DATA\[11\]			LOCATION "Pin_M20";
	cmp add_assignment $top_name "" io_DSP_DATA\[10\]			LOCATION "Pin_M21";
	cmp add_assignment $top_name "" io_DSP_DATA\[9\]			LOCATION "Pin_M22";
	cmp add_assignment $top_name "" io_DSP_DATA\[8\]			LOCATION "Pin_K18";
	cmp add_assignment $top_name "" io_DSP_DATA\[7\]			LOCATION "Pin_K19";
	cmp add_assignment $top_name "" io_DSP_DATA\[6\]			LOCATION "Pin_J18";
	cmp add_assignment $top_name "" io_DSP_DATA\[5\]			LOCATION "Pin_J21";
	cmp add_assignment $top_name "" io_DSP_DATA\[4\]			LOCATION "Pin_J22";
	cmp add_assignment $top_name "" io_DSP_DATA\[3\]			LOCATION "Pin_H19";
	cmp add_assignment $top_name "" io_DSP_DATA\[2\]			LOCATION "Pin_H20";
	cmp add_assignment $top_name "" io_DSP_DATA\[1\]			LOCATION "Pin_H21";
	cmp add_assignment $top_name "" io_DSP_DATA\[0\]			LOCATION "Pin_H22";
	cmp add_assignment $top_name "" o_DSP_IRQ_BAR			LOCATION "Pin_W19";
	cmp add_assignment $top_name "" i_DSP_DMA_RD				LOCATION "Pin_W20";
	cmp add_assignment $top_name "" i_DSP_WRITE_BUSY_BAR		LOCATION "Pin_W21";
	cmp add_assignment $top_name "" o_DSP_READ_BUSY_BAR		LOCATION "Pin_W22";
	cmp add_assignment $top_name "" io_DSP_DATA\[32\]			LOCATION "Pin_F19"
	cmp add_assignment $top_name "" i_DSP_SPARE\[2\]			LOCATION "Pin_F20"
	cmp add_assignment $top_name "" i_DSP_SPARE\[1\]			LOCATION "Pin_F21"
	cmp add_assignment $top_name "" i_DSP_SPARE\[0\]			LOCATION "Pin_F22"	
	#-- Mity DSP Interface (TVTTL) ---------------------------------------------------------------

	#-- LED Signals ------------------------------------------------------------------------------
	cmp add_assignment $top_name "" o_LED\[0\]				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_LED\[1\]				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_LED\[2\]				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_LED\[3\]				IO_STANDARD "3.3-V LVTTL";

	cmp add_assignment $top_name "" o_LED\[2\]				LOCATION "Pin_B21";
	cmp add_assignment $top_name "" o_LED\[1\]				LOCATION "Pin_B22";
	cmp add_assignment $top_name "" o_LED\[3\]				LOCATION "Pin_C21";
	cmp add_assignment $top_name "" o_LED\[0\]				LOCATION "Pin_C22";
	#-- LED Signals ------------------------------------------------------------------------------

	#-- Test Point Outputs -----------------------------------------------------------------------
	cmp add_assignment $top_name "" o_TP\[0\]				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_TP\[1\]				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_TP\[2\]				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_TP\[3\]				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_TP\[4\]				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_TP\[5\]				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_TP\[6\]				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_TP\[7\]				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_TP\[8\]				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_TP\[9\]				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_TP\[10\]				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_TP\[11\]				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_TP\[12\]				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_TP\[13\]				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_TP\[14\]				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_TP\[15\]				IO_STANDARD "3.3-V LVTTL";
	
	cmp add_assignment $top_name "" o_TP\[12\]				LOCATION "Pin_A16";
	cmp add_assignment $top_name "" o_TP\[13\]				LOCATION "Pin_A17";
	cmp add_assignment $top_name "" o_TP\[14\]				LOCATION "Pin_A18";
	cmp add_assignment $top_name "" o_TP\[15\]				LOCATION "Pin_A19";
	cmp add_assignment $top_name "" o_TP\[8\]				LOCATION "Pin_B16";
	cmp add_assignment $top_name "" o_TP\[9\]				LOCATION "Pin_B17";
	cmp add_assignment $top_name "" o_TP\[10\]				LOCATION "Pin_B18";
	cmp add_assignment $top_name "" o_TP\[11\]				LOCATION "Pin_B19";
	cmp add_assignment $top_name "" o_TP\[4\]				LOCATION "Pin_C13";
	cmp add_assignment $top_name "" o_TP\[5\]				LOCATION "Pin_C15";
	cmp add_assignment $top_name "" o_TP\[6\]				LOCATION "Pin_C17";
	cmp add_assignment $top_name "" o_TP\[7\]				LOCATION "Pin_C19";
	cmp add_assignment $top_name "" o_TP\[0\]				LOCATION "Pin_D13";
	cmp add_assignment $top_name "" o_TP\[1\]				LOCATION "Pin_D15";
	cmp add_assignment $top_name "" o_TP\[2\]				LOCATION "Pin_D17";
	cmp add_assignment $top_name "" o_TP\[3\]				LOCATION "Pin_D19";
	#-- Test Point Outputs -----------------------------------------------------------------------

	#-- Temperature Sensor -----------------------------------------------------------------------
	cmp add_assignment $top_name "" i_TMP_SENSE				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" i_TMP_SENSE				LOCATION "Pin_E11";
	#-- Temperature Sensor -----------------------------------------------------------------------

	#-- Motorized Slide Interface -----------------------------------------------------------------
	cmp add_assignment $top_name "" o_MDir					IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_MStep					IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" i_mout_sw				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" i_min_sw				IO_STANDARD "3.3-V LVTTL";

	cmp add_assignment $top_name "" o_MDir					LOCATION "Pin_A3";
	cmp add_assignment $top_name "" o_MStep					LOCATION "Pin_A4";
	cmp add_assignment $top_name "" i_mout_sw				LOCATION "Pin_A5";
	cmp add_assignment $top_name "" i_min_sw				LOCATION "Pin_A6";
	#-- Motorized Slide Interface -----------------------------------------------------------------
	
	#-- External Trigger for Live Spectrum Mapping -----------------------------------------------
	cmp add_assignment $top_name "" o_ESync_WEn2				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_ESync_Out2				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" i_ESync_In2				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_ESync_WEn1				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_ESync_Out1				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" i_ESync_In1				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" i_ESync_In0				IO_STANDARD "3.3-V LVTTL";
	
	cmp add_assignment $top_name "" i_ESync_In0				LOCATION "Pin_A7";
	cmp add_assignment $top_name "" o_ESync_Out1				LOCATION "Pin_A8";
	cmp add_assignment $top_name "" o_ESync_WEn1				LOCATION "Pin_A9";
	cmp add_assignment $top_name "" i_ESync_In1				LOCATION "Pin_A10";

	cmp add_assignment $top_name "" o_ESync_Out2				LOCATION "Pin_B7";
	cmp add_assignment $top_name "" o_ESync_WEn2				LOCATION "Pin_B8";
	cmp add_assignment $top_name "" i_ESync_In2				LOCATION "Pin_B9";		
	#-- External Trigger for Live Spectrum Mapping -----------------------------------------------

	#-- Preamp Interface -------------------------------------------------------------------------
	cmp add_assignment $top_name "" o_PA_SCLK				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_PA_AEN				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_PA_DEN				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" i_PA_SDOUT				IO_STANDARD "3.3-V LVTTL";	
	cmp add_assignment $top_name "" o_PA_SDIN				IO_STANDARD "3.3-V LVTTL";	
	cmp add_assignment $top_name "" o_PA_HV_CLK				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_PA_HV_EN_MR			IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_PA_SPARE    			IO_STANDARD "3.3-V LVTTL";

	cmp add_assignment $top_name "" o_PA_SCLK				LOCATION "Pin_B3";
	cmp add_assignment $top_name "" o_PA_AEN				LOCATION "Pin_B4";
	cmp add_assignment $top_name "" o_PA_DEN				LOCATION "Pin_B5";
	cmp add_assignment $top_name "" i_PA_SDOUT				LOCATION "Pin_B6";	
	cmp add_assignment $top_name "" o_PA_SDIN				LOCATION "Pin_C3";		
	cmp add_assignment $top_name "" o_PA_SPARE				LOCATION "Pin_C4";
	cmp add_assignment $top_name "" o_PA_HV_EN_MR			LOCATION "Pin_C6";
	
	cmp add_assignment $top_name "" o_PA_HV_CLK				LOCATION "Pin_C7";
	
	
	cmp add_assignment $top_name "" o_evch_clk				LOCATION "Pin_AA10";
	cmp add_assignment $top_name "" o_det_reset				LOCATION "Pin_AA9";
	cmp add_assignment $top_name "" o_evch_cs				LOCATION "Pin_AA8";
	cmp add_assignment $top_name "" o_evch_data				LOCATION "Pin_AA7";
	
	cmp add_assignment $top_name "" o_DET_RESET				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_evch_clk				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_evch_cs				IO_STANDARD "3.3-V LVTTL";
	cmp add_assignment $top_name "" o_evch_data				IO_STANDARD "3.3-V LVTTL";
	
	#-- Preamp Interface -------------------------------------------------------------------------
#***** Pin and I/O Standard Assignments **********************************************************

#***** Timing Assignments ************************************************************************
	# These are the Global Assigments
	set_global_assignment -name tsu_requirement 6ns;
	set_global_assignment -name tco_requirement 6ns;
	set_global_assignment -name tpd_requirement 6ns;
	set_global_assignment -name th_requirement 6ns;
	create_base_clock -fmax 100.00Mhz -duty_cycle 50 -target i_ADC_100 ADCCLK
	create_base_clock -fmax 50.00Mhz -duty_cycle 50 -target i_Mity_Clk DSPCLK 
	
	# These timing requirements are relaxed from the "Global Timing Requirements"
	set_instance_assignment -from * -to o_TP -name tpd_requirement 10ns;
	set_instance_assignment -from * -to o_TP -name tco_requirement 10ns;
	set_instance_assignment -from * -to o_LED -name tco_requirement 10ns;
	set_instance_assignment -from i_Mity_CLK -to * -name tco_requirement 10ns;
#***** Timing Assignments ************************************************************************

	
