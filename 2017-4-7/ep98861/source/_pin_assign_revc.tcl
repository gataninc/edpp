#**************************************************************************************************
# _pin_assign_revb1.tcl
#
# This script allows you to make:
#	pin assignments 
#	pin io_standards
#	Timing assignments
#
#
# Written by: Michael Solazzi
# Rev 1.0
# Febuary 3, 2010
#
#
# You can run this script from Quartus by observing the following steps:
# 1. Place this TCL script in your project directory
# 2. Open your project
# 3. Go to the View Menu and Auxilary Windows -> TCL console
# 4. In the TCL console type:
#		source <this filename.tcl>
# 5. The script will assign pins and return an "assignment made" message.
#**************************************************************************************************


#************** Open a project if one does not exist **********************************************
set project_name ep98861
set top_name ep98861


#set cmp_settings_group $top_name
#if { ![project cmp_exists $cmp_settings_group] } {
#        project create_cmp $top_name
#}
#project set_active_cmp $top_name

# Remove all assignments ( to prevent over-lap )
# cmp remove_assignment $top_name "*" "" "" ""
# Assign the device to be used with this project
set_global_assignment -name family CYCLONEIII
set_global_assignment -name device EP3C40F484C7
## set_global_assignment -name IO_STANDARD LVTTL
# cmp add_assignment $top_name "" "" DEVICE EP3C40F484C7
# set_global_assignment -name DEVICE_MIGRATION_LIST EP3C55F484C7
# set_global_assignment -name DEVICE_MIGRATION_LIST EP3C120F484C7

# manually add migration devices of
#	EP3C55F484C7
#	EP3C120F484C7
#***** Pin and I/O Standard Assignments **********************************************************
	#---- Master Reset ( From MityDSP ) ----------------------------------------------------------
#     set_location_assignment -to i_OMR Pin_Y22;
#     set_instance_assignment -to i_OMR                           -name IO_STANDARD "3.3V LVTTL";
	#---- Master Reset ( From MityDSP ) ----------------------------------------------------------
	
	#---- LVTTL Input Clocks ---------------------------------------------------------------------
     set_location_assignment -to i_Mity_Clk	Pin_T22;
     set_instance_assignment -to i_Mity_Clk                      -name IO_STANDARD "3.3V LVTTL";
	#---- LVTTL Input Clocks ---------------------------------------------------------------------

	#---- "2.5V" Input Clocks ---------------------------------------------------------------------
     set_location_assignment -to i_ADC_100		               Pin_AA12;
     set_location_assignment -to i_ADC_100(n)                    Pin_AB12;
     set_instance_assignment -to i_ADC_100                      -name IO_STANDARD "Differential 2.5-V SSTL Class I";
     set_instance_assignment -to i_ADC_100(n)                   -name IO_STANDARD "Differential 2.5-V SSTL Class I";
	#---- "2.5V" Input Clocks ---------------------------------------------------------------------

	#-- Fast ADC (LTC2207) -----------------------------------------------------------------------
     set_location_assignment -to i_ADC_DATA\[0\]			Pin_AB20;
     set_location_assignment -to i_ADC_DATA\[1\]			Pin_AB19;
     set_location_assignment -to i_ADC_DATA\[2\]			Pin_AB18;
     set_location_assignment -to i_ADC_DATA\[3\]			Pin_AB17;
     set_location_assignment -to i_ADC_DATA\[4\]			Pin_AA20;
     set_location_assignment -to i_ADC_DATA\[5\]			Pin_AA19;
     set_location_assignment -to i_ADC_DATA\[6\]			Pin_AA18;
     set_location_assignment -to i_ADC_DATA\[7\]			Pin_AA17;
     set_location_assignment -to i_ADC_DATA\[8\]			Pin_AB16;
     set_location_assignment -to i_ADC_DATA\[9\]			Pin_AB15;
     set_location_assignment -to i_ADC_DATA\[10\]			Pin_AB14;
     set_location_assignment -to i_ADC_DATA\[11\]			Pin_AB13;
     set_location_assignment -to i_ADC_DATA\[12\]			Pin_AA16;
     set_location_assignment -to i_ADC_DATA\[13\]			Pin_AA15;
     set_location_assignment -to i_ADC_DATA\[14\]			Pin_AA14;
     set_location_assignment -to i_ADC_DATA\[15\]			Pin_AA13;
     
     set_location_assignment -to i_ADC_OTR				Pin_T15;
     set_location_assignment -to o_ADC_RAND				Pin_U14;
     set_location_assignment -to o_ADC_PGA				Pin_U12;
     set_location_assignment -to o_ADC_DITHER			Pin_V13
 
     set_instance_assignment -to i_ADC_DATA\[0\]            -name IO_STANDARD "2.5V";
     set_instance_assignment -to i_ADC_DATA\[1\]            -name IO_STANDARD "2.5V";
     set_instance_assignment -to i_ADC_DATA\[2\]            -name IO_STANDARD "2.5V";
     set_instance_assignment -to i_ADC_DATA\[3\]            -name IO_STANDARD "2.5V";
     set_instance_assignment -to i_ADC_DATA\[4\]            -name IO_STANDARD "2.5V";
     set_instance_assignment -to i_ADC_DATA\[5\]            -name IO_STANDARD "2.5V";
     set_instance_assignment -to i_ADC_DATA\[6\]            -name IO_STANDARD "2.5V";
     set_instance_assignment -to i_ADC_DATA\[7\]            -name IO_STANDARD "2.5V";
     set_instance_assignment -to i_ADC_DATA\[8\]            -name IO_STANDARD "2.5V";
     set_instance_assignment -to i_ADC_DATA\[9\]            -name IO_STANDARD "2.5V";
     set_instance_assignment -to i_ADC_DATA\[10\]           -name IO_STANDARD "2.5V";
     set_instance_assignment -to i_ADC_DATA\[11\]           -name IO_STANDARD "2.5V";
     set_instance_assignment -to i_ADC_DATA\[12\]           -name IO_STANDARD "2.5V";
     set_instance_assignment -to i_ADC_DATA\[13\]           -name IO_STANDARD "2.5V";
     set_instance_assignment -to i_ADC_DATA\[14\]           -name IO_STANDARD "2.5V";
     set_instance_assignment -to i_ADC_DATA\[15\]           -name IO_STANDARD "2.5V";
     set_instance_assignment -to i_ADC_OTR                  -name IO_STANDARD "2.5V";
     set_instance_assignment -to o_ADC_RAND                 -name IO_STANDARD "2.5V";
     set_instance_assignment -to o_ADC_PGA                  -name IO_STANDARD "2.5V";
     set_instance_assignment -to o_ADC_DITHER               -name IO_STANDARD "2.5V";

	#-- Fast ADC (LTC2207) ----------------------------------------------------------------------
		
	#-- Mity DSP Interface (TVTTL) ---------------------------------------------------------------
     set_instance_assignment -to i_DSP_RFF_FF                    -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_DSP_WFF_FF                    -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[32\]               -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[31\]               -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[30\]               -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[29\]               -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[28\]               -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[27\]               -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[26\]               -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[25\]               -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[24\]               -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[23\]               -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[22\]               -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[21\]               -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[20\]               -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[19\]               -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[18\]               -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[17\]               -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[16\]               -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[15\]               -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[14\]               -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[13\]               -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[12\]               -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[11\]               -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[10\]               -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[9\]                -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[8\]                -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[7\]                -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[6\]                -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[5\]                -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[4\]                -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[3\]                -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[2\]                -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[1\]                -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to io_DSP_DATA\[0\]                -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_DSP_IRQ_BAR                   -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to i_Dsp_Altera_Int_Busy           -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to i_DSP_WRITE_BUSY_BAR            -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_DSP_READ_BUSY_BAR             -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to i_DSP_SPARE\[2\]                -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to i_DSP_SPARE\[1\]                -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to i_DSP_SPARE\[0\]	               -name IO_STANDARD "3.3V LVTTL";

     set_location_assignment -to i_DSP_RFF_FF				Pin_AA21;
     set_location_assignment -to o_DSP_WFF_FF				Pin_AA22;

     set_location_assignment -to io_DSP_DATA\[31\]			Pin_V21;
     set_location_assignment -to io_DSP_DATA\[30\]			Pin_V22;
     set_location_assignment -to io_DSP_DATA\[29\]			Pin_U20;
     set_location_assignment -to io_DSP_DATA\[28\]			Pin_U21;
     set_location_assignment -to io_DSP_DATA\[27\]			Pin_U22;
     set_location_assignment -to io_DSP_DATA\[26\]			Pin_T17;
     set_location_assignment -to io_DSP_DATA\[25\]			Pin_T18;
     set_location_assignment -to io_DSP_DATA\[24\]			Pin_R17;
     set_location_assignment -to io_DSP_DATA\[23\]			Pin_R19;
     set_location_assignment -to io_DSP_DATA\[22\]			Pin_R20;
     set_location_assignment -to io_DSP_DATA\[21\]			Pin_R21;
     set_location_assignment -to io_DSP_DATA\[20\]			Pin_R22;
     set_location_assignment -to io_DSP_DATA\[19\]			Pin_P20;
     set_location_assignment -to io_DSP_DATA\[18\]			Pin_P21;
     set_location_assignment -to io_DSP_DATA\[17\]			Pin_P22;
     set_location_assignment -to io_DSP_DATA\[16\]			Pin_N18;
     set_location_assignment -to io_DSP_DATA\[15\]			Pin_N19;
     set_location_assignment -to io_DSP_DATA\[14\]			Pin_N20;
     set_location_assignment -to io_DSP_DATA\[13\]			Pin_M16;
     set_location_assignment -to io_DSP_DATA\[12\]			Pin_M19;
     set_location_assignment -to io_DSP_DATA\[11\]			Pin_M20;
     set_location_assignment -to io_DSP_DATA\[10\]			Pin_M21;
     set_location_assignment -to io_DSP_DATA\[9\]			     Pin_M22;
     set_location_assignment -to io_DSP_DATA\[8\]			     Pin_K18;
     set_location_assignment -to io_DSP_DATA\[7\]			     Pin_K19;
     set_location_assignment -to io_DSP_DATA\[6\]			     Pin_J18;
     set_location_assignment -to io_DSP_DATA\[5\]			     Pin_J21;
     set_location_assignment -to io_DSP_DATA\[4\]	     		Pin_J22;
     set_location_assignment -to io_DSP_DATA\[3\]	     		Pin_H19;
     set_location_assignment -to io_DSP_DATA\[2\]		     	Pin_H20;
     set_location_assignment -to io_DSP_DATA\[1\]		     	Pin_H21;
     set_location_assignment -to io_DSP_DATA\[0\]	     		Pin_H22;
     set_location_assignment -to o_DSP_IRQ_BAR	     		Pin_W19;

     set_location_assignment -to i_Dsp_Altera_Int_Busy			Pin_W20;
     set_location_assignment -to i_DSP_WRITE_BUSY_BAR	     	Pin_W21;
     set_location_assignment -to o_DSP_READ_BUSY_BAR	     	Pin_W22;
     set_location_assignment -to io_DSP_DATA\[32\]			Pin_F19;
     set_location_assignment -to i_DSP_SPARE\[2\]		     	Pin_F20;
     set_location_assignment -to i_DSP_SPARE\[1\]		     	Pin_F21;
     set_location_assignment -to i_DSP_SPARE\[0\]		     	Pin_F22;	
	#-- Mity DSP Interface (TVTTL) ---------------------------------------------------------------

	#-- LED Signals ------------------------------------------------------------------------------
     set_instance_assignment -to o_LED\[3\]	                      -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_LED\[2\]	                      -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_LED\[1\]	                      -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_LED\[0\]	                      -name IO_STANDARD "3.3V LVTTL";

     set_location_assignment -to o_LED\[2\]				     Pin_B21;
     set_location_assignment -to o_LED\[1\]				     Pin_B22;
     set_location_assignment -to o_LED\[3\]				     Pin_C21;
     set_location_assignment -to o_LED\[0\]				     Pin_C22;
	#-- LED Signals ------------------------------------------------------------------------------

     #-- Test Point Outputs -----------------------------------------------------------------------

     set_instance_assignment -to o_TP\[15\]                      -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_TP\[14\]                      -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_TP\[13\]                      -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_TP\[12\]                      -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_TP\[11\]                      -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_TP\[10\]                      -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_TP\[9\]                      -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_TP\[8\]                      -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_TP\[7\]                      -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_TP\[6\]                      -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_TP\[5\]                      -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_TP\[4\]                      -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_TP\[3\]                      -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_TP\[2\]                      -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_TP\[1\]                      -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_TP\[0\]                      -name IO_STANDARD "3.3V LVTTL";

     set_location_assignment -to o_TP\[12\]				     Pin_A16;
     set_location_assignment -to o_TP\[13\]				     Pin_A17;
     set_location_assignment -to o_TP\[14\]				     Pin_A18;
     set_location_assignment -to o_TP\[15\]				     Pin_A19;
     set_location_assignment -to o_TP\[8\]				     Pin_B16;
     set_location_assignment -to o_TP\[9\]				     Pin_B17;
     set_location_assignment -to o_TP\[10\]				     Pin_B18;
     set_location_assignment -to o_TP\[11\]				     Pin_B19;
     set_location_assignment -to o_TP\[4\]				     Pin_C13;
     set_location_assignment -to o_TP\[5\]				     Pin_C15;
     set_location_assignment -to o_TP\[6\]   				Pin_C17;
     set_location_assignment -to o_TP\[7\]	     			Pin_C19;
     set_location_assignment -to o_TP\[0\]		     		Pin_D13;
     set_location_assignment -to o_TP\[1\]			     	Pin_D15;
     set_location_assignment -to o_TP\[2\]				     Pin_D17;
     set_location_assignment -to o_TP\[3\]   				Pin_D19;

	#-- Test Point Outputs -----------------------------------------------------------------------

	#-- Temperature Sensor -----------------------------------------------------------------------
     set_location_assignment -to i_TMP_SENSE				     Pin_E11;
     set_instance_assignment -to i_TMP_SENSE                      -name IO_STANDARD "3.3V LVTTL";
	#-- Temperature Sensor -----------------------------------------------------------------------

	#-- Motorized Slide Interface -----------------------------------------------------------------
     set_instance_assignment -to o_MDir                          -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_MStep                         -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to i_min_sw                        -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to i_mout_sw                       -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_MEN                            -name IO_STANDARD "3.3V LVTTL";

     set_location_assignment -to o_MDir					     Pin_A3;
     set_location_assignment -to o_MStep					Pin_A4;
     set_location_assignment -to i_mout_sw			     	Pin_A5;
     set_location_assignment -to i_min_sw				     Pin_A6;
     set_location_assignment -to o_MEN                           Pin_F15;
	#-- Motorized Slide Interface -----------------------------------------------------------------
	
	#-- External Trigger for Live Spectrum Mapping -----------------------------------------------
     set_instance_assignment -to i_ESync_In0                      -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_ESync_Out1                     -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_ESync_WEn1                     -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to i_ESync_In1                      -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_ESync_Out2                     -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_ESync_WEn2                     -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to i_ESync_In2                      -name IO_STANDARD "3.3V LVTTL";

     set_location_assignment -to i_ESync_In0	     			Pin_A7;
     set_location_assignment -to o_ESync_Out1				Pin_A8;
     set_location_assignment -to o_ESync_WEn1				Pin_A9;
     set_location_assignment -to i_ESync_In1	     			Pin_A10;
     set_location_assignment -to o_ESync_Out2				Pin_B7;
     set_location_assignment -to o_ESync_WEn2				Pin_B8;
     set_location_assignment -to i_ESync_In2		     		Pin_B9;		

	#-- External Trigger for Live Spectrum Mapping -----------------------------------------------

	#-- Preamp Interface -------------------------------------------------------------------------
     #-- o_PA_SCLK was on B3
     #-- O_PA_SDUN was on C3
     set_instance_assignment -to o_PA_SCLK                       -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_PA_AEN                        -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_PA_DEN                        -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to i_PA_SDOUT                      -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_PA_SDIN                       -name IO_STANDARD "3.3V LVTTL";
#     set_instance_assignment -to o_PA_SPARE                      -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_PA_HV_EN_MR                   -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_PA_HV_CLK				     -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_evch_clk                      -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_det_reset                     -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_evch_cs                       -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_evch_data                     -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_EvCh_Sel                      -name IO_STANDARD "3.3V LVTTL";
     set_instance_assignment -to o_Reset_En                      -name IO_STANDARD "3.3V LVTTL";

     set_location_assignment -to o_PA_SCLK   				Pin_C3;        
     set_location_assignment -to o_PA_AEN	     			Pin_B4;
     set_location_assignment -to o_PA_DEN		     		Pin_B5;
     set_location_assignment -to i_PA_SDOUT			     	Pin_B6;	
     set_location_assignment -to o_PA_SDIN				     Pin_B3;		
#     set_location_assignment -to o_PA_SPARE	     			Pin_C4;
     set_location_assignment -to o_PA_HV_EN_MR	     		Pin_C6;
     set_location_assignment -to o_PA_HV_CLK				    	Pin_C7;

     
     set_location_assignment -to o_EvCh_Sel                      Pin_AB4;
     set_location_assignment -to o_det_reset	     			Pin_AA9;
     set_location_assignment -to o_Reset_En                      Pin_AB5;               
     set_location_assignment -to o_evch_clk			     	Pin_AA10;
     set_location_assignment -to o_evch_cs		     		Pin_AA8;
     set_location_assignment -to o_evch_data	     			Pin_AA7;
     #-- Preamp Interface -------------------------------------------------------------------------
##***** Pin and I/O Standard Assignments **********************************************************
