## Generated SDC file "AD5453.sdc"

## Copyright (C) 1991-2010 Altera Corporation
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, Altera MegaCore Function License 
## Agreement, or other applicable license agreement, including, 
## without limitation, that your use is for the sole purpose of 
## programming logic devices manufactured by Altera and sold by 
## Altera or its authorized distributors.  Please refer to the 
## applicable agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 10.0 Build 218 06/27/2010 SJ Full Version"

## DATE    "Wed Sep 22 08:19:27 2010"

##
## DEVICE  "EP3C40U484C7"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {CLK50}  -period 20.000 -waveform { 0.000 10.000 } [get_ports {i_Mity_Clk}]
#create_clock -name {CLK50}  -period 20.000 -waveform { 0.000 10.000 } [get_ports {CLK50}]
create_clock -name {CLK100} -period 10.000 -waveform { 0.000  5.000 } [get_ports {i_ADC_100}]
#create_clock -name {CLK100} -period 10.000 -waveform { 0.000  5.000 } [get_ports {CLK100}]
# create_clock -name {CLK100} -period 10.000 -waveform { 0.000 5.000 } [get_ports {CLK100}]
# create_clock -name {CLK50}  -period 20.000 -waveform { 0.000 10.000 } [get_ports {CLK50}]


#**************************************************************
# Create Generated Clock
#**************************************************************



#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************

derive_clock_uncertainty


#**************************************************************
# Set Input Delay
#**************************************************************

# ***** Set Input Delay for A/D Converter
set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  2.000 [get_ports {i_ADC_DATA[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.000 [get_ports {i_ADC_DATA[*]}]
set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  2.000 [get_ports {i_ADC_OTR}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.000 [get_ports {i_ADC_OTR}]

# set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.333 [get_ports {i_ADC_DATA[*]}]
# set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.333 [get_ports {i_ADC_DATA[*]}]
# set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.333 [get_ports {i_ADC_OTR}]
# set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.333 [get_ports {i_ADC_OTR}]

# ***** Set Input Delay for A/D Converter


#***** Set Input Delay for DSP Related Signals
set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {i_DSP_WRITE_BUSY_BAR}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {i_DSP_WRITE_BUSY_BAR}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {i_Dsp_Altera_Int_Busy}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {i_Dsp_Altera_Int_Busy}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {i_DSP_RFF_FF}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {i_DSP_RFF_FF}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {io_DSP_Data[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {io_DSP_Data[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {i_DSP_SPARE[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {i_DSP_SPARE[*]}]
#***** Set Input Delay for DSP Related Signals

#***** Set Input Delay for the TMP05 Temperature Sensor
set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {i_TMP_SENSE}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {i_TMP_SENSE}]
#***** Set Input Delay for the TMP05 Temperature Sensor

#***** Set the Input Delay for the Preamp
set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {i_PA_SDOUT}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {i_PA_SDOUT}]
#***** Set the Input Delay for the Preamp

#***** Set the Input Delay for the Motorized Slide Interface
set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {i_min_sw}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {i_min_sw}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {i_mout_sw}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {i_mout_sw}]
#***** Set the Input Delay for the Motorized Slide Interface

#***** Set the Input Delay for the External Sync Signals
set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {i_ESync_In0}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {i_ESync_In0}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {i_ESync_In1}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {i_ESync_In1}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {i_ESync_In2}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {i_ESync_In2}]
#***** Set the Input Delay for the External Sync Signals
         

#**************************************************************
# Set Output Delay
#**************************************************************

# ***** Set Output Delay for A/D Converter
set_output_delay -clock {CLK100} -min 3.000 [get_ports {o_ADC_DITHER}]
set_output_delay -clock {CLK100} -max 4.000 [get_ports {o_ADC_DITHER}]

set_output_delay -clock {CLK100} -min 3.000 [get_ports {o_ADC_RAND}]         
set_output_delay -clock {CLK100} -max 4.000 [get_ports {o_ADC_RAND}]         

set_output_delay -clock {CLK100} -min 3.000 [get_ports {o_ADC_PGA}]          
set_output_delay -clock {CLK100} -max 4.000 [get_ports {o_ADC_PGA}]          
# ***** Set Output Delay for A/D Converter

# ***** Set Output Delay for DSP Related Signals
set_output_delay -clock {CLK50} -min 3.000 [get_ports {io_DSP_Data[*]}]          
set_output_delay -clock {CLK50} -max 4.000 [get_ports {io_DSP_Data[*]}]          

set_output_delay -clock {CLK50} -min 3.000 [get_ports {o_DSP_READ_BUSY_BAR}]          
set_output_delay -clock {CLK50} -max 4.000 [get_ports {o_DSP_READ_BUSY_BAR}]          

set_output_delay -clock {CLK50} -min 3.000 [get_ports {o_DSP_IRQ_BAR}]          
set_output_delay -clock {CLK50} -max 4.000 [get_ports {o_DSP_IRQ_BAR}]          

set_output_delay -clock {CLK50} -min 3.000 [get_ports {o_DSP_WFF_FF}]          
set_output_delay -clock {CLK50} -max 4.000 [get_ports {o_DSP_WFF_FF}]          
# ***** Set Output Delay for DSP Related Signals

# ***** Set Output Delay for the on-board LED
set_output_delay -clock {CLK50} -min 3.000 [get_ports {o_LED[*]}]          
set_output_delay -clock {CLK50} -max 3.000 [get_ports {o_LED[*]}]          
# ***** Set Output Delay for the on-board LED

# **** Set the Output Delay for the Preamp
set_output_delay -clock {CLK50} -min 3.000 [get_ports {o_PA_SCLK}]          
set_output_delay -clock {CLK50} -max 4.000 [get_ports {o_PA_SCLK}]          

set_output_delay -clock {CLK50} -min 3.000 [get_ports {o_PA_AEN}]          
set_output_delay -clock {CLK50} -max 4.000 [get_ports {o_PA_AEN}]          

set_output_delay -clock {CLK50} -min 3.000 [get_ports {o_PA_DEN}]          
set_output_delay -clock {CLK50} -max 4.000 [get_ports {o_PA_DEN}]          

set_output_delay -clock {CLK50} -min 3.000 [get_ports {o_PA_SDIN}]          
set_output_delay -clock {CLK50} -max 4.000 [get_ports {o_PA_SDIN}]          

#set_output_delay -clock {CLK50} 5.000 [get_ports {O_PA_SPARE}]          
set_output_delay -clock {CLK50} -min 3.000 [get_ports {o_DET_RESET}]          
set_output_delay -clock {CLK50} -max 4.000 [get_ports {o_DET_RESET}]          

set_output_delay -clock {CLK50} -min 3.000 [get_ports {o_Reset_En}]     
set_output_delay -clock {CLK50} -max 4.000 [get_ports {o_Reset_En}]     

set_output_delay -clock {CLK50} -min 3.000 [get_ports {o_PA_HV_EN_MR}]          
set_output_delay -clock {CLK50} -max 4.000 [get_ports {o_PA_HV_EN_MR}]          

set_output_delay -clock {CLK50} -min 3.000 [get_ports {o_PA_HV_CLK}]          
set_output_delay -clock {CLK50} -max 4.000 [get_ports {o_PA_HV_CLK}]          

set_output_delay -clock {CLK50} -min 3.000 [get_ports {o_evch_cs}]          
set_output_delay -clock {CLK50} -max 4.000 [get_ports {o_evch_cs}]          

set_output_delay -clock {CLK50} -min 3.000 [get_ports {o_evch_clk}]          
set_output_delay -clock {CLK50} -max 4.000 [get_ports {o_evch_clk}]          

set_output_delay -clock {CLK50} -min 3.000 [get_ports {o_evch_data}]     
set_output_delay -clock {CLK50} -max 4.000 [get_ports {o_evch_data}]     

set_output_delay -clock {CLK100} -min 3.000 [get_ports {o_EvCh_Sel}]     
set_output_delay -clock {CLK100} -max 4.000 [get_ports {o_EvCh_Sel}]     


# **** Set the Output Delay for the Preamp

# **** Set the Output Delay for the Motorized Slide Interface
set_output_delay -clock {CLK50} -min 3.000 [get_ports {o_MStep}]          
set_output_delay -clock {CLK50} -max 4.000 [get_ports {o_MStep}]          

set_output_delay -clock {CLK50} -min 3.000 [get_ports {o_MDir}]          
set_output_delay -clock {CLK50} -max 4.000 [get_ports {o_MDir}]          

set_output_delay -clock {CLK50} -min 3.000 [get_ports {o_MEn}]          
set_output_delay -clock {CLK50} -max 4.000 [get_ports {o_MEn}]          

# **** Set the Output Delay for the Motorized Slide Interface

# **** Set the Output Delay for the External Sync Interface
set_output_delay -clock {CLK50} -min 3.000 [get_ports {o_ESync_WEn2}]          
set_output_delay -clock {CLK50} -max 4.000 [get_ports {o_ESync_WEn2}]          

set_output_delay -clock {CLK50} -min 3.000 [get_ports {o_ESync_Out2}]          
set_output_delay -clock {CLK50} -max 4.000 [get_ports {o_ESync_Out2}]          

set_output_delay -clock {CLK50} -min 3.000 [get_ports {o_ESync_WEn1}]          
set_output_delay -clock {CLK50} -max 4.000 [get_ports {o_ESync_WEn1}]          

set_output_delay -clock {CLK50} -min 3.000 [get_ports {o_ESync_Out1}]          
set_output_delay -clock {CLK50} -max 4.000 [get_ports {o_ESync_Out1}]          
# **** Set the Output Delay for the External Sync Interface
		
# **** Set the Output Delay for the Digital Test Points
set_output_delay -clock {CLK100} -min 8.000 [get_ports {o_TP[*]}]          
set_output_delay -clock {CLK100} -max 9.000 [get_ports {o_TP[*]}]          
# **** Set the Output Delay for the Digital Test Points

#**************************************************************
# Set Clock Groups
#**************************************************************



#**************************************************************
# Set False Path
#**************************************************************



#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

