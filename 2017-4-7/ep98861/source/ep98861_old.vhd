--------------------------------------------------------------------------------------------
-- 	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	Artemis.vhd
----------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 23, 2010	
--   Board          : Artemis DPP Board
--   Schematic      : 4035.007.27000 Rev D & E ( Same but different layer order )
--
--   Top Level      : 4035.009.98661, S/W, Artemos, DPP
--
--   Description:	
--
--   History:       <date> - <Author> - Ver
--		10/15/14: Modified the ESync_Reg(0) to be sure that the RS485 transeivers for ESsync_OUT1 and ESync_OUT2 are disabled.  
--		7/1/13: PreampCtrl: Modified loigc around New_TEC1_Val to llok at more bits to be sure the size of the TEC2*TEC1_Factor < max pgm value.  If not set New_TEC1_Val to Max
--				:changed time (Delay_IGR) after IG is turned ON from 1 s to 4 s.  This is in support of 31200 TEC board.  give voltages time to stablize.
--		6/25/13: Detector is saturating when the beam current changes rapidly. Adjusting et_Sat_Cnt_Max to 100000 and adjusted No_Reset_Cnt_Max to 30s.  
--		6/21/13: Edited PID_Algorithm:   When detector is cooled then DTM is turned off and the detector is cooled; TEC1 does not turn on
--					Changed logic around: if(( Temp_Enable = '0' ) and (Delay_Cnt = TEC2_DELAY)) to fix the TEC1 bug (DTM is turned off). 
--		6/21/13:  Edited DET_Ctrl:  Detector is saturating when the beam current changes rapidly. Adjusting et_Sat_Cnt_Max to 10000. This chang is comparable to a ramp with 400kcps input.     
--		3/26/13:  Edited PreampCtrl:  Added CW_FORCE_LV.  CW_FORCE_LV	will turn on IG, R1, FD and RD on rev 3 TEC boards.  REV 3 TEC boards only turn on FD once IG is turned on.
--					 The SD3+ modules oscillate with FD on and module warm.  power up sequence has been adjusted in Rev 3 preAMP.
--    3/22/13:  Edited PreampCtrl:  Max TEC current is dependent on the TEC revision level.  
--              Increased current limit for rev 3 TEC board:  TEC1_MAX_CURRENT_rev3 =1.9A, TEC1_MAX2_CURRENT_rev3 = 2A and TEC2_MAX_CURRENT_rev3 = 2A.  
--		1/8/13:  Changed module Det_Crtl:  Changed the logic around Det_Sat_Cnt. This counter is reset to 0 if the saturation detection is not consecutive.  
--				Only consecutive saturation trips (up to Det_Sat_Cnt_Max) will cause Det_sat = 1;sporadic saturation trips are ignored.
--    10/15/12 - edited "Disc_Main"  changed NSlope_Threshold value from 2 to 0 on 10/15.  This improved B-N ratio.
--		06/16/12 - DSP Data Gen_EDS
--			Added "Key" at end of spectrum during a "Slow Mapping Pixel"
--			Created new state "FFR_SMAP_EOLN, just like FFR_FMAP_EOLN
--			which added a key after the last channel number:
--			31:16 = 0xEC00
--			15:0  = Frame number
--		06/13/12 - Main Module
--			Added o_ADC_PGA to CWORD bit CW_PGA_ENABLE (at position 3)
--		05/16/2012 - Module TimeCtrl.vhd
--			Changed SPEC_UPDATE to not be updated if Preset_FMAP or Preset_SMAP is enabled
--		04/29/21 - Ver 2.0 - Module MityDSP
--			Added STB_TIME_RESET ( Start/Stop Command Bit 1 and 0 both must be 1)
--			will force clock time and live time to be reset as Time_Enable is started
--			Must make it way through EDSBLOCK and TIMECTRL
--			Preset_Map was split into Preset_FMap and Preset_SMap for Fast and Slow mapping (respectively) effected files: TimeCtrl, EDSBlock, ep98861 and Dsp_Data_Gen
--		April 27, 2012
--			Module: Dsp_Data_Gen: 
--				Time Disable Latch added to FFR_STATUS register at bit 2
--				Added new state: FFR_TIME_ENABLE which will write the "Time_Enable" code to the Xilinx but not trigger a DMA. (Seems to work better)
--			
--		April 13, 2012
--			Found problem in DSP, with "Spot mapping", the serial port was limiting the speed. Altered DSP code for corrections.
--		April 5, 2012
--			Module: LTC2207
--				Added input for 1/2 Gain, which now cuts the Dout Gain by a factor of 2 to Discriminators
--		April 4, 2012
--			Changed DSP Code to "Time-Stamp" may steps for specturm clear.
--			No Changed to Altera or Xilinx other than date and version
--		March 27, 2012
--			Added a switch for 1/2 input gain
--				constant CW_HALF_GAIN			: integer := 24;
--			Module: LTC2207
--				Added input for 1/2 Gain, which cuts the DoutM Gain by a factor of 2
--
--		January 21, 2012
--			Verified Increased GAP Time as well as Averaging enable/disable circuitry
--		January 15, 2012
--			Added AVG_ENABLE to Control Word (CWORD) bit 2. 
--			This bit must be enabled AND the Gap Time > 160ns before averaging will occur.
--		December 28, 2011
--			Gap Time increased from 6 bits to 89 bits, 
--			ATCW_GapTime bits changed positions, from 5;0 to 23:16
--			Also effects EDSBlock and Main_Channel and Main_GT_Ram
--		December 12-13, 2011
--		December 12-13, 2011 - MCS - Main_Channel.vhd
--			Completed TimeConst7 for next TimeConst
--			Had to 
--			12/12/11 - MCS - MAIN_RAM.VHD
--				"Compelted" Peak_Time7 to be 11.52us (the sum of 7.38 and 3.84)
--				Had to adjust the depth of DPRAM and some other related parameters to acoomodate
--				the deeper memory
--				Also had to adjust Megafunction MAIN_RAM_DP to make it twice as deep
--				Had to add 25% Gain to the Main Channel for close to proper gain without having to change the preamp gain
--		December 13, 2011 - MCS - DISC_MAIN.VHD
--			Updated for TimeConst7, had to add to increase length of AVGSIZE as well as increase the depth of the DPRAM.
--			Update all references to TimeConst and make sure that 7 is included (and follows suit with the rest).
--		September 19, 2011
--			Added logic to Detecto Version information from TEC Control Board (refer to module PreampCtrl). Based on that information select TEC1/TEC2 calibration constants
--			to report correct currents by application (and internal use).
--			Forced o_ADC_RAND always enabled
--			Disabled WEINER filter
--		August 31, 2011
--			Added Weiner Filter with the following equation:
--				Dout = ( 0.25 * Din ) + ( Dout(delay1) ) - ( 0.25 * Dout( delay2 ))
--		August 30, 2011
--			Switched to Quartus II Ver 11 SP1
--		August 29, 2011 - MCS
--			Added a "WeinerFilter" to the main A/D input (main channel)
--			Reverted back to original TEC1/TEC2 Slope/Intercept values
--		August 25, 2011 - MCS
--			Module PreampCtrl.vhd
--				Adjusted TEC1/TEC2 ADC/Current Calculations per measurements from Joe Marchetta refer to file: TEMSDD-TEC1&2 V-I Readback vs. Actual.xlsx
--		August 24, 2011 Rev 1.00 - MCS
--			Dsp File C:\MityDSP\2.8\software\inc\lwip_port\lwipopts.h needed to be updated
--			Definition  TCP_QUEUE_OOSEQ needed to be changed from a 1 to a 0 (about line 499 )
--
-- 		July 14, 2011 Rev 1,00 - MCS
--			module: Main_Channel.vhd 
--				put BLR_BUSY back into of Fir_Acc due to large residul in Fir_Acc
--			module: pileup_reject.vhd
--				took "out-of-range" out of BLR_BUSY
-- 		July 13, 2011 Rev 1,00 - MCS
--			Took BLR_BUSY out of Fir_Acc due to large residul in Fir_Acc
--		07/12/11 Rev 0.1 - MCS - Module MSLIDE
--			Added AutoRetractTargetPosition to 250um
--			Added Retract Detection to < 500um
--		07/08/11 - Rev 1.0 - MCS
--			Changes were made to the DSP Code
--		06/30/11 - Rev 1.0 - MCS
--			Module: PreampCtrl
--				Added "PID Control" to TEC1 is now a TEC1_FACTOR of TEC2 during PID control (provided the MSB of the POT_VAL register is set to a 1.
--		06/20/11 - Rev 1.0 - MCS
--			Changes made to DSP due to timing problems effecting:
--			DSP Boot, Flashing, etc.
--		06/14/11 - Rev 1.01 - MCS
--			Module: Main_Channel.Vhd 	- Gap Time doubled
--			Module: Pileup_Reject.vhd 	- Gap Time Doubled
--			Module: EDSBLock.vhd		- Gap Time Doubled
--			Module: ep98861.vhd 		- Gap Time Doubled
--		06/13/11 Rev 0.1 - MCS
--			Module: MSLide_IMS.vhd
--				Gated off IMS_EN and IMS_CLK when MOVE_ENABLE = 0
--		06/08/11 Rev 0.2 - MCS
--			Module: Pileup_Reject.vhd Changed Out-of-Range timeout from 4x to 8x Amp Time
--		06/08/11 Rev 0.1 - MCS
--			Module: Pileup_Reject.vhd Changed Out-of-Range timeout from 2x to 4x Amp Time
--		06/02/11 Rev 1.1- MCS
--			Module: Pilepu_Reject.vhd Added "Out-Of_Range" Timeout (at 2x Amp Time) that keeps:
--				BLR_Busy active
--				Keeps PUR active for the same time
--		06/02/11 Rev 1.0
--			Modules: Disc_Fast and Disc_Main
--				Added logic to detect is FIR_DATA exceeds 16, bits, if so, then it is cut in half (High KV TEM debugging)
--			Module: DSP_DATA_GEN
--				Fixed DSS mode for an Interval of 0 (need to trap for it and keep in the DSS_WRITE state)
--			
--		06/01/11 - Module Disc_Main.vhd
--			Cut FIR_DATA in half (just to make it display properly in DSS Mode at 300KeV)
--		05/16/11 - Noise at Startup
--			Module: MSLide_IMS.VHD	
--				Changed constant IMS_MAX_SLOW	: integer := 1;		-- from 4
--			Module: MSlide.Vhd 
--				Changed kDecellStartPos from 5mm to 10mm
--				In ST_MOVE_START changed MODE_SLOW <= '1' to a dependencey of "MOVE_SLOW_ENC"
--		11051210:
--			Module: DetCtrl
--				Changed Reset Trip point from 95% to 92%
--		05/02/11 - MCS  
--			Module: Pileup_Reject
--				PA_Busy_Cnt_max( iDisc_Main) changed from "Shape_Time" to "Shape_Time + 2x Peak Time" to match that of the Dpp3
--		04/20/11:
--			Module: Disc_Main
--				Some slight changes to DISC_OUT logic
-- 		11041501: 
--			Module: PreampControl
--				Cleaned up Yellow Flashing/Flash for Status 1
-- 		11041410: 
--			Module: DETCTRL
--				Reworked some of the startup sequence to get the detector to "Kick Start" under Light, Dark and "Source" 
--				conditions
--			Module: EDSBLock
--				SCA's Eliminated
--		11040810: MSLide
--			Clean-up with AutoRetract/Startup and "CLICKING"
--		11040701:
--			Module: PreampCtrl.vhd
--				Changed Temp Error Checking to Idle and Cooling is enabled
--		110401xx
--			Module(s): Disc_Main, Disc_fast, Main_Channel
--				During PA_RESET or PA_Busy hold FIR_ACC reset
--				otherwise BLR_Disable - Inhibit BLR from updating
--				otherwise allow BLR to update
--		110331xx
--			Randomize the data to/from the DSP by XOR bits 31 to 1 by Bit 0, Leave bit 32 intact for data identification purposes
--			This works in both directions and must also be taken into account in the Xilinx FPGA
--		110328xx
--			Added a 100ms counter in EDSBlock used to keep track of input and output count rate
--			Added a bit in the Control Register (`CPS_RESET_INH at bit 25 )
--			when 0, the 100ms timer in EDSBlock counts every 100ms (this seems more stable)
--			when 1, the 100ms timer in EDSBLock is stopped with PA_RESET_DET
--			Seeing that when the 100ms timer was created in EDSBlock seemed more stable, changed the 100ms and 500ms timer
--			to count at the 50Mhz rate instead of at the 1ms and then 100/500 on that.
--			Leaving the "Switch" for the CPS_RESET_INH for further evaluation. It does seem to increase the countrate.
--		11032510: Module: Det_Ctrl
--			Had to add AD_OTR (A/D Out-of-Range) to hold "Reset_Out" asserted (internal reset detection)
--			until AD_OTR goes away. 
--			Also had to create another startup flag that prevent the AD_OTR from being used until 
--			Ready_Startup_Cnt_Max after the "DET_READY" signal goes active
--
--		110309xx:
--			Added to MSlide_Enc an "Enable" to allow movement. When a new position is entered, it must be greater than 100um
--			from the current position to start a movement. This will prevent the slide trying to move to the same position
--			causing potential viberations to the column.
--		11030801
--			Module: MityDSP
--				Added new Decode for Time_Clear_Start 
--				(Time Clear, if LSB of data = 1, then also restart)
--		11030702:
--			set O_ADC_PGA back to 1
--			Module: PreampCtrl
--				Moved Temperature error checking to the "Off-Line" State
--			Module: MSlide
--				Added three latch states for Detector Saturated, Watch-Dog and HiCountRate AutoRetract
--				Also fixed a bug with the HiCountRate logic
--			Module: DspDataGen
--				Increased Read Address Range 0x5F to 0x6F
--		11030101: 
--			set O_ADC_PGA to 0 to check for lower TEMPCO
--
--		11022403: Module DetCtrl
--			Prevented "RESET_STATE" from being anything but "Startup Idle" for non-active reset mode 
--		11022403: Module "Main"
--			Added "ADC_OFFSET" to be added to the i_ADC_DATA input to emulate a thermal drift
--		11022201: Module DetCtrl
--			Changed non-active reset to have a much longer time-out ( 10us)
--		11021602: Module DetCtrl
--			Added HV_CWORD
--			Added non-active reset mode to sense reset
--		11021601: Module PreampCtrl
--			Add ST_OFF_TEC2_START
--			and ST_OFF_TEC2_DELAY
--			which will use the same TEC2 delay time and 1st turn off TEC1, keep TEC2 on for that amount of time
--			and then turn off TEC2
--		11021110: Module MSlide
--			Correct potential for position drift for spikes on the encoder lines
--		11012410: Module Countrate
--			Elminted "PTR" and just manipulated registers directly
--			Don't use Design Partition or LogicLock Region for that module ( intefers with "Dead Time Calculation");
--		11012101: Module PREAMPCTRL
--			Changed DELAY_CNT from and integer to a 32-bit vector (to be compatible with TEC2 Delay )
--		11011401: Module PREAMPCTRL
--			Swapped turn on sequence for TECs such that:
--			TEC1 turns on
--			wait 30 seconds
--			TEC2 turns on
--		11011103:
--			Altered "PREAMPCTRL" to eliminate transititions to/from Force_HV and Force_Cool - All must go through off
--			External Input also goes to External Input State
--		11011101:
--			added Reset_Period_Sat_Cnt and Reset_Period_Sat_Cnt_Max to count how many reset periords
--			that are too small before triggering a "Detector Saturated"
--		Added Indicator Register - 0x5F
--			Indicator_Reg(  3 downto  0 ) <= ext( Status_LED1, 4 );
--			Indicator_Reg(  7 downto  4 ) <= ext( Status_LED2, 4 );
--			Indicator_Reg( 11 downto  8 ) <= ext( Status_Sync_Out, 4 );	
--			Indicator_Reg( 15 downto 12 ) <= ext( Status_Sync_in, 4 );
--			Indicator_Reg( 31 downto 16 ) <= (others=>'0');
--		10123107:
--			Dropped CPS at 7.28us with increasing beam currnet (spot size)
--			DETCTRL: Changed Reset_Sig_Holdoff_Time from 3000 to 1000
--			PILEUPREJECT: 
--				Eliminated PA_BUSY( iDisc_Pur )
--				Changed: PA_Busy_Cnt_Max( iDisc_Main )	from 2x Peak_TIme to ShapeTIme
--				FDisc_CPS is now from FDisc_Comb
--			
--		10123102: CLIP_MIN
--			MCA: Cleaneup Spectrum Coping
--			PILEUPREJECT: Changed CLipMIn to <= from <
--		10123002: Status LED1
--			PreamPCtrl: Generate Status_LED1 from within this module, export 2-bit code for LED
--						which responds to temperature and states.
--			Main: Status_LED1 is on bits 10:9
--				Code 0 = Off
--				Code 1 = Red
--				Code 2 = Green
--				Code 3 = Yellow
--			En_HV has been eliminated (Status bit 23 )
--		10123001: Slide Autoretract Delay
--			MSlide: Changed Delay_Cnt_Max from 100 to 4000 (4sec at 1ms)
--		10123001: 
--			Changed Delay_Cnt_Max from 100 to 2000 (2sec at 1ms)
--			Added a Wait for DELAY_CNT_MAX from Move_Done
--		2010/12/12
--			Live Spectrum Mapping was updated to:
--				iADR_FPixels changed to iADR_PIXELS_LINE		(0x0F)
--				iADR_FCOUNT changed to iADR_LINES_FRAME			(0x10)
--			Keys updated to:
--				krKey_Map_Start	Deleted
--				krKey_Map_Pixel	9 << 26			Bits15:0 are Pixel Count
--				krKey_Map_Line	10<<26				Bits 15:0 are Line Count
--				krMap_EOLN		11<< 26			Bits 15:0 are Frame Count
--			Added Pixel COunter, Line Counter and Frame Counter all 1-based (ie start at 1)
--				Spectrum Clear does reset all back to 1
--			Pixel Counter is incremented at end of dwell and reset at end of line
--			Line counter is incremented at end of line and reset at end of frame
--			frame counter is incremented at end of frame
--			during LSM:
--				At Start of Line
--					Line Key is added with lower 16-bits as the line count
--					Pixel Key follows line key (lower 16-bits is the pixel count)
--					Followed by non-zero spectral data
--				Middle of line
--					Only Pixel Key is writeen
--					Followed by non-zero spectral data
--				End of Line
--					Pixel Key Starts
--					Followed by non-zero spectral data
--					end of line key follows spectral data (lower 16-bits indicate frame count)
--
--		2010/12/09
--			Acceleration decreased by a factor of 16
--		2010/12/07
--			Hold TIMER always enabled
--		2010/12/06
--			Completed Motorized Slide Support (Parker Slide)
--		2010/12/02
--			Post ANL rebuild
--		2010/11/30
--			Module: PreampCtrl - move Watch-dog last and DET_SAT first
--							Moved lower temperature limit					
--			Module: DetCtrl	- Adjusted Det_Sat condition (from > to >= )
--        2010/09/24
--             Converted to TIMEQUEST Timing Analyzer
--		2010/09/14 Ver 7
--			The data being read by the Xilinx FPGA is clocked using the rising edge of i_Mity_CLK 
--			Therefore the data being sent to the Xilinx FPGA is registerd with the falling edge 
--			of i_Mity_CLK
-----------------------------------------------------------------------------------------------

library LPM;
     use lpm.lpm_components.all;

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;
----------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------
entity ep98861 is
     port(
		-- General I/O (Reset and Clocks )
--		i_omr				: in		std_logic;	-- Master Reset
		i_Mity_Clk			: in		std_logic;	-- DSP Clock 
		i_ADC_100				: in		std_logic;	-- ADC Clock ( output from ADC/ Input to FPGA - Master Clock		
		
		-- MityDSP Interface
		io_DSP_Data			: inout	std_logic_Vector( 32 downto 0 );
		o_DSP_READ_BUSY_BAR		: buffer	std_logic;
		i_DSP_WRITE_BUSY_BAR	: in		std_logic;
		i_Dsp_Altera_Int_Busy	: in		std_logic;
		o_DSP_IRQ_BAR			: buffer	std_logic;
		i_DSP_RFF_FF			: in 	std_Logic;
		o_DSP_WFF_FF			: buffer	std_logic;
		i_DSP_SPARE			: in 	std_Logic_Vector(  2 downto 0 );

		-- Temperature Sensor
		i_TMP_SENSE			: in		std_Logic;		-- Input from Analog Devices TMP05

		-- Fast ADC (LTC2207)
		o_ADC_DITHER			: buffer	std_logic;
		o_ADC_RAND			: buffer	std_logic;
		o_ADC_PGA				: buffer	std_logic;
		i_ADC_DATA			: in		std_logic_Vector( 15 downto 0 );
		i_ADC_OTR				: in		std_Logic;

		-- Misc - LED Outputs
		o_LED				: buffer	std_logic_Vector( 3 downto 0 );

		-- Preamp Interface
		o_PA_SCLK				: buffer	std_logic;
		o_PA_AEN				: buffer	std_logic;
		o_PA_DEN				: buffer	std_logic;
		i_PA_SDOUT			: in		std_Logic;
		o_PA_SDIN				: buffer	std_logic;
--		O_PA_SPARE			: buffer	std_logic;		
		o_PA_HV_EN_MR			: buffer	std_logic;
		o_PA_HV_CLK			: buffer	std_logic;
        
          o_Reset_En               : buffer  std_logic;
		o_DET_RESET			: buffer	std_Logic;         
          o_EvCh_Sel               : buffer  std_logic;
		o_evch_cs				: buffer	std_logic;
		o_evch_clk			: buffer	std_logic;
		o_evch_data			: buffer	std_logic;
				
		-- Motorized Slide Interface
		i_min_sw				: in		std_Logic;
		i_mout_sw				: in		std_logic;
		o_MStep				: buffer	std_logic;
		o_MDir				: buffer	std_Logic;
          o_Men                    : buffer  std_logic;
				
		-- External Sync		
		o_ESync_WEn2			: buffer	std_Logic;
		o_ESync_Out2			: buffer	std_logic;
		i_ESync_In2			: in		std_Logic;
		o_ESync_WEn1			: buffer	std_Logic;
		o_ESync_Out1			: buffer	std_logic;
		i_ESync_In1			: in		std_Logic;
		i_ESync_In0			: in		std_logic;
		
		-- Misc bufferputs		
		o_TP					: buffer 	std_logic_Vector( 15 downto 0 ) );
          ------------------------------------------------------------------------------------
		-- Pin Assignments Made ------------------------------------------------------------
          -------------------- ----------------------------------------------------------------
     end ep98861;
----------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------
architecture BEHAVIORAL of ep98861 is
	constant GEN_EDS                   : integer := 1;
	constant GEN_SLIDE				: integer := 1;
	constant GEN_TEMP				: integer := 1;
	
	-- Timing related Constants
	constant CLOCK_PER				: integer := 20;
	constant ms_cnt_max 			: integer := ( 1000000 / CLOCK_PER ) -1;
	-- Timing related Constants

	constant VER_APP_MAJOR			: integer := 0;        --  DPP FPGA
	constant VER_APP_MINOR	     	: integer := 1;        --  Rev C DPP
	constant VER_YEAR				: integer := 14;
	constant VER_MON				: integer :=  10;
	constant VER_DAY				: integer := 15;
	constant VER_MAJOR				: integer := 06;		-- Range 0 to 9
	constant VER_MINOR				: integer := 01;		-- Range 0 to 15
	
	-- LED Color Constants for front-panel Bi-Color LED (both Top and Bottom) --------------------
	constant LED_OFF				: std_logic_Vector( 1 downto 0 ) := "00";
	constant LED_RED				: std_logic_Vector( 1 downto 0 ) := "01";
	constant LED_GRN				: std_logic_Vector( 1 downto 0 ) := "10";
	constant LED_YEL				: std_logic_Vector( 1 downto 0 ) := "11";
	
	-- LED Color Constants for front-panel Bi-Color LED (both Top and Bottom) --------------------
	-- Read/Write Parameters ---------------------------------------------------------------------
	constant iADR_ESYNC_REG			: integer := 16#00#;		-- Enable External Load	
	constant iADR_CWORD				: integer := 16#01#;		-- Control Word
	constant iADR_HV_CTRL			: integer := 16#02#;
	constant iADR_ATCWORD			: integer := 16#03#;		-- New 110309xx	
	constant iADR_EVCH_GAIN			: integer := 16#04#;		-- Coarse Gain
	constant iADR_GAIN				: integer := 16#05#;		-- Fine Gain
	constant iADR_OFFSET			: integer := 16#06#;		-- Offset
	constant iADR_DISC_0			: integer := 16#07#;		-- Disc Thresold Level - SH
	constant iADR_DISC_1			: integer := 16#08#;		-- Disc Thresold Level - H
	constant iADR_DISC_2			: integer := 16#09#;		-- Disc Thresold Level - M
	constant iADR_DISC_3			: integer := 16#0A#;		-- Disc Thresold Level - L
	constant iADR_DISC_4			: integer := 16#0B#;		-- Disc Thresold Level - B (AmpTime/2)
	constant iADR_CLIP				: integer := 16#0C#;		-- Clip Max/Min
	constant iADR_PRESET			: integer := 16#0D#;		-- Preset Value
--	constant iADR_SCABB				: integer := 16#0E#;		-- SCA/BB Parameters
	constant iADR_Pixels_Line		: integer := 16#0F#;		-- Pixels per Line
	constant iADR_Lines_Frame		: integer := 16#10#;		-- Lines/Frame
	constant iADR_DSS_REG			: integer := 16#11#;		-- Static DSS Settings
	constant iADR_DSS_INT			: integer := 16#12#;		-- Interval Counter
	constant iADR_DSS_MAX			: integer := 16#13#;		-- Maximum Count
	constant iADR_BIT_PEAK			: integer := 16#14#;		-- Maximum Count
	constant iADR_BIT_TIME			: integer := 16#15#;		-- Maximum Count
	constant iADR_PA_Temp_Offset		: integer := 16#16#;
	constant iADR_TARGET_ADC			: integer := 16#17#;
	constant iADR_POT_VAL 			: integer := 16#18#;		-- read/write 4 preamp pots
	constant iADR_RESET_PW			: integer := 16#19#;
	constant iADR_LEAKAGE			: integer := 16#1A#;		-- No Longer Used
	constant iADR_PA_TEC2_KIKP		: integer := 16#1B#;		
	constant iADR_TEC2_DELAY			: integer := 16#1C#;		
	constant iADR_OTR_FACTOR			: integer := 16#1D#;
	constant iADR_RTIME_THR			: integer := 16#1E#;
	constant iADR_TEC1_FACTOR		: integer := 16#1F#;			-- TEC2 * Factor = TEC1
	
	constant iADR_SLIDE_HCR			: integer := 16#20#;
	constant iADR_SLIDE_HCT			: integer := 16#21#;
	constant iADR_MSlide_Tar 		: integer := 16#22#;		-- Target Position
	constant iADR_MSlide_Vel_Max		: integer := 16#23#;		-- Max Velocity
	constant iADR_MSlide_Vel_Min		: integer := 16#24#;		-- Min Velocity
	
	constant iADR_WF_CoefA			: integer := 16#25#;		
	constant iADR_WF_CoefB			: integer := 16#26#;		
	constant iADR_WF_CoefC			: integer := 16#27#;		
	constant iADR_WF_Delay			: integer := 16#28#;		
	
	constant iADR_RW_MAX			: integer := 16#2F#;
	-- Read/Write Parameters ---------------------------------------------------------------------
	
	-- Read Only Parameters ----------------------------------------------------------------------
	constant iADR_ALTERA_VER			: integer := 16#40#;			-- (64 dec)
     constant iADR_TEC_VER              : integer := 16#41#;          -- TEC Board Version Information
	constant iADR_PTIME_DT0			: integer := 16#42#;		-- Disc 1 & 0 Peak Time 
	constant iADR_PTIME_DT1			: integer := 16#43#;		-- Disc 3 & 2 Peak Time
	constant iADR_PTIME_AT0			: integer := 16#44#;		-- Main Channel Peak Time 1 & 0
	constant iADR_PTIME_AT1			: integer := 16#45#;		-- Main Channel Peak Time 3 & 2	
	constant iADR_PTIME_AT2			: integer := 16#46#;		-- Main Channel Peak Time 5 & 4
	constant iADR_PTIME_AT3			: integer := 16#47#;		-- Main Channel Peak Time 7 & 6
	constant iADR_PADC10			: integer := 16#48#;
	constant iADR_PADC32			: integer := 16#49#;
	constant iADR_PADC54			: integer := 16#4A#;
	constant iADR_PADC76			: integer := 16#4B#;
	constant iADR_TMP05				: integer := 16#4C#;	
	constant iADR_PA_TEMP			: integer := 16#4D#;
	constant iADR_STATUS			: integer := 16#4E#;		
	constant iADR_RAMP				: integer := 16#4F#;		-- Ramp Max/Min
	constant iADR_RST_TIME			: integer := 16#50#;		-- Ramp Reset Time
	constant iADR_RST_PER			: integer := 16#51#;		-- Reset Period ( or Pulse Time )
	constant iADR_CTIME				: integer := 16#52#;		-- Clock Time
	constant iADR_LTIME				: integer := 16#53#;		-- Live Time
	constant iADR_CPS				: integer := 16#54#;		-- Input Count Rate
	constant iADR_NPS				: integer := 16#55#;		-- Output Count RAte
	constant iADR_NCPS				: integer := 16#56#;		-- Net Input Counts
	constant iADR_NNPS				: integer := 16#57#;		-- Net Output Counts
	constant iADR_DCNTS_0			: integer := 16#58#;		-- Disc Counts - SH (40ns)
	constant iADR_DCNTS_1			: integer := 16#59#;		-- Disc Counts - H (160ns)	
	constant iADR_PIXEL_CNTR			: integer := 16#5A#;		-- Pixel Count
	constant iADR_FRAME_CNTR			: integer := 16#5B#;		-- Pixel Count
	constant iADR_MSlide_Pos 		: integer := 16#5C#;		-- Read Only Motorized Slide Current Position	
	constant iADR_SLIDE_MOVE_TIME		: integer := 16#5D#;		
	constant iADR_SLIDE_ANALYZE_POS	: integer := 16#5E#;		
	constant iADR_INDICATOR_REG		: integer := 16#5F#;
	constant iADR_CPeak_MaxMin		: integer := 16#60#;
	constant iADR_TEC_Current		: integer := 16#61#;
	
	constant iADR_RO_LAST			: integer := 16#6E#;	
	constant iADR_RO_MAX			: integer := 16#6F#;		-- Max ReadOnly Parameters (95 dec)
	-- Read Only Parameters ----------------------------------------------------------------------

	-- AmpTime Specific Control Word Parameters --------------------------------------------------
--	constant ATCW_GapTime_LSB 		: integer := 0;
--	constant ATCW_GapTime_MSB 		: integer := 5;
	constant ATCW_BLR_LSB			: integer := 6;
	constant ATCW_BLR_MSB			: integer := 7;
	constant ATCW_Disc_Dis_40		: integer := 8;
	constant ATCW_Disc_Dis_160		: integer := 9;
	constant ATCW_Disc_Dis_320		: integer := 10;
	constant ATCW_Disc_Dis_640		: integer := 11;
	constant ATCW_Disc_Dis_BLM		: integer := 12;
	constant ATCW_GapTime_LSB 		: integer := 16;
	constant ATCW_GapTime_MSB 		: integer := 16+7;
	-- AmpTime Specific Control Word Parameters --------------------------------------------------
	
	
	-- Control Word Bit Positions ----------------------------------------------------------------
	constant CW_WEINER_EN			: integer := 0;
	constant CW_ADC_RAND			: integer := 1;		-- 8/30/11
	constant CW_AVG_ENABLE			: integer := 2;
	constant CW_PGA_ENABLE			: integer := 3;

	constant CW_Auto_Retract_Disable	: integer := 5;
	constant CW_PRESET_MODE_L		: integer := 16;
	constant CW_PRESET_MODE_H		: integer := 18;

	constant CW_HALF_GAIN			: integer := 24;
	constant CW_CPS_RESET_INH		: integer := 25;
	constant CW_SLIDE_EN			: integer := 26;		-- just keeps track if a slide is enabled
	constant CW_BIT_GEN				: integer := 27;
	constant CW_TP_SEL_LSB			: integer := 28;
	constant CW_TP_SEL_MSB			: integer := 31;	
	-- Control Word Bit Positions ----------------------------------------------------------------

	constant ESCW_ESync1_Sel_L		: integer := 0;
	constant ESCW_ESync1_Sel_H		: integer := 2;
	constant ESCW_ESync2_Sel_L		: integer := 4;
	constant ESCW_ESync2_Sel_H		: integer := 6;

	constant ESCW_SPC_TEST			: integer := 31;
	
	-- Digital Storage Scope Parameters ----------------------------------------------------------
	constant DTrig_None 			: integer := 0;
	constant DTrig_FDisc			: integer := 1;
	constant DTrig_PA_Reset 			: integer := 2;
	constant DTrig_Blevel_Upd 		: integer := 3;
	constant DTrig_Profile_Start		: integer := 4;
	constant DTrig_Move_Start		: integer := 5;
	constant DTrig_Pos_Slope			: integer := 6;
	constant DTrig_Neg_Slope			: integer := 7;

	constant kdss_chan_selA_LSB		: integer := 20; 		-- these want to be in the top level
	constant kdss_Chan_SelA_MSB		: integer := 24;		-- These want to be in the top level

	-- Digital Storage Scope Parameters ----------------------------------------------------------
	
	-- Vector Bit Positions ----------------------------------------------------------------------
	constant iDisc_40 				: integer := 0;
	constant iDIsc_160 				: integer := 1;
	constant iDIsc_320 				: integer := 2;
	constant iDIsc_640 				: integer := 3;
	constant iDisc_Main				: integer := 4;
	-- Vector Bit Positions ----------------------------------------------------------------------
	
	-- Strobe bit positions ----------------------------------------------------------------------
	constant STB_TIME_START 			: integer := 0;
	constant STB_TIME_STOP 			: integer := 1;
	constant STB_TIME_CLR 			: integer := 2;
	constant STB_FIR_MR				: integer := 3;
	constant STB_FIFO_MR			: integer := 4;
	constant STB_READ_TRIG			: integer := 5;
	constant STB_SLIDE_STOP			: integer := 7;
	constant STB_SLIDE_MR			: integer := 8;
	constant STB_SLIDE_POS			: integer := 9;
	constant STB_SLIDE_FA			: integer := 10;
	constant STB_SLIDE_FR			: integer := 11;
	constant STB_DSS_START			: integer := 16;
	constant STB_DSS_STOP			: integer := 17;
	constant STB_TIME_RESET			: integer := 18;	-- Only Resets Times
	-- Strobe bit positions ----------------------------------------------------------------------

	constant WDog_Cnt_Max 			: integer := 2 * 15000;	-- 15 seconds at 1ms

	-- Test Register bit positions ---------------------------------------------------------------

	-- Type Declarations -------------------------------------------------------------------------
	type Write_Param_Type 			is array( iADR_ESYNC_REG to iADR_RW_MAX ) of std_logic_vector( 31 downto 0 );
	type Read_Param_Type 			is array( iADR_ALTERA_VER to iADR_RO_MAX ) of std_logic_vector( 31 downto 0 );
	-- Type Declarations -------------------------------------------------------------------------
	
	signal AmpTimeReg				: std_logic_Vector( 31 downto 0 );
	signal ATCWord					: std_logic_Vector( 31 downto 0 );

	signal Bit_Val 				: std_logic_Vector( 15 downto 0 );
	signal Bit_Reset 				: std_logic;
	signal Blevel_Upd				: std_logic;					-- Baseline Level Update
	signal Blevel					: std_logic_Vector( 11 downto 0 );		-- Baseline Level
	signal Blevel_En				: std_logic;

	signal Blevel_Data				: std_logic_vector( 31 downto 0 );
	
	signal CPeak					: std_Logic_Vector( 11 downto 0 );		-- Peak Value
	signal CLK100 					: std_logic;
	signal CLK50					: std_logic;
	signal CLK_Xilinx				: std_logic;
	signal Cmd_Stb 				: std_logic_Vector( 31 downto 0 );		-- ADD to MITYDSP
	signal CWord					: std_logic_Vector( 31 downto 0 );
	signal Cool_En_Ext_In1			: std_logic;
	signal Cool_En_Ext_In2			: std_logic;
	signal CPeak_Mux				: std_logic_Vector( 11 downto 0 );
	
	signal Dsp_Data_Gen_FFR_Code		: std_logic_Vector( 7 downto 0 );
	signal Det_Sat					: std_logic;
	signal Det_Ready				: std_logic;
	signal Det_Sat_Latch			: std_logic;
	signal Det_Reset_Det 			: std_logic;
	signal Disc_Fir_Out				: std_logic_Vector( (20*4)+19 downto 0 );
	signal Disc_Cnts				: std_logic_Vector(  63 downto 0 );	
	signal Dpp_Busy				: std_logic;
	signal Dsp_Rdata				: std_logic_Vector( 32 downto 0 );
	signal Dsp_Int_Del				: std_logic;
	signal Dsp_dma_Busy				: std_logic;
	signal Dsp_Read_Busy			: std_logic;
	signal Dss_En					: std_logic;
	signal dss_data				: std_logic_vector( 15 downto 0 );		-- Data
	signal dss_trigger				: std_logic_Vector( DTrig_Move_Start downto 0 );		-- Dymanic INputs
	signal Dsp_Oen					: std_logic;
	signal Dsp_Read_FF_Wen 			: std_logic;
	signal Dsp_Read_FF_WData 		: std_logic_Vector( 31 downto 0 );
	signal Dma_Read_Trig 			: std_logic;
	signal Dsp_Read_FF_EF_C100		: std_Logic;
     signal Dsp_Read_FF_EF              : std_logic;
	signal Dsp_Read_FF_FF			: std_Logic;
	signal Dsp_Read_FF_Ren 			: std_Logic;
	signal Dsp_Read_FF_Cnt			: std_logic_Vector( 13 downto 0 );
	signal Dsp_Read_FF_Data_Out		: std_logic_Vector( 31 downto 0 );
	signal Dsp_Write_FF_Wen			: std_logic;
	signal Dsp_Write_FF_Data			: std_logic_Vector( 32 downto 0 );
	signal Dsp_Write_FF_Ren			: std_logic;
	signal Dsp_Write_FF_RData		: std_logic_Vector( 32 downto 0 );
	signal Dsp_Write_FF_EF			: std_logic;
	signal Dsp_Write_Addr			: std_logic_Vector( 15 downto 0 );
	signal Dsp_Read_Addr			: std_logic_Vector( 15 downto 0 );
	signal Dsp_Write_En				: std_logic;
	signal Dss_Reg					: std_logic_Vector( 31 downto 0 );
	signal Dsp_FF_Write_En			: std_logic;
	signal Dsp_FF_Write_Data			: std_logic_Vector( 32 downto 0 );

     signal evch_clk 				: std_logic;
	signal evch_cs					: std_logic;
	signal evch_data 				: std_logic;
	signal Ext_Retract				: std_logic;
	signal Ext_Cool_En				: std_logic;
	signal En_Vent 				: std_logic;
	signal En_Almost_Ready			: std_logic;
	signal PACtrl_En_Ready			: std_logic;
	signal ESync_Reg				: std_logic_Vector( 31 downto 0 );

	signal FDisc_BB				: std_logic;
	signal FADC_DATA				: std_logic_Vector( 17 downto 0 );
	signal DADC_DATA				: std_logic_Vector( 17 downto 0 );
	
	signal Gap_Time				: std_logic_Vector( 7 downto 0);
	
	signal HV_State_Code			: std_logic_Vector( 4 downto 0 );
	
	signal Indicator_Reg			: std_logic_Vector(31 downto 0 );
	
	signal LTC1876_Data 			: std_logic_Vector( (32*4)-1 downto 0 );
	signal ltc2207_out				: std_Logic_vector( 17 downto 0 );		
	signal ltc2207_outM				: std_logic_vector( 17 downto 0 );
	signal ltc2207_raw				: std_logic_vector( 17 downto 0 );
	signal ltc2207_weiner			: std_logic_vector( 17 downto 0 );
	
	signal Main_Dly_Data 			: std_logic_Vector( 89 downto 0 );
	signal Main_Out				: std_logic_Vector( 19 downto 0 );
--	signal Main_Out				: std_logic_Vector( 15 downto 0 );
	signal Main_Sum				: std_logic_Vector( 31 downto 0 );	
	signal MCA_Data				: std_logic_Vector( 31 downto 0 );
	signal Meas_Done				: std_logic;					-- Peak Identification Done
	signal Meas_Done_Mux 			: std_logic;
	signal ms1_tc					: std_logic;
	signal ms_cnt					: integer range 0 to ms_cnt_max;
	signal Map_Line_Code			: std_logic_Vector(  1 downto 0 );
	signal Map_Pixel_Status			: std_logic_Vector( 15 downto 0 );
	signal Map_Line_Status			: std_logic_Vector( 15 downto 0 );
	signal Map_Frame_Status			: std_logic_Vector( 15 downto 0 );				
	signal MSlide_New_Pos			: std_logic;
	signal ms100_cnt				: integer range 0 to 100;

	signal o_PA_AD_SYNC 			: std_logic;
	signal o_PA_EVCH_SYNC			: std_logic;
	signal OTR_Factor				: std_logic_Vector( 31 downto 0 );

	signal Param_RData				: Read_Param_Type;
	signal Param_WData				: Write_Param_Type;
	signal Param_Reg				: std_logic_Vector( 31 downto 0 );
	signal Param_Bit_RDATA			: std_logic_Vector( (32*iADR_RO_MAX)+31 downto (32*iADR_ALTERA_VER) );		-- All Data
	signal Preset_FMap				: std_logic;
	signal Preset_SMap				: std_logic;
	signal Peak_Time				: std_logic_Vector( 143 downto 0 );
	signal PACtrl_Force_Reset		: std_logic;
	signal PACtrl_Enable_Reset		: std_logic;
	
	signal Read_Data_En				: std_logic;
	signal Reset_out				: std_logic;
     signal Reset_Grn_LED               : std_logic;

	signal RTime_Upd				: std_logic;
	signal Rtime_Rej				: std_logic;
	signal RTime					: std_logic_Vector( 31 downto 0 );

	signal SADC_CLK1				: std_logic;
	signal SADC_SDO1				: std_logic;

--	signal SCABB_WEN				: std_Logic;
--	signal SCABB_RD				: std_logic_Vector( 31 downto 0 );	
	signal Slide_State				: std_logic_Vector( 4 downto 0 );
	signal Slide_Home_Status			: std_logic;
	signal Slide_Move_Enable 		: std_logic;

	signal sdac_data				: std_logic_Vector( 127 downto 0 );
	signal Status					: std_logic_Vector( 31 downto 0 );
--	signal SCABB_Out				: std_logic;
	signal Spec_Update				: std_logic;
	signal Spec_Buf_Full			: std_logic;
     signal Standby                     : std_logic;
	signal Start_MCA_Update 			: std_logic;
	signal Start_Blevel_Update 		: std_logic;		
	signal Status_LED1				: std_logic_Vector( 1 downto 0 );
	signal Status_LED2				: std_logic_Vector( 1 downto 0 );
	signal Status_Sync_Out			: std_logic_Vector( 1 downto 0 );
	signal Status_Sync_in			: std_logic_Vector( 1 downto 0 );	
		
	signal Time_Enable				: std_logic;
	signal Time_Disable_Latch		: std_logic;
	signal Time_Enable_Latch			: std_logic;

	signal TLevel 					: std_logic_Vector(  (32*5)-1 downto 0 );
	signal tp_data					: std_logic_vector( 15 downto 0 );

	signal version 				: std_logic_Vector( 31 downto 0 );

	signal WDog_Cnt				: integer range 0 to WDog_Cnt_Max;
	signal WDog_Trig				: std_logic;

	----------------------------------------------------------------------------------------------
	component clock_gen
		port( 
			inclk0			: in		std_logic := '0';
			c0				: out	std_logic );
	end component;
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	component dsp_clk_buf
		port( 
			inclk0			: in		std_logic := '0';			
			c0				: out	std_logic );
	end component;
	----------------------------------------------------------------------------------------------

	--////////////////////////////////////////////////////////////////////////////////////////////
	--// MITYDSP INTERFACE ///////////////////////////////////////////////////////////////////////
	--////////////////////////////////////////////////////////////////////////////////////////////

	----------------------------------------------------------------------------------------------
	component Dsp_Data_Gen is 
		generic(
			-- Strobe bit positions ----------------------------------------------------------------------
			STB_TIME_CLR		: integer := 2;
			STB_DSS_START		: integer := 16;
			STB_DSS_STOP		: integer := 17 );
		port( 
			CLK100			: in		std_logic;							-- Internal Clock		
			-- DPP Related Signals
			Blevel_En			: in		std_logic;
			Spc_Test_En		: in		std_logic;
			dss_reg			: in		std_logic_vector( 31 downto 0 );		-- Static Parameters
			dss_interval		: in		std_logic_vector( 31 downto 0 );		-- Interval
			dss_cnt_max		: in		std_Logic_Vector( 31 downto 0 );		-- Maximum Points
			dss_data			: in		std_logic_vector( 15 downto 0 );		-- Data
			dss_trigger		: in		std_logic_Vector(  5 downto 0 );		-- Dymanic INputs
			Line_Code			: in		std_logic_Vector(  1 downto 0 );
			Pixel_Status		: in		std_logic_Vector( 15 downto 0 );
			Line_Status		: in		std_logic_Vector( 15 downto 0 );
			Frame_Status		: in		std_logic_Vector( 15 downto 0 );								
			Spec_Update		: in		std_logic := '0';		
			Time_Enable		: in		std_logic;
			Read_Data_En		: in		std_logic;
			Preset_FMap		: in		std_logic;
			Preset_SMap		: in		std_logic;
			MCA_Data			: in		std_logic_Vector( 31 downto 0 );
			Blevel_Data		: in		std_logic_Vector( 31 downto 0 );
			RData			: in		std_logic_vector( (32*111)+31 downto (32*64) );
			CMD_STB			: in  	std_logic_Vector( 31 downto 0 );			
			Read_FF_EF		: in		std_logic;
			Read_FF_FF		: in		std_logic;		
			Spec_Buf_Full		: in		std_logic;
			
			-- Read Data - All Parameters to be multiplexed
			Time_Disable_Latch	: buffer 	std_logic;
			Time_Enable_Latch	: buffer  std_logic;
			Dss_En			: buffer	std_logic;
			Start_MCA_Update	: buffer	std_logic := '0';
			Start_Blevel_Update	: buffer	std_logic := '0';
			Dpp_Busy			: buffer	std_logic;
			DMA_Read_Trig		: buffer 	std_logic;
			Read_FF_Wen		: buffer	std_logic;
			Read_FF_WData		: buffer	std_logic_vector( 31 downto 0 );
			FFR_State_Code		: buffer	std_logic_Vector(  7 downto 0 ) );
		end component Dsp_Data_Gen;
	---------------------------------------------------------------------------------------------------

	---------------------------------------------------------------------------------------------------
	component MCA is 
		port( 
			clk100			: in		std_logic;
			Dss_Enable		: in		std_logic;		
			Time_Enable		: in		std_logic;
			Spec_Clear		: in		std_logic;
			Spec_Update		: in		std_logic;
			Meas_Done			: in		std_logic;
			Cpeak			: in		std_logic_Vector( 11 downto 0 );
			CLIP_MAXMIN		: in		std_logic_Vector( 31 downto 0 );
			CPeak_Max_Min		: buffer	std_logic_Vector( 31 downto 0 );
			MCA_Data			: buffer	std_logic_Vector( 31 downto 0 ) );
	end component MCA;
	---------------------------------------------------------------------------------------------------

	---------------------------------------------------------------------------------------------------
	component BMCA is 
		port( 
			clk100			: in		std_logic;
			Dss_Enable		: in		std_logic;
			Spec_Clear		: in		std_logic;
			Spec_Update		: in		std_logic;
			Blevel_upd		: in		std_logic;
			Blevel			: in		std_logic_Vector( 11 downto 0 );
			Blevel_Data		: buffer  std_logic_Vector( 31 downto 0 ) );
	end component BMCA;
	---------------------------------------------------------------------------------------------------

	---------------------------------------------------------------------------------------------------
	component MityDSP is 
		generic(
			-- Strobe bit positions ----------------------------------------------------------------------
			STB_TIME_START 	: integer := 0;
			STB_TIME_STOP 		: integer := 1;
			STB_TIME_CLR 		: integer := 2;
			STB_FIR_MR		: integer := 3;
			STB_FIFO_MR		: integer := 4;
			STB_READ_TRIG		: integer := 5;
			STB_SLIDE_STOP		: integer := 7;
			STB_SLIDE_MR		: integer := 8;
			STB_SLIDE_POS		: integer := 9;
			STB_SLIDE_FA		: integer := 10;
			STB_SLIDE_FR		: integer := 11;		
			STB_DSS_START		: integer := 16;
			STB_DSS_STOP		: integer := 17;	
			STB_TIME_RESET		: integer := 18;	-- Only Resets Times
			iADR_RW_MAX		: integer := 31 );
		port( 
			CLK100			: in		std_logic;							-- Internal Clock
			CLK50			: in		std_logic;							-- DSP Clock
			DSP_RFF_FF		: in		std_logic;
			Read_FF_EF		: in		std_logic;
			Read_FF_FF		: in		std_logic;
			DMA_Read_Trig		: in 	std_logic;
			Read_FF_Cnt		: in		std_logic_vector( 13 downto 0 );
			DSP_AInt_Busy		: in		std_logic;
			DSP_Write_Busy		: in		std_logic;			
			Dsp_Write_FF_EF	: in		std_logic;
			Dsp_Write_FF_RData	: in		std_logic_Vector( 32 downto 0 );
			MCA_Data			: in		std_logic_vector( 31 downto 0 );
			BMCA_Data			: in		std_logic_Vector( 31 downto 0 );			
			Dsp_Write_FF_Ren	: buffer	std_logic;
			Dsp_Write_Addr		: buffer	std_logic_Vector( 15 downto 0 );
			Dsp_Write_En		: buffer	std_logic;		
			Dsp_Read_Busy		: buffer	std_logic;
			o_DSP_INT			: buffer	std_logic;
			DSP_OEN			: buffer	std_logic;
			Read_FF_Ren		: buffer 	std_logic;
			Spec_Buf_Full 		: buffer	std_logic;
			Blevel_En			: buffer	std_logic;
			Read_Data_En		: buffer  std_logic;
			Dsp_Read_Addr		: buffer	std_logic_Vector( 15 downto 0 );
--			SCABB_WEN			: buffer	std_logic;
			CMD_STB			: buffer	std_logic_Vector( 31 downto 0 );
			AmpTimeReg		: buffer	std_logic_Vector( 31 downto 0 ) );						
		end component MityDSP;
	---------------------------------------------------------------------------------------------------

     ---------------------------------------------------------------------------------------------------
	component Dsp_Read_Fifo is
          port(
               aclr				: in		std_logic  := '0';
               rdclk			: in		std_logic;
               rdreq			: in		std_logic;
               wrclk			: in		std_logic;
               wrreq			: in		std_logic;			
               data				: in		std_logic_vector (31 DOWNTO 0);
               rdempty			: out	std_logic;
               wrfull			: out	std_logic;
               wrusedw			: out 	std_logic_vector (13 DOWNTO 0);
               q				: out 	std_logic_vector (31 DOWNTO 0) );
	end component;
	---------------------------------------------------------------------------------------------------

	---------------------------------------------------------------------------------------------------
	component dsp_Write_Fifo is
		port(
			sclr				: in		std_logic  := '0';
			clock     		: in		std_logic;
			wrreq			: in		std_logic;			
			rdreq			: in		std_logic;
			data				: in		std_logic_vector (32 DOWNTO 0);
			empty			: out	std_logic;
			full			     : out	std_logic;
			q				: out 	std_logic_vector (32 DOWNTO 0) );
	end component;
	
	---------------------------------------------------------------------------------------------------
	--////////////////////////////////////////////////////////////////////////////////////////////
	--// MITYDSP INTERFACE ///////////////////////////////////////////////////////////////////////
	--////////////////////////////////////////////////////////////////////////////////////////////

	--////////////////////////////////////////////////////////////////////////////////////////////
	--// DPP INTERFACE ///////////////////////////////////////////////////////////////////////////
	--////////////////////////////////////////////////////////////////////////////////////////////
	----------------------------------------------------------------------------------------------
	component Bit_Gen is
		port( 
			clk100         : in      std_logic;                     	-- Master Clock
			BIT_EN		: in		std_Logic;					-- BIT Enable
			peak	     	: in      std_logic_vector( 31 downto 0 ); 	-- Peak 1 Value
			timing		: in      std_logic_vector( 31 downto 0 ); 	-- Peak 1 Value
			BIT_VAL		: buffer	std_Logic_Vector( 15 downto 0 );		
			Reset		: buffer	std_logic );
	end component Bit_Gen;
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	component LTC2207 is 
		port( 
			CLK100	: in		std_logic;
			DIN		: in		std_Logic_Vector( 15 downto 0 );
			Rand		: in		std_logic;
			PA_Reset	: in		std_logic;
			OTR		: in		std_Logic;					-- Logic High
			Half_Gain	: in		std_logic;
			Gain		: in		std_logic_vector( 31 downto 0 );
			Raw_Dout	: buffer	std_logic_Vector( 17 downto 0 );
			DOUT		: buffer	std_logic_Vector( 17 downto 0 );
			DoutM	: buffer	std_logic_Vector( 17 downto 0 ) );
	end component LTC2207;
	----------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------
	component WeinerFilter is 
		port( 
			CLK100	: in		std_logic;
			CoefA	: in		std_logic_Vector( 31 downto 0 );
			CoefB	: in		std_logic_vector( 31 downto 0 );
			CoefC	: in		std_logic_Vector( 31 downto 0 );
			Delay	: in		std_logic_Vector( 31 downto 0 );
			DIN		: in		std_Logic_Vector( 17 downto 0 );
			DOUT		: buffer	std_logic_Vector( 17 downto 0 ) );
	end component WeinerFilter;
	---------------------------------------------------------------------------------------------------

	
	----------------------------------------------------------------------------------------------
	component Det_Ctrl is 
		port( 	
			CLK100			: in		std_logic;					-- Master Clock
			AD_OTR			: in		std_logic;					-- A/D Out-of-Range Indication
               force_reset         : in      std_logic;
			Det_Cold			: in		std_logic;
			Reset_Enable		: in		std_logic;
			Reset_PW			: in		std_logic_Vector( 31 downto 0 );
			Din				: in		std_logic_vector( 17 downto 0 );	-- A/D Data Output - UNSIGNED
			HV_CWORD			: in		std_logic_Vector( 31 downto 0 );
			PA_MaxMin			: buffer 	std_logic_vector( 31 downto 0 );
			Reset_Width		: buffer	std_logic_Vector( 31 downto 0 );
			Reset_Period		: buffer	std_logic_vector( 31 downto 0 );
               o_Reset_Det         : buffer  std_logic;
			Reset_Out			: buffer	std_logic;
               Reset_Grn_LED       : buffer  std_logic;
			Det_Sat			: buffer	std_logic;
			Det_Ready			: buffer	std_logic );
	end component Det_Ctrl;
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	component EDSBlock is
		generic(
			-- Strobe bit positions ----------------------------------------------------------------------
			STB_TIME_START 	: integer := 0;
			STB_TIME_STOP 		: integer := 1;
			STB_TIME_CLR 		: integer := 2;
			STB_TIME_RESET		: integer := 18;	-- Only Resets Times
			STB_FIR_MR		: integer := 4;
			ATCW_BLR_LSB		: integer := 6;
			ATCW_BLR_MSB		: integer := 7;
			ATCW_Disc_Dis_40	: integer := 8;
			ATCW_Disc_Dis_160	: integer := 9;
			ATCW_Disc_Dis_320	: integer := 10;
			ATCW_Disc_Dis_640	: integer := 11;
			ATCW_Disc_Dis_BLM	: integer := 12;	
			CW_AVG_ENABLE		: integer := 2;
			CW_CPS_RESET_INH	: integer := 25;
			CW_PRESET_MODE_L	: integer := 16;
			CW_PRESET_MODE_H	: integer := 18 );		
		port(
			clk100			: in		std_logic;		
			ext_Trig			: in		std_logic;
			fast_ad			: in		std_logic_vector( 17 downto 0 );
			disc_ad			: in		std_logic_vector( 17 downto 0 );
			Cmd_Stb			: in		std_logic_Vector( 31 downto 0 );
			Gap_Time			:  in	std_logic_Vector( 7 downto 0 );
			PA_Reset_Det		: in		std_logic;
			AmpTime			: in		std_logic_Vector( 31 downto 0 );
			Blevel_En			: in 	std_logic;
			ATCWord			: in		std_logic_Vector( 31 downto 0 );
			CWord			: in		std_logic_Vector( 31 downto 0 );
			Clip_MaxMin 		: in		std_logic_Vector( 31 downto 0 );
			PRESET_VAL		: in		std_logic_Vector( 31 downto 0 );
			Pixels_Line		: in		std_Logic_Vector( 31 downto 0 );
			Lines_Frame		: in		std_logic_Vector( 31 downto 0 );
			offset			: in		std_logic_Vector( 31 downto 0 );
			OTR_Factor		: in		std_logic_Vector( 31 downto 0 );
			RTime_Thr			: in		std_logic_Vector( 31 downto 0 );		
			TLevel			: in		std_logic_Vector( (32*5)-1 downto 0 );
--			SCABB_Data 		: in		std_logic_Vector( 31 downto 0 );
--			SCABB_Wen			: in		std_logic;
--			SCABB_WA			: in		std_logic_vector( 6 downto 0 );
--			SCABB_WD			: in		std_logic_Vector( 31 downto 0 );
--			SCABB_RA			: in		std_logic_Vector( 6 downto 0 );          
--			SCABB_RD			: buffer	std_logic_Vector( 31 downto 0 );
--			SCABB_OUT			: buffer	std_logic;		
			FDisc_BB			: buffer	std_logic;
			Meas_Done			: buffer	std_logic := '0';					-- Peak Identification Done
			CPeak			: buffer	std_Logic_Vector( 11 downto 0 );		-- Peak Value
			Blevel_Upd		: buffer	std_logic := '0';					-- Baseline Level Update
			Blevel			: buffer	std_logic_Vector(  11 downto 0 );		-- Baseline Level
			Peak_Time			: buffer	std_logic_Vector( 143 downto 0 );
			Spec_Update		: buffer	std_logic := '0';
			Time_Enable		: buffer	std_logic := '0';
			Preset_FMap		: buffer	std_logic;
			Preset_SMap		: buffer	std_logic;
			evch_sel            : buffer  std_logic;
			RTime_Rej			: buffer	std_logic;
			RTime_Upd			: buffer	std_logic;
			RTime			: buffer	std_logic_Vector( 31 downto 0 );			
			Line_Code			: buffer	std_logic_Vector(  1 downto 0 );
			Pixel_Status		: buffer	std_logic_Vector( 15 downto 0 );
			Line_Status		: buffer	std_logic_Vector( 15 downto 0 );
			Frame_Status		: buffer	std_logic_Vector( 15 downto 0 );				
			Disc_Cnts			: buffer	std_logic_Vector( 63 downto 0 );
			CPS				: buffer	std_logic_vector( 31 downto 0 );		-- Input Count Rate
			NPS				: buffer	std_logic_Vector( 31 downto 0 );		-- Output Count Rate
			CTIME			: buffer	std_logic_Vector( 31 downto 0 );
			LTIME			: buffer	std_logic_Vector( 31 downto 0 );
			net_cps			: buffer	std_logic_Vector( 31 downto 0 );
			net_nps			: buffer	std_logic_Vector( 31 downto 0 );
			Disc_Fir_Out		: buffer  std_logic_Vector( (20*4)+19 downto 0 );
			Main_Dly_Data 		: buffer  std_logic_Vector( 89 downto 0 );
			Main_Out			: buffer  std_logic_Vector( 19 downto 0 );
--			Main_Out			: buffer  std_logic_Vector( 15 downto 0 );
			Main_Sum			: buffer	std_logic_Vector( 31 downto 0 );
			tp_Data			: buffer	std_logic_vector( 15 downto 0 ) );
	end component EDSBlock;
	----------------------------------------------------------------------------------------------
	--////////////////////////////////////////////////////////////////////////////////////////////
	--// DPP INTERFACE ///////////////////////////////////////////////////////////////////////////
	--////////////////////////////////////////////////////////////////////////////////////////////

	--////////////////////////////////////////////////////////////////////////////////////////////
	--// Preamp, TEC and Misc Function INTERFACE /////////////////////////////////////////////////
	--////////////////////////////////////////////////////////////////////////////////////////////
	----------------------------------------------------------------------------------------------
     component ad5920 is
          port(
               CLK50		: in		std_logic;
               val            : in		std_logic_vector( 31 downto 0 );
               pot_cs         : buffer  std_logic;          
               pot_clk        : buffer  std_logic;
               pot_dout       : buffer  std_logic );
     end component ad5920;	
     ----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	component mslide is
		generic(
			STB_SLIDE_STOP		: integer := 7;
			STB_SLIDE_MR		: integer := 8;
			STB_SLIDE_FA		: integer := 10;
			STB_SLIDE_FR		: integer := 11 );
		port(
			CLK50			: in		std_logic;
			MSlide_New_Pos		: in		std_Logic;
			Enable			: in		std_logic;
			Auto_Retract_Dis	: in		std_logic;
			ms1_tc			: in		std_logic;
			Det_Sat			: in		std_logic;
			Vel_Max			: in		std_logic_Vector( 31 downto 0 );		-- Slide Velocity
			Vel_Min			: in		std_logic_Vector( 31 downto 0 );		-- Slide Velocity
			Tar_Pos			: in		std_logic_Vector( 31 downto 0 );		-- 100% Cnt Value
			CPS				: in		std_logic_Vector( 31 downto 0 );		-- 100% Cnt Value
			HC_RATE			: in		std_logic_Vector( 31 downto 0 );		-- 100% Cnt Value
			HC_THRESHOLD		: in		std_logic_Vector( 31 downto 0 );		-- 100% Cnt Value		
			CMD_STB			: in		std_logic_Vector( 31 downto 0 );
			CMD_WD			: in		std_logic;
			CMD_RETRACT		: in		std_logic;						-- Retract due to other means
			Enc				: in		std_logic_Vector( 1 downto 0 );
			Move_Enable		: buffer	std_logic;						-- Movement in progress
			Home_Status		: buffer	std_logic;
			Move_Time			: buffer	std_logic_Vector( 31 downto 0 );
			MSlide_Dir		: buffer	std_logic;						-- Direction
			MSlide_Clk		: buffer	std_logic;						-- Steps
			MSlide_En			: buffer	std_logic;
			MSlide_Pos		: buffer	std_logic_Vector( 31 downto 0 );
			Analyze_Pos		: buffer	std_logic_Vector( 31 downto 0 );
			MSlide_State   	: buffer	std_logic_vector(  4 downto 0 )); 		-- RTEM State
	end component mslide;
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	component PreampCtrl is
		port(
			CLK50		: in		std_logic;
			Pot_Val		: in		std_logic_vector( 31 downto 0 );
			Det_Ready		: in		std_logic;
			ms1_tc		: in		std_logic;
			Ext_Enable	: in		std_logic;					-- Interlock from External Source - Must be enabled		
			CMD_WD		: in      std_logic;
			i_SDOUT		: in		std_logic;		
			Det_Sat		: in		std_logic;
			TEC1_Factor	: in		std_logic_Vector( 31 downto 0 );
			TEC2_DELAY	: in		std_logic_Vector( 31 downto 0 );
			DPP_Temp		: in		std_logic_Vector( 31 downto 0 );	-- Only use lower 16-bits and divide by 256 for degrees C		
			HV_CWORD		: in		std_logic_vector( 31 downto 0 );
			PA_TEMP_TARGET	: in		std_logic_vector( 31 downto 0 );
			PA_TEMP_OFFSET	: in		std_logic_vector( 31 downto 0 );
			PA_TEC2_KIKP	: in		std_logic_vector( 31 downto 0 );
			o_SCLK		: buffer	std_logic;
			o_nAEN		: buffer	std_logic;
			o_nDEN		: buffer	std_logic;
			o_SDIN		: buffer	std_logic;
			o_SPARE		: buffer	std_logic;
			LTC1867_Data	: buffer	std_logic_Vector( 127 downto 0 );
			PA_TEMP		: buffer	std_logic_Vector( 31 downto 0 );
			En_Vent		: buffer	std_logic;
			En_Almost_Ready: buffer  std_logic;
			En_Ready		: buffer	std_logic;		
			Force_Reset	: buffer	std_logic;
			Enable_Reset	: buffer	std_logic;
			TEC_Ver_Info   : buffer  std_logic_Vector( 31 downto 0 ):= (others=>'0');
			Standby        : buffer  std_logic;
			Status_LED1	: buffer	std_logic_Vector( 1 downto 0 );
			HV_Mr		: buffer	std_logic;
			HV_CLK		: buffer	std_logic;					-- Clock to HV Generator - Constant Frequency
			HV_State_Code	: buffer	std_logic_Vector( 4 downto 0 );
			TEC_Current	: buffer	std_logic_Vector( 31 downto 0 ) );
	end component PreampCtrl;
	---------------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------------
	component Tmp05 is 
		port( 
			CLK50         	: in      std_logic;                         -- Master Clock
			TMP_SENSE      : in      std_logic;	                       -- Pipelined   
			TMP_DATA		: buffer	std_logic_Vector( 31 downto 0 ) );
	end component Tmp05;
	---------------------------------------------------------------------------------------------------
	

	--////////////////////////////////////////////////////////////////////////////////////////////
	--// Preamp, TEC and Misc Function INTERFACE /////////////////////////////////////////////////
	--////////////////////////////////////////////////////////////////////////////////////////////

	begin
		-- Copy all of the Data structure to a bit 
		RO_Param_RData_Gen_B : for i in iADR_ALTERA_VER to iADR_RO_MAX generate
			Param_Bit_RData( (32*i)+31 downto 32*i) <= Param_RData(i);
		end generate;
		-- Copy contents of Control word to seperate variable
		CWord 	<= Param_WData( iADR_CWord );
		ATCWord 	<= Param_WData( iADR_ATCWord );
		Dss_Reg 	<= Param_WData( iADR_DSS_REG );
		ESync_Reg	<= Param_WData( iADR_ESYNC_REG );
		
		Gap_Time_Inst : lpm_add_Sub
			generic map(
				LPM_WIDTH			=> 8,
				LPM_REPRESENTATION 	=> "UNSIGNED",
				LPM_DIRECTION		=> "Add" )
			port map(
				Cin				=> '0',
				dataa			=> ATCWord( ATCW_GapTime_MSB downto ATCW_GapTime_LSB ),
				datab			=> Conv_Std_logic_vector( 2, 8 ),
				result			=> Gap_Time );
		
		version  <= ext( conv_std_logic_vector( VER_YEAR, 8 )
					& conv_std_logic_vector( VER_MON, 4 )
					& conv_std_logic_vector( VER_DAY, 5 )
					& conv_std_logic_vector( VER_APP_MAJOR, 4 )
                         & conv_std_logic_Vector( VER_APP_MINOR, 4 )                         
					& conv_std_logic_Vector( VER_MAJOR, 4 )
					& conv_std_logic_vector( VER_MINOR, 4 ), 32 );					
	
		-- Convert Paramters for Threshold Level to a long vector
		Tlevel_Gen : for i in 0 to 4 generate
			TLevel( (32*i)+31 downto 32*i ) <=  Param_WData( iADR_DISC_0 + i );			
		end generate;
		
		-- READ BACK PARAMETERS -----------------------------------------------------------------

		-- Parameter Read-Back DAta
		Param_RData( iADR_ALTERA_VER )     <= version;						-- Firmware Version
		Param_RData( iADR_STATUS  )        <= Status;						-- DSS Static Parameters		

		-- Disc Counts for 3 2 1 and 0
		Param_RData( iADR_DCnts_0 )        <= Disc_Cnts( 31 downto  0 );
		Param_RData( iADR_DCnts_1 )        <= Disc_Cnts( 63 downto 32 );

		-- Discriminator Peaking Times
		Param_RData( iADR_PTIME_DT0 )      <= x"0" & Peak_Time(  23 downto  12 ) & x"0" & Peak_Time( 11 downto  0 );		-- Disc 1 & 0
		Param_RData( iADR_PTIME_DT1 )      <= x"0" & Peak_Time(  47 downto  36 ) & x"0" & Peak_Time( 35 downto 24 );		-- Disc 3 & 2
		
		-- PeakTime Reg 4 contains the main channel Peaking Time, but also has the AmpTime starting at bit 16
		Param_RData( iADR_PTIME_AT0 )      <= x"0" & Peak_Time(  71 downto  60 ) & x"0" & Peak_Time(  59 downto  48 );	-- AmpTime 1 & 0
		Param_RData( iADR_PTIME_AT1 )      <= x"0" & Peak_Time(  95 downto  84 ) & x"0" & Peak_Time(  83 downto  72 );	-- AmpTime 3 & 2
		Param_RData( iADR_PTIME_AT2 )      <= x"0" & Peak_Time( 119 downto 108 ) & x"0" & Peak_Time( 107 downto  96 );	-- AmpTime 5 & 4
		Param_RData( iADR_PTIME_AT3 )      <= x"0" & Peak_Time( 143 downto 132 ) & x"0" & Peak_Time( 131 downto 120 );	-- AmpTime 7 & 6

		Param_Rdata(iADR_INDICATOR_REG)    <= Indicator_Reg;

		-- READ BACK PARAMETERS -----------------------------------------------------------------

		-----------------------------------------------------------------------------------------
		clock_gen_inst : clock_gen
			port map(
				inclk0 		=> i_ADC_100,
				c0			=> clk100 );
		-----------------------------------------------------------------------------------------

		-----------------------------------------------------------------------------------------
		dsp_clk_buf_inst : dsp_clk_buf 
			port map(
				inclk0		=> i_Mity_Clk, 
				c0			=> CLK50 );
		-----------------------------------------------------------------------------------------
	--////////////////////////////////////////////////////////////////////////////////////////////
	--// MITYDSP INTERFACE ///////////////////////////////////////////////////////////////////////
	--////////////////////////////////////////////////////////////////////////////////////////////
		---------------------------------------------------------------------------------------
		-- Fifo from Xilinx to Altera (DSP Writes the data )
		Dsp_Write_Data_Proc : process( Clk50 )
		begin
			if rising_Edge( clk50 ) then
				Dsp_FF_Write_En		<= not( i_DSP_WRITE_BUSY_BAR );
				Dsp_FF_Write_Data(0) 	<= io_DSP_Data(0);
				Dsp_FF_Write_Data(32) 	<= io_DSP_Data(32);
				for i in 1 to 31 loop
					Dsp_FF_Write_Data(i)	<= io_DSP_Data(i) XOR io_DSP_Data(0);
					-- Dsp_Write_Data(i) <= io_Dsp_Data(i);
				end loop;
			end if;
		end process;
		---------------------------------------------------------------------------------------
		Dsp_Write_Fifo_Inst : Dsp_Write_Fifo
			port map(
				sclr			=> Cmd_Stb( Stb_Fifo_Mr ),	
				clock		=> Clk50,				-- Write from Xilinx
				wrreq		=> Dsp_FF_Write_En, 		-- not( i_DSP_WRITE_BUSY_BAR ),
				data			=> Dsp_FF_Write_Data, 		-- io_DSP_Data,
				full		     => o_DSP_WFF_FF,
				rdreq		=> Dsp_Write_FF_Ren,
				empty		=> Dsp_Write_FF_EF, 
				q			=> Dsp_Write_FF_RData );
		-- Fifo from Xilinx to Altera (DSP Writes the data )
		---------------------------------------------------------------------------------------

		---------------------------------------------------------------------------------------
		Dsp_Read_Fifo_Inst : Dsp_Read_fifo
			port map(
				aclr			=> Cmd_Stb( Stb_Fifo_Mr ),	
				wrclk		=> Clk100,						-- Written by Altera
				wrreq		=> Dsp_Read_FF_Wen,
				data			=> Dsp_Read_FF_WData,
				wrfull		=> Dsp_Read_FF_FF,
				
				rdclk		=> CLK50,						-- Read by Xilinx
				rdreq		=> Dsp_Read_FF_Ren,
				rdempty		=> Dsp_Read_FF_EF,
				wrusedw		=> Dsp_Read_FF_Cnt,
				q			=> Dsp_Read_FF_Data_Out );
		---------------------------------------------------------------------------------------
          
		---------------------------------------------------------------------------------------
          -- Resync DSP_READ_FF_EF to the 100Mhz clock
          -- for use in DSP_DATA_GEN under the DSS Control. This will generate a timing violation w/o the resunc
          DSP_Read_FF_Flags_Proc : process( CLK100 )
          begin
               if( rising_edge( CLK100 )) then
                    Dsp_Read_FF_EF_C100 <= Dsp_Read_FF_EF;     	-- Only used for DSS to Pause when the fifo is full and wait for it to be empty before resuming   
               end if;
          end process;
		---------------------------------------------------------------------------------------
          
		---------------------------------------------------------------------------------------
		-- This is the data being read by the Xilinx FPGA. i_Mity_CLK is the clock fro the Xilinx FPGA
		-- and is used to "Resync" the data to be read. Additionally the falling edge is used, since the 
		-- Xilinx FPGA uses the rising edge to clock teh data.
		i_Mity_Clk_proc : process( CLK50 )
		begin				
			if( rising_edge( CLK50 )) then
				if( Dsp_Write_En = '1' )
					then Param_WData( conv_integer( Dsp_Write_Addr( 5 downto 0 ))) 	<= Dsp_Write_FF_RData( 31 downto 0 );
				end if;

				if(( Dsp_Write_En = '1' ) and ( conv_integer( Dsp_Write_Addr( 5 downto 0 )) = iADR_MSlide_Tar ))
					then MSlide_New_Pos <= '1';
					else MSlide_New_Pos <= '0';
				end if;
				
				-- if Read Strobe - latch read parameter @ dsp_read_Addr
				if( Dsp_Read_Addr(15) = '0' ) then
					if( Dsp_Read_Addr(6) = '0' )
						then Param_Reg <= Param_WData( conv_integer( Dsp_Read_Addr( 5 downto 0 ) ));		-- Write Parameters
						else Param_Reg <= Param_RData( conv_integer( Dsp_Read_Addr( 6 downto 0 ) ));		-- Read Parameters
					end if;
--				else
--					Param_Reg <= SCABB_RD;
				end if;
                    
                    if( Dsp_Read_Busy = '1' ) 
					then o_DSP_Read_Busy_Bar <= '0';
				elsif( Dsp_Read_FF_Ren = '1' )
					then o_DSP_Read_Busy_Bar <= '0';
					else o_DSP_Read_Busy_Bar <= '1';
				end if;
					
                    if( Dsp_Oen = '0' )
                         then io_dsp_data <= (others=>'Z');
                    elsif( dsp_read_busy = '1' ) then
                         io_dsp_data(32) 	<= '0';
					io_dsp_data(0)		<= Param_Reg(0);
					-- Randomize bits 31 to 1 by XOR with bit 0
					for i in 1 to 31 loop
						io_dsp_data(i) <= Param_Reg(i) XOR Param_Reg(0);
--						io_dsp_data(i) <= Param_Reg(i);
					end loop;
				else io_dsp_data(32) 	<= '1';
					io_dsp_data(0)		<= Dsp_Read_FF_Data_Out(0);
					-- Randomize bits 31 to 1 by XOR with bit 0
					for i in 1 to 31 loop
						io_dsp_data(i) <= Dsp_Read_FF_Data_Out(i) XOR Dsp_Read_FF_Data_Out(0);
--						io_dsp_data(i) <= Dsp_Read_FF_Data_Out(i);	
					end loop;
						
				
                    end if;                    				
			end if;
		end process;
		---------------------------------------------------------------------------------------

          ---------------------------------------------------------------------------------------
		Dsp_Data_Gen_Inst : Dsp_Data_Gen
			generic map(
				STB_TIME_CLR 		=> Stb_Time_Clr,
				STB_DSS_START		=> STB_DSS_START,
				STB_DSS_STOP		=> STB_DSS_STOP )
			port map(
				clk100			=> clk100,
				Blevel_En			=> BLevel_En,		
				Spc_Test_En		=> ESync_Reg(ESCW_SPC_TEST),				
				dss_reg			=> Param_WData( iADR_DSS_REG ),
				dss_interval		=> Param_WData( iADR_DSS_INT ),
				dss_cnt_max		=> Param_WData( iADR_DSS_MAX ),
				dss_data			=> Dss_Data,
				dss_trigger		=> Dss_Trigger,
				Line_Code			=> Map_Line_Code,
				Pixel_Status		=> Map_Pixel_Status,
				Line_Status		=> Map_Line_Status,
				Frame_Status		=> Map_Frame_Status,
				Spec_Update		=> Spec_Update,
				Time_Enable		=> Time_Enable,
				Read_Data_En		=> Read_Data_En,
				Preset_FMap		=> Preset_Fmap,
				Preset_SMap		=> Preset_Smap,
				MCA_Data			=> MCA_Data,
				Blevel_Data		=> Blevel_Data,
				RDATA			=> Param_Bit_RData,				-- All of the Read Parameter Data
				CMD_STB			=> Cmd_Stb,
				Read_FF_EF		=> Dsp_Read_FF_EF_C100,	-- Only used for DSS to Pause when the fifo is full and wait for it to be empty before resuming   
				Read_FF_FF		=> Dsp_Read_FF_FF,
				Spec_Buf_Full		=> Spec_Buf_Full,
				Dss_En			=> Dss_en,
				Time_Disable_Latch	=> Time_Disable_Latch,
				Time_Enable_Latch	=> Time_Enable_Latch,				
				Start_MCA_Update	=> Start_MCA_Update,
				Start_Blevel_Update	=> Start_Blevel_Update,
				Dpp_Busy			=> Dpp_Busy,
				DMA_Read_Trig		=> DMA_Read_Trig,
				Read_FF_Wen		=> Dsp_Read_FF_Wen,
				Read_FF_WData		=> Dsp_Read_FF_WData,		
				FFR_State_Code		=> Dsp_Data_Gen_FFR_Code );
		---------------------------------------------------------------------------------------

		----------------------------------------------------------------------------------------------
		Meas_Done_Mux 	<= RTime_Upd 			when ( not( ATCWord( ATCW_GapTime_MSB downto ATCW_GapTime_LSB )) = 0 ) else Meas_Done;
		Cpeak_Mux		<= RTime( 11 downto 0 )	when ( not( ATCWord( ATCW_GapTime_MSB downto ATCW_GapTime_LSB )) = 0 ) else CPeak;
		MCA_Inst : MCA
			port map(
				clk100			=> clk100,
				Dss_Enable		=> Dss_En,
				Time_Enable		=> Time_Enable,				
				Spec_Clear		=> Cmd_Stb( STB_TIME_CLR ),				
				Spec_Update		=> Start_MCA_Update,				
				Meas_Done			=> Meas_Done_Mux,
				Cpeak			=> Cpeak_Mux,						
--				Meas_Done			=> Meas_Done,				
--				Cpeak			=> Cpeak,								
				CLIP_MAXMIN		=> Param_WData( iADR_CLIP ),				
				CPeak_Max_Min		=> Param_RData( iADR_CPeak_MaxMin ),
				MCA_Data			=> MCA_Data );
		----------------------------------------------------------------------------------------------

		----------------------------------------------------------------------------------------------
		BMCA_Inst : BMCA
			port map(
				clk100			=> clk100,
				Dss_Enable		=> Dss_En,				
				Spec_Clear		=> Cmd_Stb( STB_TIME_CLR ),				
				Spec_Update		=> Start_Blevel_Update,				
				Blevel_upd		=> Blevel_Upd,
				Blevel			=> Blevel,								
				Blevel_Data		=> Blevel_Data );
		----------------------------------------------------------------------------------------------

		---------------------------------------------------------------------------------------
		-- Fifo from Altera to Xilinx (DSP Reads the data )
		MityDSP_Inst : MityDSP
			generic map(
				-- Strobe bit positions ----------------------------------------------------------------------
				STB_TIME_START 	=> Stb_time_Start,
				STB_TIME_STOP 		=> Stb_Time_Stop,
				STB_TIME_CLR 		=> Stb_Time_Clr,
				STB_FIR_MR		=> Stb_fir_Mr,
				STB_FIFO_MR		=> Stb_Fifo_Mr,
				STB_READ_TRIG		=> Stb_Read_Trig,
				STB_SLIDE_STOP		=> Stb_Slide_Stop,
				STB_SLIDE_MR		=> Stb_Slide_Mr,
				STB_SLIDE_POS		=> Stb_Slide_Pos,
				STB_SLIDE_FA		=> Stb_Slide_FA,				
				STB_SLIDE_FR		=> STB_SLIDE_FR,				
				STB_DSS_START		=> STB_DSS_START,
				STB_DSS_STOP		=> STB_DSS_STOP,		
				STB_TIME_RESET		=> STB_TIME_RESET,
				iADR_RW_MAX		=> iADR_RW_MAX )
			port map(
				clk100			=> clk100,
				clk50			=> CLK50,				
				DSP_RFF_FF		=> i_DSP_RFF_FF, 
				Read_FF_EF		=> Dsp_Read_FF_EF,
				Read_FF_FF		=> Dsp_Read_FF_FF,
				DMA_Read_Trig		=> DMA_Read_Trig,
				Read_FF_Cnt		=> Dsp_Read_FF_Cnt,				
				DSP_Write_Busy		=> not( i_DSP_WRITE_BUSY_BAR ),			
				DSP_AInt_Busy		=> i_Dsp_Altera_Int_Busy,
				MCA_Data			=> MCA_Data,
				BMCA_Data			=> Blevel_Data,
				Dsp_Write_FF_EF	=> Dsp_Write_FF_EF,			-- Fifo Empty Flag
				Dsp_Write_FF_RData	=> Dsp_Write_FF_RData,		-- Fifo Read DAta
				Dsp_Write_FF_Ren	=> Dsp_Write_FF_Ren,		-- Fifo Read Enable
				Dsp_Write_Addr		=> Dsp_Write_Addr,			
				Dsp_Write_En		=> Dsp_Write_En,											
				Dsp_Read_Busy		=> Dsp_Read_Busy,
				o_DSP_INT			=> o_DSP_Irq_Bar,
				DSP_OEN			=> Dsp_Oen,
				Read_FF_Ren		=> Dsp_Read_FF_Ren,
				Spec_Buf_Full 		=> Spec_Buf_Full,
				Blevel_En			=> Blevel_En,
				Read_Data_En		=> Read_Data_En,
--				SCABB_WEN			=> SCABB_WEN,
				Dsp_Read_Addr		=> Dsp_Read_Addr,				
				CMD_STB			=> CMD_STB,
				AmpTimeReg		=> AmpTimeReg );
		-- Fifo from Altera to Xilinx (DSP Reads the data )				
		-----------------------------------------------------------------------------------------

	--////////////////////////////////////////////////////////////////////////////////////////////
	--// MITYDSP INTERFACE ///////////////////////////////////////////////////////////////////////
	--////////////////////////////////////////////////////////////////////////////////////////////

	--////////////////////////////////////////////////////////////////////////////////////////////
	--// DPP INTERFACE ///////////////////////////////////////////////////////////////////////////
	--////////////////////////////////////////////////////////////////////////////////////////////
		---------------------------------------------------------------------------------------
		Bit_Gen_Inst : Bit_Gen
			port map(
				CLK100			=> clk100,
				BIT_EN			=> CWord( CW_BIT_GEN ),		-- Enable Bit Generate 
				peak     			=> Param_WData( iADR_BIT_PEAK ),
				timing  	 		=> Param_WData( iADR_BIT_TIME  ),
				BIT_VAL			=> Bit_Val,
				Reset			=> Bit_Reset );
		---------------------------------------------------------------------------------------

		--------------------------------------------------------------------------------------						
		-- High Speed A/D Converter (Must stay at 100Mhz)
		LTC2207_INST : LTC2207
			port map(
				CLK100			=> clk100,
				DIN				=> i_ADC_DATA, 
				PA_Reset			=> Reset_Out,
				Rand				=> o_ADC_RAND, 
				OTR				=> i_ADC_OTR,
				Half_Gain			=> CWord( CW_HALF_GAIN ),				
				Gain				=> Param_WData( iADR_GAIN ),	-- Parameter Write Data
				Raw_Dout			=> ltc2207_raw,
				DOUT				=> ltc2207_out,			-- Signed output
				DoutM			=> ltc2207_outM );			-- With Multiplication				

		o_ADC_DITHER 		<= '1';
		o_ADC_PGA			<= CWord( CW_PGA_ENABLE ); 			-- '1';							-- Set to 1 for Rev C DPP Board
		o_ADC_RAND		<= '1';							-- Forced to enabled 
--		o_ADC_RAND		<= CWord( CW_ADC_RAND);
		 -- High Speed A/D Converter
		---------------------------------------------------------------------------------------

		---------------------------------------------------------------------------------------
--		WeinerFilter_Inst : WeinerFilter
--			port map(
--				CLK100			=> clk100,
--				CoefA			=> Param_WData( iADR_WF_CoefA ),
--				CoefB			=> Param_WData( iADR_WF_CoefB ),
--				CoefC			=> Param_WData( iADR_WF_CoefC ),
--				Delay			=> Param_WData( iADR_WF_Delay ),
--				DIN				=> ltc2207_outm,
--				DOUT				=> ltc2207_weiner );				
		---------------------------------------------------------------------------------------
		
		---------------------------------------------------------------------------------------
		Det_Ctrl_Inst : Det_Ctrl
			port map(
				clk100			=> clk100,
				AD_OTR			=> i_ADC_OTR, 					-- A/D Out-of-Range 
                    force_reset         => PACtrl_Force_Reset,
				Reset_Enable		=> PACtrl_Enable_Reset,
				Det_Cold			=> PACtrl_En_Ready,
				Reset_PW			=> Param_WData( iADR_Reset_PW ),
				Din				=> ltc2207_raw,				-- Raw A/D Data Output
				HV_CWORD			=> Param_WData( iADR_HV_CTRL ),	-- Manually turn on/off voltages (debugging)
				PA_MaxMin			=> Param_RData( iADR_RAMP 	 ),
				Reset_Width		=> Param_RData( iADR_RST_TIME  ),
				Reset_Period		=> Param_RData( iADR_RST_PER 	 ),
                    o_Reset_Det         => o_DET_RESET,		
				Reset_Out			=> Reset_Out,
                    Reset_Grn_LED       => Reset_Grn_LED,
				Det_Sat			=> Det_Sat,
				Det_Ready			=> Det_Ready );
								
--		Det_Reset_Det	<= 	Reset_Out      when ( CWord( CW_BIT_GEN ) = '0' ) else Bit_Reset;
--		DADC_DATA 	<=  	ltc2207_out    when ( CWord( CW_BIT_GEN ) = '0' ) else sxt( Bit_Val, 18 );

		Det_Reset_Det	<= 	Bit_Reset 		when ( CWord( CW_BIT_GEN 	) = '1' ) else Reset_Out;
		DADC_DATA 	<=  	sxt( Bit_Val, 18 ) 	when ( CWord( CW_BIT_GEN 	) = '1' ) else ltc2207_out;
		FADC_DATA 	<=  	sxt( Bit_Val, 18 )	when ( CWord( CW_BIT_GEN 	) = '1' ) else ltc2207_outM;
--						ltc2207_weiner		when ( CWord( CW_WEINER_EN 	) = '1' ) else
						
		

		-----------------------------------------------------------------------------------------

		-----------------------------------------------------------------------------------------
		EDS_GEN : if(  Gen_EDS = 1 ) generate
			EDS_Block_Inst : EDSBlock
				generic map(
				-- Strobe bit positions -------------------------------------------------------
					STB_TIME_START 	=> Stb_time_Start,
					STB_TIME_STOP 		=> Stb_Time_Stop,
					STB_TIME_CLR 		=> Stb_Time_Clr,
					STB_TIME_RESET		=> Stb_TIME_Reset,
					STB_FIR_MR		=> Stb_fir_Mr,
					ATCW_BLR_LSB		=> ATCW_BLR_LSB,
					ATCW_BLR_MSB		=> ATCW_BLR_MSB,
					ATCW_Disc_Dis_40	=> ATCW_DISC_Dis_40,
					ATCW_Disc_Dis_160	=> ATCW_Disc_Dis_160,
					ATCW_Disc_Dis_320	=> ATCW_Disc_Dis_320,
					ATCW_Disc_Dis_640	=> ATCW_Disc_Dis_640,
					ATCW_Disc_Dis_BLM	=> ATCW_Disc_Dis_BLM,
					CW_AVG_ENABLE		=> CW_AVG_ENABLE,
					CW_CPS_RESET_INH	=> CW_CPS_RESET_INH, 
					CW_PRESET_MODE_L	=> CW_Preset_Mode_L,
					CW_PRESET_MODE_H	=> CW_Preset_Mode_H )					
				port map(
					clk100			=> clk100,
					ext_Trig			=> i_ESync_In0, 
					Fast_ad			=> FADC_DATA,
					Disc_ad			=> DADC_DATA,
					CMD_STB			=> CMD_STB,
					Gap_Time			=> Gap_Time,
					PA_Reset_Det		=> Det_Reset_Det,						-- Reset Detection
					AmpTime			=> AmpTimeReg,
					Blevel_En			=> Blevel_En,
					ATCWord			=> ATCWord,
					CWord			=> CWord, 							-- Control
					Clip_MaxMin 		=> Param_WData( iADR_CLIP ),				-- Clip Max/Min
					PRESET_VAL		=> Param_WData( iADR_PRESET ),			-- Preset Value
					Pixels_Line		=> Param_WData( iADR_Pixels_Line ),		-- Pixels / Frame
					Lines_Frame		=> Param_WData( iADR_Lines_Frame ),		-- Total Frames			
					offset			=> Param_WData( iADR_OFFSET ),			-- Offset
					RTime_Thr			=> Param_WData( iADR_RTIME_THR ),
					OTR_Factor		=> Param_WData( iADR_OTR_FACTOR ),			-- Out-of-Range Factor
					TLevel			=> Tlevel,
--					SCABB_Data 		=> Param_WData( iADR_SCABB ),
--					SCABB_Wen			=> SCABB_Wen,
--					SCABB_WA			=> Dsp_Write_Addr( 14 downto 8 ),
--					SCABB_WD			=> Param_WData( iADR_SCABB ), -- Dsp_Write_FF_RData( 31 downto 0 ),
--					SCABB_RA			=> Dsp_Read_Addr( 6 downto 0  ),		
--					SCABB_RD			=> SCABB_RD,
--					SCABB_OUT			=> SCABB_Out,
					FDisc_BB			=> FDisc_BB,
					Meas_Done			=> MEas_Done,
					CPeak			=> CPeak,
					Blevel_Upd		=> Blevel_Upd,
					Blevel			=> Blevel,
					Peak_Time			=> Peak_Time,
					Spec_Update		=> Spec_Update,
					Time_Enable		=> Time_Enable,
					Preset_FMap		=> Preset_FMap,
					Preset_SMap		=> Preset_SMap,
                         evch_sel            => o_EvCh_Sel,                         					
					RTime_Rej			=> RTime_Rej,
					RTime_Upd			=> RTime_Upd,
					RTime			=> RTime,					
					Line_Code			=> Map_Line_Code,
					Pixel_Status		=> Map_Pixel_Status,
					Line_Status		=> Map_Line_Status,
					Frame_Status		=> Map_Frame_Status,		
					Disc_Cnts			=> Disc_Cnts,								-- Threshold Counts
					CPS				=> Param_RData( iADR_CPS   ),					-- Input Count Rate
					NPS				=> Param_RData( iADR_NPS   ),					-- Output Count Rate
					CTIME			=> Param_RData( iADR_CTIME ),					-- Clock Time
					LTIME			=> Param_RData( iADR_LTIME ),					-- Live Time
					net_cps			=> Param_RData( iADR_NCPS  ),					-- Net Input Count 
					net_nps			=> Param_RData( iADR_NNPS  ),					-- Net Store Counts
					Disc_Fir_Out		=> Disc_Fir_Out,
					Main_Dly_Data 		=> Main_Dly_Data,
					Main_Out			=> Main_Out,
					Main_Sum			=> Main_Sum,
					tp_data			=> tp_Data );
		end generate;

		EDS_SKIP: if( Gen_EDS = 0 ) generate
			MEas_Done 		<= '0';
			Blevel_Upd 		<= '0';
			Time_Enable 		<= '0';
			Preset_FMap		<= '0';
			Preset_SMap		<= '0';			
			Spec_Update		<= '1' when ( ms1_tc = '1' ) and ( ms100_cnt = 100 ) else '0';
			Map_Line_Code		<= (others=>'0');
			Map_Pixel_Status	<= (others=>'0');
			Map_Line_Status	<= (others=>'0');
			Map_Frame_Status	<= (others=>'0');			
				
			Peak_Time(  59 downto  48) 	<= Conv_Std_logic_Vector( 12,  12 ); -- 0
			Peak_Time(  71 downto  60) 	<= Conv_Std_logic_Vector( 24,  12 ); -- 1
			Peak_Time(  83 downto  72) 	<= Conv_Std_logic_Vector( 48,  12 ); -- 2
			Peak_Time(  95 downto  84) 	<= Conv_Std_logic_Vector( 96,  12 ); -- 3
			Peak_Time( 107 downto  96) 	<= Conv_Std_logic_Vector( 192, 12 ); -- 4
			Peak_Time( 119 downto 108) 	<= Conv_Std_logic_Vector( 384, 12 ); -- 5
			Peak_Time( 131 downto 120) 	<= Conv_Std_logic_Vector( 768, 12 ); -- 6
			Peak_Time( 143 downto 132) 	<= (others=>'1');				  --All 1's - Not used
		end generate;
		 -- DPP Related Signals		
		-----------------------------------------------------------------------------------------

	--////////////////////////////////////////////////////////////////////////////////////////////
	--// DPP INTERFACE ///////////////////////////////////////////////////////////////////////////
	--////////////////////////////////////////////////////////////////////////////////////////////

		
	--////////////////////////////////////////////////////////////////////////////////////////////
	--// Preamp, TEC and Misc Function INTERFACE /////////////////////////////////////////////////
	--////////////////////////////////////////////////////////////////////////////////////////////
		-----------------------------------------------------------------------------------------
		-- Coarse Gain DAC ( Can operate at 50Mhz)
          ad5920_INST : ad5920
               port map(
                    CLK50               => CLK50,
				val                 => Param_WData( iADR_EVCH_GAIN ),	-- Parameter Write Data
                    pot_cs              => evch_cs,
                    pot_clk             => evch_clk,
                    pot_dout            => evch_data );
				
		-- must be inverted since output buffer was changed from '240 to '241 which is inverting
		o_evch_clk 	<= not( evch_clk );
		o_evch_cs 	<= not( evch_cs );
		o_evch_data  	<= not( evch_data );
		-- Coarse Gain DAC
		-----------------------------------------------------------------------------------------

		-----------------------------------------------------------------------------------------
		-- ( Can operate at 50Mhz)
		Slide_Gen: if( GEN_SLIDE = 1 ) generate
			mslide_inst : mslide
				generic map(
					STB_SLIDE_STOP		=> STB_SLIDE_STOP,
					STB_SLIDE_MR		=> STB_SLIDE_MR,
					STB_SLIDE_FA		=> STB_SLIDE_FA,
					STB_SLIDE_FR		=> STB_SLIDE_FR )
				port map(			
					clk50			=> CLK50,
					enable			=> CWord( CW_SLIDE_EN ),
					MSlide_New_Pos		=> MSlide_New_Pos,
					Auto_Retract_Dis	=> CWord( CW_Auto_Retract_Disable ),
					ms1_tc			=> ms1_tc,
					Det_Sat			=> Det_Sat, 
					Vel_Max			=> Param_WData( iADR_MSlide_Vel_Max ),
					Vel_Min			=> Param_WData( iADR_MSlide_Vel_Min ),
					Tar_Pos			=> Param_WData( iADR_MSlide_Tar ),
					CPS				=> Param_RData( iADR_CPS ),					-- Input Count Rate
					HC_RATE			=> Param_WData( iADR_SLIDE_HCR ),
					HC_THRESHOLD		=> Param_WData( iADR_SLIDE_HCT ),
					CMD_STB			=> Cmd_Stb,								-- Stop any movements		 
					CMD_WD			=> WDog_Trig, 								-- Watch Dog Trigger
					CMD_RETRACT		=> Ext_Retract,
					enc				=> i_mout_sw & i_min_sw,
					Move_Enable		=> Slide_Move_Enable,
					Home_Status		=> Slide_Home_Status,
					Move_Time			=> Param_RData( iADR_SLIDE_MOVE_TIME ),			-- Move Time in msec
					MSlide_Dir		=> o_MDir,								-- Direction
					MSlide_Clk		=> o_MStep,								-- Steps
					MSlide_En			=> o_Men,
					MSlide_Pos		=> Param_RData( iADR_MSlide_Pos ),				-- Current Position
					Analyze_Pos		=> Param_RData( iADR_SLIDE_ANALYZE_POS ),
					MSlide_State   	=> Slide_State );							-- State of MSlide Logic				 
		end generate;
		
		Slide_No_Gen : if( GEN_SLIDE = 0 ) generate
			Slide_Move_Enable 					<= '0';
			Slide_Home_Status 					<= '1';
			Param_RData( iADR_SLIDE_MOVE_TIME )	<= (others=>'0');
			o_MDir							<= '0';
			o_MStep							<= '0';
			Param_RData( iADR_MSlide_Pos ) 		<= (others=>'0');
			Param_RData( iADR_SLIDE_ANALYZE_POS ) 	<= (others=>'0');
			Slide_State 						<= (others=>'0');			
			o_Men 							<= '0';
		end generate;
		--------------------------------------------------------------------------------------------

		-----------------------------------------------------------------------------------------
		-- ( Can operate at 50Mhz)
		PreampCtrl_Inst : PreampCtrl
			port map(
				clk50			=> CLK50,
				Pot_Val			=> Param_WData( iADR_POT_VAL ),	-- Parameter Write Data
				Det_Ready			=> Det_Ready,
				ms1_tc			=> ms1_tc,					-- Start an A/D Sequence every msec                    
				Ext_Enable		=> Ext_Cool_En,
                    CMD_WD              => '0', -- WDog_Trig,
				i_SDOUT			=> i_PA_SDout,					-- Output from Preamp (input to PreampCtrl)
				Det_Sat			=> Det_Sat,
				TEC1_Factor		=> Param_WData( iADR_TEC1_FACTOR ),
				TEC2_DELAY		=> Param_WData( iADR_TEC2_DELAY ),
				DPP_Temp			=> Param_RData( iADR_TMP05 ),
				HV_CWORD			=> Param_WData( iADR_HV_CTRL ),	-- Manually turn on/off voltages (debugging)
				PA_TEMP_TARGET		=> Param_WData( iADR_TARGET_ADC ),
				PA_TEMP_OFFSET		=> Param_WData( iADR_PA_Temp_Offset ),
				PA_TEC2_KIKP		=> Param_WData( iADR_PA_TEC2_KIKP ),
				o_SCLK			=> o_PA_SCLK,
				o_nAEN			=> o_PA_AEN,
				o_nDEN			=> o_PA_DEN,
				o_SDIN			=> o_PA_SDIN,
--				o_SPARE			=> o_PA_SPARE,
				LTC1867_Data		=> LTC1876_Data,
				PA_TEMP			=> Param_RData( iADR_PA_Temp ),
				En_Vent			=> En_Vent,
				En_Almost_Ready	=> En_Almost_Ready,				
				En_Ready			=> PACtrl_En_Ready,
				Force_Reset		=> PACtrl_Force_Reset,
				Enable_Reset		=> PACtrl_Enable_Reset,
                    TEC_Ver_Info        => Param_RData( iADR_TEC_VER ),
                    Standby             => Standby,
				Status_LED1		=> Status_LED1,
				HV_MR			=> o_PA_HV_EN_MR,				-- Preamp HV Master Reset ( Shuts off everything)
 				HV_CLK			=> o_PA_HV_CLK,
				HV_State_Code		=> HV_State_Code,								
				TEC_Current		=> Param_RData( iADR_TEC_Current ) );							
		-- Copy the LTC1876 Data into the Array of Vectors
		LTC1876_Data_Gen : for i in 0 to 3 generate
			Param_RData( iADR_PADC10+i ) <= LTC1876_Data( (32*i)+31 downto 32*i );
		end generate;
		-----------------------------------------------------------------------------------------

		-----------------------------------------------------------------------------------------
		-- Interface to Analog Devices Temperature Sensor (Does not have to run at 100Mhz)
		Temp_Gen : if( GEN_TEMP = 1 ) generate
			tmp05_Inst : Tmp05
				port map(
					clk50			=> CLK50,
					TMP_SENSE			=> i_TMP_SENSE,				
					TMP_DATA			=> Param_RData( iADR_TMP05 ) );
		end generate;
		
		Temp_No_Gen : if( GEN_TEMP = 0 ) generate
			Param_RData( iADR_TMP05 ) <= (others=>'0');
		end generate;
		-----------------------------------------------------------------------------------------				
	--////////////////////////////////////////////////////////////////////////////////////////////
	--// Preamp, TEC and Misc Function INTERFACE /////////////////////////////////////////////////
	--////////////////////////////////////////////////////////////////////////////////////////////

	ms1_tc 		<= '1' when ( ms_cnt 	= ms_cnt_max 		) else '0';
	
	---------------------------------------------------------------------------------------------
	clock100_proc : process( clk100 )
	begin
		if( rising_Edge( clk100 )) then
			--//////////////////////////////////////////////////////////////////////////////////////////////////////////////
			-- Digital Storage Scope Triggers and Data 
			dss_trigger(DTrig_None) 			<= '1';
			dss_trigger(DTrig_FDisc) 		<= FDisc_BB;		-- earlier one
			dss_trigger(DTrig_PA_Reset) 		<= Det_Reset_Det;
			dss_trigger(DTrig_Blevel_Upd)		<= Blevel_Upd;
			dss_trigger(DTrig_Profile_Start)	<= '1';
			dss_trigger(DTrig_Move_Start)		<= Slide_Move_Enable;
			

			-- All this is signed
			case conv_integer( Dss_Reg( kdss_Chan_SelA_MSB downto kdss_Chan_SelA_LSB )) is
				when 0 	 => dss_Data <= FADC_DATA( 15 downto 0 );
				when 1	 => dss_Data <= DADC_DATA( 15 downto 0 );
				when 2	 => Dss_Data <= Main_Dly_Data(  0+15 downto  0 );
				when 3	 => Dss_Data <= Main_Dly_Data( 72+15 downto 72 );
				when 4	 => dss_Data <= Disc_fir_Out( (iDisc_40 *20)+15 downto iDisc_40 *20 );
				when 5	 => dss_Data <= Disc_fir_Out( (iDIsc_160*20)+15 downto iDIsc_160*20 );
				when 6	 => dss_Data <= Disc_fir_Out( (iDIsc_320*20)+15 downto iDIsc_320*20 );
				when 7	 => dss_Data <= Disc_fir_Out( (iDIsc_640*20)+15 downto iDIsc_640*20 );		-- 640 ns
				when 8	 => dss_Data <= Disc_Fir_Out( (iDisc_Main*20)+15 downto iDisc_Main*20 );		-- BLM
				when 9 	 => dss_Data <= Main_Out( 15 downto 0 );
				when 10	 => dss_data <= sxt( Blevel, 16 );
				when 14	 => dss_Data <= Main_Sum( 15 downto 0 );
				when 15 	 => dss_data <= (others=>'0');	-- Coded in DSP_DATA_GEN to be DSS_CNT
				when 20	 => dss_Data <= Disc_fir_Out( (iDisc_40 *20)+15+1 downto (iDisc_40 *20)+1 );
				when 21	 => dss_Data <= Disc_fir_Out( (iDIsc_160*20)+15+1 downto (iDIsc_160*20)+1 );
				when 22	 => dss_Data <= Disc_fir_Out( (iDIsc_320*20)+15+1 downto (iDIsc_320*20)+1 );
				when 23	 => dss_Data <= Disc_fir_Out( (iDIsc_640*20)+15+1 downto (iDIsc_640*20)+1 );		-- 640 ns
				when 24	 => dss_Data <= Disc_Fir_Out( (iDisc_Main*20)+15+1 downto (iDisc_Main*20)+1 );		-- BLM
				when 31	 => Dss_Data <= Rtime( 15 downto 0 );
				when others=> dss_Data <= (others=>'0');
			end case;
			--//////////////////////////////////////////////////////////////////////////////////////////////////////////////

						-- Digital Test Point Selection ---------------------------------------------------------
			case conv_integer( CWord( CW_TP_SEL_MSB downto CW_TP_SEL_LSB ) ) is
				when 0 =>
--					o_TP(15)			<= '0';	
--					o_tp(14)			<= det_sat;
--					o_TP(13)			<= tp_Data(14); -- Main Reset
--					o_TP(12)			<= tp_Data(13); -- PA Busy
--					o_TP(11)			<= tp_Data(12); -- FIR Reset
--					o_TP(10) 			<= '0'; -- ApX_Reset_On;
--					o_TP(15 downto 13 ) <= (others=>'0');
--					o_TP(12) 			<= Dma_Read_Trig;
--					o_TP(11) 			<= o_DSP_IRQ_BAR;
--					o_TP(10) 			<= i_DSP_WRITE_BUSY_BAR;
--					o_TP(9) 			<= o_DSP_READ_BUSY_BAR;
--					o_TP(8) 			<= i_Dsp_Altera_Int_Busy;
					o_TP(15 downto 8 ) 	<= Dsp_Data_Gen_FFR_Code;
					o_TP(7)			<= Spec_Update; 	
					o_TP(6)			<= i_DSP_Spare(2);				-- irq_map or altera_user_io(2)
					o_TP(5)			<= i_DSP_Spare(1);				-- dma_wait or altera_user_io(1)
					o_TP(4)			<= i_DSP_Spare(0);				-- dma_req or altera_user_io(0)								
					o_TP(3)			<= Time_Enable;										
					o_TP(2) 			<= Cmd_Stb( STB_TIME_START );
					o_TP(1) 			<= Cmd_Stb( STB_TIME_STOP );
					o_TP(0) 			<= Cmd_Stb( STB_TIME_CLR );
			
--				
--					o_TP( 15 downto 8 ) <= PA_TEC2_CMD;					
--					o_TP(7)			<= o_DSP_Read_Busy_Bar;
--					o_TP(6)			<= o_DSP_Irq_Bar;					
--					o_TP(5)			<= Det_Ready;
--					o_TP(4)			<= Reset_Grn_LED;
--					o_TP(3) 			<= det_sat;
--					o_TP(2)			<= Reset_Out;
--					o_TP(1) 			<= o_DET_RESET;
--					o_TP(0)			<= PACtrl_Enable_Reset;					
--
					
				when 1=> 
					o_TP(15 downto 8 ) 	<= (others=>'0');
					o_TP(7) 			<= o_DSP_WFF_FF;
					o_TP(6) 			<= i_DSP_RFF_FF;
					o_TP(5) 			<= o_DET_RESET;
					o_TP(4) 			<= o_LED(0);
					o_TP(3) 			<= o_DSP_IRQ_BAR;
					o_TP(2) 			<= Spec_Update;
					o_TP(1) 			<= i_DSP_WRITE_BUSY_BAR;
					o_TP(0) 			<= o_DSP_READ_BUSY_BAR;
			
				when 2 =>
					o_TP( 15 downto 10 )<= (others=>'0');
					o_TP(9) 			<= i_DSP_RFF_FF;
					o_TP(8) 			<= o_DSP_WFF_FF;
					o_TP(7) 			<= o_DSP_IRQ_BAR;
					o_TP(6) 			<= i_DSP_WRITE_BUSY_BAR;
					o_TP(5) 			<= o_DSP_READ_BUSY_BAR;
					o_TP(4) 			<= i_Dsp_Altera_Int_Busy;
					o_TP(3)			<= Rtime_Rej;
					o_TP( 2 downto 0 )	<= i_DSP_SPARE;
					
				when 3 =>
					o_TP 			<= tp_Data;

				when 4 =>
					o_TP( 15 downto 11 ) <= (others=>'0');				
					o_TP(10) 			<= Det_Reset_Det;
					o_TP(9) 			<= Bit_Reset;
					o_TP(8) 			<= '0';
					o_TP(7) 			<= '0';
					o_TP(6)			<= O_ESync_Wen1;
					o_TP(5)			<= o_ESync_Out1;					
					o_TP(4) 			<= i_ESync_In0;
					o_TP(3)			<= Spec_Update;					
					o_TP(2)			<= Dpp_Busy;
					o_TP(1) 			<= Time_Enable;
					o_TP(0) 			<= Preset_FMap;
				
				when 5 =>
					o_TP( 15 downto 10 ) <= (others=>'0');
--					o_TP(10) 			<= O_ESync_Wen1;
--					o_TP(9) 			<= o_ESync_Out1;
--					o_TP(8) 			<= O_ESync_Wen2;

					o_TP(9)			<= Preset_FMap;
					o_TP(8)			<= Preset_SMap;
					
					o_TP(7)			<= CWord(CW_Preset_Mode_H );
					o_TP(6)			<= CWord( CW_Preset_Mode_L+1);		
					o_TP(5)			<= CWord(CW_Preset_Mode_L );
					
					o_TP(4)			<= WDog_Trig;
					o_TP(3) 			<= Time_Disable_Latch;
					o_TP(2) 			<= Time_Enable_Latch;
					o_TP(1) 			<= TIME_Enable;
					o_TP(0) 			<= Meas_Done;				


				when 6 =>
					o_TP(7)			<= Dpp_Busy;
					o_TP(6)			<= o_ESync_WEn2;
					o_TP(5)			<= o_ESync_Out2;
					o_TP(4)			<= i_ESync_In2;
					o_TP(3)			<= o_ESync_WEn1;		
					o_TP(2) 			<= o_ESync_Out1;
					o_TP(1)			<= i_ESync_In1;
					o_TP(0)			<= i_ESync_In0;

				
				when 7 =>
					o_TP(15 downto 11) 	<= (others=>'0');
					o_TP(10)			<= ms1_tc;
					o_TP(9)			<= i_ESync_In2;
					o_TP(8)			<= Cmd_Stb( STB_SLIDE_FA );
					o_TP(7)			<= CWord( CW_SLide_En );
					o_TP(6) 			<= o_MDir;								-- Direction
					o_TP(5) 			<= o_MStep;								-- Steps
					o_TP(4) 			<= WDog_Trig; 								-- Watch Dog Trigger
					o_TP(3) 			<= Cmd_Stb( STB_SLIDE_STOP );					-- Stop any movements		 
					o_TP(2) 			<= Cmd_Stb( STB_SLIDE_MR );					-- Slide Reset
					o_TP(1) 			<= i_mout_sw;								-- Out Switch, active high
					o_TP(0)			<= i_min_sw;								-- In Limit Switch active high
					
				when 8 =>
					o_TP(15 downto 14 ) <= (others=>'0');
					o_TP(13) 			<= evch_clk;
					o_TP(12) 			<= o_evch_clk;
					o_TP(11) 			<= evch_cs;
					o_TP(10) 			<= o_evch_cs;
					o_TP(9) 			<= evch_data;
					o_TP(8) 			<= o_evch_data;
					o_TP(7) 			<= ltc2207_raw(15);
					o_TP(6) 			<= ltc2207_raw(14);
					o_TP(5) 			<= ltc2207_raw(13);
					o_TP(4) 			<= ltc2207_raw(12);
					o_TP(3) 			<= PACtrl_Enable_Reset;
					o_TP(2)			<= i_ADC_OTR;
					o_TP(1)			<= Reset_Out;
					o_TP(0) 			<= o_DET_RESET;
					
				when 9 =>
					o_TP(15 downto 12 ) <= (others =>'0');
					o_TP(11) 			<= i_min_sw;
					o_TP(10)			<= i_mout_sw;
					o_TP(9)			<= o_MStep;
					o_TP(8)			<= o_MDir;
					o_TP( 7 downto 4 ) 	<= Slide_State( 3 downto 0 );
					o_TP(3)			<= Cmd_Stb( Stb_Slide_MR );
					o_TP(2)			<= Cmd_Stb( STB_SLIDE_FA);
					o_TP(1)			<= Cmd_Stb( STB_SLIDE_FR );					
					o_TP(0)			<= MSlide_New_Pos;
					
				when 10 =>
					o_TP(15 downto 5 )  <= (others=>'0');
					o_TP(4)			<= i_DSP_WRITE_BUSY_BAR;
					o_TP(3) 			<= i_Dsp_Altera_Int_Busy;
					o_TP(2) 			<= o_DSP_READ_BUSY_BAR;
					o_TP(1)			<= TIME_Enable;
					o_TP(0)   		<= o_DSP_IRQ_BAR;
					

				when 15 =>
					o_TP(15)			<= '0';	
					o_tp(14)			<= det_sat;
					o_TP(13)			<= tp_Data(14); -- Main Reset
					o_TP(12)			<= tp_Data(13); -- PA Busy
					o_TP(11)			<= tp_Data(12); -- FIR Reset
					o_TP(10) 			<= '0'; -- ApX_Reset_On;
					o_TP(9)			<= Time_Enable;					
					o_TP(8) 			<= Dma_Read_Trig;
					o_TP(7) 			<= o_DSP_IRQ_BAR;
					o_TP(6) 			<= i_DSP_WRITE_BUSY_BAR;
					o_TP(5) 			<= o_DSP_READ_BUSY_BAR;
					o_TP(4) 			<= i_Dsp_Altera_Int_Busy;
					o_TP(3)			<= Spec_Update; -- i_DSP_Spare(3); 	-- dma_xfer or altera_user_io(3)
					o_TP(2)			<= i_DSP_Spare(2);	-- irq_map or altera_user_io(2)
					o_TP(1)			<= i_DSP_Spare(1);	-- dma_wait or altera_user_io(1)
					o_TP(0)			<= i_DSP_Spare(0);	-- dma_req or altera_user_io(0)								

				when others =>
					o_TP 			<= (others=>'0');
				end case;			

		end if;
	end process;
	---------------------------------------------------------------------------------------------
	
	---------------------------------------------------------------------------------------------
	CLK50_proc : process( CLK50 )
	begin
		if rising_edge( CLK50 ) then
               if( PACtrl_Enable_Reset = '1' ) 
                    then o_Reset_En <= '0';
                    else o_Reset_En <= '1';
               end if;
          
			if( ms1_tc = '1' ) 
				then ms_Cnt <= 0;
				else ms_cnt <= ms_cnt + 1;
			end if;					
			
			if( ms1_tc = '1' ) then
				if( ms100_cnt = 100 )
					then ms100_cnt <= 0;
					else ms100_cnt <= ms100_cnt + 1;
				end if;
			end if;
			
			
			Dsp_Int_Del <= o_DSP_Irq_Bar;

			-- Watch-Dog Timer
			-- If DSP is connected, reset watch-dog timer
			if(( o_DSP_Irq_Bar = '1' ) and ( Dsp_Int_Del = '0' )) then
				WDog_Cnt		<= 0;
				WDog_Trig		<= '0';
			elsif( o_DSP_READ_BUSY_BAR = '0' ) then
				WDog_Cnt		<= 0;
				WDog_Trig		<= '0';				
			-- else increment timer every 100 ms.
			elsif( ms1_tc = '1' ) then
				if( WDog_Cnt >= WDog_Cnt_Max ) then
					WDog_Cnt 	<= 0;
					WDog_Trig <= '1';
				else 
					WDog_Cnt 	<= WDog_Cnt + 1;
					WDog_Trig <= '0';
				end if;
			end if;			
			
			-- LED Definiation Selection ------------------------------------------------------------
			------- Bottom LED (Right)
			o_LED( 1 downto 0 ) <= Status_LED1;			
			o_LED( 3 downto 2 ) <= Status_LED2;
			------- Bottom LED (Right)
				
			------- Top LED (Left)
               if( Standby = '1' )
                    then Status_LED2 <= LED_RED;  
			elsif(( En_Almost_Ready = '0' ) or ( Det_Ready = '0' ))
                    then Status_LED2 <= LED_YEL;				
               elsif( Reset_Grn_LED = '1' )
                    then Status_LED2 <= LED_GRN;
                    else Status_LED2 <= LED_OFF;                                                       
               end if;
			------- Top LED (Left)
			    
			-- LED Definiation Selection ------------------------------------------------------------              
               -- Status Register ----------------------------------------------------------------------
         		Status( 2 downto 0 ) 	<= AmpTimeReg( 2 downto 0 );		-- Current AmpTime Selection
               Status( 4 downto 3 ) 	<=(others=>'0');
               Status( 6 downto 5 )	<= AmpTimeReg( 13 downto 12 );	-- Current ev/ch code selection
               Status( 7 )			<= Time_Enable;
               Status( 8 )			<= Preset_FMap;
               Status( 10 downto 9 ) 	<= Status_LED1;			-- LED1 Status
               Status( 11 )			<= Preset_SMap;
               Status( 12 )			<= Slide_Home_Status;
               Status( 13 )			<= Slide_Move_Enable;
               Status( 18 downto 14 )	<= ext( Slide_State, 5 );
               Status( 19 )			<= not( i_ESync_In0 );	-- Active Low Input
               Status( 20 )			<= not( i_ESync_In1 );	-- Active Low Input
               Status( 21 )			<= not( i_ESync_In2 );	-- Active Low Input
               Status( 22 )			<= En_Vent;
               Status( 23 )			<= '0';
			
			if(( PACtrl_En_Ready = '1' ) and ( Det_Ready = '1' ))
				then Status(24) <= '1';
				else Status(24) <= '0';
			end if;
						
               Status( 29 downto 25 ) 	<= HV_State_Code;		
               Status( 30 ) 			<= '0';
               Status( 31 ) 			<= Det_Sat_Latch;			-- Detector Saturated 
               -- Status Register
			----------------------------------------------------------------------

			----------------------------------------------------------------------
			-- Indicator Register	
			Indicator_Reg(  3 downto  0 ) <= ext( Status_LED1, 4 );
			Indicator_Reg(  7 downto  4 ) <= ext( Status_LED2, 4 );
			
			-- External Sync Input
			Indicator_Reg( 11 downto  8 ) <= ext( Status_Sync_Out, 4 );
			
			-- External Sync Output
			Indicator_Reg( 15 downto 12 ) <= ext( Status_Sync_in, 4 );
			Indicator_Reg( 31 downto 16 ) <= (others=>'0');
			
			----------------------------------------------------------------------
			----------------------------------------------------------------------
			if( Det_Sat = '1' )
				then Det_Sat_Latch <= '1';
			elsif( Cmd_Stb(STB_SLIDE_MR) = '1' )
				then Det_Sat_Latch <= '0';
			end if;
			
			-- External Sync 1 - Outputs ------------------------------------------------------------
			-- All outputs are active low
			-- RS-485 device MAX3078 has a guaranteed output high for inputs that are not connected
			case conv_integer( ESync_Reg( ESCW_ESync1_Sel_H downto ESCW_ESync1_Sel_L )) is				
				when 0 =>
					O_ESync_Wen1		<= '0';		-- set to 0 (receive mode) on 10/15/14
					o_ESync_Out1		<= '1';		-- Force Output to 0
					Status_Sync_Out	<= LED_Off;
				
				when 1 => 			-- DPP Busy Active High (only)
					O_ESync_Wen1		<= '1';
					o_ESync_Out1		<= not( Dpp_Busy );
					if( Dpp_Busy = '1' )
						then Status_Sync_Out	<= LED_RED;
						else Status_Sync_Out	<= LED_GRN;
					end if;											
							
				when 2 =>				-- SCA
					O_ESync_Wen1		<= '1';		-- Enable Output Enable
					o_ESync_Out1		<= '1';		-- Force Output to 0
					Status_Sync_Out	<= LED_Off;									

				when 3 =>				-- Not Cold so allow venting
					O_ESync_Wen1		<= '1';
					o_ESync_Out1		<= not( En_Vent );
					if( En_Vent = '1' )
						then Status_Sync_Out	<= LED_GRN;
						else Status_Sync_Out	<= LED_RED;
					end if;											
					
					
--				when 4 =>				-- Retracted so allow tilting
--					o_ESync_Wen1 		<= '1';				-- Setup for Read Mode
--					o_ESync_Out1		<= not( i_mout_sw );	-- If Out Switch is engaged - Allow Tilting
--				
--				when 5 =>
--					o_ESync_Wen1 		<= '1';			-- Setup for Read Mode
--					if(( En_Vent = '1' ) and ( i_MOut_Sw = '1' ))
--						then o_ESync_Out1 <= '0';
--						else o_ESync_Out1 <= '1';
--					end if;
										
				-- Includes not-used code 0
				when others =>
					o_ESync_Wen1 		<= '0';
					o_ESync_Out1		<= '0';
					Status_Sync_Out	<= LED_Off;
					
			end case;
			-- External Sync 1 - Outputs ------------------------------------------------------------

			-- External Sync 2 ( Used for Inputs ) - Outputs ----------------------------------------
			case conv_integer( ESync_Reg( ESCW_ESync2_Sel_H downto ESCW_ESync2_Sel_L )) is					
				when 0 =>
					O_ESync_Wen2		<= '0';		-- set to 0 (receive mode) on 10/15/14
					o_ESync_Out2		<= '1';		
					Ext_Cool_en		<= '1';		
					Status_Sync_In		<= LED_OFF;

				when 1 => 			-- High Vacuum - Allow Cooling		
					O_ESync_Wen2		<= '0';			-- Read Mode
					o_ESync_Out2		<= '1';			-- Don't Drive
					Ext_Cool_En		<= not( i_ESync_In2 );
					Ext_Retract		<= '0';			
					if( i_ESync_In2 = '0' )
						then Status_Sync_In	<= LED_GRN;
						else Status_Sync_In	<= LED_RED;
					end if;											

				when 2 =>				
					O_ESync_Wen2		<= '1';		-- Enable Output Enable
					o_ESync_Out2		<= '1';		-- Force Output to 0
					Ext_Cool_en		<= '1';		-- Allow Cooling
					Status_Sync_In		<= LED_OFF;									
					 
				when 3 =>				-- High Vacuum And Not Tilted
					O_ESync_Wen2		<= '0';			-- Read Mode
					o_ESync_Out2		<= '1';
					Ext_Cool_En		<= not( i_ESync_In2 );
					Ext_Retract		<= not( i_ESync_In2 );
					if( i_ESync_In2 = '0' )
						then Status_Sync_In	<= LED_GRN;
						else Status_Sync_In	<= LED_RED;
					end if;											
					
				-- Includes 0 - Unused and 7 Load Cell							
				when others =>
					O_ESync_Wen2		<= '0';
					o_ESync_Out2		<= '1';
					Ext_Cool_En		<= '1';			-- Satisfied
					Ext_Retract		<= '0';			-- Satisfied
					Status_Sync_In		<= LED_OFF;
			end case;
			-- External Sync 2 ( Used for Inputs ) - Outputs ----------------------------------------				
		end if;
	end process;
     ---------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------
end BEHAVIORAL;          -- ep98861.VHD
--------------------------------------------------------------------------------------------------


