## Generated SDC file "AD5453.sdc"

## Copyright (C) 1991-2010 Altera Corporation
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, Altera MegaCore Function License 
## Agreement, or other applicable license agreement, including, 
## without limitation, that your use is for the sole purpose of 
## programming logic devices manufactured by Altera and sold by 
## Altera or its authorized distributors.  Please refer to the 
## applicable agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 10.0 Build 218 06/27/2010 SJ Full Version"

## DATE    "Wed Sep 22 08:19:27 2010"

##
## DEVICE  "EP3C40U484C7"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {CLK100}  -period 10.000 -waveform { 0.000 5.000 } [get_ports {CLK100}]


#**************************************************************
# Create Generated Clock
#**************************************************************


#**************************************************************
# Set Clock Latency
#**************************************************************

#**************************************************************
# Set Clock Uncertainty
#**************************************************************

derive_clock_uncertainty

#**************************************************************
# Set Input Delay
#**************************************************************

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {Blevel_En}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {Blevel_En}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {Spc_Test_En}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {Spc_Test_En}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {dss_reg[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {dss_reg[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {dss_interval[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {dss_interval[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {dss_cnt_max[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {dss_cnt_max[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {dss_data[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {dss_data[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {dss_trigger[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {dss_trigger[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {Line_Code[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {Line_Code[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {Pixel_Status[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {Pixel_Status[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {Line_Status[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {Line_Status[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {Frame_Status[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {Frame_Status[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {Spec_Update}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {Spec_Update}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {Time_Enable}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {Time_Enable}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {Read_Data_En}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {Read_Data_En}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {Preset_Map}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {Preset_Map}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {MCA_Data[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {MCA_Data[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {Blevel_Data[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {Blevel_Data[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {RData[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {RData[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {CMD_STB[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {CMD_STB[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {Read_FF_EF}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {Read_FF_EF}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {Read_FF_FF}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {Read_FF_FF}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {Spec_Buf_Full}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {Spec_Buf_Full}]		

#**************************************************************
# Set Output Delay
#**************************************************************        
				


				
set_output_delay -clock { CLK100 } -max 4.000 [get_ports {Dss_En}]
set_output_delay -clock { CLK100 } -min 3.000 [get_ports {Dss_En}]

set_output_delay -clock { CLK100 } -max 4.000 [get_ports {Start_MCA_Update}]
set_output_delay -clock { CLK100 } -min 3.000 [get_ports {Start_MCA_Update}]

set_output_delay -clock { CLK100 } -max 4.000 [get_ports {Start_Blevel_Update}]		
set_output_delay -clock { CLK100 } -min 3.000 [get_ports {Start_Blevel_Update}]		

set_output_delay -clock { CLK100 } -max 4.000 [get_ports {Dpp_Busy}]
set_output_delay -clock { CLK100 } -min 3.000 [get_ports {Dpp_Busy}]

set_output_delay -clock { CLK100 } -max 4.000 [get_ports {DMA_Read_Trig}]
set_output_delay -clock { CLK100 } -min 3.000 [get_ports {DMA_Read_Trig}]

set_output_delay -clock { CLK100 } -max 4.000 [get_ports {Read_FF_Wen}]
set_output_delay -clock { CLK100 } -min 3.000 [get_ports {Read_FF_Wen}]

set_output_delay -clock { CLK100 } -max 3.000 [get_ports {Read_FF_WData[*]}]
set_output_delay -clock { CLK100 } -min 2.000 [get_ports {Read_FF_WData[*]}]


#**************************************************************
# Set Clock Groups
#**************************************************************



#**************************************************************
# Set False Path
#**************************************************************



#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

