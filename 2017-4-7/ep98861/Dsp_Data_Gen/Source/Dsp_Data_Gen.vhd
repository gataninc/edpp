-- TODO--	Use Channel number as the key--	ie Channel 0 = Start ke
--		all channels are 1-based ( ie channel + 1 )
--	CHannel 4095 is the Stop Key
-- Mapping Mode
--	Data to FPGA is 16 bits wide
--		15:12 	= Counts nibble
--		11:0 	= Channel
--	Place two next to eachother, if counts is greater than 16 then the same channel
--	is dupliated with the next nibble
-- 
---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	Dsp_Data_Gen
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : September 21, 2007
--   Board          : Artemis POE Study Board
--   Schematic      : 4035.007.25500 Rev A
--
--   Top Level      : 4035.009.9933?, S/W, Artemis, Study
--
--   Description:	
--			This module interfaces to the Critcial Link MityDSP
--
--		Packet formats
--	
--		DAta Written to FPGA
--			When the MSB is a '1', the data is a command and follows the key codes

--   History:       <date> - <Author> - Ver
--		01/10/2008 - MCS - 08011005
--			Module: Tested on the Instrument Control Board - Rev B with proper operation
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
library LPM;
	use lpm.lpm_components.all;

library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
entity Dsp_Data_Gen is 
	generic(
		-- Strobe bit positions ----------------------------------------------------------------------
		STB_TIME_CLR		: integer := 2;
		STB_DSS_START		: integer := 16;
		STB_DSS_STOP		: integer := 17 );
	port( 
		CLK100			: in		std_logic;							-- Internal Clock		
		-- DPP Related Signals
		Blevel_En			: in		std_logic;
		Spc_Test_En		: in		std_logic;
		dss_reg			: in		std_logic_vector( 31 downto 0 );		-- Static Parameters
		dss_interval		: in		std_logic_vector( 31 downto 0 );		-- Interval
		dss_cnt_max		: in		std_Logic_Vector( 31 downto 0 );		-- Maximum Points
		dss_data			: in		std_logic_vector( 15 downto 0 );		-- Data
		dss_trigger		: in		std_logic_Vector(  5 downto 0 );		-- Dymanic INputs
		Line_Code			: in		std_logic_Vector(  1 downto 0 );
		Pixel_Status		: in		std_logic_Vector( 15 downto 0 );
		Line_Status		: in		std_logic_Vector( 15 downto 0 );
		Frame_Status		: in		std_logic_Vector( 15 downto 0 );								
		Spec_Update		: in		std_logic := '0';		
		Time_Enable		: in		std_logic;
		Read_Data_En		: in		std_logic;
		Preset_FMap		: in		std_logic;
		Preset_Smap		: in		std_logic;
		MCA_Data			: in		std_logic_Vector( 31 downto 0 );
		Blevel_Data		: in		std_logic_Vector( 31 downto 0 );
		RData			: in		std_logic_vector( (32*111)+31 downto (32*64) );
		CMD_STB			: in  	std_logic_Vector( 31 downto 0 );			
		Read_FF_EF		: in		std_logic;
		Read_FF_FF		: in		std_logic;		
		Spec_Buf_Full		: in		std_logic;
		
		-- Read Data - All Parameters to be multiplexed
		Time_Disable_Latch	: buffer 	std_logic;
		Time_Enable_Latch	: buffer  std_logic;
		
		Dss_En			: buffer	std_logic;
		Start_MCA_Update	: buffer	std_logic := '0';
		Start_Blevel_Update	: buffer	std_logic := '0';
		Dpp_Busy			: buffer	std_logic;
		DMA_Read_Trig		: buffer 	std_logic;
		Read_FF_Wen		: buffer	std_logic;
		Read_FF_WData		: buffer	std_logic_vector( 31 downto 0 );
		FFR_State_Code		: buffer	std_logic_Vector( 7 downto 0 ) );
end Dsp_Data_Gen;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture behavioral of Dsp_Data_Gen is
	constant Spec_Rec_Len 			: integer := 4010;
	constant Max_CPeak_Addr			: integer := 4000;
	constant Max_Blevel_Addr			: integer := 4095;
	constant CLK_PER				: integer := 10;
	constant ms100_cnt_max			: integer := 100000000 / CLK_PER;
	
	-- Write keys are in file FIFO_WRITE.VHD
	constant krKey_Param			: std_logic_Vector( 31 downto 30 ) := "11";
	constant krKey_Spec_Data			: std_logic_Vector( 31 downto 30 ) := "10";
	constant krKey_Blevel			: std_logic_Vector( 31 downto 30 ) := "01";
	constant krKey_Dss_Data			: std_logic_Vector( 31 downto 30 ) := "00";
	
	constant krKey_Data				: std_Logic_Vector( 29 downto 26 ) := "0000";
	constant krKey_SCABB			: std_Logic_Vector( 29 downto 26 ) := "0001";
	constant krKey_Status			: std_Logic_Vector( 29 downto 26 ) := "0010";
	
	constant krKey_Dss_Start			: std_logic_Vector( 29 downto 26 ) := "0100";
	
	constant krKey_Map_Pixel			: std_logic_Vector( 29 downto 26 ) := "1001";
	constant krKey_Map_Line			: std_logic_Vector( 29 downto 26 ) := "1010";
	constant krKey_Map_EOLN			: std_logic_Vector( 29 downto 26 ) := "1011";
	constant krKey_Dsp_Counts		: std_logic_Vector( 29 downto 26 ) := "1100";
	constant krKey_TimeBuffer_Start	: std_logic_Vector( 29 downto 26 ) := "1101";
	constant krKey_SMAP_DONE			: std_logic_Vector( 29 downto 26 ) := "1110";	-- Slow Map Point Done 

	-- DSS Mode Constants ------------------------------------------------------------------------
	constant kdss_Chan_SelA_MSB		: integer := 23;		-- These want to be in the top level
	constant kdss_chan_selA_LSB		: integer := 20; 		-- these want to be in the top level
	
	constant kdss_trig_Sel_MSB		: integer := 18; 	
	constant kdss_trig_Sel_LSB		: integer := 16; 
	constant kdss_trig_level_MSB		: integer := 15;  
	constant kdss_trig_level_LSB		: integer := 0;  
	
	constant DTrig_None 			: integer := 0;
	constant DTrig_FDisc			: integer := 1;
	constant DTrig_PA_Reset 			: integer := 2;
	constant DTrig_Blevel_Upd 		: integer := 3;
	constant DTrig_Profile_Start		: integer := 4;
	constant DTrig_Move_Start		: integer := 5;
	constant DTrig_Pos_Slope			: integer := 6;
	constant DTrig_Neg_Slope			: integer := 7;
	-- DSS Mode Constants ------------------------------------------------------------------------


	-- FIFO Read States
	constant FFR_Idle				: integer := 0;
	constant FFR_Dsp_Read_Trig_Start	: integer := 1;
	constant FFR_Dsp_Read_Trig_Pause	: integer := 2;	
	constant FFR_Dsp_Read_Trig		: integer := 3;	
	constant FFR_Dss_Start			: integer := 4;
	constant FFR_Dss_Delay			: integer := 5;
	constant FFR_Dss_Write			: integer := 6;
	constant FFR_Dss_Pause			: integer := 7;
	constant FFR_Status				: integer := 8;	
	constant FFR_Time_Enable			: integer := 9;
	
	constant FFR_SMAP_Line			: integer := 10;
	constant FFR_SMAP_Pixel 			: integer := 11;							
	constant FFR_SMAP_FRAME 			: integer := 12;
	constant FFR_SMap_Status			: integer := 13;
	
	constant FFR_Blevel_Update_Start	: integer := 15;
	constant FFR_Blevel_Update_Wait	: integer := 16;
	constant FFR_Blevel_Update		: integer := 17;
	constant FFR_Blevel_Done			: integer := 18;	
	
	constant FFR_FMAP_Line			: integer := 19;
	constant FFR_FMAP_Pixel			: integer := 20;	
	constant FFR_FMAP_Update_Start	: integer := 21;
	constant FFR_FMAP_Update_Wait		: integer := 22;
	constant FFR_FMAP_EOLN			: integer := 23;

	constant FFR_MCA_Update_Start		: integer := 24;
	constant FFR_MCA_Update_Wait		: integer := 25;
	constant FFR_MCA_Update			: integer := 26;
	constant FFR_MCA_Done			: integer := 27;
	constant FFR_SPC_DONE			: integer := 28;
	constant FFR_RO_Update_Status		: integer := 29;
	constant FFR_RO_Update_Start 		: integer := 30;
	constant FFR_RO_Update			: integer := 31;
	constant FFR_RO_UPdate_Done		: integer := 32;		-- keep last
	
	constant iADR_UPDATE_START 		: integer := 16#40#;		-- (64) was 16#20#;
	constant iADR_UPDATE_END			: integer := 16#6F#;		-- (95) was 16#3F#		

	type Read_WData_Type		 is array( iADR_UPDATE_END downto iADR_UPDATE_START ) of std_logic_Vector( 31 downto 0 );
	
	-- Signal Declarations -----------------------------------------------------------------------
	signal Cur_RAddr			: integer range iADR_UPDATE_START to iADR_UPDATE_END;

	signal DSS_Cnt				: std_logic_vector( 12 downto 0 ):= (others=>'0');
	signal Dss_Int_Cnt 			: std_logic_vector( 31 downto 0 ):= (others=>'0');
	signal Dss_Int_Tc			: std_logic:= '0';
	signal Dss_trig_Cmp			: std_Logic:= '0';
	signal Dss_Trig_Cmp_Del		: std_logic:= '0';
	signal Dss_Start_Latch		: std_logic:= '0';
	signal Dss_Cnt_Tc			: std_logic:= '0';

	signal Pixel_Status_Reg 		: std_logic_Vector( 15 downto 0 );
	signal Line_Status_Reg 		: std_logic_Vector( 15 downto 0 );
	signal Frame_Status_Reg 		: std_logic_Vector( 15 downto 0 );
	
	signal FFR_STATE 			: integer range 0 to FFR_RO_UPdate_Done;
	
	signal Latch_Clear			: std_logic;
	
	signal ms100_cnt			: integer range 0 to ms100_cnt_max;
	signal ms100_tc			: std_logic;
	signal MCA_Count			: std_logic_vector( 25 downto 0 );

	signal Read_FF_RData		: std_logic_Vector( 31 downto 0 ):= (others=>'0');
	signal Read_ff_Data_Out		: std_logic_Vector( 31 downto 0 ):= (others=>'0');
	signal Read_WData			: Read_Wdata_Type;

	signal Spec_Update_Latch		: std_logic := '0';
	signal Spec_Update_Del		: std_logic;
	
	signal Time_Enable_Del 		: std_logic := '0';
--	signal Time_Disable_Latch	: std_logic := '0';	
--	signal Time_Enable_Latch		: std_logic := '0';	
	signal Timer_Latch			: std_logic := '0';	
--	signal Timer_Map_Latch		: std_logic := '0';
	---------------------------------------------------------------------------------------------------

begin	
	FFR_State_Code		<= conv_Std_logic_vector( FFR_STATE, 8 );
	ms100_tc			<= '1' when ( ms100_cnt = ms100_cnt_max ) else '0';
	
	----------------------------------------------------------------------------------------------
	-- Copy Read-Only Rdata into structured register
	Wdata_Gen : for i in iADR_UPDATE_START to iADR_UPDATE_END generate
		Read_Wdata(i) <= RData( (32*i)+31 downto 32*i);
	end generate;	
	----------------------------------------------------------------------------------------------
	
	----------------------------------------------------------------------------------------------
	dss_clk_proc : process( clk100 )
	begin
		if( rising_edge( clk100 )) then
			------------------------------------------------------------------------------------
			-- DSS Related Logic
			Dss_Trig_Cmp_Del	<= Dss_Trig_Cmp;

			if( Cmd_Stb( STB_DSS_START ) = '1' )
				then Dss_En	<= '1';
			elsif( Cmd_Stb( STB_DSS_STOP ) = '1' )
				then Dss_En	<= '0';
			elsif (( FFR_State = FFR_Dss_Write ) and ( Dss_Int_Tc = '1' ) and ( dss_Cnt_Tc = '1' ))
				then Dss_En	<= '0';
			end if;

			-- Latch the Dss_Start, for trigger modes, to then wait for the trigger condition
			if(( Dss_En = '0' ) or ( FFR_State = FFR_DSS_START ))
				then Dss_Start_Latch <= '0';
			elsif( CMD_STB( STB_DSS_START ) = '1' )
				then Dss_Start_Latch <= '1';
			end if;			
			-- DSS Related Logic
			------------------------------------------------------------------------------------
		end if;
	end process;
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- DSS Logic
	dss_trig_Compare : lpm_compare	
		generic map(
			LPM_WIDTH				=> 16,
			LPM_REPRESENTATION		=> "SIGNED",
			LPM_PIPELINE			=> 1 )
		port map(
			clock				=> CLK100,
			dataa				=> Dss_data,
			datab				=> Dss_Reg( kdss_trig_level_MSB downto kDss_trig_level_LSB ),
			agb					=> dss_trig_Cmp );

	DSS_Int_Tc 	<= '1' when ( Dss_Int_Cnt 	>= Dss_Interval 	) else '0';				
	Dss_Cnt_Tc 	<= '1' when ( Dss_Cnt 		>= Dss_Cnt_Max( 12 downto 0 )) else '0';
	
	-- DSS Logic
	----------------------------------------------------------------------------------------------

     ----------------------------------------------------------------------------------------------
	read_dsp_lclk : process( CLK100 )
	begin
		if( rising_edge( clk100 )) then
			Time_Enable_Del 	<= Time_Enable;
			Spec_Update_Del	<= Spec_Update;
			------------------------------------------------------------------------------
			-- Latch the Leading edge of Time Enable - write "Spectrum Started" to the data stream and send interrupt			
			if(( Preset_FMap = '1' ) or ( Preset_SMap = '1' ) or ( Read_Data_En = '0' ))
				then Time_Enable_Latch <= '0';
			elsif(( Time_Enable = '1' ) and ( Time_Enable_Del = '0' ))
				then Time_Enable_Latch <= '1';
			elsif( FFR_State = FFR_Dsp_Read_Trig_Start ) -- FFR_Status )
				then Time_Enable_Latch <= '0';
			elsif( FFR_State = FFR_Time_Enable )
				then Time_Enable_Latch <= '0';
			end if;
			-- Latch the Leading edge of Time Enable - write "Spectrum Started" to the data stream and send interrupt			
			------------------------------------------------------------------------------
			
			------------------------------------------------------------------------------
			-- Trailing Edge of Time_Enable - process spectrum and write Status at the end of the data
			if(( Time_Enable = '0' ) and ( Time_Enable_Del = '1' )) then
				Pixel_Status_Reg 	<= Pixel_Status;
				Line_Status_Reg 	<= Line_Status;
				Frame_Status_Reg 	<= Frame_Status;
			end if;
			
			if( Read_Data_En = '0' )
				then Time_Disable_Latch <= '0';
			elsif(( Time_Enable = '0' ) and ( Time_Enable_Del = '1' ))
				then Time_Disable_Latch <= '1';
			elsif(( Preset_FMap = '0' ) and ( Preset_SMap = '0' ) and ( FFR_State = FFR_Status ))
				then Time_Disable_Latch <= '0';			
			elsif(( Preset_FMap = '1' ) and ( FFR_State = FFR_FMap_Line ))
				then Time_Disable_Latch <= '0';
			elsif(( Preset_SMap = '1' ) and (FFR_State = FFR_SMAP_Pixel ))
				then Time_Disable_Latch <= '0';
			end if;
			-- Trailing Edge of Time_Enable - process spectrum and write Status at the end of the data
			------------------------------------------------------------------------------

			------------------------------------------------------------------------------
			-- Periodic Timer Update - Will Cause "Read-Only" Parameters to be written 
			-- At "RO_UPdate_Done" A decision will be made to continue or not
			if(( Preset_FMap = '1' ) or ( Preset_SMap = '1' ) or ( Read_Data_En = '0' ))
				then Spec_Update_Latch <= '0';			
			elsif(( Spec_Update = '1' ) and ( Spec_Update_Del = '0' ))
				then Spec_Update_Latch <= '1';
			elsif( FFR_State = FFR_MCA_Update_Start )
				then Spec_Update_Latch <= '0';
			end if;
			-- Periodic Timer Update - Non Mapping Mode
			------------------------------------------------------------------------------

			----------------------------------------------------------------------------
			if( Cmd_Stb( STB_TIME_CLR ) = '1' )
				then Latch_Clear <= '1';
			elsif( FFR_State = FFR_Status )
				then LAtch_Clear <= '0';
			end if;			
			------------------------------------------------------------------------------
					
			------------------------------------------------------------------------------
			-- Latch "Idle" Timer			
			if( ms100_tc = '1' )
				then ms100_cnt <= 0;
				else ms100_cnt <= ms100_cnt + 1;
			end if;
					
			if(( Preset_FMap = '1' ) or ( Preset_SMap = '1' ))	
				then TImer_Latch <= '0';
			elsif( ms100_tc = '1' )
				then Timer_Latch <= '1';
			elsif( FFR_State = FFR_RO_Update_Status )
				then Timer_Latch <= '0';
			end if;
			-- Latch "Idle" Timer
			------------------------------------------------------------------------------
			------------------------------------------------------------------------------------
			-- Read from FPGA State Machine 
			-- A Write command with the Read bit set indicates the amount of data to be written to the FIFO
			case FFR_STATE is
				when FFR_IDLE =>
					Read_FF_Wen						<= '0';
					Read_FF_WData 						<= (others=>'0');
				
					if( Read_FF_FF = '0' ) then					
					-- Start a DSS Sequence -----------------------------------------------------------------------------------------------
						if(( Dss_Start_Latch = '1' ) and ( Read_FF_EF = '1' )) 
							then FFR_State	<= FFR_DSS_Start;
						
						-- Clear Spectrum - Memory has been cleared, send data indicating such (to clear the host spectrum buffers ) ----------
						elsif(( Latch_Clear = '1' ) and ( MCA_Data(31) = '0' ) and ( Blevel_Data(31) = '0' )) 
							then FFR_State <= FFR_Status;						
							
						-- Time Enable has just started - Write data indicating such ----------------------------------------------------------
						elsif(( Time_Enable_Latch = '1' ) and ( Preset_FMap = '0' ) and ( Preset_SMap = '0' ))  --  and ( Spec_Buf_Full = '0' )) then
							then FFR_State <= FFR_Time_Enable; -- FFR_Status;
													
						-- Time Enable has just ended - Process Spectrum and Histogram and then update the status
						elsif(( Time_Disable_Latch = '1' ) and ( Preset_FMap = '0' ) and ( Preset_SMap = '0' ) and ( Spec_Buf_Full = '0' )) 
							then FFR_State <= FFR_MCA_Update_Start;
							
						-- Time Enable has just ended - Slow Mapping Mode 
						elsif(( Time_Disable_Latch = '1' ) and ( Preset_SMap = '1' ) and ( Spec_Buf_Full = '0' ))
							then FFR_State <= FFR_SMAP_Pixel;

						-- Time Enable has just ended - Mapping Mode Process Spectrum and Histogram and then update the status
						elsif(( Time_Disable_Latch = '1' ) and ( Preset_FMap = '1' ) and ( Spec_Buf_Full = '0' )) -- Start of Line
							then FFR_State <= FFR_FMap_Line;
										
						-- Non-Mapping Mode but get a "Spec Update - need to process histogram and baseline histogram				
						elsif(( Spec_Update_Latch = '1' ) and ( Spec_Buf_Full = '0' ))
							then FFR_State <= FFR_MCA_Update_Start;

						elsif(( Timer_Latch = '1' ) and ( Spec_Buf_Full = '0' ))
							then FFR_State <= FFR_RO_Update_Status;
							
--						elsif(( Timer_Map_Latch = '1' ) and ( Spec_Buf_Full = '0' ))
--							then FFR_State <= FFR_RO_Update_Start;							
						end if;
					end if;
				-------------------------------------------------------------------------------

				
				-------------------------------------------------------------------------------
				-- DSS Mode to Write the Data into the FIFO -----------------------------------
				when FFR_DSS_Start =>				
					Read_FF_Wen				<= '0';
					Read_FF_WData 				<= (others=>'0');
				
					Dss_Int_Cnt				<= (others=>'0');
					Dss_Cnt					<= (others=>'0');
					
					-- Wait for Trigger to occur
					if( Dss_En = '0' ) 
						then FFR_State		<= FFR_Idle;						
					elsif(( conv_integer( Dss_Reg( kdss_trig_Sel_MSB downto kDss_trig_Sel_LSB)) = DTrig_Pos_Slope ) and ( Dss_Trig_Cmp = '1' ) and ( Dss_Trig_Cmp_Del = '0' )) 
						then FFR_State 	<= FFR_Dss_Write;
					elsif(( conv_integer( Dss_Reg( kdss_trig_Sel_MSB downto kDss_trig_Sel_LSB)) = DTrig_Neg_Slope ) and ( Dss_Trig_Cmp = '0' ) and ( Dss_Trig_Cmp_Del = '1' )) 
						then FFR_State 	<= FFR_Dss_Write;
					elsif( Dss_Trigger(conv_integer( Dss_Reg( kdss_trig_Sel_MSB downto kDss_trig_Sel_LSB) ) ) = '1' ) 
						then FFR_State 	<= FFR_Dss_Write;
					end if;
										
				when FFR_Dss_Write =>
					Read_FF_Wen						<= '1';
					Read_FF_WData( 31 downto 30 ) 		<= krKey_Dss_Data;
					Read_FF_WData( 29 )					<= '1';
					Read_FF_WData( 28 downto 16 ) 		<= Dss_Cnt;
					if( Dss_Reg( kdss_Chan_SelA_MSB downto kdss_chan_selA_LSB ) = "1111" )
						then Read_FF_WData( 15 downto 0 )	<= ext( Dss_Cnt, 16 );
						else Read_FF_WData( 15 downto 0 )	<= Dss_Data;
					end if;					

					Dss_Cnt 							<= Dss_Cnt + 1;
					Dss_Int_Cnt 						<= (others=>'0');
					
					-- Counts the number of samples
					if( Dss_Cnt_Tc = '1' )								
						then FFR_State					<= FFR_Dsp_Read_Trig_Start;
					elsif( conv_integer( Dss_Interval ) = 0 ) then				-- Keep writing every sample
						if( Read_FF_FF = '1' )								-- Fifo is full - so pause
							then FFR_State 			<= FFR_Dss_Pause;
							else FFR_State 			<= FFR_Dss_Write;
						end if;
					else 
						FFR_State						<= FFR_Dss_Delay;
					end if;
					
				when FFR_Dss_Pause =>
					Read_FF_Wen						<= '0';
					Read_FF_WData 						<= (others=>'0');

					if( Read_FF_EF = '1' )
						then FFR_State	<= FFR_Dss_Write;
					end if;
					
				when FFR_Dss_Delay	=>
					Read_FF_Wen						<= '0';
					Read_FF_WData 						<= (others=>'0');
					Dss_Int_Cnt						<= Dss_Int_Cnt + 1;
					
					if( Dss_en = '0' )
						then FFR_State <= FFR_Dsp_Read_Trig_Start;
						
					elsif(( Dss_Int_Tc = '1' ) and ( Read_FF_FF = '1' ))
						then FFR_State <= FFR_Dss_Pause;						
						
					elsif(( Dss_Int_Tc = '1' ) and ( Read_FF_FF = '0' ))
						then FFR_State <= FFR_Dss_Write;
					end if;
				-- DSS Mode to Write the Data into the FIFO -----------------------------------
				-------------------------------------------------------------------------------
				
				-------------------------------------------------------------------------------
				-- Mapping Spectrum Processing 
				-- Incorporate End of Frame/End of Map to this key
				when FFR_FMAP_Line =>												
					Read_FF_WData( 31 downto 30 )	<= krKey_Param;
					Read_FF_WData( 29 downto 26 )	<= krKey_Map_Line;
					Read_FF_WData( 25 downto 16 )	<= (others=>'0');					
					Read_FF_WData( 15 downto  0 )	<= Line_Status_Reg;		
					if( Read_FF_FF = '0' ) then					
						Read_FF_Wen			<= '1';
						FFR_State 			<= FFR_FMAP_Pixel;
					end if;
					
				when FFR_FMAP_Pixel =>												
					Read_FF_WData( 31 downto 30 ) <= krKey_Param;
					Read_FF_WData( 29 downto 26 ) <= krKey_Map_Pixel;
					Read_FF_WData( 25 downto 16 ) <= (others=>'0');					
					Read_FF_WData( 15 downto  0 ) <= Pixel_Status_Reg;
					if( Read_FF_FF = '0' ) then			
						Read_FF_Wen			<= '1';
						FFR_State 			<= FFR_FMap_Update_Start;
					end if;
					
				when FFR_FMap_Update_Start =>							
					Read_FF_Wen				<= '0';
					Read_FF_WData 				<= (others=>'0');
					FFR_State 				<= FFR_FMap_Update_Wait;					

				when FFR_FMap_Update_Wait =>
					Read_FF_WData( 31 downto 30 ) <= krKey_Spec_Data;
					if( Spc_Test_En = '1' )
						then Read_FF_WDAta( 29 downto 12 )	<= ext( MCA_Data( 11 downto 0 ), 30-12 );	-- Counts = Channel Number
						else Read_FF_WData( 29 downto 12 )	<= MCA_Data( 29 downto 12 );				-- Channel Counts
					end if;					
					Read_FF_WDAta( 11 downto  0 )	<= MCA_Data( 11 downto  0 );			-- Channel Number

					if(( MCA_Data(30) = '1' ) and ( Spc_Test_En = '1' ))
						then Read_FF_Wen 		<= '1';
					elsif(( MCA_Data(30) = '1' ) and ( conv_integer( MCA_Data( 29 downto 12 ) ) = 0 ))
						then Read_FF_Wen		<= '0';
						else Read_FF_Wen		<= '1';
					end if;						

					-- TODO Add some "Timeout" term
					if( MCA_DAta(30) = '1' )
						then FFR_State 		<= FFR_MCA_Update; 
					end if;										
					
				-- Mapping Spectrum Processing 
				-------------------------------------------------------------------------------
				
				when FFR_SMAP_Pixel =>												
					Read_FF_WData( 31 downto 30 ) <= krKey_Param;
					Read_FF_WData( 29 downto 26 ) <= krKey_Map_Pixel;
					Read_FF_WData( 25 downto 16 ) <= (others=>'0');					
					Read_FF_WData( 15 downto  0 ) <= Pixel_Status_Reg;
					if( Read_FF_FF = '0' ) then			
						Read_FF_Wen			<= '1';
						FFR_State 			<= FFR_SMAP_Line; -- FFR_SMAP_FRAME;
					end if;
					
				when FFR_SMAP_Line =>												
					Read_FF_WData( 31 downto 30 )	<= krKey_Param;
					Read_FF_WData( 29 downto 26 )	<= krKey_Map_Line;
					Read_FF_WData( 25 downto 16 )	<= (others=>'0');					
					Read_FF_WData( 15 downto  0 )	<= Line_Status_Reg;		
					if( Read_FF_FF = '0' ) then					
						Read_FF_Wen			<= '1';
						FFR_State 			<= FFR_SMAP_FRAME;
					end if;
					

				-- Added "Frame Status" at the end of hte spectral data, as an "key" to trap on

				when FFR_SMAP_FRAME =>
					Read_FF_WData( 31 downto 30 ) <= krKey_Param;
					Read_FF_WData( 29 downto 26 ) <= krKey_Map_EOLN;
					Read_FF_WData( 25 downto 16 ) <= (others=>'0');					
					Read_FF_WData( 15 downto  0 ) <= Frame_Status_Reg;
					
					if( Read_FF_FF = '0' ) then
						Read_FF_Wen			<= '1';
						FFR_State 			<= FFR_MCA_Update_Start;
					end if;
				-- Slow Mapping header information
				-------------------------------------------------------------------------------

				-------------------------------------------------------------------------------
				-- Spectrum Processing 
				when FFR_MCA_Update_Start =>							
					Read_FF_Wen				<= '0';
					Read_FF_WData 				<= (others=>'0');
					MCA_Count					<= (others=>'0');
					FFR_State 				<= FFR_MCA_Update_Wait;

				-- Wait for MCA_DATA(30) == 1
				when FFR_MCA_Update_Wait =>	
					Read_FF_WData( 31 downto 30 ) <= krKey_Spec_Data;
					if( Spc_Test_En = '1' )
						then Read_FF_WDAta( 29 downto 12 )	<= ext( MCA_Data( 11 downto 0 ), 30-12 );	-- Counts = Channel Number
						else Read_FF_WData( 29 downto 12 )	<= MCA_Data( 29 downto 12 );				-- Channel Counts
					end if;					
					Read_FF_WDAta( 11 downto  0 )	<= MCA_Data( 11 downto  0 );			-- Channel Number
															
					if( MCA_Data(30)	= '1' ) then	
						if( Spc_Test_En = '1' )
							then Read_FF_Wen <= '1';
						elsif( conv_integer( MCA_Data( 29 downto 12)) /= 0 )
							then Read_FF_Wen <= '1';
							else Read_FF_Wen <= '0';
						end if;
						
						if( conv_integer( MCA_Data( 29 downto 12)) /= 0 )
							then MCA_Count <= MCA_Count + 1;
						end if;
						
						
						FFR_State 		<= FFR_MCA_Update;
					end if;				
					
				-- Spectrum Processing 
				-------------------------------------------------------------------------------
					
				-------------------------------------------------------------------------------
				-- All Spectrum Processing 
				when FFR_MCA_Update =>	
					-- Only Write non-zero values
					Read_FF_WData( 31 downto 30 ) <= krKey_Spec_Data;
					if( Spc_Test_En = '1' )
						then Read_FF_WDAta( 29 downto 12 )	<= ext( MCA_Data( 11 downto 0 ), 30-12 );	-- Counts = Channel Number
						else Read_FF_WData( 29 downto 12 )	<= MCA_Data( 29 downto 12 );				-- Channel Counts
					end if;
					
					Read_FF_WDAta( 11 downto  0 )	<= MCA_Data( 11 downto  0 );			-- Channel Number
					
					if( Spc_Test_En = '1' )
						then Read_FF_Wen 		<= '1';
					elsif( conv_integer( MCA_Data( 29 downto 12 )) /= 0 ) 
						then Read_FF_Wen		<= '1';
						else Read_FF_Wen		<= '0';
					end if;						

					if( conv_integer( MCA_Data( 29 downto 12)) /= 0 )
						then MCA_Count <= MCA_Count + 1;
					end if;
					
					-- TODO Add some "Timeout" term
					if( MCA_Data(30)	= '0' )
						then FFR_State 		<= FFR_MCA_DONE;
					end if;
				
				when FFR_MCA_Done => 					
					Read_FF_Wen				<= '0';
					Read_FF_WData 				<= (others=>'0');
					
					if( Preset_FMap = '1' )				-- Fast Mapping - Append EOLN Key
						then FFR_State 		<= FFR_FMAP_EOLN;
					elsif( Preset_SMap = '1' )
						then FFR_State 		<= FFR_SMap_Status;
						else FFR_State 		<= FFR_SPC_DONE;
					end if;
					
					
--					if( Preset_FMap = '0' )
--						then FFR_STATE 		<= FFR_SPC_DONE;
--						else FFR_State 		<= FFR_FMap_EOLN;
--					end if;
--					elsif( Line_Code = "10" ) 						-- End of Line
--						then FFR_State <= FFR_FMap_EOLN;
--					elsif( Line_Code = "11" )
--						then FFR_State <= FFR_FMap_EOLN;				-- End of Frame
--						else FFR_State <= FFR_Idle;
--					end if;									
				-- All Spectrum Processing 
				-------------------------------------------------------------------------------
				
				-------------------------------------------------------------------------------

				-------------------------------------------------------------------------------
				-- Slow Mapping header information
				when FFR_SMap_Status	=>
					Read_FF_WData( 31 downto 30 ) 	<= krKey_Param;
					Read_FF_WData( 29 downto 26 ) 	<= krKey_SMAP_DONE;
					Read_FF_WData( 25 downto  0 )		<= MCA_Count;
					if( Read_FF_FF = '0') then
						Read_FF_Wen		<= '1';
						FFR_State 		<= FFR_Dsp_Read_Trig_Start;	
					end if;
				
				-------------------------------------------------------------------------------
				-- Fast Map - End of Line - Force an Interrupt
				when FFR_FMap_EOLN =>
					Read_FF_WData( 31 downto 30 ) <= krKey_Param;
					Read_FF_WData( 29 downto 26 ) <= krKey_Map_EOLN;
					Read_FF_WData( 25 downto 16 ) <= (others=>'0');					
					Read_FF_WData( 15 downto  0 ) <= Frame_Status_Reg;
					
					if( Read_FF_FF = '0' ) then
						Read_FF_Wen			<= '1';
						if( Line_Code = "10" ) 						-- End of Line
							then FFR_State 	<= FFR_RO_Update_Status;
						elsif( Line_Code = "11" )
							then FFR_State 	<= FFR_RO_Update_Status;	-- End of Frame
							else FFR_State 	<= FFR_Idle;
						end if;
					end if;
				-------------------------------------------------------------------------------				

				-------------------------------------------------------------------------------
				-- Specturm Processing Done
				when FFR_SPC_DONE =>
					Read_FF_Wen			<= '0';
					Read_FF_WDAta			<= (others=>'0');
					
					if( Time_Disable_Latch = '1' )
						then FFR_State 	<= FFR_Status;
					elsif( Spec_Update_Latch = '1' )
						then FFR_State		<= FFR_Dsp_Read_Trig_Start;
						else FFR_State		<= FFR_Idle;
					end if;
				-- Specturm Processing Done
				-------------------------------------------------------------------------------

				-------------------------------------------------------------------------------
				-- Just write the Status and DO NOT GENERATE an interrupt
				when FFR_Time_Enable =>
					Read_FF_Wen 					<= '1';					
					Read_FF_WData( 31 downto 30 ) 	<= krKey_Param;
					Read_FF_WData( 29 downto 26 ) 	<= krKey_Status;
					Read_FF_WData( 25 downto  4 )		<= (others=>'0');
					Read_FF_WData(3)				<= Time_Enable_Latch;
					Read_FF_WData(2)				<= Time_Disable_Latch;
					Read_FF_WData(1)				<= Latch_Clear;
					Read_FF_WData(0)				<= Time_Enable;				
					FFR_State 					<= FFR_Idle;
				-------------------------------------------------------------------------------

				-------------------------------------------------------------------------------
				-- Time Clear Command is finished ( all of the internal buffers have been cleared )
				-- send it down the host to clear the spectrum buffers
				when FFR_Status =>
					Read_FF_Wen 					<= '1';					
					Read_FF_WData( 31 downto 30 ) 	<= krKey_Param;
					Read_FF_WData( 29 downto 26 ) 	<= krKey_Status;
					Read_FF_WData( 25 downto  4 )		<= (others=>'0');
					Read_FF_WData(3)				<= Time_Enable_Latch;
					Read_FF_WData(2)				<= Time_Disable_Latch;
					Read_FF_WData(1)				<= Latch_Clear;
					Read_FF_WData(0)				<= Time_Enable;				
					FFR_State 					<= FFR_Dsp_Read_Trig_Start;
				-------------------------------------------------------------------------------

				-------------------------------------------------------------------------------
				-- Periodically Dump the "ReadOny" parameters to the FIFO
				when FFR_RO_Update_Status =>
					Read_FF_Wen 						<= '1';					
					Read_FF_WData( 31 downto 30 ) 		<= krKey_Param;
					Read_FF_WData( 29 downto 26 ) 		<= krKey_Status;
					Read_FF_WData( 25 downto  2 )			<= (others=>'0');
					Read_FF_WData(1)					<= Latch_Clear;
					Read_FF_WData(0)					<= Time_Enable;				
					FFR_State 						<= FFR_RO_Update_Start;
				
				when FFR_RO_Update_Start =>
					Read_FF_Wen						<= '1';
					Read_FF_WData( 31 downto 30 ) 		<= krKey_Param;
					Read_FF_WData( 29 downto 26 ) 		<= krKey_Data;
					Read_FF_WData( 25 downto 24 ) 		<= (others=>'0');
					Read_FF_WData( 23 downto 12 ) 		<= conv_std_logic_vector( iADR_UPDATE_END, 12 );
					Read_FF_WData( 11 downto  0 ) 		<= conv_std_logic_vector( iADR_UPDATE_START, 12 );
					Cur_RAddr							<= iADR_UPDATE_START;
					FFR_State 						<= FFR_RO_Update;
				-- Periodically Dump the "ReadOny" parameters to the FIFO

				-- Now Copy the Parameters to the FIFO
				when FFR_RO_Update =>
					if( Read_FF_FF = '0' )
						then Read_FF_Wen 				<= '1';
						else Read_FF_Wen 				<= '0';
					end if;
					Read_FF_WData( 31 downto 0 ) 			<= Read_WData( Cur_RAddr );

					-- Read Pointer
					if(( Read_FF_FF = '0' ) and ( Cur_RAddr < iADR_UPDATE_END ))
						then Cur_RAddr <= Cur_RAddr + 1;
					end if;

					-- Now some Limit Checking
					if(( Read_FF_FF = '0' ) and ( Cur_RAddr = iADR_UPDATE_END ))
                              then FFR_State		<= FFR_Ro_Update_Done;		-- Only update the RO paramters
					end if;
				-- Now Copy the Parametrs to the FIFO

				when FFR_Ro_Update_Done =>
					Read_FF_Wen		<= '0';
					Read_FF_WData 		<= (others=>'0');
					if( Blevel_En = '1' )
						then FFR_State <= FFR_Blevel_Update_Start;
						else FFR_State <= FFR_Dsp_Read_Trig_Start;	
					end if;
				-------------------------------------------------------------------------------

				-------------------------------------------------------------------------------
				-- Baseline Level Histogram processing
				when FFR_Blevel_Update_Start =>							
					Read_FF_Wen				<= '0';
					Read_FF_WData 				<= (others=>'0');
					FFR_State 				<= FFR_Blevel_Update_Wait;
				
				when FFR_Blevel_Update_Wait =>
					Read_FF_WData( 31 downto 30 ) <= krKey_Blevel;
					Read_FF_WData( 29 downto 12 )	<= Blevel_Data( 29 downto 12 );	-- Counts
					Read_FF_WData( 11 downto  0 )	<= Blevel_Data( 11 downto  0 );	-- Channel number

					if(( Blevel_Data(30) = '1' ) and ( conv_integer( Blevel_Data(29 downto 12 ) ) /= 0 ))
						then Read_FF_Wen	<= '1';
						else Read_FF_Wen 	<= '0';
					end if;
					
					if( Blevel_Data(30) = '1' )
						then FFR_State 		<= FFR_Blevel_Update;
					end if;					

				when FFR_Blevel_Update =>					
					Read_FF_WData( 31 downto 30 ) <= krKey_Blevel;
					Read_FF_WData( 29 downto 12 )	<= Blevel_Data( 29 downto 12 );	-- Counts
					Read_FF_WData( 11 downto  0 )	<= Blevel_Data( 11 downto  0 );	-- Channel number

					if(( Blevel_Data(30) = '1' ) and ( conv_integer( Blevel_Data(29 downto 12 ) ) /= 0 ))
						then Read_FF_Wen	<= '1';
						else Read_FF_Wen 	<= '0';
					end if;
				
					if( Blevel_Data(30) = '0' ) 
						then FFR_State 	<= FFR_Blevel_Done;
					end if;

				when FFR_Blevel_Done => 
					Read_FF_Wen			<= '0';
					Read_FF_WData 			<= (others=>'0');
					FFR_State 			<= FFR_Dsp_Read_Trig_Start;
				-- Baseline Level Histogram processing
				-------------------------------------------------------------------------------
				
				-------------------------------------------------------------------------------
				-- DSP Read Trig: Needs 2 pipeline delays for the "Read FIFO Conter to finish updating
				-- based on the last data that was written
				when FFR_Dsp_Read_Trig_Start =>
					Read_FF_Wen		<= '0';
					Read_FF_WData 		<= (others=>'0');
					FFR_State 		<= FFR_Dsp_Read_Trig_Pause;
					
				when FFR_Dsp_Read_Trig_Pause =>
					Read_FF_Wen		<= '0';
					Read_FF_WData 		<= (others=>'0');
					FFR_State 		<= FFR_Dsp_Read_Trig;
					
				when FFR_Dsp_Read_Trig =>
					Read_FF_Wen		<= '0';
					Read_FF_WData 		<= (others=>'0');
					FFR_State 		<= FFR_Idle;
				-------------------------------------------------------------------------------
				
				when others=>
					Read_FF_Wen		<= '0';
					Read_FF_WData 		<= (others=>'0');
					FFR_STATE 		<= FFR_IDLE;
			end case;
			------------------------------------------------------------------------------------
			
			------------------------------------------------------------------------------------
			-- When to force a DMA Read
			if( FFR_State = FFR_Dsp_Read_Trig )
				then DMA_Read_Trig 	<= '1';
				else DMA_Read_Trig 	<= '0';
			end if;
			-- When to force a DMA Read
			------------------------------------------------------------------------------------

			------------------------------------------------------------------------------------
			-- When to Start Updating the MCA Spectrum
			if( FFR_State = FFR_FMap_Update_Start )	-- TODO
				then Start_MCA_Update	<= '1';
			elsif( FFR_State = FFR_MCA_Update_Start )
				then Start_MCA_Update	<= '1';
				else Start_MCA_Update	<= '0';
			end if;
			-- When to Start Updating the MCA Spectrum
			------------------------------------------------------------------------------------
			
			------------------------------------------------------------------------------------
			-- When to Start Updating Baseline Level Processing
			if( FFR_State = FFR_Blevel_Update_Start )
				then Start_Blevel_Update	<= '1';
				else Start_Blevel_Update	<= '0';
			end if;
			-- When to Start Updating Baseline Level Processing
			------------------------------------------------------------------------------------
			
			------------------------------------------------------------------------------------
			-- DPP Busy indication to the SG-Board to "Pause" in the event of data processing
			if(( Preset_FMap = '1' ) and ( Time_Enable = '1' ))
				then Dpp_Busy <= '1';
			elsif(( Preset_FMap = '1' ) and ( Spec_Update = '1' ))
				then Dpp_Busy <= '1';
			elsif( FFR_State = FFR_FMAP_Update_Start )
				then Dpp_Busy	<= '1';
			elsif( FFR_State = FFR_FMap_Update_Wait )
				then Dpp_Busy	<= '1';
			elsif(( FFR_State = FFR_MCA_Update ) and ( Preset_FMap = '1' ))
				then DPP_BUsy	<= '1';
			elsif(( FFR_State = FFR_MCA_Done ) and ( Preset_FMap = '1' ))
				then DPP_BUsy	<= '1';
				else Dpp_Busy <= '0';
			end if;
			-- DPP Busy indication to the SG-Board to "Pause" in the event of data processing
			------------------------------------------------------------------------------------
		end if;
	end process;
     ----------------------------------------------------------------------------------------------

			
---------------------------------------------------------------------------------------------------
end behavioral;			-- Dsp_Data_Gen
---------------------------------------------------------------------------------------------------

					
					