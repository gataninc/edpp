---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	Det_Ctrl.VHD
---------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-III EDS FPGA
--   Schematic      : 4035.007.25600 Rev A
--
--	Top Level		: 4035.009.98861, S/W, Artemis FPGA
--   Description:
--	History
--		11051210:
--			Changed Reset Trip point from 95% to 90%
-- 		11041410:
--			Reworked some of the startup sequence to get the detector to "Kick Start" under Light, Dark and "Source" 
--			conditions
--		11032510: 
--			Had to add AD_OTR (A/D Out-of-Range) to hold "Reset_Out" asserted (internal reset detection)
--			until AD_OTR goes away. 
--			Also had to create another startup flag that prevent the AD_OTR from being used until 
--			Ready_Startup_Cnt_Max after the "DET_READY" signal goes active
--
--		11030702:
--			Changed Reset_Startup_PW to 20us from 10us
--		11022403:
--			Prevented "RESET_STATE" from being anything but "Startup Idle" for non-active reset mode 
--		11022201: 
--			Changed non-active reset to have a much longer time-out ( 10us)
--		11011101:
--			added Reset_Period_Sat_Cnt and Reset_Period_Sat_Cnt_Max to count how many reset periords
--			that are too small before triggering a "Detector Saturated"
--		10123104:
--			Changed Reset_Sig_Holdoff_Time from 3000 to 1000
--		Simulated 06/02/01: MCS
---------------------------------------------------------------------------------------------------

library LPM;
	use lpm.lpm_components.all;

library altera_mf;
	use altera_mf.altera_mf_components.all;

library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

---------------------------------------------------------------------------------------------------
entity Det_Ctrl is 
	port( 
		CLK100			: in		std_logic;					-- 
		AD_OTR			: in		std_logic;					-- A/D Out-of-Range Indication - 1 = Out-of-range
          force_reset         : in      std_Logic;
		Reset_Enable		: in		std_logic;
		Det_Cold			: in		std_logic;
		Reset_PW			: in		std_logic_Vector( 31 downto 0 );
		Din				: in		std_logic_vector( 17 downto 0 );	-- A/D Data Output - SIGNED
		HV_CWORD			: in		std_logic_Vector( 31 downto 0 );
		PA_MaxMin			: buffer 	std_logic_vector( 31 downto 0 );
		Reset_Width		: buffer	std_logic_Vector( 31 downto 0 );
		Reset_Period		: buffer	std_logic_vector( 31 downto 0 );
          o_Reset_Det         : buffer  std_logic;					-- Reset to Detector
		Reset_Out			: buffer	std_Logic;					-- Reset to Logic	
          Reset_Grn_LED       : buffer  std_logic; 
		Det_Sat			: buffer	std_logic;
		Det_Ready			: buffer	std_logic );
end Det_Ctrl;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture behavioral of Det_Ctrl is
	--////////////////////////////////////////////////////////////////////////////////////////////
	-- Constant Declarations
	--////////////////////////////////////////////////////////////////////////////////////////////
	constant CW_ENABLE_RESET				: integer := 13;
	
	constant CLOCK_PER 					: integer := 10;		

	constant Din_Max_thr				: integer := ( 32767 * 90 ) / 100;			-- 9x% of 32K - When to issue a reset (overshoots by 3%)
	constant Det_Sat_Cnt_Max				: integer := 30;
	constant ms1_cnt_max				: integer := 1000000 / CLOCK_PER;			-- 1ms timer

	constant No_Reset_Cnt_Max 			: integer := 	 10000;					-- 10 seconds @ 1ms
	
	constant Reset_LED_Cntr_Max 			: integer :=      100;			
	constant Reset_NoActive_PW			: integer :=    10000 / CLOCK_PER;
	constant Reset_Period_Min			: integer :=     5000 / CLOCK_PER;		-- Minimum Period before detecting Saturation
	constant Reset_Sig_Active_Time		: integer :=     1000 / CLOCK_PER;		-- Min Time beofe next reset can occur		was 2000
	constant Reset_Sig_Startup_Holdoff_Time	: integer :=    10000 / CLOCK_PER;		-- Min Time beofe next reset can occur		was 2000
	constant Reset_Sig_Holdoff_Time		: integer :=     1000 / CLOCK_PER;		-- Min Time beofe next reset can occur		was 2000
	constant Ready_Startup_Cnt_Max 		: integer :=    25000;				-- 60 sec @ 1ms Startup Delay before looking at AT_OTR
	constant Reset_PW_Init 				: integer :=    50000 / CLOCK_PER;		-- Starting Reset Pulse Width (No Beam - it tales a long time to get down to Reset_PW

	-- Reset Signal Generation -----------------------------------------------------------------------
	constant St_Reset_Sig_Idle			: integer := 0;
	constant St_Reset_Sig_Gen			: integer := 1;
	constant St_Reset_Sig_Gen_Done		: integer := 2;
	constant St_Reset_Sig_Active			: integer := 3;
	constant St_Reset_Sig_Active_Done		: integer := 4;	
	constant St_Reset_Sig_Holdoff			: integer := 5;
	constant St_Reset_Sig_Done			: integer := 6;
	-- Reset Signal Generation -----------------------------------------------------------------------
	--////////////////////////////////////////////////////////////////////////////////////////////
	-- Constant Declarations
	--////////////////////////////////////////////////////////////////////////////////////////////

	--////////////////////////////////////////////////////////////////////////////////////////////
	-- Signal Declarations 
	--////////////////////////////////////////////////////////////////////////////////////////////
     
	signal Det_Sat_Cnt					: integer range 0 to Det_Sat_Cnt_Max;
	signal Din_Reg                          : std_logic_Vector( 17 downto 0 );
     signal Din_Reg1                         : std_logic_Vector( 17 downto 0 );
     signal Din_Reg2                         : std_logic_Vector( 17 downto 0 );
     signal Din_Reg3                         : std_logic_Vector( 17 downto 0 );
     signal Din_Reg4                         : std_logic_Vector( 17 downto 0 );
     signal Din_Reg5                         : std_logic_Vector( 17 downto 0 );
     signal Din_Reg6                         : std_logic_Vector( 17 downto 0 );
	signal din_Del					     : std_logic_Vector( 17 downto 0 ) 	:= (others=>'0');
	signal din_diff				     : std_logic_Vector( 19 downto 0 ) 	:= (others=>'0');

	signal Max_Val_Cmp					: std_logic;
	signal Max_Det					     : std_logic_Vector( 15 downto 0 ) := (others=>'0');
	signal Min_Det					     : std_logic_Vector( 15 downto 0 ) := (others=>'0');
	signal Max_val					     : std_logic_Vector( 17 downto 0 ) := (others=>'0');
	signal Min_val						: std_logic_Vector( 15 downto 0 ) := (others=>'0');
	signal ms1_cnt						: integer range 0 to ms1_cnt_max;
	signal ms1_cnt_Tc					: std_logic;

	signal No_Reset_Cnt					: integer range 0 to No_Reset_Cnt_Max;

	signal Ready_Startup_Cnt 			: integer range 0 to Ready_Startup_Cnt_Max;
	signal Reset_Cnt_Max				: std_logic_vector( 15 downto 0 );
	signal Reset_Diff					: std_logic_Vector( 17 downto 0 );
     signal Reset_Enable_reg                 : std_logic;
	signal Reset_Holdoff_Active_Cmp 		: std_logic;
	signal Reset_Startup_Holdoff_Active_Cmp	: std_logic;
	signal Reset_LED_Cntr				: integer range 0 to Reset_LED_Cntr_Max := 0;
	signal Reset_On_Cmp					: std_logic;
	signal Reset_NSlope					: std_logic;
	signal Reset_Period_Cnt			     : std_logic_Vector( 31 downto 0 ) := (others=>'0');
	signal Reset_PW_Cnt				     : std_logic_vector( 15 downto 0 ) := (others=>'0');
     signal Reset_PW_Reg                     : std_logic_vector( 15 downto 0 );
	signal Reset_Sig_State				: integer range 0 to St_Reset_Sig_Done;		-- State Machine to Generate Reset Signal
	signal Reset_Sig_Cnt				: std_logic_vector( 15 downto 0 );	
	signal Reset_Sig_Active_Cmp 			: std_logic;					
	signal Reset_Sig_Gen_Cmp				: std_logic;
	signal Reset_PW_Cmp					: std_logic;
	
	--////////////////////////////////////////////////////////////////////////////////////////////
	-- Signal Declarations 
	--////////////////////////////////////////////////////////////////////////////////////////////
	
	
begin
	ms1_cnt_tc <= '1' when ( ms1_cnt = ms1_cnt_max ) else '0';
	-------------------------------------------------------------------------------------------------
	-- Reset Detection (for non-active reset conditions )--------------------------------------------
	Reset_Diff_Add_Sub : lpm_add_Sub
		generic map(
			LPM_WIDTH			=> 18,
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_DIRECTION		=> "SUB" )
		port map(
			cin				=> '1',
			dataa			=> Din_Reg,
			datab			=> Din_Reg6,
			result			=> Reset_Diff );			
	
	Reset_Diff_Compare : lpm_compare
		generic map(
			LPM_WIDTH			=> 18,
			LPM_REPRESENTATION 	=> "SIGNED" )
		port map(
			dataa 			=> Reset_Diff,
			datab 			=> conv_std_logic_Vector( -500, 18 ),
			alb				=> Reset_NSlope );
	-- Reset Detection (for non-active reset conditions )--------------------------------------------
	-------------------------------------------------------------------------------------------------

	-------------------------------------------------------------------------------------------------
	-- Threshold Determination when to Reset ----------------------------------------------------------
	Reset_Compare_Inst : lpm_compare
		generic map(
			LPM_WIDTH			=> 18,
			LPM_REPRESENTATION 	=> "SIGNED" )
		port map(
			dataa 			=> Din_Reg,
			datab			=> conv_std_logic_vector( Din_Max_thr, 18 ),
			agb 				=> Reset_On_Cmp );
	-- Threshold Determination when to Reset ----------------------------------------------------------
	-------------------------------------------------------------------------------------------------

	-------------------------------------------------------------------------------------------------
	-- Max Val Comparison to determine if the current input is greater than the "Latched Maximum Value
	Max_Val_Cmp_Inst : lpm_compare
		generic map(
			LPM_WIDTH			=> 18,
			LPM_REPRESENTATION 	=> "SIGNED" )
		port map(
			dataa 			=> Din_Reg,
			datab			=> Max_Val,
			agb				=> Max_Val_Cmp );
	-- Max Val Comparison to determine if the current input is greater than the "Latched Maximum Value
	-------------------------------------------------------------------------------------------------
	
	-------------------------------------------------------------------------------------------------
	-- Reset Pulse Width Comparisons
	Reset_PW_Cmp_Inst : lpm_compare
		generic map(
			LPM_WIDTH	=> 16,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			dataa			=> Reset_PW_Reg,
			datab			=> Reset_PW( 15 downto 0 ),
			aleb				=> Reset_PW_Cmp );
	
	
	Reset_Sig_Gen_Inst : lpm_compare
		generic map(
			LPM_WIDTH			=> 16,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			dataa			=> Reset_Sig_Cnt,
			datab			=> Reset_Cnt_Max,
			ageb				=> Reset_Sig_Gen_Cmp );
	
	Reset_Sig_Active_Inst : lpm_compare
		generic map(
			LPM_WIDTH			=> 16,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(		
			dataa 			=> Reset_Sig_Cnt,
			datab			=> conv_Std_logic_Vector( Reset_Sig_Active_Time, 16 ),
			ageb				=> Reset_Sig_Active_Cmp );
											
	Reset_Startup_Holdoff_Active_Inst : lpm_compare
		generic map(
			LPM_WIDTH			=> 16,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(		
			dataa 			=> Reset_Sig_Cnt,
			datab			=> conv_Std_logic_Vector( Reset_Sig_Startup_Holdoff_Time, 16 ),
			ageb				=> Reset_Startup_Holdoff_Active_Cmp );			
			

	Reset_Holdoff_Active_Inst : lpm_compare
		generic map(
			LPM_WIDTH			=> 16,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(		
			dataa 			=> Reset_Sig_Cnt,
			datab			=> conv_Std_logic_Vector( Reset_Sig_Holdoff_Time, 16 ),
			ageb				=> Reset_Holdoff_Active_Cmp );							
	-- Reset Pulse Width Comparisons
	-------------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	Reset_Gen_Proc : process( clk100 )
	begin
		if( rising_edge( CLK100 )) then
			
	          ------------------------------------------------------------------------------------
			-- Pipeline Reset Enable (from PreampCtrl)
			Reset_Enable_reg		<= Reset_Enable;
			-- Pipeline Reset Enable (from PreampCtrl)
	          ------------------------------------------------------------------------------------

	          ------------------------------------------------------------------------------------
			-- Pipeline Detector Input Data (used for slope detection)
               Din_Reg                  <= Din;
			Din_Reg1				<= Din_Reg;
			Din_Reg2				<= Din_Reg1;
			Din_Reg3				<= Din_Reg2;
			Din_Reg4				<= Din_Reg3;
			Din_Reg5				<= Din_Reg4;
			Din_Reg6				<= Din_Reg5;
			-- Pipeline Detector Input Data (used for slope detection)
	          ------------------------------------------------------------------------------------
			
	          ------------------------------------------------------------------------------------
			-- 1 ms Timer
			if( ms1_cnt_Tc = '1' )
				then ms1_cnt <= 0;
				else ms1_cnt <= ms1_cnt + 1;
			end if;
			-- 1 ms Timer
	          ------------------------------------------------------------------------------------

	          ------------------------------------------------------------------------------------
			-- No Reset Detection				
			if( Det_Ready = '0' )			-- Not Ready
				then No_Reset_Cnt <= 0;		-- Reset Counter
				
			elsif( Reset_Out = '1' )			-- Reset Detected
				then No_Reset_Cnt <= 0;		-- Reset Counter
				
			elsif(( ms1_cnt_Tc = '1'  ) and ( No_Reset_Cnt < No_Reset_Cnt_Max ))	-- Counter in-range
				then No_Reset_Cnt <= No_Reset_Cnt + 1;	-- Increment Counter				
			end if;
			-- No Reset Detection				
	          ------------------------------------------------------------------------------------

	          ------------------------------------------------------------------------------------
			-- Max Value (Din) Detection - Reset during Reset Done -----------------------------
			if( Reset_Sig_State = St_Reset_Sig_Done ) then
				Max_Val(17) 			<= '1';			-- Reset Max Val to largest Negative Number
				Max_Val( 16 downto 0 ) 	<= (others=>'0');
			elsif(( Reset_Out = '0' ) and ( Max_Val_Cmp = '1' ))
				then Max_Val <= Din;
			end if;
			-- Hold Max Value during Reset -----------------------------------------------------
	          ------------------------------------------------------------------------------------
			
	          ------------------------------------------------------------------------------------
			-- Reset Pulse Width Counter -------------------------------------------------------
			if( Reset_Sig_State = St_Reset_Sig_Done )
				then Reset_PW_Cnt <= (others=>'0');
			elsif( Reset_Out = '1' )
				then Reset_PW_Cnt <= Reset_PW_Cnt + 1;
			end if;
			-- Reset Pulse Width Counter -------------------------------------------------------
	          ------------------------------------------------------------------------------------

	          ------------------------------------------------------------------------------------
			-- Reset Period Counter ------------------------------------------------------------
			if( Reset_Sig_State = St_Reset_Sig_Done )
				then Reset_Period_Cnt <= (others=>'0');
			elsif( Reset_Out = '0' )
				then Reset_Period_Cnt <= Reset_Period_Cnt + 1;
			end if;			
			-- Reset Period Counter ------------------------------------------------------------			
	          ------------------------------------------------------------------------------------
	          ------------------------------------------------------------------------------------
			-- Reset signal to detector (active low)			
			if(( Reset_Sig_State = St_Reset_Sig_Gen ) or ( Reset_Sig_State = St_Reset_Sig_Gen_Done ))
				then o_Reset_Det <= '0';
				else o_Reset_Det <= '1';
			end if;
			-- Reset signal to detector (active low)			
	          ------------------------------------------------------------------------------------
			
	          ------------------------------------------------------------------------------------
			-- Internal Reset Detection (active high)
			if(( Reset_Sig_State = St_Reset_Sig_Gen ) or ( Reset_Sig_State = St_Reset_Sig_Gen_Done ) or ( Reset_Sig_State = St_Reset_Sig_Active ) or ( Reset_Sig_State = St_Reset_Sig_Active_Done ))
				then Reset_Out	<= '1';	-- Active		
				else Reset_out <= '0';
			end if;
	          ------------------------------------------------------------------------------------

	          ------------------------------------------------------------------------------------
			-- Reset PW  Counter
			if(( Reset_Sig_State = St_Reset_Sig_Gen ) or ( Reset_Sig_State = St_Reset_Sig_Active ) or ( Reset_Sig_State = St_Reset_Sig_Holdoff ))
				then Reset_Sig_Cnt 	<= Reset_Sig_Cnt + 1;
				else Reset_Sig_Cnt 	<=(others=>'0');
			end if;
			-- Reset PW  Counter
	          ------------------------------------------------------------------------------------
			
	          ------------------------------------------------------------------------------------
			-- Latch Minumum Value
			if( Reset_Sig_State = St_Reset_Sig_Holdoff )
				then Min_Val			<= not( Din(15)) & Din( 14 downto 0 );	-- Reset is Done so latch the currnet input and convert to 	Signed
			end if;
			-- Latch Minumum Value
	          ------------------------------------------------------------------------------------
			
	          ------------------------------------------------------------------------------------
			-- Reset is Done, Update several registers 
			if( Reset_Sig_State = St_Reset_Sig_Done ) then
				-- Update Final Registers
				Reset_Width 			 <= ext( Reset_PW_Cnt, 32 );
				Reset_Period 			 <= Reset_Period_Cnt;			-- Latch Period
				PA_MaxMin( 15 downto  0 ) <= Min_Val;					-- Reset is Done so latch the currnet input and convert to unsigned				
				PA_MaxMin( 31 downto 16 ) <= not( Max_Val( 15 )) & Max_Val( 14 downto 0 );	-- Latch "MAX" Value and convert to unsigned			
				-- Update Final Registers	
	
				-- Increment Reset_LED_Cntr ( every "Max" numbers turn on Green LED for "Duration"
				if( Reset_LED_Cntr = Reset_LED_Cntr_Max )
					then Reset_LED_Cntr <= 0;
					else Reset_LED_Cntr <= Reset_LED_Cntr + 1;
				end if;
				-- Increment Reset_LED_Cntr ( every "Max" numbers turn on Green LED for "Duration"
					
				-- At leading edge of Reset - Toggle Green LED
				if( Reset_LED_Cntr = Reset_LED_Cntr_Max )
					then Reset_Grn_LED <= not( Reset_Grn_LED );
				end if;
				-- At leading edge of Reset - Toggle Green LED					
			end if;			
			-- Reset is Done, Update several registers 
	          ------------------------------------------------------------------------------------

	          ------------------------------------------------------------------------------------
			-- Pipeline Reset Pulse Width
			-- If Reset is not enabled - re-initialize Reset_pw_reg
			if( Reset_Enable_Reg = '0' ) 
				then Reset_Pw_Reg        <= conv_Std_Logic_Vector(Reset_PW_Init, 16 );	-- Initialize to 100 micro-seconds
			elsif(( Det_Ready = '0' ) and ( ms1_cnt_tc = '1' ) and ( Reset_PW_Reg > Reset_PW(15 downto 0 )))
				then Reset_PW_Reg		<= Reset_PW_Reg - 1;
			elsif(( Det_Ready = '1' ) and ( Reset_Sig_State = St_Reset_Sig_Done )) then
				if( Reset_PW_Reg < Reset_PW(15 downto 0 ))
					then Reset_PW_Reg <= Reset_PW_Reg + 1;
				elsif( Reset_PW_Reg > Reset_PW(15 downto 0 ))
					then Reset_PW_Reg <= Reset_PW_Reg - 1;
				end if;
			end if;
			
			
			if( Reset_Enable_Reg = '0' ) 
				then Det_Ready			<= '0';
			-- Now at Reset Done - need to test...
			elsif(( Reset_Sig_State = St_Reset_Sig_Done ) and ( Reset_PW_Reg <= Reset_PW(15 downto 0 )))
				then Det_Ready <= '1';
			end if;
			-- Pipeline Reset Pulse Width
	          ------------------------------------------------------------------------------------

	          ------------------------------------------------------------------------------------						
			-- Detector Saturated Detection
			-- Hold-off Saturation detection for 
			if( Reset_Enable_Reg = '0' ) 
				then Ready_Startup_Cnt 	<= 0;
			elsif(( ms1_cnt_tc = '1' ) and ( Ready_Startup_Cnt < Ready_Startup_Cnt_Max ))
				then Ready_Startup_Cnt <= Ready_Startup_Cnt + 1;
			end if;
						
			if(( Reset_Enable_Reg = '0' ) or ( Det_Ready = '0' )) then
				Det_Sat 		<= '0';
				Det_Sat_Cnt 	<= 0;
			elsif(( Reset_Sig_State = St_Reset_Sig_Done ) and ( Ready_Startup_Cnt = Ready_Startup_Cnt_Max ) and ( Din_Reg(15) = '0' )) then
				if( Det_Sat_Cnt < Det_Sat_Cnt_Max )
					then Det_Sat_Cnt <= Det_Sat_Cnt+ 1;
					else Det_Sat <= '1';
				end if;
			end if;
	          ------------------------------------------------------------------------------------						

			--///////////////////////////////////////////////////////////////////////////////////
			-- Reset Generation State Machine ---------------------------------------------------
			--///////////////////////////////////////////////////////////////////////////////////
			case Reset_Sig_State is
				--------------------------------------------------------------------------------
				-- Idle state - All Resets are inactive
				when St_Reset_Sig_Idle =>										
					if( force_reset = '1' ) then
						Reset_Cnt_Max 		<= Reset_PW( 15 downto 0 );
						Reset_Sig_State 	<= St_Reset_Sig_Gen;
					
					elsif(( HV_CWORD( CW_ENABLE_RESET ) = '0' ) and ( Reset_NSlope = '1' )) then 
						Reset_Cnt_Max 		<=conv_std_logic_Vector( Reset_NoActive_PW, 16 );	-- Much longer than is required but used for a "Fail-safe"
						Reset_Sig_State	<= St_Reset_Sig_Gen;
						
					elsif(( Reset_Enable_Reg = '1' ) and ( Reset_On_Cmp = '1' )) then
						Reset_Cnt_Max 		<= Reset_Pw_Reg;
						Reset_Sig_State 	<= St_Reset_Sig_Gen;	
						
					-- No_Reset_Cnt increments every 1ms and is cleared with each reset (10sec)
					-- Some typical numbers w/o source are with a 350mV peak-to-peak signal
					-- -20C		 63ms
					-- -25C        132ms
					-- -29C		233ms
					-- -30C		270ms
					-- -32C		Stops ramping
					-- When this condition is detectoed, just trigger another reset
					elsif(( Reset_Enable_Reg = '1' ) and ( ms1_cnt_Tc = '1' ) and ( No_Reset_Cnt = No_Reset_Cnt_Max )) then
						Reset_Cnt_Max 		<= Reset_Pw_Reg;
						Reset_Sig_State 	<= St_Reset_Sig_Gen;	
					end if;
				--------------------------------------------------------------------------------
				
				--------------------------------------------------------------------------------
				-- Reset to Detector is begin generated
				when St_Reset_Sig_Gen =>
					if(( HV_CWORD( CW_ENABLE_RESET ) = '0' ) and ( Reset_NSlope = '0' ))  
						then Reset_Sig_State 	<= St_Reset_Sig_Gen_Done;					
						
					-- Reset Pulse-Width has elapsed
					elsif( Reset_Sig_Gen_Cmp = '1' ) 					
						then Reset_Sig_State 	<= St_Reset_Sig_Gen_Done;
					end if;
				-- Reset to Detector is begin generated
				--------------------------------------------------------------------------------

				--------------------------------------------------------------------------------
				when St_Reset_Sig_Gen_Done =>
					Reset_Sig_State <= St_Reset_Sig_Active;
				--------------------------------------------------------------------------------

				--------------------------------------------------------------------------------
				-- Reset to Detector is no-longer being generated, but internal reset is still active
				when St_Reset_Sig_Active =>
					-- waited the minimum amount of time
					if( Reset_Sig_Active_Cmp = '1' ) then
						if( Det_Ready = '0' )
							then Reset_Sig_State <= St_Reset_Sig_Active_Done;
						elsif( AD_OTR = '0' )
							then Reset_Sig_State <= St_Reset_Sig_Active_Done;						
						elsif( Din_Reg(15) = '0' )
							then Reset_Sig_State <= St_Reset_Sig_Active_Done;
						end if;
					end if;
				-- Reset to Detector is no-longer being generated, but internal reset is still active
				--------------------------------------------------------------------------------

				--------------------------------------------------------------------------------
				when St_Reset_Sig_Active_Done =>
					Reset_Sig_State <= St_Reset_Sig_Holdoff;
				--------------------------------------------------------------------------------
				
				--------------------------------------------------------------------------------
				-- Hold-Off Time ( Time when to allow no-more resets )				
				when St_Reset_Sig_Holdoff =>
					if(( Det_Cold = '1' ) and ( Det_Ready = '1' ) and ( Ready_Startup_Cnt = Ready_Startup_Cnt_Max ))then
						if( Reset_Holdoff_Active_Cmp = '1' ) 
							then Reset_Sig_State <= St_Reset_Sig_Done;						
						end if;
					else						
						if( Reset_Startup_Holdoff_Active_Cmp = '1' )
							then Reset_Sig_State <= St_Reset_Sig_Done;						
						end if;					
					end if;
				-- Hold-Off Time ( Time when to allow no-more resets )				
				--------------------------------------------------------------------------------
					
				--------------------------------------------------------------------------------
				-- Reset Generation is Done 
				when St_Reset_Sig_Done =>
					Reset_Sig_State	<= St_Reset_Sig_Idle;
				-- Reset Generation is Done 
				--------------------------------------------------------------------------------
					
				--------------------------------------------------------------------------------
				when others =>
					Reset_Sig_State	<= St_Reset_Sig_Idle;								
				--------------------------------------------------------------------------------
			end case;
			--///////////////////////////////////////////////////////////////////////////////////
			-- Reset Generation State Machine ---------------------------------------------------
			--///////////////////////////////////////////////////////////////////////////////////
		end if;
	end process;
	----------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
end behavioral;			-- Det_Ctrl.VHD
---------------------------------------------------------------------------------------------------

