## Generated SDC file "Det_Ctrl.sdc"

## Copyright (C) 1991-2010 Altera Corporation
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, Altera MegaCore Function License 
## Agreement, or other applicable license agreement, including, 
## without limitation, that your use is for the sole purpose of 
## programming logic devices manufactured by Altera and sold by 
## Altera or its authorized distributors.  Please refer to the 
## applicable agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 10.0 Build 218 06/27/2010 SJ Full Version"

## DATE    "Thu Sep 23 14:24:29 2010"

##
## DEVICE  "EP3C40F484C7"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {CLK100} -period 10.000 -waveform { 0.000 5.000 } [get_ports {CLK100}]


#**************************************************************
# Create Generated Clock
#**************************************************************



#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************
derive_clock_uncertainty


#**************************************************************
# Set Input Delay
#**************************************************************

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.330 [get_ports {ad_otr}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.330 [get_ports {ad_otr}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.330 [get_ports {force_reset}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.330 [get_ports {force_reset}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.330 [get_ports {Reset_Enable}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.330 [get_ports {Reset_Enable}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.330 [get_ports {Reset_PW[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.330 [get_ports {Reset_PW[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.330 [get_ports {Din[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.330 [get_ports {Din[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.330 [get_ports {HV_CWORD[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.330 [get_ports {HV_CWORD[*]}]





#**************************************************************
# Set Output Delay
#**************************************************************

set_output_delay -clock { CLK100 } -max 4.000 [get_ports {PA_MaxMin[*]}]
set_output_delay -clock { CLK100 } -min 3.000 [get_ports {PA_MaxMin[*]}]

set_output_delay -clock { CLK100 } -max 4.000 [get_ports {Reset_Width[*]}]
set_output_delay -clock { CLK100 } -min 3.000 [get_ports {Reset_Width[*]}]

set_output_delay -clock { CLK100 } -max 4.000 [get_ports {Reset_Period[*]}]
set_output_delay -clock { CLK100 } -min 3.000 [get_ports {Reset_Period[*]}]

set_output_delay -clock { CLK100 } -max 4.000 [get_ports {o_Reset_Det}]
set_output_delay -clock { CLK100 } -min 3.000 [get_ports {o_Reset_Det}]

set_output_delay -clock { CLK100 } -max 4.000 [get_ports {Reset_Out}]
set_output_delay -clock { CLK100 } -min 3.000 [get_ports {Reset_Out}]

set_output_delay -clock { CLK100 } -max 4.000 [get_ports {Reset_Grn_LED}]
set_output_delay -clock { CLK100 } -min 3.000 [get_ports {Reset_Grn_LED}]

set_output_delay -clock { CLK100 } -max 4.000 [get_ports {Det_Sat}]
set_output_delay -clock { CLK100 } -min 3.000 [get_ports {Det_Sat}]

set_output_delay -clock { CLK100 } -max 4.000 [get_ports {Det_Ready}]
set_output_delay -clock { CLK100 } -min 3.000 [get_ports {Det_Ready}]

#**************************************************************
# Set Clock Groups
#**************************************************************



#**************************************************************
# Set False Path
#**************************************************************



#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

