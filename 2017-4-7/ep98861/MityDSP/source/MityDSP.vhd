---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	MityDSP.vhd
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : September 21, 2007
--   Board          : Artemis POE Study Board
--   Schematic      : 4035.007.25500 Rev A
--
--   Top Level      : 4035.009.9933?, S/W, Artemis, Study
--
--   Description:	

--   History:       <date> - <Author> - Ver
--		04/29/21 - Ver 2.0
--			Added STB_TIME_RESET ( Start/Stop Command Bit 1 and 0 both must be 1)
--			will force clock time and live time to be reset as Time_Enable is started
--		11030801 - MCS
--			Added new Decode for Time_Clear_Start 
--			(Time Clear, if LSB of data = 1, then also restart)
--		01/10/2008 - MCS - 08011005
--			Module: Tested on the Instrument Control Board - Rev B with proper operation
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
library LPM;
	use lpm.lpm_components.all;

library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
entity MityDSP is 
	generic(
		-- Strobe bit positions ----------------------------------------------------------------------
		STB_TIME_START 	: integer := 0;
		STB_TIME_STOP 		: integer := 1;
		STB_TIME_CLR 		: integer := 2;
		STB_FIR_MR		: integer := 3;
		STB_FIFO_MR		: integer := 4;
		STB_READ_TRIG		: integer := 5;
		STB_SLIDE_STOP		: integer := 7;
		STB_SLIDE_MR		: integer := 8;
		STB_SLIDE_POS		: integer := 9;
		STB_SLIDE_FA		: integer := 10;
		STB_SLIDE_FR		: integer := 11;		
		STB_DSS_START		: integer := 16;
		STB_DSS_STOP		: integer := 17;
		STB_TIME_RESET		: integer := 18;	-- Only Resets Times
		iADR_RW_MAX		: integer := 31 );
	port( 
		CLK100			: in		std_logic;							-- Internal Clock
		CLK50			: in		std_logic;							-- DSP Clock
		DSP_RFF_FF		: in		std_logic;
		Read_FF_EF		: in		std_logic;
		Read_FF_FF		: in		std_logic;
		DMA_Read_Trig		: in 	std_logic;
		Read_FF_Cnt		: in		std_logic_vector( 13 downto 0 );
		DSP_AInt_Busy		: in		std_logic;
		DSP_Write_Busy		: in		std_logic;			
		Dsp_Write_FF_EF	: in		std_logic;
		Dsp_Write_FF_RData	: in		std_logic_Vector( 32 downto 0 );
		MCA_Data			: in		std_logic_vector( 31 downto 0 );
		BMCA_Data			: in		std_logic_Vector( 31 downto 0 );
		Dsp_Write_FF_Ren	: buffer	std_logic;
		Dsp_Write_Addr		: buffer	std_logic_Vector( 15 downto 0 );
		Dsp_Write_En		: buffer	std_logic;		
		Dsp_Read_Busy		: buffer	std_logic;
		o_DSP_INT			: buffer	std_logic;
		DSP_OEN			: buffer	std_logic;
		Read_FF_Ren		: buffer 	std_logic;
		Spec_Buf_Full 		: buffer	std_logic;
		Blevel_En			: buffer	std_logic;
		Read_Data_En		: buffer  std_logic;
--		SCABB_WEN			: buffer	std_logic;
		Dsp_Read_Addr		: buffer	std_logic_Vector( 15 downto 0 );
		CMD_STB			: buffer	std_logic_Vector( 31 downto 0 );
		AmpTimeReg		: buffer	std_logic_Vector( 31 downto 0 ) );						
end MityDSP;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture behavioral of MityDSP is
	constant Spec_Buf_Full_Len				: integer := 16384 - 5000;
	constant XILINX_DMA_TRANSFER_SIZE			: integer := 512;		-- In Xilinx: Altera_Core.vhd module, "DMA_TRANSFER_SIZE
	-- Write fifo is 64 deep in XIlinx and 64 deep in Altera for a total of 128. 
	-- At 50Mhz ( or 20 ns ) it would take 20ns * 128 = 2560ns to process all data
	-- Therefore the Hold-Off Time will be for 5 us between Reads
	constant Dsp_Read_Hold_Cnt_Max			: integer := 5000 / 20;		-- wait 10us at DSP clock rate of 50Mhz 

	constant Dsp_Read_Idle 					: integer := 0;
	constant Dsp_Read_DAta					: integer := 1;
	constant Dsp_Read_Done					: integer := 2;
	constant Dsp_Read_DMA					: integer := 3;
	constant Dsp_Read_DMA_DOne				: integer := 4;
	constant Dsp_Read_Irq0					: integer := 5;
	constant Dsp_Read_Irq1					: integer := 6;
	constant Dsp_Read_HoldOff				: integer := 7;
	constant Dsp_Read_FF_Full0				: integer := 8;
	constant Dsp_Read_FF_Full1				: integer := 9;
	constant Dsp_Read_FF_Full2				: integer := 10;
	
	constant kwCmd_Write					: integer := 16#00#;
	constant kwCmd_SetDef					: integer := 16#01#;
	constant kwCmd_AmpTime					: integer := 16#02#;
	constant kwCmd_Read_Data_Enable_Disable 	: integer := 16#03#;
	constant kwCmd_Time_Start_Stop			: integer := 16#04#;
	constant kwCmd_Time_Clear				: integer := 16#05#;
	constant kwCmd_Fir_Mr					: integer := 16#06#;
	constant kwCmd_Fifo_mr					: integer := 16#07#;
--	constant kwCmd_SCABB					: integer := 16#08#;
	constant kwCmd_Blevel_Start_Stop			: integer := 16#09#;
	constant kwCmd_Dss_Start_Stop				: integer := 16#0A#;
	constant kwCmd_Slide_Move				: integer := 16#0B#;

	constant kwCmd_Read						: integer := 16#80#;
--	constant kwCmd_Read_SCABB				: integer := 16#81#;

	constant Max_Addr						: integer := 31;
		
	-- FIFO Write States
	constant FFW_IDLE 						: integer := 0;
	constant FFW_WRITE_CMD					: integer := 1;
	constant FFW_WRITE_ADDR					: integer := 2;
	constant FFW_WRITE_DATA					: integer := 3;
--	constant FFW_WRITE_SCABB					: integer := 4;
	constant FFW_Pause						: integer := 5;	-- keep last
	-- FIFO Write States	

	-- Signal Declarations -----------------------------------------------------------------------
	signal Auto_Restart_Latch				: std_logic;
	
	signal DSP_INT							: std_logic;
	signal Dsp_Read_State 					: integer range 0 to Dsp_Read_FF_Full2 := 0;
	
	signal Dsp_Read_Hold_Cnt					: integer range 0 to Dsp_Read_Hold_Cnt_Max := 0;
	signal Dsp_Read_Cnt						: std_logic_Vector( 13 downto 0  ) := (others=>'0');
	signal Dsp_FF_Int						: std_logic := '0';
	
	signal Dsp_DMA_Latch					: std_logic := '0';
	signal Dsp_Read_Trig_Latch				: std_logic := '0';
	signal FFW_STATE						: integer range 0 to FFW_Pause := 0;

	signal Dsp_Write_End_Addr				: std_logic_Vector(  7 downto 0 ) := (others=>'0');
	signal Write_Seq						: std_logic := '0';
--	signal SCABB_WSEQ						: std_logic := '0';
	-- Signal Declarations -----------------------------------------------------------------------

begin	
	----------------------------------------------------------------------------------------------
	clock_proc : process( clk100 )
	begin
		if( rising_edge( clk100 )) then

			if( Dsp_Read_State = Dsp_Read_DMA )
				then Dsp_DMA_Latch <= '0';
			
			elsif(( DMA_Read_Trig = '1'  ) or ( Read_FF_FF = '1' ) or ( Read_FF_Cnt(13) = '1' ) or ( Spec_Buf_Full = '1' ))
				then Dsp_DMA_Latch <= '1';
			end if;	
				
			------------------------------------------------------------------------------
			-- Spectrum Buffer Full - Allow for Spectum processing when DSP Read FIFO has 
			-- enough room for the entier Spectrum Buffer 
			if( Read_FF_Cnt > Spec_Buf_Full_Len )
				then Spec_Buf_Full <= '1';
				else Spec_Buf_Full <= '0';
			end if;					
		end if;
	end process;
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	dsp_clk_proc : process( CLK50 )
	begin
		if( rising_edge( CLK50 )) then
			o_DSP_INT 		<= not( DSP_INT );					-- output to dsp

			if( CMD_STB( STB_READ_TRIG ) = '1' )
				then Dsp_Read_Trig_Latch <= '1';
			elsif( Dsp_Read_State = Dsp_Read_DAta )
				then Dsp_Read_Trig_Latch <= '0';
			end if;			
			
			case Dsp_Read_State is			
				when Dsp_Read_Idle =>
					Dsp_Read_Busy 		<= '0';
					Dsp_Oen			<= '0';				-- Don't drive the data bus
					Dsp_Int			<= '0';				-- Inactive
					Read_FF_Ren		<= '0';				-- Don't Read the FIFO
					Dsp_Read_Hold_Cnt	<= 0;
					Dsp_Read_Cnt		<= ext( Read_FF_Cnt, 14 );
					
					-- Not busy writing - ok to process
					if(( Dsp_Write_Busy = '0' ) and ( Dsp_Read_Trig_Latch = '1' ))
						then Dsp_Read_State <= Dsp_Read_Data;
					
					-- Local Read FIFO is Full AND DSP is not in process of a DMA Cycle
					elsif(( DSp_Rff_ff = '1' ) and ( DSP_AInt_Busy = '0' ))
						then Dsp_Read_State <= Dsp_Read_FF_Full0;
						
					-- Not writing and there is data to be read (output fifo is not empty )
					elsif(( Dsp_Write_Busy = '0' ) and ( conv_integer( Read_FF_Cnt ) > 0 ) and ( Dsp_DMA_Latch = '1' ))		-- Force a Read
						then Dsp_Read_State <= Dsp_Read_DMA;
					elsif(( Dsp_Write_Busy = '0' ) and ( Read_FF_FF = '1' ))		-- FIfo is Full
						then Dsp_Read_State <= Dsp_Read_DMA;
					elsif(( Dsp_Write_Busy = '0' ) and (conv_integer( Read_FF_Cnt ) >= XILINX_DMA_TRANSFER_SIZE ))  
						then Dsp_Read_State <= Dsp_Read_DMA;
					end if;			
				
				-- Read the Data ( No FIFO is involved )
				when Dsp_Read_Data =>
					Dsp_Read_Busy 		<= '1';
					Dsp_Oen			<= '1';
					Dsp_Int			<= '0';				-- Inactive
					Read_FF_Ren		<= '0';				-- Don't Read the FIFO
					Dsp_Read_Hold_Cnt	<= 0;
					Dsp_Read_State		<= Dsp_Read_Done;
					
				-- Read_Done is necessary because it fires the interrupt
				when Dsp_Read_Done =>
					Dsp_Read_Busy 		<= '0';
					Dsp_Oen			<= '1';				-- Drive the data bus
					Dsp_Int			<= '0';				-- Inactive
					Read_FF_Ren		<= '0';				-- Don't Read the FIFO
					Dsp_Read_Hold_Cnt	<= 0;
					Dsp_Read_State		<= Dsp_Read_Idle;							
					
					
				-- Read the DMA Data
				when Dsp_Read_DMA =>
					Dsp_Read_Busy 		<= '0';
					Dsp_Oen			<= '1';				-- Drive the Data Bus
					Dsp_Int			<= '0';				-- Inactive
					Dsp_Read_Hold_Cnt	<= 0;
					if( Dsp_RFF_FF = '1' )
						then Dsp_Read_State <= Dsp_Read_FF_Full0;
					
					-- if the DSP Read FIFO is not full					
					else
						Read_FF_Ren		<= '1';			-- Don't Read the FIFO
						Dsp_Read_Cnt 		<= Dsp_Read_Cnt - 1;
						if( conv_integer( Dsp_Read_Cnt ) = 1 )		-- was 1
							then Dsp_Read_State <= Dsp_Read_DMA_Done;
						end if;
					end if;
					
				-- FIFO is full during a "Read" ( only can get here from "Dsp_Read_DMA )
				when Dsp_Read_FF_Full0 =>
					Dsp_Read_Busy 		<= '0';
					Dsp_Oen			<= '0';				-- Don't Drive the Data Bus					
					Dsp_Int			<= '1';				-- Inactive
					Dsp_Read_Hold_Cnt	<= 0;
					Read_FF_Ren		<= '0';				-- Don't Read the FIFO
					Dsp_Read_State		<= Dsp_Read_FF_Full1;
					

				when Dsp_Read_FF_Full1 =>
					Dsp_Read_Busy 		<= '0';
					Dsp_Oen			<= '0';				-- Don't Drive the Data Bus					
					Dsp_Int			<= '1';				-- Inactive
					Dsp_Read_Hold_Cnt	<= 0;
					Read_FF_Ren		<= '0';				-- Don't Read the FIFO
					Dsp_Read_State		<= Dsp_Read_FF_Full2;
					
				when Dsp_Read_FF_Full2 =>
					Dsp_Read_Busy 		<= '0';
					Dsp_Oen			<= '0';				-- Don't Drive the Data Bus					
					Dsp_Int			<= '0';				-- Inactive
					Dsp_Read_Hold_Cnt	<= Dsp_Read_Hold_Cnt + 1;
					Read_FF_Ren		<= '0';				-- Don't Read the FIFO
					
					-- Xilinx Fifo no longer full, Write is not in progress and the Read FIFO is not empty
					if( Dsp_Read_Hold_Cnt > Dsp_Read_Hold_Cnt_Max )
						then Dsp_Read_State <= Dsp_Read_Idle;		-- Go back to Idle
					elsif(( Dsp_Rff_FF = '0' ) and ( Dsp_Write_Busy = '0' ) and ( conv_integer( Dsp_Read_Cnt ) > 0 )) -- ( Read_FF_EF = '0' ))	
						then Dsp_Read_State <= Dsp_Read_DMA;																
					end if;
					

				-- Read_Done is necessary because it fires the interrupt
				when Dsp_Read_DMA_Done =>
					Dsp_Read_Busy 		<= '0';
					Dsp_Oen			<= '1';				-- Drive the data bus
					Dsp_Int			<= '0';				-- Inactive
					Read_FF_Ren		<= '0';				-- Don't Read the FIFO
					Dsp_Read_Hold_Cnt	<= 0;					

					-- Only move to generate the interrupt of the DMA is not in process
					if( DSP_AInt_Busy = '0' )
						then Dsp_Read_State		<= Dsp_Read_Irq0;							
					end if;
	
				
				-- Fire the Interrupt for 1 clock cycles
				when Dsp_Read_Irq0 =>
					Dsp_Read_Busy 		<= '0';
					Dsp_Oen			<= '0';				-- Drive the data bus
					Dsp_Int			<= '1';				-- Inactive
					Read_FF_Ren		<= '0';				-- Don't Read the FIFO
					Dsp_Read_Hold_Cnt	<= 0;
					Dsp_Read_State		<= Dsp_Read_Irq1;							

				when Dsp_Read_Irq1 =>
					Dsp_Read_Busy 		<= '0';
					Dsp_Oen			<= '0';				-- Drive the data bus
					Dsp_Int			<= '1';				-- Inactive
					Read_FF_Ren		<= '0';				-- Don't Read the FIFO
					Dsp_Read_Hold_Cnt	<= 0;
					Dsp_Read_State		<= Dsp_Read_HoldOff;							

				-- Don't allow any more reads for some time to allow processing of Write Commands
				when Dsp_Read_HoldOff =>
					Dsp_Read_Busy 		<= '0';
					Dsp_Oen			<= '0';				-- Drive the data bus
					Dsp_Int			<= '0';				-- Inactive
					Read_FF_Ren		<= '0';				-- Don't Read the FIFO
					Dsp_Read_Hold_Cnt	<= Dsp_Read_Hold_Cnt + 1;
					if( Dsp_Read_Hold_Cnt = Dsp_Read_Hold_Cnt_Max )
						then Dsp_Read_State		<= Dsp_Read_Idle;							
					end if;								
					
				when others =>
					Dsp_Read_Busy 		<= '0';
					Dsp_Oen			<= '0';				-- Drive the data bus
					Dsp_Int			<= '0';				-- Inactive
					Read_FF_Ren		<= '0';				-- Don't Read the FIFO
					Dsp_Read_Hold_Cnt	<= 0;
					Dsp_Read_State 	<= Dsp_Read_Idle;
			end case;

						
			
			------------------------------------------------------------------------------------
			-- Write to FPGA State Machine 
			case FFW_STATE is
				when FFW_IDLE => 
--					SCABB_WEN						<= '0';
					Dsp_Write_En					<= '0';

					-- If the Internal FIFO is not empty then process what is at the output
					if( Dsp_Write_FF_EF = '0' ) then		
						Dsp_Write_FF_Ren			<= '1';
						-- Check the MSB of the Write DAta (Read data from the Write FIFO)
						-- 0 = Address
						-- 1 = Data
						if( Dsp_Write_FF_RData( 32 ) = '0' ) then					-- Address Found
							case( conv_integer( Dsp_Write_FF_RData( 31 downto 24 ))) is
								-- Command to Read back data
								when kwCmd_Read				=> 
									Dsp_Read_Addr 				<= '0' & Dsp_Write_FF_RData( 14 downto 0 );		-- Latch Read Address
									CMD_STB( STB_READ_TRIG ) 	<= '1';									
									FFW_State 				<= FFW_WRITE_CMD;
								
--								-- Command to Read back SCA lookup data
--								-- 	the lower address contains the pointer to the lookup table
--								when kwCmd_Read_SCABB			=>
--									Dsp_Read_Addr				<= '1' & Dsp_Write_FF_Rdata( 14 downto 0 );
--									CMD_STB( STB_READ_TRIG )		<= '1';
--									FFW_STATE					<= FFW_WRITE_CMD;
																		
								-- Command to Write Data							
								when kwCmd_Write 					=> 
									Dsp_Write_Addr 			<= '0' & Dsp_Write_FF_RData( 14 downto  0 );		-- Latch Write Address
									FFW_State 				<= FFW_WRITE_ADDR;								
								
--								-- Command to Write data to the SCABB Register
--								when kwCmd_SCABB					=> 
--									Dsp_Write_Addr 			<= '1' & Dsp_Write_FF_RData( 14 downto 0 );		-- Latch Write Address
--									FFW_State 				<= FFW_WRITE_ADDR;
														
								when kwCmd_AmpTime					=> 
									AmpTimeReg 				<= ext( Dsp_Write_FF_RData( 15 downto 0 ), 32 ); 	-- use only the lower 16 bits
									FFW_State 				<= FFW_WRITE_CMD;
									
								when kwCmd_Read_Data_Enable_Disable 	=> 
									Read_Data_En 				<= Dsp_Write_FF_RData(0);
									FFW_State 				<= FFW_WRITE_CMD;

								when kwCmd_Time_Start_Stop			=> 
									case conv_integer( Dsp_Write_FF_RData( 2 downto 0 )) is
										when 1 => Cmd_Stb( STB_TIME_START ) 	<= '1';		-- Start Acquisutuin
										when 2 => Cmd_Stb( STB_TIME_STOP )  	<= '1';		-- Stop Acquisition
										when 3 => Cmd_Stb( STB_TIME_CLR  )		<= '1';		-- Clear Spectrum
										when 4 => Cmd_Stb( STB_TIME_CLR  )		<= '1';		-- Clear and Restart
												Auto_Restart_Latch 			<= '1';
										when 5 => Cmd_Stb( STB_TIME_RESET ) 	<= '1';		-- Mapping- Restart
										when others => 							 -- do nothing
									end case;									
									FFW_State 				<= FFW_WRITE_CMD;
									
--								when kwCmd_Time_Clear				=> 
--									CMD_STB(STB_TIME_CLR ) 				<= '1';	-- Always Clear
--									if( Dsp_Write_FF_RData(0) = '1' )
--										then Auto_Restart_Latch <= '1';
--									end if;
--									FFW_State 				<= FFW_WRITE_CMD;
									
								when kwCmd_Fir_Mr					=> 
									CMD_STB(STB_FIR_MR ) 		<= '1';
									FFW_State 				<= FFW_WRITE_CMD;
									
								when kwCmd_Fifo_mr					=> 
									CMD_STB(STB_FIFO_MR ) 		<= '1';
									FFW_State 				<= FFW_WRITE_CMD;
									
								when kwCmd_Blevel_Start_Stop		=> 
									Blevel_En					<= Dsp_Write_FF_RData(0);
									FFW_State 				<= FFW_WRITE_CMD;
									
								when kwCmd_Dss_Start_Stop			=> 
									if( Dsp_Write_FF_RData(0) = '1' )
										then CMD_STB( STB_DSS_START ) <= '1';
										else CMD_STB( STB_DSS_STOP ) <= '1';
									end if;
									FFW_State <= FFW_WRITE_CMD;
									
								when kwCmd_Slide_Move				=>
									if( Dsp_Write_FF_RData(0) = '1' ) then Cmd_Stb( STB_SLIDE_STOP ) 	<= '1'; 	end if;					
									if( Dsp_Write_FF_RData(1) = '1' ) then Cmd_Stb( STB_SLIDE_MR ) 		<= '1';	end if;					
									if( Dsp_Write_FF_RData(2) = '1' ) then Cmd_Stb( STB_SLIDE_POS ) 		<= '1'; 	end if;					
									if( Dsp_Write_FF_RData(3) = '1' ) then Cmd_Stb( STB_SLIDE_FA ) 		<= '1'; 	end if;
									if( Dsp_Write_FF_RData(4) = '1' ) then Cmd_Stb( STB_SLIDE_FR )		<= '1'; 	end if;
									FFW_State <= FFW_WRITE_CMD;									
									
								when others						=> 
									FFW_State <= FFW_WRITE_CMD;
															end case;
--						-- SCABB Data
--						elsif( Dsp_Write_Addr(15) = '1' ) then							
--							Dsp_Write_En		<= '1';			-- Copies data into PARAM[]
--							FFW_State			<= FFW_WRITE_SCABB;
						-- Else "Normal" Write Data
						else
							Dsp_Write_En		<= '1';			-- copies data into PARAM[]
							FFW_State			<= FFW_WRITE_DATA;					
						end if;
					elsif(( Auto_Restart_Latch = '1' ) and ( MCA_Data(31) = '0' ) and ( BMCA_Data(31) = '0' )) then
						Auto_Restart_Latch 			<= '0';
						CMD_STB( STB_TIME_START ) 	<= '1';
						FFW_State 				<= FFW_WRITE_CMD;
					else					
						CMD_STB					<= (others=>'0');
						Dsp_Write_FF_Ren			<= '0';					
					end if;									-- if Dsp_Write_FF_EF = '0' 			
				
				-- Command to STart Writing Data ----------------------------------------------
				when FFW_WRITE_ADDR =>
--					SCABB_WEN						<= '0';
					Dsp_Write_En					<= '0';
					Dsp_Write_FF_Ren				<= '0';					
					FFW_STATE						<= FFW_Pause;										
				-- Command to STart Writing Data ----------------------------------------------
				
				-- Process Written Data -------------------------------------------------------
				when FFW_WRITE_DATA =>
--					SCABB_WEN						<= '0';
					Dsp_Write_En					<= '0';
					Dsp_Write_FF_Ren				<= '0';					
					FFW_STATE						<= FFW_Pause;
				-- Process Written Data -------------------------------------------------------
				
--				-- Process Written SCABB Data -------------------------------------------------
--				when FFW_WRITE_SCABB =>
--					SCABB_WEN						<= '1';		-- now copy "PARAM" to SCABB Memory
--					Dsp_Write_En					<= '0';
--					Dsp_Write_FF_Ren				<= '0';					
--					FFW_STATE						<= FFW_Pause;
--				-- Process Written SCABB Data -------------------------------------------------

				-- Process Command Data -------------------------------------------------------
				when FFW_WRITE_CMD =>
--					SCABB_WEN						<= '0';
					CMD_STB						<= (others=>'0');
					Dsp_Write_En					<= '0';
					Dsp_Write_FF_Ren				<= '0';					
					FFW_STATE						<= FFW_Pause;
				-- Process Written Data -------------------------------------------------------

				-- 1 pipeline delay after "Dsp_Write_FF_Ren" goes active to allow for Dsp_Write_FF_EF to catch-up				
				when FFW_Pause =>				
--					SCABB_WEN						<= '0';
					CMD_STB						<= (others=>'0');
					Dsp_Write_FF_Ren				<= '0'; 						-- Advances the Read Pointer
					Dsp_Write_En					<= '0';
					FFW_STATE 					<= FFW_IDLE;
				-- 1 pipeline delay after "Dsp_Write_FF_Ren" goes active to allow for Dsp_Write_FF_EF to catch-up
				
				when others =>
--					SCABB_WEN						<= '0';
					Dsp_Write_En					<= '0';
					Dsp_Write_FF_Ren				<= '0'; 						-- Advances the Read Pointer
					FFW_STATE 					<= FFW_IDLE;
			end case;
			-- Write to FPGA State Machine 
			------------------------------------------------------------------------------------
			
		end if;
	end process;			
	----------------------------------------------------------------------------------------------

	
---------------------------------------------------------------------------------------------------
end behavioral;			-- MityDSP.vhd
---------------------------------------------------------------------------------------------------
