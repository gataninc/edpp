## Generated SDC file "AD5453.sdc"

## Copyright (C) 1991-2010 Altera Corporation
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, Altera MegaCore Function License 
## Agreement, or other applicable license agreement, including, 
## without limitation, that your use is for the sole purpose of 
## programming logic devices manufactured by Altera and sold by 
## Altera or its authorized distributors.  Please refer to the 
## applicable agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 10.0 Build 218 06/27/2010 SJ Full Version"

## DATE    "Wed Sep 22 08:19:27 2010"

##
## DEVICE  "EP3C40U484C7"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {CLK100} -period 10.000 -waveform { 0.000 5.000 } [get_ports {CLK100}]
create_clock -name {CLK50}  -period 20.000 -waveform { 0.000 10.000 } [get_ports {CLK50}]


#**************************************************************
# Create Generated Clock
#**************************************************************



#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************

derive_clock_uncertainty


#**************************************************************
# Set Input Delay
#**************************************************************

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {DSP_RFF_FF}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {DSP_RFF_FF}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {Read_FF_EF}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {Read_FF_EF}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {Read_FF_FF}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {Read_FF_FF}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {DMA_Read_Trig}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {DMA_Read_Trig}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {Read_FF_Cnt[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {Read_FF_Cnt[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {DSP_AInt_Busy}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {DSP_AInt_Busy}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {DSP_Write_Busy}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {DSP_Write_Busy}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {Dsp_Write_FF_EF}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {Dsp_Write_FF_EF}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {Dsp_Write_FF_RData[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {Dsp_Write_FF_RData[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {MCA_Data[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {MCA_Data[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK50}]  5.778 [get_ports {BMCA_Data[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK50}]  3.111 [get_ports {BMCA_Data[*]}]

#**************************************************************
# Set Output Delay
#**************************************************************

          
set_output_delay -clock { CLK50 } -max 5.000 [get_ports {Dsp_Write_FF_Ren}]
set_output_delay -clock { CLK50 } -min 3.000 [get_ports {Dsp_Write_FF_Ren}]

set_output_delay -clock { CLK50 } -max 5.000 [get_ports {Dsp_Write_Addr[*]}]
set_output_delay -clock { CLK50 } -min 3.000 [get_ports {Dsp_Write_Addr[*]}]

set_output_delay -clock { CLK50 } -max 5.000 [get_ports {Dsp_Write_En}]
set_output_delay -clock { CLK50 } -min 3.000 [get_ports {Dsp_Write_En}]

set_output_delay -clock { CLK50 } -max 5.000 [get_ports {Dsp_Read_Busy}]
set_output_delay -clock { CLK50 } -min 3.000 [get_ports {Dsp_Read_Busy}]

set_output_delay -clock { CLK50 } -max 5.000 [get_ports {o_DSP_INT}]
set_output_delay -clock { CLK50 } -min 3.000 [get_ports {o_DSP_INT}]

set_output_delay -clock { CLK50 } -max 5.000 [get_ports {DSP_OEN}]
set_output_delay -clock { CLK50 } -min 3.000 [get_ports {DSP_OEN}]

set_output_delay -clock { CLK50 } -max 5.000 [get_ports {Read_FF_Ren}]
set_output_delay -clock { CLK50 } -min 3.000 [get_ports {Read_FF_Ren}]

set_output_delay -clock { CLK50 } -max 5.000 [get_ports {Spec_Buf_Full}]
set_output_delay -clock { CLK50 } -min 3.000 [get_ports {Spec_Buf_Full}]

set_output_delay -clock { CLK50 } -max 5.000 [get_ports {Blevel_En}]
set_output_delay -clock { CLK50 } -min 3.000 [get_ports {Blevel_En}]

set_output_delay -clock { CLK50 } -max 5.000 [get_ports {Read_Data_En}]
set_output_delay -clock { CLK50 } -min 3.000 [get_ports {Read_Data_En}]

set_output_delay -clock { CLK50 } -max 5.000 [get_ports {SCABB_WEN}]
set_output_delay -clock { CLK50 } -min 3.000 [get_ports {SCABB_WEN}]

set_output_delay -clock { CLK50 } -max 5.000 [get_ports {Dsp_Read_Addr[*]}]
set_output_delay -clock { CLK50 } -min 3.000 [get_ports {Dsp_Read_Addr[*]}]

set_output_delay -clock { CLK50 } -max 5.000 [get_ports {CMD_STB[*]}]
set_output_delay -clock { CLK50 } -min 3.000 [get_ports {CMD_STB[*]}]

set_output_delay -clock { CLK50 } -max 5.000 [get_ports {AmpTimeReg[*]}]
set_output_delay -clock { CLK50 } -min 3.000 [get_ports {AmpTimeReg[*]}]

#**************************************************************
# Set Clock Groups
#**************************************************************



#**************************************************************
# Set False Path
#**************************************************************



#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

