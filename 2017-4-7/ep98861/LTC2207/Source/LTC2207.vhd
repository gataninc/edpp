---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	LTC2207
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : September 21, 2007
--   Board          : XScope Instrument Control Board
--   Schematic      : 4035.007.27000 Rev B
--
--   Top Level      : 4035.009.99330, S/W, XScope, Instrument Control
--
--   Description:	
--			This module interfaces to the Linear Technology LTC1867L 8 channel 16-bit A/D converter
--			It has a control word that defines the opeational parameers as well as steering the mux
--			The A/D data on the output is based on the previous input's mux value.
--			
--			The input from the A/D converter is signed two's complement format and passes it on as such
--
--   History:       <date> - <Author> - Ver
--		04/05/12 - MCS
--			Added same 1/2 Gain to Dout (to Discriminators )
--		03/27/12 - MCS
--			Added input for 1/2 Gain, which cuts the DoutM Gain by a factor of 2
--		01/10/2008 - MCS - 08011005
--			Module: Tested on the Instrument Control Board - Rev B with proper operation
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
library LPM;
	use lpm.lpm_components.all;

library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;
---------------------------------------------------------------------------------------------------
	
---------------------------------------------------------------------------------------------------
entity LTC2207 is 
	port( 
		CLK100	: in		std_logic;
		DIN		: in		std_Logic_Vector( 15 downto 0 );		
		PA_Reset	: in		std_logic;
		OTR		: in		std_Logic;					-- Logic High
		RAND		: in		std_logic;
		Half_Gain	: in		std_logic;
		Gain		: in		std_logic_vector( 31 downto 0 );
		Raw_Dout	: buffer	std_logic_Vector( 17 downto 0 );
		DOUT		: buffer	std_logic_Vector( 17 downto 0 );
		DoutM	: buffer	std_logic_Vector( 17 downto 0 ) );
end LTC2207;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture behavioral of LTC2207 is
     ----------------------------------------------------------------------------------------------
	constant	Mout_LSB		: integer := 16;
	
	-- Constant Declarations ---------------------------------------------------------------------
     signal Gain_C100         : std_logic_Vector( 31 downto 0 );

	signal DReg_A			: std_logic_Vector( 15 downto 0 ) := (others=>'0');
	signal DReg_B			: std_logic_Vector( 15 downto 0 ) := (others=>'0');
	signal Din_Reg			: std_logic_Vector( 15 downto 0 ) := (others=>'0');	
	signal Dout_Reg		: std_logic_Vector( 15 downto 0 ) := (others=>'0');
	signal Dout_Reg2		: std_logic_Vector( 15 downto 0 ) := (others=>'0');
	
	signal Mult_out		: std_logic_Vector( 47 downto 0 )  := (others=>'0');
	
	signal OTR_A			: std_logic := '0';
	signal OTR_B			: std_Logic := '0';
	
	----------------------------------------------------------------------------------------------
	component LTC_GAIN
		port(
			clock		: in 	std_logic ;
			dataa		: in		std_logic_Vector (15 downto 0);
			datab		: in		std_logic_Vector (31 downto 0);
			result		: out	std_logic_Vector (47 downto 0 ) );
	end component;
	----------------------------------------------------------------------------------------------
begin		
     ----------------------------------------------------------------------------------------------
	Din_Proc : process( CLK100 )
	begin
		if( rising_edge( CLK100 )) then
               Gain_C100 <= Gain;
			
			-- Unrandomize the data - XOR all the bits with the LSB ----------------------------

			OTR_A	<= OTR;		-- Pipeline Out-of-Range Indication
			DReg_A	<= Din;		-- Pipeline Data

			OTR_B	<= OTR_A;      -- Pipeline OTR_A to match it up with the "Unrandomized data)
			DReg_B(0) <= DReg_A(0);
			for i in 1 to 15 loop
				if( RAND = '1' )
					then DReg_B(i) <= DReg_A(i) XOR  DReg_A(0);
					else DReg_B(i) <= DReg_A(i);
				end if;
			end loop;
			-- Unrandomize the data - XOR all the bits with the LSB ----------------------------
	
			-- Input is two's complment so leave the number format alone and pass it on to the output
			if( OTR_B = '0' ) 
				then Din_Reg	<= DReg_B;	
			elsif( DReg_B(15) = '0' )	-- Two's Complement Full Scale
				then Din_Reg <= x"7FFF";	-- Signed Full Scale
				else Din_Reg <= x"8000";	-- Signed Min Scale		
			end if;
			-- Input is two's complment so leave the number format alone and pass it on to the output
		end if;
	end process;
     ----------------------------------------------------------------------------------------------
	
	

	-- Multiplier has 1 pipeline stage
	LTC_GAIN_INST : LTC_GAIN
		port map(
			clock			=> clk100,
			dataa			=> Din_Reg,
			datab			=> Gain_C100,
			result			=> Mult_Out );
     ----------------------------------------------------------------------------------------------

     ----------------------------------------------------------------------------------------------
	
	Dout_Proc : process( CLK100 )
	begin
		if( rising_edge( CLK100 )) then			
			Raw_Dout		<= sxt( Din_Reg, 18 );						-- Raw A/D Output			
			Dout_Reg		<= Din_Reg;			
			if( Half_Gain = '1' ) then 
				Dout			<= sxt( Dout_Reg( 15 downto 1 ), 18 );			-- Output to Discriminators
				DoutM		<= Mult_out( 17+Mout_LSB+1 downto Mout_LSB+1 );	-- Output to Main Channel
			else
				Dout			<= sxt( Dout_Reg, 18 );						-- Output to Discriminators
				DoutM		<= Mult_out( 17+Mout_LSB downto Mout_LSB );		-- Output to Main Channel
			end if;
		end if;
	end process;
     ----------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
end behavioral;			-- LTC2207
---------------------------------------------------------------------------------------------------

