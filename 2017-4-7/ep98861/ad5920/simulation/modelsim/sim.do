setenv ALTERA_PATH "C:/Modelsim/ALtera/Vhdl"
# setenv ALTERA_PATH "C:/Modeltech_pe_6.6/ALtera/Vhdl"
vcom -reportprogress 300 -work work {ad5920.vho}
# vcom -reportprogress 300 -work work {tb5453.vhd}
vcom -reportprogress 300 -work work {tb.vhd}
vsim -sdftyp {/U=ad5920_vhd.sdo} work.tb
do wave.do
run 200 us
