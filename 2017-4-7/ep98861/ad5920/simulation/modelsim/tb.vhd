library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

     entity tb is
     end tb;
     
architecture test_bench of tb is

     signal CLK50		  : std_logic := '0';
     signal val            : std_logic_vector( 31 downto 0 );
     signal pot_cs         : std_logic;          
     signal pot_clk        : std_logic;
     signal pot_dout       : std_logic;
     signal Pot_Val           : std_logic_Vector( 7 downto 0 ) := (others=>'0');
     signal Pot_Cnt           : integer range 0 to 7 := 0;


     ---------------------------------------------------------------------------------------------------
     component AD5920 is
          port(
               CLK50		: in		std_logic;
               val            : in		std_logic_vector( 31 downto 0 );
               pot_cs         : buffer  std_logic;          
               pot_clk        : buffer  std_logic;
               pot_dout       : buffer  std_logic );
     end component AD5920;
     ---------------------------------------------------------------------------------------------------
begin
     CLK50 <= not( CLK50 ) after 10 ns;
     
     U : AD5920 
          port map(
               CLK50		=> CLK50,
               val            => Val,
               pot_cs         => Pot_Cs,
               pot_clk        => Pot_Clk,
               pot_dout       => Pot_Dout );
     ---------------------------------------------------------------------------------------------------

     ---------------------------------------------------------------------------------------------------
     tb_proc : process
     begin
          val <= x"00000000";
          
          wait for 1 us;
          assert false
               report "-------------------------------------------------------------------------------"
               severity note;
          assert false
               report "Diffent Values to the Pot"
               severity note;
          
          for i in 0 to 7 loop
               val(i) <= '1';
               if( i > 0 )
                    then val(i-1) <= '0';
               end if;                    
               wait until rising_edge( Pot_Cs );
               
               assert( val( 7 downto 0 ) = Pot_Val )
                    report "     Error in Sensed Value"
                    severity error;               
              
               wait for 500 ns;
          end loop;
          
          assert false
               report "End of Simulation"
               severity failure;
     end process;
     ---------------------------------------------------------------------------------------------------
     
     ---------------------------------------------------------------------------------------------------
     
     pot_proc : process( Pot_Cs, Pot_Clk )
     begin
          if( Pot_Cs = '1' )
               then Pot_Cnt <= 0;
          elsif( rising_Edge( Pot_Clk )) then
               Pot_Val( 7-Pot_Cnt )  <= Pot_Dout;
               if( Pot_Cnt < 7 )
                    then Pot_Cnt             <= Pot_Cnt + 1;
               end if;
          end if;     
     end process;
---------------------------------------------------------------------------------------------------
end test_bench;			-- PreampCtrl.vhd
---------------------------------------------------------------------------------------------------

    
    
    