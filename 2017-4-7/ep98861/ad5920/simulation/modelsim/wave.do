onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix hexadecimal /tb/clk50
add wave -noupdate -format Literal -radix hexadecimal /tb/val
add wave -noupdate -format Logic -radix hexadecimal /tb/pot_cs
add wave -noupdate -format Logic -radix hexadecimal /tb/pot_clk
add wave -noupdate -format Logic -radix hexadecimal /tb/pot_dout
add wave -noupdate -foramt Literal -radix decimal /tb/pot_cnt
add wave -noupdate -format Logic -radix hexadecimal /tb/pot_val

TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {7789617 ps} 0}
configure wave -namecolwidth 120
configure wave -valuecolwidth 53
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
update
WaveRestoreZoom {878775 ps} {1614750 ps}
