---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	ad5920.vhd
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
-- 	Author         : Michael C. Solazzi
--   Created        : September 29, 2010
--   Board          : Artemis DPP Board
--   Schematic      : 4035.007.25500 Group 130
--
--	Top Level		: 4035.003.98861 S/W, Artemis,
--
--   Description:	
--			This module looks for changes in the Value to be sent to the device,
--             then it creates the serial string to actually program it.
--
--             pot_cs must go active before the cycle
--             pot_dout changes on teh falling edge of the output clock (pot_clk)
--             the data is latched into the device on the rising edge of (pot_clk)
--             Data buit 7 goes first
--             clock high/low is 120ns min pulse width

--	History
--		01/05/10 - MCS - Ver 1.00
--			Initial Creation
---------------------------------------------------------------------------------------------------
library LPM;
     use lpm.lpm_components.all;
     
     
library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

---------------------------------------------------------------------------------------------------
entity ad5920 is
	port(
		CLK50		: in		std_logic;
		val            : in		std_logic_vector( 31 downto 0 );
          pot_cs         : buffer  std_logic;          
          pot_clk        : buffer  std_logic;
          pot_dout       : buffer  std_logic );
end ad5920;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture behavioral of ad5920 is 
     constant Clock_Per       : integer := 20;         -- 20Mhz
     constant Clock_Cnt_Max   : integer := 120 / CLOCK_PER;               
     
     constant St_Idle         : integer := 0;
     constant St_Clock_Low    : integer := 1;
     constant St_Clock_High   : integer := 2;
     constant St_Pot_Done     : integer := 3;
     
     signal bit_Cnt           : integer range 0 to 7;

     signal Clock_Cnt         : integer range 0 to Clock_Cnt_Max;
     signal Clock_Cnt_Tc      : std_logic;

     signal ld_pot            : std_logic := '0';
     signal Pot_State         : integer range 0 to St_Pot_Done := St_Idle;               

     signal Val_Del           : std_logic_Vector( 31 downto 0 );
begin
     Clock_Cnt_Tc <= '1' when ( Clock_Cnt = Clock_Cnt_Max ) else '0';
     -----------------------------------------------------------------------------------------------
     clock_process : process( clk50 )
     begin     
          if( rising_Edge( clk50 )) then
               -- Pipeline Value -------------------------------------------------------------------
               Val_Del <= Val;  
               -- Pipeline Value -------------------------------------------------------------------
               
               -- If the Value has changed ---------------------------------------------------------
               if( Val /= Val_Del )
                    then Ld_Pot <= '1';
               elsif( Pot_State = St_Pot_Done )
                    then Ld_Pot <= '0';
               end if;
               -- If the Value has changed ---------------------------------------------------------
               
               if( Pot_State = St_Idle )
                    then Clock_Cnt <= 0;
               elsif( Clock_Cnt_Tc = '1' )
                    then Clock_Cnt <= 0;
                    else Clock_Cnt <= Clock_Cnt + 1;
               end if;

               case Pot_State is
                    when St_Idle =>
                         pot_clk        <= '0';
                         pot_cs         <= '1';        -- Inactive
                         pot_dout       <= not( Val( 7- bit_cnt ));	-- Use on Preamp requiers bits to be inverted
                         bit_Cnt        <= 0;
                         if( Ld_Pot = '1' )
                              then Pot_State <= St_Clock_Low;
                         end if;
                        
                    -- Output Low Clock Signal ------------------------------------------------------
                    when St_Clock_Low =>
                         pot_clk        <= '0';
                         pot_Cs         <= '0';        -- Active
                         pot_dout       <= not( Val( 7- bit_cnt ));	-- Use on Preamp requiers bits to be inverted
                         if( clock_cnt_tc = '1' )
                              then Pot_State <= St_Clock_High;
                         end if;
                    -- Output Low Clock Signal ------------------------------------------------------
                         
                    -- Output High Clock Signal ------------------------------------------------------
                    when St_Clock_High  =>
                         pot_clk        <= '1';
                         pot_cs         <= '0';        -- active
                         pot_dout       <= not( Val( 7- bit_cnt ));	-- Use on Preamp requiers bits to be inverted
                         
                         if( clock_Cnt_tc = '1' ) then
                              if( bit_Cnt = 7 ) then
                                   Pot_State <= St_Pot_Done;                              
                              else
                                   bit_Cnt   <= bit_Cnt + 1;
                                   Pot_State <= St_Clock_Low;
                              end if;
                         end if;
                    -- Output High Clock Signal ------------------------------------------------------
                    
                    when St_Pot_Done =>
                         pot_clk        <= '0';
                         pot_cs         <= '1';        -- Inactive
                         pot_dout       <= not( Val( 7- bit_cnt ));	-- Use on Preamp requiers bits to be inverted
                         Pot_State      <= St_Idle;
               end case;
          end if;
     end process;
     -----------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
end behavioral;			-- AD5920
---------------------------------------------------------------------------------------------------

    
    
    