---------------------------------------------------------------------------------------------------
library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

---------------------------------------------------------------------------------------------------
entity tb is
end tb;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture test_bench of tb is
	
	signal imr 	: std_logic := '1';
	signal clk100 	: std_logic := '0';
	signal bit_En	: std_logic := '0';
	signal ramp_en	: std_logic := '0';
	signal peak1	: std_logic_Vector( 15 downto 0 );
	signal peak2	: std_logic_Vector( 15 downto 0 );
	signal cps	: std_logic_Vector( 15 downto 0 );
	signal pk2dly	: std_logic_Vector( 15 downto 0 );
	signal bit_val	: std_logic_Vector( 15 downto 0 );
	signal reset	: std_logic;
	signal peak	: std_logic_Vector( 31 downto 0 );
	signal timing 	: std_logic_Vector( 31 downto 0 );
	
	component Bit_Gen is
		port( 
			clk100         : in      std_logic;                     	-- Master Clock
			BIT_EN		: in		std_Logic;					-- BIT Enable
			peak	     	: in      std_logic_vector( 31 downto 0 ); 	-- Peak 1 Value
			timing		: in      std_logic_vector( 31 downto 0 ); 	-- Peak 1 Value
			BIT_VAL		: buffer	std_Logic_Vector( 15 downto 0 );		
			Reset		: buffer	std_logic );
	end component Bit_Gen;
	---------------------------------------------------------------------------------------------------
begin
	imr		<= '0' after 555 ns;
	clk100 	<= not( clk100 ) after 5 ns;
	peak		<= peak2 & peak1;
	timing	<= pk2dly & cps;

	
	u : bit_gen
		port map(
			clk100         => clk100,
			BIT_EN		=> bit_En,
			peak			=> peak,
			timing		=> timing,
			BIT_VAL		=> bit_val,
			reset		=> reset );

	tb_proc : process
	begin
		BIT_EN		<= '0';
		Ramp_En		<= '0';
		peak1     	<= conv_Std_logic_Vector( 500, 16 );
		peak2     	<= conv_Std_logic_Vector( 25, 16 );
		cps       	<= conv_Std_logic_Vector( 1000, 16 );
		pk2dly    	<= conv_Std_logic_Vector( 500, 16 );
		wait for 1 us;
		wait until rising_edge( clk100 );
		Bit_En	<= '1' after 2 ns;
		wait for 10 ms;
		wait until rising_edge( clk100 );
		Ramp_En	<= '1' after 2 ns;
		wait for 10 ms;
		
		assert false report "End of Simulation" severity failure;
	end process;

---------------------------------------------------------------------------------------------------
end test_bench;			-- Bit_Gen
---------------------------------------------------------------------------------------------------

