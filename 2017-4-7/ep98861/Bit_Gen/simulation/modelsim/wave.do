onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix hexadecimal /tb/imr
add wave -noupdate -format Logic -radix hexadecimal /tb/clk100
add wave -noupdate -format Literal -radix hexadecimal /tb/bit_en
add wave -noupdate -format Literal -radix hexadecimal /tb/ramp_en
add wave -noupdate -format Literal -radix unsigned /tb/peak1
add wave -noupdate -format Literal -radix unsigned /tb/peak2
add wave -noupdate -format Literal -radix unsigned /tb/cps
add wave -noupdate -format Literal -radix unsigned /tb/pk2dly
add wave -noupdate -format Analog-Step -height 74 -max 26232.000000000004 -min -32768.0 -radix decimal /tb/bit_val
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {5811815000 ps} 0} {{Cursor 2} {10231245000 ps} 0}
configure wave -namecolwidth 120
configure wave -valuecolwidth 53
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {6218033813 ps} {20726435063 ps}
