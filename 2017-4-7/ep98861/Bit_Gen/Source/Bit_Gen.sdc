## Generated SDC file "Bit_Gen.sdc"

## Copyright (C) 1991-2010 Altera Corporation
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, Altera MegaCore Function License 
## Agreement, or other applicable license agreement, including, 
## without limitation, that your use is for the sole purpose of 
## programming logic devices manufactured by Altera and sold by 
## Altera or its authorized distributors.  Please refer to the 
## applicable agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 10.0 Build 218 06/27/2010 SJ Full Version"

## DATE    "Wed Sep 22 08:36:21 2010"

##
## DEVICE  "EP3C40F484C7"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {Clk100} -period 10.000 -waveform { 0.000 5.000 } [get_ports {clk100}]


#**************************************************************
# Create Generated Clock
#**************************************************************



#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************

derive_clock_uncertainty

#**************************************************************
# Set Input Delay
#**************************************************************

set_input_delay -add_delay -max -clock [get_clocks {Clk100}]  4.33 [get_ports {BIT_EN}]
set_input_delay -add_delay -min -clock [get_clocks {Clk100}]  2.33 [get_ports {BIT_EN}]

set_input_delay -add_delay -max -clock [get_clocks {Clk100}]  4.33 [get_ports {peak[*]}]
set_input_delay -add_delay -min -clock [get_clocks {Clk100}]  2.33 [get_ports {peak[*]}]

set_input_delay -add_delay -max -clock [get_clocks {Clk100}]  4.33 [get_ports {timing[*]}]
set_input_delay -add_delay -min -clock [get_clocks {Clk100}]  2.33 [get_ports {timing[*]}]



#**************************************************************
# Set Output Delay
#**************************************************************

set_output_delay -add_delay -clock [get_clocks {Clk100}]  5.000 [get_ports {BIT_VAL[*]}]
set_output_delay -add_delay -clock [get_clocks {Clk100}]  5.000 [get_ports {Reset}]


#**************************************************************
# Set Clock Groups
#**************************************************************



#**************************************************************
# Set False Path
#**************************************************************



#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

