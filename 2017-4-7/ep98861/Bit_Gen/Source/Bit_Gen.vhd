---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	Bit_Gen
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 16, 1999
--   Board          : FAD FPGA
--   Schematic      : 4035.007.20700
--
--   FPGA Code P/N  : 4035.009.99880 - SOFTWARE, FA-D FPGA
--   FPGA ROM  P/N  : 9335.078.00727 - ALTERA EPC2LC20
--   FPGA      P/N  : 9335.078.00735 - FPF20K100AQC240-3
--
--   Description:	
--		Module to generate the BIT DAC outputs based on 4 parameters
--			CPS, PK2D, PK1 and PK2
--
--   History:       <date> - <Author>
--		01/29/08 - MCS - Ver 5x01
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

---------------------------------------------------------------------------------------------------
entity Bit_Gen is
	port( 
     	clk100         : in      std_logic;                     	-- Master Clock
		BIT_EN		: in		std_Logic;					-- BIT Enable
	     peak	     	: in      std_logic_vector( 31 downto 0 ); 	-- Peak 1 Value
	     timing		: in      std_logic_vector( 31 downto 0 ); 	-- Peak 2 Value
		BIT_VAL		: buffer	std_Logic_Vector( 15 downto 0 );		
		Reset		: buffer	std_logic );
end Bit_Gen;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture behavioral of Bit_Gen is
     -- Constant Declarations

	constant Max_thr		: integer := ( 65536 * 9 ) / 10;
	constant Min_Thr		: integer := ( 65536 * 1 ) / 10;
	constant Reset_Val		: integer := ( Max_Thr - Min_Thr ) / 200;	-- Set to 2 micro seconds
	
     -- Registered Signals
	signal cps       		: std_logic_vector( 15 downto 0 ) := (others=>'0');
	signal pk2dly    		: std_logic_vector( 15 downto 0 ) := (others=>'0');
     
	signal acc			: std_logic_Vector( 15 downto 0 )  := (others=>'0');
	signal Cps_Cntr 		: std_Logic_Vector( 15 downto 0 )  := (others=>'0');
     signal Bit_En_Reg        : std_logic;
     signal Peak1             : std_logic_vector( 15 downto 0 );
     signal Peak2             : std_logic_vector( 15 downto 0 );
begin
   
	----------------------------------------------------------------------------------------------
     Clock_Proc : process( clk100 )
     begin
          if rising_edge( clk100 ) then
			cps			<= timing( 15 downto 0 );
			pk2dly 		<= timing( 31 downto 16 );
               Bit_En_Reg     <= Bit_En;
               Peak1          <= Peak( 15 downto 0 );
               Peak2          <= Peak( 31 downto 16 );
		
			------------------------------------------------------------------------------------
			-- CPS Generator( for 1 peak ) 
			if( Bit_En_Reg = '0' ) 
				then Cps_Cntr <= (others=>'0');
			elsif( Cps_Cntr >= Cps )
				then Cps_Cntr <= (others=>'0');
			
			-- Hold-Off Counting if Reset being generated
			elsif( Reset = '0' )
				then Cps_Cntr <= Cps_Cntr + 1;
			end if;
			-- CPS Generator( for 1 peak ) 
			------------------------------------------------------------------------------------
	
			------------------------------------------------------------------------------------
			-- Bit Gen Accumulator		
			if( Bit_En_Reg = '0' ) 
				then Acc <= (others=>'0');
			
			elsif( Reset = '1' ) 
				then Acc <= Acc - conv_std_logic_vector( Reset_Val, 16 );
			
			elsif( Cps_Cntr = 0 ) 
				then Acc <= Acc + Peak1;
				
			elsif(( cps_cntr = pk2dly ) and ( pk2dly > 0 ))
				then Acc <= Acc + Peak2;
			end if;
			-- BIT Gen Accumulator
			------------------------------------------------------------------------------------

			------------------------------------------------------------------------------------
			-- If Acc Exceed "Max_Thr" assert reset, and keep asserted until it falls below Min Threshold
			if( conv_integer( Acc ) >= Max_Thr )
				then Reset <= '1';				
			elsif( conv_integer( Acc ) <= Min_thr )
				then Reset <= '0';
			end if;
			------------------------------------------------------------------------------------
						
			------------------------------------------------------------------------------------
			-- Final Ouptut: Convert from Unsigned to Signed
			BIT_VAL <= not( Acc(15) ) & Acc( 14 downto 0 );
			------------------------------------------------------------------------------------
		end if;
     end process;
     ----------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
end behavioral;			-- Bit_Gen
---------------------------------------------------------------------------------------------------

