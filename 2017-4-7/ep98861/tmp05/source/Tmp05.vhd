----------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	tmp05.VHD
---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--   Description:
--             Module to read the Temperature for the Analog Devices tmp05 
--             Pulse Width Modulated Temperature Sensor and convert the reading to 
--			dec C * 256
-- 			It is nominally an 8,6Hz square wave
--			The temperature is determined by the equation
--			C = 421 - [(751 * Thi )/Tlo]

--	History
--		Feb 29, 2008 - MCS -- Ver 08022900
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
library LPM;
     use LPM.LPM_COMPONENTS.ALL;

library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

---------------------------------------------------------------------------------------------------
entity Tmp05 is 
	port( 
	     CLK50          : in      std_logic;                         -- Master Clock
	     TMP_SENSE      : in      std_logic;	                       -- Pipelined   
		TMP_DATA		: buffer	std_logic_Vector( 31 downto 0 ) );
end Tmp05;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture behavioral of Tmp05 is
	constant Cnst_Mult 		: integer := 751 * 256;
	constant Cnst_Offset	: integer := 421 * 256;	
	constant Startup_Cnt_Max	: integer := 15;
	constant CLK_PER		: integer := 20;
	constant ms500_cnt_max	: integer := 500000000 / CLK_PER;

     -- Type Declarations ----------------------------------------------
	constant ST_Idle		: integer := 0;
	constant ST_Wait_Hi0	: integer := 1;
	constant St_Wait_Lo		: integer := 2;
	constant St_Wait_Hi1	: integer := 3;
	constant St_Mult		: integer := 4;
	constant St_Divide		: integer := 5;
	constant St_Sub		: integer := 6;
	constant St_Done		: integer := 7;
     -- Type Declarations ----------------------------------------------

     -- Signal Declarations ---------------------------------------------
	signal THi			: std_logic_vector( 23 downto 0 ) := (others=>'0');	-- Temperature Hi
	signal Tlo	       	: std_logic_vector( 23 downto 0 ):= (others=>'0');	-- Temperature Low
     signal TMP_VEC           : std_logic_vector(  1 downto 0 ):= (others=>'0');
	signal Startup_Cnt		: integer range 0 to Startup_Cnt_Max := 0;
	signal tmp_State		: integer range 0 to St_Done := ST_Idle;
	signal ms500_tc		: std_logic;
	signal ms500_cnt		: integer range 0 to ms500_cnt_max;


     -- Signal Declarations ---------------------------------------------
	signal div_acc			: std_logic_Vector( 63 downto 0 ) := (others=>'0');
	signal div_Cnt			: std_logic_Vector( 31 downto 0 ) := (others=>'0');
	signal div_Diff		: std_logic_Vector( 63 downto 0 ) := (others=>'0');
	
	signal Cnt_Clr			: std_logic;
	signal Cnt_Hi			: std_logic;
	signal Cnt_Lo			: std_logic;
	signal Cnt_Mult		: std_logic;

	
	signal product			: std_logic_Vector( 63 downto 0 ) := (others=>'0');
	
	signal sub_en			: std_logic;
	signal Startup_Data		: std_logic_Vector( 15 downto 0 ) := (others=>'0');
	signal Temp_Data		: std_logic_Vector( 31 downto 0 ) := (others=>'0');
	
     -- Signal Declarations ---------------------------------------------
	begin
		ms500_tc <= '1' when ( ms500_cnt = ms500_cnt_max ) else '0';

		-------------------------------------------------------------------------------------
		-- Multiply THi by Constant and leave the result in Product
		Cnt_Mult	<= '1' when ( Tmp_State  = St_Mult ) else '0';

		UMult : lpm_mult 
			Generic map(
				LPM_WIDTHA		=> 32,
				LPM_WIDTHB		=> 32,
				LPM_WIDTHS		=> 64,
				LPM_WIDTHP		=> 64,
				LPM_REPRESENTATION	=> "UNSIGNED",
				LPM_PIPELINE		=> 1 )
			port map(
				clock			=> CLK50,
				clken			=> Cnt_Mult,
				dataa			=> ext( THi,32 ),
				datab			=> conv_std_logic_Vector( Cnst_Mult, 32 ),
				sum				=> (others=>'0'),
				result			=> Product );
		-- Multiply THi by Constant and leave the result in Product
		-------------------------------------------------------------------------------------

		-------------------------------------------------------------------------------------
		-- Now subtract quotient from the constant
		sub_en	<= '1' when ( tmp_State = St_Sub ) else '0';

		Tmp05_Data_Add_sub : lpm_add_sub
			generic map(
				LPM_WIDTH			=> 32, 
				LPM_DIRECTION		=> "SUB",
				LPM_REPRESENTATION	=> "UNSIGNED",
				LPM_PIPELINE		=> 1 )
			port map(
				clock			=> CLK50,
				clken			=> Sub_En,
				dataa			=> conv_std_logic_vector( Cnst_Offset, 32 ),
				datab			=> Div_Cnt,
				result			=> Temp_Data );
				
		-- Now subtract quotient from the constant
		-------------------------------------------------------------------------------------

     ----------------------------------------------------------------------------------------------
     clock_proc : process( CLK50 )
     begin
          if rising_edge( CLK50 ) then
			if( ms500_tc = '1' )
				then ms500_cnt <= 0;
				else ms500_cnt <= ms500_cnt + 1;
			end if;
				
			if(( ms500_tc = '1' ) and( Startup_Cnt < Startup_Cnt_Max ))
				then Startup_Cnt <= Startup_Cnt + 1;
			end if;			
			
               TMP_VEC        <= TMP_VEC(0) & TMP_SENSE;
               

			case TMP_STATE is
				when ST_IDLE => 
					if( ms500_tc = '1' ) 
						then TMP_STATE <= ST_WAIT_HI0;
					end if;
					
				when ST_WAIT_HI0 => 
					TLo <= (others=>'0');
					THi <= (others=>'0');
					if( Tmp_Vec = "01" ) 
						then TMP_STATE <= ST_WAIT_LO;
					end if;
					
				when ST_WAIT_LO => 
					if( tmp_sense = '1' )
						then THi <= THi + 1;
					end if;
					
					if( Tmp_Vec = "10" )
						then TMP_STATE <= St_Wait_Hi1;					
					end if;
					
				when ST_Wait_Hi1 	=> 
					if( tmp_sense = '0' )
						then TLo <= TLo + 1;
					end if;
					if( tmp_vec = "01" )
						then Tmp_State <= St_Mult;
					end if;
					
				when St_Mult		=> 
					Div_Acc 	<= (others=>'0');
					DIv_Cnt 	<= (others=>'0');				
					Tmp_State <= St_Divide;
					
				when St_Divide		=> 
					Div_Acc 	<= Div_Acc + ext( tlo, 64 );			
					Div_Cnt 	<= DIv_Cnt + 1;
					if( Div_Acc > Product )
						then tmp_state <= ST_Sub;
					end if;
					
				when ST_Sub => 
					tmp_State <= St_Done;	
				
				when St_Done =>
					if( Startup_Cnt < Startup_Cnt_Max )
						then Startup_Data <= temp_data( 15 downto 0 );
					end if;               
					
					TMP_DATA( 31 downto 16 ) <= Startup_Data;
					TMP_DATA( 15 downto  0 ) <= temp_data( 15 downto 0 );			-- Current Temperature
			
					tmp_State <= ST_WAIT_HI0;	
				when others => 
					Tmp_State <= ST_WAIT_HI0;
			end case;		

									
			
			
               -- At falling-edge of Tmp_sense latch Thi and Tlo ----------------------------------               
         end if;
     end process;
     ----------------------------------------------------------------------------------------------
          
---------------------------------------------------------------------------------------------------
end behavioral;			-- tmp04.VHD
---------------------------------------------------------------------------------------------------

