--------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	Disc_Main
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : November 13, 2009
--   Board          : Artemis Feasability Study
--   Schematic      : 4035.007.25500
--
--   Top Level      : 4035.009.993??, S/W, Artemis, ...
--
--   Description:	
--		This module generates a triangular filter based on 1/2 the Amp Time
--
--   History:       <date> - <Author> - Ver
--		December 13, 2011 - MCS - DISC_MAIN.VHD
--			Updated for TimeConst7, had to add to increase length of AVGSIZE as well as increase the depth of the DPRAM.
--			Update all references to TimeConst and make sure that 7 is included (and follows suit with the rest).
--		June 2, 2011
--			Added logic to detect is FIR_DATA exceeds 16, bits, if so, then it is cut in half (High KV TEM debugging)
--		06/01/11 
--			Cut FIR_DATA in half (just to make it display properly in DSS Mode at 300KeV)
--		04/20/11 
--			Some slight changes to DISC_OUT logic
---		April 1, 2011
--			During PA_RESET or PA_Busy hold FIR_ACC reset
--			otherwise BLR_Disable - Inhibit BLR from updating
--			otherwise allow BLR to update
--		November 13, 2009 - MCS - 1.00 - 
--			Created
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;	
		
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

	
---------------------------------------------------------------------------------------------------
entity Disc_Main is 
	port( 
		CLK100			: in		std_logic;					-- MAster Clock
		PA_Busy			: in		std_logic;
		PA_Reset			: in		std_logic;
		Fir_Reset			: in		std_logic;
		Disc_En			: in		std_logic;
		Time_Clr			: in		std_logic;
		Time_Enable		: in		std_logic;
		tlevel			: in		std_logic_vector( 15 downto 0 );	-- Discriminator Level		
		TimeConst			: in		std_logic_vector(  2 downto 0 );
		data_abc			: in		std_logic_vector( 53 downto 0 );
		fir_data   		: buffer 	std_logic_vector( 19 downto 0 );		
		Disc_Out			: buffer	std_logic;					-- Discriminator Level Exceeded	
		Disc_Level		: buffer	std_logic;
		Disc_Cnts			: buffer	std_logic_vector(  7 downto 0 ) );
end Disc_Main;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture behavioral of Disc_Main is
	-- Component Declarations --------------------------------------------------------------------
	component Main_Slope_Ram
		port( 
			clock			: in		std_logic := '1';
			wren				: in		std_logic := '0';
			rdaddress			: in		std_logic_Vector(  5 downto 0 );
			wraddress			: in		std_logic_Vector(  5 downto 0 );
			data				: in		std_logic_Vector( 63 downto 0 );
			q				: out	std_logic_Vector( 63 downto 0 ) );
	end component;	
	-- Component Declarations --------------------------------------------------------------------
	
	-- Constant Declarations ---------------------------------------------------------------------
	constant Acc_Width			: integer := 64; 
	constant Sum_Bits			: integer := 32;
	
	constant BLR_THR			: integer := 50;

	constant Dout_LSB			: integer := 16 + 2; -- 3;
	-- Constant Declarations ---------------------------------------------------------------------
		-- Constant Declarations ---------------------------------------------------------------------
	constant Disc_Out_Max		: integer := 159;
	constant Slope_Vec_max		: integer := 10;
	constant Slope_Cnt_max		: integer := 15;
	constant NSlope_Threshold	: integer := 2;
	-- Constant Declarations ---------------------------------------------------------------------
	
	-- Signal Declarations -----------------------------------------------------------------------
	signal AvgSize 			: integer range 0 to 63;

	signal BLR_Disable			: Std_Logic := '0';
	signal BLR_Mux				: std_logic_vector( Acc_Width-1 downto 0 )	:= (others=>'0');
	signal BLR_Offset			: std_logic_Vector( Acc_Width-1 downto 0 ) 	:= (others=>'0');
	signal BLR_Sum				: std_logic_Vector( 17 downto 0 ) 			:= (others=>'0');
	
	signal data_abc_reg			: std_logic_vector( 53 downto 0 ) := (others=>'0');
	signal delay_raddr			: std_logic_Vector(  5 downto 0 ):= (others=>'0');
	signal delay_waddr			: std_logic_Vector(  5 downto 0 ):= (others=>'0');
	signal Disc_Out_Vec			: std_logic_Vector( Disc_Out_Max downto  0 ):= (others=>'0');
	signal Disc_Out_Cps			: std_logic := '0';
	
	signal Int_Fir_Data			: std_logic_Vector( 19 downto 0 );

	signal Fir_Data_Del			: std_logic_vector( Acc_Width-1 downto 0 ):= (others=>'0');
	signal Fir_Acc				: std_logic_vector( Acc_Width-1 downto 0 ):= (others=>'0');
	signal Fir_Acc_Add			: std_logic_Vector( Acc_Width-1 downto 0 ):= (others=>'0');
	signal fir_deriv			: std_logic_Vector( Acc_Width-1 downto 0 );
	signal fir_Deriv_mux		: std_logic_Vector( Acc_Width-1 downto 0 );

	signal level_cmp 			: std_logic := '0';
	signal Level_vec			: std_logic_vector( 3 downto 0 ) := (others=>'0');
	signal NSlope				: std_logic := '0';
	signal NSlope_Cmp			: std_logic := '0';
	signal NSlope_Vec			: std_logic_Vector( Slope_Vec_max downto 0 ) := (others=>'0');
	signal PSlope_Vec			: std_logic_Vector( Slope_Vec_max downto 0 ):= (others=>'0');
	signal PSlope_Cnt			: integer range 0 to Slope_Cnt_max := 0;
	signal NSlope_Cnt			: integer range 0 to Slope_Cnt_max := 0;
	signal NSlope_Cnt_Del		: integer range 0 to Slope_Cnt_max := 0;
	
	signal PSlope_Cmp			: std_logic := '0';

	signal sub_total1			: std_logic_Vector( Sum_Bits-1 downto 0 ):= (others=>'0');
	signal sub_total2			: std_logic_Vector( Sum_Bits-1 downto 0 ):= (others=>'0');
	signal sum				: std_logic_Vector( Sum_Bits-1 downto 0 ):= (others=>'0');
	
	signal TimeConstInt			: integer range 0 to 7;
	
	-- Signal Declarations -----------------------------------------------------------------------;
	
	
begin
	----------------------------------------------------------------------------------------------
	Din_Proc : process( clk100 )
	begin
		if rising_edge( clk100 ) then
			TimeConstInt <= conv_integer( TimeConst );

			------------------------------------------------------------------------------------
			case TimeConstInt is
				when 0 		=> AvgSize <= 1;	-- 120 ns
				when 1 		=> AvgSize <= 1; 	-- 240 ns
				when 2 		=> AvgSize <= 1;	-- 480 ns;
				when 3 		=> AvgSize <= 1;	-- 960 ns;
				when 4 		=> AvgSize <= 2;	-- 1.92us
				when 5 		=> AvgSize <= 6;	-- 3.84 us;
				when 6 		=> AvgSize <= 30;	-- 7.68 us	WAS 14
				when 7 		=> AvgSize <= 45;	-- 11.52us	
				when others 	=> AvgSize <= 45;	-- 7.68 us
			end case;
			------------------------------------------------------------------------------------
			delay_waddr	<= delay_waddr + 1;
			delay_raddr 	<= delay_waddr - AvgSize;
			
			data_abc_Reg <= data_abc( 53 downto 36 ) & data_abc( 35 downto 18 ) & data_abc( 17 downto 0 );	
		end if;
	end process;
	----------------------------------------------------------------------------------------------
	-- First and Second Stage Create:
	-- sum = add_Vec1 - sub_Vec1 + add_Vec2
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Threshold Compare for using BLR
	Blr_Sum_Inst : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 18,
			LPM_DIRECTION		=> "SUB",
			LPM_REPRESENTATION 	=> "SIGNED" )
		port map(
			Cin				=> '1',
			dataa			=> data_abc_reg( 17 downto  0 ),	-- First data set
			datab			=> data_abc_reg( 53 downto 36 ),	-- Last data set
			result			=> Blr_Sum );	
				
	BLR_Disable_Inst : lpm_compare
		generic map(
			LPM_WIDTH			=> 18,
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			dataa			=> Blr_Sum,
			datab			=> conv_std_Logic_Vector( BLR_THR, 18 ),
			agb				=> BLR_Disable );					
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Triangular Filter Generation 
	-- 1st Stage
	sub_total1_Inst : lpm_add_sub
		generic map(
			LPM_WIDTH			=> Sum_Bits,
			LPM_DIRECTION		=> "SUB",
			LPM_REPRESENTATION 	=> "SIGNED" )
		port map(
			Cin				=> '1',
			dataa			=> sxt( data_abc_reg( 17 downto 0 ), 	    Sum_Bits ),	-- 1X
			datab			=> sxt( data_abc_reg( 35 downto 18 ) & '0', Sum_Bits ),	-- 2X
			result			=> sub_total1 );		-- dataa-datab
	-- Final Stage (No Gap Time)
	sum_add_Inst : lpm_add_sub
		generic map(
			LPM_WIDTH			=> Sum_Bits,
			LPM_DIRECTION		=> "ADD",
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_PIPELINE		=> 1 )
		port map(
			clock			=> CLK100,
			Cin				=> '0',
			dataa			=> sub_total1,					
			datab			=> sxt( data_abc_reg( 53 downto 36 ), Sum_Bits ),		-- 1X 
			result			=> sum );	
	-- FIR - Second Stage 
	----------------------------------------------------------------------------------------------
	
	----------------------------------------------------------------------------------------------
	-- Main Filter Accumulator
	-- Baseline Restorer Parallel Adder ----------------------------------------------------------
	----------------------------------------------------------------------------------------------
	-- fir_acc_add = SumMult + Fir_Acc
	fir_acc_add_Inst : lpm_add_Sub
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_DIRECTION		=> "ADD",
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			cin				=> '0',
			dataa			=> Fir_Acc,			
			datab			=> sxt( sum & x"0000", acc_width ),	-- Mult By 2^16 & Sign Extend 
			result			=> fir_acc_add );	
	-- fir_acc_add = SumMult + Fir_Acc
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Third ( Accumulate ) Stage
	-- Subtract the BLR value ( which is some fraction of Fir_Acc ) from the previous stage's sum
	BLR_Offset_Inst : lpm_add_Sub
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_DIRECTION		=> "SUB",
			LPM_REPRESENTATION	=> "SIGNED")
		port map(
			cin				=> '1',
			dataa			=> fir_acc_add,
			datab			=> BLR_Mux, 
			result			=> BLR_Offset );
	-- Third ( Accumulate ) Stage
	----------------------------------------------------------------------------------------------
	
	fir_acc_proc : process( clk100 )
	begin
		if( rising_edge( CLK100 )) then		
			------------------------------------------------------------------------------------
			-- BLR Mux
			case TimeConstInt is
				when 0 		=> BLR_Mux <= sxt( Fir_Acc( Acc_Width-1 downto Dout_LSB   ) & x"0", Acc_Width );	-- 0.12us
				when 1 		=> BLR_Mux <= sxt( Fir_Acc( Acc_Width-1 downto Dout_LSB+1 ) & x"0", Acc_Width );	-- 0.24us
				when 2 		=> BLR_Mux <= sxt( Fir_Acc( Acc_Width-1 downto Dout_LSB+2 ) & x"0", Acc_Width );	-- 0.48s
				when 3 		=> BLR_Mux <= sxt( Fir_Acc( Acc_Width-1 downto Dout_LSB+3 ) & x"0", Acc_Width );	-- 0.96us
				when 4 		=> BLR_Mux <= sxt( Fir_Acc( Acc_Width-1 downto Dout_LSB+4 ) & x"0", Acc_Width );	-- 1.92us
				when 5 		=> BLR_Mux <= sxt( Fir_Acc( Acc_Width-1 downto Dout_LSB+5 ) & x"0", Acc_Width );	-- 3.84us
				when 6 		=> BLR_Mux <= sxt( Fir_Acc( Acc_Width-1 downto Dout_LSB+6 ) & x"0", Acc_Width );	-- 7.68us
				when 7 		=> BLR_Mux <= sxt( Fir_Acc( Acc_Width-1 downto Dout_LSB+7 ) & x"0", Acc_Width );	-- 11.52us
				when others	=> BLR_Mux <= sxt( Fir_Acc( Acc_Width-1 downto Dout_LSB+7 ) & x"0", Acc_Width );	-- 7.68us
			end case;			
			-- BLR Mux
			------------------------------------------------------------------------------------
		
			------------------------------------------------------------------------------------	
			-- Main Filter Accumulator
			-- When to Clear it			
			if(( PA_Reset = '1' ) or ( PA_Busy = '1' ))
				then Fir_Acc	<= Fir_Acc_Add;
--				then Fir_Acc 	<= (others=>'0');
			elsif( BLR_Disable = '1' )
				then Fir_Acc	<= Fir_Acc_Add;
					
			-- Otherwise normal accumulation
				else Fir_Acc	<= Blr_Offset;
			end if;
			------------------------------------------------------------------------------------

			------------------------------------------------------------------------------------
			-- Internal Filter Data
			case TimeConstInt is
				when 0 		=> Int_Fir_Data <= fir_acc( 19+Dout_LSB+0 downto Dout_LSB+0);
				when 1 		=> Int_Fir_Data <= fir_acc( 19+Dout_LSB+1 downto Dout_LSB+1);
				when 2 		=> Int_Fir_Data <= fir_acc( 19+Dout_LSB+2 downto Dout_LSB+2);
				when 3 		=> Int_Fir_Data <= fir_acc( 19+Dout_LSB+3 downto Dout_LSB+3);
				when 4 		=> Int_Fir_Data <= fir_acc( 19+Dout_LSB+4 downto Dout_LSB+4);
				when 5 		=> Int_Fir_Data <= fir_acc( 19+Dout_LSB+5 downto Dout_LSB+5);
				when 6 		=> Int_Fir_Data <= fir_acc( 19+Dout_LSB+6 downto Dout_LSB+6);
				when 7 		=> Int_Fir_Data <= fir_acc( 19+Dout_LSB+7 downto Dout_LSB+7);
				when others 	=> Int_Fir_Data <= fir_acc( 19+Dout_LSB+7 downto Dout_LSB+7);
			end case;
			-- Internal Filter Data
			------------------------------------------------------------------------------------

			------------------------------------------------------------------------------------
			-- Clamp off Outpu tduring a reset (just for DSS view)
			if(( PA_Reset = '1' ) or ( PA_Busy = '1' ))
				then Fir_Data 	<= (others=>'0');
				else Fir_Data	<= Int_Fir_Data;				
			end if;
			-- Filter Data
		end if;
	end process;
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Compare fir_Data to the threshold level
	Level_Cmp_Inst : lpm_compare
		generic map(
			lpm_width			=> 20,
			lpm_representation	=> "SIGNED",
			lpm_pipeline		=> 1 )
		port map(	
			clock			=> clk100,
			dataa			=> Int_Fir_Data,			-- Filter Input
			datab			=> sxt( TLevel, 20 ),	-- Discriminator Threshold Level
			agb				=> Level_Cmp );		-- Filter is above the threshold level
	-- Compare fir_Data to the threshold level
	----------------------------------------------------------------------------------------------
	
	----------------------------------------------------------------------------------------------
	-- Slope Determination													
	Main_Slope_Ram_Inst : Main_Slope_Ram
		port map(
			clock				=> clk100,
			wren					=> '1',
			rdaddress				=> delay_raddr,
			wraddress				=> delay_waddr,
			data					=> fir_acc,
--			data					=> fir_acc( 31+Dout_LSB+TimeConstInt downto Dout_LSB+TimeConstInt),
			q					=> Fir_Data_Del );

--	Slope_Cmp_Inst : lpm_compare
--		generic map(
--			LPM_WIDTH				=> 32,
--			LPM_REPRESENTATION		=> "SIGNED" )
--		port map(
--			dataa				=> fir_acc( 31+Dout_LSB+TimeConstInt downto Dout_LSB+TimeConstInt), 
--			datab				=> Fir_Data_Del,
--			alb					=> NSlope_Cmp,
--			agb					=> PSlope_Cmp );
----			aeb					=> ZSlope_Cmp );	

	First_Deriv_Inst : lpm_add_Sub
		generic map(
			LPM_WIDTH				=> Acc_Width,
			LPM_REPRESENTATION		=> "SIGNED",
			LPM_DIRECTION			=> "SUB",
			LPM_PIPELINE			=> 1 )
		port map(
			clock				=> clk100,
			cin					=> '1',
			dataa				=> fir_acc,
--			dataa				=> fir_acc( 31+Dout_LSB+TimeConstInt downto Dout_LSB+TimeConstInt),
			datab				=> fir_Data_Del,
			result				=> fir_Deriv );
			
	with TimeConstInt select
		fir_Deriv_mux <= sxt( Fir_Deriv( Acc_Width-1 downto 0+5 ), Acc_Width ) when 0,
					  sxt( Fir_Deriv( Acc_Width-1 downto 1+5 ), Acc_Width ) when 1,
					  sxt( Fir_Deriv( Acc_Width-1 downto 2+5 ), Acc_Width ) when 2,
					  sxt( Fir_Deriv( Acc_Width-1 downto 3+5 ), Acc_Width ) when 3,
					  sxt( Fir_Deriv( Acc_Width-1 downto 4+5 ), Acc_Width ) when 4,
					  sxt( Fir_Deriv( Acc_Width-1 downto 5+5 ), Acc_Width ) when 5,
					  sxt( Fir_Deriv( Acc_Width-1 downto 6+5 ), Acc_Width ) when 6,
					  sxt( Fir_Deriv( Acc_Width-1 downto 7+5 ), Acc_Width ) when 7,
					  sxt( Fir_Deriv( Acc_Width-1 downto 7+5 ), Acc_Width ) when others;					  
			
	PSlope_Cmp_Inst : lpm_compare
		generic map(
			LPM_WIDTH				=> Acc_Width,
			LPM_REPRESENTATION		=> "SIGNED" )
		port map(
			dataa				=> fir_Deriv_mux,
			datab				=> conv_std_logic_Vector( 5000, Acc_Width ), 
			agb					=> PSlope_Cmp );

	NSlope_Cmp_Inst : lpm_compare
		generic map(
			LPM_WIDTH				=> Acc_Width,
			LPM_REPRESENTATION		=> "SIGNED" )
		port map(
			dataa				=> fir_Deriv_mux, 
			datab				=> conv_std_logic_Vector( -5000, Acc_Width ), 
			alb					=> NSlope_Cmp );	
	-- Slope Determination 
	----------------------------------------------------------------------------------------------
	
	----------------------------------------------------------------------------------------------
	clock_proc : process( CLK100 )
		variable pcnt : integer range 0 to Slope_Cnt_max;
		variable ncnt : integer range 0 to Slope_Cnt_max;
	
	begin 
		if(( CLK100'event ) and ( CLK100 = '1' )) then			
			
			------------------------------------------------------------------------------------
			-- Baseline Threshold Level Determination 
			Level_Vec 	<= Level_Vec( 2 downto 0 ) & Level_Cmp;		-- Stretch Level Compare for shorter amp times
			
			if( Disc_En = '0' )
				then Disc_Level <= '0';
			elsif( Level_Cmp = '1' )
				then Disc_Level <= '1';
			elsif( conv_integer( Level_vec ) /= 0 )
				then Disc_Level <= '1';
				else Disc_Level <= '0';
			end if;
			-- Baseline Threshold Level Determination 
			------------------------------------------------------------------------------------
			
			------------------------------------------------------------------------------------
			-- Look for a Negative Slope
			if(( PA_Busy = '0' ) and ( Disc_En = '1' ) and ( Disc_Level = '1' )) then
				NSlope_Vec	<= NSlope_Vec( Slope_Vec_max-1 downto 0 ) & NSlope_Cmp;
				PSlope_Vec	<= PSlope_Vec( Slope_Vec_max-1 downto 0 ) & PSlope_Cmp;
			else
				NSlope_Vec	<= NSlope_Vec( Slope_Vec_max-1 downto 0 ) & '0';
				PSlope_Vec	<= PSlope_Vec( Slope_Vec_max-1 downto 0 ) & '0';
			end if;
			-- Look for a Negative Slope
			------------------------------------------------------------------------------------
			
			------------------------------------------------------------------------------------
			-- Use a variable to count the number of "1"s in the .._Vec(i)
			-- Then after the loop, assign it to the signal ".._Cnt"
			for i in 0 to Slope_Vec_max+1 loop
				if( i = 0 ) 
					then pcnt := 0;
--				elsif(( PSlope_Vec(i-1) = '1' ) and ( PSlope_Cnt < Slope_Cnt_max ))
				elsif(( PSlope_Vec(i-1) = '1' ) and ( pcnt < Slope_Cnt_max ))
					then pcnt := pcnt + 1;
				end if;
				
				if( i = 0 )
					then ncnt := 0;
--				elsif(( NSlope_Vec(i-1) = '1' ) and ( NSlope_Cnt < Slope_Cnt_max ))
				elsif(( NSlope_Vec(i-1) = '1' ) and ( ncnt < Slope_Cnt_max ))
					then ncnt := ncnt + 1;
				end if;				
			end loop;
			PSlope_Cnt 	<= pcnt;
			NSlope_Cnt 	<= ncnt;
			NSlope_Cnt_Del <= NSlope_Cnt;
			------------------------------------------------------------------------------------

			if(( PA_Busy = '0' ) and ( Disc_En = '1' ) and ( Disc_Level = '1' ) and ( PSlope_Cnt > NSlope_Cnt ) and ( NSlope_Cnt > NSlope_Cnt_Del ) and ( NSlope_Cnt > NSlope_Threshold )) then
				NSlope 	<= '1';
				if(( NSlope = '0' ) and ( PSlope_Cmp = '0' ))			-- 110420xx
					then Disc_Out <= '1';
					else Disc_Out <= '0';
				end if;
			else
				NSlope 	<= '0';
				Disc_Out	<= '0';
			end if;

--			if(( PA_Busy = '0' ) and ( Disc_En = '1' ) and ( Disc_Level = '1' ) and ( Pslope_Cnt > NSlope_Cnt ) and ( NSlope_Cnt > NSlope_Cnt_Del ) and ( NSlope_Cnt > NSlope_Threshold ) and ( NSlope = '0' ))
--				then Disc_Out <= '1';
--				else Disc_Out <= '0';
--			end if;
			------------------------------------------------------------------------------------

			------------------------------------------------------------------------------------
			-- Delay the "Disc_Out" signal long enough to be under a PA_BUSY, since the reset 
			-- tends to trigger a discriminator count
			Disc_Out_Vec <= Disc_Out_Vec( Disc_Out_Max-1 downto 0 ) & Disc_Out;
			
			if(( Disc_Out_Vec(Disc_Out_Max) = '1' ) and ( PA_Busy = '0' ))
				then Disc_Out_Cps <= '1';
				else Disc_Out_Cps <= '0';
			end if;
			-- Delay the "Disc_Out" signal long enough to be under a PA_BUSY, since the reset 
			-- tends to trigger a discriminator count
			------------------------------------------------------------------------------------
			
			------------------------------------------------------------------------------------
			-- Now Count the number of Discriminator Counts ( up to 8 bits ) for AutoDisc
			if( Time_Clr = '1' )
				then Disc_Cnts <= (others=>'0');
			elsif(( Time_Enable = '1' ) and ( conv_integer( not( Disc_Cnts )) /= 0 ) and ( Disc_Out_CPS = '1' ))
				then Disc_Cnts <= Disc_Cnts + 1;
			end if;							
			-- Now Count the number of Discriminator Counts ( up to 8 bits ) for AutoDisc
			------------------------------------------------------------------------------------			
		end if;
	end process;
     ----------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
end behavioral;               -- Disc_Main.vhd
---------------------------------------------------------------------------------------------------

