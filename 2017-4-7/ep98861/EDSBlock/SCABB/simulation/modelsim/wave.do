onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic /tb/imr
add wave -noupdate -format Logic /tb/clk100
add wave -noupdate -format Literal -radix hexadecimal /tb/scabb_mode
add wave -noupdate -format Literal -radix hexadecimal /tb/scabb_delay
add wave -noupdate -format Literal -radix hexadecimal /tb/scabb_width
add wave -noupdate -format Logic /tb/fdisc
add wave -noupdate -format Logic /tb/meas_done
add wave -noupdate -format Logic /tb/time_enable
add wave -noupdate -format Logic /tb/we_sca_lu
add wave -noupdate -format Literal -radix hexadecimal /tb/cpeak
add wave -noupdate -format Literal -radix hexadecimal /tb/host_wa
add wave -noupdate -format Literal -radix hexadecimal /tb/host_wd
add wave -noupdate -format Literal -radix hexadecimal /tb/sca_lu_data
add wave -noupdate -format Logic /tb/scabb_out
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 9} {372468000 ps} 0}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {1371451 ps} {1462163 ps}
