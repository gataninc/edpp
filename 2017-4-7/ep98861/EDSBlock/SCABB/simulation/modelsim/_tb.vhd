---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	scabb.VHD
---------------------------------------------------------------------------------------------------

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

entity tb is
end tb;

architecture test_bench of tb is

	-- Mode Definition ---------------------------------------------------------------------------
	constant MODE_NONE		: integer := 0;
	constant MODE_BB_POS	: integer := 1;
	constant MODE_BB_NEG	: integer := 2;
	constant MODE_SCA_POS	: integer := 3;
	constant MODE_SCA_NEG	: integer := 4;
	-- Mode Definition ---------------------------------------------------------------------------

	signal imr			: std_logic := '1';
	signal clk100			: std_logic := '0';
	signal SCABB_Mode		: std_logic_Vector( 2 downto 0 );
	signal SCABB_Delay		: std_logic_Vector( 15 downto 0 );	-- Beam Blanking Delay
	signal SCABB_Width		: std_Logic_Vector( 15 downto 0 );	-- Beam Blanking Width		
	signal fdisc			: std_logic;					-- Trigger for Beam Blank Output
	signal Meas_Done		: std_logic;					-- Trigger for SCA Output
	signal Time_Enable		: std_logic;
	signal WE_SCA_LU		: std_logic;
	signal CPeak			: std_logic_Vector( 11 downto 0 );
	signal HOST_WA			: std_Logic_Vector( 6 downto 0 );
	signal HOST_WD			: std_logic_Vector( 31 downto 0 );
	signal SCA_LU_DATA		: std_logic_Vector( 31 downto 0 );
	signal SCABB_Out		: std_logic;
	
	---------------------------------------------------------------------------------------------------
	component  scabb
		port(
			imr			: in		std_logic;
			clk100		: in		std_logic;
			SCABB_Mode	: in		std_logic_Vector( 2 downto 0 );
			SCABB_Delay	: in		std_logic_Vector( 15 downto 0 );	-- Beam Blanking Delay
			SCABB_Width	: in		std_Logic_Vector( 15 downto 0 );	-- Beam Blanking Width		
			fdisc		: in		std_logic;					-- Trigger for Beam Blank Output
			Meas_Done		: in		std_logic;					-- Trigger for SCA Output
			Time_Enable	: in		std_logic;
			WE_SCA_LU		: in		std_logic;
			CPeak		: in		std_logic_Vector( 11 downto 0 );
			HOST_WA		: in		std_Logic_Vector( 6 downto 0 );
			HOST_WD		: in		std_logic_Vector( 31 downto 0 );
			SCA_LU_DATA	: buffer	std_logic_Vector( 31 downto 0 );
			SCABB_Out		: buffer	std_logic );
	end component scabb;
	---------------------------------------------------------------------------------------------------
begin
	imr	<= '0' after 333 ns;
	
	clk100 <= not( clk100 ) after 5 ns;

	U : scabb
		port map(
			imr			=> imr,
			clk100		=> clk100,
			SCABB_Mode	=> SCABB_Mode,
			SCABB_Delay	=> SCABB_Delay,
			SCABB_Width	=> SCABB_Width,
			fdisc		=> FDisc,
			Meas_Done		=> Meas_Done,
			Time_Enable	=> Time_Enable,
			WE_SCA_LU		=> WE_SCA_LU,
			CPeak		=> CPeak,
			HOST_WA		=> Host_WA,
			HOST_WD		=> Host_WD,
			SCA_LU_DATA	=> SCA_LU_DATA,
			SCABB_Out		=> SCABB_Out );
	---------------------------------------------------------------------------------------------------

	---------------------------------------------------------------------------------------------------
	tb_proc : process
	begin
		SCABB_Mode 	<= conv_Std_logic_Vector( MODE_NONE, 3 );
		SCABB_Delay 	<= x"0001";
		SCABB_Width	<= x"0001";
		fdisc		<= '0';
		Meas_Done		<= '0';
		Time_Enable	<= '0';
		WE_SCA_LU		<= '0';
		CPEak		<= x"000";
		Host_WA		<= conv_Std_logic_Vector( 0, 7 );
		Host_WD		<= x"00000000";
		
	
		wait until falling_Edge( imr );
		
		-----------------------------------------------------------------------------------------
		wait for 1 us;
		assert false
			report "----- Test the Memory ------------------------------------------------------"
			severity note;
		
		for i in 0 to 127 loop
			wait until rising_Edge( clk100 );
			wait for 2 ns;
			HOST_WA <= conv_std_logic_Vector( i, 7 );
			HOST_WD <= conv_std_logic_Vector( i, 32 );
			WE_SCA_LU <= '1';
			wait until rising_Edge( clk100 );
			wait for 2 ns;
			WE_SCA_LU <= '0';
			wait until rising_Edge( clk100 );
			wait until rising_Edge( clk100 );
			wait until rising_Edge( clk100 );
			wait until rising_Edge( clk100 );
			assert( conv_integer( sca_lu_data ) = i )
				report "Error in Memory Data"
				severity error;
		end loop;
		
		for i in 0 to 31 loop
			wait until rising_Edge( clk100 );
			wait for 2 ns;
			HOST_WA <= conv_std_logic_Vector( i, 7 );
			HOST_WD <= x"FFFFFFFF";
			WE_SCA_LU <= '1';
			wait until rising_Edge( clk100 );
			wait for 2 ns;
			WE_SCA_LU <= '0';
		end loop;
		
		for i in 32 to 127 loop
			wait until rising_Edge( clk100 );
			wait for 2 ns;
			HOST_WA <= conv_std_logic_Vector( i, 7 );
			HOST_WD <= x"00000000";
			WE_SCA_LU <= '1';
			wait until rising_Edge( clk100 );
			wait for 2 ns;
			WE_SCA_LU <= '0';
		end loop;
		-----------------------------------------------------------------------------------------

		-----------------------------------------------------------------------------------------
		Time_Enable	<= '1';
		for j in 0 to 3 loop
			case j is
				when 0 => 
					assert false
					report "----- Test Positive SCA Mode -----------------------------------------------"
					severity note;
		
					SCABB_Mode 	<= conv_Std_logic_Vector( MODE_SCA_POS, 3 );
					SCABB_Width	<= x"0010";
					SCABB_Delay	<= x"0008";
				when 1 =>
					assert false
					report "----- Test Negative SCA Mode -----------------------------------------------"
					severity note;
		
					SCABB_Mode 	<= conv_Std_logic_Vector( MODE_SCA_NEG, 3 );
					SCABB_Width	<= x"0010";
					SCABB_Delay	<= x"0008";
				when 2 => 
					assert false
					report "----- Test Positive Beam Blank Mode -----------------------------------------------"
					severity note;
		
					SCABB_Mode 	<= conv_Std_logic_Vector( MODE_BB_POS, 3 );
					SCABB_Width	<= x"0010";
					SCABB_Delay	<= x"0008";
				when 3 =>
					assert false
					report "----- Test Negative Bean Blank Mode -----------------------------------------------"
					severity note;
		
					SCABB_Mode 	<= conv_Std_logic_Vector( MODE_BB_NEG, 3 );
					SCABB_Width	<= x"0010";
					SCABB_Delay	<= x"0008";
				when others =>
			end case;
			
			for i in 0 to 7 loop
				wait until rising_edge( clk100 );
				wait for 2 ns;
				if( j < 2 ) then
					case i is
						when 0 => CPeak <= x"001"; 
						when 1 => CPeak <= x"801";
						when 2 => CPeak <= x"002";
						when 3 => CPeak <= x"802";
						when 4 => CPeak <= x"004";
						when 5 => CPeak <= x"804";
						when 6 => CPeak <= x"008";
						when 7 => CPeak <= x"808";
						when others	=> CPeak <= x"000";
					end case;
					Meas_Done <= '1';
					wait until rising_edge( clk100 );
					wait for 2 ns;
					Meas_Done <= '0';
				else
					fdisc <= '1';
					wait until rising_edge( clk100 );
					wait for 2 ns;
					fdisc <= '0';
				end if;
			
				for k in 0 to 30 loop
					wait until rising_edge( clk100 );
				end loop;
			end loop;
		end loop;
		Time_Enable	<= '0';
		-----------------------------------------------------------------------------------------
		
		assert false
			report "End of Simulations"
			severity failure;		
	end process;

end test_bench;