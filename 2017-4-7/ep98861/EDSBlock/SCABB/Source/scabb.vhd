---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	scabb.VHD
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--   Description:	
--		This module contains the necessary interface circuit for the 
--		Digital and Analog SCA outputs.
--		It has some static controls to define polarity, enables, etc
--		some additional BIT type of controls to load the parameters
--		directly.
-- 		The real input is the MEAS_DONE signal with the CPEAK vector
--		It then looks-up the appropriate value from the memory to output
--		the SCA Vector and the Analog Output, if in the SCA Mode, or 
--		Outputs the ROI Vector and ROI Load signal to the Cross Port.
-- 		The Interface to the DAC also is containd in this module and 
--		a state machine is used to update the DAC with the ON voltage 
--		at the time when the Digital SCA is fired, and outputs the OFF
--		voltage immediately following. When the Analog SCA Pulse Width 
--		as expired, then the DAC is updated and the ON voltage is loaded
--		to the DAC.
--		Additionally at the leading edge of "TIME_ENABLE" the Off voltage
--		is loaded to the DAC, it is strobed and the ON voltage is also loaded
--		in preparation of analog SCA events.
--
--		WIDTH PARAMETER ( Used for all modes )
--			Lower 16-bits are the width
--			upper 2 bits define the mode
--	History
--		01/29/08 - MCS - Ver 5x00
--			Complied under Quartus 7.2 SP1
--		06/15/05 - MCS
--			Updated for QuartusII Ver 5.0 SP 0.21
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
--		*********************************************************************
--		September 22, 2000 - MCS:
--			Final Release - EDI-II
--		*********************************************************************
---------------------------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
	
library altera_mf;
	use altera_mf.altera_mf_components.all;

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

---------------------------------------------------------------------------------------------------
entity scabb is 
     port(
          CLK100         : in      std_Logic;
		Param		: in		std_logic_Vector( 31 downto 0 );	-- Beam Blanking Delay
		fdisc		: in		std_logic;					-- Trigger for Beam Blank Output
		Meas_Done		: in		std_logic;					-- Trigger for SCA Output
		Time_Enable	: in		std_logic;
		SCABB_WEN		: in		std_logic;
		SCABB_WA		: in		std_logic_Vector(  6 downto 0 );
		SCABB_WD		: in		std_logic_Vector( 31 downto 0 );
		SCABB_RA		: in		std_logic_Vector(  6 downto 0 );
		SCABB_RD		: buffer	std_logic_Vector( 31 downto 0 );
		CPeak		: in		std_logic_Vector( 11 downto 0 );
		SCABB_Out		: buffer	std_logic );
end scabb;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture BEHAVIORAL of scabb is

	-- Mode Definition ---------------------------------------------------------------------------
	constant MODE_LSB		: integer := 16;
	constant MODE_MSB		: integer := 18;
	
	constant MODE_NONE		: integer := 0;
	constant MODE_BB_POS	: integer := 1;
	constant MODE_BB_NEG	: integer := 2;
	constant MODE_SCA_POS	: integer := 3;
	constant MODE_SCA_NEG	: integer := 4;
	-- Mode Definition ---------------------------------------------------------------------------
	
	

	-- Signal Declarations -----------------------------------------------------------------------
	signal BB				: std_logic := '0';
	signal BB_En			: std_logic := '0';
	
	signal SCABB_Cnt		: std_logic_Vector( 15 downto 0 );
	
	
	signal SCA			: std_logic := '0';
	signal Sca_En			: std_logic := '0';
	

	signal Meas_Done_Vec	: std_logic_Vector( 2 downto 0 );

	signal SCA_BB			: std_logic;
	signal SCABB_Lu		: std_Logic_vector( 31 downto 0 );
     signal SCABB_RA_C100     : std_logic_Vector(  6 downto 0 );    
     signal SCABB_RD_C100     : std_logic_Vector( 31 downto 0 );

	signal CPeak_LSB_REg0	: std_logic_vector( 4 downto 0 );
	signal CPeak_LSB_REg1	: std_logic_vector( 4 downto 0 );
	
     ----------------------------------------------------------------------------------------------
	component SCABB_RAM is 
		port(
               wrclock		: in      std_logic;
			wren			: in		std_logic  := '0';
			wraddress		: in		std_logic_vector (  6 downto 0 );
			data			: in		std_logic_vector ( 31 downto 0 );
               rdclock		: in      std_logic;
			rdaddress_a	: in		std_logic_vector (  6 downto 0 );
			rdaddress_b	: in		std_logic_vector (  6 downto 0 );
			qa			: out	std_logic_vector ( 31 downto 0 );
			qb			: out	std_logic_vector ( 31 downto 0 ) );
	end component;
     ----------------------------------------------------------------------------------------------

begin

     ----------------------------------------------------------------------------------------------
	-- Ver 3571 - CHange lookup memory from 2kx16 to 4k x 9
     ----------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------
	SCABB_RAM_INST : SCABB_RAM
		port map(
               wrclock		          => CLK100,
			wren					=> SCABB_WEN,
			wraddress				=> SCABB_WA( 6 downto 0 ),
			data					=> SCABB_WD,
			
               rdclock		          => CLK100,
			rdaddress_a			=> SCABB_RA, 
			qa					=> SCABB_RD, 
			
			rdaddress_b			=> CPeak( 11 downto 5 ),
			qb					=> SCABB_Lu );
     ----------------------------------------------------------------------------------------------
	
     ----------------------------------------------------------------------------------------------
     clk100_proc : process(CLK100)
     begin
		if( rising_edge( CLK100 )) then               
			-- Final Output --------------------------------------------------------------------
			case conv_integer( Param(MODE_MSB downto MODE_LSB) ) is
				when MODE_BB_POS 	=> SCABB_Out <= BB;			BB_En <= '1';	Sca_En <= '0';
				when MODE_BB_NEG 	=> SCABB_Out <= not( BB );	BB_En <= '1';  Sca_En <= '0';
				when MODE_SCA_POS 	=> SCABB_Out <= SCA;		BB_En <= '0';  Sca_En <= '1';
				when MODE_SCA_NEG 	=> SCABB_Out <= not( SCA );	BB_En <= '0';	Sca_En <= '1';
				when others		=> SCABB_Out <= '0';
			end case;
			-- Final Output --------------------------------------------------------------------	

             	-- pipeline the LSBs to keep them in-line with the memory --------------------------
			CPeak_LSB_REg0 <= CpeaK( 4 downto 0 );
			CPeak_LSB_REg1 <= CpeaK_LSB_Reg0;
			-- pipeline the LSBs to keep them in-line with the memory --------------------------					
			          
			-- Beam Blanking -------------------------------------------------------------------
			if( BB_En = '0' )
				then BB <= '0';
			elsif(( Time_Enable = '1' ) and ( FDisc = '1' ))
				then BB <= '1';
			elsif( SCABB_Cnt = Param(15 downto 0 ))
				then BB <= '0';
			end if;
  			-- Beam Blanking -------------------------------------------------------------------
		
			-- Single Channel Analyzer ---------------------------------------------------------
			Meas_Done_Vec 	<= Meas_Done_Vec( 1 downto 0 ) & Meas_Done;

			if( Sca_En = '0' )
				then SCA <= '0';
			elsif(( TIme_Enable = '1' ) and ( Meas_Done_Vec(2) = '1' ) and ( SCABB_LU( conv_integer( CPeak_LSB_Reg1 )) = '1' ))
				then SCA <= '1';
			elsif( SCABB_Cnt = Param( 15 downto 0 ))
				then SCA <= '0';
			end if;		
			-- Single Channel Analyzer ---------------------------------------------------------

			-- Counter for both ----------------------------------------------------------------
			if(( BB = '0' ) and ( SCA = '0' ))
				then SCABB_Cnt <= (others=>'0');
				else SCABB_Cnt <= SCABB_Cnt + 1;
			end if;			
			-- Counter for both ----------------------------------------------------------------			
		end if;                  -- MR/RED Clock
	end process;                  -- clock_Proc
     ----------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
end BEHAVIORAL;          -- scabb.vhd
---------------------------------------------------------------------------------------------------

