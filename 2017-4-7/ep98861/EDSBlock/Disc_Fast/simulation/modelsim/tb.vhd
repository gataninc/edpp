
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;
entity tb is
end tb;

architecture test_bench of tb is

	signal imr			: std_logic := '1';					-- Master Reset
	signal clk100			: std_logic := '0';					-- MAster Clock
	signal Mode			: std_Logic_Vector( 2 downto 0 );
	signal PA_Busy			: std_Logic;
	signal Fir_Reset		: std_logic;
	signal Disc_En			: std_logic;
	signal Time_Clr		: std_logic;
	signal Time_Enable		: std_logic;
	signal ad_data			: std_logic_vector( 15 downto 0 );
	signal tlevel			: std_logic_vector( 15 downto 0 );	-- Discriminator Level		
	signal Level_Out		: std_logic;					-- Discriminator Level Exceeded	
	signal Level_Cmp		: std_logic;		
	signal fir_out			: std_logic_Vector( 15 downto 0 );	-- FIR Output ( for Debug )
	signal Peak_time		: std_logic_Vector( 11 downto 0 );
	signal Disc_Cnts		: std_logiC_Vector(  7 downto 0 );
	signal PSlope			: std_logic;

	-------------------------------------------------------------------------------
	component disc_fast 
		port( 
			imr				: in		std_logic;					-- Master Reset
			clk100			: in		std_logic;					-- MAster Clock
			Mode				: in		std_Logic_Vector( 2 downto 0 );
			PA_Busy			: in		std_Logic;
			Fir_Reset			: in		std_logic;
			Disc_En			: in		std_logic;
			Time_Clr			: in		std_logic;
			Time_Enable		: in		std_logic;
			ad_data			: in		std_logic_vector( 15 downto 0 );
			tlevel			: in		std_logic_vector( 15 downto 0 );	-- Discriminator Level		
			Level_Out			: buffer	std_logic;					-- Discriminator Level Exceeded	
			Level_Cmp			: buffer	std_logic;		
			fir_out			: buffer	std_logic_Vector( 15 downto 0 );	-- FIR Output ( for Debug )
			Peak_time			: buffer	std_logic_Vector( 11 downto 0 );
			Disc_Cnts			: buffer	std_logiC_Vector(  7 downto 0 ) );
	end component disc_fast;
	-------------------------------------------------------------------------------
begin
	imr	<= '0' after 555 ns;
	clk100 <= not( clk100 ) after 5 ns;
	U : disc_fast
	
		port map(
			imr				=> imr,
			clk100			=> clk100,
			Mode				=> Mode,
			PA_Busy			=> PA_Busy,
			Fir_Reset			=> Fir_Reset,
			Disc_En			=> Disc_en,
			Time_Clr			=> Time_Clr,
			Time_Enable		=> Time_Enable,
			ad_data			=> ad_data,
			tlevel			=> Tlevel,
			Level_Out			=> Level_out,
			Level_Cmp			=> Level_Cmp,
			fir_out			=> Fir_Out,
			Peak_time			=> Peak_time,
			Disc_Cnts			=> Disc_Cnts );

	ad_proc : process
	begin
		Ad_Data <= x"0000";
		forever_loop : loop 
			wait for 10 us;
			wait until rising_edge( clk100 );
			wait for 3 ns;
			ad_data <= ad_data + 100;
		end loop;
	end process;
		

	tb_proc : process
	begin
		Mode				<= "000";
		PA_Busy			<= '0';
		Fir_Reset			<= '0';
		Disc_En			<= '1';
		Time_Clr			<= '0';
		Time_Enable		<= '0';
		tlevel			<= conv_std_logic_Vector( 100, 16 );
		
		wait for 10 us;
		for i in 0 to 4 loop
			wait until rising_Edge( clk100 );
			wait for 2 ns;
			Mode <= conv_Std_logic_Vector( i, 3 );
			wait for 40 us;
		end loop;
		
			
		assert false
			report "End of Simulation"
			severity failure;
	end process;
---------------------------------------------------------------------------------------------------
end test_bench;               -- Disc_Fast.vhd
---------------------------------------------------------------------------------------------------

