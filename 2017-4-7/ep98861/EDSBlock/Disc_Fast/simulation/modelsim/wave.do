onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix hexadecimal /tb/imr
add wave -noupdate -format Logic -radix hexadecimal /tb/clk100
add wave -noupdate -format Literal -radix hexadecimal /tb/mode
add wave -noupdate -format Logic -radix hexadecimal /tb/pa_busy
add wave -noupdate -format Logic -radix hexadecimal /tb/fir_reset
add wave -noupdate -format Logic -radix hexadecimal /tb/disc_en
add wave -noupdate -format Logic -radix hexadecimal /tb/time_clr
add wave -noupdate -format Logic -radix hexadecimal /tb/time_enable
add wave -noupdate -format Literal -radix hexadecimal /tb/ad_data
add wave -noupdate -format Logic -radix hexadecimal /tb/pslope
add wave -noupdate -format Literal -radix hexadecimal /tb/tlevel
add wave -noupdate -format Logic -radix hexadecimal /tb/level_out
add wave -noupdate -format Logic -radix hexadecimal /tb/level_cmp
add wave -noupdate -format Analog-Step -height 74 -max 12.999999999999998 -radix hexadecimal /tb/fir_out
add wave -noupdate -format Literal -radix hexadecimal /tb/peak_time
add wave -noupdate -format Literal -radix hexadecimal /tb/disc_cnts
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {179785364 ps} 0}
configure wave -namecolwidth 120
configure wave -valuecolwidth 53
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {80282265 ps}
