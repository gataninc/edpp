---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	Disc_Fast.vhd
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : November 13, 2009
--   Board          : Artemis Feasability Study
--   Schematic      : 4035.007.25500
--
--   Top Level      : 4035.009.993??, S/W, Artemis, ...
--
--   Description:	
--		From the "Stream" of the A/D converter this module genrates a triangular filter
--		whose amp times are predefined by the discriminator time constants
--		It also genertes a bus which defines the peaking time of each of the discriminators
--		which is used by the Pile-Up rejection logic;
--
--		Therefore to change the discriminator peaking time, this module gets modified 
--		and theoritically there should be no other changes
--
--   History:       <date> - <Author> - Ver
--		June 2, 2011
--			Added logic to detect is FIR_DATA exceeds 16, bits, if so, then it is cut in half (High KV TEM debugging)
--		April 1, 2011
--			During PA_RESET or PA_Busy hold FIR_ACC reset
--			otherwise BLR_Disable - Inhibit BLR from updating
--			otherwise allow BLR to update
--		November 13, 2009 - MCS - 1.00 - 
--			Created
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
		
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

---------------------------------------------------------------------------------------------------
entity disc_fast is 
	port( 
		clk100			: in		std_logic;					-- MAster Clock
		PA_Reset			: in		std_logic;
		PA_Busy			: in		std_Logic;
		Fir_Reset			: in		std_logic;
		Disc_En			: in		std_logic;
		Time_Clr			: in		std_logic;
		Time_Enable		: in		std_logic;		
		Mode				: in		std_Logic_Vector( 2 downto 0 );		
		tlevel			: in		std_logic_vector( 15 downto 0 );	-- Discriminator Level
		ad_data			: in		std_logic_vector( 17 downto 0 );
		fir_data			: buffer	std_logic_Vector( 19 downto 0 );	-- Filter Output
		Peak_time			: buffer	std_logic_Vector( 11 downto 0 );
		Disc_Out			: buffer	std_logic;					-- Discriminator Level Exceeded	
		Disc_Level		: buffer	std_logic;
		Disc_Cnts			: buffer	std_logic_vector(  7 downto 0 ) );		
end disc_fast;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture behavioral of disc_fast is
	-- Component Declarations --------------------------------------------------------------------
	component disc_fast_ram
		port( 
			clock		: in 	std_Logic  := '1';
			wren			: in 	std_Logic  := '0';
			wraddress		: in		std_logic_Vector(  5 downto 0 );
			rdaddress		: in 	std_logic_Vector(  5 downto 0 );
			data			: in		std_logic_Vector( 35 downto 0 );
			q			: out 	std_logic_Vector( 35 downto 0 ) );
	end component;	
	-- Component Declarations --------------------------------------------------------------------

	-- Constant Declarations ---------------------------------------------------------------------
	constant Acc_Width		: integer := 63; 	
	constant Sum_Bits		: integer := 32;

	constant Dout_LSB			: integer := 15+3;	-- was 15
--	constant Dout_LSB			: integer := 15+6;	-- was 15
	
	constant BLR_THR		: integer := 50; 
	constant BLR_LSB		: integer := 8;
	constant Fir_Out_LSB 	: integer := 6;	-- was 8;

	-- Mode == 0	-  40 ns
	-- Mode == 1	-  80 ns (not used)
	-- Mode == 2	- 160 ns
	-- Mode == 3	- 320 ns
	-- Mode == 4	- 640 ns

	
	constant Disc_Out_Max		: integer := 159;
	constant Slope_Vec_max		: integer := 10;
	constant Slope_Cnt_max		: integer := 15;
	constant NSlope_Threshold	: integer := 0;  --changed value from 2 to 0 on 10/15.  This improved B-N ratio
	
	
	constant CLK_PER			: integer := 10;		-- 100Mhz
	constant Adr_Width			: integer :=  6;		-- Enough to Handle 640ns 
	
	constant PTime_SHigh0		: integer := (   40 / CLK_PER );		-- Delay for Memory Address COunter
	constant PTime_SHigh1		: integer := (   80 / CLK_PER );		-- Delay for Memory Address COunter
	constant PTime_SHigh2		: integer := (  160 / CLK_PER );		-- Delay for Memory Address COunter
	constant PTime_SHigh3		: integer := (  320 / CLK_PER );		-- Delay for Memory Address COunter
	constant PTime_SHigh4		: integer := (  640 / CLK_PER );		-- Delay for Memory Address COunter
	-- Constant Declarations ---------------------------------------------------------------------

	-- Signal Declarations -----------------------------------------------------------------------
	signal adr_wr				: std_Logic_Vector( Adr_Width-1 downto 0 ):= (others=>'0');
	signal adr_rd				: std_logic_Vector( Adr_Width-1 downto 0 ):= (others=>'0');

	signal BLR_Disable			: Std_Logic := '0';
	signal BLR_Offset			: std_logic_Vector( Acc_Width-1 downto 0 ):= (others=>'0');
	signal BLR_Mux				: std_logic_Vector( Acc_Width-1 downto 0 ):= (others=>'0');

	signal delay_raddr			: std_logic_Vector(  4 downto 0 ):= (others=>'0');
	signal delay_waddr			: std_logic_Vector(  4 downto 0 ):= (others=>'0');
	signal Delay_Mux			: std_logic_Vector( Adr_Width-1 downto 0 ):= (others=>'0');
	signal dly_data			: std_logic_vector( 53 downto 0 ):= (others=>'0');
	signal Disc_Out_Vec			: std_logic_Vector( Disc_Out_Max	downto 0 ):= (others=>'0');
	signal Disc_Out_Cps			: std_logic := '0';

	signal Int_fir_data			: std_logic_Vector( 19 downto 0 );

	signal fir_Acc				: std_logic_vector( Acc_Width-1 downto 0 ):= (others=>'0');
	signal Fir_Acc_Add			: std_logic_Vector( Acc_Width-1 downto 0 ):= (others=>'0');
	signal fir_acc_mux			: std_logic_vector( Acc_Width-1 downto 0 ):= (others=>'0');
	signal Fir_Data_Del			: std_logic_vector( 19 downto 0 ):= (others=>'0');
	signal Fir_Data_Del1		: std_logic_vector( 19 downto 0 ):= (others=>'0');

	signal level_cmp 			: std_logic := '0';
	signal Level_vec			: std_logic_vector( 1 downto 0 ) := (others=>'0');
	
	signal ModeInt				: integer range 0 to 7;
	
	signal NSlope				: std_logic := '0';
	signal NSlope_Cmp			: std_logic := '0';
	signal NSlope_Vec			: std_logic_Vector( Slope_Vec_max downto 0 ) := (others=>'0');	
	
	signal sub_total1			: std_logic_Vector( Sum_Bits-1 downto 0 ):= (others=>'0');
	signal sum				: std_logic_Vector( Sum_Bits-1 downto 0 ):= (others=>'0');
	signal sum_blr				: std_logic_Vector( 17 downto 0 ):= (others=>'0');
	-- Signal Declarations -----------------------------------------------------------------------
begin
	----------------------------------------------------------------------------------------------
	Din_Proc : process( clk100 )
	begin
		if( rising_edge( clk100 )) then
			ModeInt				<= conv_integer( MODE );
			
			if( PA_Reset = '1' )
				then dly_data( 17 downto 0 ) <= conv_std_logic_Vector( -40000, 18 );
				else dly_data( 17 downto 0 ) 	<= ad_data;			-- Already signed so no conversion necessary
			end if;
		
			------------------------------------------------------------------------------------
			-- Keep incrementing Write Address
			-- Read Address is a "Constant" behind Write Address
			case ModeInt is
				when 0 		=> Delay_Mux <= conv_std_logic_Vector( PTime_SHigh0-3, Adr_Width );
				when 1 		=> Delay_Mux <= conv_std_logic_Vector( PTime_SHigh1-3, Adr_Width );
				when 2 		=> Delay_Mux <= conv_std_logic_Vector( PTime_SHigh2-3, Adr_Width );
				when 3 		=> Delay_Mux <= conv_std_logic_Vector( PTime_SHigh3-3, Adr_Width );
				when others 	=> Delay_Mux <= conv_std_logic_Vector( PTime_SHigh4-3, Adr_Width );
			end case;
			
			adr_wr				<= adr_wr + 1;
			adr_Rd				<= adr_wr - Delay_Mux;
			-- Keep incrementing Write Address
			------------------------------------------------------------------------------------
		
			------------------------------------------------------------------------------------
			-- Based on Mode select 
			--		Delay for Read Address
			--		Peak Time
			case ModeInt is
				when 0 		=> Peak_time <= conv_Std_logic_Vector( PTime_SHigh0, 12 );
				when 1 		=> Peak_time <= conv_Std_logic_Vector( PTime_SHigh1, 12 );
				when 2 		=> Peak_time <= conv_Std_logic_Vector( PTime_SHigh2, 12 );
				when 3 		=> Peak_time <= conv_Std_logic_Vector( PTime_SHigh3, 12 );
				when others 	=> Peak_time <= conv_Std_logic_Vector( PTime_SHigh4, 12 );
			end case;
			-- Based on Mode select 
			------------------------------------------------------------------------------------
		end if;								-- rising_edge( clk100 )		
	end process;
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Discriminator Delay Memory
	disc_fast_ram_inst : disc_fast_ram
		port map(
			clock			=> clk100,
			wren				=> '1',
			wraddress			=> adr_wr,
			rdaddress			=> adr_Rd,
			data				=> dly_data( 35 downto 0 ),
			q				=> dly_data( 53 downto 18 ) );
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Get the difference from 1st to last ( which will be positive for the duration of the event )
	sum_blr_Inst : lpm_add_sub
		generic map(
			LPM_WIDTH				=> 18,
			LPM_DIRECTION			=> "SUB",
			LPM_REPRESENTATION 		=> "SIGNED" )
		port map(
			Cin					=> '1',
			dataa				=> dly_data( 17 downto  0 ),	-- First data set
			datab				=> dly_data( 53 downto 36 ),	-- Last data set
			result				=> sum_blr );		
			
			
	--VERY IMPORTANT!! INVESTIGATE. 		
	BLR_Disable_Inst : lpm_compare
		generic map(
			LPM_WIDTH				=> 18,
			LPM_REPRESENTATION		=> "UNSIGNED" )
		port map(	
			dataa				=> sum_blr,
			datab				=> conv_std_Logic_Vector( BLR_THR, 18 ),
			agb					=> BLR_Disable );					
	-- Get the difference from 1st to last ( which will be positive for the duration of the event )
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- First and Second Stage Create:
	-- sum = add_Vec1 - 2Xsub_Vec1 + add_Vec2
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Triangular Filter Generation 
	-- 1st Stage
	sub_total1_Inst : lpm_add_sub
		generic map(
			LPM_WIDTH			=> Sum_Bits,
			LPM_DIRECTION		=> "SUB",
			LPM_REPRESENTATION 	=> "SIGNED" )
		port map(
			Cin				=> '1',
			dataa			=> sxt( dly_data( 17 downto  0 ), 	 Sum_Bits ),	-- adata
			datab			=> sxt( dly_data( 35 downto 18 ) & '0', Sum_Bits ),	-- 2Xadata_del1
			result			=> sub_total1 );								-- dataa-datab
	-- 2nd stage
	sum_Inst : lpm_add_sub
		generic map(
			LPM_WIDTH			=> Sum_Bits,
			LPM_DIRECTION		=> "ADD",
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_PIPELINE		=> 1 )
		port map(
			clock			=> CLK100,
			Cin				=> '0',
			dataa			=> sub_total1,							-- ( dataa-datab)-datab_del3
			datab			=> sxt( dly_data( 53 downto 36 ), Sum_Bits ),		
			result			=> sum );								-- (( dataa-datab)-datab_del3 ) + datac_del3
	-- FIR - Second Stage 
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- fir_acc_add = 2^16*Sum + Fir_Acc
	fir_acc_add_Inst : lpm_add_Sub
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_DIRECTION		=> "ADD",
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(	
			cin				=> '0',
			dataa			=> Fir_Acc,
			datab			=> sxt( ( sum & x"0000" ), acc_width ),	-- X 2^16 and Sign Extend		
			result			=> fir_acc_add );	
	-- fir_acc_add = SumMult + Fir_Acc
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Third ( Accumulate ) Stage
	-- Accumulate sub_total2 the previous sub_total with the FIR value		
	BLR_Offset_Inst : lpm_add_Sub
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_DIRECTION		=> "SUB",
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			cin				=> '1',
			dataa			=> fir_acc_add,
			datab			=> BLR_Mux,
			result			=> BLR_Offset );
	-- Third ( Accumulate ) Stage
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	fir_acc_Proc : process( clk100 )
	begin
		if( rising_edge( clk100 )) then		
			------------------------------------------------------------------------------------
			-- BLR Mux
			case ModeInt is
				when 0 		=> BLR_Mux <= sxt( Fir_Acc( Acc_Width-1 downto Dout_LSB   ) & x"0", Acc_Width );	-- 0.12us
				when 1 		=> BLR_Mux <= sxt( Fir_Acc( Acc_Width-1 downto Dout_LSB+1 ) & x"0", Acc_Width );	-- 0.24us
				when 2 		=> BLR_Mux <= sxt( Fir_Acc( Acc_Width-1 downto Dout_LSB+2 ) & x"0", Acc_Width );	-- 0.48s
				when 3 		=> BLR_Mux <= sxt( Fir_Acc( Acc_Width-1 downto Dout_LSB+3 ) & x"0", Acc_Width );	-- 0.96us
				when others	=> BLR_Mux <= sxt( Fir_Acc( Acc_Width-1 downto Dout_LSB+4 ) & x"0", Acc_Width );	-- 7.68us
			end case;			
			-- BLR Mux
			------------------------------------------------------------------------------------

			------------------------------------------------------------------------------------	
			-- Main Filter Accumulator
			-- When to Clear it			
			if(( PA_Reset = '1' ) or ( PA_Busy = '1' ))
				then Fir_Acc	<= Fir_Acc_Add;
--				then Fir_Acc 	<= (others=>'0');

			-- When to "Inhibit" BLR Update
			elsif( BLR_Disable = '1' )
				then Fir_Acc	<= Fir_Acc_Add;
					
			-- Otherwise normal accumulation
				else Fir_Acc	<= Blr_Offset;
			end if;
			------------------------------------------------------------------------------------
			
			------------------------------------------------------------------------------------	
			-- Filter Data
			Int_Fir_Data	<= Fir_Acc( 19+Dout_LSB+ModeInt downto Dout_LSB+ModeInt);
			-- Clamp Off the output during a reset (just for Dss View)
			if(( PA_Reset = '1' ) or ( PA_Busy = '1' ))
				then Fir_Data <= (others=>'0');
				else Fir_Data <= Int_Fir_Data;
			end if;		
		end if;
	end process;	
	----------------------------------------------------------------------------------------------
	

	----------------------------------------------------------------------------------------------
	-- Compare fir_Data to the threshold level
	Level_Cmp_Inst : lpm_compare
		generic map(
			lpm_width			=> 20,
			lpm_representation	=> "SIGNED" )
		port map(	
			dataa			=> Int_fir_data,		-- Use the Delayed Filter Input to match-up with slopde detection
			datab			=> sxt( TLevel, 20 ),	-- Discriminator Threshold Level
			agb				=> Level_Cmp );		-- Filter is above the threshold level
	-- Compare fir_Data to the threshold level
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Compare for Slope Determination (Negative Slope)
	Slope_Cmp_Inst : lpm_compare
		generic map(
			LPM_WIDTH				=> 20,
			LPM_REPRESENTATION		=> "SIGNED" )
		port map(
			dataa				=> Int_fir_data, 
			datab				=> fir_data_del,
			alb					=> NSlope_Cmp );
	-- Compare for Slope Determination (Negative Slope)
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	clock_proc : process( CLK100 )
	
	begin 
		if( rising_edge( CLK100 )) then
			-- And Pipeline stages for slope detection
			fir_data_del1 	<= Int_fir_data;
			fir_data_del	<= fir_data_del1;		
			level_vec		<= Level_Vec(0) & Level_Cmp;		-- Stretch Level_Cmp by number of pipeline delays
			-- Filter Data
			------------------------------------------------------------------------------------	

			------------------------------------------------------------------------------------	
			-- Discriminator threshold Level
			if(( PA_Busy = '0' ) and ( Disc_En = '1' ) and (( Level_Cmp = '1' ) or ( Level_Vec(0) = '1' ) or ( Level_Vec(1) = '1' )))
				then Disc_Level <= '1';
				else Disc_Level <= '0';
			end if;
			-- Discriminator threshold Level
			------------------------------------------------------------------------------------	

			------------------------------------------------------------------------------------	
			-- Setup for a Leading Edge Detect of the Negative Slope when above threshold
			if(( PA_Busy = '0' ) and ( Disc_En = '1' ) and ( Disc_Level = '1' ) and ( NSlope_Cmp = '1' ))
				then NSlope_Vec(0) <= '1';
				else NSlope_Vec(0) <= '0';
			end if;
			-- Setup for a Leading Edge Detect of the Negative Slope when above threshold
			------------------------------------------------------------------------------------	
			
			------------------------------------------------------------------------------------	
			-- Now the Leading Edge of a Negative Slope when above threshold results in a Disc_Out signal
			if(( PA_Busy = '0' ) and ( Disc_en = '1' ) and ( Disc_Level = '1' ) and ( NSlope_Cmp = '1' ) and ( NSlope_Vec(0) = '0' ))
				then Disc_Out <= '1';
				else Disc_out <= '0';
			end if;
			------------------------------------------------------------------------------------	

			------------------------------------------------------------------------------------
			-- Delay the "Disc_Out" signal long enough to be under a PA_Reset, since the reset 
			-- tends to trigger a discriminator count
			Disc_Out_Vec <= Disc_Out_Vec( Disc_Out_Max-1 downto 0 ) & Disc_Out;
			
			if(( Disc_Out_Vec(Disc_Out_Max) = '1' ) and ( PA_Busy = '0' ))
				then Disc_Out_Cps <= '1';
				else Disc_Out_Cps <= '0';
			end if;
			-- Delay the "Disc_Out" signal long enough to be under a PA_BUSY, since the reset 
			-- tends to trigger a discriminator count
			------------------------------------------------------------------------------------
			
			------------------------------------------------------------------------------------
			-- Now Count the number of Discriminator Counts ( up to 8 bits ) for AutoDisc
			if( Time_Clr = '1' )
				then Disc_Cnts <= (others=>'0');
			elsif(( Time_Enable = '1' ) and ( conv_integer( not( Disc_Cnts )) /= 0 ) and ( Disc_Out_CPS = '1' ))
				then Disc_Cnts <= Disc_Cnts + 1;
			end if;							
			-- Now Count the number of Discriminator Counts ( up to 8 bits ) for AutoDisc
			------------------------------------------------------------------------------------									
		end if;
	end process;
	----------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
end behavioral;               -- Disc_Fast.vhd
---------------------------------------------------------------------------------------------------

