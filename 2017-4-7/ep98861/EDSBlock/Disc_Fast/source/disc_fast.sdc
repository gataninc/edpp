## Generated SDC file "AD5453.sdc"

## Copyright (C) 1991-2010 Altera Corporation
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, Altera MegaCore Function License 
## Agreement, or other applicable license agreement, including, 
## without limitation, that your use is for the sole purpose of 
## programming logic devices manufactured by Altera and sold by 
## Altera or its authorized distributors.  Please refer to the 
## applicable agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 10.0 Build 218 06/27/2010 SJ Full Version"

## DATE    "Wed Sep 22 08:19:27 2010"

##
## DEVICE  "EP3C40U484C7"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {CLK100}  -period 10.000 -waveform { 0.000 5.000 } [get_ports {CLK100}]


#**************************************************************
# Create Generated Clock
#**************************************************************

#**************************************************************
# Set Clock Latency
#**************************************************************

#**************************************************************
# Set Clock Uncertainty
#**************************************************************

derive_clock_uncertainty

#**************************************************************
# Set Input Delay
#**************************************************************


set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {PA_Reset}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {PA_Reset}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {PA_Busy}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {PA_Busy}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {Fir_Reset}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {Fir_Reset}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {Disc_En}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {Disc_En}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {Time_Clr}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {Time_Clr}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {Time_Enable}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {Time_Enable}]


set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {Mode[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {Mode[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {tlevel[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {tlevel[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {ad_data[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {ad_data[*]}]

#**************************************************************
# Set Output Delay
#**************************************************************

set_output_delay -clock { CLK100 } -max 4.000 [get_ports {Peak_time[*]}]
set_output_delay -clock { CLK100 } -min 3.000 [get_ports {Peak_time[*]}]

set_output_delay -clock { CLK100 } -max 4.000 [get_ports {fir_data[*]}]
set_output_delay -clock { CLK100 } -min 3.000 [get_ports {fir_data[*]}]

set_output_delay -clock { CLK100 } -max 4.000 [get_ports {Disc_Out}]
set_output_delay -clock { CLK100 } -min 3.000 [get_ports {Disc_Out}]

set_output_delay -clock { CLK100 } -max 4.000 [get_ports {Disc_Level}]
set_output_delay -clock { CLK100 } -min 3.000 [get_ports {Disc_Level}]

set_output_delay -clock { CLK100 } -max 4.000 [get_ports {Disc_Cnts[*]}]
set_output_delay -clock { CLK100 } -min 3.000 [get_ports {Disc_Cnts[*]}]

#**************************************************************
# Set Clock Groups
#**************************************************************



#**************************************************************
# Set False Path
#**************************************************************



#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

