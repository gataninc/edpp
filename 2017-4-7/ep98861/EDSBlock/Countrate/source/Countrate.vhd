-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	cpscntr.VHD
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--   Description:	
--		This module maintains counts at 5 X 200ms time intervals
--		It also maintains the counts based on an input CNT_EN signal,
--		for Net Counts
--
--		Maintains a running sum of the input and output count for the 
--		past 5 time intervals:
--			SUM = Cnt(n) + Cnt(n-1) ...+Cnt(n-4)
--		Therefore the resulting count rates are per 1 sec but updated 
--		every 200ms
--
--
--   History:       <date> - <Author>
-------------------------------------------------------------------------------

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

-------------------------------------------------------------------------------
entity Countrate is 
     port( 
          CLK100	    	: in      std_logic;                         -- Master Clock
		ms100_tc		: in		std_Logic;					-- 100ms Timer
		inc_cps		: in		std_logic;					-- FDISC or PEAK_DONE
		clear		: in		std_logic;
		enable		: in		std_logic;
		cps			: buffer	std_logic_Vector( 31 downto 0 );
		net			: buffer	std_logic_Vector( 31 downto 0 ) );
     end Countrate;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture BEHAVIORAL of Countrate is

	type CP_MS_TYPE is array( 4 downto 0 ) of std_Logic_Vector( 20 downto 0 );
	
	signal cp_ms		: CP_MS_Type;
	signal cps_sum		: std_Logic_vector( 23 downto 0 );		

	signal ms_cntr		: std_logic_Vector( 20 downto 0 ):= (others=>'0');
	signal ms100_vec	: std_logic_Vector( 1 downto 0 );
	signal ms200_en	: std_Logic;

	-- Parallel Adder to add up 5x 200ms number of counts -------------------
	component CPS_PAdd
		PORT(
			clock		: in 	STD_LOGIC  := '0';
			data0x		: in 	std_logic_vector (20 downto 0);
			data1x		: in 	std_logic_vector (20 downto 0);
			data2x		: in 	std_logic_vector (20 downto 0);
			data3x		: in 	std_logic_vector (20 downto 0);
			data4x		: in 	std_logic_vector (20 downto 0);
			result		: out 	std_logic_vector (23 downto 0 ) );
	end component;	
	-- Parallel Adder to add up 5x 200ms number of counts -------------------

begin
	-- Parallel Adder to add up 5x 200ms number of counts -------------------
	CPS_PAdd_Inst : CPS_PAdd
		port map(
			clock		=> CLK100,
			data0x		=> cp_ms(0),
			data1x		=> cp_ms(1),
			data2x		=> cp_ms(2),
			data3x		=> cp_ms(3),
			data4x		=> cp_ms(4),
			result		=> cps_sum );	
	-- Parallel Adder to add up 5x 200ms number of counts -------------------
     --------------------------------------------------------------------------
	clock_proc : process( CLK100 )
     begin
		if rising_edge( CLK100 ) then
			ms100_vec <= ms100_vec(0) & ms100_tc;
			cps 		<= ext( cps_sum, 32 );

			if(( ms100_vec = "01" ) and ( ms200_en = '1' ))
				then ms_cntr <= (others=>'0');
			elsif(( inc_cps = '1' ) and ( conv_integer( not( ms_cntr )) /= 0 ))
				then ms_cntr <= ms_cntr + 1;
			end if;
			
			-- Leading Edge of 100ms timer
			if( ms100_vec = "01" ) then			
				ms200_en <= not( ms200_en );
				
				if( ms200_en = '1' ) then
					cp_ms(0)		<= ms_cntr;
					cp_ms(1)		<= cp_ms(0);
					cp_ms(2)		<= cp_ms(1);
					cp_ms(3)		<= cp_ms(2);
					cp_ms(4)		<= cp_ms(3);
				end if;
			end if;
						
			if( clear = '1' )
				then net <= (others=>'0');
			elsif(( enable = '1' ) and ( inc_cps = '1' ) and ( conv_integer( not( net )) /= 0 ))
				then net <= net + 1;
			end if;
		end if;                  -- MR/RED Clock
	end process;                  -- clock_Proc
     ----------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
end BEHAVIORAL;          -- Countrate.VHD
---------------------------------------------------------------------------------------------------

