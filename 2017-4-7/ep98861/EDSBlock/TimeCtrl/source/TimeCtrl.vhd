--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	Cntrate.VHD
-------------------------------------------------------------------------------

----------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-III EDS FPGA
--   Schematic      : 4035.007.25600 Rev A
--
--	Top Level		: 4035.009.98860, S/W, DPP-III, FPGA, SILI
--
--   Description:	
--
--	History
--		05/16/2012 
--			Changed SPEC_UPDATE to not be updated if Preset_FMAP or Preset_SMAP is enabled
--		04/29/12 Ver 2.0
--			Preset SMAP_CLOCK and SMAP_LIVE were added
--		02/14/2008 MCS 08021400 
--			PA_RESET = '0' was added to enable LTIME to be incremented
-------------------------------------------------------------------------------
library LPM;
     use lpm.lpm_components.all;

library altera;
	use altera.maxplus2.all;

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

-------------------------------------------------------------------------------
entity TimeCtrl is 
     port( 
          CLK100			: in      std_logic;                                   	-- Master Clock
		EXT_TRIG			: in		std_logic;
		Time_Clr			: in		std_logic;
		Time_Start		: in		std_logic;
		Time_Stop			: in		std_logic;	
		Time_Reset		: in		std_logic;
		BH_LTIME_EN		: in 	std_logic;
		PRESET_MODE		: in		std_logic_Vector(  2 downto 0 );
		PRESET			: in		std_logic_Vector( 31 downto 0 );
		Pixels_Line		: in		std_Logic_Vector( 31 downto 0 );	-- How many Pixels/Line
		Lines_Frame		: in		std_logic_Vector( 31 downto 0 );	-- How Many Lines/Frame
		Spec_Update		: buffer	std_logic;
		Time_Enable		: buffer	std_logic;
		Preset_Fmap		: buffer	std_logic;
		Preset_SMap		: buffer	std_logic;
		CTIME			: buffer	std_logic_Vector( 31 downto 0 );
		LTIME			: buffer	std_logic_Vector( 31 downto 0 );
		Line_Code			: buffer	std_logic_Vector(  1 downto 0 );
		Pixel_Status		: buffer	std_logic_Vector( 15 downto 0 );
		Line_Status		: buffer	std_logic_Vector( 15 downto 0 );
		Frame_Status		: buffer	std_logic_Vector( 15 downto 0 ) );
     end TimeCtrl;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture BEHAVIORAL of TimeCtrl is
	
	-- CONSTANT DECLARATIONS ---------------------------------------------------------------------
	constant CLOCK_PER			: integer := 10;
	constant us1_max   			: integer :=       1000/CLOCK_PER;				-- 1 us at 100Mhz
	constant ms1_max   			: integer :=    1000000/CLOCK_PER;				-- 1 ms at 100Mhz
	constant ms100_max			: integer :=  100000000/CLOCK_PER;				-- 100ms
	
	constant Preset_100ms_at1us	: integer := ms100_max / 1000;				-- At 1 us Interval
	constant Preset_100ms_at1ms	: integer := ms100_max / 1000000;				-- At 1 ms Interval
	

	constant PR_NONE 			: integer := 0;
	constant PR_CLOCK			: integer := 1;
	constant PR_LIVE			: integer := 2;
	constant PR_LSMAP_CLOCK		: integer := 3;
	constant PR_LSMAP_LIVE		: integer := 4;
	constant PR_SMAP_CLOCK		: integer := 5;
	constant PR_SMAP_LIVE		: integer := 6;
	
	constant BH_LT_CNTR_MAX 		: integer := 127;		-- 1.2% 63;	-- 15;
	constant BH_LT_CNTR_PRESET	: integer := 64;		-- 1.2% 32;	-- 8;

	constant Pst_Disabled 		: integer := 0;
	constant Pst_Clear			: integer := 1;
	constant Pst_Idle			: integer := 2;
	constant Pst_Wait_Done		: integer := 3;	
	constant Pst_Eof			: integer := 4;	
	constant Pst_Eoln 			: integer := 5;	
	constant Pst_Inc_Pixel 		: integer := 6;		
	constant Pst_Done			: integer := 7;
	-- CONSTANT DECLARATIONS ---------------------------------------------------------------------

	-- SIGNAL DECLARATIONS -----------------------------------------------------------------------
	
	signal ct_tc				: std_logic := '0';
	signal CTime_Cnt 			: integer range 0 to ms1_max := 0;

	signal Early_Preset_Val		: std_logic_Vector( 31 downto 0 ):= (others=>'0');
	signal Ext_Trig_Vec			: std_logic_vector( 1 downto 0 ) := (others=>'0');
	
	signal Frame_Cnt			: std_logic_Vector( 15 downto 0 ) := (others=>'0');

	signal LTime_Cnt 			: integer range 0 to ms1_max := 0;
	signal LT_tc				: std_logic := '0';
	signal Line_Cnt			: std_logic_Vector( 15 downto 0 ) := (others=>'0');

	signal preset_clock 		: std_Logic := '0';
	signal preset_live 			: std_logic := '0';
	signal Preset_Done			: std_logic := '0';
	signal Preset_Val_reg		: std_logic_Vector( 31 downto 0 ):= (others=>'0'); ----dwell time????
	signal Pixel_Cnt			: std_logic_Vector( 15 downto 0 ) := (others=>'0');
	signal Pixel_Cnt_state 		: integer range 0 to Pst_Done := 0;
	signal Preset_100ms			: std_logic_vector( 31 downto 0 ) := (others=>'0');
	signal ms100_cnt			: integer range 0 to ms100_max;
	signal ms100tc				: std_logic;
	signal Preset_Early_CTime_En	: std_logic;
	signal Preset_Early_LTime_En	: std_Logic;
	signal Preset_100ms_Cmp		: std_logic;
	
	signal EOLN_CMP 			: std_logic;
	signal EOF_CMP				: std_logic;
	signal Time_Clr_Reg			: std_logic;
	signal Time_Reset_Reg		: std_logic;
	signal Time_Reset_Reg_Del	: std_logic;

	-- SIGNAL DECLARATIONS -----------------------------------------------------------------------

begin
	----------------------------------------------------------------------------------------------
	EOLN_COMPARE : lpm_compare
		generic map(
			LPM_WIDTH			=> 16,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			dataa	 		=> Pixel_Cnt,
			datab			=> Pixels_Line( 15 downto 0 ),
			ageb				=> EOLN_CMP );

	EOF_COMPARE : lpm_compare
		generic map(
			LPM_WIDTH			=> 16,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			dataa 			=> Line_Cnt,
			datab			=> Lines_Frame( 15 downto 0 ),
			ageb				=> EOF_CMP );					
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Preset > 100ms
	Preset_100ms_Cmp_Inst : lpm_compare
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			dataa 			=> Preset_Val_reg,
			datab			=> Preset_100ms,
			ageb				=> Preset_100ms_Cmp );
	-- Preset > 100ms
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Dependent on Preset Mode, which Time to use -----------------------------------------------

	-- Determine if Preset value Greater than 100ms ( @ 1us ) ------------------------------------
	Early_Preset_Add_Sub : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "UNSIGNED",
			LPM_DIRECTION		=> "SUB",
			LPM_PIPELINE		=> 1 )
		port map(
			clock			=> CLK100,
			cin				=> '1',	
			dataa			=> Preset_Val_reg,
			datab			=> Preset_100ms,		
			result			=> Early_Preset_Val );
			
	Preset_Early_CTime_En_Inst : lpm_compare
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			dataa			=> CTime,
			datab			=> Early_Preset_Val,
			alb				=> Preset_Early_CTime_En );

	Preset_Early_LTime_En_Inst : lpm_compare
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			dataa			=> LTime,
			datab			=> Early_Preset_Val,
			alb				=> Preset_Early_LTime_En );

	----------------------------------------------------------------------------------------------

	ms100tc	<= '1' when ( ms100_cnt >= ms100_max ) else '0';
	----------------------------------------------------------------------------------------------
	-- When is Mapping mode, LSB of Preset, Clock Time and Live Time is 1 us otherwise it is 1ms 
	--	This is where the time-base is used	
	ct_tc		<= '1' when (( Preset_FMap = '1' ) and ( CTime_Cnt = us1_max )) else
	                  '1' when (( Preset_FMap = '0' ) and ( CTime_Cnt = ms1_max )) else 
	                  '0';		

	LT_tc		<= '1' when (( Preset_FMap = '1' ) and ( LTime_Cnt =  us1_max )) else 
	                  '1' when (( Preset_FMap = '0' ) and ( LTime_Cnt =  ms1_max )) else
	                  '0';
	-- When is Mapping mode, LSB of Preset, Clock Time and Live Time is 1 us otherwise it is 1ms 
	----------------------------------------------------------------------------------------------
	
	Preset_Done <= '1' when (( Preset_Live  = '1' ) and ( LTime >= Preset_Val_Reg ) and ( BH_LTIME_EN = '1' ) and ( LT_Tc = '1' )) else 
				'1' when (( Preset_Clock = '1' ) and ( CTime >= Preset_Val_Reg ) and ( Ct_tc = '1' )) else 
				'1' when (( Preset_FMap  = '1' ) and ( Ext_Trig_Vec = "10" )) else
				'0';

	----------------------------------------------------------------------------------------------
	clock_proc : process( CLK100 )
     begin
		if rising_edge( CLK100 ) then
			Ext_Trig_Vec		<= Ext_Trig_Vec(0) & Ext_Trig;
			Preset_Val_reg		<= Preset - 1;
			Time_Clr_Reg 		<= Time_Clr;
			Time_Reset_Reg		<= Time_Reset;
			Time_Reset_Reg_Del	<= Time_Reset_Reg;		
			-- Static Mode Decodes based on Preset_Mode ----------------------------------------
			case conv_integer( Preset_Mode) is
				when PR_NONE 		=> Preset_Clock <= '0'; Preset_Live <= '0'; Preset_FMap <= '0'; Preset_SMap <= '0'; 
				when PR_CLOCK		=> Preset_Clock <= '1'; Preset_Live <= '0'; Preset_FMap <= '0'; Preset_SMap <= '0';   
				when PR_LIVE		=> Preset_Clock <= '0'; Preset_Live <= '1'; Preset_FMap <= '0'; Preset_SMap <= '0';  
				when PR_LSMAP_CLOCK	=> Preset_Clock <= '1'; Preset_Live <= '0'; Preset_FMap <= '1'; Preset_SMap <= '0';  
				when PR_LSMAP_LIVE	=> Preset_Clock <= '0'; Preset_Live <= '1'; Preset_FMap <= '1'; Preset_SMap <= '0'; 
				when PR_SMAP_CLOCK	=> Preset_Clock <= '1'; Preset_Live <= '0'; Preset_FMap <= '0'; Preset_SMap <= '1'; 
				when PR_SMAP_LIVE	=> Preset_Clock <= '0'; Preset_Live <= '1'; Preset_FMap <= '0'; Preset_SMap <= '1';
				when others 		=> Preset_Clock <= '0'; Preset_Live <= '0'; Preset_FMap <= '0'; Preset_SMap <= '0';   
			end case;
			
--			case conv_integer( Preset_Mode ) is
--				when PR_CLOCK 		=> Preset_Clock <= '1';	-- Preset Clock
--				when PR_LSMAP_CLOCK	=> Preset_Clock <= '1';	-- Preset Clock @Fast Mapping Ver 3b04
--				when PR_SMAP_CLOCK  => Preset_Clock <= '1';
--				when others		=> Preset_Clock <= '0';
--			end case;
			
--			case conv_integer( Preset_Mode ) is
--				when PR_LIVE 		=> Preset_Live <= '1';	-- Preset Live
--				when PR_LSMAP_LIVE	=> Preset_Live <= '1';
--				when PR_SMAP_LIVE   => Preset_Live <= '1';
--				when others		=> Preset_Live <= '0';
--			end case;

--			-- This is used by DSP_Data_Gen, only to be set during fast mapping
--			case conv_integer( Preset_Mode ) is
--				when PR_LSMAP_CLOCK	=> Preset_Map <= '1';
--				when PR_LSMAP_Live	=> Preset_Map <= '1';
--				--when PR_SMAP_CLOCK	=> Preset_Map <= '1';
--				--when PR_SMAP_LIVE	=> Preset_Map <= '1';
--				when others		=> Preset_Map <= '0';		
--			end case;			
--			
--			`
--			-- Fast Mapping Preset sets 1us clock time
--			case conv_integer( Preset_Mode ) is
--				when PR_LSMAP_CLOCK	=> Preset_FMap <= '1';
--				when PR_LSMAP_Live	=> Preset_FMap <= '1';
--				when others		=> Preset_FMap <= '0';
--			end case;
--			
--			-- Slow Mapping
--			case conv_integer( Preset_Mode ) is
--				when PR_SMAP_CLOCK	=> Preset_SMap <= '1';
--				when PR_SMAP_LIVE	=> Preset_SMap <= '1';
--				when others		=> Preset_SMap <= '0';
--			end case;
			
			-- Static Mode Decodes based on Preset_Mode ------------------------------------

			if( Preset_FMap = '1' )
				then Preset_100ms <= conv_std_logic_vector( Preset_100ms_at1us, 32 );
				else Preset_100ms <= conv_Std_logic_vector( Preset_100ms_at1ms, 32 );
			end if;

		
               ------------------------------------------------------------------------------------
			-- 100ms Timer for Spectrum Updating 
			if( Time_Enable = '0' )
				then ms100_cnt <= 0;
			elsif( ms100tc = '1' )
				then ms100_cnt <= 0;
				else ms100_cnt <= ms100_cnt + 1;
			end if;
			-- 100ms Timer for Spectrum Updating 
               ------------------------------------------------------------------------------------
		
               -- Clock and Live Time Base Timer --------------------------------------------------
--			if( (( Preset_FMap = '1' ) and ( Ext_Trig_Vec = "01" ))
--			 or (( Preset_SMap = '1' ) and ( Time_Reset_Reg = '1' ))
--			 or (( Preset_FMap = '0' ) and ( Preset_SMap = '0' ) and ( Time_Clr_Reg = '1' ))
--			 or (( Time_Enable = '1' ) and ( Preset_Done = '1' ))
--			 or ( Time_Clr_Reg = '1' )
--			 or ( Time_Reset_Reg = '1' )
			 if( Time_Enable = '0' ) then
				CTime_Cnt <= 0;
				LTime_Cnt <= 0;
			else
				if( ct_tc = '1' )
					then CTime_Cnt <= 0;
					else CTime_Cnt <= CTime_Cnt + 1;
				end if;
				
				if(( BH_LTIME_EN = '1' ) and ( LT_tc = '1' )) 
					then LTime_Cnt <= 0;
				elsif(( BH_LTIME_EN = '1' ) and ( LT_tc = '0' ))
					then LTime_Cnt <= LTime_Cnt + 1;
				end if;				
			end if;
               -- Clock Timer Base Timer ------------------------------------------------------

			-- Clock and Live Time Counters ------------------------------------------------

			if( (( Preset_FMap = '1' ) and ( Ext_Trig_Vec = "01" ))
			 or (( Preset_SMap = '1' ) and ( Time_Reset_Reg = '1' ))
			 or (( Preset_FMap = '0' ) and ( Preset_SMap = '0' ) and ( Time_Clr_Reg = '1' ))  ) then

			
--			if( (( Preset_FMap = '1' ) and ( ExT_Trig_Vec = "01" ))
--			  or ( Time_Clr_Reg = '1' ) 
--			  or ( Time_Reset_Reg = '1' )) then
				CTime <= (others=>'0');
				LTime <= (others=>'0');

			elsif( Time_Enable = '1' ) then
				if( ct_tc = '1' )
					then Ctime <= CTime + 1;
				end if;
				
				if(( BH_LTIME_EN = '1' ) and ( LT_Tc = '1' ))
					then LTime <= LTime + 1;
				end if;				
			end if;
			-- Clock and Live Time Counters ------------------------------------------------


			-- Acquisition Timer Enable---------------------------------------------------------						
			if( Time_Stop = '1' ) 
				then Time_Enable <= '0';
			elsif(( Time_Enable = '1' ) and ( Preset_Done = '1' ))
				then Time_Enable <= '0';
			elsif(( Preset_FMap = '1' ) and ( Ext_Trig_Vec = "01" ))
				then Time_Enable <= '1';
			elsif(( Preset_SMap = '1') and ( Time_Reset_Reg_Del = '1' ))		--  Slow Mapping
				then Time_Enable <= '1';
			elsif(( Preset_FMap = '0' ) and ( Preset_SMap = '0' ) and ( Time_Start = '1' ))
				then Time_Enable <= '1';
				
			end if;
			-- Acquisition Timer Enable---------------------------------------------------------

			-- Spec Update Defines when data is to be sent to the host
			-- If Preset is Doned
			-- When Acquisition is not enabled
			-- Acq not enabled but baseline histogram is enabled @ 100ms				
			if(( Preset_FMap = '1' ) or ( Preset_SMap = '1' ))
				then Spec_Update <= '0';		-- Don't Updated during Preset FMap or SMap (Update will occur by TED Time Disable during SMAP and FMAP modes ))
				
			elsif(( Time_Enable = '1' ) and ( Preset_Done = '1' ))
				then Spec_Update <= '1';
				
			elsif(( Time_Enable = '1' ) and ( ms100tc = '1' )) then					
				if(( Preset_Clock = '0' ) and ( Preset_Live = '0' ))
					then Spec_Update <= '1';
			
				elsif(( Preset_100ms_cmp = '1' )) then		-- PReset IS Greater than 100ms
					if(( Preset_Clock = '1' ) and ( Preset_Early_CTime_En = '1' ))			
						then Spec_Update <= '1';				

					elsif(( Preset_Live = '1' ) and ( Preset_Early_LTime_En = '1' ))			
						then Spec_Update <= '1';				
						else Spec_Update <= '0';
					end if;
				else
					Spec_Update <= '0';
				end if;						
			else
				Spec_Update <= '0';
			end if;
														
			-- Pixel Counter -----------------------------------------------------				
			if( (( Preset_Fmap = '0' ) and ( Preset_SMap = '0' ))
				or ( Time_Clr_Reg = '1' )
				or ( Pixel_Cnt_State = Pst_Eof )
				or ( Pixel_Cnt_State = Pst_Eoln ))
					then Pixel_Cnt <= conv_Std_logic_vector( 1, 16 );				
			elsif( Pixel_Cnt_State = Pst_Inc_Pixel )
				then Pixel_Cnt <= Pixel_Cnt + 1;			
			end if;
			-- Pixel Counter -----------------------------------------------------
			
			-- Line Counter ------------------------------------------------------				
			if( (( Preset_Fmap = '0' ) and ( Preset_SMap = '0' ))
				or ( Time_Clr_Reg = '1' )
				or ( Pixel_Cnt_State = Pst_Eof ))
					then Line_Cnt <= conv_Std_logic_vector( 1, 16 );							
			elsif( Pixel_Cnt_State = Pst_Eoln )
				then Line_Cnt <= Line_Cnt + 1;
			end if;
			-- Line Counter ------------------------------------------------------
			
			-- Frame Counter -----------------------------------------------------
			if( Time_Clr_Reg = '1' )
				then Frame_Cnt <= conv_Std_logic_vector( 1, 16 );
				
			elsif(( Preset_Fmap = '0' ) and ( Preset_SMap = '0' ))
				then Frame_Cnt <= conv_Std_logic_vector( 1, 16 );
				
			elsif( Pixel_Cnt_State = Pst_Eof )
				then Frame_Cnt <= Frame_Cnt + 1;
			end if;
			-- Frame Counter -----------------------------------------------------
			
			-- Update Status -----------------------------------------------------
			if((( Preset_FMap = '0' ) and ( Preset_SMap = '0' ))
				or ( Time_Clr_Reg = '1' ) 
				or ( Pixel_Cnt_State = Pst_Done )) then
					Pixel_Status 	<= Pixel_Cnt;
					Line_Status	<= Line_Cnt;
					Frame_Status	<= Frame_Cnt;
					
					if( conv_integer( Pixel_Cnt ) = 1 )
						then Line_Code <= "01";					-- Start of a Line
					elsif( EOLN_CMP = '0' )						
						then Line_Code <= "00";					-- Middle of a Line
					elsif(( EOLN_CMP = '1' ) and ( EOF_CMP = '0' )) 	
						then line_code <= "10";					-- End of LIne
					elsif(( EOLN_CMP = '1' ) and ( EOF_CMP = '1' )) 	
						then Line_Code <= "11";					-- End of Frame
					end if;
			end if;
			-- Update Status -----------------------------------------------------	


			-- Pixel Counting State for Live Mapping ---------------------------------------		
			-- Mapping Mode State Machine
			case Pixel_Cnt_State is
				when Pst_Disabled =>
				
					if(( Preset_FMap = '1' ) or ( Preset_SMap = '1' ))
						then Pixel_Cnt_state <= Pst_Idle;
					end if;
				
				when Pst_Idle =>															
					if(( Preset_FMap = '0' ) and ( Preset_SMap = '0' ))
						then Pixel_Cnt_State <= Pst_Disabled;
					elsif( Time_Enable = '1' )
						then Pixel_Cnt_State <= Pst_Wait_Done;
					end if;

				--/////////////////////////////////////////////////////////////////////////////
				-- Mapping is active here 				
			
				-- Wait for TIME_ENABLE to go in-active ---------------------------------------
				when Pst_Wait_Done =>				
					if(( Preset_FMap = '0' ) and ( Preset_SMap = '0' ))
						then Pixel_Cnt_State <= Pst_Disabled;
					elsif( Time_Enable = '0' ) then
						if( EOLN_CMP = '0' )						-- Not End of Line (or frame)
							then Pixel_Cnt_State <= Pst_Inc_Pixel;
						elsif(( EOLN_CMP = '1' ) and ( EOF_CMP = '1' )) 	-- End of Frame
							then Pixel_Cnt_State <= Pst_EOF;
						elsif(( EOLN_CMP = '1' ) and ( EOF_CMP = '0' )) 	-- End of LIne
							then Pixel_Cnt_State <= Pst_EOLN;
						end if;
					end if;
				-- Wait for TIME_ENABLE to go in-active ---------------------------------------
				
				-- End of Frame State ( used to increment frame counter and reset pixel and line counter )
				when Pst_Eof =>
					Pixel_Cnt_State <= Pst_Done;
				-- End of Frame State ( used to increment frame counter and reset pixel and line counter )

				-- End of Line State ( used to increment line counter and reset pixel counter )
				when Pst_Eoln =>
					Pixel_Cnt_State <= Pst_Done;
				-- End of Line State ( used to increment line counter and reset pixel counter )
				
				-- Increment Pixel Counter (don't touch line or frame counters )
				when Pst_Inc_Pixel =>
					Pixel_Cnt_State <= Pst_Done;
				-- Increment Pixel Counter (don't touch line or frame counters )

				when Pst_Done =>
					Pixel_Cnt_State <= Pst_Idle;

				when others =>
					Pixel_Cnt_State <= Pst_Idle;
			end case;
						
		end if;                  -- MR/RED Clock
	end process;                  -- clock_Proc
     --------------------------------------------------------------------------

-------------------------------------------------------------------------------
end BEHAVIORAL;          -- TimeCtrl.VHD
-------------------------------------------------------------------------------
