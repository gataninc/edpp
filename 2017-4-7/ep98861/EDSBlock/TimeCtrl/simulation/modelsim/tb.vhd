library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;
entity tb is
end tb;

architecture test_bench of tb is



-------------------------------------------------------------------------------
component TimeCtrl is 
     port( 
          CLK50			: in      std_logic;                                   	-- Master Clock
		Blevel_En			: in 	std_logic;
		EXT_TRIG			: in		std_logic;
		Time_Clr			: in		std_logic;
		Time_Start		: in		std_logic;
		Time_Stop			: in		std_logic;	
		BH_LTIME_EN		: in 	std_logic;
		PRESET_MODE		: in		std_logic_Vector(  2 downto 0 );
		PRESET			: in		std_logic_Vector( 31 downto 0 );
		Pixels_Line		: in		std_Logic_Vector( 31 downto 0 );	-- How many Pixels/Line
		Lines_Frame		: in		std_logic_Vector( 31 downto 0 );	-- How Many Lines/Frame
		Spec_Update		: buffer	std_logic;
		Time_Enable		: buffer	std_logic;
		Preset_Map		: buffer	std_logic;
		End_Of_Frame		: buffer	std_logic;
		End_of_Map		: buffer	std_logic;
		CTIME			: buffer	std_logic_Vector( 31 downto 0 );
		LTIME			: buffer	std_logic_Vector( 31 downto 0 );
		Line_Code			: buffer	std_logic_Vector(  1 downto 0 );
		Pixel_Status		: buffer	std_logic_Vector( 15 downto 0 );
		Line_Status		: buffer	std_logic_Vector( 15 downto 0 );
		Frame_Status		: buffer	std_logic_Vector( 15 downto 0 ) );
     end component TimeCtrl;
-------------------------------------------------------------------------------
begin

end test_bench;