----------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	Main_RAM.VHD
----------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : September 21, 2007
--   Board          : XScope Instrument Control Board
--   Schematic      : 4035.007.27000 Rev B
--
--   Top Level      : 4035.009.99330, S/W, XScope, Instrument Control
--
--	Description:	
--		This module contains the delay lines to be used for the Embedded DPP Function
--		It has the delay line to accomodate a full 51.2us peaking time at a 125Mhz rate.
--		The Amp Time selections based on "TimeConst" are:
--		0.1, 0.2, 0.4, 0.8, 1.6, 3.2, 6.4, 12.8, 25.6 and 51.2us
--
--   History:
--		12/12/11 - MCS - MAIN_RAM.VHD
--			"Compelted" Peak_Time7 to be 11.52us (the sum of 7.38 and 3.84)
--			Had to adjust the depth of DPRAM and some other related parameters to acoomodate
--			the deeper memory
--			Also had to adjust Megafunction MAIN_RAM_DP to make it twice as deep
-------------------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
	

library altera_mf;
	use altera_mf.altera_mf_components.all;

	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

-------------------------------------------------------------------------------------------
entity Main_RAM is 
	port( 
		CLK100		: in		std_logic;					-- Master Clock
		WE_Fir_Mr		: in		std_logic;
		adata		: in		std_logic_Vector(   17 downto 0 );		-- A/D Input Data
		TimeConst		: in		std_logic_Vector(    2 downto 0 );
		PA_Reset		: in		std_logic;
		Fir_Reset		: buffer	std_logic;
		Peak_Time		: buffer	std_Logic_Vector(  143 downto 48 );
		Peak_Time_Mux	: buffer	std_Logic_Vector(   11 downto  0 );
		Dout			: buffer	std_logic_Vector(   89 downto  0 ) );	-- Delayed Output Data
end Main_RAM;
-------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------
architecture behavioral of Main_RAM is
	
	-- Constant Declarations -------------------------------------------------------------
	constant CLK_PER 			: integer := 10;

	constant Peak_Time0 		: integer :=   120 / CLK_PER;	--    0.2us AT 	( 0.06us delay )
	constant Peak_Time1 		: integer :=   240 / CLK_PER;	--    0.2us AT 	( 0.12us delay )
	constant Peak_Time2 		: integer :=   480 / CLK_PER;	--    0.4us AT 	( 0.24us delay )
	constant Peak_Time3 		: integer :=   960 / CLK_PER;	--    0.8us AT 	( 0.48us delay )
	constant Peak_Time4 		: integer :=  1920 / CLK_PER;	--    1.6us AT 	( 0.96us delay )
	constant Peak_Time5 		: integer :=  3840 / CLK_PER;	--    3.2us AT 	( 1.92us delay )
	constant Peak_Time6 		: integer :=  7680 / CLK_PER;	--    6.4us AT 	( 3.84us delay )
	constant Peak_Time7			: integer := 11520 / CLK_PER;	--   11.52us AT	( 5.76us Delay )
	
	constant ADR_WIDTH			: integer := 10;		-- 6.4us
	
	-- Constant Declarations -------------------------------------------------------------
	
	-- Signal Declarations ---------------------------------------------------------------
	signal AD_CLR_tc			: std_logic := '0';
	signal AD_CLR				: std_Logic := '0';
	
	signal AD_CLR_Cnt 			: integer range 0 to 2*Peak_Time7 := 0;
	signal AD_Clr_Max			: integer range 0 to 2*Peak_Time7 := 0;
	signal Adr_Diff 			: std_logic_vector(  11 downto 0 ):= (others=>'0');

	signal dly_data			: std_logic_vector( 89 downto 0 ):= (others=>'0');

	signal Peaking_Time			: std_logic_Vector( 11 downto 0 );
	
	signal Ram_Out				: std_logic_vector( (3*19)+18 downto 0 );
	signal radr				: std_logic_Vector(  ADR_WIDTH-1 downto 0 ):= (others=>'0');		-- PT1

	signal wadr 				: std_logic_vector(  ADR_WIDTH-1 downto 0 ):= (others=>'0');		-- Used for All
	
	signal TimeConstReg			: integer range 0 to 7;
	-- Signal Declarations ---------------------------------------------------------------
	
	--------------------------------------------------------------------------------------
	component Main_RAM_DP is
		port(
			clock		: in 	std_Logic  := '1';
			wren			: in 	std_Logic  := '0';
			wraddress		: in 	std_logic_vector (  9 downto 0 );
			rdaddress		: in 	std_logic_vector (  9 downto 0 );
			data			: in 	std_logic_vector ( 17 downto 0 );
			q			: out	std_logic_vector ( 17 downto 0 ) );
	end component; 
	--------------------------------------------------------------------------------------

begin
	--------------------------------------------------------------------------------------
	-- 4x xns Delay Lines @40Mhz
	DLY_RAM_GEN : for i in 0 to 3 generate
		Main_Ram_Inst : Main_RAM_DP
			port map(
				clock			=> clk100,
				wren				=> '1',
				wraddress			=> Wadr,
				rdaddress			=> Radr,
				data				=> dly_data( (18*(i+0))+17 downto 18*(i+0) ),
				q				=> dly_data( (18*(i+1))+17 downto 18*(i+1) ));
	end generate;	
	-- 4x xns Delay Lines @40Mhz
	--------------------------------------------------------------------------------------

	AD_CLR_tc 	<= '1' when ( AD_CLR_Cnt = conv_integer( Peaking_Time )) else '0';

	--------------------------------------------------------------------------------------
	clock_proc : process( CLK100 )
	begin
		if( rising_edge( CLK100 )) then		
			-- Somwhat Static Parameters -------------------------------------------------------
			TimeConstReg				<= conv_integer( TimeConst );

			Peak_Time(  59 downto  48) 	<= Conv_Std_logic_Vector( Peak_Time0, 12 ); 				-- AmpTime 0
			Peak_Time(  71 downto  60) 	<= Conv_Std_logic_Vector( Peak_Time1, 12 ); 				-- AmpTime 1
			Peak_Time(  83 downto  72) 	<= Conv_Std_logic_Vector( Peak_Time2, 12 ); 				-- AmpTime 2
			Peak_Time(  95 downto  84) 	<= Conv_Std_logic_Vector( Peak_Time3, 12 ); 				-- AmpTime 3
			Peak_Time( 107 downto  96) 	<= Conv_Std_logic_Vector( Peak_Time4, 12 ); 				-- AmpTime 4
			Peak_Time( 119 downto 108) 	<= Conv_Std_logic_Vector( Peak_Time5, 12 ); 				-- AmpTime 5
			Peak_Time( 131 downto 120) 	<= Conv_Std_logic_Vector( Peak_Time6, 12 ); 				-- AmpTime 6
			Peak_Time( 143 downto 132) 	<= Conv_Std_logic_Vector( Peak_Time7, 12 );				-- AmpTime 7

			case TimeConstReg is
				when 0 		=> Peaking_Time 	<= Peak_Time(  59 downto  48);				-- AmpTime 0
				when 1 		=> Peaking_Time 	<= Peak_Time(  71 downto  60);				-- AmpTime 1
				when 2 		=> Peaking_Time 	<= Peak_Time(  83 downto  72); 				-- AmpTime 2
				when 3 		=> Peaking_Time 	<= Peak_Time(  95 downto  84); 				-- AmpTime 3
				when 4 		=> Peaking_Time 	<= Peak_Time( 107 downto  96); 				-- AmpTime 4
				when 5 		=> Peaking_Time 	<= Peak_Time( 119 downto 108); 				-- AmpTime 5
				when 6	 	=> Peaking_Time 	<= Peak_Time( 131 downto 120); 				-- AmpTime 6
				when others 	=> Peaking_Time 	<= Peak_Time( 143 downto 132); 				-- AmpTime 7
							end case;
			Peak_Time_Mux 	<= Peaking_Time;
			Adr_Diff		<= ( '0' & Peaking_Time( 11 downto 1 )) - 3;		-- PT/2

			wadr			<= wadr + 1;
			radr			<= wadr - Adr_Diff( Adr_Width-1 downto 0 );
			
			if( WE_FIR_MR = '1' ) 
				then AD_CLR		<= '1';
			elsif( conv_integer( TimeConst ) /= TimeConstReg )
				then AD_CLR		<= '1';
			elsif( AD_CLR_Tc = '1' )
				then AD_CLR		<= '0';
			end if;
			
			if( AD_CLR = '0' )
				then AD_CLR_Cnt <= 0;
			elsif( AD_Clr_Tc = '0' )
				then AD_CLR_Cnt <= AD_CLR_Cnt + 1;
			end if;			
			
			-- Changed 10/1/09
			Fir_Reset <= Ad_Clr_tc;
			
			-- When Clearing the Filters due to a change in AmpTIme
			-- 	Hold the input off, so when the clear is removed, 
			--	the filters track properly			
			if( AD_Clr = '1' ) 
				then dly_data( 17 downto 0 ) <= (others=>'0');
			elsif( PA_Reset = '1' )
				then dly_data( 17 downto 0 ) <= conv_Std_logic_Vector( -40000, 18 );
				else dly_data( 17 downto 0 ) <= adata;			-- Already signed so no conversion necessary
			end if;
			Dout			<= Dly_Data;									
		end if;
	end process;
	--------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------
end behavioral;               -- Main_RAM
-------------------------------------------------------------------------------------------