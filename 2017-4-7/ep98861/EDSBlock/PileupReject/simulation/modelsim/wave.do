onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix hexadecimal /tb/clk
add wave -noupdate -format Logic -radix hexadecimal /tb/ms100_Tc
add wave -noupdate -format Logic -radix hexadecimal /tb/inc_cps
add wave -noupdate -format logic -radix hexadecimal /tb/u/acc_en/q
add wave -noupdate -format logic -radix hexadecimal /tb/u/cnt/q
add wave -noupdate -format Logic -radix hexadecimal /tb/cps
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {179785364 ps} 0}
configure wave -namecolwidth 120
configure wave -valuecolwidth 53
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {80282265 ps}
