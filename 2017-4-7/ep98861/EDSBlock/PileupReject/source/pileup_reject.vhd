-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_psteer.VHD
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:
-- 		Super High	150 ns Peak Time
-- 		High 		300 ns Peak TIme
--		Medium 		600 ns PEak Time
--		Low 	  	    3000 ns Peak Time

--		This module accepts as input each of the Disc Edge Detects
--		and Generates the blocking pulses
--			Index	Discriminator	Block WIdth 	Block Len
--			  0		Super High    	N/A
--			  1		High			  800ns
--			  2	 	Med			  2 us
--			  3	 	Low		 	  16us
--			  4	 	Energy	  	 0.8 to 204.8us
--			
--	It also generates the FDISC ( First Discriminator Output Pulse )
--		It forms the Logical AND of the Discriminator Edge Detect and
--		the Blocking Pulse to generate a single-wide Fast Discriminator Pulse
--	History
--		December 28, 2011
--			Gap Time increased from 6 bits to 89 bits, 
--			ATCW_GapTime bits changed positions, from 5;0 to 23:16
--			Also effects EDSBlock and Main_Channel and Main_GT_Ram
--		07/14/11 - Rev 1.0 - MCS
--			Took out-of-range out of BLR_BUSY
--		06/10/11 Rev 0.1 - MCS
--			Chagned Out-of-Range Timeout to be a multiple of OTR_FACTOR
--		06/08/11 Rev 0.2 - MCS
--			Changed Out-of-Range timeout from 4x to 8x Amp Time
--		06/08/11 Rev 0.1 - MCS
--			Changed Out-of-Range timeout from 2x to 4x Amp Time
--		06/02/11 Rev 1,1- MCS
--			Added "Out-Of_Range" Timeout (at 2x Amp Time) that keeps:
--				BLR_Busy active
--				Keeps PUR active for the same time
--		05/02/11 - MCS  
--			PA_Busy_Cnt_max( iDisc_Main) changed from "Shape_Time" to "Shape_Time + 2x Peak Time" to match that of the Dpp3
--		06/23/08 - MCS - Ver 720F+
--			Changed ( PEAK_DONE_TC = '1' ) and ( PUR = '0' ) to just ( PEAK_DONE_TC = '1' )
--		03/17/08 - MCS - Ver 62b4
--			Super High dropped from 0.2us to 0.1us
-- 		01/30/08 Ver 5x01 - module: Pileup_Reject
--			SHigh Disc Start Always qualified with med Compare level
--			High Disc Start Alwauys qualified with Med Compare level
-- 	   			additionally disc_cmp_e_vec(DE_MAX) removed from High Disc Start
--		01/29/08 - MCS - Ver 5x00
--			Complied under Quartus 7.2 SP1
--		08/27/07 - MCS - Ver 4x34
--			Module: Pileup_Reject: Pur_Time was replaced with PEAK_TIME_D4
--			Module: Pileup_Reject: Added RESET_CNT which is 16-bits to accomodate 2x max Peak_Time ( was only 12 bits which was not enough )
--			Module: Pileup_Reject: Some general clean-up
--		08/13/07 - MCS - Ver 4x33
--			Force PUR active when state RESET0, RESET1 & RESET2
--		07/16/07 - MCS - Ver 4x31
--			PA_BUSY getting stuck due to EFT test for CE Testing.
--			Modle: Pileup_Reject type declaration changed to constants
--			during the test, state machine would not know where to go, so it would get lost.
--		06/15/05 - MCS
--			Updated for QuartusII Ver 5.0 SP 0.21
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;

library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

-------------------------------------------------------------------------------
entity pileup_reject is 	
	port( 
		CLK100         	: in      std_logic;			     	-- Master Clock Input
		AVG_ENABLE		: in		std_Logic;
		PA_Reset			: in		std_logic;					-- PreAmp Reset
		BLevel_En			: in		std_logic;
		Disc_En			: in		std_logic_Vector(  5 downto 0 );
		Disc_In        	: in		std_logic_Vector(  4 downto 0 );	-- Discriminator Inputs
		Disc_Level		: in		std_logic_Vector(  4 downto 0 );
		Gap_Time			: in		std_logic_vector(  7 downto 0 );	-- Gap Time
		Peak_Time			: in		std_logic_Vector( 59 downto 0 );	-- Peak time
		TimeConst			: in		std_logic_Vector(  2 downto 0 );
		Clip_Min			: in		std_logic_vector( 11 downto 0 );
		Clip_Max			: in		std_logic_vector( 11 downto 0 );
		Main_Out			: in		std_logic_Vector( 19 downto 0 );
		OTR_FACTOR		: in		std_logic_Vector( 31 downto 0 );
		RTime_Rej			: in		std_logic;
		BH_LTIME_EN		: buffer	std_logic;
		FDISC_BB			: buffer	std_Logic;					-- Fast Disc
		FDisc_CPS			: buffer	std_logic;
		Inc_NPS			: buffer	std_logic;
		BLR_BUSY			: buffer	std_logic := '0';
		PA_Busy			: buffer	std_logic_Vector( 5 downto 0 );
		Blevel_Upd		: buffer	std_logic;
		CPeak			: buffer	std_logic_vector( 11 downto 0 );
		Meas_Done			: buffer	std_logic;
		Out_Of_Range_Cen	: buffer	std_logic );
end pileup_reject;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture behavioral of pileup_reject is

	-- Constant Declarations -------------------------------------------------
	constant iDisc_40 	: integer := 0;
	constant iDisc_160 	: integer := 1;
	constant iDisc_320 	: integer := 2;
	constant iDisc_600 	: integer := 3;
	constant iDisc_Main	: integer := 4;
	constant iDisc_Pur	: integer := 5;

	constant Cmp_Clip_Min 		: integer := 0;
	constant Cmp_Clip_Max 		: integer := 1;			
	constant Cmp_Positive 		: integer := 2;
	constant Cmp_In_Range 		: integer := 3;
	constant Cmp_Out_Of_Range	: integer := 4;
	
	constant BH_LT_CNTR_MAX 		: integer := 127;		-- 1.2% 63;	-- 15;
	constant BH_LT_CNTR_PRESET	: integer := 64;		-- 1.2% 32;	-- 8;
	
	constant CLK_PER			: integer := 10;

--	constant Clock_Uncertainty 	: integer := 25; 	-- Additional Clock Periods for uncertainty
	constant Clock_Uncertainty 	: integer := 8; 	-- Additional Clock Periods for uncertainty
	constant iCntr_Max			: integer := 16383;
	
	constant FDISC_CPS_MAX		: integer := 79;
	constant Max_Clr_Time_Const 	: integer := 2;
	constant blevel_cnt_max 		: integer := 50000 / CLK_PER;		-- 50 usec update rate
	
--	blevel_Cnt_Tc 		<= '1' when ( blevel_Cnt 	= Peak_Time_Reg( 59 downto 48 )) else '0';
--	blevel_Cnt_Tc 		<= '1' when ( blevel_Cnt 	= conv_integer( Peak_Time_Reg( 59 downto 48+2 ))) else '0';

	-- Constant Declarations -------------------------------------------------

	-- Type Declarations -----------------------------------------------------
	type Reject_Cnt_Type 		is array( iDisc_Main downto iDisc_40 ) of integer range 0 to iCntr_Max;
	type Blk_Cnt_Type 			is array( iDisc_Main downto iDisc_40 ) of integer range 0 to iCntr_Max;
	type Busy_Cnt_Type 			is array( iDisc_Main downto iDisc_40 ) of integer range 0 to iCntr_Max;
--	type Busy_Cnt_Type 			is array( iDisc_Pur downto iDisc_40 ) of integer range 0 to iCntr_Max;
	-- Type Declarations -----------------------------------------------------
		
	-- Signal Declarations ---------------------------------------------------
	signal Avg_Time_Start		: integer range 0 to iCntr_Max := 0;
	signal Avg_Time_End			: integer range 0 to iCntr_Max := 0;
	signal Avg_Time_En			: std_logic;
	signal Avg_Time_Clr			: std_logic;
	signal Avg_Main_Sum			: std_logic_vector( 19+4 downto 0 );
	signal Avg_Main 			: std_logic_vector( 19+4 downto 0 );
	signal Avg_Time			: integer range 0 to 511;
	signal Avg_En				: std_logic;
	signal Avg_Done			: std_logic;
	
	
	signal BH_lt_inc_en			: std_logic := '0';
	signal BH_lt_tmr_en			: std_logic := '0';
	signal BH_lt_cntr			: integer range 0 to BH_LT_CNTR_MAX := 0;

	signal Blk_Cnt_Max			: Blk_Cnt_Type;
	signal Blk_Cnt				: Blk_Cnt_Type;
	signal Blk_ST				: std_logic_Vector( iDisc_Main downto iDisc_40 ) := (others=>'0');
	signal Blevel_Cnt			: integer range 0 to Blevel_Cnt_max;
	signal Blevel_Cnt_Tc		: std_logic := '0';
	signal BLR_BUSY_Cnt_Max		: integer range 0 to iCntr_Max := 0;
	signal BLR_BUSY_Cnt			: integer range 0 to iCntr_Max := 0;
	signal BLOCK_OUT			: std_logic_Vector( iDisc_Main downto iDisc_40 ):= (others=>'0');
		
	signal FDisc_Comb			: std_logic := '0';
	signal FDisc_Vec			: std_logic_Vector( FDISC_CPS_MAX downto 0 ):= (others=>'0');

	signal Gap_Time_Reg			: integer range 0 to 255;

	signal Int_Cmp_Vec 			: std_logic_Vector( 4 downto 0 ):= (others=>'0');
	
	signal Main_Out_Del			: std_logic_Vector( 15 downto 0 );
	signal Main_Out_PSlope		: std_logic;
	signal max_fir_cmp			: std_logic;
	signal Max_Clr_Cnt			: integer range 0 to iCntr_Max := 0;
	signal Max_Clr_PreReset_Max	: integer range 0 to iCntr_Max := 0;
	signal Max_Clr_En			: std_logic := '0';
	signal Max_Fir				: std_logic_Vector( 15 downto 0 );
	
--	signal Out_Of_Range_Cen		: std_logic;
	signal Out_Of_Range_Cnt_Max	: integer range 0 to iCntr_Max := 0;
	signal Out_Of_Range_Cnt		: integer range 0 to iCntr_Max := 0;
	signal OTR_MULT_PRODUCT		: std_logic_vector(43 downto 0 );
	
	signal PA_Reset_Del 		: stD_logic := '0';
	signal PA_Busy_Del			: stD_logic := '0';
	signal PA_Busy_Cnt			: Busy_Cnt_Type;
	signal PA_Busy_Cnt_Max		: Busy_Cnt_Type;
	signal Peak_Done_Max		: integer range 0 to iCntr_Max := 0;
	signal Peak_Time_D2			: integer range 0 to iCntr_Max:= 0;
	signal Peak_Time_D4			: integer range 0 to iCntr_Max:= 0;
	signal Pur_Str_En 			: std_Logic:= '0';
	signal Pur_Str_Cnt			: integer range 0 to iCntr_Max:= 0;
	signal Pur_Str_Cnt_Max 		: integer range 0 to iCntr_Max:= 0;
	signal PSteer_Cnt_En 		: std_logic_Vector( iDisc_Main downto iDisc_40 ):= (others=>'0');
	signal PSteer_Cnt 			: Reject_Cnt_Type;
	signal Pur_Det				: std_logic:= '0';
	signal Peak_Done			: std_logic:= '0';
	signal Peak_PreReset		: std_logic:= '0';
	signal Peak_Time_Reg		: std_Logic_vector( 59 downto 0 ):= (others=>'0');

	signal RTime_Busy 			: std_logic;
	signal RTime_Busy_Cnt 		: integer range 0 to iCntr_Max;
	signal RTime_Busy_Cnt_Max 	: integer range 0 to iCntr_Max;
	
	signal Shape_Time 			: integer range 0 to iCntr_Max;
	signal SBlock_Out			: std_logic_Vector( iDisc_Main downto iDisc_40 ):= (others=>'0');
	signal SBlock_Cnt			: Blk_Cnt_Type;

	signal Pur				: std_logic:= '0';					-- Pile-Up Reject
	signal BLM_PBUsy			: std_logic:= '0';

	----------------------------------------------------------------------------------------------
	component cpscntr 
	     port( 
			CLK		    	: in      std_logic;                         -- Master Clock
			ms100_tc		: in		std_Logic;					-- 100ms Timer
			inc_cps		: in		std_logic;					-- FDISC or PEAK_DONE
			cps			: buffer	std_logic_Vector( 31 downto 0 ));
	end component;		-- cpscntr;
	----------------------------------------------------------------------------------------------
	
	----------------------------------------------------------------------------------------------
	component OTR_MULT
		port( 
			dataa		: IN STD_LOGIC_VECTOR (11 DOWNTO 0);
			datab		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
			result		: OUT STD_LOGIC_VECTOR (43 DOWNTO 0 ) );
	end component;	
begin
	----------------------------------------------------------------------------------------------
	fir_pslope_compare : lpm_compare
		generic map(
			lpm_width			=> 16,
			lpm_representation	=> "SIGNED" )
		port map(
			dataa			=> Main_Out( 15 downto 0 ),
			datab			=> Main_Out_del,			
			agb				=> Main_Out_Pslope );
			
	-- Check FIR_OUT against MAX_FIR, if >=, need to update max_fir
	fir_max_cmp : lpm_compare
		generic map(
			lpm_width			=> 16,
			lpm_representation	=> "SIGNED" )
		port map(
			dataa			=> Main_Out( 15 downto 0 ), 
			datab			=> max_fir,			
			ageb				=> max_fir_cmp );
			
	Zero_Max_Cmp : lpm_compare
		generic map(
			LPM_WIDTH			=> 20, 
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			dataa			=> Main_Out, 
			datab			=> (others=>'0'),
			agb				=> Int_Cmp_Vec(Cmp_Positive) );		

	Over_Max_Cmp : lpm_compare
		generic map(
			LPM_WIDTH			=> 20,
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			dataa			=> Main_Out,
			datab			=> conv_Std_logic_Vector( 4000, 20 ),
			alb				=> Int_Cmp_Vec(Cmp_In_Range),
			ageb				=> Int_Cmp_Vec(Cmp_Out_Of_Range) );
			
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Compare Converted peak to Min/Max Clip Value
	Clip_Min_Cmp : lpm_compare
		generic map(
			LPM_WIDTH			=> 16, 
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			dataa			=> Max_Fir,
			datab			=> ext( '0' & CLIP_MIN, 16 ),
			ageb				=> Int_Cmp_Vec(Cmp_Clip_Min) );		-- Needs to be >= to include CLIP_MIN

	Clip_Max_Cmp : lpm_compare
		generic map(
			LPM_WIDTH			=> 16, 
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			dataa			=> Max_Fir,
			datab			=> ext( '0' & Clip_Max, 16 ),
			aleb				=> Int_Cmp_Vec(Cmp_Clip_Max) );		-- Needs to be <= to include CLIP_MAX
			
	-- Compare Converted peak to Min/Max Clip Value
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Accumulator for Main_Out per the "Number of Averages"	
	Avg_Time_Sum_Inst : lpm_add_Sub
		generic map(
			LPM_WIDTH	=> 24,
			LPM_REPRESENTATION 	=> "SIGNED",
			LPM_DIRECTION		=> "ADD" )
		port map(
			cin				=> '0',
			dataa			=> sxt( Main_Out, 24 ),
			datab			=> Avg_Main,
			result			=> Avg_Main_Sum );						
	----------------------------------------------------------------------------------------------
	Cpeak_Proc : process( clk100 )
	begin
		if( rising_Edge( clk100 )) then
			------------------------------------------------------------------------------------
			-- Fir Max Clear Logic 0.3125 X Peak Time after BLM Disc Input
			if( Peak_Done = '1' )
				then Max_Clr_En <= '0';
			elsif( Disc_In( iDisc_Main ) = '1' )
				then Max_Clr_En <= '1';
			end if;
			
			if(( Max_Clr_En = '0' ) or ( Disc_In( iDisc_Main ) = '1' ))
				then Max_Clr_Cnt <= 0;
			elsif( Max_Clr_En = '1' ) 
				then Max_Clr_Cnt <= Max_Clr_Cnt + 1;
			end if;
			
			if( Max_Clr_Cnt = Max_Clr_PreReset_Max )
				then Peak_PreReset <= '1';
				else Peak_PreReset <= '0';
			end if;
			
			if(( Avg_En = '1' ) and ( Max_Clr_Cnt = Avg_Time_End ))
				then Avg_Done <= '1';
				else Avg_Done <= '0';
			end if;
			
			if(( Avg_En = '0' ) and ( Max_Clr_Cnt = Peak_Done_Max ))
				then Peak_Done <= '1';
			elsif(( Avg_En = '1' ) and ( Avg_Done = '1' ))
				then PEak_Done <= '1';
				else Peak_Done <= '0';
			end if;

			-- Logic to "average" peak instead of peak-detect ---------------------------------------------------------------------------
			-- Avg_Time Starts some time before PEak_Done and ends with Peak_Done			
			if( Max_Clr_Cnt = Avg_Time_Start )
				then Avg_Time_Clr <= '1';
				else Avg_Time_Clr <= '0';
			end if;
				
			if( Avg_En = '0' )
				then Avg_Time_En <= '0';
			elsif( Max_Clr_Cnt >= Avg_Time_End )
				then Avg_Time_En <= '0';
			elsif( Max_Clr_Cnt = Avg_Time_Start )
				then Avg_Time_En <= '1';
			end if;						
			
			if( Max_Clr_Cnt = Avg_Time_Start )
				then Avg_Main <= (others=>'0');
			elsif( Avg_Time_En = '1' )			
				then Avg_Main <= Avg_Main_Sum;
			end if;
			-- Logic to "average" peak instead of peak-detect ---------------------------------------------------------------------------
			------------------------------------------------------------------------------------

			------------------------------------------------------------------------------------
			-- When Peak is Done - Store Max Detected Value 	
			if( Avg_En= '0' ) then		
				if(( Pur 						= '1' ) 
					or ( Peak_PreReset 			= '1'  ) 
					or ( Meas_Done 			= '1' )) -- Reject_Done = '1' )) 
						then Max_Fir <= (others=>'0');
				elsif(( Main_Out_PSlope = '1' ) 
					and ( Max_Fir_Cmp = '1' )
					and ( Int_Cmp_Vec( Cmp_Positive ) = '1' ) 
					and ( Int_Cmp_Vec( Cmp_In_Range ) = '1' ))
						then Max_Fir <= Main_Out( 15 downto 0 );
				end if;							
			else 
				if( Pur = '1' )
					then Max_Fir <= (others=>'0');
				elsif( Avg_Done = '1' ) then
					case Avg_Time is
						when 256 		=> Max_Fir <= Avg_Main( 15+8 downto 8 );
						when 128 		=> Max_Fir <= Avg_Main( 15+7 downto 7 );
						when 64 		=> Max_Fir <= Avg_Main( 15+6 downto 6 );
						when 32 		=> Max_Fir <= Avg_Main( 15+5 downto 5);
						when 16 		=> Max_Fir <= Avg_Main( 15+4 downto 4 );
						when others 	=> Max_Fir <= Avg_Main( 15+4 downto 4 );
					end case;
				end if;
			end if;
				
			if( Peak_Done = '1' ) then 
				Cpeak <= Max_Fir( 11 downto 0 );
				if(( Pur = '0' ) 
--					and ( Int_Cmp_Vec(Cmp_Positive) = '1' )
--					and ( Int_Cmp_Vec(Cmp_In_Range) = '1' )
					and ( Int_Cmp_Vec(Cmp_Clip_Min) = '1' )
					and ( Int_Cmp_Vec(Cmp_Clip_Max) = '1' )) 
						then Meas_Done 	<= '1';
						else Meas_Done 	<= '0';
				end if;
			else
				Meas_Done <= '0';				
			end if;
			-- When Peak is Done - Store Max Detected Value 
			-------------------------------------------------------------------------------		
		
			-------------------------------------------------------------------------------									
			-- When to count event in the output count rate -------------------------------
			-- When Measured value >= 0 AND < 4000
			-- Stretch Inc_NPS to 2- clocks wide (used at 50Mhz)			
			if(( Pur = '0' )
				and ( Peak_Done = '1' ) 
				and ( Int_Cmp_Vec(Cmp_Positive) 	= '1' )
				and ( Int_Cmp_Vec(Cmp_In_Range) 	= '1' )
				and ( PA_Reset 				= '0' )
				and ( PA_Busy( iDisc_Main )      	= '0' ))
					then Inc_NPS <= '1';
					else Inc_NPS <= '0';
			end if;
			-- When to count event in the output count rate -------------------------------
			-------------------------------------------------------------------------------		
			
			------------------------------------------------------------------------------------
			-- Fir Max Clear Logic 0.3125 X Peak Time after BLM Disc Input						
			Main_Out_Del	<= Main_Out( 15 downto 0 );			
			------------------------------------------------------------------------------------
			
		end if;
	end process;			
	----------------------------------------------------------------------------------------------
	
	----------------------------------------------------------------------------------------------
	-- Discriminator Blocking Pulse Lengths
	-- From the Discriminator Input until the next longer discriminator fires
	Blk_Cnt_Max(iDisc_40)		<= Clock_Uncertainty;
	Blk_Cnt_Max(iDisc_160)		<= Clock_Uncertainty + 4 + conv_integer( Peak_Time_Reg( 23 downto 12 ) );
	Blk_Cnt_Max(iDisc_320)		<= Clock_Uncertainty + 4 + conv_integer( Peak_Time_Reg( 35 downto 24 ) );
	Blk_Cnt_Max(iDisc_600)		<= Clock_Uncertainty + 4 + conv_integer( Peak_Time_Reg( 47 downto 36 ) );
	Blk_Cnt_Max(iDisc_Main)		<= 2* Clock_Uncertainty + 4 + conv_integer( Peak_Time_Reg( 59 downto 48+1 )); 		-- 1/2 Peak Time
	-- Discriminator Blocking Pulse Lengths
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Peak Time Related Parameters
	Peak_Time_D2				<= conv_integer( Peak_Time_Reg( 59 downto 48+1 ) );		-- 1/2 Peak Time
	Peak_Time_D4				<= conv_integer( Peak_Time_Reg( 59 downto 48+2 ) );		-- 1/4 Peak Time
	Shape_Time 				<= conv_integer( Peak_Time_Reg( 59 downto 48   ) ) + Gap_Time_Reg;

	-- Amount of time to stretech the PUR signal a detection has occured
	Pur_Str_Cnt_Max			<= conv_integer( Peak_Time_Reg( 59 downto 48 ) & '0' ) + Gap_Time_Reg;

	-- Clear MAX_FIR ( 1st Time )
	Max_Clr_PreReset_Max		<= Peak_Time_D4;	-- 1/4 Peak Time

	-- Peak Done TIme is from the BLM Discriminator Input to Peak Done
	Peak_Done_Max			    	<= Peak_Time_D2 + Gap_Time_Reg + 2;
--	Peak_Done_Max			    	<= Shape_Time;	-- 12/14/11


	Avg_Time_Start				<= Peak_Time_D2 -15;			-- When to start averaging wrt time of main disc
	Avg_Time_End				<= Avg_Time_Start + Avg_Time;

	
	-- BLM Busy goes active at BLM Disc Input and stays active til peak goes below baseline
	-- THIS GENERATES BLR_BUSY which is responsible for the trailing edge of the event
	BLR_BUSY_Cnt_Max 			<= conv_integer( Peak_Time_Reg( 59 downto 48) & '0' ) + Gap_Time_Reg;		-- 2x Peak Time + Gap Time
	
	-- PreAmp Busy Times for Preamp resets which is 2x the Peaking Time
	PA_Busy_Cnt_Max( iDisc_40   )	<= Clock_Uncertainty + conv_integer( Peak_Time_Reg( 11 downto  0 ) & '0' );
	PA_Busy_Cnt_Max( iDisc_160  )	<= Clock_Uncertainty + conv_integer( Peak_Time_Reg( 23 downto 12 ) & '0' );
	PA_Busy_Cnt_Max( iDisc_320  )	<= Clock_Uncertainty + conv_integer( Peak_Time_Reg( 35 downto 24 ) & '0' );
	PA_Busy_Cnt_Max( iDisc_600  )	<= Clock_Uncertainty + conv_integer( Peak_Time_Reg( 47 downto 36 ) & '0' );
	PA_Busy_Cnt_Max( iDisc_Main )	<= Shape_Time + conv_integer( Peak_Time_Reg( 59 downto 48 ) & '0' );		-- 3x Peak Time + Gap Time same as Dpp3
--	PA_Busy_Cnt_Max( iDisc_Main )	<= Shape_Time + conv_integer( Peak_Time_Reg( 59 downto 48 ) & "00" );		-- Increased to 5x Gaptime -- 12/14/11
	
	OTR_MULT_INST : OTR_MULT
		port map(
			dataa			=> Peak_Time_Reg( 59 downto 48 ),
			datab			=> OTR_FACTOR,
			result			=> OTR_MULT_PRODUCT );			
			
	Out_Of_Range_Cnt_Max 	<= conv_Integer( OTR_MULT_PRODUCT( 13+16 downto 16 ));
	RTime_Busy_Cnt_Max 		<= Shape_Time + conv_integer( Peak_Time_Reg( 59 downto 48 ));



--	Out_Of_Range_Cnt_Max		<= conv_integer( Peak_Time_Reg( 59 downto 48 ) & "000" );				-- 8x Peak Time		
	--	Out_Of_Range_Cnt_Max		<= conv_integer( Peak_Time_Reg( 59 downto 48 ) & "00" );				-- 4x Peak Time
--	Out_Of_Range_Cnt_Max		<= conv_integer( Peak_Time_Reg( 59 downto 48 ) & '0' );				-- 2x Peak Time
	--	PA_Busy_Cnt_Max( iDisc_Main )	<= Shape_Time;													-- Peak Time + Gap Time	

--	PA_Busy_Cnt_Max( iDisc_Main )	<= conv_integer( Peak_Time_Reg( 59 downto 48 ) & "0" );			-- 2x Peak Time
--	PA_Busy_Cnt_Max( iDisc_Pur  ) <= conv_integer( Peak_Time_Reg( 59 downto 48 ) & "00" );			-- 4x Peak Time
	-- Peak Time Related Parameters
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Fast Discriminator ( Combinatorial )
	FDisc_Comb	<= '1' when (( Disc_In(iDisc_40   ) = '1' ) and ( SBlock_Out(iDisc_40   ) = '0' ) and ( Block_Out(iDisc_40    ) = '0' )) else
				   '1' when (( Disc_In(iDisc_160  ) = '1' ) and ( SBlock_Out(iDisc_160  ) = '0' ) and ( Block_Out(iDisc_160   ) = '0' )) else 
				   '1' when (( Disc_In(iDisc_320  ) = '1' ) and ( SBlock_Out(iDisc_320  ) = '0' ) and ( Block_Out(iDisc_320   ) = '0' )) else 
				   '1' when (( Disc_In(iDisc_600  ) = '1' ) and ( SBlock_Out(iDisc_600  ) = '0' ) and ( Block_Out(iDisc_600   ) = '0' )) else
				   '1' when (( Disc_In(iDisc_Main ) = '1' ) and ( SBlock_Out(iDisc_Main ) = '0' ) and ( Block_Out(iDisc_Main  ) = '0' )) else
				   '0';

	-- Pile-Up Reject Detection
	Pur_Det 	<= 	'1' when ((Disc_In( iDisc_40  )='1') and (SBlock_Out(iDisc_40  )='0') and (Block_Out(iDisc_40  )= '0') and (PSteer_Cnt_En(iDisc_40  )='1')) else
				'1' when ((Disc_In( iDisc_160 )='1') and (SBlock_Out(iDisc_160 )='0') and (Block_Out(iDisc_160 )= '0') and (PSteer_Cnt_En(iDisc_160 )='1')) else
				'1' when ((Disc_In( iDisc_320 )='1') and (SBlock_Out(iDisc_320 )='0') and (Block_Out(iDisc_320 )= '0') and (PSteer_Cnt_En(iDisc_320 )='1')) else
				'1' when ((Disc_In( iDisc_600 )='1') and (SBlock_Out(iDisc_600 )='0') and (Block_Out(iDisc_600 )= '0') and (PSteer_Cnt_En(iDisc_600 )='1')) else
				'1' when ((Disc_In( iDisc_Main)='1') and (SBlock_Out(iDisc_Main)='0') and (Block_Out(iDisc_Main)= '0') and (PSteer_Cnt_En(iDisc_Main)='1')) else
				'0';
			 
	-- Pile-Up Reject Signal
	Pur	<= '1' when ( PA_Reset 	= '1' ) else
		   '1' when ( Pur_Det  	= '1' ) else
		   '1' when ( Pur_Str_En = '1' ) else
		   '1' when ( PA_Busy( iDisc_Main ) = '1' ) else
		   '1' when ( Out_Of_Range_Cen = '1' ) else
		   '1' when ( RTime_Busy = '1' ) else
--		   '1' when ( PA_Busy( iDisc_Pur ) = '1' ) else 
		   '0';
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Block Starting Pulse based on the Discriminator Edge Detects ---------
	BLK_ST(iDisc_40 )		<= '0';
	
	BLK_ST(iDisc_160)		<= '1' when (( Disc_In(iDisc_40) = '1' ) and ( Block_Out(iDisc_40) = '0' )) else
						   '0';

	BLK_ST(iDIsc_320)		<= '1' when (( Disc_In(iDisc_40  ) = '1' ) and ( Block_Out(iDisc_40  ) = '0' )) else
						   '1' when (( Disc_In(iDisc_160 ) = '1' ) and ( Block_Out(iDisc_160 ) = '0' )) else 
						   '0';

	BLK_ST(iDIsc_600)		<= '1' when (( Disc_In(iDisc_40  ) = '1' ) and ( Block_Out(iDisc_40  ) = '0' )) else
						   '1' when (( Disc_In(iDisc_160 ) = '1' ) and ( Block_Out(iDisc_160 ) = '0' )) else 
						   '1' when (( Disc_In(iDisc_320 ) = '1' ) and ( Block_Out(iDisc_320 ) = '0' )) else 
						   '0';


	BLK_ST(iDisc_Main)		<= '1' when (( Disc_In(iDisc_40   ) = '1' ) and ( Block_Out(iDisc_40   ) = '0' )) else
						   '1' when (( Disc_In(iDisc_160  ) = '1' ) and ( Block_Out(iDisc_160  ) = '0' )) else 
						   '1' when (( Disc_In(iDisc_320  ) = '1' ) and ( Block_Out(iDisc_320  ) = '0' )) else 
						   '1' when (( Disc_In(iDisc_600  ) = '1' ) and ( Block_Out(iDisc_600  ) = '0' )) else 
						   '0';							
	-- Block Starting Pulse based on the Discriminator Edge Detects ---------
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
     clock_proc : process( clk100 )
     begin
		if(( CLK100'event ) and ( CLK100 = '1' )) then
			Peak_Time_Reg	<= Peak_time;
			Gap_Time_Reg	<= Conv_Integer( Gap_Time );		-- two clock minimum sample size
			
			if( Gap_Time_Reg > 256 )
				then Avg_Time <= 256;
			elsif( Gap_Time_Reg > 128 )
				then Avg_Time <= 128;
			elsif( Gap_Time_Reg > 64 )
				then Avg_Time <= 64;
			elsif( Gap_Time_REg > 32 )
				then Avg_Time <= 32;
			elsif( Gap_Time_Reg > 16 )
				then Avg_Time <= 16;
				else Avg_Time <= 1;
			end if;
						
			if(( Avg_Enable = '1' ) and ( Gap_Time_Reg > 16 ))
				then Avg_En <= '1';
				else Avg_En <= '0';
			end if;
				
			------------------------------------------------------------------------------------
			if( RTime_Rej = '1' )
				then RTime_Busy <= '1';
			elsif( RTime_Busy_Cnt >= RTIme_Busy_Cnt_Max )
				then RTime_Busy <= '0';
			end if;
			
			if(( RTime_Busy = '0' ) or ( RTime_Rej = '1' ))
				then RTime_Busy_Cnt <= 0;
			elsif( RTime_Busy = '1' )
				then RTime_Busy_Cnt <= RTime_Busy_Cnt + 1;
			end if;
			------------------------------------------------------------------------------------
			
			------------------------------------------------------------------------------------
			-- Blocking Pulse Counters ---------------------------------------------------------
			Block_Out( iDisc_40 ) <= '0';
			for i in iDisc_160 to iDisc_Main loop
				-- High Speed Blocking
				if( Disc_En(i) = '0' )
					then Block_Out(i) <= '0';
				elsif( BLK_ST(i) = '1' )
					then Block_Out(i) <= '1';
				elsif(  Blk_Cnt(i) = Blk_Cnt_Max(i) )
					then Block_Out(i) <= '0';
				elsif( Disc_In(i) = '1' )
					then Block_Out(i) <= '0';
				end if;

				if(( Disc_En(i) = '0' ) or ( Block_Out(i) = '0' ) or ( BLK_ST(i) = '1' ))		-- 4x33 was PA_Reset
					then Blk_Cnt(i) <= 0;
				elsif( Block_Out(i) = '1' )
					then Blk_Cnt(i) <= Blk_Cnt(i) + 1; 
				end if;	
			end loop;
			-- Blocking Pulse Counters ---------------------------------------------------------
			------------------------------------------------------------------------------------	
			
			------------------------------------------------------------------------------------
			-- Blocking Same Disc Input --------------------------------------------------------
			for i in iDisc_40 to iDisc_Main loop
				if( Disc_En(i) = '0' )
					then SBlock_Out(i) <= '0';
				elsif( Disc_In(i) = '1' )
					then SBlock_Out(i) <= '1';
				elsif( SBlock_Cnt(i) = Peak_Time_Reg( (12*i)+11 downto 12*i))			-- Block with Disc Peak Time
					then SBlock_Out(i) <= '0';
				end if;
				
				if(( Disc_En(i) = '0' ) or ( Disc_In(i) = '1' ))
					then SBlock_Cnt(i) <= 0;
				elsif( SBlocK_Out(i) = '1' )
					then SBlock_Cnt(i) <= SBlock_Cnt(i) + 1;
				end if;				
			end loop;
			-- Blocking Same Disc Input --------------------------------------------------------
			------------------------------------------------------------------------------------

			------------------------------------------------------------------------------------
			-- Super High to Energy  Reject Enable and Reject Counter
			for i in iDisc_40 to iDisc_Main loop			
				-------------------------------------------------------------------------------
				-- All Pulse Steering Count Enables
				if(( Disc_En(i) = '1' ) and ( FDisc_Comb = '1' ))
					then PSteer_Cnt_En(i) <= '1';
				elsif( PSteer_Cnt( i ) = 0 )
					then PSteer_Cnt_En(i) <= '0';
				end if;
				-- All Pulse Steering Count Enables
				-------------------------------------------------------------------------------
			end loop;
			-- Super High Reject
			------------------------------------------------------------------------------------
			

			----------------------------------------------------------------------------------------------
			-- 40ns Discriminator ------------------------------------------------------------------
			-- 40ns to ...
			if( Disc_In( iDisc_40 ) = '1' )
				then PSteer_Cnt( iDisc_40 ) <= Shape_Time;
			
			-- 160ns to 40ns 
			elsif(( Disc_In( iDisc_160) = '1' ) and ( Block_Out( iDisc_160 ) = '0' ))
				then PSteer_Cnt( iDisc_40 ) <= Shape_Time - ( conv_integer( Peak_Time_Reg( 23 downto 12 )) - conv_integer( Peak_Time_Reg( 11 downto 0 )) );
			
			-- 320ns to 40ns
			elsif(( Disc_In( iDIsc_320) = '1' ) and ( Block_Out( iDIsc_320 ) = '0' ))
				then PSteer_Cnt( iDisc_40 ) <= Shape_Time - ( conv_integer( Peak_Time_Reg( 35 downto 24 )) - conv_integer( Peak_Time_Reg( 11 downto 0 )) );

			-- 600ns to 40ns
			elsif(( Disc_In( iDIsc_600) = '1' ) and ( Block_Out( iDIsc_600 ) = '0' ))
				then PSteer_Cnt( iDisc_40 ) <= Shape_Time - ( conv_integer( Peak_Time_Reg( 47 downto 36 )) - conv_integer( Peak_Time_Reg( 11 downto 0 )) );

			-- BLM to SHigh
			elsif(( Disc_In( iDisc_Main ) = '1' ) and ( Block_Out( iDisc_Main ) = '0' ))
				then PSteer_Cnt( iDisc_40 ) <= Shape_Time - ( Peak_Time_D2 - conv_integer( Peak_Time_Reg( 11 downto 0 )) );
			
			-- Now just count down
			elsif(( PSteer_Cnt_En( iDisc_40 ) = '1' ) and ( PSteer_Cnt( iDisc_40 ) > 0 ))
				then PSteer_Cnt( iDisc_40 ) <= PSteer_Cnt( iDisc_40 ) - 1;
			end if;
			-- 40ns Discriminator ------------------------------------------------------------------

			-- 160ns Discriminator -----------------------------------------------------------------
			-- 40ns to 160ns
			if(( Disc_In( iDisc_40 ) = '1' ))
				then PSteer_Cnt( iDisc_160 ) <= Shape_Time + ( conv_integer( Peak_Time_Reg( 23 downto 12 )) - conv_integer( Peak_Time_Reg( 11 downto 0 )) );

			-- 160ns to 160ns
			elsif(( Disc_In( iDisc_160 ) = '1' ) and ( Block_Out( iDisc_160) = '0' ))
				then PSteer_Cnt( iDisc_160 ) <= Shape_time;

			-- 320ns to 160ns
			elsif(( Disc_In( iDisc_320) = '1' ) and ( Block_Out( iDisc_320) = '0' ))
				then PSteer_Cnt( iDisc_160 ) <= Shape_Time - ( conv_integer( Peak_Time_Reg( 35 downto 24 )) - conv_integer( Peak_Time_Reg( 23 downto 12 )) );

			-- 600ns to 160ns
			elsif(( Disc_In( iDisc_600 ) = '1' ) and ( Block_Out( iDisc_600) = '0' ))
				then PSteer_Cnt( iDisc_160 ) <= Shape_Time - ( conv_integer( Peak_Time_Reg( 47 downto 36 )) - conv_integer( Peak_Time_Reg( 23 downto 12 )) );
	
			-- BLM to High
			elsif(( Disc_In( iDisc_Main) = '1' ) and ( Block_Out( iDisc_Main) = '0' ))
				then PSteer_Cnt( iDisc_160 ) <= Shape_Time - ( Peak_Time_D2 - conv_integer( Peak_Time_Reg( 23 downto 12 )) );
			
			-- Now just count down
			elsif(( PSteer_Cnt_En( iDisc_160 ) = '1' ) and ( PSteer_Cnt( iDisc_160 ) > 0 ))
				then PSteer_Cnt( iDisc_160 ) <= PSteer_Cnt( iDisc_160) - 1;
			end if;
			-- 160ns Discriminator -----------------------------------------------------------------

			-- 320ns Discriminator -----------------------------------------------------------------
			-- 40ns to 320ns
			if(( Disc_In( iDisc_40 ) = '1' ))
				then PSteer_Cnt( iDisc_320) <= Shape_Time + ( conv_integer( Peak_Time_Reg( 35 downto 24 )) - conv_integer( Peak_Time_Reg( 11 downto 0 )) );
		
			-- 160ns to 320ns
			elsif(( Disc_In( iDisc_160 ) = '1' ) and ( Block_Out( iDisc_160 ) = '0' ))
				then PSteer_Cnt(iDisc_320) <= Shape_Time + ( conv_integer( Peak_Time_Reg( 35 downto 24 )) - conv_integer( Peak_Time_Reg( 23 downto 12 )) );
		
			-- 320ns to 320ns
			elsif(( Disc_In( iDisc_320 ) = '1' ) and ( Block_Out( iDisc_320) = '0' ))
				then PSteer_Cnt( iDisc_320 ) <= Shape_time;

			-- 600ns to 320ns
			elsif(( Disc_In( iDisc_600 ) = '1' ) and ( Block_Out( iDisc_600) = '0' ))
				then PSteer_Cnt( iDisc_320 ) <= Shape_Time - ( conv_integer( Peak_Time_Reg( 47 downto 36 )) - conv_integer( Peak_Time_Reg( 35 downto 24 )) );

			-- BLM to Med
			elsif(( Disc_In( iDisc_Main) = '1' ) and ( Block_Out( iDisc_Main) = '0' ))
				then PSteer_Cnt( iDisc_320 ) <= Shape_Time - ( Peak_Time_D2 - conv_integer( Peak_Time_Reg( 35 downto 24 )));

			-- Now just count down			
			elsif(( PSteer_Cnt_En( iDisc_320) = '1' ) and ( PSteer_Cnt( iDisc_320 ) > 0 ))
				then PSteer_Cnt( iDisc_320) <= PSteer_Cnt( iDisc_320) - 1;
			end if;
			-- 320ns Discriminator -----------------------------------------------------------------

			-- 600ns Discriminator -----------------------------------------------------------------
			-- 40ns to 600ns
			if(( Disc_In( iDisc_40 ) = '1' ))
				then PSteer_Cnt( iDisc_600) <= Shape_Time + ( conv_integer( Peak_Time_Reg( 47 downto 36 )) - conv_integer( Peak_Time_Reg( 11 downto 0 )) );
		
			-- 160ns to 600ns
			elsif(( Disc_In( iDisc_160 ) = '1' ) and ( Block_Out( iDisc_160 ) = '0' ))
				then PSteer_Cnt(iDisc_600) <= Shape_Time + ( conv_integer( Peak_Time_Reg( 47 downto 36 )) - conv_integer( Peak_Time_Reg( 23 downto 12 )) );
		
			-- 320ns to 600ns
			elsif(( Disc_In( iDisc_320 ) = '1' ) and ( Block_Out( iDisc_320 ) = '0' ))
				then PSteer_Cnt(iDisc_600) <= Shape_Time + ( conv_integer( Peak_Time_Reg( 47 downto 36 )) - conv_integer( Peak_Time_Reg( 35 downto 24 )) );

			-- 600ns to 600ns
			elsif(( Disc_In( iDisc_600 ) = '1' ) and ( Block_Out( iDisc_600) = '0' ))
				then PSteer_Cnt( iDisc_600 ) <= Shape_time;

			-- BLM to Med
			elsif(( Disc_In( iDisc_Main) = '1' ) and ( Block_Out( iDisc_Main) = '0' ))
				then PSteer_Cnt( iDisc_600 ) <= Shape_Time - ( Peak_Time_D2 - conv_integer( Peak_Time_Reg( 35 downto 24 )));

			-- Now just count down			
			elsif(( PSteer_Cnt_En( iDisc_600) = '1' ) and ( PSteer_Cnt( iDisc_600 ) > 0 ))
				then PSteer_Cnt( iDisc_600) <= PSteer_Cnt( iDisc_600) - 1;
			end if;
			-- 600ns Discriminator -----------------------------------------------------------------

		
			-- BLM Discriminator -------------------------------------------------------------------------
			-- BLM to 40ns
			if( Disc_In( iDisc_40 ) = '1' )
				then PSteer_Cnt( iDisc_Main ) <= Shape_Time + ( Peak_Time_D2 - conv_integer( Peak_Time_Reg( 11 downto 0 )) );
			
			-- BLM to 160ns
			elsif(( Disc_In( iDisc_160 ) = '1' ))--  and ( Block_Out( iDisc_160) = '0' ))
				then PSteer_Cnt( iDisc_Main) 	<= Shape_Time + ( Peak_Time_D2 - conv_integer( Peak_Time_Reg( 23 downto 12 )) );
			
			-- BLM to 320ns
			elsif(( Disc_In( iDisc_320 ) = '1' )) --  and ( Block_Out( iDisc_320) = '0' ))
				then PSteer_Cnt( iDisc_Main) 	<= Shape_Time + ( Peak_Time_D2 - conv_integer( Peak_Time_Reg( 35 downto 24 )) );

			-- BLM to 600ns
			elsif(( Disc_In( iDisc_600 ) = '1' )) --  and ( Block_Out( iDisc_600) = '0' ))
				then PSteer_Cnt( iDisc_Main) 	<= Shape_Time + ( Peak_Time_D2 - conv_integer( Peak_Time_Reg( 47 downto 36 )) );

			-- BLM to BLM			
			elsif(( Disc_In( iDisc_Main ) = '1' )) --  and ( Block_Out( iDisc_Main ) = '0' ))
				then PSteer_Cnt( iDisc_Main ) <= Shape_Time;
			
			-- Now just count down
			elsif(( PSteer_Cnt_En( iDisc_Main ) = '1' ) and ( PSteer_Cnt( iDisc_Main ) > 0 ))
				then PSteer_Cnt( iDisc_Main ) <= PSteer_Cnt( iDisc_Main ) - 1;
			end if;
			-- BLM Discriminator -------------------------------------------------------------------------
			----------------------------------------------------------------------------------------------
		
			------------------------------------------------------------------------------------
			-- Stretched Pile-Up Reject Signal - Assert with a Detection and maintain it for SHAPE_TIME
			if( Pur_Det = '1' )
				then Pur_Str_En <= '1';
			elsif( Pur_Str_Cnt = Pur_Str_Cnt_Max )
				then Pur_Str_En <= '0';
			end if;
			
			if(( Pur_Str_En = '0' ) or ( Pur_Det = '1' ))
				then Pur_Str_Cnt <= 0;
			elsif( Pur_Str_En = '1' )
				then Pur_Str_Cnt <= Pur_Str_Cnt + 1;
			end if;			
			-- Stretched Pile-Up Reject Signal - Assert with a Detection and maintain it for SHAPE_TIME
			------------------------------------------------------------------------------------

			------------------------------------------------------------------------------------
			-- Preamp Busy Logic for All Discriminators
			-- The last one (iDisc_PUR) is only used to stetech the PUR longer, but not alter "PA_BUSY"
--			for i in iDisc_40 to iDisc_Pur loop
			for i in iDisc_40 to iDisc_Main loop
				-------------------------------------------------------------------------------
				-- Assert PA Busy when PA Reset goes active, but don't start
				-- the timer until PA Reset goes away
				if( Disc_En(i) = '0' )
					then PA_Busy(i) <= '0';
				elsif( PA_Reset = '1' )
					then PA_Busy(i) <= '1';
				elsif( PA_Busy_Cnt(i) = PA_Busy_Cnt_Max(i))
					then PA_Busy(i) <= '0';
				end if;

				if(( PA_Busy(i) = '0' ) or ( PA_Reset = '1' ))
					then PA_Busy_Cnt(i) <= 0;
				elsif(( PA_Busy(i) = '1' ) and ( PA_Reset = '0' ))
					then PA_Busy_Cnt(i) <= PA_Busy_Cnt(i) + 1;
				end if;
				-- Assert PA Busy when PA Reset goes active, but don't start
				-- the timer until PA Reset goes away
				-------------------------------------------------------------------------------			
			end loop;
			------------------------------------------------------------------------------------			
			
			------------------------------------------------------------------------------------
			-- if Out-of-Range - time-out for 2x Amp Time and hold-off the BLR (or keep BLR_Busy active)
			if( Int_Cmp_Vec(Cmp_Out_Of_Range) = '1' )
				then Out_Of_Range_Cen <= '1';
			elsif( Out_Of_Range_Cnt >= Out_Of_Range_Cnt_Max )
				then Out_Of_Range_Cen <= '0';
			end if;
			
			if( Out_Of_Range_Cen = '0' )
				then Out_Of_Range_Cnt <= 0;
			elsif( Out_Of_Range_Cnt < iCntr_Max )
				then Out_Of_Range_Cnt <= Out_of_Range_Cnt + 1;
			end if;
			------------------------------------------------------------------------------------
			

			----------------------------------------------------------------------------------------------
			-- BLM Pulse Busy: used to gate-off Baseline Restoration
			PA_Busy( iDisc_Pur ) <= '0';
			if(( FDisc_Comb = '1' ) or ( PA_Busy(iDisc_Main) = '1' )) 
				then BLR_BUSY <= '1';
			elsif( BLR_BUSY_Cnt = BLR_BUSY_Cnt_Max  )
				then BLR_BUSY <= '0';
			end if;
			
			-- Peak Done Resets the counter
			if(( BLR_BUSY = '0' ) or ( FDisc_Comb = '1' ) or ( PA_Busy(iDisc_Main) = '1' )) 
				then BLR_BUSY_Cnt <= 0;
			elsif( BLR_BUSY = '1' )
				then BLR_BUSY_Cnt <= BLR_BUSY_Cnt + 1;
			end if;		
			-- BLM Pulse Busy: used to gate-off Baseline Restoration
			------------------------------------------------------------------------------------

			------------------------------------------------------------------------------------
			-- Baseline Histogtram Timing
			if( ( BLevel_En 						= '1' ) 
				and ( Pur 						= '0' )
				and ( PA_Reset 					= '0' )
				and ( BLR_Busy 					= '0' ) 
				and ( Out_Of_Range_Cen				= '0' )
				and ( conv_Integer( PA_Busy ) 		= 0 )
				and ( conv_integer( PSteer_Cnt_En ) 	= 0 )
				and ( conv_integer( Disc_Level ) 		= 0 )) then				
					if( Blevel_Cnt = Blevel_Cnt_Max ) then
						Blevel_Cnt <= 0;
						Blevel_Upd <= '1';
					else
						Blevel_Cnt <= Blevel_Cnt + 1;
						Blevel_Upd <= '0';
					end if;
			else
				Blevel_Cnt <= Blevel_Cnt;
				Blevel_Upd <= '0';
			end if;
				
			
--			if( Blevel_Cnt = Blevel_Cnt_Max )
--				then Blevel_Cnt <= 0;
--				else Blevel_Cnt <= Blevel_Cnt + 1;
--			end if;
--
--			if( ( BLevel_En 						= '1' ) 
--				and ( Pur 						= '0' )
--				and ( PA_Reset 					= '0' )
--				and ( BLR_Busy 					= '0' ) 
--				and ( conv_Integer( PA_Busy ) 		= 0 )
--				and ( conv_integer( PSteer_Cnt_En ) 	= 0 )
--				and ( conv_integer( Disc_Level ) 		= 0 )
--				and ( Blevel_Cnt 					= Blevel_Cnt_Max ))
--					then Blevel_Upd <= '1';
--					else Blevel_Upd <= '0';
--			end if;			
			-- Baseline Histogtram Timing
			------------------------------------------------------------------------------------

			
			------------------------------------------------------------------------------------
			-- FDISC Generation
			--	FDisc is used for timing purposes (Beam Blank)
			--	FDisc_CPS is used for CPS Determination, but it is delayed to mask out a leading count just before Reset
			-- Stretch to 2- clocks wide (used at 50Mhz)
			FDisc_Vec 	<= FDisc_vec( FDISC_CPS_MAX-1 downto 0 ) & FDisc_Comb;
               
			FDisc_BB		<= FDisc_Comb;

			-- Now take the delayed FDisc, and if PA Reset or PA busy is not active, send it out
--			if(( PA_Reset = '0' ) and ( PA_Busy(iDisc_Pur) = '0' ) and ( FDisc_Vec(FDisc_CPS_Max) = '1' ))
			if(( PA_Reset = '0' ) and ( PA_Busy(iDisc_Main) = '0' ) and ( FDisc_Comb = '1' ))
				then FDisc_CPS <= '1';
				else FDisc_CPS <= '0';
			end if;			
			-- FDISC Generation
			------------------------------------------------------------------------------------
			
			------------------------------------------------------------------------------------
			-- Live Time Correction via Barhnart Method 
			-- IF Maximum Count
			if( BH_lt_cntr = BH_LT_CNTR_MAX ) then
				BH_lt_cntr 	<= BH_LT_CNTR_PRESET;  	-- Preset the Counter
				BH_LTIME_EN 	<= '0';				-- Disable Live Time Counter
				BH_lt_inc_en 	<= '0';				-- Disable Counter from Being Incremented

			-- If Minimum Count
			elsif( BH_lt_cntr = 0 ) then 
				BH_lt_cntr 	<= BH_LT_CNTR_PRESET;  	--  Preset the counter
				BH_LTIME_EN 	<= '1';				-- Enable Live Time Counter 
				BH_lt_inc_en	<= '1';				-- Enable Counter to be incremented

			-- If output Count			
			elsif(( Inc_NPS = '1' ) and ( FDisc_CPS = '0' ))
				then BH_lt_cntr <= BH_lt_Cntr - 1;

			elsif(( Inc_NPS = '1' ) and ( FDisc_CPS = '1' ) and ( BH_lt_inc_en = '0' ))  		-- 6/23/00
				then BH_lt_cntr <= BH_lt_cntr - 1;		-- Decrement Count

			-- IF Input Count and Counter can be incremented
			elsif(( Inc_NPS = '0' ) and ( FDisc_CPS = '1' ) and ( BH_lt_inc_en = '1' ))			-- 6/23/00
				then BH_lt_cntr <= BH_lt_cntr + 1;		-- Increment Count
			end if;
			-- Live Time Counter via Barhnart Method 			
               ------------------------------------------------------------------------------------
			
		end if;
	end process;
	----------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
end behavioral;               -- Pileup_Reject
---------------------------------------------------------------------------------------------------



