---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	EDSBlock.vhd
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : September 21, 2007
--   Board          : XScope Instrument Control Board
--   Schematic      : 4035.007.27000
--
--   Top Level      : 4035.009.99330, S/W, XScope, Instrument Control
--
--   Description:	
--		This module is responsible for all EDS Data
--   History:       <date> - <Author> - Ver
--		December 28, 2011
--			Gap Time increased from 6 bits to 8 bits, 
--			ATCW_GapTime bits changed positions, from 5;0 to 23:16
--			Also effects EDSBlock and Main_Channel and Main_GT_Ram
--		06/10/11 Rev 0.1 - MCS
--			Module: Pileup_Reject: Chagned Out-of-Range Timeout to be a multiple of OTR_FACTOR
--		11041410
--			SCA's Eliminated
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
	
library altera_mf;
	use altera_mf.altera_mf_components.all;

	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
entity EDSBlock is
	generic(
		-- Strobe bit positions ----------------------------------------------------------------------
		STB_TIME_START 		: integer := 0;
		STB_TIME_STOP 			: integer := 1;
		STB_TIME_CLR 			: integer := 2;
		STB_TIME_RESET			: integer := 18;	-- Only Resets Times
		STB_FIR_MR			: integer := 3;
		-- Control Word Bit Positions ----------------------------------------------------------------
		ATCW_BLR_LSB			: integer := 6;
		ATCW_BLR_MSB			: integer := 7;
		ATCW_Disc_Dis_40		: integer := 8;
		ATCW_Disc_Dis_160		: integer := 9;
		ATCW_Disc_Dis_320		: integer := 10;
		ATCW_Disc_Dis_640		: integer := 11;
		ATCW_Disc_Dis_BLM		: integer := 12;	
		CW_AVG_ENABLE			: integer := 2;
		CW_CPS_RESET_INH		: integer := 25;
		CW_PRESET_MODE_L		: integer := 16;
		CW_PRESET_MODE_H		: integer := 18 );
	-- Control Word Bit Positions ----------------------------------------------------------------		
	port(
		clk100			: in		std_logic;		
		ext_Trig			: in		std_logic;
		fast_ad			: in		std_logic_vector( 17 downto 0 );
		disc_ad			: in		std_logic_vector( 17 downto 0 );
		Gap_Time			: in		std_logic_Vector(  7 downto 0 );
		Cmd_Stb			: in		std_logic_Vector( 31 downto 0 );
		PA_Reset_Det		: in		std_logic;
		AmpTime			: in		std_logic_Vector( 31 downto 0 );
		Blevel_En			: in 	std_logic;
		ATCWord			: in		std_logic_Vector( 31 downto 0 );
		CWord			: in		std_logic_Vector( 31 downto 0 );
		Clip_MaxMin 		: in		std_logic_Vector( 31 downto 0 );
		PRESET_VAL		: in		std_logic_Vector( 31 downto 0 );
		Pixels_Line		: in		std_Logic_Vector( 31 downto 0 );
		Lines_Frame		: in		std_logic_Vector( 31 downto 0 );
		offset			: in		std_logic_Vector( 31 downto 0 );
		OTR_Factor		: in		std_logic_Vector( 31 downto 0 );
		RTime_Thr			: in		std_logic_Vector( 31 downto 0 );		
		TLevel			: in		std_logic_Vector( (32*5)-1 downto 0 );
--		SCABB_Data 		: in		std_logic_Vector( 31 downto 0 );
--		SCABB_Wen			: in		std_logic;
--		SCABB_WA			: in		std_logic_vector( 6 downto 0 );
--		SCABB_WD			: in		std_logic_Vector( 31 downto 0 );
--		SCABB_RA			: in		std_logic_Vector( 6 downto 0 );          
--		SCABB_RD			: buffer	std_logic_Vector( 31 downto 0 );
--		SCABB_OUT			: buffer	std_logic;		
		FDisc_BB			: buffer	std_logic;
		Meas_Done			: buffer	std_logic := '0';					-- Peak Identification Done
		CPeak			: buffer	std_Logic_Vector( 11 downto 0 );		-- Peak Value
		Blevel_Upd		: buffer	std_logic := '0';					-- Baseline Level Update
		Blevel			: buffer	std_logic_Vector(  11 downto 0 );		-- Baseline Level
		Peak_Time			: buffer	std_logic_Vector( 143 downto 0 );
		Spec_Update		: buffer	std_logic := '0';
		Time_Enable		: buffer	std_logic := '0';
		Preset_FMap		: buffer	std_logic;
		Preset_SMap		: buffer	std_logic;
          evch_sel            : buffer  std_logic;
		RTime_Rej			: buffer	std_logic;
		RTime_Upd			: buffer	std_logic;
		RTime			: buffer	std_logic_Vector( 31 downto 0 );
		Line_Code			: buffer	std_logic_Vector(  1 downto 0 );
		Pixel_Status		: buffer	std_logic_Vector( 15 downto 0 );
		Line_Status		: buffer	std_logic_Vector( 15 downto 0 );
		Frame_Status		: buffer	std_logic_Vector( 15 downto 0 );				
		Disc_Cnts			: buffer	std_logic_Vector( 63 downto 0 );
		CPS				: buffer	std_logic_vector( 31 downto 0 );		-- Input Count Rate
		NPS				: buffer	std_logic_Vector( 31 downto 0 );		-- Output Count Rate
		CTIME			: buffer	std_logic_Vector( 31 downto 0 );
		LTIME			: buffer	std_logic_Vector( 31 downto 0 );
		net_cps			: buffer	std_logic_Vector( 31 downto 0 );
		net_nps			: buffer	std_logic_Vector( 31 downto 0 );
		Disc_Fir_Out		: buffer  std_logic_Vector( (20*4)+19 downto 0 );
		Main_Dly_Data 		: buffer  std_logic_Vector( 89 downto 0 );
		Main_Out			: buffer  std_logic_Vector( 19 downto 0 );
--		Main_Out			: buffer  std_logic_Vector( 15 downto 0 );
		Main_Sum			: buffer	std_logic_Vector( 31 downto 0 );
		tp_Data			: buffer	std_logic_vector( 15 downto 0 ) );
	end EDSBlock;
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
architecture behavioral of EDSBLock is
--	constant GEN_SCA 	: integer := 0;
	constant GEN_DISC	: integer := 1;
	
	-- Vector Bit Positions ----------------------------------------------------------------------
	constant iDisc_40 				: integer := 0;
	constant iDIsc_160 				: integer := 1;
	constant iDIsc_320 				: integer := 2;
	constant iDIsc_640 				: integer := 3;
	constant iDisc_Main				: integer := 4;
	-- Vector Bit Positions ----------------------------------------------------------------------

	constant CLK_PER				: integer := 10;
	constant CPS_Ms100_cnt_max		: integer := 100000000 / CLK_PER;
	
	signal CPS_Ms100_Cnt 			: integer range 0 to CPS_Ms100_Cnt_Max;
	signal CPS_Ms100_tc				: std_logic;
			
	signal Amp_Time_Disc_En			: std_logic_Vector( iDisc_Main downto iDisc_40 );

	signal BH_LTime_En				: std_logic;
	signal BLR_Busy				: std_logic := '0';
	
	signal Disc_en					: std_logic_Vector( iDisc_Main downto iDisc_40 );
	signal Disc_level_out			: std_logic_Vector( iDisc_Main downto iDisc_40 );
	signal Disc_level_Cmp			: std_logic_Vector( iDisc_Main downto iDisc_40 );
	signal Disc_Mode_Vec 			: std_logic_Vector( 11 downto 0 );

	signal Inc_CPS 				: std_logic;
	signal Fir_Reset				: std_logic;
	
	signal Inc_NPS					: std_logic;
	
	signal Peak_Time_Mux		     : std_logic_Vector( 11 downto 0 );
	signal PA_Busy					: std_Logic_vector(  5 downto 0 );

	signal Out_Of_Range_Cen			: std_logic;
     ----------------------------------------------------------------------------------------------
	component Countrate is 
		port( 
			CLK100	    	: in      std_logic;                         -- Master Clock
			ms100_tc		: in		std_Logic;					-- 100ms Timer
			inc_cps		: in		std_logic;					-- FDISC or PEAK_DONE
			clear		: in		std_logic;
			enable		: in		std_logic;
			cps			: buffer	std_logic_Vector( 31 downto 0 );
			net			: buffer	std_logic_Vector( 31 downto 0 ) );
	end component Countrate;
     ----------------------------------------------------------------------------------------------

     ----------------------------------------------------------------------------------------------
	component disc_fast is 
		port( 
			clk100			: in		std_logic;					-- MAster Clock
			PA_Reset			: in		std_logic;
			PA_Busy			: in		std_Logic;
			Fir_Reset			: in		std_logic;
			Disc_En			: in		std_logic;
			Time_Clr			: in		std_logic;
			Time_Enable		: in		std_logic;		
			Mode				: in		std_Logic_Vector( 2 downto 0 );		
			tlevel			: in		std_logic_vector( 15 downto 0 );	-- Discriminator Level
			ad_data			: in		std_logic_vector( 17 downto 0 );
			fir_data			: buffer	std_logic_Vector( 19 downto 0 );	-- Filter Output
			Peak_time			: buffer	std_logic_Vector( 11 downto 0 );
			Disc_Out			: buffer	std_logic;					-- Discriminator Level Exceeded	
			Disc_Level		: buffer	std_logic;
			Disc_Cnts			: buffer	std_logic_vector(  7 downto 0 ) );		
	end component disc_fast;
     ----------------------------------------------------------------------------------------------
    
     ----------------------------------------------------------------------------------------------
	component Disc_Main is 
		port( 
			CLK100			: in		std_logic;					-- MAster Clock
			PA_Busy			: in		std_logic;
			PA_Reset			: in		std_logic;
			Fir_Reset			: in		std_logic;
			Disc_En			: in		std_logic;
			Time_Clr			: in		std_logic;
			Time_Enable		: in		std_logic;
			tlevel			: in		std_logic_vector( 15 downto 0 );	-- Discriminator Level		
			TimeConst			: in		std_logic_vector(  2 downto 0 );
			data_abc			: in		std_logic_vector( 53 downto 0 );
			fir_data   		: buffer 	std_logic_vector( 19 downto 0 );		
			Disc_Out			: buffer	std_logic;					-- Discriminator Level Exceeded	
			Disc_Level		: buffer	std_logic;
			Disc_Cnts			: buffer	std_logic_vector(  7 downto 0 ) );
	end component Disc_Main;
     ----------------------------------------------------------------------------------------------

     ----------------------------------------------------------------------------------------------
	component Main_Channel is 
		port( 
			CLK100		: in		std_logic;					-- MAster Clock
			PA_Reset		: in		std_logic;
			PA_Busy		: in		std_logic;	
			BLR_Busy		: in		std_logic;
			Fir_Reset		: in		std_logic;
			TimeConst 	: in		std_logic_Vector(   2 downto 0 );
			BLR_Sel		: in		std_logic_Vector(   1 downto 0 );		
			data_abc		: in		std_logic_Vector(  89 downto 0 );	-- First TAP
			offset		: in		std_logic_Vector(  31 downto 0 );
			Gap_Time		: in		std_logic_Vector(   7 downto 0 );
			Main_out		: buffer	std_logic_Vector(  19 downto 0 );	-- FIR Output 
--			Main_out		: buffer	std_logic_Vector(  15 downto 0 );	-- FIR Output 
			Blevel_Out	: buffer	std_logic_Vector(  11 downto 0 );
			sum			: buffer	std_logic_Vector(  31 downto 0 ) );
	end component Main_Channel;
     ----------------------------------------------------------------------------------------------

     ----------------------------------------------------------------------------------------------
	component Main_RAM is 
		port( 
			CLK100		: in		std_logic;					-- Master Clock
			WE_Fir_Mr		: in		std_logic;
			PA_Reset		: in		std_logic;
			adata		: in		std_logic_Vector(   17 downto 0 );		-- A/D Input Data
			TimeConst		: in		std_logic_Vector(    2 downto 0 );
			Fir_Reset		: buffer	std_logic;
			Peak_Time		: buffer	std_Logic_Vector(  143 downto 48 );
			Peak_Time_Mux	: buffer	std_Logic_Vector(   11 downto  0 );
			Dout			: buffer	std_logic_Vector(   89 downto  0 ) );	-- Delayed Output Data
	end component Main_RAM;
     ----------------------------------------------------------------------------------------------

     ----------------------------------------------------------------------------------------------
     component pileup_reject is 	
		port( 
			CLK100         	: in      std_logic;			     	-- Master Clock Input
			AVG_ENABLE		: in		std_logic;
			PA_Reset			: in		std_logic;					-- PreAmp Reset
			BLevel_En			: in		std_logic;
			Disc_En			: in		std_logic_Vector(  5 downto 0 );
			Disc_In        	: in		std_logic_Vector(  4 downto 0 );	-- Discriminator Inputs
			Disc_Level		: in		std_logic_Vector(  4 downto 0 );
			Gap_Time			: in		std_logic_vector(  7 downto 0 );	-- Gap Time
			Peak_Time			: in		std_logic_Vector( 59 downto 0 );	-- Peak time
			TimeConst			: in		std_logic_Vector(  2 downto 0 );
			Clip_Min			: in		std_logic_vector( 11 downto 0 );
			Clip_Max			: in		std_logic_vector( 11 downto 0 );
			Main_Out			: in		std_logic_Vector( 19 downto 0 );
			OTR_Factor		: in		std_logic_Vector( 31 downto 0 );
			RTime_Rej			: in		std_logic;
			BH_LTIME_EN		: buffer	std_logic;
			FDISC_BB			: buffer	std_Logic;					-- Fast Disc
			FDisc_CPS			: buffer	std_logic;
			Inc_NPS			: buffer	std_logic;
			BLR_BUSY			: buffer	std_logic := '0';
			PA_Busy			: buffer	std_logic_Vector( 5 downto 0 );
			Blevel_Upd		: buffer	std_logic;
			CPeak			: buffer	std_logic_vector( 11 downto 0 );
			Meas_Done			: buffer	std_logic;
			Out_Of_Range_Cen	: buffer	std_logic );
	end component pileup_reject;
     ----------------------------------------------------------------------------------------------

	-------------------------------------------------------------------------------------------
	component Rise_Time is 
		port( 
			CLK100		: in		std_logic;					-- Master Clock
			Din			: in		std_logic_Vector( 17 downto 0 );	-- A/D Input Data
			RTime_Thr		: in		std_logic_Vector( 31 downto 0 );
			Rtime_Upd		: buffer	std_logic;
			RTime_Rej		: buffer	std_logic;
			RTime		: buffer	std_logic_Vector( 31 downto 0 ) );
	end component Rise_Time;
	-------------------------------------------------------------------------------------------
--	----------------------------------------------------------------------------------------------
--	component scabb is 
--		port(
--               CLK100         : in      std_logic;
--			Param		: in		std_logic_Vector( 31 downto 0 );	-- Beam Blanking Delay
--			fdisc		: in		std_logic;					-- Trigger for Beam Blank Output
--			Meas_Done		: in		std_logic;					-- Trigger for SCA Output
--			Time_Enable	: in		std_logic;
--			SCABB_WEN		: in		std_logic;
--			SCABB_WA		: in		std_logic_Vector(  6 downto 0 );
--			SCABB_WD		: in		std_logic_Vector( 31 downto 0 );
--			SCABB_RA		: in		std_logic_Vector(  6 downto 0 );
--			SCABB_RD		: buffer	std_logic_Vector( 31 downto 0 );
--			CPeak		: in		std_logic_Vector( 11 downto 0 );
--			SCABB_Out		: buffer	std_logic );
--	end component scabb;
--     ----------------------------------------------------------------------------------------------

     ----------------------------------------------------------------------------------------------
	component TimeCtrl is 
		port( 
			CLK100			: in      std_logic;                                   	-- Master Clock
			EXT_TRIG			: in		std_logic;
			Time_Clr			: in		std_logic;
			Time_Start		: in		std_logic;
			Time_Stop			: in		std_logic;	
			Time_Reset		: in		std_logic;
			BH_LTIME_EN		: in 	std_logic;
			PRESET_MODE		: in		std_logic_Vector(  2 downto 0 );
			PRESET			: in		std_logic_Vector( 31 downto 0 );
			Pixels_Line		: in		std_Logic_Vector( 31 downto 0 );	-- How many Pixels/Line
			Lines_Frame		: in		std_logic_Vector( 31 downto 0 );	-- How Many Lines/Frame
			Spec_Update		: buffer	std_logic;
			Time_Enable		: buffer	std_logic;
			Preset_FMap		: buffer	std_logic;
			Preset_SMap		: buffer	std_logic;
			CTIME			: buffer	std_logic_Vector( 31 downto 0 );
			LTIME			: buffer	std_logic_Vector( 31 downto 0 );
			Line_Code			: buffer	std_logic_Vector(  1 downto 0 );
			Pixel_Status		: buffer	std_logic_Vector( 15 downto 0 );
			Line_Status		: buffer	std_logic_Vector( 15 downto 0 );
			Frame_Status		: buffer	std_logic_Vector( 15 downto 0 ) );
	end component TimeCtrl;
     ----------------------------------------------------------------------------------------------
     

begin
     ----------------------------------------------------------------------------------------------
	-----------------------------------------------------------------------------------------
	-- Main Channel Memory
	-----------------------------------------------------------------------------------------		
	-- PA_Reset_Det is how long the input is reset, 
	-- PA_Busy is stretched to 2x the peaking time
	Main_RAM_Inst : Main_Ram
		port map(
			clk100			=> CLK100,
			WE_Fir_Mr			=> Cmd_Stb( STB_FIR_MR ),
			PA_Reset			=> PA_Reset_Det,
			adata			=> Fast_AD,
			TimeConst			=> AmpTime( 2 downto 0 ),
			Fir_Reset			=> Fir_Reset,
			Peak_time			=> Peak_Time(  143 downto 48 ),
			Peak_Time_Mux		=> Peak_Time_Mux,
			Dout				=> Main_Dly_Data );
	-----------------------------------------------------------------------------------------

	-----------------------------------------------------------------------------------------
	-- Discriminator Channels 
	-----------------------------------------------------------------------------------------
	Disc_Mode_Vec( 11 downto 3 ) <= "100" & "011" & "010";
	Disc_Mode_Vec(  2 downto 0 ) <= "000" when ( conv_integer( AmpTime( 2 downto 0 )) = 0 ) else "001";
	
	Disc_Fast_Gen : for i in iDisc_40 to iDisc_640 generate
		Disc_Gen : if ( GEN_DISC = 1  ) generate     
			Disc_Fast_Inst : disc_fast
				port map(
					CLK100		=> CLK100,
					PA_Reset		=> PA_Reset_Det,
					PA_Busy		=> PA_Busy( i ),
					Fir_Reset		=> Fir_Reset,
					Disc_En		=> Disc_En(i),
					Time_Clr		=> Cmd_Stb(STB_TIME_CLR),
					Time_Enable	=> Time_Enable,
					Mode			=> Disc_Mode_Vec( (i*3)+2 downto i*3 ),
					tlevel		=> TLevel(       (i*32)+15 downto i*32 ),
					ad_data		=> Disc_ad,
					fir_data		=> Disc_Fir_Out(   ( i*20)+19 downto i*20 ),
					Peak_time		=> Peak_Time(      ( i*12)+11 downto i*12 ),
					Disc_out		=> Disc_Level_Out(i),
					Disc_Level	=> Disc_Level_Cmp(i),
					Disc_Cnts		=> Disc_Cnts(    (i*8)+ 7 downto i*8 ));							
		end generate;				-- GEN_DISC = 1

		Disc_No_Gen : if( GEN_DISC = 0 ) generate
				Disc_Fir_Out(   ( i*20)+19 downto i*20 ) 	<= (others=>'0');
				Peak_Time(      ( i*12)+11 downto i*12 ) 	<= (others=>'0');
				Disc_Level_Out(i) 						<= '0';
				Disc_Level_Cmp(i) 						<= '0';
				Disc_Cnts(    (i*8)+ 7 downto i*8 ) 		<= (others=>'0');
		end generate;				-- GEN_DISC = 0		
	end generate;
	
	-----------------------------------------------------------------------------------------

	-----------------------------------------------------------------------------------------
	-- Main Discriminaotr Channel 
	-----------------------------------------------------------------------------------------
	Disc_Main_Inst : Disc_Main
		port map(
			clk100			=> CLK100,
			PA_Busy			=> PA_Busy( iDisc_Main ),
			PA_Reset			=> PA_Reset_Det,
			Fir_Reset			=> Fir_Reset,
			Disc_En			=> Disc_En(iDisc_Main),
			Time_Clr			=> Cmd_Stb(STB_TIME_CLR),
			Time_Enable		=> Time_Enable,
			tlevel			=> TLevel(       (iDisc_Main*32)+15 downto iDisc_Main*32 ),
			TimeConst			=> AmpTime( 2 downto 0 ),
			data_abc			=> Main_Dly_Data( 53 downto 0 ),
			fir_data			=> Disc_Fir_Out( (iDisc_Main*20)+19 downto iDisc_Main*20 ),
			Disc_out			=> Disc_Level_Out(iDisc_Main),
			Disc_Level		=> Disc_Level_Cmp(iDisc_Main),
			Disc_Cnts			=> Disc_Cnts(    (iDisc_Main*8)+ 7 downto iDisc_Main*8 ));				
		
	-----------------------------------------------------------------------------------------

	-----------------------------------------------------------------------------------------
	-- Main PHA Channel
	Main_Channel_Inst : Main_Channel
		port map(
			clk100			=> CLK100,
			PA_Reset			=> PA_Reset_Det,
			PA_Busy			=> PA_Busy( iDisc_Main ),
			BLR_Busy			=> BLR_Busy,
			Fir_Reset			=> Fir_Reset,
			TimeConst			=> AmpTime( 2 downto 0 ),
			BLR_SEL			=> ATCWord( ATCW_BLR_MSB downto ATCW_BLR_LSB ),
			data_abc			=> Main_Dly_Data,
			offset			=> Offset,				
			Gap_Time			=> Gap_Time,
			Main_out			=> Main_Out,
			Blevel_Out		=> Blevel,
			sum				=> Main_Sum );		
	-----------------------------------------------------------------------------------------

	-----------------------------------------------------------------------------------------				
	PileUp_Reject_Inst : pileup_reject 
		port map(
			clk100			=> CLK100,
			AVG_ENABLE		=> CWord(CW_AVG_ENABLE),
			PA_Reset			=> PA_Reset_Det,
			Blevel_En			=> Blevel_En,
			Disc_En			=> '1' & Disc_En,
			Disc_In        	=> Disc_Level_Out,
			Disc_Level		=> Disc_Level_Cmp,
			Gap_Time			=> Gap_Time,
			Peak_Time			=> Peak_Time_Mux & Peak_Time( (iDIsc_640*12)+11 downto 0 ),
			TimeConst			=> AmpTime( 2 downto 0 ),
			Clip_Min			=> Clip_MaxMin( 11+00 downto 00 ),
			Clip_Max			=> Clip_MaxMin( 11+16 downto 16 ),
			Main_Out			=> Main_Out,
			OTR_Factor		=> OTR_Factor,
			RTime_Rej			=> RTime_Rej,
			BH_LTIME_EN		=> BH_LTime_En,
			FDISC_BB			=> FDisc_BB,
			FDisc_CPS			=> Inc_CPS,
			Inc_NPS			=> Inc_NPS,
			BLR_BUSY			=> BLR_Busy,			
			PA_Busy			=> PA_Busy,
			Blevel_Upd		=> Blevel_Upd,
			CPeak			=> CPeak,
			Meas_Done			=> Meas_Done,
			Out_Of_Range_Cen	=> Out_Of_Range_Cen );

	-----------------------------------------------------------------------------------------

     ----------------------------------------------------------------------------------------------
	Rise_Time_Inst : Rise_Time 
		port map(
			CLK100			=> CLK100,
			Din				=> Fast_AD,
			RTime_Thr			=> Rtime_Thr,
			Rtime_Upd			=> RTime_Upd,
			RTime_Rej			=> Rtime_Rej,
			RTime			=> RTime );	
	
	-----------------------------------------------------------------------------------------
	CPS_Countrate_Inst : Countrate
		port map(
			CLK100	    	=> CLK100,
			ms100_tc		=> Cps_Ms100_tc, -- ms100tc,
			inc_cps		=> Inc_CPS,
			clear		=> Cmd_Stb( STB_TIME_CLR ),
			enable		=> Time_Enable,
			cps			=> CPS,
			net			=> Net_CPS );
	-----------------------------------------------------------------------------------------
	
	-----------------------------------------------------------------------------------------
	NPS_Countrate_Inst : Countrate
		port map(
			CLK100	    	=> CLK100,
			ms100_tc		=> Cps_Ms100_tc, -- ms100tc,
			inc_cps		=> Inc_NPS,
			clear		=> Cmd_Stb( STB_TIME_CLR ),
			enable		=> Time_Enable,
			cps			=> NPS,
			net			=> Net_NPS );	
	-----------------------------------------------------------------------------------------
	
	-----------------------------------------------------------------------------------------
	TimeCtrl_Inst : TimeCtrl
		port map(
			CLK100			=> CLK100,
			EXT_TRIG			=> Ext_Trig,
			Time_Clr			=> Cmd_Stb( STB_TIME_CLR ),
			Time_Start		=> Cmd_Stb( STB_TIME_START ),
			Time_Stop			=> Cmd_Stb( STB_TIME_STOP ),
			Time_Reset		=> Cmd_Stb( STB_TIME_RESET ),
			BH_LTIME_EN		=> BH_LTime_En, 
			PRESET_MODE		=> CWord( CW_PRESET_MODE_H downto CW_PRESET_MODE_L ),
			PRESET			=> Preset_Val,
			Pixels_Line		=> Pixels_Line,
			Lines_Frame		=> Lines_Frame,
			Spec_Update		=> Spec_Update,
			Time_Enable		=> Time_Enable,
			Preset_FMap		=> Preset_Fmap,
			Preset_SMap		=> Preset_Smap,
			CTIME			=> CTime,
			LTIME			=> LTime,
			Line_Code			=> Line_Code,
			Pixel_Status		=> Pixel_Status,
			Line_Status		=> Line_Status,
			Frame_Status		=> Frame_Status );
	-----------------------------------------------------------------------------------------

--	-----------------------------------------------------------------------------------------
--	SCA_Gen : if( GEN_SCA = 1 ) generate
--		scabb_inst : scabb
--			port map(
--				clk100              => CLK100,
--				Param			=> SCABB_Data,
--				fdisc			=> FDisc_BB,
--				Meas_Done			=> Meas_Done,
--				Time_Enable		=> Time_Enable,
--				SCABB_WEN			=> SCABB_WEN,
--				SCABB_WA			=> SCABB_WA,
--				SCABB_WD			=> SCABB_WD,
--				SCABB_RA			=> SCABB_RA,
--				SCABB_RD			=> SCABB_RD,
--				CPeak			=> CPeak,
--				SCABB_Out			=> SCABB_Out );
--	end generate;
--	
--	SCA_No_Gen : if( GEN_SCA = 0 ) generate
--			SCABB_RD		<= (others=>'0');
--			SCABB_Out		<= '0';
--	end generate;
--	-----------------------------------------------------------------------------------------

		tp_Data(0) 	<= Disc_Level_Out(0);
		tp_data(1) 	<= Disc_Level_Cmp(0);
		tp_Data(2) 	<= Disc_Level_Out(1);
		tp_data(3) 	<= Disc_Level_Cmp(1);
		tp_Data(4) 	<= Disc_Level_Out(2);
		tp_data(5) 	<= Disc_Level_Cmp(2);
		tp_Data(6) 	<= Disc_Level_Out(3);
		tp_data(7) 	<= Disc_Level_Cmp(3);
		tp_Data(8) 	<= Disc_Level_Out(4);
		tp_data(9) 	<= Disc_Level_Cmp(4);
		tp_data(10) 	<= FDisc_BB;
		tp_Data(11) 	<= Inc_CPS;
		tp_data(12) 	<= Fir_Reset;
		tp_data(13) 	<= PA_Busy(iDisc_Main);
		tp_data(14) 	<= Out_Of_Range_Cen;
		tp_data(15) 	<= Blevel_Upd;

		-----------------------------------------------------------------------------------------

		-- Constants - so non-registered --------------------------------------------------------
		Amp_Time_Disc_En(iDisc_Main) 					<= '1';
		-- Constants - so non-registered --------------------------------------------------------
		-----------------------------------------------------------------------------------------

		Cps_Ms100_tc <= '1' when ( CPS_Ms100_Cnt = CPS_Ms100_Cnt_Max ) else '0';
				
		-----------------------------------------------------------------------------------------
		clock100_proc : process( clk100 )
		begin
			if(( clk100'event ) and ( clk100 = '1' )) then	

				if( Cps_Ms100_Tc = '1' )
					then CPS_Ms100_Cnt <= 0;
				elsif( CWord(CW_CPS_RESET_INH) = '0' )
					then CPS_Ms100_Cnt <= CPS_Ms100_Cnt + 1;
				elsif(( CWord(CW_CPS_RESET_INH) = '1' ) and ( PA_Reset_Det = '0' ))
					then CPS_Ms100_Cnt <= CPS_Ms100_Cnt + 1;
				end if;
			
                    if( AmpTime(12) = '1' )        -- 10eV/Ch
                         then evch_sel <= '0';    -- Set 10Ev/Ch         
                         else evch_sel <= '1';    -- Set 5ev/ch
                    end if;
                         
				Disc_Cnts( 40 )			<= Disc_En( iDisc_40 );
				Disc_Cnts( 41 )			<= Disc_En( iDisc_160 );
				Disc_Cnts( 42 )			<= Disc_En( iDisc_320 );
				Disc_Cnts( 43 )			<= Disc_En( iDisc_640 );
				Disc_Cnts( 44 )			<= Disc_En( iDisc_Main );	
				Disc_Cnts( 63 downto 45 ) 	<= (others=>'0');				

				-- Form Discriminator Enable based on Amp Time Selection ----------------------
				for i in iDisc_40 to iDisc_640 loop				
					-- Use 1/2 of the Peak_Time Mux
					if( conv_integer( Peak_Time_Mux( 11 downto 1 ) ) > conv_integer( Peak_time( (i*12)+11 downto i*12 )))
						then Amp_Time_Disc_En(i) <= '1';
						else Amp_Time_Disc_En(i) <= '0';
					end if;					
				end loop;
				-- Form Discriminator Enable based on Amp Time Selection ----------------------
			
				-- Form individual discriminator enables based on Amp Time Selection AND  -----
				--	Control Word Entry not to disable
				for i in iDisc_40 to iDisc_Main loop
					if( ATCWord(ATCW_Disc_Dis_40+i) = '1' )
						then Disc_En(i) <= '0';
						else Disc_En(i) <= AmP_Time_Disc_En(i);
					end if;
				end loop;
				-- Form individual discriminator enables based on Amp Time Selection AND  -----

			end if;
		end process;
		-----------------------------------------------------------------------------------------
		

---------------------------------------------------------------------------------------------------
end behavioral;               -- EDSBlock.VHD 
---------------------------------------------------------------------------------------------------

