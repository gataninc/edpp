## Generated SDC file "AD5453.sdc"

## Copyright (C) 1991-2010 Altera Corporation
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, Altera MegaCore Function License 
## Agreement, or other applicable license agreement, including, 
## without limitation, that your use is for the sole purpose of 
## programming logic devices manufactured by Altera and sold by 
## Altera or its authorized distributors.  Please refer to the 
## applicable agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 10.0 Build 218 06/27/2010 SJ Full Version"

## DATE    "Wed Sep 22 08:19:27 2010"

##
## DEVICE  "EP3C40U484C7"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {CLK100}  -period 10.000 -waveform { 0.000 5.000 } [get_ports {CLK100}]


#**************************************************************
# Create Generated Clock
#**************************************************************


#**************************************************************
# Set Clock Latency
#**************************************************************

#**************************************************************
# Set Clock Uncertainty
#**************************************************************

derive_clock_uncertainty

#**************************************************************
# Set Input Delay
#**************************************************************


set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {PA_Reset}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {PA_Reset}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {PA_Busy}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {PA_Busy}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {BLR_Busy}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {BLR_Busy}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {Fir_Reset}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {Fir_Reset}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {TimeConst[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {TimeConst[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {BLR_Sel[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {BLR_Sel[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {data_abc[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {data_abc[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {offset[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {offset[*]}]

set_input_delay -add_delay -max -clock [get_clocks {CLK100}]  4.33 [get_ports {Gap_Time[*]}]
set_input_delay -add_delay -min -clock [get_clocks {CLK100}]  2.33 [get_ports {Gap_Time[*]}]

#**************************************************************
# Set Output Delay
#**************************************************************                   
		
set_output_delay -clock { CLK100 } -max 5.000 [get_ports {Main_out[*]}]
set_output_delay -clock { CLK100 } -min 3.000 [get_ports {Main_out[*]}]

set_output_delay -clock { CLK100 } -max 5.000 [get_ports {Blevel_Out[*]}]
set_output_delay -clock { CLK100 } -min 3.000 [get_ports {Blevel_Out[*]}]


set_output_delay -clock { CLK100 } -max 5.000 [get_ports {sum[*]}]
set_output_delay -clock { CLK100 } -min 3.000 [get_ports {sum[*]}]

#**************************************************************
# Set Clock Groups
#**************************************************************



#**************************************************************
# Set False Path
#**************************************************************



#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************

