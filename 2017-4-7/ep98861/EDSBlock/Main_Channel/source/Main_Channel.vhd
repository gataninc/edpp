------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	Main_Channel.VHD
------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : November 13, 2009
--   Board          : Artemis Feasability Study
--   Schematic      : 4035.007.25500
--
--   Top Level      : 4035.009.993??, S/W, Artemis, ...
--
--   Description:	
--		Creates the Main Channel Filter
--	
--   History:       <date> - <Author> - Ver
--		December 28, 2011
--			Gap Time increased from 6 bits to 8 bits, 
--			ATCW_GapTime bits changed positions, from 5;0 to 23:16
--			Also effects EDSBlock and Main_Channel and Main_GT_Ram
--		December 12, 2011 - MCS - Main_Channel.vhd
--			Completed TimeConst7 for next TimeConst
--		june 14, 2011 - MCS
--			Doubled GapTime
---		April 1, 2011
--			During PA_RESET or PA_Busy hold FIR_ACC reset
--			otherwise BLR_Disable - Inhibit BLR from updating
--			otherwise allow BLR to update
--		November 13, 2009 - MCS - 1.00 - 
--			Created
---------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;

library altera_mf;
	use altera_mf.altera_mf_components.all;
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

------------------------------------------------------------------------------------
entity Main_Channel is 
	port( 
		CLK100		: in		std_logic;					-- MAster Clock
		PA_Reset		: in		std_logic;
		PA_Busy		: in		std_logic;	
		BLR_Busy		: in		std_logic;
		Fir_Reset		: in		std_logic;
		TimeConst 	: in		std_logic_Vector(   2 downto 0 );
		BLR_Sel		: in		std_logic_Vector(   1 downto 0 );		
		data_abc		: in		std_logic_Vector(  89 downto 0 );	-- First TAP
		offset		: in		std_logic_Vector(  31 downto 0 );
		Gap_Time		: in		std_logic_Vector(   7 downto 0 );
		Main_out		: buffer	std_logic_Vector(  19 downto 0 );	-- FIR Output 
--		Main_out		: buffer	std_logic_Vector(  15 downto 0 );	-- FIR Output 
		Blevel_Out	: buffer	std_logic_Vector(  11 downto 0 );
		sum			: buffer	std_logic_Vector(  31 downto 0 ) );
end Main_Channel;
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
architecture behavioral of Main_Channel is
	-- Gap Time Memory ---------------------------------------------------------------------------
	component MAIN_GT_RAM
		port( 
			clock		: in 	std_logic:= '1';
			wren			: in 	std_logic := '0';
			wraddress		: in 	std_logic_Vector(  7 downto 0 );
			rdaddress		: in 	std_logic_Vector(  7 downto 0 );
			data			: in 	std_logic_Vector( 53 downto 0 );
			q			: out 	std_logic_Vector( 53 downto 0 ) );
	end component;
	-- Gap Time Memory ---------------------------------------------------------------------------
	
	-- Constant Declarations ---------------------------------------------------------------------
	constant Main_Out_LSB 		: integer := 9;
	constant BLR_OUT_LSB		: integer := Main_Out_LSB-2;	-- 1/4 of a Main Channel
	
	constant Fir_Out_LSB		: integer := 8;
	constant BLR_Thr			: integer := 50;

	constant Acc_Width			: integer := 64; -- 4 + 32 + 13 + 8; -- 57;
	constant Sum_Bits			: integer := 32; -- 4+ 20;
	
	constant Dout_LSB			: integer := 11;

	constant Dout_LSB0			: integer := Dout_LSB+4;		--  0.1 us
	-- Constant Declarations ---------------------------------------------------------------------

	-- Signal Declarations -----------------------------------------------------------------------
	signal Blevel_Clamp_Max		: std_logic;
	signal Blevel_Clamp_Min		: std_logic;
	signal BLR_Disable 			: std_Logic;
	signal blr_sum				: std_logic_Vector( 17 downto 0 ):= (others=>'0');
	signal BLR_Offset   		: std_logic_vector( Acc_Width-1 downto 0 ):= (others=>'0');
	signal BLR_SEL_REG			: integer range 0 to 3;
	signal Blr_Acc				: std_logic_Vector( Acc_WIdth-1 downto 0 ):= (others=>'0');
	signal Blr_Out				: std_logic_Vector( Acc_Width-1 downto 0 ):= (others=>'0');
	signal BLR_Mux				: std_logic_Vector( Acc_Width-1 downto 0 ):= (others=>'0');
	signal blr_acc_add			: std_logic_Vector( Acc_Width-1 downto 0 ):= (others=>'0');

	signal data_abc_reg			: std_logic_Vector( 89 downto 0 ):= (others=>'0');	-- First TAP

	signal Fir_Acc	 			: std_logic_vector( Acc_Width-1 downto 0 ):= (others=>'0');
	signal fir_acc_add			: std_logic_vector( Acc_Width-1 downto 0 ):= (others=>'0');
	signal Fir_Acc_mult			: std_logic_vector( Acc_Width-1 downto 0 ):= (others=>'0');
	signal Fir_Acc_Mux 			: std_logic_Vector( Acc_Width-1 downto 0 ):= (others=>'0');
	signal fir_Acc_Sum 			: std_logic_vector( 32 downto 0 );
	signal fir_out				: std_logic_Vector( 31 downto 0 ):= (others=>'0');
	signal fir_radr			: std_logic_vector(  7 downto 0 ):= (others=>'0');
	signal fir_wadr 			: std_logic_vector(  7 downto 0 ):= (others=>'0');
	signal Fir_Offset			: std_logic_Vector( 31 downto 0 ) := (others=>'0');

	signal offset_reg			: std_logic_Vector(  31 downto 0 ):= (others=>'0');

	signal Main_Out_PSlope		: std_logic := '0';
	signal Max_Fir_Cmp			: std_logic := '0';
--	signal Main_Out_Clamp_Max	: std_logic;
--	signal Main_Out_Clamp_Min	: std_logic;

	signal out_vec				: std_logic_Vector( 89 downto 36 ) := (others=>'0');

	signal sub_total1			: std_logic_Vector( Sum_Bits-1 downto 0 ):= (others=>'0');
	signal sub_total2			: std_logic_Vector( Sum_Bits-1 downto 0 ):= (others=>'0');
	
	signal TimeConstInt			: integer range 0 to 7;
	-- Signal Declarations -----------------------------------------------------------------------

	
begin
	-------------------------------------------------------------------------------
	GapTimeProc : process( clk100 )
	begin
		if( rising_edge( clk100 )) then
			TimeConstInt		<= conv_integer( TimeConst );			
			BLR_Sel_Reg 		<= conv_integer( BLR_Sel );
			offset_Reg		<= offset;

			-- Address Counters for Gap Time
			fir_wadr 			<= fir_wadr + 1;
			fir_radr			<= fir_wadr - Gap_Time;
			-- Address Counters for Gap Time

			data_abc_reg		<= data_abc;			
		end if;
	end process;
	-------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Gap TIme of Filter Generation -------------------------------------------------------------
	MAIN_GT_RAM_INST : Main_GT_RAM
		port map(
			clock			=> clk100,
			wren				=> '1',
			wraddress			=> fir_wadr,
			rdaddress			=> fir_radr,
			data				=> data_abc_reg(89 downto 36 ),
			q				=> out_vec );
	-- Gap TIme of Filter Generation -------------------------------------------------------------
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Threshold Comparison for using BLR 
	sum_blr_sub : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 18,
			LPM_DIRECTION		=> "SUB",
			LPM_REPRESENTATION 	=> "SIGNED" )
		port map(
			Cin				=> '1',
			dataa			=> data_abc_reg( 17 downto  0 ),	-- 0PT
			datab			=> out_Vec(  89 downto 72 ),		-- 2PT+GT
			result			=> blr_sum );		
			
	BLR_Disable_Compare : lpm_compare
		generic map(
			LPM_WIDTH			=> 18,
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			dataa			=> blr_sum,
			datab			=> conv_std_logic_Vector( BLR_THR, 18 ),
			agb				=> BLR_Disable );
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Triangular Filter Generation 
	-- 1st Stage
	sub1_add_sub : lpm_add_sub
		generic map(
			LPM_WIDTH			=> Sum_Bits,
			LPM_DIRECTION		=> "SUB",
			LPM_REPRESENTATION 	=> "SIGNED" )
		port map(
			cin				=> '1',
			dataa			=> sxt( data_abc_reg( 17 downto  0 ), sum_bits ),		-- 0PT
			datab			=> sxt( data_abc_reg( 53 downto 36 ), sum_bits ),		-- 1PT
			result			=> sub_total1 );			

	-- 2nd Stage (Gap Time)
	sub2_add_sub : lpm_add_sub
		generic map(
			LPM_WIDTH			=> Sum_Bits,
			LPM_DIRECTION		=> "SUB",
			LPM_REPRESENTATION 	=> "SIGNED" )
		port map(
			cin				=> '1',
			dataa			=> sub_total1,
			datab			=> sxt( out_Vec(  53 downto 36 ), sum_bits ),		-- 1PT + GT
			result			=> sub_total2 );			

	-- Final Stage
	sum_add_Inst : lpm_add_sub
		generic map(
			LPM_WIDTH			=> Sum_Bits,
			LPM_DIRECTION		=> "ADD",
			LPM_REPRESENTATION 	=> "SIGNED",
			LPM_PIPELINE		=> 1 )
		port map(
			clock			=> clk100,
			cin				=> '0',
			dataa			=> sub_total2,			
			datab			=> sxt( out_vec(  89 downto 72 ), sum_bits ),		-- 2PT + GT
			result			=> sum );		
	----------------------------------------------------------------------------------------------
	-- Main Filter Accumulator
	-- Baseline Restorer Parallel Adder ----------------------------------------------------------
	----------------------------------------------------------------------------------------------
	-- fir_acc_add = SumMult + Fir_Acc
	fir_acc_add_Inst : lpm_add_Sub
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_DIRECTION		=> "ADD",
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			cin				=> '0',
			dataa			=> Fir_Acc,			
			datab			=> sxt( sum & x"0000", acc_width ),	-- Mult By 2^16 & Sign Extend 
			result			=> fir_acc_add );	
	-- fir_acc_add = SumMult + Fir_Acc
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	-- Third ( Accumulate ) Stage
	-- Subtract the BLR value ( which is some fraction of Fir_Acc ) from the previous stage's sum
	BLR_Offset_Inst : lpm_add_Sub
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_DIRECTION		=> "SUB",
			LPM_REPRESENTATION	=> "SIGNED")
		port map(
			cin				=> '1',
			dataa			=> fir_acc_add,
			datab			=> BLR_Mux, --fraction of fir_acc 
			result			=> BLR_Offset );
	-- Third ( Accumulate ) Stage
	----------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------
	fir_acc_proc : process( clk100 )
	begin
		if( rising_edge( clk100 )) then		
			------------------------------------------------------------------------------------
			-- Multiplex BLR Select and Amp Time Reg for the BLR Mux feed-back
			if( BLR_SEL_REG = 1 ) then					-- Low BLR Setting
				case ( TimeConstInt ) is				
					when 0		=> BLR_Mux <= sxt( fir_Acc( Acc_Width-1 downto Dout_LSB0   ), 		Acc_Width );
					when 1		=> BLR_Mux <= sxt( fir_Acc( Acc_Width-1 downto Dout_LSB0+1 ), 		Acc_Width );
					when 2		=> BLR_Mux <= sxt( fir_Acc( Acc_Width-1 downto Dout_LSB0+2 ), 		Acc_Width );
					when 3		=> BLR_Mux <= sxt( fir_Acc( Acc_Width-1 downto Dout_LSB0+3 ), 		Acc_Width );
					when 4		=> BLR_Mux <= sxt( fir_Acc( Acc_Width-1 downto Dout_LSB0+4 ), 		Acc_Width );
					when 5		=> BLR_Mux <= sxt( fir_Acc( Acc_Width-1 downto Dout_LSB0+5 ), 		Acc_Width );
					when 6		=> BLR_Mux <= sxt( fir_Acc( Acc_Width-1 downto Dout_LSB0+6 ), 		Acc_Width );
					when 7		=> BLR_Mux <= sxt( fir_Acc( Acc_Width-1 downto Dout_LSB0+7 ), 		Acc_Width );
					when others 	=> BLR_Mux <= (others=>'0');
				end case;
			elsif( BLR_SEL_REG = 2 ) then						-- Medium BLR Setting
				case ( TimeConstInt ) is				
					when	0 		=> BLR_Mux <= sxt( fir_Acc( Acc_Width-1 downto Dout_LSB0   ) & x"0", 	Acc_Width );
					when 1 		=> BLR_Mux <= sxt( fir_Acc( Acc_Width-1 downto Dout_LSB0+1 ) & x"0", 	Acc_Width );
					when 2		=> BLR_Mux <= sxt( fir_Acc( Acc_Width-1 downto Dout_LSB0+2 ) & x"0", 	Acc_Width );
					when 3		=> BLR_Mux <= sxt( fir_Acc( Acc_Width-1 downto Dout_LSB0+3 ) & x"0", 	Acc_Width );
					when 4		=> BLR_Mux <= sxt( fir_Acc( Acc_Width-1 downto Dout_LSB0+4 ) & x"0", 	Acc_Width );
					when 5		=> BLR_Mux <= sxt( fir_Acc( Acc_Width-1 downto Dout_LSB0+5 ) & x"0", 	Acc_Width );
					when 6		=> BLR_Mux <= sxt( fir_Acc( Acc_Width-1 downto Dout_LSB0+6 ) & x"0", 	Acc_Width );
					when 7		=> BLR_Mux <= sxt( fir_Acc( Acc_Width-1 downto Dout_LSB0+7 ) & x"0", 	Acc_Width );
					when others 	=> BLR_Mux <= (others=>'0');
				end case;			
			elsif( BLR_SEL_REG = 3 ) then						-- High BLR Setting
				case ( TimeConstInt ) is							
					when 0		=> BLR_Mux <= sxt( fir_Acc( Acc_Width-1 downto Dout_LSB0   ) & x"00", 	Acc_Width );
					when 1		=> BLR_Mux <= sxt( fir_Acc( Acc_Width-1 downto Dout_LSB0+1 ) & x"00", 	Acc_Width );
					when 2		=> BLR_Mux <= sxt( fir_Acc( Acc_Width-1 downto Dout_LSB0+2 ) & x"00", 	Acc_Width );
					when 3		=> BLR_Mux <= sxt( fir_Acc( Acc_Width-1 downto Dout_LSB0+3 ) & x"00", 	Acc_Width );
					when 4		=> BLR_Mux <= sxt( fir_Acc( Acc_Width-1 downto Dout_LSB0+4 ) & x"00", 	Acc_Width );
					when 5		=> BLR_Mux <= sxt( fir_Acc( Acc_Width-1 downto Dout_LSB0+5 ) & x"00", 	Acc_Width );
					when 6		=> BLR_Mux <= sxt( fir_Acc( Acc_Width-1 downto Dout_LSB0+6 ) & x"00", 	Acc_Width );
					when 7		=> BLR_Mux <= sxt( fir_Acc( Acc_Width-1 downto Dout_LSB0+7 ) & x"00", 	Acc_Width );
					when others 	=> BLR_Mux <= (others=>'0');
				end case;	
			else
				BLR_Mux <= (others=>'0');
			end if;
			-- Multiplex BLR Select and Amp Time Reg for the BLR Mux feed-back
			------------------------------------------------------------------------------------	

			------------------------------------------------------------------------------------	
			-- Main Filter Accumulator
			-- When to "Inhibit" BLR Update
			if(( BLR_Disable = '1' ) or ( BLR_SEL_REG = 0 ) or ( PA_Reset = '1' ) or ( PA_Busy = '1' ) or ( Fir_Reset = '1' ) or ( BLR_Busy = '1' ))
				then Fir_Acc	<= Fir_Acc_Add;
								
			-- Otherwise normal accumulation
				else Fir_Acc	<= Blr_Offset;
			end if;
			------------------------------------------------------------------------------------
		
			------------------------------------------------------------------------------------
			-- Pipeline Output Mux for FIR_Out
			case( TimeConstInt ) is
				when 0 		=> fir_out <= fir_acc( 31+Dout_LSB + 0 downto Dout_LSB + 0 );
				when 1 		=> fir_out <= fir_acc( 31+Dout_LSB + 1 downto Dout_LSB + 1 );
				when 2 		=> fir_out <= fir_acc( 31+Dout_LSB + 2 downto Dout_LSB + 2 );
				when 3 		=> fir_out <= fir_acc( 31+Dout_LSB + 3 downto Dout_LSB + 3 );
				when 4 		=> fir_out <= fir_acc( 31+Dout_LSB + 4 downto Dout_LSB + 4 );
				when 5 		=> fir_out <= fir_acc( 31+Dout_LSB + 5 downto Dout_LSB + 5 );
				when 6 		=> fir_out <= fir_acc( 31+Dout_LSB + 6 downto Dout_LSB + 6 );
				when 7 		=> fir_out <= sxt( fir_Acc_Sum( 32 downto 2 ), 32 );					-- Add 25%
				when others 	=> fir_out <= fir_acc( 31+Dout_LSB + 7 downto Dout_LSB + 7 ); 
			end case;
				
			-- Pipeline Mux for Fir_Out		
			------------------------------------------------------------------------------------												
		end if;
	end process;
	----------------------------------------------------------------------------------------------			
	
	-- Increase fir_acc for Peak Time 7 by 37.5%
	Main_PAdder_Inst : LPM_ADD_SUB
		generic map(
			LPM_WIDTH			=> 33,
			LPM_REPRESENTATION 	=> "SIGNED",
			LPM_DIRECTION		=> "ADD",
			LPM_PIPELINE		=> 1 )
		port map(
			clock			=> clk100,
			Cin				=> '0',
			dataa			=> sxt( fir_acc( 31+Dout_LSB + 7 downto Dout_LSB + 7 ), 33 ),		-- x1
			datab			=> sxt( fir_acc( 31+Dout_LSB + 5 downto Dout_LSB + 5 ), 33 ),		-- x1/4
			result			=> fir_Acc_sum );				
	
	-------------------------------------------------------------------------------
	-- Now Add the Offset to FIR_Out
	--         3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1
	--	      1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
	--         - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	--         2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1                     0 0
	-- Dataa   3 3 3 3 3 3 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 0 0  4x Fir_Out sign extended to 32 bits
	--         - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	--         3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1
	-- Datab   1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0  Offset 
	--         - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
	-- Result               |<--- Main Out ---------------->|
	-- Blevel_Out                         |<--- Blevel Out ------>| 
	--	However, Blevel Out is mapped to fir_out bit 4 so it is immune to any offset adjustments.
	Offset_Adder : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_DIRECTION		=> "ADD" )
		port map(
			cin				=> '0',
			dataa 			=> Fir_Out,	
			datab 			=> Offset_reg,	
			result			=> Fir_Offset );
	-------------------------------------------------------------------------------
	

	-------------------------------------------------------------------------------
	-- Test Fir_out with respect to Blevel to clamp at Max or Min
	blevel_clamp_max_compare : lpm_compare
		generic map(
			LPM_WIDTH			=> 32-BLR_OUT_LSB,
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			dataa 			=> Fir_Out( 31 downto BLR_OUT_LSB),
			datab			=> conv_std_logic_vector( 2047,  32-BLR_OUT_LSB ),
			agb				=> Blevel_Clamp_Max );
			
	blevel_clamp_min_compare : lpm_compare
		generic map(
			LPM_WIDTH			=> 32-BLR_OUT_LSB,
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			dataa 			=> Fir_Out( 31 downto BLR_OUT_LSB),
			datab			=> conv_std_logic_vector( -2048,  32-BLR_OUT_LSB ),
			alb				=> Blevel_Clamp_Min );			
	-- Test Fir_out with respect to Blevel to clamp at Max or Min
	-------------------------------------------------------------------------------
	
	-------------------------------------------------------------------------------
	-- Clamp Main Channel Outut at 4000
--	Main_Clamp_Max_Compare : lpm_compare
--		generic map(
--			LPM_WIDTH			=> 32-Main_Out_LSB,
--			LPM_REPRESENTATION => "SIGNED" )
--		port map(
--			dataa			=> Fir_Offset( 31 downto Main_Out_LSB ),
--			datab			=> conv_Std_logic_vector( 4095, 32-Main_Out_LSB),
--			agb				=> Main_Out_Clamp_Max );
--
--	Main_Clamp_Min_Compare : lpm_compare
--		generic map(
--			LPM_WIDTH			=> 32-Main_Out_LSB,
--			LPM_REPRESENTATION => "SIGNED" )
--		port map(
--			dataa			=> Fir_Offset( 31 downto Main_Out_LSB ),
--			datab			=> conv_Std_logic_vector( -4096, 32-Main_Out_LSB),
--			alb				=> Main_Out_Clamp_Min );			
	-------------------------------------------------------------------------------
	
     -------------------------------------------------------------------------------
	Post_Process : process( CLK100 )
	begin
		if(( CLK100'event ) and ( CLK100 = '1' )) then	
		
			-------------------------------------------------------------------------------
			if( Blevel_Clamp_Max = '1' )
				then Blevel_out <= conv_Std_logic_Vector( 2047, 12 );
			elsif( Blevel_Clamp_Min = '1' )
				then Blevel_Out <= conv_Std_logic_Vector( -2048, 12 );
				else Blevel_Out <= Fir_Out( BLR_OUT_LSB+11 downto BLR_OUT_LSB);
			end if;

			-- "Clamp off the ouptut" during a reset (just for DSS view)
			if(( PA_Reset = '1' ) or ( PA_Busy = '1' )) 
				then Main_Out 	<= (others=>'0');
				else Main_Out	<= Fir_Offset( Main_Out_LSB+19 downto Main_Out_LSB );
			end if;
--			elsif( Main_Out_Clamp_Max = '1' )
--				then Main_Out 	<= conv_Std_logic_Vector( 4095, 16 );		
--			elsif( Main_Out_Clamp_Min = '1' )
--				then Main_Out 	<= conv_Std_logic_Vector( -4096, 16 );		
--				else Main_Out	<= Fir_Offset( Main_Out_LSB+15 downto Main_Out_LSB );
--			end if;
			-- Pipeline Fir_Out for Blevel_out 
			-------------------------------------------------------------------------------
		end if;
	end process;

------------------------------------------------------------------------------------
end behavioral;               -- Main_Channel.vhd
------------------------------------------------------------------------------------
			
