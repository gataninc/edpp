
------------------------------------------------------------------------------------
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

entity tb is
end tb;

architecture test_bench of tb is

	signal imr			: std_logic := '1';					-- Master Reset
	signal CLK100			: std_logic := '0';			-- MAster Clock
	signal Filter_Shape		: std_logic_Vector( 1 downto 0 );
	signal PA_Busy			: std_logic;
	signal Fir_Reset		: std_logic;
	signal Max_Clr			: std_logic;
	signal TimeConst 		: std_logic_Vector(   2 downto 0 );
	signal BLR_Sel			: std_logic_Vector(   1 downto 0 );		
	signal data_abc		: std_logic_Vector( 143 downto 0 );	-- First TAP
	signal fgain			: std_logic_vector(  31 downto 0 );
	signal offset			: std_logic_Vector(  31 downto 0 );
	signal Gap_Time		: std_logic_Vector(   5 downto 0 );
	signal Main_out		: std_logic_Vector(  15 downto 0 );	-- FIR Output 
	signal Max_Fir			: std_logic_vector(  15 downto 0 );
	signal Blevel_Out		: std_logic_Vector(  11 downto 0 );
	signal WE_Fir_Mr		: std_logic;
	signal PA_Reset		: std_Logic;
	signal adata			: std_logic_Vector(   15 downto 0 );		-- A/D Input Data
	signal Peak_Time		: std_Logic_Vector(  143 downto 48 );
	signal Peak_Time_Mux	: std_Logic_Vector(   11 downto  0 );
	signal Dout			: std_logic_Vector(  143 downto  0 );	-- Delayed Output Data
	signal Disc_En			: std_logic;
	signal Time_Clr		: std_logic;
	signal Time_Enable		: std_logic;
	signal tlevel			: std_logic_vector( 15 downto 0 );	-- Discriminator Level
	signal Level_Out		: std_logic;					-- Discriminator Level Exceeded	
	signal Level_Cmp		: std_logic;
	signal fir_out			: std_logic_Vector( 15 downto 0 ); 	-- FIR Output ( for Debug )
	signal Disc_Cnts		: std_logic_vector(  7 downto 0 );
	signal NSlope_Cmp		: std_logic_vector( 1 downto 0 );
	signal PSlope_Cmp		: std_logic_Vector( 1 downto 0 );
	signal pslope			: std_logic;

	-------------------------------------------------------------------------------
	component Disc_BLM is 
		port( 
			imr				: in		std_logic;					-- Master Reset
			CLK100			: in		std_logic;					-- MAster Clock
			PA_Busy			: in		std_logic;
			Fir_Reset			: in		std_logic;
			Disc_En			: in		std_logic;
			Time_Clr			: in		std_logic;
			Time_Enable		: in		std_logic;
			TimeConst			: in		std_logic_vector(  2 downto 0 );
			data_abc			: in		std_logic_vector( 79 downto 0 );
			tlevel			: in		std_logic_vector( 15 downto 0 );	-- Discriminator Level
			Level_Out			: buffer	std_logic;					-- Discriminator Level Exceeded	
			Level_Cmp			: buffer	std_logic;
			fir_out			: buffer	std_logic_Vector( 15 downto 0 ); 	-- FIR Output ( for Debug )
			Disc_Cnts			: buffer	std_logic_vector(  7 downto 0 );
			NSlope_Cmp		: buffer	std_logic_vector( 1 downto 0 );
			PSlope_Cmp		: buffer	std_logic_Vector( 1 downto 0 );
			pslope			: buffer	std_logic );
	end component Disc_BLM;
	-------------------------------------------------------------------------------

	------------------------------------------------------------------------------------
	component Main_Channel is 
		port( 
			imr			: in		std_logic;					-- Master Reset
			CLK100		: in		std_logic;					-- MAster Clock
			Filter_Shape	: in		std_logic_Vector( 1 downto 0 );
			PA_Busy		: in		std_logic;
			Fir_Reset		: in		std_logic;
			Max_Clr		: in		std_logic;
			TimeConst 	: in		std_logic_Vector(   2 downto 0 );
			BLR_Sel		: in		std_logic_Vector(   1 downto 0 );		
			data_abc		: in		std_logic_Vector( 143 downto 0 );	-- First TAP
			fgain		: in		std_logic_vector(  31 downto 0 );
			offset		: in		std_logic_Vector(  31 downto 0 );
			Gap_Time		: in		std_logic_Vector(   5 downto 0 );
			Main_out		: buffer	std_logic_Vector(  15 downto 0 );	-- FIR Output 
			Max_Fir		: buffer	std_logic_vector(  15 downto 0 );
			Blevel_Out	: out	std_logic_Vector(  11 downto 0 ) );
	end component Main_Channel;
	------------------------------------------------------------------------------------

	-------------------------------------------------------------------------------------------
	component Main_RAM is 
		port( 
			imr            : in      std_logic;                    	-- Master Reset
			CLK100		: in		std_logic;					-- Master Clock
			WE_Fir_Mr		: in		std_logic;
			PA_Reset		: in		std_Logic;
			adata		: in		std_logic_Vector(   15 downto 0 );		-- A/D Input Data
			TimeConst		: in		std_logic_Vector(    2 downto 0 );
			Fir_Reset		: buffer	std_logic;
			Peak_Time		: buffer	std_Logic_Vector(  143 downto 48 );
			Peak_Time_Mux	: buffer	std_Logic_Vector(   11 downto  0 );
			Dout			: buffer	std_logic_Vector(  143 downto  0 ) );	-- Delayed Output Data
	end component Main_RAM;
	-------------------------------------------------------------------------------------------	
begin
	imr 		<= '0' after 555 ns;
	clk100 	<= not( clk100 ) after 5 ns;


	------------------------------------------------------------------------------------
	RAM : Main_RAM 
		port map(
			imr			=> imr,
			CLK100		=> clk100,
			WE_Fir_Mr		=> We_Fir_Mr,
			PA_Reset		=> PA_Reset,
			adata		=> adata,
			TimeConst		=> TimeConst,
			Fir_Reset		=> Fir_Reset,
			Peak_Time		=> Peak_Time,
			Peak_Time_Mux	=> Peak_time_Mux,
			Dout			=> data_abc );
	-------------------------------------------------------------------------------
	ub : Disc_BLM 
		port map(
			imr			=> imr,
			CLK100		=> clk100,
			PA_Busy		=> PA_Reset,
			Fir_Reset		=> Fir_Reset,
			Disc_En		=> Disc_En,
			Time_Clr		=> Time_Clr,
			Time_Enable	=> Time_Enable,
			TimeConst 	=> TimeConst,
			data_abc		=> data_abc( 79 downto 0 ),
			tlevel		=> Tlevel,
			Level_Out		=> Level_out,
			Level_Cmp		=> Level_Cmp,
			fir_out		=> Fir_out,
			Disc_Cnts		=> Disc_Cnts,
			NSlope_Cmp	=> NSlope_Cmp,
			PSlope_Cmp	=> PSlope_Cmp,
			pslope		=> PSlope );
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	U : Main_Channel
		port map(
			imr			=> imr,
			CLK100		=> clk100,
			Filter_Shape	=> Filter_Shape,
			PA_Busy		=> PA_Reset,
			Fir_Reset		=> Fir_Reset,
			Max_Clr		=> Max_Clr,
			TimeConst 	=> TimeConst,
			BLR_Sel		=> BLr_Sel,
			data_abc		=> data_abc,
			fgain		=> fgain,
			offset		=> Offset,
			Gap_Time		=> Gap_Time,
			Main_out		=> Main_Out,
			Max_Fir		=> Max_fir,
			Blevel_Out	=> Blevel_Out );
	------------------------------------------------------------------------------------

	------------------------------------------------------------------------------------
	ad_proc : process
	begin
		Max_Clr	<= '0';
		adata 	<= x"0000";
		forever_loop : loop
			wait for 5 us;
			wait until rising_edge( clk100 );
			wait for 5 ns;
			adata <= adata + 100;
			Max_Clr <= '1';
			wait until rising_edge( clk100 );
			wait for 5 ns;
			Max_Clr	<= '0';
			wait for 15 us;
		end loop;
	end process;
			
	------------------------------------------------------------------------------------
	tb_proc : process
	begin
		Disc_En			<= '1';
		Time_Clr			<= '0';
		Time_Enable		<= '0';
		tlevel			<= conv_Std_logic_Vector( 10, 16 );
		fgain 			<= conv_std_Logic_Vector( 16384, 32 );
		Offset 			<= (others=>'0');
		Gap_Time			<= conv_std_logic_vector( 16, 6 );
		BLR_Sel			<= "11";
		Filter_Shape		<= "00";
		WE_Fir_Mr			<= '0';
		PA_Reset			<= '0';
		TimeConst			<= conv_Std_logic_Vector( 6, 3 );
		PA_Busy			<= '0';
		PA_Reset 			<= '0';
		assert false
			report "Generate a Triangular Filter"
			severity note;
		wait for 100 us;
		assert false
			report "Generate a 2-Slope Cusp Filter"
			severity note;
		Filter_Shape <= "01";
		wait for 100 us;
		assert false
			report "Generate a 4-Slope Cusp Filter"
			severity note;
		Filter_Shape <= "10";
		wait for 100 us;
		
		assert false
			report "end of simulation"
			severity failure;			
	end process;

end test_bench;
			
