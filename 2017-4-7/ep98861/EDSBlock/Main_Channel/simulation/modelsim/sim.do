vcom -reportprogress 300 -work work {Main_Channel.vho}
vcom -reportprogress 300 -work work {../../../Main_Ram/simulation/modelsim/Main_Ram.vho}
vcom -reportprogress 300 -work work {../../../Disc_BLM/simulation/modelsim/Disc_BLM.vho}
vcom -reportprogress 300 -work work {tb.vhd}
vsim -sdftyp {/U=Main_Channel_vhd.sdo} -sdftyp {/RAM=../../../Main_Ram/simulation/modelsim/Main_Ram_vhd.sdo} -sdftyp {/ub=../../../Disc_BLM/simulation/modelsim/Disc_BLM_vhd.sdo} work.tb
do wave.do
run 1 ms
