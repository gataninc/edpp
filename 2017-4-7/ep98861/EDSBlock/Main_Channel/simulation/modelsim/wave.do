onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix hexadecimal /tb/imr
add wave -noupdate -format Logic -radix hexadecimal /tb/clk100
add wave -noupdate -format Literal -radix hexadecimal /tb/filter_shape
add wave -noupdate -format Logic -radix hexadecimal /tb/pa_busy
add wave -noupdate -format Logic -radix hexadecimal /tb/fir_reset
add wave -noupdate -format Logic -radix hexadecimal /tb/max_clr
add wave -noupdate -format Logic -radix hexadecimal /tb/we_fir_mr
add wave -noupdate -format Logic -radix hexadecimal /tb/pa_reset
add wave -noupdate -format Literal -radix hexadecimal /tb/timeconst
add wave -noupdate -format Literal -radix hexadecimal /tb/peak_time_mux
add wave -noupdate -format Literal -radix hexadecimal /tb/peak_time
add wave -noupdate -format Literal -radix hexadecimal /tb/blr_sel
add wave -noupdate -format Literal -radix hexadecimal /tb/adata
add wave -noupdate -format Literal -radix hexadecimal /tb/data_abc
add wave -noupdate -format Literal -radix hexadecimal /tb/fgain
add wave -noupdate -format Literal -radix hexadecimal /tb/offset
add wave -noupdate -format Literal -radix hexadecimal /tb/gap_time
add wave -noupdate -format Analog-Step -height 74 -max 95.0 -min -32.0 -radix decimal /tb/main_out
add wave -noupdate -format Literal -radix unsigned /tb/max_fir
add wave -noupdate -format Literal -radix unsigned /tb/blevel_out
add wave -noupdate -format Logic -radix unsigned /tb/level_out
add wave -noupdate -format Logic -radix unsigned /tb/level_cmp
add wave -noupdate -format Analog-Step -height 74 -max 100.0 -min -100.0 -radix decimal /tb/fir_out
add wave -noupdate -format Logic -radix unsigned /tb/NSlope_Cmp
add wave -noupdate -format Logic -radix unsigned /tb/PSlope_Cmp
add wave -noupdate -format Logic -radix unsigned /tb/pslope
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {9088119 ps} 0}
configure wave -namecolwidth 121
configure wave -valuecolwidth 53
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {4316160 ps} {14785144 ps}
