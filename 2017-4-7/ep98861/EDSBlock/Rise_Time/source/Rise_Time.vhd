----------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	Rise_Time.VHD
----------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : June 15, 2011
--   Board          : Artemis DPP
--   Schematic      : 4035.007.25500 
--
--   Top Level      : 4035.009.98861, S/W, Artemis, Dpp
--
--	Description:	
--		This module contains monitors the A/D input and measures the step rise time
--   History:
-------------------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
	

library altera_mf;
	use altera_mf.altera_mf_components.all;

	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

-------------------------------------------------------------------------------------------
entity Rise_Time is 
	port( 
		CLK100		: in		std_logic;					-- Master Clock
		Din			: in		std_logic_Vector( 17 downto 0 );	-- A/D Input Data
		RTime_Thr		: in		std_logic_Vector( 31 downto 0 );
		Rtime_Upd		: buffer	std_logic;
		RTime_Rej		: buffer	std_logic;
		RTime		: buffer	std_logic_Vector( 31 downto 0 ) );
end Rise_Time;
-------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------
architecture behavioral of Rise_Time is
	
	-- Constant Declarations -------------------------------------------------------------
	-- Constant Declarations -------------------------------------------------------------
	
	-- Signal Declarations ---------------------------------------------------------------
	signal Din_Del1		: std_logic_vector( 17 downto 0 );
	signal Din_Del2		: std_logic_vector( 17 downto 0 );
	signal Din_Diff		: std_logic_vector( 17 downto 0 );
	
	signal Din_PSlope		: std_logic;
	signal Din_PSlope_Del	: std_logic;
	signal Rtime_Cnt		: std_logic_Vector( 31 downto 0 );
	signal Rtime_Rej_Cmp	: std_logic;
	-- Signal Declarations ---------------------------------------------------------------
	
begin
	Din_Diff_Inst : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 18,
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_DIRECTION		=> "SUB",
			LPM_PIPELINE		=> 1 )
		port map(
			clock			=> CLK100,
			cin				=> '1',
			dataa			=> Din_Del1,
			datab			=> Din_Del2,
			result			=> Din_Diff );
		
	Din_PSlope_Inst : lpm_compare
		generic map(
			LPM_WIDTH			=> 18,
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_PIPELINE		=> 1 )
		port map(
			clock			=> CLK100,
			dataa			=> Din_Diff,
			datab			=> conv_std_logic_vector( 2, 18 ),
			agb				=> Din_PSlope);
			
	RTime_Rej_Inst : lpm_compare
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			dataa			=> Rtime_Cnt,
			datab			=> RTime_Thr,
			agb				=> Rtime_Rej_Cmp );

	--------------------------------------------------------------------------------------
	clock_proc : process( CLK100 )
	begin
		if( rising_edge( CLK100 )) then
			Din_Del1		<= Din;
			Din_Del2		<= Din_Del1;
			Din_PSlope_Del <= Din_PSlope;

			-- Count the pulse width of the positive slopes
			if( Din_PSlope = '1' )
				then RTime_Cnt <= RTime_Cnt + 1;
				else RTime_Cnt <= (others=>'0');
			end if;
			
			-- When the positive slope goes away and the count > 2, update RTime
			if(( Din_PSlope = '0' ) and ( Din_PSlope_Del = '1' ) and ( conv_integer( RTime_Cnt ) > 2 )) then
				RTime 	<= RTime_Cnt;
				RTime_Upd <= '1';
				RTime_Rej	<= Rtime_Rej_Cmp;
			else
				RTime_Upd <= '0';
				RTime_Rej <= '0';				
			end if;
			
		end if;
	end process;
	--------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------
end behavioral;               -- Rise_Time
-------------------------------------------------------------------------------------------