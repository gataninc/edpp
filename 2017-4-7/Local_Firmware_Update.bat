@echo off
REM REV C
echo ***************************************************************
echo REV C
echo ***************************************************************
echo Copy Altera FPGA Configuration File
copy ep98861\source\ep98861.rpd config_files\*.*
echo Copy DSP configuration file
copy ep98841\Release\ep98841.hex config_files\*.*
echo
echo Copy Xilinx configuration file
copy ep98851\ep98851.bit config_files\*.*
echo
echo convert .BIT file to .MCS file
cd config_files
promgen -w -p mcs -u 0 ep98851.bit -o ep98851.mcs
cd ..
pause
@echo off