September 20, 2011
	Removed: pa_adc3_curr and pa_adc5_curr, use TEC1_Current and TEC2_Current respectively
	Filtering tab, ADC Spread Spectrum Enable, WeinerFilter Enable and Delay (ns) are disabled
	Needs new firmware, Altera, Xilinx, DSP and TEC dated 9/20/11
	TEC Board needs firmware version based on board rev.
	Current Rev uses file ep98881-rev1.0.pof
	Rev 1 with rework uses file ep98881-rev 1.1.pof
	future Rev 2 uses file ep98881- rev 2.0.pof


folder: ep98841 is the DSP code used by the MityDSP. There are two sub-folders: "Debug" and "Release". 
	The run-time code resides in the "Release" folder.
folder: ep98851 is the Xilinx code used by the MityDSP. As a result of the Xilinx tools, a ep98851.bit file is left in the 
	working directory. The "_firmware_update.bat" file will copy that file to the "config_files" folder and then 
	run another Xilinx Application to translate the .bit file to the .mcs file.
folder: ep099861 is the run-time Altera FPGA for the eDPP board. When a synthesis is complete, a "create programming" file
	needs to be run from within Quartus using the ep98861.coff file which results in the ep98861.rpd file.

The batch file _firmware_update.bat will copy the approprite files to the ..\config_files folder and then run what-ever 
necessary translations. 
