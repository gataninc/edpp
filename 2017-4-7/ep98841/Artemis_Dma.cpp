/***
 * \file TelnetEchoExample.cpp
 *
 * This file implements the tcArtemisDsp class.
 *
 *  \author  Critical Link LLC
 *
 *  \date    8/2009
 *
 *  \remarks
 *
 *  \verbatim

      o  0
      | /       Copyright (c) 2009
     (CL)---o   Critical Link, LLC
       \
        O

    \endverbatim
 **/
#ifdef MITYDSP_HEADER_BLOCK
/**************************************************************************/
/*                                                                        */
/*  $------------------------------------------------------------------$  */
/*                                                                        */
/*  $Workfile:: DspCameraLink.cpp                                      $  */
/*                                                                        */
/*  $------------------------------------------------------------------$  */
/*                                                                        */
/*  $Description::                                                     $  */
/*                                                                        */
/*  $------------------------------------------------------------------$  */
/*                                                                        */
/*     o  0                                                               */
/*     | /       Copyright (c) 2007-2008                                  */
/*    (CL)---o   Critical Link, LLC                                       */
/*      \                                                                 */
/*       O                                                                */
/**************************************************************************/
#endif

#include <stdlib.h>
#include <string.h>

#include "core/DspError.h"
#include "core/DspIntDispatch.h"
#include "Artemis_Dma.h"
#include "Artemis_DmaRegMap.h"
#include "ep98841.h"

#include "core/DspFirmware.h"
//include "core/DspCameraLink.h"
//include "core/private/DspCameraLinkRegMap.h"
#include <csl_emif.h>
#include <tsk.h>
#include <mbx.h>

using namespace MityDSP;

static long _TimeBuffer[TimeBufferSize];
static int _TimeBufferPtr = 0;
static float _TimeBuffer_Period;
static float _TimeBuffer_Start;
static bool _TimeBuffer_Enable = FALSE;

typedef struct {
	unsigned int DataOffset;
	unsigned int Len;
} tsDmaMessage;

MBX_Handle DmaIntMbox = MBX_create(sizeof(tsDmaMessage), 64, NULL);


/////////////////////////////////////////////////////////////////////////////
///
/// @page tcDspCameraLink_sec tcDspCameraLink
///
/// @section intro_sec Introduction
///
/// The tcDspCameraLink class is used to provide a MityDSP interface to 
/// camera imaging hardware utilizing the LVDS Camera Link protocol.
///
/// Typically, a specific camera handler class (e.g., a hamamatsu camera) is
/// derived fromthe tcDspCameraLink in order to provide any custom configuration
/// protocol using the Camera Link asynchronous serial interface for setting
/// up the camera framerate, size, etc.
///
/// @sa MityDSP::tcDspCameraLink Class Reference
///
/// @section example_sec Example
/// This is a simple example of tcDspCameraLink interfacing to a QVGA monochrome
/// camera:
///
/// @code
/// {
///     tcDspCameraLink *mpCameraLink;
///     short* mpBuffer[240*320];
///     MBX_Handle mhLinkMbox = MBX_create(sizeof(tcDspCameraLink::tsFrameDataNotify),10,NULL);
///     tcDspCameraLink::tsFrameDataNotify lsFrameData;
///
///     // create tcDspTouchScreen instance
///     mpCameraLink = new tcCameraLink((void *)0xB0000280);
///     mpCameraLink->SetImageSize(240,320);
///     mpCameraLink->SetPack(true);
///
///     // Ask for a frame of data
///     printf("Capturing 1 frame of data\n");
///     mpCameraLink->CaptureFrames(mpBuffer,
///                                 240*320*sizeof(short),
///                                 mhLinkMbox,
///                                 1,
///                                 1);
///
///     int RV = MBX_pend(mhLinkMbox,&lsFrameData,SYS_FOREVER);
///
///     if (RV)
///     {
///        short* lpFrameData = lsFrameData.mpStart;  // should be the same as mpBuffer in this example
///
///        // process the frame data....
///     }
///
///
/// } @endcode
///
/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
///
/// This constructor is used to open a DspCameraLink interface with
/// the specified settings (optional).  The interface is not
/// yet enabled for capture following construction.
///
/// @param[in] apAddress    Base Address of ADC core
/// @param[in] anThreadPrio Priority to run capture thread, used to
///                         parse DMA data from link.
/// @param[in] anFrameRows  Number of rows per frame
/// @param[in] anFrameCols  Number of cols per frame
///
/// @return None.
///
/// @sa DspCameraLink.h
///
/////////////////////////////////////////////////////////////////////////////
//tcDspCameraLink::tcDspCameraLink(void *apAddress, int anThreadPrio);  //MIKE: threadprio should be the priority of the DMA reader thread which should be just under the network stack priorities.
tcArtemis_Dma::tcArtemis_Dma(void *apAddress, int anThreadPrio) :  //MIKE: threadprio should be the priority of the DMA reader thread which should be just under the network stack priorities.
   mnThreadPrio(anThreadPrio)                                   
 ,  mnInterruptCount(0)
 ,  mbFpgaDmaEnabled(false)
{
		mbFpgaDmaEnabled = false;
		tuFirmwareVersion version = tcDspFirmware::get_version_info(apAddress);  

		mnDppConnectedSocket = -1;

	    // set base register, level and default vector
    	mpBaseAddr = (unsigned int *)apAddress;

		// Manually set the Level and Vector to Match the core
		mnMyIntLevel = 5;
		mnMyIntVector = 3;

	    // print version info, then push base address past the version register
	    tcDspFirmware::print_version_info(apAddress);

	    // disable Link and interrupts
		mpBaseAddr[XA_IADR_DMA_EN_REG] &= ~( XILINX_DMA_EN | XILINX_ALTERA_INT_EN );
		mpBaseAddr[XA_IADR_WR_FIFO_MR] 	= 0x0;
		mpBaseAddr[XA_IADR_DMA_IRQ_CLR] = ( XILINX_DMA_CLR | XILINX_ALTERA_INT_CLR );

	    msL2Mode = CACHE_setL2Mode(CACHE_0KCACHE);             //MIKE: setup / configure the DSP cache
	    CACHE_setL2Mode(msL2Mode);

	   // catch invalid level info
    	if ((mnMyIntLevel < gnFirstLevel) || (mnMyIntLevel >= (gnFirstLevel + gnNumLevels)))
    	{
        	mnMyIntMask = 0;
        	tcDspError::report(__FILE__, __LINE__, warning,
        	                   "Invalid Interrupt Level or Auto-Level failed (%1d:%1d)",
            	               mnMyIntLevel, mnMyIntVector);
	    }
		else	// otherwise, install the ISR
    	{
        	// compute interrupt mask
        	mnMyIntMask = 1 << mnMyIntLevel;

			//MIKE: here is where the isr callback gets registered with our interrupt dispatcher.
			// we pass in the this pointer so the ISR can use it to regain context back into the appropriate C++ class	 		 
        	tcDspInterruptDispatch::register_isr_callback
        	    (mnMyIntLevel, mnMyIntVector, interrupt_dispatch, (Arg)this);
    	}

    	// initialize the semaphore to data traffic handling
//    	mhDataSem = SEM_create(0,NULL);

		mbFpgaDmaEnabled = true;

        // Disable "NoHold" in EMIF Global Control Register (clear 0x00000080 in 0x01800000)
		EMIF_FSETS(GBLCTL, NOHOLD, DISABLE);

		//MIKE: this is where we launch the thread that is going to catch the data that the DMA engine has 
		// pushed to us. It will be the thread that waits on the semaphore and calls the ethernet stack 
		//to write the data up to the PC.
			 
    	// spawn the listener thread
    	TSK_Attrs attrs = TSK_ATTRS;
    	attrs.name = "ARTEMIS_DMA";			// Thread name
    	attrs.priority = anThreadPrio;
    	TSK_create((Fxn)CaptureThreadDispatch, &attrs, (Arg)this);
}	

/////////////////////////////////////////////////////////////////////////////
///
/// This destructor is used to close up and free the resources tied to the
/// associated Camera Link interface.
///
/// @return None.
///
/// @sa DspCameraLink.h
///
/////////////////////////////////////////////////////////////////////////////
tcArtemis_Dma::~tcArtemis_Dma()
{
    // un-register ISR
    if ((mnMyIntLevel >= gnFirstLevel) &&
        (mnMyIntLevel < (gnFirstLevel + gnNumLevels)))
    {
        tcDspInterruptDispatch::unregister_isr_callback
            (mnMyIntLevel, mnMyIntVector, interrupt_dispatch, (Arg)this);
    }
}

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
unsigned int tcArtemis_Dma::Interrupt_Count(void )
{
	return( mnInterruptCount );
}
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
///
/// Initiate a frame capture cycle.  This routine will enable the Camera Link
/// FIFO capture logic and begin capturing Frames of data of size
/// specified by SetImageSize() to the apCaptureBuffer memory.
///
/// The routine is designed to support buffering of multiple frames of data
/// so that calling software may pipeline process one or more images while more
/// are transferred from the link to memory.  The user provided data is treated
/// as a circular buffer window.  The capture logic always starts at the zeroth
/// offset for the first frame.
///
/// If the buffer is not an even multiple of anNumFramesPerMbx the "remainder"
/// of the capture buffer will not be used;  the capture process will roll over
/// to the beginning of the capture buffer so as to gauranteee continuous frame
/// data in memory.
///
/// @param[out] apCaptureBuffer   Buffered Memory, should be a minimum of
///                               2xRowsxColsxanNumFramesPerMbx elements.  Must
///                               by 4 byte aligned.
///
/// @param[in]  anBufferSize      size of buffer in bytes.
///
/// @param[in]  ahCaptureMbx      Mailbox handle that will be used to post tsFrameDataNotify
///                               mailbox messages.
///
/// @param[in]  anNumFrames   Total number of frames to collect before stopping.
///                           If < 1, capture will run continuously until
///                           aborted with AbortCapture.
///
/// @param[in]  anNumFramesPerMbx Post Number of frames collected per mailbox msg
///
/// @param[in]  apUserData optional user pointer returned in mailbox msg, default NULL.
///
/// @warning it is the callers responsibility to ensure that he has preformed
///          whatever processing is necessary on the incoming data before the
///          capture code overwrites it.  This is why the capture buffer should
///          be a minimum of 2 times the amount of data recieved per semaphore
///          post.
///
/// @warning If number of pixels per frame is odd and you are packing, then
///          you must specify an even number of frames per mailbox for transfer
///
///
/// @return 0 on success, -1 if camera is currently active or parameter error
///                     is detected.
///
/////////////////////////////////////////////////////////////////////////////
int tcArtemis_Dma::CaptureFrames(char* apCaptureBuffer, unsigned int   anBufferMask )
{
    int rc = 0;

	mpCaptureBuffer   	= apCaptureBuffer;			// mpCaptureBuffer needs to be initialized here
    mnBufferSize      	= anBufferMask+1;				// mnBufferSize needs to be initialized here													// THis is what is used in the ISR

	mnCurrDmaTransferSize	= 0;					// Number of Bytes Transferred for DMA but not sent to the host
	mnCurrDmaStartAddr		= 0;
    mbStopCapture     		= false;
	mpBaseAddr[XA_IADR_WR_FIFO_MR] 	= 0x0;
	mpBaseAddr[XA_IADR_DMA_IRQ_CLR] = ( XILINX_DMA_CLR | XILINX_ALTERA_INT_CLR );
//    SEM_reset(mhDataSem,0);

	// Starting Address of Capture Buffer
	mpBaseAddr[XA_IADR_DMA_START_ADDR] = ((unsigned int)mpCaptureBuffer ) & 0xFFFFFFF;					// Starting Address of Capture buffer
	mpBaseAddr[XA_IADR_DMA_ADDR_MASK] = ((unsigned int)anBufferMask) ;		// Address Mask of Capture Buffer, in 16bit words 
	mpBaseAddr[XA_IADR_DMA_INT_THRESHOLD] = 50000;		// Address Mask of Capture Buffer, in 16bit words 


	// Set the amount of data to copy the 
	// mnNumFPGADmaBytes <=	65535*4 for invalidation of the cache
	// mnNumFpgaDmaBytes 	= 512 * 4;					// Number of Bytes for Xilinx to DMA before Generating an interrupt												
	// mpBaseAddr[XA_IADR_DMA_INT_SIZE] = mnNumFpgaDmaBytes;												// FPGA Bytes in a DMA
//	mpBaseAddr[XA_IADR_DMA_INT_SIZE] = 8000; //not used

//	mnDmaPacketSize		= 20000;					// Size of DMA Packet before sending it off to the host
//	mpBaseAddr[XA_IADR_DMA_XFER_SIZE] = mnDmaPacketSize;												// DMA Packet Size

	SetFIFOInterruptLevel(eeDMALevel);

    // flush the cache of the entire buffer, ensure any write data is posted and
    // buffer is "clean"
    int bytecount = 0;
    while (bytecount < mnBufferSize)
    {
    	int numbytesflush = mnBufferSize-bytecount;

        // CSL library says mays count passed to invalidate routines is 65535*4
        if (numbytesflush > 65535*4)
        	numbytesflush = 65535*4;

		if (msL2Mode != CACHE_0KCACHE)
        	CACHE_wbInvL2(apCaptureBuffer+bytecount, numbytesflush, CACHE_WAIT);
		else
            CACHE_wbInvL1d(apCaptureBuffer+bytecount, numbytesflush, CACHE_WAIT);
      	bytecount += numbytesflush;
    }
	// Don't enable DMA and Interrupts until the socket has been created
	// Use the "SetDppSocket" to determine weither to enable/disable DMA and interrupts
	return rc;
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
void tcArtemis_Dma::Set_TimeBuffer_Period( float val )
{
	_TimeBuffer_Period = val;
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
void tcArtemis_Dma::Set_TimeBuffer_Start( float val )
{
	_TimeBuffer_Start = val;
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
void tcArtemis_Dma::Add_TimeBuffer( int key )
{
	int tmp;
	float tmpf;
//	static char buffer[2048];

	if( key == 0x01 )
		_TimeBuffer_Enable = TRUE;


	if( _TimeBuffer_Enable == TRUE )
	{
		tmpf = ( (float)CLK_gethtime() - _TimeBuffer_Start ) / _TimeBuffer_Period;
		tmp = (int) tmpf & 0xFFFFFF;
		tmp |= (( key & 0xFF ) << 24 );

		_TimeBuffer[_TimeBufferPtr] = tmp;
		if( ++_TimeBufferPtr >= TimeBufferSize )
			_TimeBufferPtr = 0;

		if( key == 0xFF )
		{
			_TimeBuffer_Enable = FALSE;
		}
	}
}   
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
int tcArtemis_Dma::Get_TimeBufferPtr( void )
{
	return( _TimeBufferPtr );
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
void tcArtemis_Dma::Set_TimeBufferPtr( int val )
{
	_TimeBufferPtr = val;
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
int tcArtemis_Dma::Get_TimeBuffer( int index )
{
	return( _TimeBuffer[index] );
}
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
int tcArtemis_Dma::SetDppSocket( int socket )
{

	int cnt;
	mnDppConnectedSocket = socket;

//	cnt = SEM_count(mhDataSem );
	return( cnt );
}
//-------------------------------------------------------------------------------------------------

/////////////////////////////////////////////////////////////////////////////
///
/// This method is a utility function to dispatch the CaptureThread method.
///
/// @return 0
///
/// @sa DspCameraLink.h
///
/////////////////////////////////////////////////////////////////////////////
int tcArtemis_Dma::CaptureThreadDispatch(Arg arMyObject)
{

    ((tcArtemis_Dma *)arMyObject)->CaptureThread();
    return (0);
}
//-------------------------------------------------------------------------------------------------

/////////////////////////////////////////////////////////////////////////////
///
/// Static interrupt dispatch routine.  Required because of the hidden this
/// pointer associated with a member function, which cannot be passed
/// directly to the interrupt dispatcher.
///
/// @param[in]  ahMyObject   The "this->" pointer for the instance of
/// tcDspAdcBase associated with this ISR.
///
/// @return 0
///
/////////////////////////////////////////////////////////////////////////////
int tcArtemis_Dma::interrupt_dispatch(Arg ahMyObject)
{

    ((tcArtemis_Dma *)ahMyObject)->LinkInterrupt();
    return (0);
}
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
///
/// Interrupt service routine for the ADC cores.  The ISR reads and clears
/// any pending interrupts.  If any of the pending interrupts has a
/// callback registered for it, the routine is called.
///
/// "Interrupt-ness" is taken care of by the 'dispatcher' in DSP/BIOS.
/// Installed by the constructor.
///
/////////////////////////////////////////////////////////////////////////////
void tcArtemis_Dma::LinkInterrupt(void)
{
	unsigned int lvFPGA_Status;
	tsDmaMessage lsDmaData;

	// increment interrupt counter (debug)
	mnInterruptCount++;
	Add_TimeBuffer( 0xF5 );

	// Check to see if the interrupt was also triggered by Altera
	lvFPGA_Status = mpBaseAddr[XA_IADR_RD_FPGA_STATUS];

	// Check to see if data can be sent by the "XFER IRQ in the Status Register
	if( lvFPGA_Status & XILINX_DMA_XFER_IRQ )
	{
		Add_TimeBuffer( 0xF6 );
//	    SEM_ipost(mhDataSem);   
		//mnSemCount = SEM_count(mhDataSem);

		lsDmaData.DataOffset = mpBaseAddr[XA_IADR_DMA_XFER_START_ADDR];
		lsDmaData.Len = mpBaseAddr[XA_IADR_DMA_INT_COUNT];
		MBX_post(DmaIntMbox, &lsDmaData, 0);

		Add_TimeBuffer( 0xF7 );

	}

	// clear DMA level interrupt ONLY
	mpBaseAddr[XA_IADR_DMA_IRQ_CLR] = 0x1;	
	Add_TimeBuffer( 0xF8 );
    return;
}

/////////////////////////////////////////////////////////////////////////////
///
/// Main capture thread.
/// This thread will capture incoming QDMA'd data from the camera link ISR.
/// If there is enough frame data to post a mailbox message to the waiting
/// client thread than it will do so.  If more data is expected then the
/// FIFO interrupts will be re-enabled.
///
/// @sa DspCameraLink.h
///
/////////////////////////////////////////////////////////////////////////////
#if 0 //test/debug
char buffer[256];
extern void sendit(char *str);
#endif //test/debug
void tcArtemis_Dma::CaptureThread(void)
{
	unsigned int lvTransferSize;
	unsigned int lvCurrStartAddr = 0;
	static unsigned int lvNextStartAddr;
	unsigned int length = 0;
	unsigned int end_length = 0;
	unsigned int start_length = 0;
	tsDmaMessage lsDmaData;

    sys_thread_register(TSK_self());

    while(true)
    {
		// Wait for a Semaphore ( triggered from the ISR )
		// Note: 100 (in ms) can be replaced by "SYS_FOREVER" for no timeout
//        if ( SEM_pend(mhDataSem,SYS_FOREVER))	//  && ( mbFpgaDmaEnabled ))
		  if (MBX_pend(DmaIntMbox,&lsDmaData,SYS_FOREVER))

        {
//			SEM_reset(mhDataSem,0);			// Reset teh Semaphone
			Add_TimeBuffer( 0xF9 );
			// First Read as much from Xilinx as possible and then reset the Xilinx
			// Get the number of bytes since last transfer	

#if 0
			lvTransferSize = mpBaseAddr[XA_IADR_DMA_INT_COUNT];			// This is 1-based

			// Get current Transfer Starting Address
			lvCurrStartAddr = mpBaseAddr[XA_IADR_DMA_XFER_START_ADDR];
#endif
			lvTransferSize = lsDmaData.Len;
			lvCurrStartAddr = lsDmaData.DataOffset;

#if 0 //test/debug
sprintf(buffer,"len:%d, offset:%d\r\n",lvTransferSize,lvCurrStartAddr);
sendit(buffer);
if(lvCurrStartAddr && (lvCurrStartAddr != lvNextStartAddr))
{
sprintf(buffer,"OFFSET MISMATCH:told:%d, calc:%d\r\n",lvCurrStartAddr,lvNextStartAddr);
sendit(buffer);

}
#endif //test/debug
			if( lvTransferSize > 0 )
			{
				Add_TimeBuffer( 0xFA );
				// Now that all relevant data has been collected Clear Transfer Interrupt (DMA_XFER_START will be updated )
				mpBaseAddr[XA_IADR_DMA_IRQ_CLR] = 0x2;

				// Determine the next start address
				lvNextStartAddr = lvCurrStartAddr + lvTransferSize;

				// Assign the Default parameters for Length 0 and Length1
				end_length = length;
				start_length = 0;

				// Last address is "mhBufferSize-4"
				if ( lvNextStartAddr <= mnBufferSize )
				{
					if (msL2Mode != CACHE_0KCACHE)
						CACHE_wbInvL2(&mpCaptureBuffer[lvCurrStartAddr], lvTransferSize, CACHE_WAIT);
					else
				    	CACHE_wbInvL1d(&mpCaptureBuffer[lvCurrStartAddr], lvTransferSize, CACHE_WAIT);

					Add_TimeBuffer( 0xFB );
					if( mnDppConnectedSocket > -1 )			// Have a valid Socket connection - so send it
					{
						if( write( mnDppConnectedSocket, &mpCaptureBuffer[lvCurrStartAddr], lvTransferSize ) < 0 )
						{
							close( mnDppConnectedSocket );						
							Add_TimeBuffer( 0xFE );
						}
					}

				}
				//else if( lvNextStartAddr == mnBufferSize)
				//{
				//	if( mnDppConnectedSocket > -1 )			// Have a valid Socket connection - so send it
				//	{
				//		if( write( mnDppConnectedSocket, &mpCaptureBuffer[lvCurrStartAddr], lvTransferSize ) < 0 )
				//			close( mnDppConnectedSocket );						
				//	}
				//	if (msL2Mode != CACHE_0KCACHE)
				//		CACHE_wbInvL2(&mpCaptureBuffer[lvCurrStartAddr], lvTransferSize, CACHE_WAIT);
				//	else
				//    	CACHE_wbInvL1d(&mpCaptureBuffer[lvCurrStartAddr], lvTransferSize, CACHE_WAIT);
				// }
				else // lvNextStartAddr > mnBufferSize )						// Not Enough Room to fit all of the data
				{
					Add_TimeBuffer( 0xFC );
					end_length = ( mnBufferSize - lvCurrStartAddr );	// Length from Current Addr to ENd of buffer
					start_length = lvTransferSize - end_length;			// amount "overhanging" the buffer
//sprintf(buffer,"---strt:%d, xfer:%d, endl:%d, startlen:%d\r\n",lvCurrStartAddr,lvTransferSize,end_length, start_length);
//sendit(buffer);

					// Now Invalidate the Cache
					if (msL2Mode != CACHE_0KCACHE)
				       	CACHE_wbInvL2(&mpCaptureBuffer[lvCurrStartAddr	], end_length, CACHE_WAIT);
				    else
			    		CACHE_wbInvL1d(&mpCaptureBuffer[lvCurrStartAddr	], end_length, CACHE_WAIT);

					// Have a valid socket connection...
					if( mnDppConnectedSocket > -1 )			// Have a valid Socket connection
					{
						if( write( mnDppConnectedSocket, &mpCaptureBuffer[lvCurrStartAddr], end_length ) < 0 )
						{
							mnDppConnectedSocket = -1;
							Add_TimeBuffer( 0xFE );
						}
					}


					// If the connection was lost, close the socket
					if( mnDppConnectedSocket == -1 )
					{
						close( mnDppConnectedSocket );						
						Add_TimeBuffer( 0xFE );
					}


					// Now Invalidate the Cache
					if (msL2Mode != CACHE_0KCACHE)
				       	CACHE_wbInvL2(&mpCaptureBuffer[0				], start_length, CACHE_WAIT);
				    else
				    	CACHE_wbInvL1d(&mpCaptureBuffer[0				], start_length, CACHE_WAIT);

					if(mnDppConnectedSocket > -1 )
					{
						Add_TimeBuffer( 0xFD );
						if( write( mnDppConnectedSocket, &mpCaptureBuffer[0], start_length ) < 0 )
						{
							mnDppConnectedSocket = -1;
							Add_TimeBuffer( 0xFE );
						}
					}

					// If the connection was lost, close the socket
					if( mnDppConnectedSocket == -1 )
					{
						close( mnDppConnectedSocket );						
						Add_TimeBuffer( 0xFE );
					}
				}
			}																// lvTransferSize > 0
			// Now that all relevant data has been collected Clear Paused Flag
//CLgone			mpBaseAddr[XA_IADR_DMA_IRQ_CLR] = 0x4;
			Add_TimeBuffer( 0xFF );
		}																	// SEM_Pend...
    }																		// while true...
}																			// Capture Thread


/////////////////////////////////////////////////////////////////////////////
///
/// Aborts an ongoing frame capture cycle.
///
/// @return 0 on succes (and camera is aborted), -1 if camera is not currently active
///
/////////////////////////////////////////////////////////////////////////////
int tcArtemis_Dma::AbortCapture(void)
{
    int rc = 0;

	if(( mpBaseAddr[XA_IADR_DMA_EN_REG] & XILINX_DMA_EN ) == 0 )
    {
        rc = -1;
    }
    else
    {
        mbStopCapture = true;
		while( mpBaseAddr[XA_IADR_DMA_EN_REG] & XILINX_DMA_EN ) 
        {
            TSK_sleep(10);
        }
    }
    return rc;
}


/////////////////////////////////////////////////////////////////////////////
///
/// Disables all FIFO interrupt enables.
///
/// @return none
///
/////////////////////////////////////////////////////////////////////////////

void tcArtemis_Dma::DisableFIFOInterrupts(void)
{
    tuCamLinkIer IER;
		 
    IER.mnLword = 0;
    mpBaseAddr[IER_OFFSET] = IER.mnLword;

    return;
}


/////////////////////////////////////////////////////////////////////////////
///
/// Enables interrupts for the specified level.  This routine will
/// disable other level interrupts if they were previously enabled.
///
/// @param aeLevel Level to Enable the Interrupt for
///
/// @return none
///
/////////////////////////////////////////////////////////////////////////////
void tcArtemis_Dma::SetFIFOInterruptLevel(teFIFOLevel aeLevel)
{
    tuCamLinkIer IER;
		 //MIKE: map to the bits in your core

    IER.mnLword = 0;
    switch (aeLevel)
    {
        case eeOneQ:
            IER.msBits.mbInt1QFull = 1;
            break;
        case eeHalf:
            IER.msBits.mbIntHalfFull = 1;
            break;
        case eeThreeQ:
            IER.msBits.mbInt3QFull = 1;
            break;
        case eeFull:
            IER.msBits.mbIntFull = 1;
            break;
        case eeDMALevel:
            IER.msBits.mbIntDmaCompleted = 1;
            break;
        default:
        case eeEmpty:
            IER.msBits.mbIntNotEmpty = 1;
            break;
    }

    mpBaseAddr[IER_OFFSET] = IER.mnLword;

    return;
}
/////////////////////////////////////////////////////////////////////////////
