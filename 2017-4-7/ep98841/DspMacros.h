#ifndef DSPMACROS_H

#ifdef MITYDSP_PRO
#define MIDR (volatile unsigned int*)0x70000000
/** This macro forces the EMIF to be in sync when performing closely packed
  * reads followed by writes to the EMIF bus.  This function should be used
  * under the following circumstances (for the MITYDSP_PRO):
  *
  * 1. write performed (EMIF space)
  * 2. read is performed outside of 2048 byte window of write is performed.
  *
  * If the read is sensitive timing wise (e.g., FLASH based command operations)
  * to the writes, then between 1. and 2. a SYNC_EMIF should be inserted.
  *
  * See section 7.1 of the spru971 manual (6455 EMIF users guide) for further 
  * information.
  *
  * Notes:  This routine does not use the CSL.  The EMIF CSL support for the 
  * 6455 looks a bit dubious (read: incomplete) at the time of authorship...
  */
#define SYNC_EMIF \
{ \
   /* perform dummy write to the EMIF module ID and revision register */ \
   volatile unsigned int emid = 0xDEADBEEF;                              \
   *MIDR = emid;                                                         \
                                                                         \
   /* perform dummy read  to the EMIF module ID and revision register */ \
   emid  = *MIDR;                                                         \
}
#else
#define SYNC_EMIF ;
#endif

#ifdef MITYDSP_PRO
#else
#ifndef PLL_DIV0
#include "boot/c6711reg.h"
#endif
#define CPU_FREQ_26MHZ   0x0000000E
#define CPU_FREQ_28MHZ   0x0000000D
#define CPU_FREQ_31MHZ   0x0000000C
#define CPU_FREQ_33MHZ   0x0000000B
#define CPU_FREQ_36MHZ   0x0000000A
#define CPU_FREQ_40MHZ   0x00000009
#define CPU_FREQ_44MHZ   0x00000008
#define CPU_FREQ_50MHZ   0x00000007
#define CPU_FREQ_57MHZ   0x00000006
#define CPU_FREQ_67MHZ   0x00000005
#define CPU_FREQ_80MHZ   0x00000004
#define CPU_FREQ_100MHZ  0x00000003
#define CPU_FREQ_133MHZ  0x00000002
#define CPU_FREQ_200MHZ  0x00000001

/**
 *  This macro safely reconfigures the CPU Frequency according to the input 
 *  parameter.
 *
 *  Note:  CPU_FREQ must be one of :
 *         - CPU_FREQ_200MHZ
 *         - CPU_FREQ_133MHZ
 *         - CPU_FREQ_100MHZ
 *         - CPU_FREQ_80MHZ
 *         - CPU_FREQ_67MHZ
 *         - CPU_FREQ_57MHZ
 *         - CPU_FREQ_50MHZ
 *         - CPU_FREQ_44MHZ
 *         - CPU_FREQ_40MHZ
 *         - CPU_FREQ_36MHZ
 *         - CPU_FREQ_33MHZ
 *         - CPU_FREQ_31MHZ
 *         - CPU_FREQ_28MHZ
 *         - CPU_FREQ_26MHZ
 *
 *  This routine first determines the type of MityDSP being used (MityDSP XM
 *  or standard), as the maximum refresh rate is slower for the XM parts due 
 *  to an errata from the 32 MByte SDRAM vendors.  The routine adjusts the 
 *  SDRAM refresh timing down so that when the PLL is reset (and the clocks
 *  drop back to 25 MHz, etc.) the SDRAM will still be refreshed properly. Then
 *  the PLL is reconfigured, re-enabled, and the SDRAM refresh is reset 
 *  accordingly.
 *
 *
 */
#define RECONFIG_CLOCKS(CPU_FREQ) \
{ \
    /* First, figure out if we are running on an XM or Standard MityDSP. */          \
    /* The XM part requires a 7.81 usec refresh timing */                            \
    /* The Standard part requires a 15.62 usec refresh timing  */                    \
    /* This can be determined by reading the SDRAM control register and  */          \
    /* checking the size of the RAM we are using */                                  \
    int isXM = 0;                                                                    \
    volatile unsigned int emif_sdctrl = *(unsigned volatile int *)EMIF_SDCTRL;       \
    if (emif_sdctrl & 0x7C000000 == 0x50000000)                                      \
    {                                                                                \
        isXM = 1;                                                                    \
    }                                                                                \
    if ( ((CPU_FREQ >= CPU_FREQ_200MHZ) && (CPU_FREQ <= CPU_FREQ_26MHZ))  )          \
    {                                                                                \
        /* If the PLL_MULTIPLIER is not set to 16 (giving 400 MHz clock to work with)\
           then reset the PLL */                                                     \
        int PLL_RECONFIG = !(((*(unsigned volatile int*)PLL_MULT)&0x01F) == 0x010);    \
        if (PLL_RECONFIG)                                                            \
        {                                                                            \
           /* First set SDRAM refresh to handle the base 25/4 EMIF clock that will  */  \
           /* be present while PLL is locking...          */                            \
           if (isXM)                                                                    \
              *(unsigned volatile int *)EMIF_SDRP   = 0x00000030; /* SDRAM refresh period, 7.68 usec @ 6.25 Mhz*/ \
           else                                                                         \
              *(unsigned volatile int *)EMIF_SDRP   = 0x00000061; /* SDRAM refresh period, 15.62 usec @ 6.25 Mhz*/ \
           /* perform PLL initialization */                                             \
           *(unsigned volatile int *)PLL_CSR  = 0x00000008; /* PLLRST=1; PLLEN=0 */     \
           *(unsigned volatile int *)PLL_DIV0 = 0x00008000; /* D0 Enable; 1/1 */        \
           *(unsigned volatile int *)PLL_MULT = 0x00000010; /* mult=16x -> 400 Mhz */    \
           *(unsigned volatile int *)OSC_DIV1 = 0x00008000; /* OscDiv En; 1/1 */        \
           *(unsigned volatile int *)PLL_DIV1 = 0x00008003; /* D1 Enable; 1/4 -> 100 MHz */ \
           *(unsigned volatile int *)PLL_DIV2 = 0x00008007; /* D2 Enable; 1/8 -> 50 Mhz */  \
           *(unsigned volatile int *)PLL_DIV3 = 0x00008007; /* D3 Enable; 1/8 -> 50 Mhz*/   \
           *(unsigned volatile int *)PLL_CSR  = 0x00000000; /* PLLRST=0; PLLEN=0 */     \
           while(!(*(unsigned volatile int *)PLL_CSR & 0x00000020)); /* wait for pll-lock */   \
           *(unsigned volatile int *)PLL_CSR  = 0x00000001; /* PLLRST=0; PLLEN=1 */     \
           if (isXM)                                                                    \
              *(unsigned volatile int *)EMIF_SDRP   = 0x00000186; /* SDRAM refresh period, 7.8 usec @ 50 Mhz*/   \
           else                                                                                                  \
              *(unsigned volatile int *)EMIF_SDRP   = 0x0000030D; /* SDRAM refresh period, 15.62 usec @ 50 Mhz*/ \
           /* bogus write to FPGA to ensure EMIF is locked, this will stall processor */   \
           /* until the FPGA has dealt with clock bounce */                                \
           *(unsigned volatile int *)0xB0000000 = 0xDEADBEEF;                              \
        }                                                                               \
        int curCPUDiv =  (*(unsigned volatile int *)PLL_DIV1)&0x1F;                     \
        int DIV2Setting = (((CPU_FREQ)+1)<<1)-1;                                        \
        if (curCPUDiv < CPU_FREQ) /* slow down D2 is first */                           \
        {                                                                               \
           *(unsigned volatile int *)PLL_DIV2 = 0x8000|DIV2Setting;   /* D2 Enable */   \
           *(unsigned volatile int *)PLL_DIV1 = 0x8000|CPU_FREQ;        /* D1 Enable */ \
        }                                                                               \
        else /* speed up, D1 is first */                                                \
        {                                                                               \
           *(unsigned volatile int *)PLL_DIV1 = 0x8000|CPU_FREQ;        /* D1 Enable */ \
           *(unsigned volatile int *)PLL_DIV2 = 0x8000|DIV2Setting;   /* D2 Enable */   \
        }                                                                               \
    }                                                                                   \
}

#endif



#endif

