/**
// History
//		April 13, 2012
//			Found problem in DSP, with "Spot mapping", the serial port was limiting the speed. Altered DSP code for corrections.
//  01/14/2012 - M. Solazzi
//		Looked into the increased time delay from "Start Spectrum" command until the "acknowledge" from the data-stream
//		in received from the host. It was found that the modification 8/23/2011 in file 
//			C:\MityDSP\2.8\software\inc\lwip_port\lwipopts.h needed to be updated changing TCP_QUEUE_OOSEQ from a 1 to a 0 causes this
//		erratic and increased delay. Setting it back to a 1 causes an average start time of < 15ms.
//		Left it in this state for more testing of start time delay
//
//	08/24/2011	- M. Solazzi
//		File C:\MityDSP\2.8\software\inc\lwip_port\lwipopts.h needed to be updated
//		Definition  TCP_QUEUE_OOSEQ needed to be changed from a 1 to a 0 (about line 499 )
//	06/20/2011
// 		DspMacros.h file had to be patched due to timing constraints - this is from MityDSP Ver 2.8
//		See n-drive in folder "N:\Tools\Critical Link\MityDSP Setup\Patch_MItyDSP2.8"
//
//
 * \file TelnetEchoExample.h
 *
 * This file describes the tcTelnetEchoExample class. 
 * 
 *  \author  Critical Link LLC
 *
 *  \date    8/2009
 *
 *  \remarks
 *
 *
 *  \verbatim

      o  0
      | /       Copyright (c) 2009
     (CL)---o   Critical Link, LLC
       \
        O

    \endverbatim
 **/
#ifndef EP98840_H
#define EP98840_H

#include <std.h>
#include <tsk.h>
#include <sem.h>


#include <core/DspFirmware.h>
#include <core/DspSerial.h>
#include <core/DspBankSelect.h>
#include <core/DspFlash.h>
#include <core/DspConfig.h>
#include <core/DspParseIHex.h>
#include <core/DspError.h>
#include <core/DSPGpio.h>
#include <core/DspFlash.h>
#include <core/DspBootStrapper.h>
#include <net/DSPNetStack.h>
#include <net/net_drvr/net_drvr.h>
#include <net/net_drvr/net_drvr_b.h>
#include <net/net_iface.h>
#include <net/telnetd/telnetd.h> 

// forward declarations
namespace MityDSP
{
   class tcDspSerial;
   class tcDspFlash;
}

class tcDspNetIFConfigUtil;

// Parser declarations
#define STAT_SUCCESS        (0x00)
#define STAT_FAILURE        (0x01)

#define RS232_SERIAL_BASE     	0xB0000080
#define USB_SERIAL_BASE       	0xB0000100
#define ETHERNET_BASE         	0xB0000180
#define RTCI2C_BASE           	0xB0000200		// Not used
#define GPIO_BASE             	0xB0000280		// Not used
#define ADC8344_BASE          	0xB0000300		
#define CF_GPIO_BASE		  	0xB0000380
#define CF_BASE				  	0xA0000000
#define FLASH_BASE_ADDR       	0x90000000
#define BANK_SEL_ADDR         	0xB0000004

// DSP Version as used by EDAX
//#define EDAX_VER_APP			( ( 0x20 + 1 ) & 0xFF  ) << 8
#define EDAX_VER_APP_MAJOR	    (      2 & 0xF ) << 12
#define EDAX_VER_APP_MINOR		(      3 & 0xF ) << 8 
#define EDAX_VER_YEAR			(     17 & 0xFF  ) << 25
#define EDAX_VER_MON			(     5 & 0xF   ) << 21
#define EDAX_VER_DAY			(     4 & 0x1F  ) << 16
#define EDAX_VER_MAJOR			(      5 & 0xF   ) << 4
#define EDAX_VER_MINOR			(     16 & 0xF   ) << 0


#define DMA_ENABLED 1					//Define this constant to Enable DMA

// Constants for the GPIO used to program the Altera FLASH EEPROM
// Individual offsets shifted left by 2 for the byte/long conversion
#define ALTERA_BASE	  				0xB0000400	
#define ALTERA_FLASH_WIP					1				// Write in Progress

#define XA_IADR_RD_VER				 		0
#define XA_IADR_RD_VER1				 		1
#define XA_IADR_FLASH_STATUS		 		2
#define XA_IADR_RD_FLASH_DATA		 		3

#define XA_IADR_WR_FLASH_ENABLE		 		2
#define XA_IADR_WR_FLASH_NCS		 	 	3
#define XA_IADR_WR_FLASH_NCE		 	 	4
#define XA_IADR_WR_FLASH_NCON		 		5
#define XA_IADR_WR_FLASH_BYTE_MSB	 		6
#define XA_IADR_WR_FLASH_BYTE_LSB	 		7
#define XA_IADR_WR_FLASH_BYTE_READ    		8		// Starts a Read Process
#define XA_IADR_WR_FPGA_ADDR				9
#define XA_IADR_WR_FPGA_DATA			 	10
#define XA_IADR_WR_FPGA_SPARE		 		11
#define XA_IADR_DMA_EN_REG					12
#define XA_IADR_DMA_IRQ_CLR					13
#define XA_IADR_DMA_START_ADDR				14
//#define XA_IADR_DMA_END_ADDR				15
#define XA_IADR_DMA_ADDR_MASK				15
#define XA_IADR_DMA_INT_SIZE				16
// #define XA_IADR_DMA_XFER_SIZE				17
#define XA_IADR_DMA_INT_THRESHOLD           17   //Number of 100mhz idle clock ticks to cause transfer
#define XA_IADR_DMA_INT_COUNT				18
// #define XA_IADR_DMA_XFER_COUNT				19
#define XA_IADR_DMA_ADDR					20
#define XA_IADR_DMA_XFER_START_ADDR			21
#define XA_IADR_ETH_DEF						22
#define XA_IADR_RD_FPGA_STATUS 		 		23
#define XA_IADR_RD_FPGA_DATA		 	 	24
#define XA_IADR_USER_IO						25		// bit 4 sets UserIo Mode, bits3-0 are passed to SpareIO
#define XA_IADR_WR_FIFO_MR			 		26

#define USER_IO_MODE 1 << 31
#define USER_IO_ACK  1 << 0

// ALtera Core, DMA Register Bit Positions
#define XILINX_DMA_EN 			(unsigned int)	1 << 0
#define XILINX_DMA_ROLL_EN 		(unsigned int)	1 << 1
#define XILINX_ALTERA_INT_EN 	(unsigned int)	1 << 2
#define XILINX_DMA_CLR			(unsigned int) 	1 << 0
#define XILINX_ALTERA_INT_CLR	(unsigned int)  1 << 1

#define XILINX_FIFO_CNT_MASK	0xFFFF
#define XILINX_ETHERNET_DEF		1 << 0
#define XILINX_DMA_XFER_WAIT 	(unsigned int) 1 << 30
#define XILINX_DMA_XFER_IRQ 	(unsigned int) 1 << 31

//#define XILINX_FIFO_AF			(unsigned int)	1 << 31
//#define XILINX_FIFO_FF			(unsigned int)	1 << 30
//#define ALTERA_FIFO_IRQS		(unsigned int)	1 << 29
//#define XILINX_FIFO_EF			(unsigned int)	1 << 28

#define ALTERA_READ_FIFO_EF 		16
#define ALTERA_READ_FIFO_FF 		17
#define ALTERA_WRITE_FIFO_EF 		18
#define ALTERA_WRITE_FIFO_FF 		19
#define ALTERA_READ_FIFO_READY		29

#define AS_WRITE_ENABLE 		0x06
#define AS_WRITE_DISABLE 		0x04
#define AS_READ_STATUS 			0x05
#define AS_WRITE_STATUS 		0x01
#define AS_READ_BYTES 			0x03
#define AS_FAST_READ_BYTES 		0x0B
#define AS_PAGE_PROGRAM 		0x02
#define AS_ERASE_SECTOR 		0xD8
#define AS_ERASE_BULK 			0xC7
#define AS_READ_SILICON_ID 		0xAB

#define EPCS1_ID 				0x10
#define EPCS4_ID 				0x12
#define EPCS16_ID 				0x14
#define EPCS64_ID 				0x16

#define EPCS1 					1 * 131072 
#define EPCS4 					4 * 131072 
#define EPCS16 					16 * 131072 
#define EPCS64 					64 * 131072 
// Constants for the GPIO used to program the Altera FLASH EEPROM


// Constants for some "Packet Based I/O from the Host
#define krwKey_Data_Mask	    (unsigned int) 0x3 << 30

#define krKey_Blevel_Data		(unsigned int) 0x0 << 30
#define krKey_Dss_Data			(unsigned int) 0x1 << 30
#define krKey_Spec_Data			(unsigned int) 0x2 << 30
#define krKey_Param_Data		(unsigned int) 0x3 << 30

//----- Commands from the Host to the DSP  ----------------------------------------------------
#define krwKey_Mask				(unsigned int) 0xFF << 24			// Mask to check for key

#define hrKey_Data  			(unsigned int) 0xC0 << 24		// was 26
#define hrKey_SCABB				(unsigned int) 0xC1 << 24		// was 26
#define krKey_Generic_Flash		(unsigned int) 0xC2 << 24

#define hwKey_Data  			(unsigned int) 0x80 << 24			// Write Data to Altera
#define hwKey_AmpTime			(unsigned int) 0x81 << 24
#define hwKey_Time_Start_Stop	(unsigned int) 0x82 << 24
#define hwKey_Time_Clear		(unsigned int) 0x83 << 24
#define hwKey_Fir_Mr			(unsigned int) 0x84 << 24
#define hwKey_Fifo_MR			(unsigned int) 0x85 << 24
#define hwKey_SCABB				(unsigned int) 0x86 << 24
#define hwKey_Blevel_Start_Stop	(unsigned int) 0x87 << 24
#define hwKey_Dss_Start_Stop	(unsigned int) 0x88 << 24
#define hwKey_Slide_Move		(unsigned int) 0x89 << 24
#define hwKey_Time_Clear_Start  (unsigned int) 0x8A << 24

#define hwKey_VerInfo  			(unsigned int) 0x90 << 24
#define hwKey_Read_Data_Enable	(unsigned int) 0x91 << 24
#define hwKey_SetDef			(unsigned int) 0x92 << 24
#define hwKey_DppInit			(unsigned int) 0x93 << 24
#define hwKey_Dpp_Reset			(unsigned int) 0x94 << 24
#define hwKey_Serial_Enable		(unsigned int) 0x95 << 24
#define hwKey_Next_DHCP			(unsigned int) 0x96 << 24
#define hwKey_Next_WINS			(unsigned int) 0x97 << 24
#define hwKey_Next_IPAddr		(unsigned int) 0x98 << 24
#define hwKey_Next_Gateway		(unsigned int) 0x99 << 24
#define hwKey_Next_Netmask		(unsigned int) 0x9A << 24
#define hwKey_Next_Wins_Name	(unsigned int) 0x9B << 24
#define kwKey_Flash_Altera 	 	(unsigned int) 0x9C << 24
#define kwKey_Flash_Xilinx 		(unsigned int) 0x9D << 24
#define kwKey_Flash_Dsp 		(unsigned int) 0x9E << 24
#define kwKey_Generic_Flash		(unsigned int) 0x9F << 24
#define kwKey_Erase_Altera		(unsigned int) 0xA0 << 24
#define kwKey_Read_TimeBuffer	(unsigned int) 0xA1 << 24	// 4/4/12

#define hwKey_Time_Start_Ack	(unsigned int) 0x8D << 24
#define hwKey_Ack_Req			(unsigned int) 0x8F << 24

//----- Commands from the Host to the DSP  ----------------------------------------------------

//----- Constants for the Commands from the DSP to the Altera FPGA ----------------------------
#define  awCmd_Write					(unsigned int) 0x00 << 24
#define  awCmd_SetDef					(unsigned int) 0x01 << 24
#define  awCmd_AmpTime					(unsigned int) 0x02 << 24
#define  awCmd_Read_Data_Enable_Disable (unsigned int) 0x03 << 24
#define  awCmd_Time_Start_Stop			(unsigned int) 0x04 << 24
#define  awCmd_Time_Clear				(unsigned int) 0x05 << 24
#define  awCmd_Fir_Mr					(unsigned int) 0x06 << 24
#define  awCmd_Fifo_mr					(unsigned int) 0x07 << 24
#define  awCmd_SCABB					(unsigned int) 0x08 << 24
#define  awCmd_Blevel_Start_Stop		(unsigned int) 0x09 << 24
#define  awCmd_Dss_Start_Stop			(unsigned int) 0x0A << 24
#define  awCmd_Slide_Move				(unsigned int) 0x0B << 24
#define  awCmd_Ack_Req					(unsigned int) 0x0F << 24
#define  awCmd_Read						(unsigned int) 0x80 << 24
#define  awCmd_Read_SCABB				(unsigned int) 0x81 << 24
//----- Constants for the Commands from the DSP to the Altera FPGA ----------------------------

//----- Data Keys in Read-back Data -----------------------------------------------------------
//#define krKey_Time_Stop			(unsigned int) 0x02 << 24		// was 26
//#define krKey_Map_Start			(unsigned int) 0x03 << 24		// was 26
//#define krKey_Spec_Clear		(unsigned int) 0x04 << 24		// was 26

#define krKey_SCABB_Start			(unsigned int) 0x01 << 26
#define krKey_Ack_Req				(unsigned int) 0x05 << 26
#define krKey_VerInfo				(unsigned int) 0x06 << 26
#define krKey_Flash_Status			(unsigned int) 0x07 << 26
#define krKey_Generic_Flash_Start 	(unsigned int) 0x08 << 26;
#define krKey_Map_Pixel  			(unsigned int) 0x09 << 26;
#define krKey_Map_Line 				(unsigned int) 0x0A << 26;
#define krKey_Map_EOLN 				(unsigned int) 0x0B << 26;
#define krKey_DSP_Counts			(unsigned int) 0x0C << 26;
#define krKey_TimeBuffer_Start		(unsigned int) 0x0D << 26;


#define kSTARTSTOP_START 		1		// Start Acquistion
#define kSTARTSTOP_STOP 		2		// Stop Acquisition
#define kSTARTSTOP_CLEAR 		3		// Clear Spectrum/Data
#define kSTARTSTOP_CLEARSTART 	4		// Clear and Restart
#define kSTARTSTOP_MAPRESTART 	5		// Mapping Restart
//----- Data Keys in Read-back Data -----------------------------------------------------------

//-- Read/Write Parameters --------------------------------------------------------------------
#define iADR_SYNC_REG       0x00		// Enable External Load
#define iADR_CWORD		    0x01		// Control Word
#define iADR_HV_CTRL		0x02
#define iADR_ATCWORD		0x03
#define iADR_EVCH_GAIN		0x04		// Coarse Gain
#define iADR_GAIN		    0x05		// Fine Gain
#define iADR_OFFSET		    0x06		// Offset
#define iADR_DISC_0		    0x07		// Disc Thresold Level - SH
#define iADR_DISC_1		    0x08		// Disc Thresold Level - H
#define iADR_DISC_2		    0x09		// Disc Thresold Level - M
#define iADR_DISC_3		    0x0A		// Disc Thresold Level - L
#define iADR_DISC_4		    0x0B		// Disc Thresold Level - B (AmpTime/2)
#define iADR_CLIP		    0x0C		// Clip Max/Min
#define iADR_PRESET		    0x0D		// Preset Value
#define iADR_SCABB		    0x0E		// SCA/BB Parameters
#define iADR_Pixels_Line	0x0F		// Pixels per frame
#define iADR_Lines_Frame	0x10		// total Frames
#define iADR_DSS_REG        0x11		// Static DSS Settings
#define iADR_DSS_INT        0x12		// Interval Counter
#define iADR_DSS_MAX        0x13		// Maximum Count
#define iADR_BIT_PEAK		0x14		// Maximum Count
#define iADR_BIT_TIME		0x15		// Maximum Count
#define iADR_PA_TEMP_OFFSET 0x16
#define iADR_TARGET_ADC		0x17		// Preamp Reset Pot Values
#define iADR_POT_VAL 		0x18		// Preamp Pot Values
#define iADR_RESET_PW		0x19
#define iADR_PA_TEC2_KIKP	0x1B
#define iADR_TEC2_DELAY		0x1C
#define iADR_OTR_FACTOR		0x1D
#define iADR_RTIME_THR		0x1E
#define iADR_TEC1_FACTOR	0x1F		// TEC2 * FACTOR = TEC1

#define iADR_SLIDE_HCR		0x20		// Auto Retract High Count Rate
#define iADR_SLIDE_HCT		0x21		// Auto Retract High Count Threshold
#define iADR_MSlide_Tar 	0x22		// Target Slide Position
#define iADR_MSlide_Vel_Max	0x23		// Motorized Slide Velocity
#define iADR_MSlide_Vel_Min	0x24

#define iADR_WF_COEFA		0x25		// 08/30/11
#define iADR_WF_COEFB		0x26		// 08/30/11
#define iADR_WF_COEFC		0x27		// 08/30/11
#define iADR_WF_DELAY		0x28		// 08/30/11

#define iADR_RW_MAX		 	0x2F		// Maximum Adelsdresses
//-- Read/Write Parameters ------------------------------------------------------------------


// -- Read Only Parameters ----------------------------------------------------------------------
#define iADR_ALTERA_VER      	0x40		// Altera Version
#define iADR_TEC_VER		 	0x41
#define iADR_PTIME_DT0		 	0x42		// Disc 1 & 0  Peak Time
#define iADR_PTIME_DT1		 	0x43		// Disc 3 & 2  Peak Time
#define iADR_PTIME_AT0		 	0x44		// Main Channel Peak Time 1 & 0
#define iADR_PTIME_AT1		 	0x45		// Main Channel Peak Time 3 & 2
#define iADR_PTIME_AT2		 	0x46		// Main Channel Peak Time 5 & 4
#define iADR_PTIME_AT3		 	0x47		// Main Channel Peak Time 7 & 6
#define iADR_PADC10			 	0x48		// Preamp ADC Channels 1 and 0
#define iADR_PADC32			 	0x49		// Preamp ADC Channels 3 and 2
#define iADR_PADC54			 	0x4A		// Preamp ADC Channels 5 and 4
#define iADR_PADC76			 	0x4B		// Preamp ADC Channels 7 and 6
#define iADR_TMP05			 	0x4C		// Temperature Sensor (tmp05)
#define iADR_PA_TEMP		 	0x4D
#define iADR_STATUS          	0x4E		// Status

#define iADR_RAMP		     	0x4F		// Ramp Max/Min
#define iADR_RST_TIME	     	0x50		// Ramp Reset Time
#define iADR_RST_PER		 	0x51		// Reset Period ( or Pulse Time )
#define iADR_CTIME		     	0x52		// Clock Time
#define iADR_LTIME		     	0x53		// Live Time
#define iADR_CPS			 	0x54		// Input Count Rate
#define iADR_NPS			 	0x55		// Output Count RAte
#define iADR_NCPS		     	0x56		// Net Input Counts
#define iADR_NNPS		     	0x57		// Net Output Counts
#define iADR_DCNTS_0		 	0x58		// Disc Counts - SH (40ns)
#define iADR_DCNTS_1		 	0x59		// Disc Counts - H (160ns)
//#define iADR_PIXEL_CNTR		 	0x5A		// Pixel Count
//#define iADR_FRAME_CNTR		 	0x5B		// Frame Count
#define iADR_MSlide_Pos 	 	0x5C		// Current Slide Position
#define iADR_SLIDE_MOVE_TIME 	0x5D		// Slide Move Time (msec)
#define iADR_SLIDE_ANALYZE_POS 	0x5E

#define iADR_RO_MAX     	 0x5F	
//-- Read Only Parameters ----------------------------------------------------------------------

#define ATCW_GapTime_LSB 		 0
#define ATCW_GapTime_MSB 		 4
#define ATCW_BLR_LSB			 6
#define ATCW_BLR_MSB			 7
#define ATCW_Disc_Dis_40		 8
#define ATCW_Disc_Dis_160		 9
#define ATCW_Disc_Dis_320		10
#define ATCW_Disc_Dis_640		11
#define ATCW_Disc_Dis_BLM		12	

#define CW_WEINER_EN			 0			// 08/13/11
#define CW_ADC_RANDOM			 1			// 08/13/11
#define CW_PGA_ENABLE			 3			// 06/13/12
#define CW_PRESET_MODE_L		16
#define CW_PRESET_MODE_H		18
#define CW_SLIDE_EN				26
#define CW_BIT_GEN				27
#define CW_TP_SEL_LSB			28
#define CW_TP_SEL_MSB			31	

#define MSlide_Stop				1 << 0
#define MSlide_Reset 			1 << 1
#define MSlide_Position			1 << 2
#define MSlide_Find_Analyze  	1 << 3
#define MSlide_Find_Retract 	1 << 4


#define HVCW_ONLINE				8

#define DPP_PORT		      	22				// Data Port (DMA)
#define TELNET_PORT     	  	23				// Telnet ( TCP/IP and FLash )
#define PARAM_PORT				24				// Parameter Port

// Priorities range 0 to 15 where 15 is the highest
//#define PRIORITY_PORT_24		8	// Parmeter Data Port was
//#define PRIORITY_PORT_23		1	// Telnet port
//#define PRIORITY_PORT_22		7	// Data Port was
//#define PRIORITY_PORT_DMA		6	// Dma Channel (Capture_Thread priority)
//#define PRIORITY_FLASH_WRITE    6	// Thread to Write to the Flash at a lower priority
//#define PRIORITY_1SEC_TIMER		6	// 1 Second Timer

#define PRIORITY_PORT_24		 	8	// Parmeter Data Port was
#define PRIORITY_PORT_23		 	1	// Telnet port
#define PRIORITY_PORT_22		 	8	// Data Port was
#define PRIORITY_PORT_DMA		 	7	// Dma Channel (Capture_Thread priority)
#define PRIORITY_FLASH_WRITE   		6	// Thread to Write to the Flash at a lower priority
#define PRIORITY_1SEC_TIMER	 		9	// 1 Second Timer

// #define TELNET_PRIORITY        	8  	//!< priority for debug interface monitoring
#define SERIAL_PRIORITY      	  	6  	//!< priority for serial port interface monitoring
#define NET_RX_PRIORITY        		12  //!< prioriry for network priority (10,11,12)
#define LWIP_INIT_PRIORITY      	5  	//!< priority for lwip initialization task
//#define SCOPE_PRIORITY         	11  //!< priority for scope 


#define  PRO_HDR_OPC_FW_DOWNLOAD           0x000B
#define  PRO_HDR_OPC_SW_DOWNLOAD           0x000C

//#define GPIO_TTL_INPUT_MASK        0x000000FF
//#define GPIO_OC_OUTPUT_MASK        0x0000FF00
//#define GPIO_OC_OUTPUT_SHIFT       8
//#define GPIO_LED_OUTPUT_MASK       0x00FF0000
//#define GPIO_LED_OUTPUT_SHIFT      16//
//#define GPIO_ADC_PWRDOWN_N_MASK    0xFF000000
//#define GPIO_ADC_PWRDOWN_N_SHIFT   24
//#define GPIO_INPUTS                (GPIO_TTL_INPUT_MASK)
//#define GPIO_OUTPUTS               (GPIO_OC_OUTPUT_MASK|GPIO_LED_OUTPUT_MASK|GPIO_ADC_PWRDOWN_N_MASK)

// These next macros are used for printing out network IP/MASK/GW addresses....
#define IP_EXPAND_PRINTF(x) \
x.addr&0xFF,(x.addr&0xFF00)>>8,(x.addr&0xFF0000)>>16,(x.addr&0xFF000000)>>24

#define BUILD_IP(a,b,c,d) \
((d<<24)|(c<<16)|(b<<8)|(a))

/**
*  The main application class.  This class runs/coordinates the PMS particle detection
*  MityDSP application.  This class is a singleton.
*
*/
class tcArtemisDsp
{
	public:
		static void InitializeDispatch(void);
//		static void AlteraDataReaderDispatch(void);
//		static void TelnetServerDispatch(void);
//		static void DataServerDispatch(void);
		static void OneSecDispatch( void );
		static void DppServerDispatch(void);
		static void ParamServerDispatch(void);
		static void FlashWriterDispatch(void);
		static void SerialMonitorDispatch(void);
		static void SendErrorDispatch(MityDSP::teErrorValues Error, const char* s);
		static void StackInitDispatch(void);
		static void SendSerial(const char* s, bool IsDebugData = false);

	protected:
		tcArtemisDsp(void);
		~tcArtemisDsp(void);
         
		int                     	Initialize(void);
		void                    	SerialMonitor(void);
		void                    	StackInit(void);
		void 						StackStats( void );
		void                    	HandleCommand(const char* command);
		void                    	DumpVersion(void);
		void                    	DoNetConfigCommand(const char *apCmdbuf);
		void 						Altera_Command( unsigned int addr );
		void 						Altera_Writer( unsigned int addr, unsigned int data );
		unsigned int 				Altera_Reader( unsigned int addr );
		void 						altera_write_flash( unsigned int addr, unsigned int data );
		int 						altera_read_flash( void );
		int 						Data_Test_Single( int addr );
		int 						Data_Test_Multiple( int addr );
		int 						Address_Test( void );

		int 						Run_BIT( void );
		void 						Altera_Flash_Erase_Init( void );

		void 						Altera_Flash_Init( void );
		int 						Altera_Flash_Read_ID( void );
		void 						Altera_Flash_Done( void );
		void 						Altera_Flash_Erase( int erase_time );
        static int 				    writeCodeToFlash( unsigned int anOpcode, MityDSP::tcDspFlash *lpFlash, unsigned int anLength, void *apBuffer );
	    static 	bool				DownLdParserCallback(int anStatus, void *apBuffer, int anLength, unsigned int anCksum, void *apUser);
		void 						hex2bin( void );
		void						OneSec_Thread(void);
		void 						Set_Standby( bool Home );
		void 						WriteFlash(void );
		void 						ReadFlash(void  );
		void						SetEthernetDefaults( void );
		void 						SetDefParams(void);
		int 						Param_Addr( int evch, int amptime, int addr  );
		int 						Generic_Param_Addr( int offset );
		void 						SetAmpTime( unsigned int );

		volatile unsigned int 		*mpAlteraBaseReg;
		int                      	send_str (int socket, const char *str);
		int                      	send_buff (int socket, const char *str, int len);
		int                      	DppConnect();
		int 						ParamConnect();
		int 						mnDppConnectedSocket;		//  TCP Socket for Altera Flash )
		int							mnParamConnectedSocket;		// 
		volatile unsigned int 		mn_dsp_ver;
		volatile unsigned int 		mn_xilinx_ver;
	   	static MityDSP::tcDspSerial *mpDebugPort;         	//!< pointer to debug serial port 
		tcNetDrvr*                  mpNetDrvr;           	//!< pointer to MityDSP Network/MAC core API
	   	struct netif                msNetIf;             	//!< lwip network interface structure
	   	tsNetIface                  msNetIface;          	//!< more lwip network interface structure info
	   	tcDspNetIFConfigUtil*       mpNetIFConfigUtil;   	//!< for maintaining IP configuration
		SEM_Handle					mhFlashSem;

		static MityDSP::tcDspBankSelect *mpBankSel;
		static MityDSP::tcDspFlash *mpFlash;                // declare mpFlash as a static member
		static char                 mnParseStatus;
        static int                  mnCodeDownLdType; 
};

#endif
