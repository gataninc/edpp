/***
 * \file TelnetEchoExample.cpp
 *
 * This file implements the tcArtemisDsp class.
 *
 *  \author  Critical Link LLC
 *
 *  \date    8/2009
 *
 *  \remarks
 *
 *  \verbatim

      o  0
      | /       Copyright (c) 2009
     (CL)---o   Critical Link, LLC
       \
        O

    \endverbatim
 **/
#include "ep98841.h"
#include "Artemis_Dma.h"
#include "version.h"
#include <core/DspFirmware.h>
#include <core/DspSerial.h>
#include <core/DspBankSelect.h>
#include <core/DspFlash.h>
#include <core/DspConfig.h>
#include <core/DspGpio.h>
#include <core/DspBootStrapper.h>
//#include <core/DspParseIHex.h>
#include <core/version.h>
#include <net/DspNetIFConfigUtil.h>
#include <net/DSPNetStack.h>
#include <net/net_drvr/net_drvr.h>
#include <assert.h>    
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string_extras.h>
#include <clk.h>
#include <tsk.h>
#include <gbl.h>
#include <net/wins/wins.h>
#include <stdint.h>
#include <math.h>
#include <csl_cache.h>

#include "core/DspError.h"
#include "net/DspNetIFConfigUtil.h"

using std::snprintf;

using namespace MityDSP;

tcArtemisDsp* gpExample = NULL;
tcArtemis_Dma* mpArtemis_Dma;

static char flash_in_buffer[20*1024];

#define kFLASH_DEF_PARAM		1
// #define kFLASH_NUM_PARAM 		( 0x3F + 1 )
#define kFLASH_NUM_PARAM 		0x80			// 	(iADR_RW_MAX-iADR_SYNC_REG)+1	// Number of Writable Parameters
#define kFLASH_NUM_AMPTIME 		8
#define kFLASH_NUM_EVCH 		2
#define kFLASH_NUM_OTHERS		512

#define kFLASH_MAX_TEST			5
#define kFLASH_DATA_SINGLE		kFLASH_NUM_OTHERS-kFLASH_MAX_TEST
#define kDPP_FLASH_SIZE 		( kFLASH_NUM_OTHERS + (kFLASH_NUM_EVCH*kFLASH_NUM_AMPTIME*kFLASH_NUM_PARAM)+kFLASH_DEF_PARAM ) * 4

static char _Error_Data[kFLASH_MAX_TEST*4];
static char _Dpp_Data[kDPP_FLASH_SIZE];
static char _Dpp_Flash_Data[kDPP_FLASH_SIZE];

// Define Stack Ring Buffer (ImageBuffer)
// #define IMAGE_BUF_SIZE 4096 * 1024	// Does not work
// #define IMAGE_BUF_SIZE 80 * 1024		// Original Changed back on 2/2/11 because of a slow-down in "Clear" - not confirmed if this fixes the problem.
// have seen a general slow-down with the larger buffer size 


static bool _Flash_Altera = false;
static bool _Flash_Xilinx = false;
static bool _Flash_DSP    = false;

static bool _SerialEnable = true;
static bool _SerialVerbose = false;
static bool _Flash_Busy	 = false;
static bool _DSP_Flash_Done = false;
static bool _XILINX_Flash_Done = false;
static bool _ALTERA_Flash_Done = false;
static bool _Altera_Erase_Done = false;

static int _Altera_Flash_Current_Page;
static int _Altera_Flash_Pages;
static int _Altera_Flash_Byte_Bal;
static int _Altera_Flash_Byte_Per_Page;
static int _Altera_Flash_Current_Byte;
static int _Dsp_Counts = 0;
static int _AutoOffDisable = 0;


#define MAX_CODE_DOWNLOAD_SZ 1048576
static char _flash_data[MAX_CODE_DOWNLOAD_SZ];
static int _flash_size;
static int _flash_ptr;
static bool _Flash_DSP_Error;
static bool _Flash_Xilinx_Error;
static int _flash_status_reset = 0;

#define out_buffer_len 8*1024
static char param_host_buffer[out_buffer_len];
#define one_sec_out_buffer_len 1*1024
static char onesec_host_buffer[one_sec_out_buffer_len];
//static int _write_ptr = 0;


// #define IMAGE_BUF_SIZE 1024 * 1024		// Does work
// #define IMAGE_BUF_SIZE 2048 * 1024		// Does work
//#define IMAGE_BUF_SIZE 128 * 1024		// Original Changed back on 2/2/11 because of a slow-down in "Clear" - not confirmed if this fixes the problem.
//#define IMAGE_BUF_SIZE 100000
#define IMAGE_BUF_MASK 0x1FFFF
#pragma DATA_ALIGN(CACHE_L2_LINESIZE)
static char gnImageBuffer[IMAGE_BUF_MASK];

// declare static member variables
tcDspBankSelect      *tcArtemisDsp::mpBankSel = NULL;	//Create global bank select use with global mpFlash
tcDspFlash           *tcArtemisDsp::mpFlash = NULL;

tcDspSerial          *tcArtemisDsp::mpDebugPort = NULL;
char                  tcArtemisDsp::mnParseStatus;
int                   tcArtemisDsp::mnCodeDownLdType = -1;  
char				 mpCodeDownLdBuf[MAX_CODE_DOWNLOAD_SZ];
// char *mpCodeDownLdBuf
tcDspParseIHex		 *mpParser;
int _amptime, _evch;
LCK_Handle ghFlashLock = LCK_create(NULL);	//try to ensure tasks don't trample flash access
#define LCK_TIMEOUT 1000 //give a task 1 sec to finish with flash


//-------------------------------------------------------------------------------------------------
tcArtemisDsp::tcArtemisDsp(void)
:  mnDppConnectedSocket(-1)
 , mnParamConnectedSocket(-1)

//:  mnConnectedSocket(-1)
// , mnDppConnectedSocket(-1)
// , mnParamConnectedSocket(-1)
{
	mpAlteraBaseReg[XA_IADR_DMA_EN_REG] 			= 0x0;					// Disable DMA

}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
tcArtemisDsp::~tcArtemisDsp(void)
{
    /* NOT REACHED */
}
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
void tcArtemisDsp::InitializeDispatch(void)
{
	gpExample = new tcArtemisDsp();
    gpExample->Initialize();
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
void tcArtemisDsp::SerialMonitorDispatch(void)
{
    gpExample->SerialMonitor();
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
void tcArtemisDsp::SendErrorDispatch(MityDSP::teErrorValues Error, const char* s)
{
    gpExample->SendSerial(s, true);
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
int tcArtemisDsp::Data_Test_Single( int addr )
{
	int error = 0;

	int i;
	unsigned int wdata;
	unsigned int rdata;

	for(i=0;i<32;i++)
	{
		wdata = 1 << i;
		Altera_Writer( awCmd_Write | addr, wdata );						// Write the data to the FPGA
		rdata = Altera_Reader( awCmd_Read | addr );									// Read the Data back from the FPGA
		if( wdata != rdata )											// if not the same data
			error |= ( 1 << i );										// Set the bit that failed
	}

	return error;
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
int tcArtemisDsp::Address_Test( void )
{
	int error = 0;
	int i;
	unsigned int wdata;
	unsigned int rdata;


	// First Write all of the data...
	wdata = 1;
	for( i=iADR_SYNC_REG;i<iADR_RW_MAX+1;i++)
	{
		Altera_Writer( awCmd_Write | i, wdata );						// Write the data to the FPGA
		wdata ++;
	}																			// for i...

	// Now go back and read it...
	wdata = 1;
	for( i=iADR_SYNC_REG;i<iADR_RW_MAX+1;i++)
	{
		rdata = Altera_Reader( awCmd_Read | i );									// Read the Data back from the FPGA
		if( wdata != rdata )
			error |= ( 1 << i );												// Set the bit that failed
		wdata ++;
	}		
	return error;
}
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
int tcArtemisDsp::Run_BIT( void )
{
	int error = 0;
	int i;
	unsigned int temp;
	int j;

	for(j=0;j<kFLASH_MAX_TEST;j++)
	{
		switch( j )
		{
			case 0 	: temp = Data_Test_Single( iADR_SYNC_REG );		break;	// Perform Data test on the "Test Register"	
			case 1  : temp = Address_Test();						break;	// Perform the Address test on all of the Read/Write Registers
			default : temp = 0;										break;
		}

		memcpy( &_Error_Data[j*4], &temp, 4 );
		if( temp > 0 )
		{
			for(i=0;i<32;i++)
			error +=( ( temp >> i ) & 0x1 );
		}
	}
	return( error );
}
//-------------------------------------------------------------------------------------------------
///////////////////////////////////////////////////////////////////////////////
///
/// Callback for the Intel HEX parse.  Note, the overall checksum is not used during download from
/// the web as we are relying:
/// 1) Checkum for each code download line
/// 2) Download of 192K bytes from the web (which spans multiple lines)
/// 3) TCP' reliability
///
/// @param[in] anStatus  Parser status.
/// @param[in] apBuffer  Buffer containing parsed data.
/// @param[in] anLength  Maximum offset used.
/// @param[in] anCksum   File overall checksum.
///
/// @return True to reset parser, false otherwise.
///
///////////////////////////////////////////////////////////////////////////////
bool tcArtemisDsp::DownLdParserCallback(int anStatus, void *apBuffer, int anLength, unsigned int anCksum, void *apUser)
{

    int               rc;
    static char       buffer[80];			// used to echo message back to serial port

     
    if ( (anStatus == (int)tcDspParserBase::eeParseOK) && (mnParseStatus != STAT_FAILURE ) )
    {
        mnParseStatus = STAT_SUCCESS;
        
        snprintf(buffer,sizeof(buffer),"\r\n  Writing Image to Flash ... \r\n");
		if( _SerialVerbose == true )
	        SendSerial(buffer);

        rc = writeCodeToFlash( mnCodeDownLdType, mpFlash, anLength, apBuffer);

        if (rc == anLength)
       {
			if( _Flash_DSP == true )
				_DSP_Flash_Done = true;

			if( _Flash_Xilinx == true )
				_XILINX_Flash_Done = true;

            snprintf(buffer,sizeof(buffer),"\r\n>>> The MityDSP needs to be rebooted for the new code to take effect\r\n");
//            snprintf(buffer,sizeof(buffer),"  >>> Rebooting MityDSP, please wait  \r\n");
			if( _SerialVerbose == true )
    	        SendSerial(buffer);
			TSK_sleep(1000);
               
		     // disable firmware interrupts
			//LCK_pend(ghFlashLock,LCK_TIMEOUT);
			//tcDspFirmware::global_int_disable(IE_MASK_4 | IE_MASK_5 | IE_MASK_6 | IE_MASK_7);
	        //tcDspBootstrapper::Reboot(mpFlash);
			//LCK_post(ghFlashLock);	//ok, should never get here but I like the balance
        }
       else
       {
            snprintf(buffer,sizeof(buffer),"  >>> Write to Flash failed \r\n");
			if( _SerialVerbose == true )
    	        SendSerial(buffer);
       }
    }
    else
    {    
		if( _Flash_DSP == true )
			_Flash_DSP_Error = true;

		if( _Flash_Xilinx == true )
			_Flash_Xilinx_Error = true;

        // only log first failure
        if (mnParseStatus != STAT_FAILURE)
        {
			switch( anStatus )
			{
				case -1 :  snprintf(buffer,sizeof(buffer),"\r\n>>> Download file error, <eeParseChecksumErr> paser error code: %d\r\n\tChecksum failure\r\n", anStatus); break;
				case -2 :  snprintf(buffer,sizeof(buffer),"\r\n>>> Download file error, <eeParseSyncErr> paser error code: %d\r\n\tData sync error\r\n", anStatus); break;
				case -3 :  snprintf(buffer,sizeof(buffer),"\r\n>>> Download file error, <eeParseBustdMsgErr> paser error code: %d\r\n\tBroken message (missing data)\r\n", anStatus); break;
				case -4 :  snprintf(buffer,sizeof(buffer),"\r\n>>> Download file error, <eeParseTooBigErr> paser error code: %d, %d\r\n\tResult too big for output buffer\r\n", anStatus, sizeof( apBuffer) ); break;
				case -5 :  snprintf(buffer,sizeof(buffer),"\r\n>>> Download file error, <eeParseBaseLast> paser error code: %d\r\n\tErrors beond this are outside base class\r\n", anStatus); break;
				default :  snprintf(buffer,sizeof(buffer),"\r\n>>> Download file error, paser error code: %d\r\n", anStatus); break;
			}
			if( _SerialVerbose == true )
    	        SendSerial(buffer);
        }
    }
	_Flash_Xilinx = false;
	_Flash_DSP = false;
    
    // reset parser after any error or complete message
    return(true);
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
int tcArtemisDsp::writeCodeToFlash( unsigned int anOpcode, tcDspFlash* lpFlash, unsigned int anLength, void *apBuffer )
{
    // Get Flash config block
    const tsConfigSect* lpFlashConfig = tcDspConfig::GetInstance(lpFlash)->GetConfigBlock();
    unsigned int lnLoadIndex;
    unsigned int lpLoadAddress;
    char temp;
    char    *lpCodeBuffer;
    int      rc;

    lpCodeBuffer = (char *) apBuffer;
    
    if ( PRO_HDR_OPC_SW_DOWNLOAD == anOpcode )
    {
        lnLoadIndex = lpFlashConfig->mnAppLoadIndex;
        lpLoadAddress = lpFlashConfig->msApplications[lnLoadIndex].mnOffset;
        //Convert buffer from little-endian to big-endian
        //(only needed for application code)
        for (int i=0; i<anLength; i+=4)
        {
            temp = lpCodeBuffer[i];
            lpCodeBuffer[i] = lpCodeBuffer[i+3];
            lpCodeBuffer[i+3] = temp;
            temp = lpCodeBuffer[i+1];
            lpCodeBuffer[i+1] = lpCodeBuffer[i+2];
            lpCodeBuffer[i+2] = temp;
        }
    }
    else
    {
        lnLoadIndex = lpFlashConfig->mnFpgaLoadIndex;
        lpLoadAddress = lpFlashConfig->msFPGA[lnLoadIndex].mnOffset;
    }
	rc = anLength;
	LCK_pend(ghFlashLock,LCK_TIMEOUT);
    // secret work to enable Flash Bank 0 writes
    lpFlash->overrideBank0(true, 0x6712);
    rc = lpFlash->write((void *)lpLoadAddress, lpCodeBuffer, anLength);
    
    // disable Flash Bank 0 writes
    lpFlash->overrideBank0(false);
	LCK_post(ghFlashLock);
    return rc;
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
/**
 *   The main application initialization and entry point.  The Initialize methods
 *   constructs all of the MityDSP API interfaces (C++ objects) and initializes
 *   the TCP/IP stack and spawns the application threads.
 */
int tcArtemisDsp::Initialize(void)
{
	static char 		buffer[512];
	unsigned int 		data;
    TSK_Attrs*      	attrs;
	int					serial_pause = 1000;
	_SerialEnable = true;
	_SerialVerbose = false;

     // disable firmware interrupts
    tcDspFirmware::global_int_disable(IE_MASK_4 | IE_MASK_5 | IE_MASK_6 | IE_MASK_7);

	//Create instance of flash access here
	mpBankSel = new tcDspBankSelect((void*)BANK_SEL_ADDR);
	mpFlash   = new tcDspFlash((void *)FLASH_BASE_ADDR, mpBankSel);

    // register error callback and set error reporting level
    tcDspError::meSendToStdout = status;
    tcDspError::mfCallback     = SendErrorDispatch;
    tcDspError::meOutputFormat = eeFormatB;
    tcDspError::meTimeFormat   = eeMsecSinceBoot;
  
    // display firmware global revision info
    tcDspFirmware::print_version_info(NULL);

	// create global flash access objects
	mpBankSel = new tcDspBankSelect((void*)BANK_SEL_ADDR);
    mpFlash   = new tcDspFlash((void *)FLASH_BASE_ADDR, mpBankSel);

    // create RS-232 serial interface
    mpDebugPort = new tcDspSerial((void *)RS232_SERIAL_BASE,
                                  gnAutoLevel,      // automatically determine vector
                                  UART_115200,      // set baud
                                  UART_LCR_CHR8,
                                  UART_LCR_1_STOP,
                                  UART_LCR_NOPARITY,
                                  false);            // no HW flow control
    
   // enable interrupts tied to system
    tcDspFirmware::global_int_enable(IE_MASK_4 | IE_MASK_5 | IE_MASK_6 | IE_MASK_7);

    // make sure hardware interrupts are enabled
    HWI_enable();

	// Clear Screen "Esc[2J" where ESC = 0x1B
	buffer[0] = 0x1B;	// Esc
	buffer[1] = 0x5B;	// [
	buffer[2] = 0x32;	// 2
	buffer[3] = 0x4A;	// J
	buffer[4] = 0x00;	// nul
	SendSerial( buffer );

    // launch stack initialization thread (and associated network threads)
    attrs = new TSK_Attrs;
    *attrs = TSK_ATTRS;
    attrs->name = "lwip_init";
    attrs->priority = LWIP_INIT_PRIORITY;
    attrs->stacksize = 8192*2;
    TSK_create((Fxn)StackInitDispatch, attrs);

    TSK_sleep(serial_pause);				// EDAX?		was 1000

    // launch serial port debug monitor thread
    attrs = new TSK_Attrs;
    *attrs = TSK_ATTRS;
    attrs->name = "serial_mon";
    attrs->priority = SERIAL_PRIORITY;
    attrs->stacksize = 8192;
    TSK_create((Fxn)SerialMonitorDispatch, attrs);

	// Create the Socket Connection for the DPP DAta
    // launch telnet TCP connect thread
    TSK_Attrs* attrs_22 = new TSK_Attrs;
    *attrs_22 = TSK_ATTRS;
    attrs_22->name = "Artemis_Port_22(DPP)";
    attrs_22->priority = PRIORITY_PORT_22;
    attrs_22->stacksize = 8192;
    TSK_create((Fxn)DppServerDispatch, attrs_22);

	// Create the Socket Connection for the DPP Parameters
    // launch telnet TCP connect thread
    TSK_Attrs* attrs_24 = new TSK_Attrs;
    *attrs_24 = TSK_ATTRS;
    attrs_24->name = "Artemis_Port_24(Paramters)";
    attrs_24->priority = PRIORITY_PORT_24;
    attrs_24->stacksize = 8192;
    TSK_create((Fxn)ParamServerDispatch, attrs_24);


    TSK_Attrs* attrs_flash = new TSK_Attrs;
    *attrs_flash = TSK_ATTRS;
    attrs_flash->name = "Artemis_Flash_Writter";
    attrs_flash->priority = PRIORITY_FLASH_WRITE;
    attrs_flash->stacksize = 8192;
	TSK_create( (Fxn)FlashWriterDispatch, attrs_flash);


    TSK_Attrs* attrs_OncSec = new TSK_Attrs;
    *attrs_OncSec = TSK_ATTRS;
    attrs_OncSec->name = "Artemis_1sec_Timer";
    attrs_OncSec->priority = PRIORITY_1SEC_TIMER;
    attrs_OncSec->stacksize = 8192;
	TSK_create( (Fxn)OneSecDispatch, attrs_flash);

	// Initialize output array and parser class for FLASH downloads
//	mpCodeDownLdBuf = new char [MAX_CODE_DOWNLOAD_SZ]; 
   	mpParser = new tcDspParseIHex(mpCodeDownLdBuf, MAX_CODE_DOWNLOAD_SZ ); 
  	mpParser->RegisterCallback(DownLdParserCallback); 

	//*********************************************************************************************
	// Now must initialize pointers for ALTERA_BASE address
	mpAlteraBaseReg = (unsigned int*) ALTERA_BASE;
	mpAlteraBaseReg[XA_IADR_WR_FLASH_ENABLE] = 0;			// Make sure Flash Mode is disabled
	Altera_Command( awCmd_Read_Data_Enable_Disable );		// Disable Data Collection in ALtera
	TSK_sleep(1);											// sleep for 1m Second
	mpAlteraBaseReg[XA_IADR_WR_FIFO_MR] 	= 0x01;			// Reset the Xilinx/Altera FIFOs
	TSK_sleep(100);											// sleep for 1m Second
	Altera_Command( awCmd_Fifo_mr);							// Reset the FIFOs in Altera
	TSK_sleep(100);

	//*********************************************************************************************
    TSK_sleep(1000);	

	//*********************************************************************************************
	// Before everything (Altera has been configured, run any built-in tests
	// snprintf( buffer, 512, "\r\n\nRunning Built-in Tests....." );
	// SendSerial( buffer );
	// int errors = Run_BIT();
	// snprintf( buffer, 512, "..... Found %d Errors\r\n", errors );
	// SendSerial( buffer );
	//*********************************************************************************************

	//*********************************************************************************************
	// Check to see if the Ethernet Default parameters must be set
	SetEthernetDefaults();
	//*********************************************************************************************

	//*********************************************************************************************
	// After the BIT enable the DMA Engine
	snprintf( buffer, 512, "Initializing DMA Engine....\r\n" );
	SendSerial( buffer );
	mpArtemis_Dma = new tcArtemis_Dma( (void *) ALTERA_BASE, PRIORITY_PORT_DMA );  // Must be below two stack thread priorities
	mpArtemis_Dma->CaptureFrames( gnImageBuffer, IMAGE_BUF_MASK );
	snprintf( buffer, 512, "     Address of gnImageBuffer @ [%x]\r\n\n", &gnImageBuffer );
	SendSerial( buffer );
	// After the BIT enable the DMA Engine
	//*********************************************************************************************


	//*********************************************************************************************
	// Acknowledge any interrupts 
	mpAlteraBaseReg[XA_IADR_RD_FPGA_STATUS] = 0xFF;					// write to the Status to acknowledge the intrrupt
	mpAlteraBaseReg[XA_IADR_WR_FIFO_MR] 	= 0x01;					// Reset the Xilinx/Altera FIFOs
	// Acknowledge any interrupts 
	//*********************************************************************************************


	//*********************************************************************************************
	// Read the Dsp and Xilinx Version numbers ----------------------------------------------------
	mn_dsp_ver = ( EDAX_VER_MINOR | EDAX_VER_MAJOR | EDAX_VER_APP_MAJOR | EDAX_VER_APP_MINOR | EDAX_VER_DAY | EDAX_VER_MON | EDAX_VER_YEAR );
	mn_xilinx_ver = mpAlteraBaseReg[XA_IADR_RD_VER1];
	TSK_sleep(100);
	snprintf(buffer,512,"     Dsp Ver [%x]\r\n     Xilinx Ver = [%x]\r\n\n", mn_dsp_ver, mn_xilinx_ver);
	SendSerial(buffer);
	// Read the Dsp and Xilinx Version numbers ----------------------------------------------------
	//*********************************************************************************************

	//*********************************************************************************************
	snprintf( buffer, 512, "Reading the DSP Flash Memory for DPP Parameters...\r\n");
	SendSerial( buffer );
	ReadFlash();			// Read the Flash into the approprite record

	//*********************************************************************************************
	// Now Update all AmpTime Parameters
	// extract the last amptime, evch and to set the new parameters
	memcpy( &data, &_Dpp_Data[0], 4 );		// Get the last amptime, evch and 
 	SetAmpTime( data  );
	//*********************************************************************************************

	// Force Detector offline and "Home it" 
	Set_Standby( true );

	//*********************************************************************************************

	//*********************************************************************************************
	snprintf( buffer, 512, "\r\n\nInitialization Completed\r\n\n" );
	SendSerial( buffer );
    TSK_sleep(serial_pause);				// EDAX?		was 1000
	//*********************************************************************************************

	//*********************************************************************************************
    return 0;
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
void tcArtemisDsp::SetEthernetDefaults()
{
	int tmp;
	static char 		buffer[512];
	tcDspNetIFConfigUtil::tsDspNetConfig lsConfig;

	tmp = mpAlteraBaseReg[XA_IADR_ETH_DEF];
	sprintf( buffer, "\r\nTesting Switch to set default ethernet parameters %x\r\n", tmp );
//	if( _SerialVerbose == true )
		SendSerial( buffer );

	if(( tmp & XILINX_ETHERNET_DEF ) == XILINX_ETHERNET_DEF ){
		sprintf( buffer, "\r\nSetting default ethernet parameters\r\n" );
		SendSerial( buffer );

		// Get current network config
	    mpNetIFConfigUtil->GetConfig(&lsConfig);
		lsConfig.msIpAddr.addr = BUILD_IP(192,168,0,100);
		lsConfig.msGateway.addr = BUILD_IP(192,168,0,1);
		lsConfig.msNetmask.addr = BUILD_IP(255,255,0,0);
		lsConfig.mnUseDHCP = 0;
		lsConfig.mnUseWINS = 0;

		TSK_sleep(1000);			// wait 1 second
		sprintf( buffer, "IP Addr\t\t= %d.%d.%d.%d\r\nGateway\t\t= %d.%d.%d.%d\r\nNetmask\t\t= %d.%d.%d.%d\r\nDHCP\t\t= disabled\r\nWins\t\t= disabled\r\n", IP_EXPAND_PRINTF(lsConfig.msIpAddr), IP_EXPAND_PRINTF(lsConfig.msGateway), IP_EXPAND_PRINTF(lsConfig.msNetmask));      									 		
		SendSerial( buffer );
		sprintf( buffer, "Configure local network card as follows\r\nIP Addr\t\t= 192.168.0.2\r\nSubmask\t\t= 255.255.0.0\r\nDef. Gateway\t= 192.168.0.1\r\n" ); 									
		SendSerial( buffer );
	    mpNetIFConfigUtil->UpdateConfig(&lsConfig);
	}
}

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
/**
 *   Command handler for example IOCheckOut application.
 *
 *   \param[in] command pointer to the command.
 */
void tcArtemisDsp::HandleCommand(const char* command)
{
    static char cmd[2048];
    static char response[2048];

    strncpy(cmd, command, 2048);

    // make string all upper case
    UPCASE(cmd);

    // remove characters that were backspaced over
    ERASE(cmd);

    SendSerial("\r\n");

    if (0 == strncmp(cmd,"HELP",4))
    {
        SendSerial("Serial Echo Commands\r\n" );
		SendSerial("   HELP                          -> Print this menu\r\n" );
		SendSerial("   NETCFG                         -> Print Network Info\r\n" );
		SendSerial("   NETCFG DHCP [0:1]              -> Enable DHCP on Next Boot\r\n");
		SendSerial("   NETCFG NETMASK aa.bb.cc.dd     -> Set NetMask on Next Boot\r\n");
		SendSerial("   NETCFG IPADDR aa.bb.cc.dd      -> Set net address on Next Boot\r\n");
		SendSerial("   NETCFG GATEWAY aa.bb.cc.dd     -> Set Gateway on Next Boot\r\n");
		SendSerial("   NETCFG WINSNAME NAME           -> Set Wins Name on Next Boot\r\n");
		SendSerial("   NETCFG WINS [0:1]              -> Enable Wins on Next Boot\r\n");
		SendSerial("   VER                            -> Print Version Info\r\n");
		SendSerial("   NETSTATS                       -> Print Network Statistics\r\n");
		snprintf( response, 2048, "   S[0:1]                         -> Serial Port Enable/Disable %d\r\n", _SerialEnable);
		SendSerial( response );

		snprintf( response, 2048, "   V[0:1]                         -> Serial Verbose Enable/Disable %d\r\n", _SerialVerbose );
		SendSerial( response);
		SendSerial("   RESET                          -> Reset MityDSP\r\n");

    }
    else if (0 == strncmp(cmd,"VER",3))
    {
        DumpVersion();
    }
    else if (strncmp(cmd,"NETCFG",6) == 0)
    {
        DoNetConfigCommand(cmd);
    }
	else if ( strncmp( cmd, "NETSTATS", 8 ) == 0 )
	{
		StackStats();
		stats_display();
	}
	else if ( strncmp( cmd, "S0", 2 ) == 0 )
	{
		_SerialEnable = false;
	}
	else if( strncmp( cmd, "S1", 2 ) == 0 )
	{
		_SerialEnable = true;
	}
	else if( strncmp( cmd, "V0", 2 ) == 0 )
	{
		_SerialVerbose = false;
	}
	else if( strncmp( cmd, "V1", 2 ) == 0 )
	{
		_SerialVerbose = true;
	}
	else if( strncmp( cmd, "RESET", 5 ) == 0 )
	{
        snprintf(response,sizeof(response),"  >>> Rebooting MityDSP, please wait  \r\n");
        SendSerial(response);               
		TSK_sleep(1000);
	     // disable firmware interrupts
		LCK_pend(ghFlashLock,LCK_TIMEOUT);
		tcDspFirmware::global_int_disable(IE_MASK_4 | IE_MASK_5 | IE_MASK_6 | IE_MASK_7);
        tcDspBootstrapper::Reboot(mpFlash);
		LCK_post(ghFlashLock);	//ok, should never get here but I like the balance
	}
    else
    {
        snprintf(response,2048,"Unknown Command : %s\r\n", cmd);
        SendSerial(response);
    }
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
/**
 *  Static callback method to spawn the TCP/IP stack initialization.
 */
void tcArtemisDsp::StackInitDispatch(void)
{
    gpExample->StackInit();
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
/**
 *   LWIP call backs are C style callbacks.
 */
extern "C"
{
   /** 
    *  This routine will dump out TCP/IP lwIP stack errors, if any...
    *
    *  \param msg the lwIP generated error message.
    */
   void LwipAssertReport(const char* msg)
   {
       gpExample->SendSerial(msg, false);
//       tcDspError::report(__FILE__,__LINE__,error, "LWIP ASSERT: %s", msg);
       TSK_sleep(100);
   }
//-------------------------------------------------------------------------------------------------
   
//-------------------------------------------------------------------------------------------------
   /** 
    *  This routine will dump out TCP/IP lwIP stack information messages..
    *
    *  \param fmt a printf style format followed by printf style varargs...
    */
   void LwipPrintf(const char* fmt, ...)
   {
        static char buffer[1024];
        va_list      ap;
        va_start(ap, fmt);
        vsprintf(buffer, fmt, ap );
        va_end(ap);
		strcat( buffer, "\r\n" );
        gpExample->SendSerial(buffer, false);
   }
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
/**
 *   This method initializes the ethernet TCP/IP stack (lwIP).
 * 
 *   Once the stack is initialized and a valid IP has beeen 
 *   acquired, threads using the TCP/IP sockets layer are 
 *   spawned.  
 */
void tcArtemisDsp::StackInit(void)
{
    unsigned int      lnTicksPerMsec = CLK_countspms() / CLK_getprd();
    tsDSPNetStackInit si;
#if 0 //1703
    tcDspBankSelect  *lpBankSel;
    tcDspFlash       *lpFlash;
#endif
    tcDspConfig      *lpConfig;
	int lostConfig = false;


    // Do low level stack initialization...
    DSPGetNetStackDefaults(&si);

    // Override all default stack limits as required (the si structure should be reviewed
    // by the application software engineering team)
    si.max_threads = 15;
    si.nacb_func = LwipAssertReport;
    si.tcpip_priority = NET_RX_PRIORITY-2;
    DSPNetStackInit(&si);
    
    lwip_printf = LwipPrintf;

#if 0 //1703
    lpBankSel = new tcDspBankSelect((void*)BANK_SEL_ADDR);
    lpFlash   = new tcDspFlash((void *)FLASH_BASE_ADDR, lpBankSel);
    lpConfig  = tcDspConfig::GetInstance(lpFlash);
	mpFlash   = lpFlash;
#else
    lpConfig  = tcDspConfig::GetInstance(mpFlash);
#endif

	{//we will read the network config and determine if bad
	    int  err;
		unsigned int *lWorkBfr;
		lWorkBfr = new unsigned int [256];
		int lFlashOffset = lpConfig->GetAppDataOffset();

	    mpFlash->read((void *)lFlashOffset, (void *)lWorkBfr, sizeof(int)*256, &err);
	    if (lWorkBfr[0] != DSP_NETIF_MAGICNUM)
	    {
			//was bad, we will want our defaults, so need to set after defaulted
			lostConfig = true;
		}
	}
//1703    mpNetIFConfigUtil = new tcDspNetIFConfigUtil(lpFlash, lpConfig, lpConfig->GetAppDataOffset());
    mpNetIFConfigUtil = new tcDspNetIFConfigUtil(mpFlash, lpConfig, lpConfig->GetAppDataOffset());
    tcDspNetIFConfigUtil::tsDspNetConfig lsConfig;
    mpNetIFConfigUtil->GetConfig(&lsConfig);

	if(lostConfig)
	{
		lsConfig.msIpAddr.addr = BUILD_IP(192,168,0,100);
		lsConfig.msGateway.addr = BUILD_IP(192,168,0,1);
		lsConfig.msNetmask.addr = BUILD_IP(255,255,0,0);
		lsConfig.mnUseDHCP = 0;
		lsConfig.mnUseWINS = 0;
	    mpNetIFConfigUtil->UpdateConfig(&lsConfig);
	}
    // instantiate the network driver
    mpNetDrvr = (tcNetDrvr_B *) new tcNetDrvr_B((unsigned char*)lsConfig.maMACAddress,
                                                (void*)ETHERNET_BASE);
    //memset(&msNetIface, 0, sizeof(tsNetIface));
    net_iface_set_defaults(&msNetIface, (tcNetDrvr *)mpNetDrvr);
    msNetIface.mnRxThreadPriority = NET_RX_PRIORITY;

    // make sure auto MDIX is enabled.
    memset(&msNetIf, 0, sizeof(struct netif));
    ((tcNetDrvr_B*)mpNetDrvr)->SetMIIRegister(0x19, (((tcNetDrvr_B*)mpNetDrvr)->GetMIIRegister(0x19) | 0x8000));

    tsNetPhyStatus lsStatus;
    ((tcNetDrvr_B*)mpNetDrvr)->GetPhyStatus (lsStatus);

    if (lsConfig.mnUseDHCP)
    {
        static struct ip_addr    ipaddr;
        static struct ip_addr    netmask;
        static struct ip_addr    gateway;

        memset(&ipaddr, 0, sizeof(ip_addr));
        memset(&netmask, 0, sizeof(ip_addr));
        memset(&gateway, 0, sizeof(ip_addr));

        netif_add(&msNetIf, &ipaddr, &netmask, &gateway,
                  &msNetIface, net_iface_init, tcpip_input);
        netif_set_default(&msNetIf);

        if (dhcp_start(&msNetIf) == ERR_OK)
        {
            tcDspError::report(__FILE__, __LINE__, status ,
                               "DHCP Initialized");
        }
        else
        {
            tcDspError::report(__FILE__, __LINE__, warning,
                               "DHCP Initialization Failed");
        }

        // Don't allow any more network activity until DHCP completes...
        while (netif_is_up(&msNetIf) == false)
        {
            TSK_sleep(1000 * lnTicksPerMsec);
        }
    }
    else
    {
        netif_add(&msNetIf, &lsConfig.msIpAddr, &lsConfig.msNetmask, &lsConfig.msGateway,
                  &msNetIface, net_iface_init, tcpip_input);
        netif_set_default(&msNetIf);
        netif_set_up(&msNetIf);
    }

    // report network settings
    tcDspError::report(__FILE__,__LINE__, status,
                       "\r\nNetwork Address = %d.%d.%d.%d\r\n",
                       msNetIf.ip_addr.addr&0xFF,
                       (msNetIf.ip_addr.addr&0xFF00) >> 8,
                       (msNetIf.ip_addr.addr&0xFF0000) >> 16,
                       (msNetIf.ip_addr.addr&0xFF000000) >> 24);

    // spawn polling server (for DSP Locator)
    mpNetIFConfigUtil->RunQueryServer(&msNetIf);

}
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
/**
 *   This method initializes the ethernet TCP/IP stack (lwIP).
 * 
 *   Once the stack is initialized and a valid IP has beeen 
 *   acquired, threads using the TCP/IP sockets layer are 
 *   spawned.
 *
 * Set the terminal window to:
 *  use LF in Received' Lines
 * use CR on Transmit Lines
   
 */
void tcArtemisDsp::StackStats(void)
{
	static char response[2048];

		tsNetDrvrStats lsStats;

		mpNetDrvr->get_stats(&lsStats);

		snprintf(response,2048,"DRIVER\r\n"); 
		SendSerial(response);

		snprintf(response,2048,"\tIsrCnt: %d\r\n",lsStats.mnIsrCnt); 
		SendSerial(response);

		snprintf( response, 2048, "\tRxPkts: %d\r\n", lsStats.mnRxPkts );
		SendSerial(response);

		snprintf( response, 2048, "\tRxBytes: %d\r\n", lsStats.mnRxBytes );
		SendSerial(response);

		snprintf( response, 2048, "\tRxBadPkts: %d\r\n", lsStats.mnRxBadPkts );
		SendSerial(response);

		snprintf( response, 2048, "\tTxPkts: %d\r\n", lsStats.mnTxPkts );
		SendSerial(response);

		snprintf( response, 2048, "\tTxBytes: %d\r\n", lsStats.mnTxBytes );
		SendSerial(response);

		snprintf( response, 2048, "\tTxBadPkts: %d\r\n", lsStats.mnTxBadPkts );
		SendSerial(response);

		snprintf( response, 2048, "\tTxRetries: %d\r\n", lsStats.mnTxRetries );
		SendSerial(response);

}	
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
// Static function to launch the Telnet Connection thread
void tcArtemisDsp::DppServerDispatch(void)
{
    // Start listening for connections
    gpExample->DppConnect();
}
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
// Static function to launch the Telnet Connection thread
void tcArtemisDsp::ParamServerDispatch(void)
{
    // Start listening for connections
    gpExample->ParamConnect();
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
// Static function to launch the Telnet Connection thread
void tcArtemisDsp::FlashWriterDispatch(void)
{
    // Start listening for connections
    gpExample->WriteFlash();
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
// Static function to launch the Telnet Connection thread
void tcArtemisDsp::OneSecDispatch(void)
{
    // Start listening for connections
    gpExample->OneSec_Thread();
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
/**
 *   This thread monitors the serial port for activity and
 *   routes the data to the serial command parser.
 */
void tcArtemisDsp::SerialMonitor(void)
{
    int  index;
    int  bytes;
    int  command_index = 0;
    char buffer[128];
    static char command[1024];  // keep the stack small...

    // One-time initialization. Register this thread for LWIP usage
    sys_thread_register(TSK_self());

    while(true)
    {
        do
        {
            // read some data
            bytes = mpDebugPort->get(buffer, 128);
            
            if (bytes > 0)
            {
                
                mpDebugPort->put(buffer, bytes);		// Local Serial echo
                buffer[bytes] = '\0';					// Null terminate the buffer 

                for (index = 0; index < bytes; index++)
                {
                    if (buffer[index] != '\n')
                    {
                        if ( (buffer[index] == '\r') or (command_index == 1023) )
                        {
                            command[command_index] = 0;
                            HandleCommand(command);
                            command_index = 0;
                        }
                        else
                        {
                            command[command_index++] = buffer[index];
                        }
                    }
                }
            }
        } while (bytes != 0);
        TSK_sleep(10);
    }
}
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
/**
 * This function sends the specified string to the client associated with
 * the specified socket.
 * \param     telnetconn The socket to send to.
 * \param     str        The zero terminated string to send.
 * \return    0 if successful, -1 if the connection is closed.
 */
int tcArtemisDsp::send_buff (int socket, const char *str, int len)
{
    if (socket < 0)
    {
        return -1;
    }
    // Write out the string
    if (write (socket, (void*) str, len ) < 0)
    {
        close (socket);
        return -1;
    }
    return 0;
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
/**
 *   This routine routes debug or user context messages to the 
 *   connected serial console.
 *
 *   \param[in] s pointer to null terminated string to issue
 *   \param[in] IsDebugData flag indicating data was generated by tcDspError::report method.
 *
 */
void tcArtemisDsp::SendSerial(const char* s, bool IsDebugData)
{

    if(( _SerialEnable == true ) &&  (NULL != mpDebugPort))
    {
        int rc;
        int bytes = 0;
        int bytes_to_go = strlen(s);
        while (bytes < bytes_to_go)
        {
           rc = mpDebugPort->put(&s[bytes], bytes_to_go, 2, 100);
           if (rc > 0)
              bytes+=rc;
        }
    }

}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
/**
 *  This routine handles the NETCONFIG commands from the command handler.
 */
void tcArtemisDsp::DoNetConfigCommand(const char *apCmdbuf)
{
    char cmd[64], arg1[64], arg2[64];
    char buffer[128];
    tcDspNetIFConfigUtil::tsDspNetConfig lsConfig;

    tsNetPhyStatus lsStatus;
    ((tcNetDrvr_B*)mpNetDrvr)->GetPhyStatus (lsStatus);		

    int narg;
    int a1,a2,a3,a4;

    narg = sscanf(apCmdbuf,"%s %s %s",cmd, arg1, arg2);

    mpNetIFConfigUtil->GetConfig(&lsConfig);


    if (narg == 1)
    {
        snprintf(buffer,128,"Current Network settings:\r\n");									        	SendSerial(buffer);
        snprintf(buffer,128,"\tDHCP Enable\t- %s\r\n", !msNetIf.dhcp ? "no" : (msNetIf.dhcp->state == DHCP_OFF) ? "no" : "yes");        SendSerial(buffer);
        snprintf(buffer,128,"\tIP Address\t- %d.%d.%d.%d\r\n", IP_EXPAND_PRINTF(msNetIf.ip_addr) );        	SendSerial(buffer);
        snprintf(buffer,128,"\tIP GateWay\t- %d.%d.%d.%d\r\n", IP_EXPAND_PRINTF(msNetIf.gw) );        		SendSerial(buffer);
        snprintf(buffer,128,"\tIP NetMask\t- %d.%d.%d.%d\r\n", IP_EXPAND_PRINTF(msNetIf.netmask) );			SendSerial(buffer);
        snprintf(buffer,128,"Network settings applied on next boot:\r\n");								    SendSerial(buffer);
        snprintf(buffer,128,"\tDHCP Enabled\t- %s\r\n", lsConfig.mnUseDHCP ? "yes" : "no");			        SendSerial(buffer);
        snprintf(buffer,128,"\tWINS Enabled\t- %s\r\n", lsConfig.mnUseWINS ? "yes" : "no");			        SendSerial(buffer);
        snprintf(buffer,128,"\tIP Address\t- %d.%d.%d.%d\r\n", IP_EXPAND_PRINTF(lsConfig.msIpAddr) );	    SendSerial(buffer);
        snprintf(buffer,128,"\tIP GateWay\t- %d.%d.%d.%d\r\n", IP_EXPAND_PRINTF(lsConfig.msGateway) );     	SendSerial(buffer);
        snprintf(buffer,128,"\tIP NetMask\t- %d.%d.%d.%d\r\n", IP_EXPAND_PRINTF(lsConfig.msNetmask) );     	SendSerial(buffer);
        snprintf(buffer,128,"\tWINS Name\t- %s\r\n",  lsConfig.maNetBIOSName);					        	SendSerial(buffer);
		snprintf(buffer,128,"\tLink Status\t- %s\r\n", lsStatus.mbLinkUp ? "Connected" : "Disconnected" );	SendSerial(buffer);

//		snprintf(buffer,128,"   Link Status     -%d\r\n", netif_is_link_up(&msNetIf));	        				
//		SendSerial(buffer);
//		tcDspNetPhy::tcDspNetPhy(tcDspNetPhy::mpBaseAddr) lsDspPhy;
//		snprintf(buffer,128,"   Link Status     -%d\r\n", tcDspNetPhy::LinkUp( tcNetDrvr_B::mnPhyAddr );
//		snprintf(buffer,128,"   Link Status     -%d\r\n", tcNetDrvr::LinkUp ); SendSerial(buffer);
//		snprintf(buffer,128,"	Link Status		-%d\r\n", msNetIf.flags ); SendSerial(buffer);
//		int link = netif_is_link_up(&msNetIf);

    }
    else if (narg == 3)
    {
        UPCASE(arg1);
        bool args_ok = true;
        if (strncmp(arg1,"DHCP",4) == 0)
        {
            if (arg2[0] == '0')
            {
                lsConfig.mnUseDHCP = 0;
                snprintf(buffer, 128, "Disabling DHCP on next boot\r\n");
            }
            else
            {
                lsConfig.mnUseDHCP = 1;
                snprintf(buffer, 128, "Enabling DHCP on next boot\r\n");
            }
        }
        else if (strncmp(arg1,"NETMASK",7) == 0)
        {
            sscanf(arg2,"%d.%d.%d.%d",&a1,&a2,&a3,&a4);
            lsConfig.msNetmask.addr = BUILD_IP(a1,a2,a3,a4);
            snprintf(buffer, 128, "Setting NETMASK to %d.%d.%d.%d on next boot\r\n",IP_EXPAND_PRINTF(lsConfig.msNetmask));
        }
        else if (strncmp(arg1,"IPADDR",6) == 0)
        {
            sscanf(arg2,"%d.%d.%d.%d",&a1,&a2,&a3,&a4);
            lsConfig.msIpAddr.addr = BUILD_IP(a1,a2,a3,a4);
            snprintf(buffer, 128, "Setting IPADDR to %d.%d.%d.%d on next boot\r\n",IP_EXPAND_PRINTF(lsConfig.msIpAddr));
        }
        else if (strncmp(arg1,"GATEWAY",7) == 0)
        {
            sscanf(arg2,"%d.%d.%d.%d",&a1,&a2,&a3,&a4);
            lsConfig.msGateway.addr = BUILD_IP(a1,a2,a3,a4);
            snprintf(buffer, 128, "Setting GATEWAY to %d.%d.%d.%d on next boot\r\n",IP_EXPAND_PRINTF(lsConfig.msGateway));
        }
        else if (strncmp(arg1,"WINSNAME",8) == 0)
        {
            strncpy(lsConfig.maNetBIOSName,arg2,16);
            snprintf(buffer, 128, "Updating WINS name to %s\r\n",lsConfig.maNetBIOSName);
        }
        else if (strncmp(arg1,"WINS",4) == 0)
        {
            if (arg2[0] == '0')
            {
                lsConfig.mnUseWINS = 0;
                snprintf(buffer, 128, "Disabling WINS on next boot\r\n");
            }
            else
            {
                lsConfig.mnUseWINS = 1;
                snprintf(buffer, 128, "Enabling WINS on next boot\r\n");
            }
        }
        else
        {
            snprintf(buffer, 128, "Invalid syntax, type help for command options\r\n");
            SendSerial(buffer);
            args_ok = false;
        }
        if (args_ok)
        {
            SendSerial(buffer);
            mpNetIFConfigUtil->UpdateConfig(&lsConfig);
        }
    }
    else
    {
        snprintf(buffer, 128, "Invalid syntax, type help for command options\r\n");
        SendSerial(buffer);
    }
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
/**
 *  Dumps relavent version information of linked libraries, code gen tools, BIOS,
 *  and application data to the debug interface port.
 */
void tcArtemisDsp::DumpVersion(void)
{
    char buffer[128];
    Uint16 version;
    int major, minor, build;

    // Dump BIOS Version Info
    version = GBL_getVersion();
    snprintf(buffer,128,"BIOS Version %d.%d.%d", (version>>12), ((version&0x0F00)>>8), (version&0x00FF)); 
    SendSerial(buffer);
    
    // Dump TI Code Generator Tools Info
    major = __TI_COMPILER_VERSION__/1000000;
    minor = __TI_COMPILER_VERSION__-(major*1000000);
    build = minor;
    minor /= 1000;
    build -= minor*1000;
    snprintf(buffer, 128,"TI Code Gen Tools %d.%d.%d\r\n", major, minor, build); 
    SendSerial(buffer);   

    
    // Dump MDK revision info
    snprintf(buffer,128,"%s : Built %s\r\n", dsp_sdk_str, dsp_sdk_date);
    SendSerial(buffer);

    // Dump FPGA application info
    tuFirmwareVersion luVer = tcDspFirmware::get_version_info((void*)0xB0000000);
    snprintf(buffer,128,"FPGA Version : %d.%02d, built %02d/%02d/%04d\r\n", luVer.msBits.majorRev, luVer.msBits.minorRev,
                         luVer.msBits.buildMonth,  luVer.msBits.buildDay,  luVer.msBits.buildYear+2000);
    SendSerial(buffer);
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
void tcArtemisDsp::SetDefParams( void )
{
	int amptime, param, evch;

	unsigned int data;
	bool Orig_TEC_Rev = false;

	data = 6;				// Set Amptime to 7.68us
	data |= ( 1 << 12 );	// set 10ev/ch
	memcpy( &_Dpp_Data[0], &data, 4 );

	// First get the Version info for the TEC Control Board
	data = Altera_Reader( awCmd_Read | iADR_TEC_VER );							// Read back the data from the Altera FPGA
	data &= 0xFF;				// only keep lower 8 bits
	switch( data )
	{
		case 0x00 :
		case 0x01 :
		case 0x10 :
			Orig_TEC_Rev = true;
			break;
		default : 
			Orig_TEC_Rev = false;
			break;
	}


		


	for(evch=0;evch<kFLASH_NUM_EVCH;evch++)
	{
		for( amptime=0;amptime<kFLASH_NUM_AMPTIME;amptime++)
		{
			for( param=0;param<kFLASH_NUM_PARAM;param++)
			{
				switch( param)
				{
					case iADR_SYNC_REG :
							data = 0;
							break;

					case iADR_CWORD : 							// Control Word
							data = ( 1 << CW_SLIDE_EN );		// Enable Motorized Slide
							data |= ( 1 << CW_PGA_ENABLE );		// Set ADC PGA Enable Bit active (by default)
//							data |= ( 1 << CW_ADC_RANDOM );		// Enable Randomize from ADC
//							data |= ( 1 << CW_WEINER_EN );		// Enable Weiner Filter
							break;	
					case iADR_ATCWORD :							// Amp Time Control Word
							data = 18;							// Gap Time = 200 ns
							data |= ( 1 << ATCW_BLR_LSB );		// BLR = 1
							break;

					case iADR_EVCH_GAIN			: data = 230; break; // 160;	break;					// 5eV/Ch and 10eV/Ch both the same
					case iADR_GAIN				: data = 65536; break;					// Coarse Gain - Unity
					case iADR_OFFSET			: data = 0x0;	break;					// Offset
					case iADR_DISC_0			: data = 300;	break;					// Disc Thresold Level - SH
					case iADR_DISC_1			: data = 250;	break;					// Disc Thresold Level - H
					case iADR_DISC_2			: data = 150;	break;					// Disc Thresold Level - M
					case iADR_DISC_3			: data = 125;	break;					// Disc Thresold Level - L
					case iADR_DISC_4			: data =  75;	break;					// Disc Thresold Level - B (AmpTime/2)
					case iADR_CLIP		    	: data = ( 4000<<16);					// Clip Max
											      data |= ( 9 - amptime ) * (2-evch);	// Clip Min
												  break;
					case iADR_PRESET			: data = (unsigned int) 1000000000/10;	break;	// Preset Value
					case iADR_SCABB		    	: data = 0x0;	break;					// SCA/BB Parameters
					case iADR_Pixels_Line		: data = 256;	break;					// Pixels per frame
					case iADR_Lines_Frame		: data = 200; 	break;					// total Frames
					case iADR_DSS_REG       	: data = 0x0;	break;					// Static DSS Settings
					case iADR_DSS_INT       	: data = 1000;	break;					// Interval Counter
					case iADR_DSS_MAX       	: data = 500;	break;					// Maximum Count
					case iADR_BIT_PEAK			: data = (3000 << 16 ) | 100;	break;	// BIT PEak 1 and PEak 2
					case iADR_BIT_TIME			: data = 1000;		break;				// BIT Peak Period and peak2 delay
					case iADR_HV_CTRL			: data = 0x02000; break;				// default HV is disalbed ( off-line)
					case iADR_TARGET_ADC		: data = -25 * 65536; break;			// setpoint for -30C
					case iADR_RESET_PW			: data = 49;	break;					// 300 us
					case iADR_TEC2_DELAY		: data = 1000; break;					// 1 second
					case iADR_PA_TEC2_KIKP		: data = 200 << 16;						// Constant I (200)
											      data |= 4000;							// Constant P  (4000)
											  break;
					case iADR_PA_TEMP_OFFSET	: data = 0 * 65536; break;
					case iADR_SLIDE_HCR			: data = 50000;	break;
					case iADR_SLIDE_HCT			: data = 10000; break;
					case iADR_MSlide_Tar		: data = 0; break;						// Target Slide Position
					case iADR_MSlide_Vel_Max	: data = 32; break;
					case iADR_MSlide_Vel_Min	: data = 7; break;
					case iADR_OTR_FACTOR		: data = 8 * 65536; break;
					case iADR_RTIME_THR			: data = 25; break;

					case iADR_TEC1_FACTOR		: if( Orig_TEC_Rev == true )
													data = 65536 * 2; 
												  else
												 	data = (unsigned int) (65536 * 1.05f );
												  break;

					case iADR_POT_VAL 			: if( Orig_TEC_Rev == true )
												  {
														data = ( 1 << 31 );					// Enable DTM Cooling
													 	data |=  ( 117 <<  8 );				// TEC 1
											      		data |= ( 102 << 16 );				// TEC 2
												  }
												  else
												  {
														data = ( 1 << 31 );					// Enable DTM Cooling
													 	data |=  ( 115 <<  8 );				// TEC 1
											      		data |= ( 150 << 16 );				// TEC 2
												  }
												  break;				
					case iADR_WF_COEFA			: data = 0.25 * 65536; break;
					case iADR_WF_COEFB			: data = 1.00 * 65536; break;
					case iADR_WF_COEFC			: data = -0.25 * 65536; break;
					case iADR_WF_DELAY			: data = 1000 / 100; break;
					default 					: data = 0x0; 	break;


				}
				memcpy( &_Dpp_Data[Param_Addr( evch, amptime, param)], &data, 4 );
			}
		}
	}

	// Don't Do This
	/*
	for( int i=0;i<kFLASH_NUM_OTHERS;i++)
	{
		data = 0; // i;
		memcpy( &_Dpp_Data[Generic_Param_Addr(i)], &data, 4 );
	}
	*/
	
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
int tcArtemisDsp::Param_Addr( int evch, int amptime, int addr  )
{
	int tmp;
	tmp =                                                                 1 * 4;		// 1st 4-byes for ev/ch, Amptime and 
	tmp +=                                                             addr * 4;		// Which parameter in an amptime
	tmp +=                                       amptime * kFLASH_NUM_PARAM * 4;		// Which Amp TIme
	tmp +=                     evch * kFLASH_NUM_AMPTIME * kFLASH_NUM_PARAM * 4;		// Which evch
	return( tmp );
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
int tcArtemisDsp::Generic_Param_Addr( int offset )
{
	int tmp;
	tmp = ((kFLASH_NUM_EVCH*kFLASH_NUM_AMPTIME*kFLASH_NUM_PARAM)+kFLASH_DEF_PARAM ) * 4;
	if( offset > ( kFLASH_NUM_OTHERS-1) )
		offset = ( kFLASH_NUM_OTHERS - 1 );
	tmp += ( offset * 4 );																			// which offset...

	return( tmp );				
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
void tcArtemisDsp::ReadFlash()
{
#if 0 //1703
    tcDspBankSelect  *lpBankSel;
    tcDspFlash       *lpFlash;
#endif
    tcDspConfig      *lpConfig;
	int flash_offset;
	int i;
	static char out_buffer[512];

#if 0 //1703
    lpBankSel 		= new tcDspBankSelect((void*)BANK_SEL_ADDR);
    lpFlash   		= new tcDspFlash((void *)FLASH_BASE_ADDR, lpBankSel);
    lpConfig  		= tcDspConfig::GetInstance(lpFlash);
#else
    lpConfig  		= tcDspConfig::GetInstance(mpFlash);
#endif

	snprintf(out_buffer,512,"Reading Flash....." );
	SendSerial(out_buffer);

	// Calculate the Offset to the Flash for the dataset
	flash_offset = lpConfig->GetAppDataOffset() + DSP_NETIF_BLOCKSIZE;

	LCK_pend(ghFlashLock,LCK_TIMEOUT);
	//Read the Flash Memory
//	lpFlash->read( (void *)flash_offset, &_Dpp_Data[0], kDPP_FLASH_SIZE );	
	mpFlash->read( (void *)flash_offset, &_Dpp_Data[0], kDPP_FLASH_SIZE );	
	LCK_post(ghFlashLock);


	// Now Copy "Error Data to the top of the _Dpp_Data
	for(i=0;i<kFLASH_MAX_TEST;i++)
		memcpy( &_Dpp_Data[Generic_Param_Addr( kFLASH_DATA_SINGLE+i)], &_Error_Data[i*4], 4 );

	snprintf(out_buffer,512,"Done\r\n" );
	SendSerial(out_buffer);
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
// Thread to write parameter data to the flash
// it wait for a binary semaphore to be posted
// then it asserts a busy flag ( to hold off any additional postings )
//	and writes the data to the flash memory

void tcArtemisDsp::WriteFlash(void  )
{
#if 0 //1703
    tcDspBankSelect  *lpBankSel;
    tcDspFlash       *lpFlash;
#endif
    tcDspConfig      *lpConfig;
	int flash_offset;
	static char out_buffer[512];

   	mhFlashSem = SEM_create(0,NULL);
	sys_thread_register( TSK_self());

#if 0 //1703
    lpBankSel 		= new tcDspBankSelect((void*)BANK_SEL_ADDR);
    lpFlash   		= new tcDspFlash((void *)FLASH_BASE_ADDR, lpBankSel);
    lpConfig  		= tcDspConfig::GetInstance(lpFlash);
#else
    lpConfig  		= tcDspConfig::GetInstance(mpFlash);
#endif
	// Calculate the Offset to the Flash for the dataset
	flash_offset = lpConfig->GetAppDataOffset() + DSP_NETIF_BLOCKSIZE;

	while(1) 
	{
		if( SEM_pendBinary( mhFlashSem, SYS_FOREVER ))			//	SEM_pend( mhFlashSem, SYS_FOREVER )
		{
			snprintf(out_buffer,512,"\r\nWriting to Flash...."  );
			SendSerial(out_buffer);

			// Write to the Flash Memory
			_Flash_Busy = true;
//			lpFlash->write( (void *)flash_offset, &_Dpp_Data[0], kDPP_FLASH_SIZE );		
			LCK_pend(ghFlashLock,LCK_TIMEOUT);
//			lpFlash->write( (void *)flash_offset, &_Dpp_Flash_Data[0], kDPP_FLASH_SIZE );		
			mpFlash->write( (void *)flash_offset, &_Dpp_Flash_Data[0], kDPP_FLASH_SIZE );		
			LCK_post(ghFlashLock);
			snprintf(out_buffer,512,"...Done\r\n" );
			SendSerial(out_buffer);
			_Flash_Busy = false;
		}										// if SEM_pend
	}											// While(1)										// While(1)
}	
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
void tcArtemisDsp::SetAmpTime( unsigned int data  )
{

	unsigned int tmp;
	static char out_buffer[512];

	int evch, amptime, addr;
	bool reload;

	amptime = (int)( data >> 0 ) & 0x7;
	evch =    (int)( data >> 12 ) & 0x1;
	_AutoOffDisable = (int)( data >> 16 ) & 0x1;

	tmp = (evch << 12 ) | amptime;			
	memcpy( &_Dpp_Data[0], &tmp, 4 );		// Now update _Dpp_Data[0] with Amp Time
	snprintf(out_buffer,512,"Changing AmpTime %d /evch %d [%x]\r\n", amptime, evch, tmp );
	if( _SerialVerbose == true )
		SendSerial(out_buffer);

	// Stop any acquisitions
	snprintf(out_buffer,512,"     Stopping Acquistion\r\n" );
	if( _SerialVerbose == true )
		SendSerial(out_buffer);
	Altera_Command( awCmd_Time_Start_Stop | kSTARTSTOP_STOP );

	// Write the new AmpTime to Altera
	snprintf(out_buffer,512,"     Setting New AmpTime in Altera\r\n" );
	if( _SerialVerbose == true )
		SendSerial(out_buffer);
	tmp = ( evch << 12 ) | amptime;
	Altera_Command( awCmd_AmpTime | tmp );				// Now Write it to the FPGA

	// Now send the command to write the rest of the data to the Altera FPGA
	snprintf(out_buffer,512,"     Sending All AmpTime Parameters\r\n" );
	if( _SerialVerbose == true )
		SendSerial(out_buffer);

	// Copy all parameters to the Altera FPGA
	for(addr=iADR_SYNC_REG;addr<iADR_RW_MAX;addr++){
		reload = false;
		memcpy( &tmp, &_Dpp_Data[Param_Addr( evch, amptime, addr)], 4 );
		// Now some "Range Checking"
		switch( addr )
		{
			// if TEC2 Delay is less than 10 ms - reset it to 45 seconds
			case iADR_TEC2_DELAY :
				if( tmp < 10 )
				{
					tmp = 45000;
					reload = true;
				}
				break;

			// case iADR_HV_CTRL :
			// 	if( tmp == 0 )
			// 	{
			//		tmp = 0x02000; 				// default HV is disalbed ( off-line)
			//		reload = true;
			//	}
			//	break;

			default : 
				break;
		}
		if( reload == true )
		{
			memcpy( &_Dpp_Data[Param_Addr( evch, amptime, addr)], &tmp, 4 );
		}

		Altera_Writer( awCmd_Write | addr, tmp );						// Write the data to the FPGA
	}

	// Reset the Digital Filters
	snprintf(out_buffer,512,"     Resetting the Digital Filters\r\n" );
	if( _SerialVerbose == true )
		SendSerial(out_buffer);
	Altera_Command( awCmd_Fir_Mr );
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
void tcArtemisDsp::Set_Standby( bool Home )
{
	static char buffer[2048];
	unsigned int data;
	int levch;
	int lamptime;

	snprintf(buffer,128,"\r\nSet the Detector Off-Line\r\n" );
	SendSerial(buffer);

	memcpy( &data, &_Dpp_Data[Param_Addr(_evch,_amptime,iADR_HV_CTRL)], 4 );			// Get the currnet HV_CWord
	data &= ~( 1 << HVCW_ONLINE );													// Clear the HVCW OnLine bit
	Altera_Writer( awCmd_Write | iADR_HV_CTRL, data);								// Write it to the DPP

	for(levch=0;levch<kFLASH_NUM_EVCH;levch++)
	{
		for(lamptime=0;lamptime<kFLASH_NUM_AMPTIME;lamptime++)
		{
			memcpy( &_Dpp_Data[Param_Addr(levch,lamptime,iADR_HV_CTRL)], &data, 4 );	// Rewrite the HV_CWord
		}
	}
	Altera_Command( awCmd_Slide_Move | MSlide_Stop );									// Stop the Slide
	TSK_sleep(1);
	Altera_Command( awCmd_Slide_Move | MSlide_Reset );									// Reset the Slide
	TSK_sleep(1);

	if( Home == true )
	{
		snprintf(buffer,128,"\r\nHome the Detector\r\n" );
		SendSerial(buffer);
		Altera_Command( awCmd_Slide_Move | MSlide_Find_Retract );						// "Home" the slide (slow retraction)
	} 
	else
	{
		snprintf(buffer,128,"\r\nRetect the Detector\r\n" );
		SendSerial(buffer);
		data = 0x0;
		Altera_Command( awCmd_Slide_Move | data );
		for(levch=0;levch<kFLASH_NUM_EVCH;levch++)
		{
			for(lamptime=0;lamptime<kFLASH_NUM_AMPTIME;lamptime++)
			{
				memcpy( &_Dpp_Data[Param_Addr(levch,lamptime,iADR_MSlide_Tar)], &data, 4 );	// Rewrite the Target_Position Register
			}
		}
	}
}

//-------------------------------------------------------------------------------------------------
// Initial Setup to Flash the Altera EEPROM
void tcArtemisDsp::Altera_Flash_Init( void )
{
	static char		out_buffer[512];
	int silicon_id;
	int rpd_filesize;
	int erase_time;

	snprintf( out_buffer, 512, "Starting Altera Flash\r\n" );
	if( _SerialVerbose == true )
		SendSerial( out_buffer );

	mpAlteraBaseReg[XA_IADR_DMA_EN_REG] = 0x0;

	altera_write_flash( XA_IADR_WR_FLASH_ENABLE, 0x01 );	// Set Flash Enable High
	altera_write_flash( XA_IADR_WR_FLASH_NCON  , 0x00 );	// Set nConfig Low ( Enter Programming Mode )
	altera_write_flash( XA_IADR_WR_FLASH_NCE   , 0x01 );	// Set nCE High
	altera_write_flash( XA_IADR_WR_FLASH_NCS   , 0x01 );	// Set NCS High

	// ----- Read Silicon ID ----------------------------------------------------------
	snprintf( out_buffer, 512, "Reading Silicon ID\r\n" );
	if( _SerialVerbose == true )
		SendSerial( out_buffer );
	silicon_id = Altera_Flash_Read_ID( );
	// ----- Read Silicon ID ----------------------------------------------------------

	rpd_filesize = 0;
	switch( silicon_id ) 
	{
		case EPCS1_ID 	: 
			rpd_filesize 	= EPCS1;  
			erase_time 		=   6;	
			snprintf( out_buffer, 512, "     Found EPCS1\r\n" ); 
			break;

		case EPCS4_ID 	: 
			rpd_filesize 	= EPCS4;  
			erase_time 		=  10;	
			snprintf( out_buffer, 512, "     Found EPCS4\r\n" ); 
			break;

		case EPCS16_ID 	: 
			rpd_filesize 	= EPCS16; 
			erase_time 		=  40;	
			snprintf( out_buffer, 512, "     Found EPCS16\r\n" ); 
			break;

		case EPCS64_ID 	: 
			rpd_filesize 	= EPCS64; 
			erase_time 		= 160;	
			snprintf( out_buffer, 512, "     Found EPCS64\r\n" ); 
			break;

		default			: 
			rpd_filesize 	= 0; 	 
			erase_time 		= 0;	
			snprintf( out_buffer, 512, "FLASH ABORTED: Device not found Silicon ID [%x]\r\n", silicon_id ); 
			Altera_Flash_Done();
			break;
	 }		 						
	if( _SerialVerbose == true )
		 SendSerial( out_buffer );

	 if( _flash_size < rpd_filesize )
	 {
			snprintf( out_buffer, 512, "FLASH ABORTED: Not enough data found" ); 
			if( _SerialVerbose == true )
			 	SendSerial( out_buffer );
	 }

	// Valid Flash Found - OK To Erase and Start the programming process
	if( rpd_filesize > 0 )
	{
		Altera_Flash_Erase( erase_time );	// Bulk Erase 
		snprintf( out_buffer, 512, "Page Programing Setup\r\n" );
		if( _SerialVerbose == true )
			SendSerial( out_buffer );

		_Altera_Flash_Pages 	= rpd_filesize / 256;		// How many pages to be programmed
		_Altera_Flash_Byte_Bal 	= rpd_filesize % 256;		// how may bytes left in last page	
		
		// If there is any balance after the divide, program the balance on the next page
		if( _Altera_Flash_Byte_Bal > 0 )
			_Altera_Flash_Pages ++;

		altera_write_flash( XA_IADR_WR_FLASH_NCS, 		0x00 );				// Set NCS Low
		altera_write_flash( XA_IADR_WR_FLASH_BYTE_MSB, AS_WRITE_ENABLE );	// Send the Write Enable ( Start Page Programming
		altera_write_flash( XA_IADR_WR_FLASH_NCS,      	0x01 );				// Set NCS High
		_Altera_Flash_Byte_Per_Page = 256;
		_Altera_Flash_Current_Byte = 0;
		_Altera_Flash_Current_Page = 0;
	}

}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
// Initial Setup to Flash the Altera EEPROM
void tcArtemisDsp::Altera_Flash_Erase_Init( void )
{
	static char		out_buffer[512];
	int silicon_id;
	int erase_time;

	snprintf( out_buffer, 512, "Starting Altera Flash\r\n" );
	if( _SerialVerbose == true )
		SendSerial( out_buffer );

	mpAlteraBaseReg[XA_IADR_DMA_EN_REG] = 0x0;

	altera_write_flash( XA_IADR_WR_FLASH_ENABLE, 0x01 );	// Set Flash Enable High
	altera_write_flash( XA_IADR_WR_FLASH_NCON  , 0x00 );	// Set nConfig Low ( Enter Programming Mode )
	altera_write_flash( XA_IADR_WR_FLASH_NCE   , 0x01 );	// Set nCE High
	altera_write_flash( XA_IADR_WR_FLASH_NCS   , 0x01 );	// Set NCS High

	// ----- Read Silicon ID ----------------------------------------------------------
	snprintf( out_buffer, 512, "Reading Silicon ID\r\n" );
	if( _SerialVerbose == true )
		SendSerial( out_buffer );
	silicon_id = Altera_Flash_Read_ID( );
	// ----- Read Silicon ID ----------------------------------------------------------

	switch( silicon_id ) 
	{
		case EPCS1_ID 	: 
			erase_time 		=   6;	
			snprintf( out_buffer, 512, "     Found EPCS1\r\n" ); 
			break;

		case EPCS4_ID 	: 
			erase_time 		=  10;	
			snprintf( out_buffer, 512, "     Found EPCS4\r\n" ); 
			break;

		case EPCS16_ID 	: 
			erase_time 		=  40;	
			snprintf( out_buffer, 512, "     Found EPCS16\r\n" ); 
			break;

		case EPCS64_ID 	: 
			erase_time 		= 160;	
			snprintf( out_buffer, 512, "     Found EPCS64\r\n" ); 
			break;

		default			: 
			erase_time 		= 0;	
			snprintf( out_buffer, 512, "FLASH ABORTED: Device not found Silicon ID [%x]\r\n", silicon_id ); 
			Altera_Flash_Done();
			break;
	}		 						
	if( _SerialVerbose == true )
		SendSerial( out_buffer );
	if( erase_time >0 )
		Altera_Flash_Erase( erase_time );	// Bulk Erase 
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
int tcArtemisDsp::Altera_Flash_Read_ID( void )
{
	int silicon_id;

	altera_write_flash( XA_IADR_WR_FLASH_NCS, 0x0 );						// Set NCS Low
	altera_write_flash( XA_IADR_WR_FLASH_BYTE_MSB, AS_READ_SILICON_ID );	// Send the COmmand to read the Silicon ID
	altera_write_flash( XA_IADR_WR_FLASH_BYTE_MSB, 0x0 );					// Write 3 Dummy Bytes
	altera_write_flash( XA_IADR_WR_FLASH_BYTE_MSB, 0x0 );					// Write 3 Dummy Bytes
	altera_write_flash( XA_IADR_WR_FLASH_BYTE_MSB, 0x0 );					// Write 3 Dummy Bytes

    TSK_sleep(1000);
	silicon_id = altera_read_flash();
	altera_write_flash( XA_IADR_WR_FLASH_NCS, 0x01 );						// Set NCS High
	return( silicon_id );
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
void tcArtemisDsp::Altera_Flash_Erase( int erase_time )
{
	int 			StatusReg;
	static char		out_buffer[512];

	altera_write_flash( XA_IADR_WR_FLASH_NCS, 		0x00 );				// Set NCS Low
	altera_write_flash( XA_IADR_WR_FLASH_BYTE_MSB, 	AS_WRITE_ENABLE );	// Send Write Enable Command
	altera_write_flash( XA_IADR_WR_FLASH_NCS, 		0x01 );				// Set NCS High

	altera_write_flash( XA_IADR_WR_FLASH_NCS, 		0x00 );				// Set NCS Low
	altera_write_flash( XA_IADR_WR_FLASH_BYTE_MSB,  AS_ERASE_BULK );		// Send the Erase Bulk Command
	altera_write_flash( XA_IADR_WR_FLASH_NCS,      	0x01 );				// Set NCS High

	altera_write_flash( XA_IADR_WR_FLASH_NCS, 		0x00 );				// Set NCS Low
	altera_write_flash( XA_IADR_WR_FLASH_BYTE_MSB, AS_READ_STATUS );	// Set the Read Status Command

	snprintf( out_buffer, 512, "\tErasing Flash...\r\n" );
	if( _SerialVerbose == true )
		SendSerial( out_buffer );

	erase_time = erase_time/2;

	// Count down sleeping once a second
	StatusReg = 0xFF;
	do{
		snprintf( out_buffer, 512, "<Erase in-process:%03d> Status %02X                  \r", erase_time, StatusReg );
		if( _SerialVerbose == true )
			SendSerial( out_buffer );
		TSK_sleep( 1000 );
		StatusReg = altera_read_flash() & 0xFF;
	}
	while( ( --erase_time > 0 ) || ( (StatusReg & ALTERA_FLASH_WIP )> 0 ));
							
	altera_write_flash( XA_IADR_WR_FLASH_NCS,      	0x01 );				// Set NCS High
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
// Some "Clean-up code on the DSP causes the DSP to run too fast and over-run the 
// sequencer ( when sending 2-bytes to the Flash EEPROM )
void tcArtemisDsp::altera_write_flash( unsigned int addr, unsigned int data )
{
	// 1st check the status and make sure the "State Machine" is not busy
	int status;
	int lc = 100;
	do
	{
		// The LSB of Status is when the FLASH state-machine is busy
		// ie ( Status0 = 0 when idle )
		status = mpAlteraBaseReg[XA_IADR_FLASH_STATUS] & 0x1;
	}
	while(( status ) && ( --lc > 0 ));
	mpAlteraBaseReg[addr] = ( data & 0xFF );			// Only write the 8 LSBs
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
// Read the Byte from the Altera Flash
// Command must be setup previous to this call
// but it will wait for not busy and read the data
int tcArtemisDsp::altera_read_flash( void )
{
	int status;
	int lc;

	// Make Sure the State Machine is not busy...
	lc = 100;
	do
	{
		// The LSB of Status is when the FLASH state-machine is busy
		// ie ( Status0 = 0 when idle )
		status = mpAlteraBaseReg[XA_IADR_FLASH_STATUS] & 0x1;
	}
	while(( status ) && ( --lc > 0 ));

	// Now send the Commnad to Read a Byte of Data from the Flash 
	mpAlteraBaseReg[XA_IADR_WR_FLASH_BYTE_READ] = 0x00;			

	// Again wait for Read State machine not to be busy
	lc = 100;
	do
	{
		// The LSB of Status is when the FLASH state-machine is busy
		// ie ( Status0 = 0 when idle )
		status = mpAlteraBaseReg[XA_IADR_FLASH_STATUS] & 0x1;
	}
	while(( status ) && ( --lc > 0 ));

	// Read Back the Data
	return( mpAlteraBaseReg[XA_IADR_RD_FLASH_DATA] );
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
void tcArtemisDsp::Altera_Flash_Done( void )
{
		altera_write_flash( XA_IADR_WR_FLASH_NCE, 	 0x00 );	// Set nCE Low
		altera_write_flash( XA_IADR_WR_FLASH_NCON, 	 0x01 );	// Set nConfig High
		altera_write_flash( XA_IADR_WR_FLASH_NCS, 	 0x01 );	// Set NCS High
		altera_write_flash( XA_IADR_WR_FLASH_ENABLE, 0x00 );	// Set Flash Enable Low
		mpAlteraBaseReg[ XA_IADR_DMA_EN_REG] = XILINX_DMA_EN;
		_Flash_Altera = false;
}
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
/**
 * This function sends the specified string to the client associated with
 * the specified socket.
 * \param     telnetconn The socket to send to.
 * \param     str        The zero terminated string to send.
 * \return    0 if successful, -1 if the connection is closed.
 */
int tcArtemisDsp::send_str (int socket, const char *str)
{
    if (socket < 0)
    {
        return -1;
    }
    // Write out the string
    if (write (socket, (void*) str, strlen (str)) < 0)
    {
        close (socket);
        return -1;
    }
    return 0;
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
void tcArtemisDsp::Altera_Command( unsigned int addr )
{
	unsigned int tmp;
	int lc;

	// Check to see of the Write FIFO is full. if so wait
	lc = 100;					
	do
	{
		tmp = mpAlteraBaseReg[XA_IADR_RD_FPGA_STATUS];				// Read the Xilinx-Altera FIFO Status
		tmp = ( tmp >> ALTERA_WRITE_FIFO_FF ) & 0x1;				// Check the Write Fifo full flag
		if( tmp > 0 )
	  		TSK_sleep(1);											// wait 1msec				
	} while(( tmp > 0 ) && ( --lc > 0 ));
	// Now it is OK to write the Data to the FPGA
	mpAlteraBaseReg[XA_IADR_WR_FPGA_ADDR] = addr;
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
void tcArtemisDsp::Altera_Writer( unsigned int addr, unsigned int data )
{
	unsigned int tmp;
	int lc;


	// Check to see of the Write FIFO is full. if so wait
	lc = 100;					
	do
	{
		tmp = mpAlteraBaseReg[XA_IADR_RD_FPGA_STATUS];				// Read the Xilinx-Altera FIFO Status
		tmp = ( tmp >> 20 ) & 0x1FF;								// Write FIFO Count is from bits 20 to 28;
		if( tmp > 500 )												// if not enough room then sleep for 1 ms
			TSK_sleep(1);			
	} while(( tmp > 500 ) && ( --lc > 0 ));

	// Now it is OK to write the Data to the FPGA
	mpAlteraBaseReg[XA_IADR_WR_FPGA_ADDR] = addr;
	mpAlteraBaseReg[XA_IADR_WR_FPGA_DATA] = data;
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
unsigned int tcArtemisDsp::Altera_Reader( unsigned int addr )
{
	unsigned int tmp;
	int lc = 100;

	// Check to see of the Write FIFO is full. if so wait
	do
	{
		tmp = mpAlteraBaseReg[XA_IADR_RD_FPGA_STATUS];				// Read the Xilinx-Altera FIFO Status
		tmp = ( tmp >> ALTERA_WRITE_FIFO_FF ) & 0x1;				// Check the Write Fifo full flag
		if( tmp > 0 )
	  		TSK_sleep(1);											// wait 1msec
					
	} while(( tmp > 0 ) && ( --lc > 0 ));
	// Now it is OK to write the Address to the FPGA
	mpAlteraBaseReg[XA_IADR_WR_FPGA_ADDR] =  addr;		// Write the Address 

					
	// Check to see of the FIFO is full. if so wait
	lc = 100;
	do
	{
		tmp = mpAlteraBaseReg[XA_IADR_RD_FPGA_STATUS];				// Read the Xilinx-Altera FIFO Status
		tmp = ( tmp >> ALTERA_READ_FIFO_READY ) & 0x1;				// Check the data is ready
		if( tmp == 0 )
	  		TSK_sleep(1);											// wait 1msec
					
	} while(( tmp == 0 ) && ( --lc > 0 ));
	// Now it is OK to write the Data to the FPGA
	tmp = mpAlteraBaseReg[XA_IADR_RD_FPGA_DATA];					// Read the Data TODO
	return( tmp );
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
/**
 *   This function runs forever, listening for TCP 
 *   connects on port (DPP_PORT). 
 */
int tcArtemisDsp::DppConnect ()
{
	int dpp_server_socket;

    struct sockaddr_in sa;
    struct sockaddr_in conn_client_saddr;
    fd_set readset;
    int ii, fdmax, length;
    static char in_buffer[512];
	static char out_buffer[512];

	TSK_sleep(1000);
    // One-time initialization. Register this thread for LWIP usage
    sys_thread_register(TSK_self());

    //----- First acquire Dpp socket for listening for connections -----------------------------
    dpp_server_socket = socket(AF_INET, SOCK_STREAM, 0);
    
    memset (&sa, 0, sizeof (sa));
    sa.sin_family      = AF_INET;
    sa.sin_addr.s_addr = htonl (INADDR_ANY);
    sa.sin_port        = htons (DPP_PORT);

    if (bind (dpp_server_socket, (struct sockaddr *) &sa, sizeof(sa)) < 0)
    {
		if( _SerialVerbose == true )
	        SendSerial("Dpp Data Socket bind failed");
		return -1;
    }
    // Put socket into listening mode 
    if (listen (dpp_server_socket, 2) == -1)
    {
		if( _SerialVerbose == true )
	        SendSerial ("Dpp Data Listen Failed");
		return -1;
    }
    //----- First acquire Altera socket for listening for connections -----------------------------

   
    //----- Wait forever for network input: This could be connections or data ---------------------
    while (1)
    {
        /* Determine what sockets need to be in readset */
        FD_ZERO (&readset);
		FD_SET  (dpp_server_socket, &readset );

		// if Dpp Socket connected -------------------------------------------------------------
		if(mnDppConnectedSocket >= 0)
			FD_SET( mnDppConnectedSocket, &readset);
		// if Dpp Socket connected -------------------------------------------------------------

		fdmax = 0;
		if( dpp_server_socket 		> fdmax ) fdmax = dpp_server_socket;
		if( mnDppConnectedSocket 	> fdmax ) fdmax = mnDppConnectedSocket;
		fdmax ++;

        //----- Wait for data or a new connection -------------------------------------------------
		// Changes readset to only contains bit that correspond to the sockets that have data
        ii = select (fdmax, &readset, NULL, 0, 0);        
        if (ii == 0)
            continue;

		snprintf( out_buffer, 512, "\r\n--------------------------------------------------------------------------------\r\nReceived RxData on Dpp Data Port[%d]\r\n", mnDppConnectedSocket );
		if( _SerialVerbose == true )
			SendSerial(out_buffer);

        //----- At least one descriptor is ready on Dpp Socket ---------------------------------
        if (FD_ISSET (dpp_server_socket, &readset))
        {
            if (mnDppConnectedSocket == -1)
            {
                mnDppConnectedSocket = accept (dpp_server_socket, (struct sockaddr *) &conn_client_saddr, (u32_t*)&length);

				// We couldn't create a socket for this connection
                if (mnDppConnectedSocket < 0)
                {
                    mnDppConnectedSocket = -1;
					snprintf( out_buffer, 512, "\tNo-connection to DPP Data Socket[%d]\r\n", mnDppConnectedSocket );
					if( _SerialVerbose == true )
						SendSerial( out_buffer );
                }
				else
				{
					snprintf( out_buffer, 512, "\tConnecting to DPP Data Socket[%d]\r\n", mnDppConnectedSocket );
					if( _SerialVerbose == true )
						SendSerial( out_buffer );
					Altera_Command( awCmd_Fifo_mr );					// Reset the FIFOs in Altera	


					// The DMA Engine needs the handle to the mnDppConnectedSocket
					mpArtemis_Dma->SetDppSocket( mnDppConnectedSocket ); 
					mpAlteraBaseReg[XA_IADR_DMA_IRQ_CLR] = ( XILINX_DMA_CLR | XILINX_ALTERA_INT_CLR );										// Clear any Interrupts
					mpAlteraBaseReg[XA_IADR_DMA_EN_REG] = mpAlteraBaseReg[XA_IADR_DMA_EN_REG] | ( XILINX_DMA_EN | XILINX_ALTERA_INT_EN );	// Enable DMA
					Altera_Command( awCmd_Read_Data_Enable_Disable | 0x1 );					// Enable Data Collection in Altera

					// Commands to turn off the Nagling algorithm to disable the 100ms delay
					int sockOpt = 1;
					setsockopt(mnDppConnectedSocket, IPPROTO_TCP, TCP_NODELAY, &sockOpt, sizeof(int));

					// read the socket to clear any initial data that might be present
	                length = read (mnDppConnectedSocket, in_buffer, 512);		// Might need to be commented
					snprintf( out_buffer, 512, "\tStarted DMA Engine on the Data Socket[%d]\r\n", mnDppConnectedSocket );
					if( _SerialVerbose == true )
						SendSerial( out_buffer );
				}
            } 
            else 
            {
                // Socket already open, only 1 client allowed, just accept then close
                int sock;
                struct sockaddr client_saddr;
                socklen_t length = sizeof (client_saddr);

                sock = accept (dpp_server_socket, &client_saddr, &length);
                if (sock >= 0)
                {
                    send_str(sock,"ERROR: Dpp Data Another Client is already connected\r\n");
                    close (sock);
					snprintf( out_buffer, 512, "\tERROR: Another Client is connected to the DPP Data Socket\r\nConnection Refused\r\n\n");
					if( _SerialVerbose == true )
						SendSerial( out_buffer );
                }
            }
        }
        //----- At least one descriptor is ready on Altera Socket ---------------------------------

        //----- Process data on the Dpp Socket -------------------------------------------------
        if(( mnDppConnectedSocket >= 0 ) && (FD_ISSET (mnDppConnectedSocket, &readset)))
        {

            // This socket is ready for reading. This could be because
   	        // someone typed  character or because the socket is closed
			//for( int i=0;i<512;i++)
			//     in_buffer[i] = 0;
            length = read (mnDppConnectedSocket, in_buffer, 512);
            if (length <= 0)
            {
				// Disable data collection in the Altera FPGA
				snprintf( out_buffer, 512, "\tClosing DPP Data Socket[%d]\r\n", mnDppConnectedSocket );
				if( _SerialVerbose == true )
					SendSerial( out_buffer );
				Altera_Command( awCmd_Read_Data_Enable_Disable );		// Disable Data Collection in ALtera
				Altera_Command( awCmd_Fifo_mr );						// Reset the FIFOs in Altera
				// TSK_sleep(1);											// sleep for 1m Second
				// The DMA Engine needs the handle to the mnDppConnectedSocket
				// mpAlteraBaseReg[XA_IADR_DMA_EN_REG] = mpAlteraBaseReg[XA_IADR_DMA_EN_REG] & ~( XILINX_DMA_EN | XILINX_ALTERA_INT_EN );	// Clear DMA Enable
				mpArtemis_Dma->SetDppSocket( -1 ); 
				// TSK_sleep(1000);										// wait 100 ms
                close (mnDppConnectedSocket);
   	            mnDppConnectedSocket = -1;
       	    } 
        }
		else
		{
          //  length = read (mnDppConnectedSocket, in_buffer, 512);
		}
		snprintf(out_buffer,512,"Processed RxData on Data Port [%d]\r\n----------------------------------------------------------------------------\r\n", mnDppConnectedSocket );
		if( _SerialVerbose == true )
			SendSerial( out_buffer );
        //----- Process data on the Server Socket -------------------------------------------------
    }
}
//-------------------------------------------------------------------------------------------------


//-------------------------------------------------------------------------------------------------
/**
 *   This function runs forever, listening for TCP 
 *   connects on port (DPP_PORT). 
 */
int tcArtemisDsp::ParamConnect ()
{
	int param_server_socket;
	unsigned int data;
	unsigned int tmp;

    struct sockaddr_in sa;
    struct sockaddr_in conn_client_saddr;
    fd_set readset;
    int ii, fdmax, length;
	static char ser_buf[512];
	char tmps[16];


	int j;

	#define _Write_Seq_Idle  			0
	#define _Write_Seq_Write 			1
	#define _Write_Seq_SCABBWrite 		2			    
	#define _Write_Seq_Next_DHCP 		3
	#define _Write_Seq_Next_Wins		4
	#define _Write_Seq_Next_IPAddr		5
	#define _Write_Seq_Next_Gateway		6
	#define _Write_Seq_Next_Netmask		7
	#define _Write_Seq_Next_Wins_Name	8	
	#define _Write_Flash_DSP			9			
	#define _Write_Flash_Xilinx			10
	#define _Write_Flash_Altera			11
	#define _Write_Seq_Generic_Flash	12
	#define _Write_Erase_Altera 		13

	static int _write_addr;
	static int _write_end;
	static int _write_seq = _Write_Seq_Idle; 
	static bool _flash_update = false;
	static bool CopyAll = false;
	static bool CopyAmpTime = false;
	int start_addr;
	int end_addr;
    int a1,a2,a3,a4;
	int pct, pct_last = 0;
	int lc;
	int rc;
	int StatusReg = 0x01;
	int ptr;
	int levch, lamptime;
	int i;
	int _write_ptr = 0;			//separated from one second usage
    tcDspNetIFConfigUtil::tsDspNetConfig lsConfig;


    // One-time initialization. Register this thread for LWIP usage
    sys_thread_register(TSK_self());

    //----- First acquire Dpp socket for listening for connections -----------------------------
    param_server_socket = socket(AF_INET, SOCK_STREAM, 0);
    
    memset (&sa, 0, sizeof (sa));
    sa.sin_family      = AF_INET;
    sa.sin_addr.s_addr = htonl (INADDR_ANY);
    sa.sin_port        = htons (PARAM_PORT);

    if (bind (param_server_socket, (struct sockaddr *) &sa, sizeof(sa)) < 0)
    {
		if( _SerialVerbose == true )
	        SendSerial("Dpp Param Socket bind failed");
		return -1;
    }
    // Put socket into listening mode 
    if (listen (param_server_socket, 2) == -1)
    {
		if( _SerialVerbose == true )
	        SendSerial ("Dpp Parameter Listen Failed");
		return -1;
    }
    //----- First acquire Altera socket for listening for connections -----------------------------

   
    //----- Wait forever for network input: This could be connections or data ---------------------
    while (1)
    {
        /* Determine what sockets need to be in readset */
        FD_ZERO (&readset);
		FD_SET  (param_server_socket, &readset );

		// if Dpp Socket connected -------------------------------------------------------------
		if(mnParamConnectedSocket >= 0)
			FD_SET( mnParamConnectedSocket, &readset);
		// if Dpp Socket connected -------------------------------------------------------------

		fdmax = 0;
		if( param_server_socket 		> fdmax ) fdmax = param_server_socket;
		if( mnParamConnectedSocket 		> fdmax ) fdmax = mnParamConnectedSocket;
		fdmax ++;

        //----- Wait for data or a new connection -------------------------------------------------
		// Changes readset to only contains bit that correspond to the sockets that have data
		//snprintf(out_buffer,512,"Waiting for RxData on Data Port[%d]..........\r\n", mnDppConnectedSocket );
		//SendSerial(out_buffer);

        ii = select (fdmax, &readset, NULL, 0, 0);        
        if (ii == 0) 
            continue;
		mpArtemis_Dma->Set_TimeBuffer_Period( (float)CLK_countspms() / 1000.0f );		// Determime high  resolution counts/micro-second
		mpArtemis_Dma->Set_TimeBuffer_Start( (float)CLK_gethtime() );					// get the starting time

		if(( _Flash_Altera == false ) && ( _Flash_Xilinx == false ) && ( _Flash_DSP == false ))
		{
			snprintf( ser_buf, 512, "\r\n--------------------------------------------------------------------------------\r\nRecieved RxData on Dpp Parameter Port[%d]\r\n", mnParamConnectedSocket );
			if( _SerialVerbose == true )
				SendSerial(ser_buf);
		}

		//mpAlteraBaseReg[XA_IADR_USER_IO] = USER_IO_MODE | 0x01;	
        //----- At least one descriptor is ready on Dpp Socket ---------------------------------
        if (FD_ISSET (param_server_socket, &readset))
        {
            if (mnParamConnectedSocket == -1)
            {
                mnParamConnectedSocket = accept (param_server_socket, (struct sockaddr *) &conn_client_saddr, (u32_t*)&length);

				// We couldn't create a socket for this connection
                if (mnParamConnectedSocket < 0)
                {
                    mnParamConnectedSocket = -1;
					snprintf( ser_buf, 512, "\tNo-connection to DPP Parameter Socket[%d]\r\n", mnParamConnectedSocket );
//					if( _SerialVerbose == true )
						SendSerial( ser_buf );
                }
				else
				{
					snprintf( ser_buf, 512, "\tConnecting to DPP Parameter Socket[%d]\r\n", mnParamConnectedSocket );
//					if( _SerialVerbose == true )
						SendSerial( ser_buf );

					// Commands to turn off the Nagling algorithm to disable the 100ms delay
					int sockOpt = 1;
					setsockopt(mnParamConnectedSocket, IPPROTO_TCP, TCP_NODELAY, &sockOpt, sizeof(int));

					TSK_sleep(1);									// sleep for 1m Second

					// read the socket to clear any initial data that might be present
	                length = read (mnParamConnectedSocket, flash_in_buffer, 20*1024);		// Might need to be commented
				}
            } 
            else 
            {
                // Socket already open, only 1 client allowed, just accept then close
                int sock;
                struct sockaddr client_saddr;
                socklen_t length = sizeof (client_saddr);

                sock = accept (param_server_socket, &client_saddr, &length);
                if (sock >= 0)
                {
                    send_str(sock,"\tERROR: Dpp Parameter Another Client is already connected\r\n");
                    close (sock);
					snprintf( ser_buf, 512, "\tERROR: Another Client is connected to the DPP Parameter Socket\r\nConnection Refused\r\n\n");
//					if( _SerialVerbose == true )
						SendSerial( ser_buf );

                }
            }
        }
        //----- At least one descriptor is ready on Altera Socket ---------------------------------
		if( mnParamConnectedSocket == -1 )
			continue;

        //----- Process data on the Dpp Socket -------------------------------------------------
        if(FD_ISSET (mnParamConnectedSocket, &readset))
        {
            length = read (mnParamConnectedSocket, flash_in_buffer, 20*1024);
			ptr = 0;

			//mpAlteraBaseReg[XA_IADR_USER_IO] = USER_IO_MODE | 0x02;	
            if (length <= 0)
            {
				snprintf( ser_buf, 512, "\tClosing DPP Parameter Socket[%d]\r\n\tSetting Detector Off-Line\r\n\tRetracting Detector\r\n", mnParamConnectedSocket );
				SendSerial( ser_buf );
                close (mnParamConnectedSocket);
   	            mnParamConnectedSocket = -1;
				TSK_sleep(100);

				if( _AutoOffDisable == 0 )
					Set_Standby( true );
       	    } 
       	    else 
       	    {	// Length >= 0 
				//************* FLASH XILINX ********************************************************************************
				if( _Flash_Xilinx == true )
				{								// _Flash_Xilinx = true
					mnCodeDownLdType = PRO_HDR_OPC_FW_DOWNLOAD;

					// Process all of the data in the buffer
					memcpy( &_flash_data[_flash_ptr], &flash_in_buffer[0], length );
					_flash_ptr += length;									// Keep track of how much data

					pct = (int)(( _flash_ptr * 100 ) / (float) _flash_size );
					if( pct >= ( pct_last +5 ))
					{
						// Send back some Statistics
						snprintf(ser_buf,512,"Programming Xilinx %d %%\r;\0", pct );
						if( _SerialVerbose == true )
							SendSerial( ser_buf );
						TSK_sleep(250);
						pct_last = pct;
					}

					if( _flash_ptr >= _flash_size )							// all of the data now is contained in _flash_data[]
					{
						// Add the block of new data to the parser
						rc = mpParser->AddData( &_flash_data[0], _flash_ptr);	
					}							// _flash_ptr > _flash_size
				}								// _Flash_Xilinx = true
				//************* FLASH XILINX ********************************************************************************

				//************* FLASH DSP ***********************************************************************************
				else if( _Flash_DSP == true )
				{								// _flash_dsp = true
					mnCodeDownLdType = PRO_HDR_OPC_SW_DOWNLOAD;

					// Process all of the data in the buffer
					memcpy( &_flash_data[_flash_ptr], &flash_in_buffer[0], length );
					_flash_ptr += length;									// Keep track of how much data

					pct = (int)(( _flash_ptr * 100 ) / (float) _flash_size );
					if( pct >= ( pct_last +5 ))
					{
						// Send back some Statistics
						snprintf(ser_buf,512,"Programming DSP %d %%\r;\0", pct );
						if( _SerialVerbose == true )
							SendSerial( ser_buf );
						TSK_sleep(250);
						pct_last = pct;
					}

					if( _flash_ptr >= _flash_size )							// all of the data now is contained in _flash_data[]
					{
						// Add the block of new data to the parser
						rc = mpParser->AddData( &_flash_data[0], _flash_ptr);	
					}
				}								// _flash_dsp = true
				//************* FLASH DSP ***********************************************************************************

				else if( _Flash_Altera == true )
				{								// _Flash_Altera = true
					// flash_in_buffer[] contains the current set of data
					// The amount of data is in variable length
					do
					{
						//----- Page Programming Loop -------------------------------------------------
						// if current byte is 0, New Page must program appropriate information
						if( _Altera_Flash_Current_Byte == 0 )
						{									
							//----- Start of a Page -----------------------------------------------
							pct = (int)((float)( _Altera_Flash_Current_Page * 100 ) / (float) _Altera_Flash_Pages );
							if(( pct > pct_last ) && (( pct % 10 ) == 0 ))
							{
								snprintf(ser_buf,512,"Programming %d %%\r", (int)(( _Altera_Flash_Current_Page * 100 ) / _Altera_Flash_Pages) );
								if( _SerialVerbose == true )
									SendSerial( ser_buf );
								pct_last = pct;
							}
							altera_write_flash( XA_IADR_WR_FLASH_NCS, 		0x00 );				// Set NCS Low
							altera_write_flash( XA_IADR_WR_FLASH_BYTE_MSB, 	AS_WRITE_ENABLE );	// Set the Write Enable Commnad
							altera_write_flash( XA_IADR_WR_FLASH_NCS,      	0x01 );				// Set NCS High

							altera_write_flash( XA_IADR_WR_FLASH_NCS, 	    0x00 );				// Set NCS Low
							altera_write_flash( XA_IADR_WR_FLASH_BYTE_MSB, AS_PAGE_PROGRAM );	// Set the Page Program Mode
							altera_write_flash( XA_IADR_WR_FLASH_BYTE_MSB, (_Altera_Flash_Current_Page * _Altera_Flash_Byte_Per_Page ) >> 16 );	
							altera_write_flash( XA_IADR_WR_FLASH_BYTE_MSB, (_Altera_Flash_Current_Page * _Altera_Flash_Byte_Per_Page ) >>  8 );	
							altera_write_flash( XA_IADR_WR_FLASH_BYTE_MSB, (_Altera_Flash_Current_Page * _Altera_Flash_Byte_Per_Page ) >>  0 );	

							if(( _Altera_Flash_Current_Page == ( _Altera_Flash_Pages-1 )) && ( _Altera_Flash_Byte_Bal != 0 ))
								_Altera_Flash_Byte_Per_Page = _Altera_Flash_Byte_Bal;
							//----- Start of a Page -----------------------------------------------
						}	// if current_byte = 0

						// Now Send data to EEPROM
						if( _Altera_Flash_Current_Byte < _Altera_Flash_Byte_Per_Page )
						{
							altera_write_flash( XA_IADR_WR_FLASH_BYTE_LSB, flash_in_buffer[ptr] );	// Write 2 bytes
							_Altera_Flash_Current_Byte ++;
							ptr++;
						} 

						if( _Altera_Flash_Current_Byte == _Altera_Flash_Byte_Per_Page )
						{
							altera_write_flash( XA_IADR_WR_FLASH_NCS,      	0x01 );				// Set NCS High
							altera_write_flash( XA_IADR_WR_FLASH_NCS, 		0x00 );				// Set NCS Low
							altera_write_flash( XA_IADR_WR_FLASH_BYTE_MSB, AS_READ_STATUS );	// Set to Read Status Command
							lc = 1000;
							do
							{
								StatusReg = altera_read_flash();
							}
							while(( StatusReg & ALTERA_FLASH_WIP ) && ( --lc > 0 ));	
							altera_write_flash( XA_IADR_WR_FLASH_NCS,      	0x01 );				// Set NCS High
							_Altera_Flash_Current_Page ++;
							_Altera_Flash_Current_Byte = 0;
						}																		// if...
					} while ( ptr < length );

					//----- Page Programming Loop -------------------------------------------------

					//----- Program is Done -------------------------------------------------------
					if( _Altera_Flash_Current_Page >= _Altera_Flash_Pages )
					{
						snprintf( ser_buf, 512, "\tClean-Up\r\n" );
						if( _SerialVerbose == true )
							SendSerial( ser_buf );
						Altera_Flash_Done();
						snprintf( ser_buf, 512, "FLASH COMPLETE\r;\0" );
						if( _SerialVerbose == true )
							SendSerial( ser_buf );
						snprintf( ser_buf, 512, "\tFlash Complete\r\n" );
						if( _SerialVerbose == true )
							SendSerial( ser_buf );

						// extract the last amptime, evch and set the new parameters
						memcpy( &data, &_Dpp_Data[0], 4 );
					 	SetAmpTime( data  );
						_Flash_Altera = false;
						_ALTERA_Flash_Done = true;

						TSK_sleep(1000);
						data = krwKey_Data_Mask;									// Append Data Key
						data |= krKey_Flash_Status;
						data |= 0;																// Append Data Key
						memcpy( &param_host_buffer[_write_ptr], &data, 4 );							// copy data to the output buffer Command
						_write_ptr += 4;
						send_buff( mnParamConnectedSocket, param_host_buffer, _write_ptr );			// send it back to the host
						_write_ptr = 0;
//						TSK_sleep(1000);
					     // disable firmware interrupts
						//LCK_pend(ghFlashLock,LCK_TIMEOUT);
						//tcDspFirmware::global_int_disable(IE_MASK_4 | IE_MASK_5 | IE_MASK_6 | IE_MASK_7);
				        //tcDspBootstrapper::Reboot(mpFlash);
						//LCK_post(ghFlashLock);	//ok, should never get here but I like the balance

						//----- Program is Done -------------------------------------------------------
					}
				}								// _Flash_Altera = true
				//************* FLASH ALTERA ********************************************************************************

				//************* Any other processing ************************************************************************
				else
				{								// Any other data processing

					_flash_update = false;
				    do	
					{
						mpArtemis_Dma->Add_TimeBuffer( 0x14 );
						memcpy( &data, &flash_in_buffer[ptr], 4 );			// Copy input buffer to the data register
						snprintf( ser_buf, 512, "\r\nData Port[%d] %04x:%04x hex\r\n", length, ( data >> 16 ) & 0xFFFF, data & 0xFFFF );
						if( _SerialVerbose == true )
							SendSerial(ser_buf);
						mpArtemis_Dma->Add_TimeBuffer( 0x15 );

						switch( _write_seq )
						{
							//------------------ Writting to ALtera WRITE SEQUENCE ----------------------------------------------------------------
							case _Write_Seq_Write :							// write data 
								// Spit out the data to the RS-232 port ( for debugging)
								Altera_Writer( awCmd_Write | _write_addr, data );						// Now Write it to the FPGA

								// Now Update local memory for EEPROM
								memcpy( &tmp, &_Dpp_Data[0], 4 );			// Get Current Amp Time
								_amptime = ( tmp >> 0 ) & 0x7;
								_evch  =   ( tmp >> 12 ) & 0x1;

								if( _write_addr == iADR_DISC_0 )
								{											// Disc 0 Write Address
									if( _amptime == 0 )
									{										// AmpTime 0 - only upate this amptime
										memcpy( &_Dpp_Data[Param_Addr( _evch, _amptime, _write_addr)], &data, 4 );	// Write to structure
									}
									else
									{
										for(lamptime=1;lamptime<kFLASH_NUM_AMPTIME;lamptime++)
										{
											memcpy( &_Dpp_Data[Param_Addr(_evch,lamptime,_write_addr)],&data,4);	// Disc0 Value to remaining Amptimes
										}		// for amptime
									}
								}											// Disc 0 Write Address
								else										
								{											// not Disc 0 Write Address
									CopyAmpTime = false;
									CopyAll = false;
									switch( _write_addr )
									{
										// Individual Amp Time, eV/Ch and Parameters
										case iADR_ATCWORD			: // AmpTime Control Word
										case iADR_GAIN				: // Gain
										case iADR_OFFSET			: // Offset
										case iADR_DISC_4			: // Disc Main Threshold
										case iADR_CLIP				: // Clip Max/Min
											break;		

										// Common to All Amp Times for a given ev/ch
										case iADR_EVCH_GAIN			: // ev/ch or Preamp Gain
										case iADR_DISC_0			: // Disc 0 Threshold
										case iADR_DISC_1			: // Disc 1 Threshold
										case iADR_DISC_2			: // Disc 2 Threshold
										case iADR_DISC_3			: // Disc 3 Threshold
											CopyAmpTime = true;
											break;

										// Common to All Amp Times, Ev/Ch and 
										case iADR_CWORD				: // Control Word
										case iADR_PRESET			: // Preset Value
										case iADR_SCABB				: // SCA/BB
										case iADR_Pixels_Line		: // Pixels/line
										case iADR_Lines_Frame		: // Lines/Frame
										case iADR_SYNC_REG			: // External Sync Reg
										case iADR_DSS_REG   		: // Dss Reg
										case iADR_DSS_INT   		: // Dss Interval
										case iADR_DSS_MAX   		: // Dss Max Points
										case iADR_BIT_PEAK			: // Bit Peak positions
										case iADR_BIT_TIME			: // Bit Time values
										case iADR_POT_VAL 			: // TEC Pot value
										case iADR_HV_CTRL			: // HV Control Register
										case iADR_TARGET_ADC		: // Target Temperature
										case iADR_RESET_PW			: // Reset PW
										case iADR_PA_TEC2_KIKP		: // TEC2 PID parameters
										case iADR_TEC2_DELAY		: // TEC2 Turn on/off delay
										case iADR_PA_TEMP_OFFSET 	: // Preamp Temp offset
										case iADR_SLIDE_HCR			: // AutoRetract Count Rate
										case iADR_SLIDE_HCT			: // AutoRetract Threshold
										case iADR_MSlide_Tar		: // Slide Target Position
										case iADR_MSlide_Vel_Max	: // Slide Max Velocity
										case iADR_MSlide_Vel_Min	: // Slide Min Velocity
										case iADR_OTR_FACTOR		: // Over voltge factor
										case iADR_RTIME_THR			: // RiseTime Reject Threshold
										case iADR_TEC1_FACTOR		: // TEC2 * FACTOR = TEC1 Program
										case iADR_WF_COEFA			: // Weiner Filter Coef A
										case iADR_WF_COEFB			: // Weiner Filter COef B
										case iADR_WF_COEFC			: // Weiner Filter Coef C
										case iADR_WF_DELAY			: // Weiner Filter Delay (in 10x ns)

											CopyAll = true;
											break;
										default						: // Don't copy to anything
											break;
									}

									if( CopyAll == true )			// Copy to All AmpTimes and All Ev/Ch
									{
										for( levch = 0; levch<kFLASH_NUM_EVCH;levch++)		// only 5 and 10 ev/ch
										{
											for(lamptime=0;lamptime<kFLASH_NUM_AMPTIME;lamptime++)
												memcpy( &_Dpp_Data[Param_Addr(levch,lamptime,_write_addr)],&data,4);	// Copy HV_CTRL register to All evch, All Amptimes
										}			// for evch
									} 
									else if( CopyAmpTime )			// Copy to All AmpTimes
									{
										for(lamptime=0;lamptime<kFLASH_NUM_AMPTIME;lamptime++)
											memcpy( &_Dpp_Data[Param_Addr(_evch,lamptime,_write_addr)],&data,4);	// Copy HV_CTRL register to All evch, All Amptimes
									} 
									else
									{
											memcpy( &_Dpp_Data[Param_Addr( _evch, _amptime, _write_addr)], &data, 4 );	// Write to structure
									}
								}											// not Disc 0 Write Address

		
								if( ++_write_addr >= _write_end )
								{
									_flash_update = true;								// A parameter has changed - need to update the flash
									_write_seq = _Write_Seq_Idle;
								}
								break;	
							//------------------ Writting to ALtera WRITE SEQUENCE ----------------------------------------------------------------


							case _Write_Seq_Generic_Flash : 
								// data contains the data just written
								// _write_addr is index to the Generic memory
								if( _write_addr < kFLASH_NUM_OTHERS )
								{
									memcpy( &_Dpp_Data[Generic_Param_Addr(_write_addr)], &data, 4 );
								}
								if( ++_write_addr >= _write_end )
								{
									_flash_update = true;
									_write_seq = _Write_Seq_Idle;
								}
								break;
							//------------------ SCABB Write Sequence -----------------------------------------------------------------------------
							case _Write_Seq_SCABBWrite :
								Altera_Writer( awCmd_SCABB | ( _write_addr & 0x7F ) << 8 | iADR_SCABB, data );		// Now Write it to the FPGA
								if( ++_write_addr >= _write_end )
								{
									_write_seq = _Write_Seq_Idle;
								}
								break;
							//------------------ SCABB Write Sequence -----------------------------------------------------------------------------
							case _Write_Seq_Next_DHCP :											// Assert "Write Sequence"
								// Advance the pointer to get the parameter data
								if(( data & 0x1 ) == 0 )
					            	lsConfig.mnUseDHCP = 0;
					            else 
					            	lsConfig.mnUseDHCP = 1;
					            mpNetIFConfigUtil->UpdateConfig(&lsConfig);
								_write_seq = _Write_Seq_Idle;
								break;

							case _Write_Seq_Next_Wins :
								if(( data & 0x1 ) == 0 )
									lsConfig.mnUseWINS = 0;
								else
									lsConfig.mnUseWINS = 1;
							  	mpNetIFConfigUtil->UpdateConfig(&lsConfig);
								_write_seq = _Write_Seq_Idle;
								break;

							case _Write_Seq_Next_IPAddr :
								a1 = ( data >> 24 ) & 0xFF;
								a2 = ( data >> 16 ) & 0xFF;
								a3 = ( data >>  8 ) & 0xFF;
								a4 = ( data >>  0 ) & 0xFF;
					            lsConfig.msIpAddr.addr = BUILD_IP(a1,a2,a3,a4);
					            mpNetIFConfigUtil->UpdateConfig(&lsConfig);
								_write_seq = _Write_Seq_Idle;
								break;

							case _Write_Seq_Next_Gateway	:
								a1 = ( data >> 24 ) & 0xFF;
								a2 = ( data >> 16 ) & 0xFF;
								a3 = ( data >>  8 ) & 0xFF;
								a4 = ( data >>  0 ) & 0xFF;
					            lsConfig.msGateway.addr = BUILD_IP(a1,a2,a3,a4);
					            mpNetIFConfigUtil->UpdateConfig(&lsConfig);
								_write_seq = _Write_Seq_Idle;
								break;

							case _Write_Seq_Next_Netmask	:
								a1 = ( data >> 24 ) & 0xFF;
								a2 = ( data >> 16 ) & 0xFF;
								a3 = ( data >>  8 ) & 0xFF;
								a4 = ( data >>  0 ) & 0xFF;
					            lsConfig.msNetmask.addr = BUILD_IP(a1,a2,a3,a4);
					            mpNetIFConfigUtil->UpdateConfig(&lsConfig);
								_write_seq = _Write_Seq_Idle;
								break;

							case _Write_Seq_Next_Wins_Name :
								if(( data == 0 ) || ( _write_addr > 16 ))				// Null Character - String is ended so update
								{
						            strncpy(lsConfig.maNetBIOSName,tmps,16);			// Now Copy tmps to the proper record
						            mpNetIFConfigUtil->UpdateConfig(&lsConfig);			// Update the EEPROM wiht the new Wins Name 
									_write_seq = _Write_Seq_Idle;
								}
								else
								{							
									memcpy( &tmps[_write_addr], &data, 4 );		// copy Wins Name into buffer (16 bytes)
									_write_addr ++;
								}
								break;

							case _Write_Flash_DSP :
								_Flash_DSP = true;
								_Flash_DSP_Error = false;

								_flash_ptr = 0;
								_flash_size = data;
								pct_last = 0;
								data = krwKey_Data_Mask;									// Append Data Key
								data |= krKey_Flash_Status;
								data |= 7;																// Append Data Key
								memcpy( &param_host_buffer[_write_ptr], &data, 4 );							// copy data to the output buffer Command
								_write_ptr += 4;
								send_buff( mnParamConnectedSocket, param_host_buffer, _write_ptr );			// send it back to the host
								_write_ptr = 0;

								snprintf( ser_buf, 512, "\0" );
								if( _SerialVerbose == true )
									SendSerial( ser_buf );
								snprintf( ser_buf, 512, "Starting DSP Flash %d bytes;\0", _flash_size );
								if( _SerialVerbose == true )
									SendSerial( ser_buf );

								// reset parser
								mpParser->ResetParser();		
								// Set the output to all F's		
								mpParser->ResetOutputBuffer(0xFF);
								// add data to the parser (all of the data)

								data = krwKey_Data_Mask;									// Append Data Key
								data |= krKey_Flash_Status;
								data |= 8;																// Append Data Key
								memcpy( &param_host_buffer[_write_ptr], &data, 4 );							// copy data to the output buffer Command
								_write_ptr += 4;
								send_buff( mnParamConnectedSocket, param_host_buffer, _write_ptr );			// send it back to the host
								_write_ptr = 0;
								_write_seq = _Write_Seq_Idle;
								break;


							case _Write_Flash_Xilinx :
								_Flash_Xilinx = true;
								_Flash_Xilinx_Error = false;
								_flash_ptr = 0;
								_flash_size = data;
								pct_last = 0;

								data = krwKey_Data_Mask;									// Append Data Key
								data |= krKey_Flash_Status;
								data |= 4;																// Append Data Key
								memcpy( &param_host_buffer[_write_ptr], &data, 4 );							// copy data to the output buffer Command
								_write_ptr += 4;
								send_buff( mnParamConnectedSocket, param_host_buffer, _write_ptr );			// send it back to the host
								_write_ptr = 0;

								snprintf( ser_buf, 512, "\0" );
								if( _SerialVerbose == true )
									SendSerial( ser_buf );
								snprintf( ser_buf, 512, "Starting Xilinx Flash %d bytes;\0", _flash_size );
								if( _SerialVerbose == true )
									SendSerial( ser_buf );

								// reset parser
								mpParser->ResetParser();		
								// Set the output to all F's		
								mpParser->ResetOutputBuffer(0xFF);
								// add data to the parser (all of the data)

								data = krwKey_Data_Mask;									// Append Data Key
								data |= krKey_Flash_Status;
								data |= 5;																// Append Data Key
								memcpy( &param_host_buffer[_write_ptr], &data, 4 );							// copy data to the output buffer Command
								_write_ptr += 4;
								send_buff( mnParamConnectedSocket, param_host_buffer, _write_ptr );			// send it back to the host
								_write_ptr = 0;
								_write_seq = _Write_Seq_Idle;
								break;

							case _Write_Flash_Altera :
								_Flash_Altera = true;
								pct_last = 0;
								_flash_size = data;

								data = krwKey_Data_Mask;									// Append Data Key
								data |= krKey_Flash_Status;
								data |= 1;																// Append Data Key
								memcpy( &param_host_buffer[_write_ptr], &data, 4 );							// copy data to the output buffer Command
								_write_ptr += 4;
								send_buff( mnParamConnectedSocket, param_host_buffer, _write_ptr );			// send it back to the host
								_write_ptr = 0;
								TSK_sleep(100);						
								Altera_Flash_Init();
								TSK_sleep(100);						
								data = krwKey_Data_Mask;									// Append Data Key
								data |= krKey_Flash_Status;
								data |= 2;																// Append Data Key
								memcpy( &param_host_buffer[_write_ptr], &data, 4 );							// copy data to the output buffer Command
								_write_ptr += 4;
								send_buff( mnParamConnectedSocket, param_host_buffer, _write_ptr );			// send it back to the host
								_write_ptr = 0;
								_write_seq = _Write_Seq_Idle;
								break;

							case _Write_Erase_Altera :
								data = krwKey_Data_Mask;									// Append Data Key
								data |= krKey_Flash_Status;
								data |= 1;																// Append Data Key
								memcpy( &param_host_buffer[_write_ptr], &data, 4 );							// copy data to the output buffer Command
								_write_ptr += 4;
								send_buff( mnParamConnectedSocket, param_host_buffer, _write_ptr );			// send it back to the host
								_write_ptr = 0;
								TSK_sleep(100);						
								Altera_Flash_Erase_Init();
								Altera_Flash_Done();
								_Altera_Erase_Done = true;
								_write_seq = _Write_Seq_Idle;
								break;


							//------------------ IDLE WRITE SEQUENCE ------------------------------------------------------------------------------
							default :				// Idle or undefined
								// Check to see if it is command
								switch( data & krwKey_Mask ) 
								{

									case hwKey_Data : 												// Write Key Detected
										_write_addr = data & 0xFFF;									// Save the Write Start Address
										_write_end  = ( data >> 12 ) & 0xFFF;							// Save the Write End Address
										_write_seq  = _Write_Seq_Write;											// Assert "Write Sequence"
										break;

									case kwKey_Generic_Flash :
										_write_addr = (data & 0xFFF);								// Save the Write Start Addres														
										_write_end = ( data >> 12 ) & 0xFFF;						// Save the Write End Address
										_write_seq = _Write_Seq_Generic_Flash;
										break;

									case hwKey_AmpTime :											// New Amp Time or Ev/Ch)
										SetAmpTime( data );								// Set the New AmpTime (data contains the new information)

										data = krKey_Param_Data | hrKey_Data;
										data |= ( iADR_RO_MAX << 12 );
										data |= ( iADR_SYNC_REG );

										memcpy( &param_host_buffer[_write_ptr], &data, 4 );				// echo the commnad back to the output buffer
										_write_ptr += 4;											// advance the pointer by 4
										for(j=iADR_SYNC_REG;j<iADR_RO_MAX;j++)
										{
											data = Altera_Reader( awCmd_Read | j );								// Read back the data from the Altera FPGA
											memcpy( &param_host_buffer[_write_ptr], &data, 4 );			// Copy the data from to the "output" buffer
											_write_ptr += 4;										// advance the _write_ptr by 4
										}
										// Now send the data back to the host and reset the "Write pointer"
										send_buff( mnParamConnectedSocket, param_host_buffer, _write_ptr );			// Sernd the Data
										_write_ptr = 0;															// reset the pointer

										break;

									case hwKey_Time_Start_Stop:
										mpArtemis_Dma->Add_TimeBuffer( 0x10 );
										//mpAlteraBaseReg[XA_IADR_USER_IO] = USER_IO_MODE | 0x03;	
										Altera_Command( awCmd_Time_Start_Stop | ( data & 0x07 ));
										mpArtemis_Dma->Add_TimeBuffer( 0x11 );
										break;

//									case hwKey_Time_Clear:
//										mpArtemis_Dma->Add_TimeBuffer( 1 );
//										Altera_Command( awCmd_Time_Clear );
//										mpArtemis_Dma->Add_TimeBuffer( 2 );
//										break;
//
//									case hwKey_Time_Clear_Start :
//										mpArtemis_Dma->Add_TimeBuffer( 0x12 );
//										Altera_Command( awCmd_Time_Clear | 0x1 );		// if LSB is set, will automatically restart collection
//										mpArtemis_Dma->Add_TimeBuffer( 0x13 );
//										break;

									case hwKey_Fir_Mr:
										Altera_Command( awCmd_Fir_Mr );
										break;

									case hwKey_Fifo_MR:
										Altera_Command( awCmd_Fifo_mr );
										break;

									case hwKey_SCABB:	
										_write_addr = 0;
										_write_end  = 128;
										_write_seq  = _Write_Seq_SCABBWrite;
										break;

									case hwKey_Blevel_Start_Stop:
										Altera_Command( awCmd_Blevel_Start_Stop | ( data & 0x01 ));
										break;

									case hwKey_Dss_Start_Stop:
										Altera_Command( awCmd_Dss_Start_Stop | ( data & 0x01 ));
										break;

									case hwKey_Slide_Move:
										Altera_Command( awCmd_Slide_Move | ( data & 0xFFFF ));
										break;

									case hwKey_VerInfo : 
										if( _write_ptr > 0 )
										{
											send_buff( mnParamConnectedSocket, param_host_buffer, _write_ptr );			// send it back to the host
											_write_ptr = 0;
											TSK_sleep(250);
										}

										snprintf(ser_buf,512,"Getting Version Information" );
										if( _SerialVerbose == true )
											SendSerial( ser_buf );
										data = krKey_VerInfo;
										data |= krwKey_Data_Mask;												// Append Data Key
										memcpy( &param_host_buffer[_write_ptr], &data, 4 );							// copy data to the output buffer Command
										_write_ptr += 4;

										data = mn_dsp_ver; 														// DSP Version
										memcpy( &param_host_buffer[_write_ptr], &data, 4 );							// copy data to the output buffer Index 0
										_write_ptr += 4;

										data = mn_xilinx_ver;													// Xilinx Version
										memcpy( &param_host_buffer[_write_ptr], &data, 4 );							// copy data to the output buffer Index 1
										_write_ptr += 4;

										data = Altera_Reader( awCmd_Read | iADR_ALTERA_VER );					// Altera Version
										memcpy( &param_host_buffer[_write_ptr], &data, 4 );							// copy data to the output buffer Index 2
										_write_ptr += 4;

										memcpy( &data, &_Dpp_Data[0], 4 );										// Get AmpTime, Evch and 
										memcpy( &param_host_buffer[_write_ptr], &data, 4 );							// copy data to the output buffer Index 3
										_write_ptr += 4;


										// Now Append Network Parameters						  	// send_str(mnConnectedSocket,"Telnet Echo Connected, Echoing Serial Input...\r\n");
									    mpNetIFConfigUtil->GetConfig(&lsConfig);					// Get the Network Configuration

										// Current DHCP Enble
										if(( msNetIf.dhcp == false ) || (msNetIf.dhcp->state == DHCP_OFF))
											data = 0;									// if Current DHCP is enabled and the state is On set bit 1
										else 
											data = 1;

										// Next boot DHCP Enable
										if( lsConfig.mnUseDHCP == true )				// Check Next DHCP
											data |= 2;	

										// Next boot Wins Enable
										if( lsConfig.mnUseWINS == true )				// Check Next Wins
											data |= 4;
										memcpy( &param_host_buffer[_write_ptr], &data, 4 );							// copy data to the output buffer Index 4
										_write_ptr += 4;

										// Current IP Address
										tmp = msNetIf.ip_addr.addr;
										data = ( tmp & 0xFF ) << 24;
										data |= ( tmp & 0xFF00 ) << 8;
										data |= ( tmp & 0xFF0000 ) >> 8;
										data |= ( tmp & 0xFF000000 ) >> 24;
										memcpy( &param_host_buffer[_write_ptr], &data, 4 );							// copy data to the output buffer Index 5
										_write_ptr += 4;

										// Currnet Gateway
										tmp = msNetIf.gw.addr;
										data = ( tmp & 0xFF ) << 24;
										data |= ( tmp & 0xFF00 ) << 8;
										data |= ( tmp & 0xFF0000 ) >> 8;
										data |= ( tmp & 0xFF000000 ) >> 24;
										memcpy( &param_host_buffer[_write_ptr], &data, 4 );							// copy data to the output buffer Index 6
										_write_ptr += 4;
	 
										// Current Netmmask
										tmp  = msNetIf.netmask.addr;
										data = ( tmp & 0xFF ) << 24;
										data |= ( tmp & 0xFF00 ) << 8;
										data |= ( tmp & 0xFF0000 ) >> 8;
										data |= ( tmp & 0xFF000000 ) >> 24;
										memcpy( &param_host_buffer[_write_ptr], &data, 4 );							// copy data to the output buffer Index 7
										_write_ptr += 4;

										// Next boot IP Address
										tmp  = lsConfig.msIpAddr.addr;
										data = ( tmp & 0xFF ) << 24;
										data |= ( tmp & 0xFF00 ) << 8;
										data |= ( tmp & 0xFF0000 ) >> 8;
										data |= ( tmp & 0xFF000000 ) >> 24;
										memcpy( &param_host_buffer[_write_ptr], &data, 4 );							// copy data to the output buffer Index 8
										_write_ptr += 4;

										// Next Boot Gateway
										tmp  = lsConfig.msGateway.addr;
										data = ( tmp & 0xFF ) << 24;
										data |= ( tmp & 0xFF00 ) << 8;
										data |= ( tmp & 0xFF0000 ) >> 8;
										data |= ( tmp & 0xFF000000 ) >> 24;
										memcpy( &param_host_buffer[_write_ptr], &data, 4 );							// copy data to the output buffer Index 9
										_write_ptr += 4;

										// Next boot netmask
										tmp  = lsConfig.msNetmask.addr;
										data = ( tmp & 0xFF ) << 24;
										data |= ( tmp & 0xFF00 ) << 8;
										data |= ( tmp & 0xFF0000 ) >> 8;
										data |= ( tmp & 0xFF000000 ) >> 24;
										memcpy( &param_host_buffer[_write_ptr], &data, 4 );							// copy data to the output buffer Index 10
										_write_ptr += 4;

										// Current WINS Name
										memcpy( &param_host_buffer[_write_ptr], &lsConfig.maNetBIOSName, 16 );		// copy Wins Name into buffer (16 bytes)
										_write_ptr += 16;


										// Serial Port Settings
										data = 0;
										data |= ( _SerialEnable   << 0 );
										data |= ( _SerialVerbose  << 1 );
										memcpy( &param_host_buffer[_write_ptr], &data, 4 );
										_write_ptr += 4;

										// Now send the data back to the host and reset the "Write pointer"
										send_buff( mnParamConnectedSocket, param_host_buffer, _write_ptr );			// Sernd the Data
										_write_ptr = 0;															// reset the pointer

										break;


									case hwKey_Read_Data_Enable: 
										Altera_Command( awCmd_Read_Data_Enable_Disable | ( data & 0x01 ));
										break;

									case hwKey_SetDef :
										snprintf(ser_buf,512,"\tSetting Default Parameters\r\n" );
										if( _SerialVerbose == true )
											SendSerial( ser_buf );
										SetDefParams();
										_flash_update = true;

										memcpy( &data, &_Dpp_Data[0], 4 );					// Get Current Amp Time
										SetAmpTime( data );						// Set the New AmpTime (data contains the new information)

										// Now Echo back all of the parameters back to the host...
										data = krKey_Param_Data | hrKey_Data;
										data |= ( iADR_RO_MAX << 12 );
										data |= ( iADR_SYNC_REG );

										memcpy( &param_host_buffer[_write_ptr], &data, 4 );				// echo the commnad back to the output buffer
										_write_ptr += 4;											// advance the pointer by 4
										for(j=iADR_SYNC_REG;j<iADR_RO_MAX;j++)
										{
											data = Altera_Reader( awCmd_Read | j );					// Read back the data from the Altera FPGA
											memcpy( &param_host_buffer[_write_ptr], &data, 4 );			// Copy the data from to the "output" buffer
											_write_ptr += 4;										// advance the _write_ptr by 4
										}
										// Now send the data back to the host and reset the "Write pointer"
										send_buff( mnParamConnectedSocket, param_host_buffer, _write_ptr );			// Sernd the Data
										_write_ptr = 0;															// reset the pointer
										break;

									case hwKey_DppInit :
										snprintf(ser_buf,512,"\tInitializing DPP Parameters\r\n" );
										if( _SerialVerbose == true )
											SendSerial( ser_buf );
										memcpy( &data, &_Dpp_Data[0], 4 );					// Get Current Amp Time
										SetAmpTime( data );						// Set the New AmpTime (data contains the new information)

										// Now Echo back all of the parameters back to the host...
										data = krKey_Param_Data | hrKey_Data;
										data |= ( iADR_RO_MAX << 12 );
										data |= ( iADR_SYNC_REG );

										memcpy( &param_host_buffer[_write_ptr], &data, 4 );				// echo the commnad back to the output buffer
										_write_ptr += 4;											// advance the pointer by 4
										for(j=iADR_SYNC_REG;j<iADR_RO_MAX;j++)
										{
											data = Altera_Reader( awCmd_Read | j );							// Read back the data from the Altera FPGA
											memcpy( &param_host_buffer[_write_ptr], &data, 4 );						// Copy the data from to the "output" buffer
											_write_ptr += 4;													// advance the _write_ptr by 4
										}
										// Now send the data back to the host and reset the "Write pointer"
										send_buff( mnParamConnectedSocket, param_host_buffer, _write_ptr );			// Sernd the Data
										_write_ptr = 0;															// reset the pointer

										break;
									case hwKey_Dpp_Reset :
							            snprintf(ser_buf,sizeof(ser_buf),"  >>> Rebooting MityDSP, please wait  \r\n");
						    	        SendSerial(ser_buf);               
										TSK_sleep(1000);
									     // disable firmware interrupts
										LCK_pend(ghFlashLock,LCK_TIMEOUT);
										tcDspFirmware::global_int_disable(IE_MASK_4 | IE_MASK_5 | IE_MASK_6 | IE_MASK_7);
								        tcDspBootstrapper::Reboot(mpFlash);
										LCK_post(ghFlashLock);	//ok, should never get here but I like the balance
										break;

									case hwKey_Serial_Enable :
										// Bit 0 is the Serial Port Enable/Disable
										if(( data & 0x01 ) == 0 )
											_SerialEnable = false;
										else 
											_SerialEnable = true;

										// Bit1 is the Serial Port Verbose
										if(( data & 0x02 ) == 0 )
											_SerialVerbose = false;
										else
											_SerialVerbose = true;
										break;

									case hwKey_Next_DHCP :
										_write_seq  = _Write_Seq_Next_DHCP;											// Assert "Write Sequence"
										break;

									case hwKey_Next_WINS :
										_write_seq = _Write_Seq_Next_Wins;
										break;

									case hwKey_Next_IPAddr :
										_write_seq = _Write_Seq_Next_IPAddr;
										break;

									case hwKey_Next_Gateway	:
										_write_seq = _Write_Seq_Next_Gateway;
										break;

									case hwKey_Next_Netmask	:
										_write_seq = _Write_Seq_Next_Netmask;
										break;

									case hwKey_Next_Wins_Name :
										_write_addr = 0;
										_write_seq = _Write_Seq_Next_Wins_Name;
										break;

									case hwKey_Time_Start_Ack:
										//mpAlteraBaseReg[XA_IADR_USER_IO] = USER_IO_MODE | 0x00;	
										break;
									case hwKey_Ack_Req :
										mpArtemis_Dma->Add_TimeBuffer( 3 );
										data &= 0xFFFF;												// keep the lower 16 bits
										data |= krwKey_Data_Mask;									// Append Data Key
										data |= krKey_Ack_Req;										// Append Acq Key
										memcpy( &param_host_buffer[_write_ptr], &data, 4 );							// copy data to the output buffer
										_write_ptr += 4;
										send_buff( mnParamConnectedSocket, param_host_buffer, _write_ptr );			// send it back to the host
										_write_ptr = 0;
										mpArtemis_Dma->Add_TimeBuffer( 4 );
										break;

									case hrKey_Data :
										memcpy( &param_host_buffer[_write_ptr], &data, 4 );				// echo the commnad back to the output buffer
										_write_ptr += 4;											// advance the pointer by 4
										start_addr = data & 0xFFF;									// Save the Write Start Address
										end_addr  = ( data >> 12 ) & 0xFFF;							// Save the Write End Address
										for(j=start_addr;j<end_addr;j++)
										{
											data = Altera_Reader( awCmd_Read | j );					// Read back the data from the Altera FPGA
											memcpy( &param_host_buffer[_write_ptr], &data, 4 );			// Copy the data from to the "output" buffer
											_write_ptr += 4;										// advance the _write_ptr by 4
										}
										// Now send the data back to the host and reset the "Write pointer"
										send_buff( mnParamConnectedSocket, param_host_buffer, _write_ptr );			// Sernd the Data
										_write_ptr = 0;															// reset the pointer
										break;
									case hrKey_SCABB :
										data = krKey_Param_Data | krKey_SCABB_Start;
										memcpy( &param_host_buffer[_write_ptr], &data, 4 );				// echo the commnad back to the output buffer
										_write_ptr += 4;											// advance the pointer by 4
										for(j=0;j<128;j++)
										{
											data = Altera_Reader( awCmd_Read_SCABB | j );
											memcpy( &param_host_buffer[_write_ptr], &data, 4 );			// Copy the data from to the "output" buffer
											_write_ptr += 4;										// advance the _write_ptr by 4
										}
										// Now send the data back to the host and reset the "Write pointer"
										send_buff( mnParamConnectedSocket, param_host_buffer, _write_ptr );			// Sernd the Data
										_write_ptr = 0;															// reset the pointer
										break;
									case krKey_Generic_Flash :
										data = krKey_Param_Data | krKey_Generic_Flash_Start;
										memcpy( &param_host_buffer[_write_ptr], &data, 4 );				// echo the commnad back to the output buffer
										_write_ptr += 4;											// advance the pointer by 4


										for(j=0;j<kFLASH_NUM_OTHERS;j++)
										{
											memcpy( &data, &_Dpp_Data[Generic_Param_Addr(j)], 4 );	// Copy NVRAM data[i] to variable data
											memcpy( &param_host_buffer[_write_ptr], &data, 4 );			// Copy the data from to the "output" buffer
											_write_ptr += 4;													// advance the _write_ptr by 4
										}
										// Now send the data back to the host and reset the "Write pointer"
										send_buff( mnParamConnectedSocket, param_host_buffer, _write_ptr );			// Sernd the Data
										_write_ptr = 0;															// reset the pointer
										break;
									case kwKey_Flash_Altera :
										_write_seq = _Write_Flash_Altera;
										break;

									case kwKey_Flash_Xilinx :
										_write_seq = _Write_Flash_Xilinx;
										break;

									case kwKey_Flash_Dsp :
										_write_seq = _Write_Flash_DSP;
										break;

									case kwKey_Erase_Altera :
										_write_seq = _Write_Erase_Altera;
										break;

									case kwKey_Read_TimeBuffer :
										// TODO - Add Read-Key
										data = krKey_Param_Data | krKey_TimeBuffer_Start;
										memcpy( &param_host_buffer[_write_ptr], &data, 4 );				// echo the commnad back to the output buffer
										_write_ptr += 4;											// advance the pointer by 4

										for(i=0;i<TimeBufferSize;i++)
										{
											data = mpArtemis_Dma->Get_TimeBuffer(i);		// Get the Data from the time-buffer
											memcpy( &param_host_buffer[_write_ptr], &data, 4 );
											_write_ptr += 4;
										}
										send_buff( mnParamConnectedSocket, param_host_buffer, _write_ptr );			// send it back to the host
										_write_ptr = 0;
										mpArtemis_Dma->Set_TimeBufferPtr( 0 );
										break;

	 								// Any other keys, do nothing
									default :
										break;
								}
								break;
							//------------------ IDLE WRITE SEQUENCE ------------------------------------------------------------------------------
						}																// switch write sequence
						ptr += 4;														// Increment pointer by 4 (bytes)
					}
					while( ptr < length );

					// A parameter has changed ( so the flash must be updated ) AND the flash is not busy and not in the middle of a write sequence
					if(( _flash_update == true ) && ( _write_seq == _Write_Seq_Idle ) && ( _Flash_Busy == false ))
					{				
						// Copy _Dpp_Data structure into another replica _Dpp_Flash_Data which is used by the binary semaphore to write the data to flash
						// The semaphore will set and clear the _Flash_Busy flag
						for( int i=0; i<kDPP_FLASH_SIZE; i++){
							_Dpp_Flash_Data[i] =_Dpp_Data[i];
						}
						SEM_postBinary( mhFlashSem );		// Post the Binary Semaphore ( non-counting) to trigger the thread to write the flash				
						_flash_update = false;				// Reset the flag indicating a parameter has changed
					}
				}								// Any other data processing
				//************* Any other processing ************************************************************************


       	    }									// Length >= 0 

        }										// if FDSET
        //----- Process data on the Server Socket -------------------------------------------------
		if(( _Flash_Altera == false ) && ( _Flash_Xilinx == false ) && ( _Flash_DSP == false ))
		{
			snprintf(ser_buf,512,"Processed RxData on Parameter Port [%d]\r\n----------------------------------------------------------------------------\r\n", mnParamConnectedSocket );
			if( _SerialVerbose == true )
				SendSerial( ser_buf );
		}
    }
}
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
void tcArtemisDsp::OneSec_Thread( void )
{
	static char buffer[2048];
	static bool Last_Link;
	bool Current_Link;
	bool LinkUp;
	static int lc;
	unsigned int data;
	int _write_ptr = 0;			//separated from param usage

	sys_thread_register( TSK_self());

	while(1)
	{
		TSK_sleep(1000);			// sleep for 1 Second
		mpArtemis_Dma->Add_TimeBuffer( 0x20 );

		if(( _Flash_Altera == false ) && ( _Flash_Xilinx == false ) && ( _Flash_DSP == false ))
		{
			lc ++;
			// Assign Last_Link with Current Link value

			// lsStatus.mbLinkup is somewhat "buggy" gets false triggers
		    tsNetPhyStatus lsStatus;
		    ((tcNetDrvr_B*)mpNetDrvr)->GetPhyStatus (lsStatus);		

			// Linkup is more reliable 
			LinkUp = ((tcNetDrvr_B*)mpNetDrvr)->LinkUp();


			// Use the fact of both to determine the current link status
			(( lsStatus.mbLinkUp == true ) || ( LinkUp == true )) ? Current_Link = true : Current_Link = false;
			snprintf(buffer,128,"\r\n lc %d Status Up %d LinkUp %d Current Link %d", lc, lsStatus.mbLinkUp, LinkUp, Current_Link );

			if(( Current_Link == true ) && ( Last_Link == false ))
			{
				snprintf(buffer,128,"\r\nNetwork Cable Connected\r\n" );
				if( _SerialVerbose == true )
					SendSerial(buffer);
				TSK_sleep(100);			
			}
			else if(( Current_Link == false ) && ( Last_Link == true ))
			{
				snprintf(buffer,128,"\r\nNetwork Cable Disconnected\r\nRetracting Detector and setting it off-line\r\n" );
				if( _SerialVerbose == true )
					SendSerial(buffer);
				TSK_sleep(100);
				// Set_Standby( true ); // Sometimes gives trouble on board with intermittent connections
			}																					// if current_link && Last_link
			Last_Link 		= Current_Link;														// Done with "Link Status" to update last link
		}																						// if not flashing...

		if( _flash_status_reset > 0 )
		{
			_flash_status_reset --;
			if( _flash_status_reset == 0 )
			{
				data = krwKey_Data_Mask;												// Append Data Key
				data |= krKey_Flash_Status;
				data |= 0;																// Flash Done for DSP (6= Xilinx)
				memcpy( &onesec_host_buffer[_write_ptr], &data, 4 );							// copy data to the output buffer Command
				_write_ptr += 4;
				send_buff( mnParamConnectedSocket, onesec_host_buffer, _write_ptr );			// send it back to the host
				_write_ptr = 0;
			}
		}

		if( _ALTERA_Flash_Done == true )
		{
			_flash_status_reset = 5;
			data = krwKey_Data_Mask;												// Append Data Key
			data |= krKey_Flash_Status;
			data |= 3;																// Flash Done for DSP (6= Xilinx)
			memcpy( &onesec_host_buffer[_write_ptr], &data, 4 );							// copy data to the output buffer Command
			_write_ptr += 4;
			send_buff( mnParamConnectedSocket, onesec_host_buffer, _write_ptr );			// send it back to the host
			_write_ptr = 0;
			_ALTERA_Flash_Done = false;
		}

		if( _Altera_Erase_Done == true )
		{
			_flash_status_reset = 5;
			data = krwKey_Data_Mask;												// Append Data Key
			data |= krKey_Flash_Status;
			data |= 3;																// Flash Done for DSP (6= Xilinx)
			memcpy( &onesec_host_buffer[_write_ptr], &data, 4 );							// copy data to the output buffer Command
			_write_ptr += 4;
			send_buff( mnParamConnectedSocket, onesec_host_buffer, _write_ptr );			// send it back to the host
			_write_ptr = 0;
			_Altera_Erase_Done = false;
		}

		if( _Flash_DSP_Error == true )
		{
			_flash_status_reset = 5;
			_Flash_DSP_Error = false;
			snprintf( buffer, 512, "\r\nFLASH DSP Failed\r;\0" );
			data = krwKey_Data_Mask;												// Append Data Key
			data |= krKey_Flash_Status;
			data |= 0xA;															// Flash Done for DSP (6= Xilinx)
			if( _SerialVerbose == true )
				SendSerial( buffer );
			memcpy( &onesec_host_buffer[_write_ptr], &data, 4 );							// copy data to the output buffer Command
			_write_ptr += 4;
			send_buff( mnParamConnectedSocket, onesec_host_buffer, _write_ptr );			// send it back to the host
			_write_ptr = 0;
		} 


		if( _Flash_Xilinx_Error == true ) 
		{
			_flash_status_reset = 5;
			_Flash_Xilinx_Error = false;
			snprintf( buffer, 512, "\r\nFLASH Xilinx Failed\r;\0" );
			data = krwKey_Data_Mask;												// Append Data Key
			data |= krKey_Flash_Status;
			data |= 0xB;																// Flash Done for DSP (6= Xilinx)
			if( _SerialVerbose == true )
				SendSerial( buffer );
			memcpy( &onesec_host_buffer[_write_ptr], &data, 4 );							// copy data to the output buffer Command
			_write_ptr += 4;
			send_buff( mnParamConnectedSocket, onesec_host_buffer, _write_ptr );			// send it back to the host
			_write_ptr = 0;
		}

		if( _DSP_Flash_Done == true )
		{
			_flash_status_reset = 5;
			snprintf( buffer, 512, "\r\nFLASH DSP COMPLETE\r;\0" );
			data = krwKey_Data_Mask;												// Append Data Key
			data |= krKey_Flash_Status;
			data |= 9;																// Flash Done for DSP (6= Xilinx)
			if( _SerialVerbose == true )
				SendSerial( buffer );
			memcpy( &onesec_host_buffer[_write_ptr], &data, 4 );							// copy data to the output buffer Command
			_write_ptr += 4;
			send_buff( mnParamConnectedSocket, onesec_host_buffer, _write_ptr );			// send it back to the host
			_write_ptr = 0;
			_DSP_Flash_Done = false;
		}

		if( _XILINX_Flash_Done == true )
		{
			_flash_status_reset = 5;
			snprintf( buffer, 512, "\r\nFLASH Xilinx COMPLETE\r;\0" );
			data = krwKey_Data_Mask;												// Append Data Key
			data |= krKey_Flash_Status;
			data |= 6;																// Flash Done for DSP (6= Xilinx)
			if( _SerialVerbose == true )
				SendSerial( buffer );
			memcpy( &onesec_host_buffer[_write_ptr], &data, 4 );							// copy data to the output buffer Command
			_write_ptr += 4;
			send_buff( mnParamConnectedSocket, onesec_host_buffer, _write_ptr );			// send it back to the host
			_write_ptr = 0;
			_XILINX_Flash_Done = false;
		}
		mpArtemis_Dma->Add_TimeBuffer( 0x21 );
		_Dsp_Counts ++;
		data = krwKey_Data_Mask;												// Append Data Key
		data |= krKey_DSP_Counts;
		data |= ( _Dsp_Counts & 0xFFFFFF );
		memcpy( &onesec_host_buffer[_write_ptr], &data, 4 );
		_write_ptr += 4;
		send_buff( mnParamConnectedSocket, onesec_host_buffer, _write_ptr );			// send it back to the host
		_write_ptr = 0;
		mpArtemis_Dma->Add_TimeBuffer( 0x22 );
	}																						// while true
}

//-------------------------------------------------------------------------------------------------






//-------------------------------------------------------------------------------------------------
void sendit(char *str)
{
tcArtemisDsp::SendSerial(str,false);
}
