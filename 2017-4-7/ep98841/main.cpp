/**
 * \file main.cpp
 *
 * This file includes the code for the main entry point for the 
 * MityDSP application.
 * 
 *  \author  Critical Link LLC
 *
 *  \date    8/2009
 *
 *  \remarks
 *
 *
 *  \verbatim

      o  0
      | /       Copyright (c) 2009
     (CL)---o   Critical Link, LLC
       \
        O

    \endverbatim
 **/
#include <csl.h>
#include <std.h>
#include <tsk.h>
#include "ep98841.h"
//#include <core/DspMacros.h>
#define CPU_FREQ_200MHZ  0x00000001
#ifndef PLL_DIV0
#include "boot/c6711reg.h"
#endif
#define RECONFIG_CLOCKS(CPU_FREQ) \
{ \
    /* First, figure out if we are running on an XM or Standard MityDSP. */          \
    /* The XM part requires a 7.81 usec refresh timing */                            \
    /* The Standard part requires a 15.62 usec refresh timing  */                    \
    /* This can be determined by reading the SDRAM control register and  */          \
    /* checking the size of the RAM we are using */                                  \
    int isXM = 0;                                                                    \
    volatile unsigned int emif_sdctrl = *(unsigned volatile int *)EMIF_SDCTRL;       \
    if (emif_sdctrl & 0x7C000000 == 0x50000000)                                      \
    {                                                                                \
        isXM = 1;                                                                    \
    }                                                                                \
    if ( ((CPU_FREQ >= CPU_FREQ_200MHZ) && (CPU_FREQ <= CPU_FREQ_200MHZ))  )          \
    {                                                                                \
        /* If the PLL_MULTIPLIER is not set to 16 (giving 400 MHz clock to work with)\
           then reset the PLL */                                                     \
        int PLL_RECONFIG = !(((*(unsigned volatile int*)PLL_MULT)&0x01F) == 0x010);    \
        if (PLL_RECONFIG)                                                            \
        {                                                                            \
           /* First set SDRAM refresh to handle the base 25/4 EMIF clock that will  */  \
           /* be present while PLL is locking...          */                            \
           if (isXM)                                                                    \
              *(unsigned volatile int *)EMIF_SDRP   = 0x00000030; /* SDRAM refresh period, 7.68 usec @ 6.25 Mhz*/ \
           else                                                                         \
              *(unsigned volatile int *)EMIF_SDRP   = 0x00000061; /* SDRAM refresh period, 15.62 usec @ 6.25 Mhz*/ \
           /* perform PLL initialization */                                             \
           *(unsigned volatile int *)PLL_CSR  = 0x00000008; /* PLLRST=1; PLLEN=0 */     \
           *(unsigned volatile int *)PLL_DIV0 = 0x00008000; /* D0 Enable; 1/1 */        \
           *(unsigned volatile int *)PLL_MULT = 0x00000010; /* mult=16x -> 400 Mhz */    \
           *(unsigned volatile int *)OSC_DIV1 = 0x00008000; /* OscDiv En; 1/1 */        \
           *(unsigned volatile int *)PLL_DIV1 = 0x00008003; /* D1 Enable; 1/4 -> 100 MHz */ \
           *(unsigned volatile int *)PLL_DIV2 = 0x00008007; /* D2 Enable; 1/8 -> 50 Mhz */  \
           *(unsigned volatile int *)PLL_DIV3 = 0x00008007; /* D3 Enable; 1/8 -> 50 Mhz*/   \
           *(unsigned volatile int *)PLL_CSR  = 0x00000000; /* PLLRST=0; PLLEN=0 */     \
           while(!(*(unsigned volatile int *)PLL_CSR & 0x00000020)); /* wait for pll-lock */   \
           *(unsigned volatile int *)PLL_CSR  = 0x00000001; /* PLLRST=0; PLLEN=1 */     \
           if (isXM)                                                                    \
              *(unsigned volatile int *)EMIF_SDRP   = 0x00000186; /* SDRAM refresh period, 7.8 usec @ 50 Mhz*/   \
           else                                                                                                  \
              *(unsigned volatile int *)EMIF_SDRP   = 0x0000030D; /* SDRAM refresh period, 15.62 usec @ 50 Mhz*/ \
           /* bogus write to FPGA to ensure EMIF is locked, this will stall processor */   \
           /* until the FPGA has dealt with clock bounce */                                \
           *(unsigned volatile int *)0xB0000000 = 0xDEADBEEF;                              \
        }                                                                               \
        int curCPUDiv =  (*(unsigned volatile int *)PLL_DIV1)&0x1F;                     \
        int DIV2Setting = (((CPU_FREQ)+1)<<1)-1;                                        \
        if (curCPUDiv < CPU_FREQ) /* slow down D2 is first */                           \
        {                                                                               \
           *(unsigned volatile int *)PLL_DIV2 = 0x8000|DIV2Setting;   /* D2 Enable */   \
           *(unsigned volatile int *)PLL_DIV1 = 0x8000|CPU_FREQ;        /* D1 Enable */ \
        }                                                                               \
        else /* speed up, D1 is first */                                                \
        {                                                                               \
           *(unsigned volatile int *)PLL_DIV1 = 0x8000|CPU_FREQ;        /* D1 Enable */ \
           *(unsigned volatile int *)PLL_DIV2 = 0x8000|DIV2Setting;   /* D2 Enable */   \
        }                                                                               \
    }                                                                                   \
}


asm("               .def ___far__");   //
asm("___far__       .set 0x80080000");       // START OF MEMORY Following      
asm("               .def ___fend__");  
asm("___fend__      .set 0x00000000");       // LEAVE AS ZERO TO USE END OF .bss

int main(int argc, char* argv[])
{
    // speed up CPU to 200 Mhz for processing margin.
    RECONFIG_CLOCKS(CPU_FREQ_200MHZ) 

    CSL_init();

    TSK_Attrs* lpAttrs = new TSK_Attrs;
    *lpAttrs           = TSK_ATTRS;
    lpAttrs->name      = "Initialize";
    lpAttrs->stacksize = 8192*2;
    lpAttrs->priority  = 5;
//    TSK_create((Fxn)tcTelnetEchoExample::InitializeDispatch,lpAttrs);
    TSK_create((Fxn)tcArtemisDsp::InitializeDispatch,lpAttrs);
    return 1;
    
}

