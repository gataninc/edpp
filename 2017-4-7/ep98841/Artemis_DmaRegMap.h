#ifdef MITYDSP_HEADER_BLOCK
/**************************************************************************/
/*                                                                        */
/*  $------------------------------------------------------------------$  */
/*                                                                        */
/*  $Workfile:: DspCameraLinkRegMap.h                                  $  */
/*  $Revision:: 5                                                      $  */
/*  $Date:: 9/21/07 8:22a                                              $  */
/*  $Author:: Mikew                                                    $  */
/*                                                                        */
/*  $------------------------------------------------------------------$  */
/*                                                                        */
/*  $Description::                                                     $  */
/*   Register definitions for the Camera Link Base class.                 */
/*                                                                        */
/*  $------------------------------------------------------------------$  */
/*                                                                        */
/*     o  0                                                               */
/*     | /       Copyright (c) 2006-2007                                  */
/*    (CL)---o   Critical Link, LLC                                       */
/*      \                                                                 */
/*       O                                                                */
/**************************************************************************/
#endif

#ifndef tcArtemis_DmaRegMap_H
#define tcArtemis_DmaRegMap_H

#define VER_OFFSET  0
#define CCR_OFFSET  1
#define FDR_OFFSET  16
#define FZR_OFFSET  3
#define IER_OFFSET  4
#define ISR_OFFSET  5
#define PCR_OFFSET  6
#define FTR_OFFSET  7
#define DSR_OFFSET  8
#define DER_OFFSET  9
#define DISR_OFFSET 10
#define HTR_OFFSET  11
#define VTR_OFFSET  12

// CCR Register
typedef union
{
    struct 
    {
        unsigned int mbEnableCapture  : 1;   //!< set to 1 to enable camera capture
        unsigned int mbPack           : 1;   //!< set to 1 to pack data (if appropriate)
        unsigned int mbCCEnable       : 1;   //!< set to 1 to enable camera control drivers
        unsigned int mbMaskFrameBits  : 1;   //!< set to 1 to mask Frame and Line bits to zero
		 		 unsigned int mbResetFIFO      : 1;   //!< when 1, attached FIFO is cleared
		 		 unsigned int mbXORDValid      : 1;   //!< when 1, invert DVALID signal
		 		 unsigned int mbInvertLV       : 1;   //!< Invert Line Valid signal
		 		 unsigned int mbInvertFV       : 1;   //!< Invert Frame Valid signal
		 		 unsigned int mnFrameDecimate  : 4;   //!< number of frames to skip between successive captures (0 to 15)
		 		 unsigned int mbDMAEnable      : 1;   //!< set to 1 if supported and direct DMA to SDRAM is desired
		 		 unsigned int mbDMARollEnable  : 1;   //!< set to 1 if DMA should continue and roll over in SDRAM (circular buffer)
		 		 unsigned int mbDMAAvailable   : 1;   //!< read only, set to 1 if DMA engine is available on FPGA build
        unsigned int mnRSV            : 1;   //!< unused
        unsigned int mnPXD            : 4;   //!< Bits per pixel
        unsigned int mnRSV2           : 12;  //!< unused
    } msBits;
    unsigned int mnLword;
} tuCamLinkCcr;

// FZR Register
typedef union
{
    struct 
    {
        unsigned int mnLevel    : 16;
        unsigned int mnDepth    : 16;
    } msBits;
    unsigned int mnLword;
} tuCamLinkFzr;

// Interrupt Status Register
typedef union
{
    struct 
    {
        unsigned int mbFifoNotEmpty   :  1;
        unsigned int mbFifo1QFull     :  1;
        unsigned int mbFifoHalfFull   :  1;
        unsigned int mbFifo3QFull     :  1;
        unsigned int mbFifoFull       :  1;
        unsigned int mbDmaCompleted   :  1;
        unsigned int mnReserved       : 26;
    } msBits;
    unsigned int mnLword;
} tuCamLinkIsr;

// Interrupt Enable Register
typedef union
{
    struct 
    {
        unsigned int mbIntNotEmpty      :  1;
        unsigned int mbInt1QFull        :  1;
        unsigned int mbIntHalfFull      :  1;
        unsigned int mbInt3QFull        :  1;
        unsigned int mbIntFull          :  1;
        unsigned int mbIntDmaCompleted  :  1;
        unsigned int mnReserved         : 26;
    } msBits;
    unsigned int mnLword;
} tuCamLinkIer;

typedef union
{
    struct
		 {
		     unsigned int mnNumPixels  : 24;
		 		 unsigned int mnReserved   :  8;
    } msBits;
		 unsigned int mnLword;
} tuCamLinkPcr;

typedef union
{
    struct
		 {
		     unsigned int mnFrameTime  : 24;
		 		 unsigned int mnReserved   :  8;
    } msBits;
		 unsigned int mnLword;
} tuCamLinkFtr;

typedef union
{
    struct
    {
        unsigned int mnStartAddress : 30;
        unsigned int mnReserved     : 2;
    } msBits;
    unsigned int mnLword;
} tuCamLinkDsr;

typedef union
{
    struct
    {
        unsigned int mnEndAddress : 30;
        unsigned int mnReserved   : 2;
    } msBits;
    unsigned int mnLword;
} tuCamLinkDer;

typedef union
{
    struct
    {
        unsigned int mnNumWordsPerInt : 28;
        unsigned int mnReserved       : 4;
    } msBits;
    unsigned int mnLword;
} tuCamLinkDisr;


/**
 * HTR Register.
 * The Horizontal Data Timing Register allows users to discard pixels 
 * received in a horizontal line (when the data valid, line valid, and
 * frame valid strobes are asserted).  At the beginning of a line (rising
 * edge of line valid), the number of pixels specified by START_PIXEL will
 * be discarded and not clocked into the FIFO.  Once the START_PIXEL has
 * been reached, the core will then capture until END_PIXEL 
 * or until the line valid is de-asserted, at which point the
 * horizontal capture state machine will reset.
 */
typedef union
{
    struct 
    {
        unsigned int mnStop      : 12; //!< Pixel position to stop capturing
        unsigned int mnReserved  :  4; //!< not used
        unsigned int mnStart     : 12; //!< first position to capture
        unsigned int mnReserved1 :  4; //!< not used
    } msBits;
    unsigned int mnLword;
} tuCamLinkHtr;

/**
 * VTR Register.
 * The Vertical Data Timing Register allows users to discard lines received in
 * a frame (when the data valid, line valid, and frame valid strobes are asserted).
 * At the beginning of a frame (rising edge of frame valid), the number of lines
 * specified by START_LINE will be discarded and not clocked into the FIFO.  Once
 * the START_LINE has been reached, the core will then capture until END_LINE
 * is reached or until the frame valid is de-asserted, at which point
 * the line capture state machine will reset.
 */
typedef union
{
    struct 
    {
        unsigned int mnStop      : 12; //!< Line position to stop capturing
        unsigned int mnReserved  :  4; //!< not used
        unsigned int mnStart     : 12; //!< First line to capture
        unsigned int mnReserved1 :  4; //!< not used
    } msBits;
    unsigned int mnLword;
} tuCamLinkVtr;
#endif
