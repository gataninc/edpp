#ifdef MITYDSP_HEADER_BLOCK
/**************************************************************************/
/*                                                                        */
/*  $------------------------------------------------------------------$  */
/*                                                                        */
/*  $Workfile:: DspCameraLink.h                                        $  */
/*  $Revision:: 5                                                      $  */
/*  $Date:: 1/17/07 10:15a                                             $  */
/*  $Author:: Johnk                                                    $  */
/*                                                                        */
/*  $------------------------------------------------------------------$  */
/*                                                                        */
/*  $Description::                                                     $  */
/*                                                                        */
/*                                                                        */
/*                                                                        */
/*  $------------------------------------------------------------------$  */
/*                                                                        */
/*     o  0                                                               */
/*     | /       Copyright (c) 2007                                       */
/*    (CL)---o   Critical Link, LLC                                       */
/*      \                                                                 */
/*       O                                                                */
/**************************************************************************/
#endif

#ifndef tcArtemis_Dma_H
#define tcArtemis_Dma_H

#include <std.h>
#include <mbx.h>
#include <sem.h>
#include <csl_cache.h>

// Common code belongs in the MityDSP namespace
namespace MityDSP
{        
	#define TimeBufferSize 16384

    ////////////////////////////////////////////////////////////////////////////////
    /// @class tcDspCameraLink DspCameraLink.h "core/DspCameraLink.h"
    /// Instances of this class handle 
    /// @sa @ref tcDspCameraLink_sec "tcDspCameraLink Page"
    ////////////////////////////////////////////////////////////////////////////////
	//    class tcDspCameraLink
    class tcArtemis_Dma
    {
    public:
        tcArtemis_Dma(void *apAddress, int anThreadPrio = 8 );
                        // int anFrameRows = 1, int anFrameCols = 1024);
        virtual ~tcArtemis_Dma();
                
        virtual int CaptureFrames(char*         apCaptureBuffer, unsigned int  anBufferSize );
		int SetDppSocket( int socket );
		void 		Add_TimeBuffer( int key );
		void 		Set_TimeBuffer_Period( float val );
		void 		Set_TimeBuffer_Start( float val );
		int 		Get_TimeBufferPtr( void );
		void 		Set_TimeBufferPtr( int val );
		int 		Get_TimeBuffer( int index );

		unsigned int Interrupt_Count(void );
        virtual int  AbortCapture(void);
        static int 	interrupt_dispatch(Arg arMyObject);

    protected:
        /**
         *  The teFIFOLevel enumeration defines the various FIFO levels that may 
         *  be checked for the tcDspAdcBase class.
         */

        typedef enum
        {
            eeEmpty,      //!<  FIFO is empty
            eeOneQ,       //!<  FIFO is at least one quarter full
            eeHalf,       //!<  FIFO is at least one half full
            eeThreeQ,     //!<  FIFO is at least three quarters full
            eeFull,       //!<  FIFO is full
            eeDMALevel    //!<  Direct FPGA DMA transfer count hit
        } teFIFOLevel;        
       static int  CaptureThreadDispatch(Arg arMyObject);

        virtual void CaptureThread(void);
        virtual void LinkInterrupt(void);
        virtual void DisableFIFOInterrupts(void);
        virtual void SetFIFOInterruptLevel(teFIFOLevel aeLevel);


		#ifdef MITYDSP_PRO
	        CACHE_L2Size           msL2Mode;            //!< used for flushing the cache
		#else
	        CACHE_L2Mode           msL2Mode;            //!< used for flushing the cache
		#endif

        volatile unsigned int* mpBaseAddr;
        unsigned short         mnMyIntMask;         //!< core interrupt mask.
        int                    mnMyIntLevel;        //!< core interrupt level.
        int                    mnMyIntVector;       //!< core interrupt vector.
        SEM_Handle             mhDataSem;           //!< DMA data transfer complete semaphore
        int                    mnBufferSize;        //!< size of the working data buffer
        int                    mnThreadPrio;        //!< DMA receiver thread priority
        char*                  mpCaptureBuffer;     //!< DMA data buffer to transfer to
        volatile bool          mbStopCapture;       //!< when true, abort capturing process
        volatile int           mnCurrDmaStartAddr;  //!< offset (character) to next location for DMA transfers
        int                    mnHalfFifoSize;      //!< 1/2 size of the FIFO (DMA transfer size)
        bool                   	mbIsPacked;          //!< when true, pixels are packed into 2 16 bit words
//		int                    	mnFifoFull;          //!< very, very bad
		void*                  	mpUserData;          //!< passed back to the user from data payload
		volatile bool          	mbFpgaDmaEnabled;    //!< true when direct DMA transfer is being used
		volatile unsigned int 	mnNumFpgaDmaBytes;
		volatile unsigned int 	mnDmaTransferSize;
		volatile unsigned int   mnDmaPacketSize;
		volatile unsigned int 	mnCurrDmaTransferSize;
		volatile unsigned int   mnIntByteCount;
		volatile unsigned int 	mnAlteraInterrupt;
		volatile int 			mnDppConnectedSocket;	// !< TCP Socket for Altera Flash )
		volatile unsigned int	mnThreadSent;
        volatile unsigned int 	mnInterruptCount;         ///< ISR counter (debug).
		volatile unsigned int 	mnThreadCount;
		volatile unsigned int 	mnSemCount;
    };

} // end of MityDSP namespace


#endif











